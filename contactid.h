#ifndef CONTACTID_H
#define CONTACTID_H

#include "msgs.h"


#define TIPUS_EVENT_ALARMA      1
#define TIPUS_EVENT_SUPERVISIO  2
#define TIPUS_EVENT_AVERIA      3
#define TIPUS_EVENT_ONOFF       4
#define TIPUS_EVENT_OMISIO      5
#define TIPUS_EVENT_TEST        6
#define TIPUS_EVENT_PHONECALL   7
#define TIPUS_EVENT_OUTPUT      8
#define TIPUS_EVENT_RELE        9
#define TIPUS_EVENT_BUS         10
#define TIPUS_EVENT_ANALOG      11
#define TIPUS_EVENT_GEO         12


string BusID[]={
    "Master Panel",
    "Wireless rx",
    "res",
    "res",
    "res",
    "res",
    "res",
    "res",

    "RXUNC ",
    "MIO 1",
    "MIO 2",
    "MIO 3",
    "MIO 4",
    "MIO 5",
    "MIO 6",
    "MIO 7",

    "res",
    "res",
    "Gsm-Sms mod",
    "TCP/CSD nuvalink",
    "TX104",
    "Tcp-Ip mod",
    "res",
    "res",

    "res",
    "res",
    "res",
    "res",
    "res",
    "res",
    "res",
    "res",

    "res",
    "res",
    "res",
    "res",
    "res",
    "res",
    "res",
    "res",

    "Relay mod 1",
    "Relay mod 2",
    "Relay mod 3",
    "Relay mod 4",
    "Relay mod 5",
    "Relay mod 6",
    "verify",
    "Rxu4c",

    "res",
    "nuvalink",
    "res",
    "ICON-W",
    "W Repeater",
    "res",
    "KNX interface",
    "res",

    "Keypad 1",
    "Keypad 2",
    "Keypad 3",
    "Keypad 4",
    "Keypad 5",
    "Keypad 6",
    "Keypad 7",
    "Keypad 8"

};


typedef struct {
    string cID;
    string descr;
    int tipus;
} TContactIDdescr;

TContactIDdescr taulaCID[] = {
"000", "MISS_EVENT_CODE00", TIPUS_EVENT_ALARMA,
"001", "MISS_EVENT_CODE01", TIPUS_EVENT_ALARMA,
"002", "MISS_EVENT_CODE02", TIPUS_EVENT_ALARMA,
"003", "MISS_EVENT_CODE03", TIPUS_EVENT_ALARMA,
"004", "MISS_EVENT_CODE04", TIPUS_EVENT_ALARMA,
"005", "MISS_EVENT_CODE05", TIPUS_EVENT_ALARMA,
"006", "MISS_EVENT_CODE06", TIPUS_EVENT_ALARMA,
"007", "MISS_EVENT_CODE07", TIPUS_EVENT_ALARMA,
"008", "MISS_EVENT_CODE08", TIPUS_EVENT_ALARMA,
"009", "MISS_EVENT_CODE09", TIPUS_EVENT_ALARMA,
"010", "MISS_EVENT_CODE10", TIPUS_EVENT_ALARMA,
"011", "MISS_EVENT_CODE11", TIPUS_EVENT_ALARMA,
"012", "MISS_EVENT_CODE12", TIPUS_EVENT_ALARMA,
"013", "MISS_EVENT_CODE13", TIPUS_EVENT_ALARMA,
"014", "MISS_EVENT_CODE14", TIPUS_EVENT_ALARMA,
"015", "MISS_EVENT_CODE15", TIPUS_EVENT_ALARMA,
"016", "MISS_EVENT_CODE16", TIPUS_EVENT_ALARMA,
"017", "MISS_EVENT_CODE17", TIPUS_EVENT_ALARMA,
"018", "MISS_EVENT_CODE18", TIPUS_EVENT_ALARMA,
"019", "MISS_EVENT_CODE19", TIPUS_EVENT_ALARMA,
"020", "MISS_EVENT_CODE20", TIPUS_EVENT_ALARMA,
"021", "MISS_EVENT_CODE21", TIPUS_EVENT_ALARMA,
"022", "MISS_EVENT_CODE22", TIPUS_EVENT_ALARMA,
"023", "MISS_EVENT_CODE23", TIPUS_EVENT_ALARMA,
"024", "MISS_EVENT_CODE24", TIPUS_EVENT_ALARMA,
"025", "MISS_EVENT_CODE25", TIPUS_EVENT_ALARMA,
"026", "MISS_EVENT_CODE26", TIPUS_EVENT_ALARMA,
"027", "MISS_EVENT_CODE27", TIPUS_EVENT_ALARMA,
"028", "MISS_EVENT_CODE28", TIPUS_EVENT_ALARMA,
"029", "MISS_EVENT_CODE29", TIPUS_EVENT_ALARMA,
"030", "MISS_EVENT_CODE30", TIPUS_EVENT_ALARMA,

//Medical Alarms �100
"100", "MISS_EVENT_CID100", TIPUS_EVENT_ALARMA,
"101", "MISS_EVENT_CID101", TIPUS_EVENT_ALARMA,
"102", "MISS_EVENT_CID102", TIPUS_EVENT_ALARMA,
//Fire Alarms �110
"110", "MISS_EVENT_CID110", TIPUS_EVENT_ALARMA,
"111", "MISS_EVENT_CID111", TIPUS_EVENT_ALARMA,
"112", "MISS_EVENT_CID112", TIPUS_EVENT_ALARMA,
"113", "MISS_EVENT_CID113", TIPUS_EVENT_ALARMA,
"114", "MISS_EVENT_CID114", TIPUS_EVENT_ALARMA,
"115", "MISS_EVENT_CID115", TIPUS_EVENT_ALARMA,
"116", "MISS_EVENT_CID116", TIPUS_EVENT_ALARMA,
"117", "MISS_EVENT_CID117", TIPUS_EVENT_ALARMA,
"118", "MISS_EVENT_CID118", TIPUS_EVENT_ALARMA,
//Panic Alarms �120
"120", "MISS_EVENT_CID120", TIPUS_EVENT_ALARMA,
"121", "MISS_EVENT_CID121", TIPUS_EVENT_ALARMA,
"122", "MISS_EVENT_CID122", TIPUS_EVENT_ALARMA,
"123", "MISS_EVENT_CID123", TIPUS_EVENT_ALARMA,
"124", "MISS_EVENT_CID124", TIPUS_EVENT_ALARMA,
"125", "MISS_EVENT_CID125", TIPUS_EVENT_ALARMA,
//Burglar Alarms �130
"130", "MISS_EVENT_CID130", TIPUS_EVENT_ALARMA,
"131", "MISS_EVENT_CID131", TIPUS_EVENT_ALARMA,
"132", "MISS_EVENT_CID132", TIPUS_EVENT_ALARMA,
"133", "MISS_EVENT_CID133", TIPUS_EVENT_ALARMA,
"134", "MISS_EVENT_CID134", TIPUS_EVENT_ALARMA,
"135", "MISS_EVENT_CID135", TIPUS_EVENT_ALARMA,
"136", "MISS_EVENT_CID136", TIPUS_EVENT_ALARMA,
"137", "MISS_EVENT_CID137", TIPUS_EVENT_ALARMA,
"138", "MISS_EVENT_CID138", TIPUS_EVENT_ALARMA,
"139", "MISS_EVENT_CID139", TIPUS_EVENT_ALARMA,
//General Alarm � 140
"140", "MISS_EVENT_CID140", TIPUS_EVENT_ALARMA,
"141", "MISS_EVENT_CID141", TIPUS_EVENT_ALARMA,
"142", "MISS_EVENT_CID142", TIPUS_EVENT_ALARMA,
"143", "MISS_EVENT_CID143", TIPUS_EVENT_BUS,
"144", "MISS_EVENT_CID144", TIPUS_EVENT_ALARMA,
"145", "MISS_EVENT_CID145", TIPUS_EVENT_BUS,
"146", "MISS_EVENT_CID146", TIPUS_EVENT_ALARMA,
"147", "MISS_EVENT_CID147", TIPUS_EVENT_ALARMA,
//24, "Hour Non-Burglary - 150 and 160
"150", "MISS_EVENT_CID150", TIPUS_EVENT_ALARMA,
"151", "MISS_EVENT_CID151", TIPUS_EVENT_ALARMA,
"152", "MISS_EVENT_CID152", TIPUS_EVENT_ALARMA,
"153", "MISS_EVENT_CID153", TIPUS_EVENT_ALARMA,
"154", "MISS_EVENT_CID154", TIPUS_EVENT_ALARMA,
"155", "MISS_EVENT_CID155", TIPUS_EVENT_ALARMA,
"156", "MISS_EVENT_CID156", TIPUS_EVENT_ALARMA,
"157", "MISS_EVENT_CID157", TIPUS_EVENT_ALARMA,
"158", "MISS_EVENT_CID158", TIPUS_EVENT_ALARMA,
"159", "MISS_EVENT_CID159", TIPUS_EVENT_ALARMA,
"161", "MISS_EVENT_CID161", TIPUS_EVENT_ALARMA,
"162", "MISS_EVENT_CID162", TIPUS_EVENT_ALARMA,
"163", "MISS_EVENT_CID163", TIPUS_EVENT_ALARMA,

"1FA", "MISS_EVENT_CID1FA", TIPUS_EVENT_ALARMA,
//SUPERVISORY
//Fire Supervisory - 200 and 210
"200", "MISS_EVENT_CID200", TIPUS_EVENT_SUPERVISIO,
"201", "MISS_EVENT_CID201", TIPUS_EVENT_SUPERVISIO,
"202", "MISS_EVENT_CID202", TIPUS_EVENT_SUPERVISIO,
"203", "MISS_EVENT_CID203", TIPUS_EVENT_SUPERVISIO,
"204", "MISS_EVENT_CID204", TIPUS_EVENT_SUPERVISIO,
"205", "MISS_EVENT_CID205", TIPUS_EVENT_SUPERVISIO,
"206", "MISS_EVENT_CID206", TIPUS_EVENT_SUPERVISIO,
//TROUBLES
//System Troubles -300 and 310
"300", "MISS_EVENT_CID300", TIPUS_EVENT_AVERIA,
"301", "MISS_EVENT_CID301", TIPUS_EVENT_AVERIA,
"302", "MISS_EVENT_CID302", TIPUS_EVENT_AVERIA,
"303", "MISS_EVENT_CID303", TIPUS_EVENT_AVERIA,
"304", "MISS_EVENT_CID304", TIPUS_EVENT_AVERIA,
"305", "MISS_EVENT_CID305", TIPUS_EVENT_AVERIA,
"306", "MISS_EVENT_CID306", TIPUS_EVENT_SUPERVISIO,
"307", "MISS_EVENT_CID307", TIPUS_EVENT_AVERIA,
"308", "MISS_EVENT_CID308", TIPUS_EVENT_BUS,
"309", "MISS_EVENT_CID309", TIPUS_EVENT_AVERIA,
"310", "MISS_EVENT_CID310", TIPUS_EVENT_AVERIA,
"311", "MISS_EVENT_CID311", TIPUS_EVENT_AVERIA,
"312", "MISS_EVENT_CID312", TIPUS_EVENT_AVERIA,
"313", "MISS_EVENT_CID313", TIPUS_EVENT_SUPERVISIO,
//Sounder / Relay Troubles -320
"320", "MISS_EVENT_CID320", TIPUS_EVENT_OUTPUT,
"321", "MISS_EVENT_CID321", TIPUS_EVENT_OUTPUT,
"322", "MISS_EVENT_CID322", TIPUS_EVENT_OUTPUT,
"323", "MISS_EVENT_CID323", TIPUS_EVENT_RELE,
"324", "MISS_EVENT_CID324", TIPUS_EVENT_RELE,
"325", "MISS_EVENT_CID325", TIPUS_EVENT_RELE,
"326", "MISS_EVENT_CID326", TIPUS_EVENT_AVERIA,
"327", "MISS_EVENT_CID327", TIPUS_EVENT_AVERIA,

//System Peripheral Trouble -330 and 340
"330", "MISS_EVENT_CID330", TIPUS_EVENT_AVERIA,
"331", "MISS_EVENT_CID331", TIPUS_EVENT_AVERIA,
"332", "MISS_EVENT_CID332", TIPUS_EVENT_AVERIA,
"333", "MISS_EVENT_CID333", TIPUS_EVENT_BUS,
"334", "MISS_EVENT_CID334", TIPUS_EVENT_AVERIA,
"335", "MISS_EVENT_CID335", TIPUS_EVENT_AVERIA,
"336", "MISS_EVENT_CID336", TIPUS_EVENT_AVERIA,
"337", "MISS_EVENT_CID337", TIPUS_EVENT_BUS,
"338", "MISS_EVENT_CID338", TIPUS_EVENT_BUS,
"339", "MISS_EVENT_CID339", TIPUS_EVENT_BUS,
"341", "MISS_EVENT_CID341", TIPUS_EVENT_BUS,
"342", "MISS_EVENT_CID342", TIPUS_EVENT_BUS,
"343", "MISS_EVENT_CID343", TIPUS_EVENT_BUS,
"344", "MISS_EVENT_CID344", TIPUS_EVENT_AVERIA,
//Communication Troubles -350 and 360
"350", "MISS_EVENT_CID350", TIPUS_EVENT_AVERIA,
"351", "MISS_EVENT_CID351", TIPUS_EVENT_AVERIA,
"352", "MISS_EVENT_CID352", TIPUS_EVENT_AVERIA,
"353", "MISS_EVENT_CID353", TIPUS_EVENT_AVERIA,
"354", "MISS_EVENT_CID354", TIPUS_EVENT_AVERIA,
"355", "MISS_EVENT_CID355", TIPUS_EVENT_AVERIA,
"356", "MISS_EVENT_CID356", TIPUS_EVENT_AVERIA,
"357", "MISS_EVENT_CID357", TIPUS_EVENT_AVERIA,
"359", "MISS_EVENT_CID359", TIPUS_EVENT_AVERIA,
"35D", "MISS_EVENT_CID35D", TIPUS_EVENT_AVERIA,
"35E", "MISS_EVENT_CID35E", TIPUS_EVENT_AVERIA,
"35F", "MISS_EVENT_CID35F", TIPUS_EVENT_AVERIA,
"370", "MISS_EVENT_CID370", TIPUS_EVENT_AVERIA,
"371", "MISS_EVENT_CID371", TIPUS_EVENT_AVERIA,
"372", "MISS_EVENT_CID372", TIPUS_EVENT_AVERIA,
"373", "MISS_EVENT_CID373", TIPUS_EVENT_AVERIA,
"374", "MISS_EVENT_CID374", TIPUS_EVENT_AVERIA,
"375", "MISS_EVENT_CID375", TIPUS_EVENT_AVERIA,
"376", "MISS_EVENT_CID376", TIPUS_EVENT_AVERIA,
"377", "MISS_EVENT_CID377", TIPUS_EVENT_AVERIA,
"378", "MISS_EVENT_CID378", TIPUS_EVENT_AVERIA,
"380", "MISS_EVENT_CID380", TIPUS_EVENT_AVERIA,
"381", "MISS_EVENT_CID381", TIPUS_EVENT_AVERIA,
"382", "MISS_EVENT_CID382", TIPUS_EVENT_AVERIA,
"383", "MISS_EVENT_CID383", TIPUS_EVENT_ALARMA,
"384", "MISS_EVENT_CID384", TIPUS_EVENT_AVERIA,
"385", "MISS_EVENT_CID385", TIPUS_EVENT_AVERIA,
"386", "MISS_EVENT_CID386", TIPUS_EVENT_AVERIA,
"387", "MISS_EVENT_CID387", TIPUS_EVENT_AVERIA,
"388", "MISS_EVENT_CID388", TIPUS_EVENT_AVERIA,
"389", "MISS_EVENT_CID389", TIPUS_EVENT_AVERIA,
"391", "MISS_EVENT_CID391", TIPUS_EVENT_AVERIA,
"392", "MISS_EVENT_CID392", TIPUS_EVENT_AVERIA,
"393", "MISS_EVENT_CID393", TIPUS_EVENT_AVERIA,
//OPEN/CLOSE/REMOTE ACCESS
"400", "MISS_EVENT_CID400", TIPUS_EVENT_ONOFF,
"401", "MISS_EVENT_CID401", TIPUS_EVENT_ONOFF,
"402", "MISS_EVENT_CID402", TIPUS_EVENT_ONOFF,
"403", "MISS_EVENT_CID403", TIPUS_EVENT_ONOFF,
"404", "MISS_EVENT_CID404", TIPUS_EVENT_ONOFF,
"405", "MISS_EVENT_CID405", TIPUS_EVENT_ONOFF,
"406", "MISS_EVENT_CID406", TIPUS_EVENT_ONOFF,
"407", "MISS_EVENT_CID407", TIPUS_EVENT_ONOFF,
"408", "MISS_EVENT_CID408", TIPUS_EVENT_ONOFF,
"409", "MISS_EVENT_CID409", TIPUS_EVENT_ONOFF,
"441", "MISS_EVENT_CID441", TIPUS_EVENT_ONOFF,
"442", "MISS_EVENT_CID442", TIPUS_EVENT_ONOFF,
"450", "MISS_EVENT_CID450", TIPUS_EVENT_ONOFF,
"451", "MISS_EVENT_CID451", TIPUS_EVENT_ONOFF,
"452", "MISS_EVENT_CID452", TIPUS_EVENT_ONOFF,
"453", "MISS_EVENT_CID453", TIPUS_EVENT_ONOFF,
"454", "MISS_EVENT_CID454", TIPUS_EVENT_ONOFF,
"455", "MISS_EVENT_CID455", TIPUS_EVENT_ONOFF,
"456", "MISS_EVENT_CID456", TIPUS_EVENT_ONOFF,
"457", "MISS_EVENT_CID457", TIPUS_EVENT_ONOFF,
"458", "MISS_EVENT_CID458", TIPUS_EVENT_ONOFF,
"459", "MISS_EVENT_CID459", TIPUS_EVENT_ONOFF,
"4FC", "MISS_EVENT_CID4FC", TIPUS_EVENT_ONOFF,
"461", "MISS_EVENT_CID461", TIPUS_EVENT_ONOFF,
"462", "MISS_EVENT_CID462", TIPUS_EVENT_ONOFF,
"463", "MISS_EVENT_CID463", TIPUS_EVENT_ONOFF,
"464", "MISS_EVENT_CID464", TIPUS_EVENT_ONOFF,
"465", "MISS_EVENT_CID465", TIPUS_EVENT_ONOFF,
"466", "MISS_EVENT_CID466", TIPUS_EVENT_ONOFF,
"411", "MISS_EVENT_CID411", TIPUS_EVENT_ONOFF,
"412", "MISS_EVENT_CID412", TIPUS_EVENT_ONOFF,
"413", "MISS_EVENT_CID413", TIPUS_EVENT_ONOFF,
"414", "MISS_EVENT_CID414", TIPUS_EVENT_ONOFF,
"415", "MISS_EVENT_CID415", TIPUS_EVENT_ONOFF,
"416", "MISS_EVENT_CID416", TIPUS_EVENT_ONOFF,
"421", "MISS_EVENT_CID421", TIPUS_EVENT_ONOFF,
"422", "MISS_EVENT_CID422", TIPUS_EVENT_ONOFF,
"423", "MISS_EVENT_CID423", TIPUS_EVENT_ONOFF,
"424", "MISS_EVENT_CID424", TIPUS_EVENT_ONOFF,
"425", "MISS_EVENT_CID425", TIPUS_EVENT_ONOFF,
"426", "MISS_EVENT_CID426", TIPUS_EVENT_ONOFF,
"427", "MISS_EVENT_CID427", TIPUS_EVENT_ONOFF,
"428", "MISS_EVENT_CID428", TIPUS_EVENT_ONOFF,
"429", "MISS_EVENT_CID429", TIPUS_EVENT_ONOFF,
"430", "MISS_EVENT_CID430", TIPUS_EVENT_ONOFF,
"431", "MISS_EVENT_CID431", TIPUS_EVENT_ONOFF,
"432", "MISS_EVENT_CID432", TIPUS_EVENT_ONOFF,
"433", "MISS_EVENT_CID433", TIPUS_EVENT_ONOFF,
"434", "MISS_EVENT_CID434", TIPUS_EVENT_ONOFF,
//BYPASSES / DISABLES
"501", "MISS_EVENT_CID501", TIPUS_EVENT_OMISIO,
//Souder / Relay Disables -520
"520", "MISS_EVENT_CID520", TIPUS_EVENT_OMISIO,
"521", "MISS_EVENT_CID521", TIPUS_EVENT_OMISIO,
"522", "MISS_EVENT_CID522", TIPUS_EVENT_OMISIO,
"523", "MISS_EVENT_CID523", TIPUS_EVENT_OMISIO,
"524", "MISS_EVENT_CID524", TIPUS_EVENT_OMISIO,
"525", "MISS_EVENT_CID525", TIPUS_EVENT_OMISIO,
"526", "MISS_EVENT_CID526", TIPUS_EVENT_OMISIO,
"527", "MISS_EVENT_CID527", TIPUS_EVENT_OMISIO,
//System Peripheral Disables -530 and 540
"531", "MISS_EVENT_CID531", TIPUS_EVENT_OMISIO,
"532", "MISS_EVENT_CID532", TIPUS_EVENT_OMISIO,
//Communication Disables -550 and 560
"551", "MISS_EVENT_CID551", TIPUS_EVENT_OMISIO,
"552", "MISS_EVENT_CID552", TIPUS_EVENT_OMISIO,
"553", "MISS_EVENT_CID553", TIPUS_EVENT_OMISIO,
//Bypasses �570
"570", "MISS_EVENT_CID570", TIPUS_EVENT_OMISIO,
"571", "MISS_EVENT_CID571", TIPUS_EVENT_OMISIO,
"572", "MISS_EVENT_CID572", TIPUS_EVENT_OMISIO,
"573", "MISS_EVENT_CID573", TIPUS_EVENT_OMISIO,
"574", "MISS_EVENT_CID574", TIPUS_EVENT_OMISIO,
"575", "MISS_EVENT_CID575", TIPUS_EVENT_OMISIO,
"576", "MISS_EVENT_CID576", TIPUS_EVENT_OMISIO,
"577", "MISS_EVENT_CID577", TIPUS_EVENT_OMISIO,
//TEST / MISC.
//Test/Misc. �600, 610
"601", "MISS_EVENT_CID601", TIPUS_EVENT_TEST,
"602", "MISS_EVENT_CID602", TIPUS_EVENT_TEST,
"603", "MISS_EVENT_CID603", TIPUS_EVENT_TEST,
"604", "MISS_EVENT_CID604", TIPUS_EVENT_TEST,
"605", "MISS_EVENT_CID605", TIPUS_EVENT_TEST,
"606", "MISS_EVENT_CID606", TIPUS_EVENT_TEST,
"607", "MISS_EVENT_CID607", TIPUS_EVENT_TEST,
"608", "MISS_EVENT_CID608", TIPUS_EVENT_TEST,
"609", "MISS_EVENT_CID609", TIPUS_EVENT_TEST,
"60A", "MISS_EVENT_CID60A", TIPUS_EVENT_ANALOG,
"60B", "MISS_EVENT_CID60B", TIPUS_EVENT_ANALOG,
"610", "MISS_EVENT_CID611", TIPUS_EVENT_TEST,
"611", "MISS_EVENT_CID611", TIPUS_EVENT_TEST,
"612", "MISS_EVENT_CID612", TIPUS_EVENT_TEST,
"613", "MISS_EVENT_CID613", TIPUS_EVENT_TEST,
"614", "MISS_EVENT_CID614", TIPUS_EVENT_TEST,
"615", "MISS_EVENT_CID615", TIPUS_EVENT_TEST,
"616", "MISS_EVENT_CID616", TIPUS_EVENT_TEST,
//Event Log �620
"621", "MISS_EVENT_CID621", TIPUS_EVENT_TEST,
"622", "MISS_EVENT_CID622", TIPUS_EVENT_TEST,
"623", "MISS_EVENT_CID623", TIPUS_EVENT_TEST,
"624", "MISS_EVENT_CID624", TIPUS_EVENT_TEST,
"625", "MISS_EVENT_CID625", TIPUS_EVENT_TEST,
"626", "MISS_EVENT_CID626", TIPUS_EVENT_BUS,
"627", "MISS_EVENT_CID627", TIPUS_EVENT_TEST,
"628", "MISS_EVENT_CID628", TIPUS_EVENT_TEST,
"629", "MISS_EVENT_CID629", TIPUS_EVENT_TEST,
"62A", "MISS_EVENT_CID62A", TIPUS_EVENT_BUS,
//Scheduling �630
"630", "MISS_EVENT_CID630", TIPUS_EVENT_TEST,
"631", "MISS_EVENT_CID631", TIPUS_EVENT_TEST,
"632", "MISS_EVENT_CID632", TIPUS_EVENT_TEST,
//Personnel Monitoring -640
"641", "MISS_EVENT_CID641", TIPUS_EVENT_TEST,
"642", "MISS_EVENT_CID642", TIPUS_EVENT_TEST,
//Misc. -650
"651", "MISS_EVENT_CID651", TIPUS_EVENT_TEST,
"652", "MISS_EVENT_CID652", TIPUS_EVENT_TEST,
"653", "MISS_EVENT_CID653", TIPUS_EVENT_TEST,
"654", "MISS_EVENT_CID654", TIPUS_EVENT_TEST,
// GEO
"6E0", "MISS_EVENT_CID6E0", TIPUS_EVENT_GEO,
"6E2", "MISS_EVENT_CID6E2", TIPUS_EVENT_GEO,

"700", "MISS_EVENT_CID700", TIPUS_EVENT_OUTPUT,
"701", "MISS_EVENT_CID701", TIPUS_EVENT_RELE,

"900", "MISS_EVENT_CID900", TIPUS_EVENT_PHONECALL,
"903", "MISS_EVENT_CID903", TIPUS_EVENT_TEST,

"0", "MISS_EVENT_CID0", TIPUS_EVENT_ALARMA };

#endif
