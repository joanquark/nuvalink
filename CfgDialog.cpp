#include "wx_pch.h"
#include "CfgDialog.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(CfgDialog)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(CfgDialog)
//*)

//(*IdInit(CfgDialog)
const long CfgDialog::ID_STATICTEXT1 = wxNewId();
const long CfgDialog::ID_CHOICE1 = wxNewId();
const long CfgDialog::ID_STATICTEXT2 = wxNewId();
const long CfgDialog::ID_CHOICE2 = wxNewId();
const long CfgDialog::ID_STATICTEXT3 = wxNewId();
const long CfgDialog::ID_CHOICE3 = wxNewId();
const long CfgDialog::ID_PANEL1 = wxNewId();
const long CfgDialog::ID_STATICTEXT6 = wxNewId();
const long CfgDialog::ID_TEXTCTRL3 = wxNewId();
const long CfgDialog::ID_STATICTEXT7 = wxNewId();
const long CfgDialog::ID_TEXTCTRL4 = wxNewId();
const long CfgDialog::ID_STATICTEXT8 = wxNewId();
const long CfgDialog::ID_TEXTCTRL5 = wxNewId();
const long CfgDialog::ID_STATICTEXT9 = wxNewId();
const long CfgDialog::ID_TEXTCTRL6 = wxNewId();
const long CfgDialog::ID_PANEL2 = wxNewId();
const long CfgDialog::ID_NOTEBOOK1 = wxNewId();
const long CfgDialog::ID_SPEEDBUTTON1 = wxNewId();
const long CfgDialog::ID_SPEEDBUTTON2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(CfgDialog,wxDialog)
	//(*EventTable(CfgDialog)
	//*)
END_EVENT_TABLE()

CfgDialog::CfgDialog(wxWindow* parent)
{
	BuildContent(parent);
}

void CfgDialog::BuildContent(wxWindow* parent)
{
	//(*Initialize(CfgDialog)
	wxFlexGridSizer* FlexGridSizer2;
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, wxID_ANY, _("Config"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("wxID_ANY"));
	SetClientSize(wxSize(301,235));
	Notebook1 = new wxNotebook(this, ID_NOTEBOOK1, wxPoint(0,0), wxSize(296,184), 0, _T("ID_NOTEBOOK1"));
	Panel1 = new wxPanel(Notebook1, ID_PANEL1, wxDefaultPosition, wxSize(294,174), wxTAB_TRAVERSAL, _T("ID_PANEL1"));
	FlexGridSizer1 = new wxFlexGridSizer(3, 2, 0, 0);
	StaticText1 = new wxStaticText(Panel1, ID_STATICTEXT1, _("Select Connection"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	FlexGridSizer1->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice1 = new wxChoice(Panel1, ID_CHOICE1, wxDefaultPosition, wxSize(160,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	Choice1->Append(_("USB"));
	Choice1->Append(_("RS485"));
	Choice1->Append(_("GSM CSD"));
	Choice1->Append(_("TCP-IP"));
	Choice1->Append(_("Cloud"));
	FlexGridSizer1->Add(Choice1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(Panel1, ID_STATICTEXT2, _("RS485 Comport"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	FlexGridSizer1->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice2 = new wxChoice(Panel1, ID_CHOICE2, wxDefaultPosition, wxSize(160,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
	FlexGridSizer1->Add(Choice2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText3 = new wxStaticText(Panel1, ID_STATICTEXT3, _("CSD Comport"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	FlexGridSizer1->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice3 = new wxChoice(Panel1, ID_CHOICE3, wxDefaultPosition, wxSize(160,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE3"));
	FlexGridSizer1->Add(Choice3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Panel1->SetSizer(FlexGridSizer1);
	SetSizer(FlexGridSizer1);
	Layout();
	Panel2 = new wxPanel(Notebook1, ID_PANEL2, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("ID_PANEL2"));
	FlexGridSizer2 = new wxFlexGridSizer(2, 2, 10, 30);
	StaticText6 = new wxStaticText(Panel2, ID_STATICTEXT6, _("Cloud IP/DNS"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT6"));
	FlexGridSizer2->Add(StaticText6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtIPCloud = new wxTextCtrl(Panel2, ID_TEXTCTRL3, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL3"));
	FlexGridSizer2->Add(TxtIPCloud, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText7 = new wxStaticText(Panel2, ID_STATICTEXT7, _("Cloud Service Port"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT7"));
	FlexGridSizer2->Add(StaticText7, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtPortCloud = new wxTextCtrl(Panel2, ID_TEXTCTRL4, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL4"));
	FlexGridSizer2->Add(TxtPortCloud, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText8 = new wxStaticText(Panel2, ID_STATICTEXT8, _("IP PRN"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT8"));
	FlexGridSizer2->Add(StaticText8, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtIPPRN = new wxTextCtrl(Panel2, ID_TEXTCTRL5, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL5"));
	FlexGridSizer2->Add(TxtIPPRN, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText9 = new wxStaticText(Panel2, ID_STATICTEXT9, _("Port PRN"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT9"));
	FlexGridSizer2->Add(StaticText9, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtPortPRN = new wxTextCtrl(Panel2, ID_TEXTCTRL6, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL6"));
	FlexGridSizer2->Add(TxtPortPRN, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Panel2->SetSizer(FlexGridSizer2);
	FlexGridSizer2->Fit(Panel2);
	FlexGridSizer2->SetSizeHints(Panel2);
	Notebook1->AddPage(Panel1, _("Connection"), false);
	Notebook1->AddPage(Panel2, _("Cloud & SYS"), false);
	wxBitmap SpeedButton1_BMP(_("C:/LOCALPRJ/01.MLink/icons/toolbar/ok.png"), wxBITMAP_TYPE_ANY);
	SpeedButton1 = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, SpeedButton1_BMP, 0, 2, -1, true, wxPoint(72,184), wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	SpeedButton1->SetUserData(0);
	wxBitmap SpeedButton2_BMP(_("C:/LOCALPRJ/01.MLink/icons/toolbar/exit.png"), wxBITMAP_TYPE_ANY);
	SpeedButton2 = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, SpeedButton2_BMP, 0, 2, -1, true, wxPoint(176,184), wxSize(56,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	SpeedButton2->SetUserData(0);

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CfgDialog::OnSpeedButton1LeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CfgDialog::OnSpeedButton2LeftClick);
	//*)
}

void CfgDialog::SetCfg(CConfig* con)
{
    this->config=con;

    const HKEY HK=HKEY_LOCAL_MACHINE;
    const char * cle="HARDWARE\\DEVICEMAP\\SERIALCOMM";

    //Obrim el registre
    HKEY ret;
    RegOpenKeyEx(HK,cle,0,KEY_ALL_ACCESS,&ret);

    unsigned long lNom=128, lVal=128, ty=0;
    char nomVal[128];
    unsigned char valor[128];

    long res;
    int n=0;
    int comport=255;
    string temp;
    do {
        lNom=128;
        lVal=128;
        res = RegEnumValue( ret, n, nomVal, &lNom, 0, &ty, valor, &lVal);
        if (res == ERROR_NO_MORE_ITEMS)
            break;

        temp = nomVal;
        wxString np = (char *)valor;

        if (temp.find("VCP",0) != string::npos){
            comport = ascii2nibble(valor[3]);
        }else if (temp.find("USB",0) != string::npos){
            comport = ascii2nibble(valor[3]);
            Choice2->Append(np);
        }
        Choice3->Append("COM"+iToS(n));
        //wxMessageBox(np);

        n++;
    } while (res != ERROR_NO_MORE_ITEMS && n<128);
    RegCloseKey(ret);

    (*TxtIPCloud) << (config->GetParamTcpIp("ipclouddest"));
    (*TxtPortCloud) << (config->GetParamTcpIp("ipcloudport"));
    (*TxtIPPRN) << (config->GetParamTcpIp("ipprndest"));
    (*TxtPortPRN) << (config->GetParamTcpIp("ipprnport"));
}

CfgDialog::~CfgDialog()
{
	//(*Destroy(CfgDialog)
	//*)
}


void CfgDialog::OnSpeedButton1LeftClick(wxCommandEvent& event)
{
    EndModal(Choice1->GetSelection());
    config->SetParamTcpIp("ipclouddest",TxtIPCloud->GetValue());
    config->SetParamTcpIp("ipcloudport",TxtPortCloud->GetValue());
    config->SetParamTcpIp("ipprndest",TxtIPPRN->GetValue());
    config->SetParamTcpIp("ipprnport",TxtPortPRN->GetValue());
}

void CfgDialog::OnSpeedButton2LeftClick(wxCommandEvent& event)
{
    EndModal(wxNOT_FOUND);
}
