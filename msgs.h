#ifndef MISS_H
#define MISS_H


#define VERSIO 1.50
    #define MSG_VERSION "1.50"

#define LANG_UN         "un"
#define MISS_PANEL_ON   "On"
#define MISS_PANEL_OFF "Off"


#include "languages/english.h"

#include "languages/CLanguage.h"

#endif
