#include "wx_pch.h"
#include "CloudDialog.h"
#include "jsonval.h"
#include "jsonwriter.h"
#include "jsonreader.h"
#include <wx/stdpaths.h>
#include "util.h"

extern "C" {
    #include "SSL/SSLxfer.h"
}

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(CloudDialog)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(CloudDialog)
//*)

//(*IdInit(CloudDialog)
const long CloudDialog::ID_LISTCTRL1 = wxNewId();
const long CloudDialog::ID_LCDWINDOW1 = wxNewId();
const long CloudDialog::ID_STATICTEXT2 = wxNewId();
const long CloudDialog::ID_LCDWINDOW2 = wxNewId();
const long CloudDialog::ID_BUTTON2 = wxNewId();
const long CloudDialog::ID_BUTTON1 = wxNewId();
const long CloudDialog::ID_SPEEDBUTTON1 = wxNewId();
const long CloudDialog::ID_SPEEDBUTTON2 = wxNewId();
const long CloudDialog::ID_STATICTEXT1 = wxNewId();
const long CloudDialog::ID_TEXTCTRL1 = wxNewId();
const long CloudDialog::ID_STATICTEXT3 = wxNewId();
const long CloudDialog::ID_TEXTCTRL2 = wxNewId();
const long CloudDialog::ID_SPEEDBUTTON3 = wxNewId();
//*)

BEGIN_EVENT_TABLE(CloudDialog,wxDialog)
	//(*EventTable(CloudDialog)
	//*)
END_EVENT_TABLE()

CloudDialog::CloudDialog(wxWindow* parent,wxWindowID id)
{
	BuildContent(parent,id);
	DevIndx=0;
}

void CloudDialog::BuildContent(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(CloudDialog)
	wxBoxSizer* BoxSizer1;
	wxBoxSizer* BoxSizer2;
	wxBoxSizer* BoxSizer3;
	wxBoxSizer* BoxSizer4;
	wxBoxSizer* BoxSizer6;
	wxBoxSizer* BoxSizer7;

	Create(parent, id, _("Cloud Devices"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
	SetClientSize(wxSize(750,350));
	SetMinSize(wxSize(-1,-1));
	BoxSizer1 = new wxBoxSizer(wxVERTICAL);
	BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
	Listab = new wxListCtrl(this, ID_LISTCTRL1, wxDefaultPosition, wxSize(912,300), wxLC_REPORT|wxVSCROLL|wxHSCROLL, wxDefaultValidator, _T("ID_LISTCTRL1"));
	    Listab->ClearAll();
	    Listab->InsertColumn(0,"SN");
	    Listab->InsertColumn(1,"Alias");
	    Listab->InsertColumn(2,"group");
	    Listab->InsertColumn(3,"Last Poll");
	    Listab->InsertColumn(4,"Last Event");
	    Listab->InsertColumn(5,"Device");
	    Listab->InsertColumn(6,"FW");
	    Listab->InsertColumn(7,"Batt");
	    Listab->InsertColumn(8,"Ext power");

	    Listab->SetColumnWidth(0,80);
	    Listab->SetColumnWidth(1,80);
	    Listab->SetColumnWidth(2,80);
	    Listab->SetColumnWidth(3,80);
	    Listab->SetColumnWidth(4,80);
	    Listab->SetColumnWidth(5,80);
	    Listab->SetColumnWidth(6,80);
	    Listab->SetColumnWidth(7,80);
	    Listab->SetColumnWidth(8,80);
	BoxSizer2->Add(Listab, 5, wxALL|wxEXPAND, 5);
	BoxSizer1->Add(BoxSizer2, 2, wxALL|wxEXPAND, 5);
	BoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
	BoxSizer7->Add(-1,-1,1, wxALL|wxEXPAND, 5);
	PosLcd = new wxLCDWindow(this,wxDefaultPosition,wxSize(70,30));
	PosLcd->SetNumberDigits( 4);
	BoxSizer7->Add(PosLcd, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("devices of"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE, _T("ID_STATICTEXT2"));
	BoxSizer7->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TotalLcd = new wxLCDWindow(this,wxDefaultPosition,wxSize(70,30));
	TotalLcd->SetNumberDigits( 4);
	BoxSizer7->Add(TotalLcd, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Back = new wxButton(this, ID_BUTTON2, _("<<"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
	BoxSizer7->Add(Back, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Forward = new wxButton(this, ID_BUTTON1, _(">>"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	BoxSizer7->Add(Forward, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	BoxSizer7->Add(-1,-1,1, wxALL|wxEXPAND, 5);
	BoxSizer1->Add(BoxSizer7, 0, wxALL|wxEXPAND, 5);
	BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
	BoxSizer3->Add(-1,-1,1, wxALL|wxEXPAND, 5);
	wxBitmap Open_BMP(_("./icons/toolbar/cloud-download.png"), wxBITMAP_TYPE_ANY);
	Open = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, Open_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(90,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	Open->SetUserData(0);
	BoxSizer3->Add(Open, 1, wxALL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap Close_BMP(_("./icons/toolbar/exit.png"), wxBITMAP_TYPE_ANY);
	Close = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, Close_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(90,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	Close->SetUserData(0);
	BoxSizer3->Add(Close, 1, wxALL|wxSHAPED|wxFIXED_MINSIZE, 5);
	BoxSizer6 = new wxBoxSizer(wxVERTICAL);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Device Alias"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	BoxSizer6->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TextFilter = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxSize(80,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	BoxSizer6->Add(TextFilter, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	BoxSizer3->Add(BoxSizer6, 1, wxALL, 5);
	BoxSizer4 = new wxBoxSizer(wxVERTICAL);
	StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Filter Group"), wxDefaultPosition, wxDefaultSize, wxALIGN_CENTRE, _T("ID_STATICTEXT3"));
	BoxSizer4->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtFilterGr = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxSize(76,21), wxTE_CENTRE, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	BoxSizer4->Add(TxtFilterGr, 1, wxALL, 5);
	BoxSizer3->Add(BoxSizer4, 1, wxALL, 5);
	wxBitmap Find_BMP(_("./icons/search64.png"), wxBITMAP_TYPE_ANY);
	Find = new wxSpeedButton(this, ID_SPEEDBUTTON3, wxEmptyString, Find_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(90,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON3"));
	Find->SetUserData(0);
	BoxSizer3->Add(Find, 1, wxALL|wxSHAPED|wxFIXED_MINSIZE, 5);
	BoxSizer3->Add(-1,-1,1, wxALL|wxEXPAND, 5);
	BoxSizer1->Add(BoxSizer3, 1, wxALL|wxEXPAND, 5);
	SetSizer(BoxSizer1);
	SetSizer(BoxSizer1);
	Layout();

	Connect(ID_LISTCTRL1,wxEVT_COMMAND_LIST_BEGIN_DRAG,(wxObjectEventFunction)&CloudDialog::OnListabBeginDrag);
	Connect(ID_BUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&CloudDialog::OnBackClick);
	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&CloudDialog::OnForwardClick);
	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CloudDialog::OnSpeedButton1LeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CloudDialog::OnSpeedButton2LeftClick);
	Connect(ID_SPEEDBUTTON3,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CloudDialog::OnFindLeftClick);
	//*)
}

CloudDialog::~CloudDialog()
{
	//(*Destroy(CloudDialog)
	//*)
}

void CloudDialog::SetParams(wxString user,wxString pass,int cnt,wxString IP,int port,wxString *ver)
{
    SysUser=user;
    SysPass=pass;
    DevCnt=cnt;
    versio=ver;
    DevIP=IP;
    DevPort=port;
    Query();
// ------------------------------------------ test code --------------------------
/*    wxJSONValue root;
    int answersize=0;
    char devices[32768];
    root["csId"]="craid1";
    wxJSONWriter writer( wxJSONWRITER_NONE );
    wxString jsontxt;
    writer.Write(root,jsontxt);
    jsontxt+="\r\n";
    if (wxFileExists("nuvathings.pem")){
        CipherFile("nuvathings.pem","nvt.nvk",false);
        wxRemoveFile( "nuvathings.pem" );
    }
    UnCipherFile("nvt.nvk","nvt.pem",false);
    wxStandardPaths path;
    wxString filepem=path.GetDataDir();
    if (SSLtransfer("127.0.0.1",11112,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsontxt.size(),&devices[0],&answersize)==TRUE){
        wxString jsonwithdevices="";
        for (int i=0;i<answersize;i++){
            jsonwithdevices.Append(wxChar(devices[i]));
        }
    }
    wxRemoveFile( "nvt.pem" ); */
}

void CloudDialog::Query()
{
    wxJSONValue root;
    int answersize=0;
    char devices[32768];
    root["Login"]=SysUser;
    root["pass"]=SysPass;
    root["Op"]="get";
    root["group"]=TxtFilterGr->GetValue();
    root["indx"]=_wx(iToS(DevIndx));

    TotalLcd->SetValue(_wx(iToS(DevCnt)));
    PosLcd->SetValue(_wx(iToS(DevIndx)));

    wxJSONWriter writer( wxJSONWRITER_NONE );
    wxString jsontxt;
    writer.Write(root,jsontxt);
    jsontxt+="\r\n";
    if (wxFileExists("nuvathings.pem")){
        CipherFile("nuvathings.pem","nvt.nvk",false);
        wxRemoveFile( "nuvathings.pem" );
    }
    UnCipherFile("nvt.nvk","nvt.pem",false);
    wxStandardPaths path;
    wxString filepem=path.GetDataDir();
    if (SSLtransfer(DevIP,DevPort,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsontxt.size(),&devices[0],&answersize)==TRUE){
        wxString jsonwithdevices="";
        for (int i=0;i<answersize;i++){
            jsonwithdevices.Append(wxChar(devices[i]));
        }
        //wxMessageBox(jsonwithdevices);
        wxJSONReader reader(wxJSONREADER_TOLERANT);
        int numErrors = reader.Parse( jsonwithdevices, &root );
        if ( numErrors > 0 )  {
            // if there are errors in the JSON document return
            return;
        }
        int CntPush=0;
        // now display the column names;
        Listab->DeleteAllItems();
        wxJSONValue dev = root["devices"];
        int size=root.Size();

        for ( int x = 0; x < dev.Size(); x++ )  {
            wxJSONValue value = dev[x];
            int tmp = Listab->InsertItem(CntPush, "-", 0);
            Listab->SetItemData(tmp, CntPush);
            Listab->SetItem(CntPush, 0, _wx(value["SN"].AsString()));
            Listab->SetItem(CntPush, 1, _wx(value["name"].AsString()));
            Listab->SetItem(CntPush, 2, _wx(value["group"].AsString()));

            wxJSONValue lastEv = value["lastEvent"];

            Listab->SetItem(CntPush, 3, lastEv["receivedAtHR"].AsString());
            Listab->SetItem(CntPush, 4, lastEv["event"].AsString());
            wxString tdevice=_wx(value["tdevice"].AsString());
            Listab->SetItem(CntPush, 5, tdevice);
            if (lastEv.HasMember("zone")){
                int zone=lastEv["zone"].AsInt();
            }
            if (value.HasMember("fwh")){
                if (value.HasMember(("fwl"))){
                    int fwv=value["fwh"].AsInt();
                    fwv=value["fwl"].AsInt();
                    wxString ver=_wx(iToS(value["fwh"].AsInt())+iToH(value["fwl"].AsInt(),2));
                    Listab->SetItem(CntPush, 6, ver);
                }

            }
            if (value.HasMember("levelBatt")){
                char buf[16];
                double batt=value["levelBatt"].AsDouble();
                sprintf(buf, "%f",batt);
                string sbatt=buf;
                Listab->SetItem(CntPush, 7, _wx(sbatt));
            }
            if (value.HasMember("levelAc")){
                char buf[16];
                float ac=value["levelAc"].AsDouble();
                sprintf(buf, "%f",ac);
                string sac=buf;
                Listab->SetItem(CntPush, 8, _wx(sac));
            }
            Listab->EnsureVisible(CntPush);
        }
    }
    wxRemoveFile( "nvt.pem" );
}
// --------------------------------- find device ------------------
void CloudDialog::OnFindLeftClick(wxCommandEvent& event)
{
    wxJSONValue root;
    int answersize=0;
    char devices[32768];
    root["Login"]=SysUser;
    root["pass"]=SysPass;
    root["Op"]="find";
    root["devName"]=TextFilter->GetValue();
    wxJSONWriter writer( wxJSONWRITER_NONE );
    wxString jsontxt;
    writer.Write(root,jsontxt);
    jsontxt+="\r\n";
    if (wxFileExists("nuvathings.pem")){
        CipherFile("nuvathings.pem","nvt.nvk",false);
        wxRemoveFile( "nuvathings.pem" );
    }
    UnCipherFile("nvt.nvk","nvt.pem",false);
    wxStandardPaths path;
    wxString filepem=path.GetDataDir();
    if (SSLtransfer(DevIP,DevPort,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsontxt.size(),&devices[0],&answersize)==TRUE){
        wxString jsonwithdevices="";
        for (int i=0;i<answersize;i++){
            jsonwithdevices.Append(wxChar(devices[i]));
        }
        //wxMessageBox(jsonwithdevices);
        wxJSONReader reader(wxJSONREADER_TOLERANT);
        int numErrors = reader.Parse( jsonwithdevices, &root );
        if ( numErrors > 0 )  {
            // if there are errors in the JSON document return
            return;
        }
        int CntPush=0;
        // now display the column names;
        Listab->DeleteAllItems();
        wxJSONValue dev = root["devices"];
        int size=root.Size();

        for ( int x = 0; x < dev.Size(); x++ )  {
            wxJSONValue value = dev[x];
            int tmp = Listab->InsertItem(CntPush, "-", 0);
            Listab->SetItemData(tmp, CntPush);
            Listab->SetItem(CntPush, 0, _wx(value["SN"].AsString()));
            Listab->SetItem(CntPush, 1, _wx(value["name"].AsString()));
            Listab->SetItem(CntPush, 2, _wx(value["group"].AsString()));

            wxJSONValue lastEv = value["lastEvent"];

            Listab->SetItem(CntPush, 3, lastEv["receivedAtHR"].AsString());
            Listab->SetItem(CntPush, 4, lastEv["event"].AsString());
            wxString tdevice=_wx(value["tdevice"].AsString());
            Listab->SetItem(CntPush, 5, tdevice);
            if (lastEv.HasMember("zone")){
                int zone=lastEv["zone"].AsInt();
            }
            if (value.HasMember("fwh")){
                if (value.HasMember(("fwl"))){
                    int fwv=value["fwh"].AsInt();
                    fwv=value["fwl"].AsInt();
                    wxString ver=_wx(iToS(value["fwh"].AsInt())+iToH(value["fwl"].AsInt(),2));
                    Listab->SetItem(CntPush, 6, ver);
                }
            }
            if (value.HasMember("levelBatt")){
                char buf[16];
                double batt=value["levelBatt"].AsDouble();
                sprintf(buf, "%f",batt);
                string sbatt=buf;
                Listab->SetItem(CntPush, 7, _wx(sbatt));
            }
            if (value.HasMember("levelAc")){
                char buf[16];
                float ac=value["levelAc"].AsDouble();
                sprintf(buf, "%f",ac);
                string sac=buf;
                Listab->SetItem(CntPush, 8, _wx(sac));
            }
            Listab->EnsureVisible(CntPush);
        }
    }
    wxRemoveFile( "nvt.pem" );
}

// -------------------- download file from cloud ------------------
void CloudDialog::OnSpeedButton1LeftClick(wxCommandEvent& event)
{
    wxListItem item;
    long itemIndex=-1;
    itemIndex=Listab->GetNextItem(itemIndex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);

    item.m_itemId = itemIndex;
    item.m_col = 0;
    item.m_mask = wxLIST_MASK_TEXT;
    Listab->GetItem(item);

    wxJSONValue root;
    int answersize=0;
    char devices[256000];
    root["Login"]=SysUser;
    root["pass"]=SysPass;
    root["Op"]="open";
    root["SN"]=item.m_text;

    item.m_col = 6;
    Listab->GetItem(item);
    *versio=(wxString)item.m_text;      // returning versio reference

    wxJSONWriter writer( wxJSONWRITER_NONE );
    wxString jsontxt;
    writer.Write(root,jsontxt);
    jsontxt+="\r\n";

    UnCipherFile("nvt.nvk","nvt.pem",false);
    wxStandardPaths path;
    wxString filepem=path.GetDataDir();
    if (SSLtransfer(DevIP,DevPort,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsontxt.size(),&devices[0],&answersize)==TRUE){
        // keep wxString with JSON instalation data.
        wxRemoveFile("nvt.pem");
        for (int i=0;i<answersize;i++){
            JSONDev.Append(wxChar(devices[i]));
        }
        EndModal(wxID_OK);
    }else{
        wxRemoveFile("nvt.pem");
        EndModal(wxID_CLOSE);
    }
}

wxString CloudDialog::GetJSONDev()
{
    return JSONDev;
}

void CloudDialog::OnSpeedButton2LeftClick(wxCommandEvent& event)
{
    EndModal(wxID_OK);
}

void CloudDialog::OnSpeedButton4LeftClick(wxCommandEvent& event)
{
}

void CloudDialog::OnForwardClick(wxCommandEvent& event)
{
    if ((DevIndx+10)<DevCnt){
        DevIndx+=10;

    }
    Query();
}

void CloudDialog::OnBackClick(wxCommandEvent& event)
{
    if (DevIndx>10){
        DevIndx-=10;
    }
    else{
        DevIndx=0;
    }
    Query();
}



void CloudDialog::OnListabBeginDrag(wxListEvent& event)
{
}
