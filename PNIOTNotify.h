#if !defined(PNIOTNotify_H)
#define PNIOTNotify_H

/////////////////////////////////////////////////////////////////////////////////
// PNIOTNOtify
//
// Purpose:		abstract class used to notify about ProtocolNIOT frames to other objects rather than main.

class PNIOTNotify
{
public:


	PNIOTNotify ()
	{}

	// notify methods
	virtual void OnDataAvail ( unsigned char* data, int len ) = 0;
	virtual void OnUpdate ( string cmd ) = 0;

};

#endif
