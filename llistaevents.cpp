#include "llistaevents.h"

CEventList::CEventList()
{
    punter=0;
    bytesEvent=-1;
    maxPunter=-1;
    tipusEvents=TIPUS_EVENT_MSK;
    rebreTots = false;
}

//Fa un clear de la llista d'events pero s'enrecorda de les altres dades
void CEventList::Clear()
{
    for (int i=0; i<events.size(); i++)
        delete events[i];

    vector<CEvent *> tmp;
    events.clear();
    tmp.swap(events);
    rebreTots = false;
}

CEventList::~CEventList()
{
    Clear();
}

/*bool CEventList::SetDirEeprom(string &adr)
{
    addressIni = adr;
    isEeprom=true;
    return true;
}^*/

bool CEventList::SetDirFlash(wxString adr)
{
    addressFlash = adr;
    return true;
}

bool CEventList::SetPunter(int punter)
{
    if (maxPunter>0) {
        this->punter = punter % maxPunter;
    }
    else
        this->punter = punter;

    return true;
}

bool CEventList::SetMaxPunter(int max)
{
    if (max <0 )
        return false;

    maxPunter = max;
    return true;
}

bool CEventList::SetBytesEvent(int bytes)
{
    if (bytes <0 )
        return false;

    bytesEvent = bytes;
    return true;
}

bool CEventList::SetTypeEvents(int tipus)
{
    tipusEvents = tipus;
    return true;
}

wxString &CEventList::GetDirFlash()
{
    return addressFlash;
}


int CEventList::GetPunter()
{
    return punter;
}

int CEventList::GetBytesEvent()
{
    return bytesEvent;
}

int CEventList::AddEvent(CEvent *event)
{
    events.push_back(event);
    return events.size()-1;
}


CEvent *CEventList::GetEvent(int index)
{
    if ((index >= 0) && (index < events.size()))
        return events[index];
    else
        return 0;
}

bool CEventList::DeleteEvent(int index)
{

    vector<CEvent*>::iterator it;

    if ( (index >= 0) && (index < events.size()) ){
        it=events.begin()+index;
        events.erase(it);
        return TRUE;
    }else{
        return FALSE;
    }

}


bool CEventList::ModifyEvent(int index,CEvent *event)
{
    if ( (index >= 0) && (index < events.size()) )
        events[index]=event;
    else
        return FALSE;
}

int CEventList::GetMaxPunter()
{
    return maxPunter;
}

int CEventList::GetNumEvents()
{
    return events.size();
}

int CEventList::GetTypeConEvents()
{
    return tipusEvents;
}
