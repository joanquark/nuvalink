#ifndef MAIN_H
#define MAIN_H

/* ****************************************************************************
 * nuvalink
 * Joan Vidal
 *
 * Versio       Data    Programador Descr
 * ------   --------                ----------------------

* 1.10     24/08/15    Joan             + NuvaLink IOT config platform, new icon issued, .mli filescreated
* 2.01     10/05/17    Joan             + T_NIOT_PKT_DEVST ( fast Device Status return )
           21/11/17    Joan             + Events queue size and address failure when read.
                                        + wifi rssi not shown as long not present in DetSt packet.
           30/01/18    Joan             + Before send packet, flush any pending data of socket , solving problem with Cloud connections as thread is stopped.
           07/02/18    Joan             + Socket fail on G500 transfer
                                        + Deveices won't sent FF blocks (fast rx alias ), so beforehand IniRawMsk, inits RawMsk from file values, so avoiding set it to FF blocks
* 2.05     27/10/18    JOan             + ciphered DevSt return solve ( receive padding of packet )
           01/12/18    Joan             + TDATA_INT32
 * ************************************************************************** */


#include <wx/wxprec.h>
#include <wx/filedlg.h> //File Dialog
#include <wx/textctrl.h>
#include <wx/treectrl.h>
#include <wx/panel.h>
#include <wx/imaglist.h>
#include <wx/icon.h>
#include <wx/sizer.h>
#include <wx/wizard.h>
#include <wx/splitter.h>
#include <wx/image.h>
#include <wx/bitmap.h>
#include <wx/sound.h>
#include <iostream>
#include <wx/wfstream.h>
#include <wx/txtstrm.h>
#include <string>
using namespace std;

#include "nuvalink.h"
#include "paneledit.h"
#include "panelllegenda.h"
#include "msgs.h"
#include "myapp.h"

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

class MyFrame: public wxFrame
{
    Cnuvalink* nuvalink;
    CLanguage* Lang;
    bool online, instOberta;
    wxSplitterWindow *splitter1, *splitter;

	public:
        wxTextCtrl *control;
        wxTreeCtrl *Tree;
        CPanelEdit *panelEdit;
        wxPanel *panel;
        wxTimer *timerConexio;
        wxTimer *timerHello;
        wxToolBar *tb;
        string DirectoriBase;

        wxSocketClient* socket;
        wxSocketServer* sockets;

        wxString ServerAddress;
        wxString ServerPort;
        wxIPV4address Clientaddr;

        wxString fileName;

		MyFrame(wxFrame *frame, const wxString& title,CLanguage *Lang,Cnuvalink *nuvalink);
		~MyFrame();
        void SetTitleBar();
		void Setnuvalink(Cnuvalink* nuvalink);
		bool OpenInstal(string& nomFitxer);
        void NeedLogin(void);


	private:
	    void OnXQuit(wxCloseEvent &event);
		void OnQuit(wxCommandEvent& event);
        void OnLogin(wxCommandEvent& event);
		void OnAbout(wxCommandEvent& event);
		void OnNew(wxCommandEvent& event);
		void OnOpen(wxCommandEvent& event);
		void OnOpenMnu(wxCommandEvent & event);
		void OnOpenRecent0(wxCommandEvent& event);
		void OnOpenRecent1(wxCommandEvent& event);
		void OnOpenRecent2(wxCommandEvent& event);
		void OnOpenRecent3(wxCommandEvent& event);
		void Open(int recent);
		void OnSave(wxCommandEvent& event);
		void OnSaveAs(wxCommandEvent& event);
		void OnExport(wxCommandEvent& event);
		void OnClose(wxCommandEvent& event);
		void OnAddComponent(wxCommandEvent& event);
		void OnDelComponent(wxCommandEvent& event);
		void OnChangeModelName(wxCommandEvent& event);
		void OnConConfig(wxCommandEvent& event);
		void OnConLocal(wxCommandEvent& event);
		void OnConModem(wxCommandEvent& event);
		void OnConCSD(wxCommandEvent& event);
		void OnConTcpip(wxCommandEvent& event);
		void OnTreeSelection(wxTreeEvent& event);
        void OnTreeMenu(wxTreeEvent& event);
        void OnConnect(wxCommandEvent& event);
        void OnDisconnect(wxCommandEvent& event);
        void OnButtReceive(wxCommandEvent& event);
        void OnButtSend(wxCommandEvent& event);
        void OnButtSendCh(wxCommandEvent& event);
        void OnMenuRxFlash(wxCommandEvent& event);
        void OnMenuTxFlash(wxCommandEvent& event);
        void OnMenuMngPacket(wxCommandEvent& event);
        void OnMenuTxPacket(wxCommandEvent& event);
        void OnMenuResetBusError(wxCommandEvent& event);
        void OnMenuLog(wxCommandEvent& event);
        void OnButtVerify(wxCommandEvent& event);
        void OnAplicar(wxCommandEvent& event);

        void OnButtLog(wxCommandEvent& event);
        void OnButtReset(wxCommandEvent& event);
        void OnButtEepReset(wxCommandEvent& event);
        void OnButtFlash(wxCommandEvent& event);
        void OnButtEep(wxCommandEvent& event);
        void OnFloatMnu(wxTreeEvent& event);
        void TimerHello(wxTimerEvent& event);
        void SetOnline(bool online);
        void SetInstOberta(bool oberta);
        bool AskSaveInstal();
        bool ChoiceNewModel(string *model, string *versio);
        void MakeFileInstal(wxMenu* fileMenu);
        void FloatMnuInstalacio();
        void FloatMnuGrup();
        void FloatMnuRebre();
        void FloatMnuModel();

        string GetStrVal(int intConvert);

		DECLARE_EVENT_TABLE();
};


int idDsaveFrame = wxNewId();
int idBotoAddFrame = wxNewId();
int idBotoModifyFrame = wxNewId();
int idBotoDeleteFrame = wxNewId();
int idBotoSendFrame = wxNewId();
int idMDesignator = wxNewId();
int idMFrame = wxNewId();
int idDMngFrame = wxNewId();


class DManageFrame : public wxDialog
{
    wxComboBox* MDesignator;
    wxTextCtrl* MFrame;
    wxButton* bDeleteFrame;
    wxButton* bAddFrame;
    wxButton* bModifyFrame;
    wxButton* bSendFrame;
    wxButton *FBoutonCancel;
    Cnuvalink  *nuvalink;
    int Mindex;
    bool FNoChangeLabel;
public:
    DManageFrame(wxWindow* parent, wxWindowID id, const wxString& title,Cnuvalink  *nuvalink);
    void OnKeyTextCtrlChanged(wxCommandEvent& event);
    void OnKeyComboboxChanged(wxCommandEvent& event);

private:

    void OnButtDeleteFrame(wxCommandEvent& event);
    void OnButtAddFrame(wxCommandEvent& event);
    void OnButtModifyFrame(wxCommandEvent& event);
    void OnButtSendFrame(wxCommandEvent& event);
    void OnButtApplyFrame(wxCommandEvent& event);
    void OnSelectDesignator(wxCommandEvent& event);
    wxArrayString ListDesignators;
    wxArrayString ListFrames;
    DECLARE_EVENT_TABLE();
};




#endif // MAIN_H
