#include "AgroChannels.h"


CAgroChannels::CAgroChannels()
{
    Clear();
}

CAgroChannels::~CAgroChannels()
{
    Clear();
}

void CAgroChannels::Clear()
{
    InA1="";
    InA2="";
    InA3="";
    InD1="";
    InD2="";
    InCC="";
    I2C="";
}

CAgroChannels CAgroChannels::operator= (CAgroChannels param)
{
    return *this;
}


bool CAgroChannels::setInA1(string ext)
{
    InA1=ext;
    return true;
}

bool CAgroChannels::setInA2(string ext)
{
    InA2=ext;
    return true;
}

bool CAgroChannels::setInA3(string ext)
{
    InA3=ext;
    return true;
}

bool CAgroChannels::setInD1(string ext)
{
    InD1=ext;
    return true;
}

bool CAgroChannels::setInD2(string ext)
{
    InD2=ext;
    return true;
}
bool CAgroChannels::setInCC(string ext)
{
    InCC=ext;
    return true;
}

bool CAgroChannels::setI2C(string ext)
{
    I2C=ext;
    return true;
}


string& CAgroChannels::getInA1()
{
    return InA1;
}

string& CAgroChannels::getInA2()
{
    return InA2;
}

string& CAgroChannels::getInA3()
{
    return InA3;
}

string& CAgroChannels::getInD1()
{
    return InD1;
}

string& CAgroChannels::getInD2()
{
    return InD2;
}

string& CAgroChannels::getInCC()
{
    return InCC;
}

string& CAgroChannels::getI2C()
{
    return I2C;
}
