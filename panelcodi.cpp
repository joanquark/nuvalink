#include "panelcodi.h"

//asistente
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/tokenzr.h>
#include "NuvaLink.h"

//#define WXID_CONST_WIZARDBUTTON 0x250000

int idBotoTx = wxNewId();
int idBotoRx = wxNewId();

BEGIN_EVENT_TABLE(CPanelCodi, wxPanel)
     //EVT_TEXT(wxID_ANY,CPanelCodi::ChangeText)
    EVT_TEXT_ENTER(wxID_ANY,CPanelCodi::ChangeText)
    EVT_BUTTON(idBotoTx, CPanelCodi::OnButtTx)
    EVT_BUTTON(idBotoRx, CPanelCodi::OnButtRx)
END_EVENT_TABLE()


CMyObj::CMyObj()
{
data = NULL;
}

CMyObj::CMyObj(void *in_data)
{
data = in_data;
}

CMyObj::~CMyObj()
{
data = NULL;
}


CPanelCodi::CPanelCodi(wxWindow* par,wxWindowID id,Cnuvalink *nuva,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CLanguage* lang)
{

    //this->parent.ptr_winPan=par;
    nuvalink=nuva;

    Create(par,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,25);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,25);
    }

    Lang = lang;

    text=0;
    choiceh=0;
    choicel=0;
    label=0;
    labelKpA=0;
    codi=0;
    TxButton=0;
    RxButton=0;

    //asistente
    //wizardButton=0;
    model = "";
    versio = "";

    vSizer = new wxBoxSizer( wxVERTICAL );
    hSizer = new wxBoxSizer( wxHORIZONTAL );
    SetSizer( hSizer );

    //this.nuvaLink=nuvalink;
}

void CPanelCodi::Clear()
{
    if (text) {
        delete text;
        text=0;
    }
    if (choiceh){
        delete choiceh;
        choiceh=0;
    }
    if (choicel){
        delete choicel;
        choicel=0;
    }
    if (label) {
        delete label;
        label=0;
    }
    if (labelKpA) {
        delete labelKpA;
        labelKpA=0;
    }

    //asistente
//    if (wizardButton) {
//        delete wizardButton;
//        wizardButton=0;
//    }

    if (TxButton){
        delete TxButton;
        TxButton=0;
    }
    if (RxButton){
        delete RxButton;
        RxButton=0;
    }

    int i=0;
    while (vSizer->Detach(i++));
    i=0;
    while (hSizer->Detach(i++));



}

CPanelCodi::~CPanelCodi()
{
    Clear();
    codi=0;
}

bool correct(){

 return true;
}
void CPanelCodi::ChangeText(wxCommandEvent& event)
{
//   wxCommandEvent event( wxEVT_MY_EVENT, GetId() );
//   wxObject *myObjText = event.GetObject();

    // Give it some contents
 //   ((wxTextCtrl *)myObjText).SetValue( wxT("Hallo") );
 //   wxObject myobj =
 //   wxTextCtrl myText = (wxTextCtrl)event.GetClientObject();
  //  event.SetString(_wx("herlo"));
   // if(mytext.IsModified())
//    ((wxTextCtrl)event).GetString();//currentText
// if (correct())
//     {
//       nuvalink->SetTreeItemBold();
//         event.Skip();
//          wxMessageBox(_("HHHH"));
//     }
 //    else

         text->SetFocus();
}



void CPanelCodi::LostFocusText(wxFocusEvent& event)
{

    /*
     wxObject obj = new wxTextCtrl();

     obj.SetValue();

     m_this = new wxTextCtrl();
     m_this = (wxTextCtrl *) ( (CMyObj*)(event.m_callbackUserData) )->data;

  //  wxTextCtrl intText = new wxTextCtrl(m_this);
//    wxObject* myObj = (CMyObj *)(event.m_callbackUserData);

   //  wxTextCtrl mysup = (wxTextCtrl)((wxObject) *m_this);

     wxString myStr = ((wxTextCtrl)m_this).GetValue();
//    wxTextCtrl *m_this = (wxTextCtrl*)(myObj->data);
//    wxMessageBox();



  //   if (m_this->IsModified())
  //   {
 //       nuvalink->SetTreeItemBold();
  //      wxMessageBox(_("HHHH"));
         event.Skip();
  //   }
  //   else
   //     event.Skip();
     //    text->SetFocus();

   // wxMessageBox(_wx(event.GetString()));

   // ((wxTextCtrl)object).clear();

*/
}
/*
bool CPanelCodi::Setnuvalink(Cnuvalink *nuvalink)
{
    this->nuvalink=nuvalink;
    return true;
}
*/
void CPanelCodi::GenComponents()
{

    static int ie=0;
    if (codi) {
        Clear();


        //wxBitmap TxButton_BMP(_("./icons/toolbar/uploadm.png"), wxBITMAP_TYPE_ANY);
        //TxButton = new wxSpeedButton(this, idBotoTx, wxEmptyString, TxButton_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(30,30), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTONCOM"));
        //TxButton->SetMinSize(wxSize(0,0));
        //TxButton->SetMaxSize(wxSize(0,0));
        //TxButton->SetUserData(0);

        TxButton = new wxBitmapButton(this, idBotoTx, wxBitmap(wxImage(wxT("icons/toolbar/uploadm.png"))), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator);
        TxButton->SetDefault();
        TxButton->SetToolTip(wxT("Tx"));
        //TxButton new wxButton(this, idBotoTx, _T("Tx"),

        Connect(idBotoTx,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CPanelCodi::OnButtTx);


        //wxBitmap RxButton_BMP(_("./icons/toolbar/downloadm.png"), wxBITMAP_TYPE_ANY);
        //RxButton = new wxSpeedButton(this, idBotoRx, wxEmptyString, RxButton_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(30,30), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTONCOM"));
        //RxButton->SetMinSize(wxSize(0,0));
        //RxButton->SetMaxSize(wxSize(0,0));
        //RxButton->SetUserData(0);
        RxButton = new wxBitmapButton(this, idBotoRx, wxBitmap(wxImage(wxT("icons/toolbar/downloadm.png"))), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator);
        RxButton->SetDefault();
        RxButton->SetToolTip(wxT("Rx"));
        //RxButton new wxButton(this, idBotoTx, _T("Tx"),

        Connect(idBotoRx,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CPanelCodi::OnButtRx);

        // 99B3CC - 9baccc - 99a4a8 - e8ebef
        SetBackgroundColour(wxColour(0xB8,0xC0,0xCA));

        if (codi->GetTypeConData() == TDATA_BITMASK) {
            SetBackgroundColour(wxColour(0xC8,0xD0,0xDA));
            wxString tLabel;
            string kpaddress = codi->GetKpAddress();
            if ( kpaddress != "")
                tLabel = " " + _wx(codi->GetKpAddress()) + " - " + codi->GetDescr();
            else
                tLabel = " " + _wx(codi->GetDescr());
            wxStaticBox *boxKpA = new wxStaticBox(this, -1, _wx(tLabel));

            hBoxSizer = new wxStaticBoxSizer(boxKpA, wxHORIZONTAL);
            hBoxSizer->Add( RxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
            hBoxSizer->Add( TxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);

            for (int i=0; i<8; i++) {
                string descr = codi->GetBitDescr(i);
                wxCheckBox *checkBox = new wxCheckBox(this, -1, _wx(descr), wxDefaultPosition, wxDefaultSize, wxNO_BORDER|wxALIGN_LEFT);
                check.push_back(checkBox);
                bool res;
                unsigned char *p, c=0x01;
                p = codi->GetVal();
                if ( *p & (c<<i) )
                    res=true;
                else
                    res=false;
                checkBox->SetValue(res);
                vSizer->Add(checkBox);
            }

            hBoxSizer->Add(vSizer);
            vSizer->Layout();
        }
        else {
            wxString tLabel;
            tLabel = _wx(codi->GetKpAddress()) +" - " +_wx(codi->GetDescr());
            wxStaticBox *boxKpA = new wxStaticBox(this, -1, _wx(""));
            hBoxSizer = new wxStaticBoxSizer(boxKpA, wxHORIZONTAL);
            label = new wxStaticText(this,-1,wxT(""),wxDefaultPosition,wxSize(1,-1),wxST_NO_AUTORESIZE|wxNO_BORDER|wxALIGN_LEFT);
            label->SetLabel(wxT("Description"));
            label->SetLabel(_wx(tLabel));
            if (codi->GetWizz()==1){
                choiceh = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
                wxStringTokenizer arrTok(codi->GetWizzh(), wxT(";"));
                while ( arrTok.HasMoreTokens() )
                {
                    wxString tok=arrTok.GetNextToken();
                    choiceh->AppendString(Lang->GetModelMiss(tok));
                }
                BYTE *data;
                data=codi->GetVal();
                choiceh->SetSelection(data[0]);
                hBoxSizer->Add( label, wxSizerFlags(3).Center());
                hBoxSizer->Add( RxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
                hBoxSizer->Add( TxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
                hBoxSizer->Add( choiceh, wxSizerFlags(2).Center().Border(wxTOP|wxLEFT|wxDOWN,3));
            }else if (codi->GetWizz()==2){
                choiceh = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
                choicel = new wxChoice(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
                wxStringTokenizer arrTok(codi->GetWizzh(), wxT(";"));
                while ( arrTok.HasMoreTokens() )
                {
                    wxString tok=arrTok.GetNextToken();
                    choiceh->AppendString(Lang->GetModelMiss(tok));
                }
                wxStringTokenizer arrTok1(codi->GetWizzl(), wxT(";"));
                while ( arrTok1.HasMoreTokens() )
                {
                    wxString tok=arrTok1.GetNextToken();
                    choicel->AppendString(Lang->GetModelMiss(tok));
                }
                BYTE *data;
                data=codi->GetVal();
                choicel->SetSelection(data[0]&0x0F);
                choiceh->SetSelection(data[0]>>4);
                hBoxSizer->Add( label, wxSizerFlags(3).Center());
                hBoxSizer->Add( RxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
                hBoxSizer->Add( TxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);


                hBoxSizer->Add( choiceh, wxSizerFlags(2).Center().Border(wxTOP|wxLEFT|wxDOWN,3));
                hBoxSizer->Add( choicel, wxSizerFlags(2).Center().Border(wxTOP|wxLEFT|wxDOWN,3));
            }else{
                if (codi->IsEditable() == false){
                    text = new wxTextCtrl(this,ie,wxT(""),wxDefaultPosition,wxSize(1,-1),wxST_NO_AUTORESIZE|wxSUNKEN_BORDER|wxTE_READONLY);
                }
                else{
                    text = new wxTextCtrl(this,-1,wxT(""),wxDefaultPosition,wxSize(1,-1),wxST_NO_AUTORESIZE|wxSUNKEN_BORDER);
                }
                text->SetLabel(_wx(codi->GetStringVal()));
                text->SetMaxLength(codi->GetValStringLength());
                hBoxSizer->Add( label, wxSizerFlags(3).Center());
                hBoxSizer->Add( RxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
                hBoxSizer->Add( TxButton,0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);


                hBoxSizer->Add( text, wxSizerFlags(2).Center().Border(wxTOP|wxLEFT|wxDOWN,3));
            }
        }
        hBoxSizer->Layout();
        hSizer->Add( hBoxSizer, wxSizerFlags(1).Expand());
        hSizer->Layout();
    }
}

bool CPanelCodi::SetCodi(CCodi *codi, const string &model, const string &versio, int privlevel,unsigned int tzona)
{
    this->codi = codi;
    this->model = model;
    this->versio = versio;
    this->privlevel = privlevel;        // passing nuvalink privilege level.
    this->tzona=tzona;
    GenComponents(/*Addr*/);
    return true;
}

void CPanelCodi::UnSetCodi()
{
    codi=0;
    Clear();
}

CCodi *CPanelCodi::GetCodi()
{
    return codi;
}

//Aplica el valor del camp de text al CCodi corresponent
bool CPanelCodi::ApplyValues()
{
    if (!codi)
        return false;
    string add=codi->GetAddress();
    if (codi->GetTypeConData() == TDATA_BITMASK) {
        if (check.size()!=8)
            return false;

        unsigned char p=0, c=0x01;
        bool res;
        for (int i=0; i<8; i++) {
            wxCheckBox *checkBox = check[i];
            if (!checkBox) return false;
            res = checkBox->GetValue();
            if (res) p |= c;
            c <<= 1;
        }
        if (!codi->SetVal(&p)) return false;
    }
    else {
        if (codi->GetWizz()==1){
            BYTE data;
            data=choiceh->GetSelection();
            codi->SetVal(&data);
        }else if (codi->GetWizz()==2){
            BYTE data;
            data=((choiceh->GetSelection()<<4) | (choicel->GetSelection()));
            codi->SetVal(&data);
        }else{
            if (!text) return false;

            if (codi->IsEditable()) {
                string lab = (string)(text->GetLabel());
                wxString codidesc = codi->GetDescr();
            // ----------------------------- mirem privilegis per aplicar valor a aquesta adre�a ------------------------------
                if (codi->GetUpl()>privlevel) {
                    if (codi->GetStringIniVal()!=lab){
                        if (codi->GetStringIniVal()==codi->GetDefVal()){
                            // ha canviat pero l'anterior era el valor de f�brica!!!, aix� significa crear codi, i no necessita perm�s nivell 2.
                        }else{
                            wxMessageBox(Lang->GetAppMiss("MSG_ERROR_NOTENOUGH_PRIVLEVEL") + _wx(this->label->GetLabel()));
                            return false;
                        }
                    }
                }
                if (!codi->SetStringVal(lab)){
                    return false;
                }
            }
        }
    }

    return true;
}

//Actualitza els valors del CCodi al camp de text
//(es el contrari que ApplyValues()
bool CPanelCodi::UpdateValues()
{
    if (!codi)
        return false;

    string add=codi->GetAddress();
    if (codi->GetTypeConData() == TDATA_BITMASK) {
        if (check.size()!=8)
            return false;
        bool res;
        unsigned char *p, c=0x01;
        for (int i=0; i<8; i++) {
            p = codi->GetVal();
            if ( *p & (c<<i) ) res=true;
            else res=false;
            wxCheckBox *checkBox = check[i];
            if (checkBox) checkBox->SetValue(res);
            else return false;
        }
    }
    else {
        if (codi->GetWizz()==1){
            BYTE *data;
            data=codi->GetVal();
            choiceh->SetSelection(data[0]);
        }else if (codi->GetWizz()==2){
            BYTE *data;
            data=codi->GetVal();
            choicel->SetSelection(data[0]&0x0F);
            choiceh->SetSelection(data[0]>>4);
        }else{
            if (!text) return false;
            string lab = codi->GetStringVal();
            text->SetLabel(_wx(lab));
        }
    }
    return true;
}

void CPanelCodi::OnButtTx(wxCommandEvent &ev)
{
   // parent.ptr_winPan->SendCodi(this->GetCodi());
    this->ApplyValues();
    nuvalink->SendCodeValue(this->codi,false);

}

void CPanelCodi::OnButtRx(wxCommandEvent &ev)
{
    if (nuvalink->IsRxCodi()==false && nuvalink->IsTxOn()==false){
        nuvalink->ReceiveCodeValue(this->codi,false);
        this->UpdateValues();
    }
   // parent.ptr_winPan->ReadCodi(this->GetCodi());
}
