//Aixo es un tro� del libcomm.h del Joan
//Hi ha els defines dels identificadors de totes les trames
#define OFF_IDEN    3
/* ******************************* FRAME IDENTIFIERS ************************ */



  // m�scares de for�ament de l'estat d'usuari
#define M_USER_FORCEARM                	0x8000
#define M_USER_FORCEDISARM             	0x4000
#define M_USER_NIGHTARM					0x2000
#define M_USER_PERIMETERARM				0x1000

#define  ON_USER_NUMBER_MASK			0x0FFF
#define  ON_USER_INSTAL                	0
#define  ON_USER_MASTER                	1
#define  ON_USER_CODE                  	1
#define  ON_USER_CANCEL                	2
#define  ON_USER_SERVICE               	4
// ------------------------------- system arming/disarming users -------------------
#define  ON_USER_KEYZ1                 	0xF1	// Es per tal que ON_USER internament assigni codi 15 de clau, per� que ho diferenci� de trucada desde teclat.
#define  ON_USER_SYSAUT					0xFA
#define  ON_USER_NUVALINK               0xFE
// ------------------------------- system arming/disarming users -------------------
#define  ON_USER_WRONG                 	0xFF
