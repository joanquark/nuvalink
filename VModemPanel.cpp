#include "wx_pch.h"
#include "VModemPanel.h"

/*
	ChoiceVModem->Append(_("V23"));
	ChoiceVModem->Append(_("Bell 202"));
	ChoiceVModem->Append(_("V21 originate"));
	ChoiceVModem->Append(_("V21 answer"));
	ChoiceVModem->Append(_("BELL 103 originate"));
	ChoiceVModem->Append(_("BELL 103 answer"));
	ChoiceVModem->Append(_("Vista 48/25/12D"));
	ChoiceVModem->Append(_("Vista 4140"));
	ChoiceVModem->Append(_("Paradox SP/MG series"));
	ChoiceVModem->Append(_("Texecom Premier series"));
	ChoiceVModem->Append(_("Napco XP series"));
	ChoiceVModem->Append(_("Network NX8/4"));
	ChoiceVModem->Append(_("DSC PC16xx series"));
	ChoiceVModem->Append(_("BOSCH ICP series"));
	ChoiceVModem->Append(_("BOSCH DS series"));
	ChoiceVModem->Append(_("Pyronix Matrix series"));
	ChoiceVModem->Append(_("ROKONET Prosys series"));
	ChoiceVModem->Append(_("SATEL Integra series"));
	*/
const char VModemSelect[18]=
            {'0','1','2','3','4','5','A','9','5','2','4','4','4','2','4','2','4','2'};


#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(VModemPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(VModemPanel)
//*)

//(*IdInit(VModemPanel)
const long VModemPanel::ID_STATICTEXT4 = wxNewId();
const long VModemPanel::ID_CHOICE2 = wxNewId();
const long VModemPanel::ID_STATICTEXT3 = wxNewId();
const long VModemPanel::ID_CHOICE1 = wxNewId();
const long VModemPanel::ID_CHECKBOX1 = wxNewId();
const long VModemPanel::ID_TEXTCTRL2 = wxNewId();
const long VModemPanel::ID_STATICTEXT1 = wxNewId();
const long VModemPanel::ID_LCDWINDOW1 = wxNewId();
const long VModemPanel::ID_STATICTEXT2 = wxNewId();
const long VModemPanel::ID_SPEEDBUTTON1 = wxNewId();
const long VModemPanel::ID_SPEEDBUTTON2 = wxNewId();
const long VModemPanel::ID_TIMER1 = wxNewId();
//*)

int SOCKETSMODEM_ID = wxNewId();


BEGIN_EVENT_TABLE(VModemPanel,wxPanel)
	//(*EventTable(VModemPanel)
	//*)
	EVT_SOCKET  (SOCKETSMODEM_ID , VModemPanel::SocketSModemEvent)

END_EVENT_TABLE()

VModemPanel::VModemPanel(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	BuildContent(parent,id,pos,size);
}

void VModemPanel::BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(VModemPanel)
	wxBoxSizer* BoxSizer1;
	wxFlexGridSizer* FlexGridSizer1;
	wxStaticBoxSizer* StaticBoxSizer2;
	wxStaticBoxSizer* StaticBoxSizer3;

	Create(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(3, 1, 0, 0);
	StaticBoxSizer3 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Config"));
	StaticText3 = new wxStaticText(this, ID_STATICTEXT4, _("Serial Port"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
	StaticBoxSizer3->Add(StaticText3, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceCOM = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxSize(126,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
	ChoiceCOM->Append(_("COM1"));
	ChoiceCOM->Append(_("COM2"));
	ChoiceCOM->Append(_("COM3"));
	ChoiceCOM->Append(_("COM4"));
	ChoiceCOM->Append(_("COM5"));
	ChoiceCOM->Append(_("COM6"));
	ChoiceCOM->Append(_("COM7"));
	ChoiceCOM->Append(_("COM8"));
	ChoiceCOM->Append(_("COM9"));
	ChoiceCOM->Append(_("COM10"));
	ChoiceCOM->Append(_("COM11"));
	ChoiceCOM->Append(_("COM12"));
	ChoiceCOM->Append(_("COM13"));
	ChoiceCOM->Append(_("COM14"));
	ChoiceCOM->Append(_("COM15"));
	ChoiceCOM->Append(_("COM16"));
	ChoiceCOM->Append(_("COM17"));
	ChoiceCOM->Append(_("COM18"));
	ChoiceCOM->Append(_("COM19"));
	ChoiceCOM->Append(_("COM20"));
	StaticBoxSizer3->Add(ChoiceCOM, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT3, _("VModem Selection"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	StaticBoxSizer3->Add(StaticText2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceVModem = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxSize(103,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	ChoiceVModem->Append(_("V23"));
	ChoiceVModem->Append(_("Bell 202"));
	ChoiceVModem->Append(_("V21 originate"));
	ChoiceVModem->Append(_("V21 answer"));
	ChoiceVModem->Append(_("BELL 103 originate"));
	ChoiceVModem->Append(_("BELL 103 answer"));
	ChoiceVModem->Append(_("Vista 48/25/12D"));
	ChoiceVModem->Append(_("Vista 4140"));
	ChoiceVModem->Append(_("Paradox SP/MG/DGP series"));
	ChoiceVModem->Append(_("Texecom Premier series"));
	ChoiceVModem->Append(_("Napco XP series"));
	ChoiceVModem->Append(_("Network NX8/4"));
	ChoiceVModem->Append(_("DSC PC16xx series"));
	ChoiceVModem->Append(_("BOSCH ICP series"));
	ChoiceVModem->Append(_("BOSCH DS series"));
	ChoiceVModem->Append(_("Pyronix Matrix series"));
	ChoiceVModem->Append(_("ROKONET Prosys series"));
	ChoiceVModem->Append(_("SATEL Integra series"));
	StaticBoxSizer3->Add(ChoiceVModem, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	CheckLog = new wxCheckBox(this, ID_CHECKBOX1, _("Logging Off"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_CHECKBOX1"));
	CheckLog->SetValue(false);
	StaticBoxSizer3->Add(CheckLog, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer3, 1, wxALL|wxEXPAND, 5);
	StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Logging"));
	TxtLog = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxSize(742,339), wxTE_AUTO_SCROLL|wxTE_MULTILINE|wxTE_RICH, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	StaticBoxSizer2->Add(TxtLog, 1, wxALL|wxEXPAND, 5);
	FlexGridSizer1->Add(StaticBoxSizer2, 1, wxALL|wxEXPAND, 5);
	BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Time Elapsed"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	BoxSizer1->Add(StaticText1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	LcdTime = new wxLCDWindow(this,wxDefaultPosition,wxSize(110,34));
	LcdTime->SetNumberDigits( 5);
	BoxSizer1->Add(LcdTime, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	CurSt = new wxStaticText(this, ID_STATICTEXT2, _("Current Status"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	BoxSizer1->Add(CurSt, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	wxBitmap StartBt_BMP(_("./icons/toolbar/connect_creating.png"), wxBITMAP_TYPE_ANY);
	StartBt = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, StartBt_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(30,30), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	StartBt->SetMinSize(wxSize(0,0));
	StartBt->SetMaxSize(wxSize(0,0));
	StartBt->SetUserData(0);
	BoxSizer1->Add(StartBt, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	wxBitmap StopBt_BMP(_("./icons/toolbar/connect_no.png"), wxBITMAP_TYPE_ANY);
	StopBt = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, StopBt_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(30,30), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	StopBt->SetMaxSize(wxSize(0,0));
	StopBt->SetUserData(0);
	BoxSizer1->Add(StopBt, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	FlexGridSizer1->Add(BoxSizer1, 1, wxALL|wxEXPAND, 5);
	SetSizer(FlexGridSizer1);
	TimerCtrl.SetOwner(this, ID_TIMER1);
	TimerCtrl.Start(25, false);
	FlexGridSizer1->Fit(this);
	FlexGridSizer1->SetSizeHints(this);

	Connect(ID_CHOICE2,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&VModemPanel::OnChoiceCOMSelect);
	Connect(ID_CHOICE1,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&VModemPanel::OnChoiceVModemSelect);
	Connect(ID_CHECKBOX1,wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&VModemPanel::OnCheckLogClick);
	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&VModemPanel::OnStartBtLeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&VModemPanel::OnStopBtLeftClick);
	Connect(ID_TIMER1,wxEVT_TIMER,(wxObjectEventFunction)&VModemPanel::OnTimerCtrlTrigger);
	//*)

	sockets=psocket=0;
	cnt25ms=0;
	verbose=true;
	napcoquickloader=false;


}


VModemPanel::~VModemPanel()
{
	//(*Destroy(VModemPanel)
	//*)
	if (sockets)
        sockets->Destroy();
    if (psocket)
        psocket->Destroy();

    psocket=sockets=0;
    serial.Close();
    TimerCtrl.Stop();
}

void VModemPanel:: SetParams(CProtocolNIOT* proto,CConfig* conf)
{
    this->protocolNIOT=proto;
    this->config=conf;

    Start();
}

void VModemPanel::Start()
{

    if (protocolNIOT->IsOnline()==false){
        TxtLog->AppendText("** Need to be Online before Start Virtual Modem **\r\n");
        return;
    }
    TxtLog->AppendText(">> DEVICE REQUEST >>\r\n");
    string scom=config->GetConnectionParam("vmodemCOM").c_str();
    vmodemCOM=sToI(scom);
    if (vmodemCOM<=0)
        vmodemCOM=1;
    ChoiceCOM->Select(vmodemCOM-1);

    hconnected=vconnected=sconnected=false;
    wxIPV4address   addr;
    addr.AnyAddress();
    string sport=config->GetParamTcpIp("ipvmodemport").c_str();
    int port=sToI(sport);
    addr.Service(port);

    if (sockets){
        sockets->Destroy();
        sockets=0;
    }
    try {
        sockets = new wxSocketServer(addr,wxSOCKET_NOWAIT);
    }
    catch (std::string error) {
        error="UNABLE TO CREATE VMODEM SOCKET\r\n";
        TxtLog->AppendText(_wx(error));
        return;
    }
    sockets->Discard();
    sockets->SetEventHandler(*this, SOCKETSMODEM_ID);
    sockets->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
    sockets->Notify(true);

    eLapsed_t=0;
    LcdTime->SetValue(iToS(eLapsed_t));

    if (serial.Open(vmodemCOM,115200,8,1,0,true,true)==false){
        TxtLog->AppendText("COMPORT open failure\r\n");
    }else{
        serial.SetDTR(true);                  // DTR always on (Bosch AMAX)
        serial.SetRTS(true,0xFF);             // RTS mode On
        //serial.SetDTR(false);                 // DTR only when connected
        //serial.SetRTS(true,0xFF);             // RTS mode mode.
        protocolNIOT->GenOrderPacket(ORDER_TEST,0,0);
        if (protocolNIOT->SendPacket()){
            TxtLog->AppendText("**<< DEVICE ONLINE **\r\n");
        }else{
            TxtLog->AppendText("**<< DEVICE DOES NOT ANSWER, TRY LATER **\r\n");
        }
    }
    TimerCtrl.Start(25, false);
}
// -------------------------------------------------------------------------
/*
void VModemPanel::OnPeerDataIn(unsigned char* data,int len)
{
    psocket->Write(data,len);
}

// -------------------------------------------------------------------------
void VModemPanel::OnPeerConnect(bool condis)
{
    if (condis==true){
        vconnected=true;
        HayesSendConnect();
    }else{
        vconnected=false;
        HayesSendNoCarrier();
    }
}*/

// -------------------- interrupts every 25ms ------------------
void VModemPanel::OnTimerCtrlTrigger(wxTimerEvent& event)
{
    if (++cnt25ms>=40){
        cnt25ms=0;
        LcdTime->SetValue(iToS(++eLapsed_t));
    }
    char buff[512];
    uint16_t len;
    protocolNIOT->DisThreadPoll(true);
    if (len=serial.ReadData(buff,512)){
        sconnected=true;
        ProcessFrameFromPC(buff,len);
    }else{
            protocolNIOT->ReceiveAllIdens();
            // means the peer is connected.
            int rx=protocolNIOT->ProcessRx(25);
            if (rx>0){
                int jj=0;
            }

            CNIOT tr;
            if (protocolNIOT->SearchAnswer(true)){
                unsigned char* dades=protocolNIOT->GetData(&len);
                if (tr.GetTNIOTFr(protocolNIOT->GetTramaRx())==T_NIOT_PKT_STREAM){
                    if (sconnected){
                        serial.SendData(dades,len);
                    }else if (psocket!=NULL){
                        psocket->Write(dades,len);
                    }
                    if (CheckLog->IsChecked()==false)
                        ShowRawFrame(dades,len,true);
                }else if (tr.GetTNIOTFr(protocolNIOT->GetTramaRx())==T_NIOT_PKT_RPC){
                    if (dades[0]==T_NIOT_RPC_ORDER){
                        if (dades[1]==ORDER_DISCONNECT || dades[1]==ORDER_FINISH_COMM){
                            vconnected=false;
                            serial.SetDTR(false);
                            serial.SetRTS(true,0xFF);
                            HayesSendNoCarrier();
                        }else if (dades[1]==ORDER_VMODEM || dades[1]==ORDER_CONNECT){
                            vconnected=true;
                            serial.SetDTR(true);
                            serial.SetRTS(true,0xFF);
                            HayesSendConnect();
                        }else if (dades[1]==ORDER_DO_CALLBACK){
                            // indicate the software there is callback,
                            iscallback=true;
                            HayesSendRing();
                        }
                    }
                }
            }
    }
}

void VModemPanel::SocketSModemEvent(wxSocketEvent &event)
{
    wxSocketBase *tmpsock;
    char buff[512];
    tmpsock = event.GetSocket();

    int i;
    switch (event.GetSocketEvent()){
        case wxSOCKET_CONNECTION:
        {  // When Connecting from VSP3.
            if (hconnected==false){
                hconnected=true;
                psocket=sockets->Accept(false);
                psocket->SetEventHandler(*this, SOCKETSMODEM_ID);
                psocket->SetNotify( wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG | wxSOCKET_OUTPUT_FLAG);
                psocket->Notify(true);
                TxtLog->AppendText("***Client TCP Connected***\r\n");
            }
            break;
        }
        case wxSOCKET_INPUT:
        {   // When receiving something from the port
            if (hconnected){
                psocket->Read(buff,512);
                int cnt=psocket->LastCount();
                ProcessFrameFromPC(buff,cnt);
            }
            break;
        }

        case wxSOCKET_LOST:
        {
            if (hconnected){
                psocket->Destroy();
                psocket=0;
                hconnected=false;
                TxtLog->AppendText("***Client Disconnected***\r\n");
            }
            break;
        }
        case wxSOCKET_OUTPUT:
        {
            break;
        }
    }
}


bool VModemPanel::HayesDecode(char* buff,int CntBRx)
{
    int i;
    int phayes;
    wxString codeph;

    if (CntBRx>512)
        return false;

    if (CntBRx){
        if (StrCmp((char*)&hayescmd[0],"+++")==3){
            TxtLog->SetDefaultStyle(wxTextAttr(*wxBLACK, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL)));
            TxtLog->AppendText(_wx(hayescmd));

            HayesSendOk();
        }else if (StrCmp((char*)&hayescmd[0],"at")==2){
            // this a hayes chain..... analize then
            TxtLog->SetDefaultStyle(wxTextAttr(*wxBLACK, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL)));
            TxtLog->AppendText(_wx(hayescmd));

            bool sendatdok=true;
            phayes=2;
            while ((isprnchar(hayescmd[phayes])) && (phayes<CntBRx)){
                switch (hayescmd[phayes]){
                    case '&':
                        phayes++;
                        break;
                    case 'v':
                        phayes++;
                        verbose=true;
                        if (hayescmd[phayes]=='0')
                            verbose=false;
                        phayes++;
                        break;
                    case 'b':
                        phayes++;
                        if (isdigitchar(hayescmd[phayes]))
                            VModem=hayescmd[phayes++];

                        break;
                    case 's':
                        phayes++;
                        while ((!isletterchar(hayescmd[phayes])) && (phayes<CntBRx))
                            phayes++;
                        break;
                    case 'z':
                        phayes++;
                        VModem='4';         // default modem setting.
                        verbose=true;
                        napcoquickloader=false;
                        break;
                    case 'h':
                        // Hang down!
                        phayes++;
                        protocolNIOT->GenOrderPacket(ORDER_DISCONNECT, 0,0);
                        protocolNIOT->SendPacket();
                        break;
                    case 'a':
                        phayes++;
                        if (napcoquickloader)
                            sendatdok=false;
                        if (iscallback){
                            protocolNIOT->GenOrderPacket(ORDER_VMODEM, &VModem,1);
                            protocolNIOT->SendPacket();
                        }
                        break;
                    case 'x':
                        phayes++;
                        if (isdigit(hayescmd[phayes])){
                            phayes++;
                        }
                        break;
                    case 'q':
                        phayes++;
                        if (isdigit(hayescmd[phayes])){
                            if (hayescmd[phayes]=='6')      // ja que veig: AT&F&Q6S37=1N0&C1S9=4.
                                napcoquickloader=true;
                            phayes++;
                        }
                        break;
                    case ';':
                    case 'r':
                        // this is reverse channel mode.
                        if (VModem=='4')
                            VModem='5';
                        if (VModem=='2')
                            VModem='3';
                        phayes++;
                        break;
                    case 'd':
                        phayes++;
                        iscallback=false;
                        protocolNIOT->GenOrderPacket(ORDER_VMODEM, &VModem,1);
                        protocolNIOT->SendPacket();
                        if (napcoquickloader==false){
                            sendatdok=false;
                        }else{
                            sendatdok=true;
                        }

                        break;          // continue if a or ; at the end of atdt chain
                    default:
                        phayes++;
                        break;
                }
            }
            if (sendatdok){
                HayesSendOk();
            }
        }else if (vconnected){
            protocolNIOT->SendStreamPkt(buff,CntBRx);
            if (CheckLog->IsChecked()==false)
                ShowRawFrame(buff,CntBRx,true);
        }
        return true;
    }else{
        return false;
    }
}

// -----------------------------------------------------------------------------------------------
void VModemPanel::HayesSendOk()
{
    if (sconnected){
        if (verbose)
            serial.SendData((char*)"\r\nOK\r\n",6);
        else
            serial.SendData((char*)"\r0\r",3);
    }else if (hconnected){
        if (verbose)
            psocket->Write((char*)"\r\nOK\r\n",6);
        else
            psocket->Write((char*)"\r0\r",6);
    }
    TxtLog->AppendText("\r\nOK\r\n");
}

// -----------------------------------------------------------------------------------------------
void VModemPanel::HayesSendRing()
{
    if (sconnected){
            serial.SendData((char*)"\r\nRING\r\n",8);
    }else if (hconnected){
            psocket->Write((char*)"\r\nRING\r\n",8);
    }
    TxtLog->AppendText("\r\nRING\r\n");
}

void VModemPanel::HayesSendConnect()
{
    if (verbose==false){
        if (sconnected)
            serial.SendData((char*)"1\r",2);
        else
            psocket->Write("1\r",2);
    }else{
        if (sconnected)
            serial.SendData((char*)"\r\nCONNECT\r\n",11);
        else
            psocket->Write("\r\nCONNECT\r\n",11);
    }
    TxtLog->AppendText("\r\nCONNECT\r\n");
}

void VModemPanel::HayesSendNoCarrier()
{
    if (sconnected)
        serial.SendData((char*)"\r\nNO CARRIER\r\n",14);
    else if (hconnected)
        psocket->Write("\r\nNO CARRIER\r\n",14);

    TxtLog->AppendText("\r\nNO CARRIER\r\n");
}

void VModemPanel::ProcessFrameFromPC(unsigned char* buff, int len)
{
    char endchar;
    for (int i=0;i<len;i++){
        char minor=SetMinorCase((char)buff[i]);
        cmd+=minor;
        hayescmd+=minor;
        phayesrx++;
        endchar=(unsigned char)buff[i];
    }
    if ((endchar==0x0D) || (endchar==0x0A) || (endchar==';')){
        int len=phayesrx;
        phayesrx=0;
        if (HayesDecode(buff,len)){
        }
        hayescmd="";
        cmd="";
    }else if (vconnected){
            // data mode, transparent data to the peer.
        protocolNIOT->SendStreamPkt(buff,len);
        if (CheckLog->IsChecked()==false)
            ShowRawFrame(buff,len,false);

    }
}

void VModemPanel::ShowRawFrame(unsigned char* frame,int len,bool rx)
{
    if (rx){
        TxtLog->SetDefaultStyle(wxTextAttr(*wxBLUE, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        TxtLog->AppendText(">");
    }else{
        TxtLog->SetDefaultStyle(wxTextAttr(*wxRED, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        TxtLog->AppendText(">");
    }
    wxString datafr="";
    for (int i=0; i<len; i++) {
        datafr.Append((wxChar)nibble2ascii((frame[i]&0xF0)>>4));
        datafr.Append((wxChar)nibble2ascii((frame[i]&0x0F)));
        if (i<len-1){ //*text << ", ";
            datafr.Append(',');
            datafr.Append(' ');
        }
    }
    datafr+="\n";
    TxtLog->AppendText(datafr);
}

void VModemPanel::OnStartBtLeftClick(wxCommandEvent& event)
{
    Start();
}

void VModemPanel::OnStopBtLeftClick(wxCommandEvent& event)
{
    TimerCtrl.Stop();
    protocolNIOT->GenOrderPacket(ORDER_DISCONNECT, 0,0);
    protocolNIOT->SendPacket();
    serial.Close();

	if (sockets)
        sockets->Destroy();
    if (psocket)
        psocket->Destroy();

    psocket=sockets=0;

}

void VModemPanel::OnChoiceCOMSelect(wxCommandEvent& event)
{
    vmodemCOM=ChoiceCOM->GetSelection()+1;
    config->SetParamConexio("vmodemCOM",_wx(iToS(vmodemCOM)));
}

void VModemPanel::OnChoiceVModemSelect(wxCommandEvent& event)
{
    VModem=VModemSelect[ChoiceVModem->GetSelection()];
}

void VModemPanel::OnCheckLogClick(wxCommandEvent& event)
{
}
