#ifndef SSLXFER_H_INCLUDED
#define SSLXFER_H_INCLUDED

#include <string>
using namespace std;

extern void helloworld(void);
extern BOOL SSLtransfer(char* host,uint16_t port,char* verifyCert,char* path,char* txt,int size,char* answer,int *answersize);

extern BOOL SSLConnect(char* host,uint16_t port,char* verifyCert,char* path);
extern BOOL SSLClose();
extern BOOL SSLSend(unsigned char* buff, int len);
extern int SSLReceive(unsigned char* buff, int MAX);

#endif // SSLXFER_H_INCLUDED

