
// ######################################################
// #                                                             #
// #          Author: Joan Vidal                                 #
// #                                                             #
// #                                               ###############
// #                                               ##### NUVA ####
// ###############################################################


//#include "SSLxfer.h"

#ifdef HAVE_CONFIG_H
        #include <config.h>
#endif

#include <wolfssl/ssl.h>

#if defined(WOLFSSL_MDK_ARM) || defined(WOLFSSL_KEIL_TCP_NET)
        #include <stdio.h>
        #include <string.h>

        #if !defined(WOLFSSL_MDK_ARM)
            #include "cmsis_os.h"
            #include "rl_fs.h"
            #include "rl_net.h"
        #else
            #include "rtl.h"
            #include "wolfssl_MDK_ARM.h"
        #endif
#endif

#include <wolfssl/wolfcrypt/settings.h>

#if !defined(WOLFSSL_TRACK_MEMORY) && !defined(NO_MAIN_DRIVER)
    /* in case memory tracker wants stats */
    #define WOLFSSL_TRACK_MEMORY
#endif

#include <wolfssl/ssl.h>

#include <ssl/testwolfssl.h>

//WOLFSSL_API int ProcessChainBuffer(WOLFSSL_CTX* ctx, const unsigned char* buff,long sz, int format, int type, WOLFSSL* ssl);


//const char* const pem[]={
//"    -----BEGIN CERTIFICATE-----\r\nMIIDmTCCAoGgAwIBAgIEMleJrjANBgkqhkiG9w0BAQsFADB9MQswCQYDVQQGEwJFUzESMBAGA1UE\r\nCBMJQ2F0YWx1bnlhMRIwEAYDVQQHEwlCYXJjZWxvbmExEzARBgNVBAoTCm51dmF0aGluZ3MxFDAS\r\nBgNVBAsTC2VuZ2luZWVyaW5nMRswGQYDVQQDExJ3d3cubnV2YXRoaW5ncy5jb20wHhcNMTYwNDI1\r\nMTYxNDI5WhcNMTYwNzI0MTYxNDI5WjB9MQswCQYDVQQGEwJFUzESMBAGA1UECBMJQ2F0YWx1bnlh\r\nMRIwEAYDVQQHEwlCYXJjZWxvbmExEzARBgNVBAoTCm51dmF0aGluZ3MxFDASBgNVBAsTC2VuZ2lu\r\nZWVyaW5nMRswGQYDVQQDExJ3d3cubnV2YXRoaW5ncy5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IB\r\nDwAwggEKAoIBAQCk4g2y83x3WXvOqLoLCwsZkRJagmfXNzSqyWtbbWhYS/NTxLIjcAjsVgV1nzgl\r\nPCL+usziyVxxybsAS9HCXOZmxB0TBudj/Lp6d1W5Bi0gzBxxKrv8dZUUM+Rkkz/L1NvPewVVoWqS\r\nvwpVpZtB6++tjoafD68aYc0xQqPTI3IR8EO9jQMMs8Ptd8I5xmrb0+u2pNrW0NqU40db6x11O8Ni\r\nzTGHAYz5IkXBlkxBZCZE7gDsJm5xveWho2ikEbZc8iTWYewRYNekEsoESa0ccL0hrLgoFR0ylqAV\r\ngD0lIu6vu0G24v5fXOE7r0EMlHuthPpvuen5IuHzYunwBKkcqnG3AgMBAAGjITAfMB0GA1UdDgQW\r\nBBS1HCkhzvBEt0rZGT3f+KpIM44ZLjANBgkqhkiG9w0BAQsFAAOCAQEAPerNd/L0AbW7V8MF+9N3\r\nK3Mnyz9uDN3VsRUwI821xXWcSB1pzUKOc0+iEVhKg1EKPN6sEaT9wYNL00R7IneNmWWUaaCLxXl8\r\nc5yEAjyS5ZMpMPoYHsiND2xzbUc6gbu+mAipaFZe1YcBDFzCVKqSETOsL+Li89n6wFr9XQODARw/\r\nvfJ5KX1J0BJuscFFDTMlSwa0kKkcAHayQMWHg+kA9olduJZ1CUxOgF8yaZcB+p0GuJhkSnmMSIBS\r\nV4ecmMdTlRYHr+hZSYpeyon6xUcFj66Qr4LVH0gfcN9sJXRkYsDm8jqEj3K+heDeEwdHM1wrPahh\r\nV3M6kAKAusAnqsXc3w==\r\n-----END CERTIFICATE-----\r\n"
//};

SOCKET_T sockfd = WOLFSSL_SOCKET_INVALID;
WOLFSSL_METHOD*  method  = 0;
WOLFSSL_CTX*     ctx     = 0;
WOLFSSL*         ssl     = 0;

static int TcpConnect(SOCKET_T* sockfd, const char* ip, word16 port,int udp, WOLFSSL* ssl)
{
    SOCKADDR_IN_T addr;
    build_addr(&addr, ip, port, udp);
    if (udp) {
        wolfSSL_dtls_set_peer(ssl, &addr, sizeof(addr));
    }
    tcp_socket(sockfd, udp);

    if (!udp) {
        if (connect(*sockfd, (const struct sockaddr*)&addr, sizeof(addr)) != 0)
            return 0;
    }
    return 1;
}

static int NonBlockingSSL_Connect(WOLFSSL* ssl)
{
#ifndef WOLFSSL_CALLBACKS
    int ret = wolfSSL_connect(ssl);
#else
    int ret = wolfSSL_connect_ex(ssl, handShakeCB, timeoutCB, timeout);
#endif
    int error = wolfSSL_get_error(ssl, 0);
    SOCKET_T sockfd = (SOCKET_T)wolfSSL_get_fd(ssl);
    int select_ret;
    int tries=10;

    while (tries  && ret != SSL_SUCCESS && (error == SSL_ERROR_WANT_READ || error == SSL_ERROR_WANT_WRITE)) {
        int currTimeout = 1;

/*        if (error == SSL_ERROR_WANT_READ)
            printf("... client would read block\n");
        else
            printf("... client would write block\n");
*/
#ifdef WOLFSSL_DTLS
        currTimeout = wolfSSL_dtls_get_current_timeout(ssl);
#endif
        select_ret = tcp_select(sockfd, currTimeout);

        if ((select_ret == TEST_RECV_READY) || (select_ret == TEST_ERROR_READY)) {
            #ifndef WOLFSSL_CALLBACKS
                ret = wolfSSL_connect(ssl);
            #else
                ret = wolfSSL_connect_ex(ssl,handShakeCB,timeoutCB,timeout);
            #endif
            error = wolfSSL_get_error(ssl, 0);
        }
        else if (select_ret == TEST_TIMEOUT && !wolfSSL_dtls(ssl)) {
            error = SSL_ERROR_WANT_READ;
        }
#ifdef WOLFSSL_DTLS
        else if (select_ret == TEST_TIMEOUT && wolfSSL_dtls(ssl) &&
                                            wolfSSL_dtls_got_timeout(ssl) >= 0) {
            error = SSL_ERROR_WANT_READ;
        }
#endif
        else {
            error = SSL_FATAL_ERROR;
        }
        if (tries)
            tries--;
    }
//    if (ret != SSL_SUCCESS){
 //       err_sys("SSL_connect failed");
 //   }

    return ret;
}


BOOL SSLSend (unsigned char* buff, int len)
{
    if (ssl){
        wolfSSL_write(ssl, buff, len);
        return TRUE;
    }
    return FALSE;
}

int SSLReceive(unsigned char* buff, int MAX)
{
    int read=0;
    if (ssl){
        read=wolfSSL_read(ssl, buff, MAX);
        Sleep(10);
        if (read<0){
            return 0;
        }
        return read;
    }
    return 0;
}

BOOL SSLClose()
{
    if (ssl){
        wolfSSL_shutdown(ssl);
        closesocket(sockfd);
        wolfSSL_free(ssl);
        wolfSSL_CTX_free(ctx);
        wolfSSL_Cleanup();

        ssl =   NULL;
        ctx =   NULL;
        sockfd = WOLFSSL_SOCKET_INVALID;
    }
    return TRUE;
}

BOOL SSLConnect(char* host,uint16_t port,char* verifyCert,char* path)
{
    sockfd = WOLFSSL_SOCKET_INVALID;
    method  = 0;
    ctx     = 0;
    ssl     = 0;
    //char* verifyCert = "nuvathings.pem";

    wolfSSL_Init();
    method = wolfTLSv1_2_client_method();
    if (method==NULL)
        return FALSE;
    ctx = wolfSSL_CTX_new(method);
    if (ctx==NULL)
        return FALSE;

    wolfSSL_CTX_SetMinDhKey_Sz(ctx, 1024);
    int res=wolfSSL_CTX_load_verify_locations(ctx, verifyCert,0);
    if (res != SSL_SUCCESS){
        return FALSE;
    }

    ssl=wolfSSL_new(ctx);
    if (ssl==NULL)
        return FALSE;

    if (TcpConnect(&sockfd, host, port, 0, ssl)){
        wolfSSL_set_fd(ssl, sockfd);
        //wolfSSL_connect(ssl);
        wolfSSL_set_using_nonblock(ssl, 1);
        tcp_set_nonblocking(&sockfd);
        if (NonBlockingSSL_Connect(ssl)==SSL_SUCCESS){
            unsigned char buff[1]={0x02};
            wolfSSL_write(ssl, buff, 1);
/*          // DEBUG CODE
            int i;
            int indx=0;
            for (i=0;i<5000;i+=100){
                int read;
                if ((read=wolfSSL_read(ssl, &answer[indx],32768))>=1){
                    break;
                }
                Sleep(50);
            //SSLClose();
            //return FALSE;
            */
            Sleep(1000);
            return TRUE;
        }
    }

    SSLClose();
    return FALSE;

}



// -------------------------- generates a SSL transfer (send and receive ) ....
// SSLxfer("127.0.0.1",11111,"nuvathings.pem",jsontxt,jsontxt.size())
BOOL SSLtransfer(char* host,uint16_t port,char* verifyCert,char* path,char* txt,int size,char* answer,int *answersize)
{
    // using local variables so not using global ones which are for the CLOUD CONNECTION.
    SOCKET_T sockfd1 = WOLFSSL_SOCKET_INVALID;
    WOLFSSL_METHOD*  method1  = 0;
    WOLFSSL_CTX*     ctx1    = 0;
    WOLFSSL*         ssl1    = 0;

    BOOL success=FALSE;
    *answersize=0;
    //char reply[80];
    int  input;

    sockfd1 = WOLFSSL_SOCKET_INVALID;
    method1  = 0;
    ctx1     = 0;
    ssl1     = 0;

    wolfSSL_Init();
    method1 = wolfTLSv1_2_client_method();
    if (method1==NULL)
        return FALSE;

    ctx1 = wolfSSL_CTX_new(method1);
    if (ctx1==NULL){
        method = NULL;
        return FALSE;
    }
    //wolfSSL_UseMaxFragment(16384);
    //wolfSSL_CTX_UseMaxFragment(16384);

    wolfSSL_CTX_SetMinDhKey_Sz(ctx1, 1024);
    int res=wolfSSL_CTX_load_verify_locations(ctx1, verifyCert,0);
    if (res != SSL_SUCCESS){
        return FALSE;
    }
//    if (ProcessChainBuffer(ctx, pem,sizeof(pem), SSL_FILETYPE_PEM, 4, NULL)!=SSL_SUCCESS)
//        return FALSE;

    ssl1=wolfSSL_new(ctx1);
    if (ssl1==NULL){
        ctx1  = NULL;
        return FALSE;
    }

    if (TcpConnect(&sockfd1, host, port, 0, ssl1)){
        wolfSSL_set_fd(ssl1, sockfd1);
        //wolfSSL_connect(ssl);
        wolfSSL_set_using_nonblock(ssl1, 1);
        tcp_set_nonblocking(&sockfd1);
        if (NonBlockingSSL_Connect(ssl1)==SSL_SUCCESS){
            wolfSSL_write(ssl1, txt, size);
            int i;
            int indx=0;
            for (i=0;i<6000;i+=100){
                int read=0;
                if ((read=wolfSSL_read(ssl1, &answer[indx],8192))>=1){
                    success=TRUE;
                    indx+=read;
                    if (read==8192){
                        // There is going to arrive more information!
                        int jj=0;
                    }else{
                        break;
                    }
                }
                Sleep(100);
            }
            *answersize=indx;
        }
        wolfSSL_shutdown(ssl1);
        closesocket(sockfd1);
        wolfSSL_free(ssl1);
        wolfSSL_CTX_free(ctx1);
        wolfSSL_Cleanup();

        ssl1 =   NULL;
        ctx1 =   NULL;
        sockfd1 = WOLFSSL_SOCKET_INVALID;
    }else{
        return FALSE;
    }

    return success;

}

