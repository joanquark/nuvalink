#include "panelfitxer.h"
#include <wx/filedlg.h>

int idBotoExaminar = wxNewId();

BEGIN_EVENT_TABLE(CPanelFile, wxPanel)
    EVT_BUTTON(idBotoExaminar, CPanelFile::OnButtExaminar)
END_EVENT_TABLE()

CPanelFile::CPanelFile(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CLanguage *lang)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,25);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,25);
    }
    Lang = lang;
    text=0;
    label=0;
    fitxer=0;
    check=0;

    vSizer = new wxBoxSizer( wxVERTICAL );
    hSizer = new wxBoxSizer( wxHORIZONTAL );
    SetSizer( hSizer );

}

void CPanelFile::Clear()
{
    if (text) {
        delete text;
        text=0;
    }

    if (label) {
        delete label;
        label=0;
    }

    if (check) {
        delete check;
        check=0;
    }

    int i=0;
    while (vSizer->Detach(i++));
    i=0;
    while (hSizer->Detach(i++));
}

CPanelFile::~CPanelFile()
{
    Clear();
    fitxer=0;
}

void CPanelFile::GenComponents()
{
    if (fitxer) {
        Clear();

        SetBackgroundColour(wxColour(0xdd,0xdd,0xdd));

        check = new wxCheckBox(this, -1, "");

        label = new wxStaticText(this,-1,wxT(""),wxDefaultPosition,wxSize(1,-1),wxST_NO_AUTORESIZE|wxNO_BORDER|wxALIGN_LEFT);
        //label->SetBackgroundColour(wxColour(0x30,0x30,0x90));
        //label->SetForegroundColour(wxColour(0xff,0xff,0xff));
//        label->SetTitle(wxT("Description"));
        wxString tLabel;
        string index = iToS(fitxer->GetIndex());
        if ( index != "")
            tLabel = " " + _wx(index) + " - " + fitxer->GetDescr();
        else
            tLabel = " " + fitxer->GetDescr();
        label->SetLabel(_wx(tLabel));

        text = new wxTextCtrl(this,-1,wxT(""),wxDefaultPosition,wxSize(1,-1),wxST_NO_AUTORESIZE|wxSUNKEN_BORDER);
        text->SetLabel(_wx(fitxer->GetNomFitxer()));

        botoExaminar = new wxButton(this, idBotoExaminar, Lang->GetAppMiss("MISS_PANEL_BROWSE"));

        hSizer->Add( check, wxSizerFlags(0).Center().Border(wxRIGHT, 5));
        hSizer->Add( label, wxSizerFlags(3).Center());
        hSizer->Add( text, wxSizerFlags(2).Right().Border(wxLEFT, 2));
        hSizer->Add( botoExaminar, wxSizerFlags(1).Right().Border(wxLEFT, 2));
        hSizer->Layout();

        UpdateValues();
    }
}

bool CPanelFile::SetFitxer(CFitxerFlash *fitxer)
{
    this->fitxer = fitxer;
    GenComponents();
    return true;
}

void CPanelFile::UnSetFitxer()
{
    fitxer=0;
    Clear();
}

CFitxerFlash *CPanelFile::GetFile()
{
    return fitxer;
}

//Aplica el valor del camp de text al CFitxerFlash (el nom del fitxer)
bool CPanelFile::ApplyValues()
{
    if (!fitxer)
        return false;

    if (text) {
        string lab = (string)(text->GetLabel());
        if (!fitxer->SetNomFitxer(lab)) return false;
    }

    if (check) {
        fitxer->SetSelec(check->IsChecked());
    }

    return true;
}

//Actualitza els valors del Fitxer al camp de text
//(es el contrari que ApplyValues()
bool CPanelFile::UpdateValues()
{
    if (!fitxer)
        return false;

    if (text) {
        string lab = fitxer->GetNomFitxer();
        text->SetLabel(_wx(lab));
    }

    if (check) {
        check->SetValue(fitxer->IsSelec());
    }
    return true;
}

void CPanelFile::OnButtExaminar(wxCommandEvent& event)
{

    string ext=fitxer->GetFileExt();
    if (ext==""){
        ext="*";
    }

    //Obre finestra de seleccionar fitxers
    wxFileDialog* fileDlg = new wxFileDialog(this, "Flash file", "", "", "*."+_wx(ext), wxOPEN);

    if (fileDlg->ShowModal() == wxID_OK) {
        wxString wxNomFitxer = fileDlg->GetFilename();
        wxString wxNomDirectori = fileDlg->GetDirectory();
        string nomFitxer = wxNomDirectori.c_str();
        nomFitxer += '\\';
        nomFitxer += wxNomFitxer.c_str();
        fitxer->SetNomFitxer(nomFitxer);
        fitxer->SetSelec(true);
        UpdateValues();
    }
}
