#ifndef MESURA_H
#define MESURA_H

#define FORMULA_ZONES_A     1
#define FORMULA_ZONES_B     2
#define FORMULA_ALIM_554    3
#define FORMULA_ALIM_050    4
#define FORMULA_TEMP16      5
#define FORMULA_MBAR        6

#include <iostream>
#include <sstream>
#include <string>
using namespace std;

#include "msgs.h"

class CMesura
{
    int offset;
    wxString descr;
    string unitats;
    int formula;
    int valorMesura;
    float valorFinal;
    string stringVal;
    float vdd;
    float vac;

    public:
        CMesura();
        ~CMesura();

        bool SetOffset(int offset);
        bool SetDescr(wxString &descr);
        bool SetUnits(string &unitats);
        bool SetValue(int valor);
        bool SetValue(unsigned char* valor);
        bool SetFormula(int formula);
        bool SetVDD(float vdd);
        bool SetVAC(float vac);

        bool Calcula();

        int GetOffset();
        wxString& GetDescr();
        string& GetUnitats();
        float GetValor();
        string& GetStringVal();
        int GetFormula();
};

#endif
