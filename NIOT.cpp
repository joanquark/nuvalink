#include "NIOT.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <string.h>
//#define _wx(x) (wxString)((string)x).c_str()

CNIOT::CNIOT()
{
    NIOTFrTx.SeqNum.Val=0;
    ppush=0;
    ns=0;
    NIOTptr=0;
	NIOTPending=0;
	SN="FFFFFFFF";
}

CNIOT::~CNIOT()
{
    Reset();
}

void CNIOT::Reset()
{
    ppush=0;
}

int CNIOT::ProcessNIOTRx(unsigned char* buff,BYTE* subscriber,int cnt)
{
    BYTE* pi;
    TNIOTFr* pNIOT;
    pNIOT=(TNIOTFr*)buff;

	if (pNIOT->Header!=SOH)
		return -1;

    AES crypt;
    crypt.SetParameters(16*8,16*8); // 128 bit key and ciphered data.

    NIOTinflen=pNIOT->Ctrl.Val;

    if (pNIOT->TF&M_NIOT_CIPHER){
        string keys=aeskey;
        char* key=&keys[0];
        crypt.StartDecryption((unsigned char*)key);
        unsigned char iv[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        unsigned char uncipherbuff[1024];
        // cipher length = total lenght -4(soh+flags+appid)
        crypt.Decrypt(&pNIOT->SN.v[0],uncipherbuff,(cnt-SIZE_UNCIPHERED_NIOTHEADER)/16,crypt.CBC,iv);
        memcpy(&pNIOT->SN.v[0],uncipherbuff,cnt-8);
    }
    string besn="";
    besn+=nibble2ascii(pNIOT->SN.v[0]>>4);
    besn+=nibble2ascii(pNIOT->SN.v[0]);
    besn+=nibble2ascii(pNIOT->SN.v[1]>>4);
    besn+=nibble2ascii(pNIOT->SN.v[1]);
    besn+=nibble2ascii(pNIOT->SN.v[2]>>4);
    besn+=nibble2ascii(pNIOT->SN.v[2]);
    besn+=nibble2ascii(pNIOT->SN.v[3]>>4);
    besn+=nibble2ascii(pNIOT->SN.v[3]);

    if (((pNIOT->TF & M_NIOT_TPKT)!=T_NIOT_PKT_ACK) && ((pNIOT->TF & M_NIOT_TPKT)!=T_NIOT_PKT_ENCAPSULATED) && (this->GetSN()=="FFFFFFFF"))
        this->SetSN(besn);

    if (pNIOT->TF&M_NIOT_CRC_ADD){
        unsigned char crc[2];
        CalcCRC(buff, cnt-2, crc);

        if ((crc[0] != buff[cnt-2]) || (crc[1] != buff[cnt-1]))
            return -1;
    }
    ns++;

    memcpy((void*)&NIOTFrRx.Header,(void*)&pNIOT->Header,sizeof(TNIOTFr));
    ppop=0;                     // pop pointer to 0.
    return NIOTinflen;

}

int CNIOT::GetFlags()
{
    return NIOTFrRx.TF;
}

int CNIOT::NeedAck()
{
    return (NIOTFrRx.TF & M_NIOT_NEED_ACK);
}

int CNIOT::GetHops()
{
    return NIOTFrRx.Hops;
}

// the bool indicates if more data has to be received, poplen, indicates about lenght of poped data.
int CNIOT::PopBlock(unsigned char wantedID,unsigned char* popbuff)
{

	int blklen;
	unsigned char *pi;
	ppop=0;
	int NIOTlen=NIOTinflen;
	pi=&NIOTFrRx.Buff[0];

	BYTE tpkt=NIOTFrRx.TF&M_NIOT_TPKT;
	if ((tpkt!=T_NIOT_PKT_COMPOSITE) && ((tpkt!=wantedID) && (wantedID!=0xFF)))
        return 0;
    // ------------------ check Class type NIOT frames -----------------
	switch (NIOTFrRx.TF&M_NIOT_TPKT){
		case T_NIOT_PKT_ACK:
//			NIOTComRxAck(NIOTFrRx.Ctrl.Val,NIOTFrRx.SN.Val);
			return 0;

		case T_NIOT_PKT_FLASH:				// this is standard Flash packet, don't needs ID-LEN fields ( Flash0, with optimization to send 8 bytes fitting in 20 bytes BLE packets
			memcpy(popbuff,pi,NIOTinflen);
			return NIOTinflen;

		case T_NIOT_PKT_RAM:
			memcpy(popbuff,pi,NIOTinflen);
			return NIOTinflen;

		case T_NIOT_PKT_REQFLASH:
			return 0;

		case T_NIOT_PKT_REQRAM:
			return 0;

		case T_NIOT_PKT_STREAM:
            memcpy(popbuff,pi,NIOTinflen);
			return NIOTinflen;
	}


	while (NIOTlen>0/*ppop<NIOTinflen*/){

		blklen=NIOTFrRx.Buff[ppop+1];
		if (!blklen){
			blklen=NIOTlen-2;             // null block is in fact total fr infolen-2!
		}
		unsigned char id=NIOTFrRx.Buff[ppop];

		if ((wantedID==id) || ((wantedID==0xFF))){
            memcpy((void*)popbuff,(void*)&NIOTFrRx.Buff[ppop+2],blklen);
            ppop+=(blklen+2);
            return blklen;
		}
        ppop+=(blklen+2);
        NIOTlen-=(blklen+2);      // +L0D+
    }
    return 0;
}

bool CNIOT::SetSN(string sn)
{
    this->SN=sn;
}

string CNIOT::GetSN()
{
    return this->SN;
}


bool CNIOT::FormatHeader(unsigned char flags,unsigned char hops,unsigned char subtype,string cccccccc)
{
    ppush=0;
    NIOTFrTx.Header=SOH;
    NIOTFrTx.TF=flags;
    NIOTFrTx.SeqNum.Val=ns;
    NIOTFrTx.Flg=M_NIOT_FLG_DOWNLINK;       // packets sent from Nuvalink are always downlink packets.
    NIOTFrTx.Try=0;
    NIOTFrTx.Hops=(hops&0x0F)<<4;           // rhis are requested hops
    NIOTFrTx.subType=subtype;

    if (this->SN!=""){
        cccccccc=this->SN;
    }
    while (cccccccc.length()<8)
        cccccccc = "0" + cccccccc;

    for (int i=0; i<4; i++) {
        NIOTFrTx.SN.v[i] = ascii2nibble(cccccccc[i*2]);
        NIOTFrTx.SN.v[i] <<= 4;
        NIOTFrTx.SN.v[i] |= ascii2nibble(cccccccc[(i*2)+1]);
    }
}

bool CNIOT::AddBlock(unsigned char TID,unsigned char* buff,int len)
{
    int putlen;
    putlen=len;

    if (TID!=0xFF){         // BlockID==0xFF means specialized NIOT packet.
        NIOTFrTx.Buff[ppush++]=TID;
        NIOTFrTx.Buff[ppush++]=(unsigned char)putlen;
    }
    memcpy(&NIOTFrTx.Buff[ppush],buff,len);

    ppush+=len;
}
// -------------------------------------------------------------------
// changing try number, emans for :
// decrypt frame if so
// recalc CRC
// encryp frame again.
int CNIOT::UpdateTry(int tries,int len,unsigned char* raw)
{
    NIOTFrTx.Try=tries;
    int inflen=len-sizeof(TNIOTHeader)-2;

    if (NIOTFrTx.TF&M_NIOT_CIPHER){
        string keys=aeskey;
        char* key=&keys[0];
        AES crypt;
        crypt.StartDecryption((unsigned char*)key);
        unsigned char iv[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        unsigned char uncipherbuff[1024];
        // cipher length = total lenght -4(soh+flags+appid)
        crypt.Decrypt(&NIOTFrTx.SN.v[0],uncipherbuff,(len-SIZE_UNCIPHERED_NIOTHEADER)/16,crypt.CBC,iv);
        memcpy(&NIOTFrTx.SN.v[0],uncipherbuff,(len-SIZE_UNCIPHERED_NIOTHEADER));
    }
    if (NIOTFrTx.TF & M_NIOT_CRC_ADD){
		NIOTFrTx.Buff[inflen] = 0;
		NIOTFrTx.Buff[inflen+1] = 0;
        CalcCRC(&NIOTFrTx.Header, len-2, &NIOTFrTx.Buff[inflen]);
    }
    if (NIOTFrTx.TF & M_NIOT_CIPHER){
        // apply cipher now.
        AES crypt;
        crypt.SetParameters(16*8,16*8); // 128 bit key and ciphered data.
        string keys=aeskey;
        char* key=&keys[0];
        crypt.StartEncryption((unsigned char*)key);
        unsigned char iv[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        unsigned char cipherbuff[1024];
        crypt.Encrypt((unsigned char*)&NIOTFrTx.SN.v[0],cipherbuff,(len-SIZE_CIPHERED_NIOTHEADER)/16,crypt.CBC,iv);     // ppush has CRC bytes in case of
        memcpy((unsigned char*)&NIOTFrTx.SN.v[0],cipherbuff,(len-SIZE_UNCIPHERED_NIOTHEADER));
    }
    if (raw)
        memcpy((unsigned char*)raw,(unsigned char*)&NIOTFrTx.Header,len);
}
int CNIOT::CloseFr(unsigned char *raw)
{
    int cnt;
    int fill=0;

    if (NIOTFrTx.TF&M_NIOT_CIPHER){
		int j=(ppush+SIZE_CIPHERED_NIOTHEADER+2)%16;	// +8 as we encrypt also part of the header except from SN + 2 for CRC part(duty)
		if (j>0){
            fill=16-j;
            for (int cnt=0;cnt<fill;cnt++){		// pad with 0x00
                NIOTFrTx.Buff[ppush+cnt]=cnt;
            }
		}
    }
    NIOTFrTx.Ctrl.Val=ppush&0x0FFF;         // this are "real" info bytes
    if (NIOTFrTx.TF&M_NIOT_CRC_ADD){
		NIOTFrTx.Buff[fill + ppush] = 0;
		NIOTFrTx.Buff[fill + ppush + 1] = 0;
        CalcCRC(&NIOTFrTx.Header, sizeof(TNIOTHeader)+ppush+fill, &NIOTFrTx.Buff[ppush+fill]);
        ppush+=2;
    }
    if (NIOTFrTx.TF & M_NIOT_CIPHER){
        // apply cipher now.
        AES crypt;
        crypt.SetParameters(16*8,16*8); // 128 bit key and ciphered data.

        string keys=aeskey;
        char* key=&keys[0];
        crypt.StartEncryption((unsigned char*)key);
        unsigned char iv[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        unsigned char cipherbuff[1024];

        crypt.Encrypt((unsigned char*)&NIOTFrTx.SN.v[0],cipherbuff,(ppush+SIZE_CIPHERED_NIOTHEADER)/16,crypt.CBC,iv);     // ppush has CRC bytes in case of
        memcpy((unsigned char*)&NIOTFrTx.SN.v[0],cipherbuff,ppush+SIZE_CIPHERED_NIOTHEADER);
    }

    frtxlen=sizeof(TNIOTHeader)+ppush+fill;     // ppush already contains +2 crc
    if (raw)
        memcpy((unsigned char*)raw,(unsigned char*)&NIOTFrTx.Header,frtxlen);
    return frtxlen;
}

bool CNIOT::SetAesKey(string key)
{
    aeskey=key;
}

unsigned char* CNIOT::GetFrameBuff()
{
    return &NIOTFrTx.Header;
}


unsigned char* CNIOT::GetAckBuff()
{
    return &NIOTFrAck.Header;
}

int CNIOT::NIOTAck(BYTE CHACK)
{
    memcpy((void*)&NIOTFrAck.Header,(void*)&NIOTFrRx.Header,sizeof(TNIOTFr));

	NIOTFrAck.TF=T_NIOT_PKT_ACK;
	if (CHACK==0x06)
        NIOTFrAck.TF|=0x01;
	NIOTFrAck.Ctrl.Val=0;         // no info back in ack
	if (NIOTFrAck.TF&M_NIOT_CRC_ADD){
        CalcCRC(&NIOTFrAck.Header, sizeof(TNIOTHeader), &NIOTFrAck.Buff[0]);
	}
	return  sizeof(TNIOTHeader)+2;
}

unsigned char CNIOT::GetTNIOTFr(unsigned char* fr)
{
    return (((TNIOTFr*)fr)->TF & M_NIOT_TPKT);
}

unsigned char CNIOT::GetTNIOTAck(unsigned char* fr)
{
    return (((TNIOTFr*)fr)->TF & M_NIOT_TRUEACK);
}

WORD CNIOT::GetLenNIOTFr(unsigned char* fr)
{
    WORD res=(((TNIOTFr*)fr)->Ctrl.Val);
    return res;
}

bool CNIOT::IsBroadCast(unsigned char*fr)
{
    return (((TNIOTFr*)fr)->SN.Val==0xFFFFFFFF);
}
