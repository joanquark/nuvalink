#include "TreeCtrl.h"

CTreeCtrl::CTreeCtrl()
{
    Tree=0;
}

CTreeCtrl::~CTreeCtrl()
{
    Clear();
}

void CTreeCtrl::Clear()
{
    for (int i=0; i<table.size(); i++)
        delete table[i];

    table.clear();
    vdades.clear();
    vgrups.clear();
    vmodels.clear();
    vevents.clear();
    vseccions.clear();
    vflashgrups.clear();
    vestats.clear();
}

bool CTreeCtrl::SetRoot(wxTreeItemId &id)
{
    if (SearchIndx(id) != -1) return false;

    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_BASE;
    rela->index=0;
    table.push_back(rela);

    return true;
}

wxTreeItemId CTreeCtrl::GetRoot()
{
    for (int i=0; i<table.size(); i++) {
        struct rel *rela = table[i];
        if (rela->tipus==TREE_TIPUS_BASE)
            return rela->treeId;
    }

//    return -1;
}

//Afegeix element
bool CTreeCtrl::Add(wxTreeItemId &id, CModel *model)
{
    if (SearchIndx(id) != -1) return false;

    vmodels.push_back(model);
    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_CMODEL;
    rela->index=vmodels.size()-1;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::Add(wxTreeItemId &id, CData *dades)
{
    if (SearchIndx(id) != -1) return false;

    vdades.push_back(dades);
    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_CDATA;
    rela->index=vdades.size()-1;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::Add(wxTreeItemId &id, CGroup *grup)
{
    if (SearchIndx(id) != -1) return false;

    vgrups.push_back(grup);
    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_CGROUP;
    rela->index=vgrups.size()-1;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::Add(wxTreeItemId &id, CGroupFlash *grupFlash)
{
    if (SearchIndx(id) != -1) return false;

    vflashgrups.push_back(grupFlash);
    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_CFLASHGROUP;
    rela->index=vflashgrups.size()-1;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::Add(wxTreeItemId &id, CStatus *estat)
{
    if (SearchIndx(id) != -1) return false;

    vestats.push_back(estat);
    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_CSTATUS;
    rela->index=vestats.size()-1;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::Add(wxTreeItemId &id, int idSeccio)
{
    if (SearchIndx(id) != -1) return false;

    vseccions.push_back(idSeccio);
    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_STSECTION;
    rela->index=vseccions.size()-1;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::Add(wxTreeItemId &id, CEventList *llista)
{
    if (SearchIndx(id) != -1) return false;

    vevents.push_back(llista);
    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_EVENTS;
    rela->index=vevents.size()-1;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::AddVerify(wxTreeItemId &id)
{
    if (SearchIndx(id) != -1) return false;

    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_VERIFY;
    table.push_back(rela);

    return true;
}


bool CTreeCtrl::AddVModem(wxTreeItemId &id)
{
    if (SearchIndx(id) != -1) return false;

    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_VMODEM;
    table.push_back(rela);

    return true;
}

bool CTreeCtrl::AddVCOM(wxTreeItemId &id)
{
    if (SearchIndx(id) != -1) return false;

    struct rel *rela = new struct rel;
    rela->treeId=id;
    rela->tipus=TREE_TIPUS_VCOM;
    table.push_back(rela);

    return true;
}


int CTreeCtrl::SearchIndx(wxTreeItemId &id)
{
    for (int i=0; i<table.size(); i++) {
        struct rel *rela = table[i];
        if (rela->treeId == id) {
            return i;
        }
    }

    return -1;
}

int CTreeCtrl::GetTypeCon(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    rela = table[aa];
    return rela->tipus;
}

//Funcio recursiva que busca la branca pare que �s model de central
//Quan estigui seleccionat un grup podrem saber a quina central correspon
//Si no troba resultat retorna -1
int CTreeCtrl::SearchParentModel(int indexFill)
{
    struct rel *rela;
    int index=-1;

    if ((indexFill <0) || (indexFill >= table.size()))
        return -1;

    rela = table[indexFill];
    if (rela->tipus == TREE_TIPUS_CMODEL) {
        index = indexFill;
    }
    else if ((rela->tipus == TREE_TIPUS_CGROUP) || (rela->tipus == TREE_TIPUS_CSTATUS)
        || (rela->tipus == TREE_TIPUS_STSECTION) || (rela->tipus == TREE_TIPUS_EVENTS) || (rela->tipus == TREE_TIPUS_VERIFY)
        || (rela->tipus == TREE_TIPUS_CFLASHGROUP)) {
        if (Tree) {
            wxTreeItemId id = Tree->GetItemParent(rela->treeId);
            int aa = SearchIndx(id);
            index = SearchParentModel(aa);
        }
    }

    return index;
}

//Funcio recursiva que busca la branca pare que �s estat
//Quan estigui seleccionada una seccio podrem saber a quin CStatus
//Si no troba resultat retorna -1
int CTreeCtrl::BuscaEstatPare(int indexFill)
{
    struct rel *rela;
    int index=-1;

    if ((indexFill <0) || (indexFill >= table.size()))
        return -1;

    rela = table[indexFill];
    if (rela->tipus == TREE_TIPUS_CSTATUS) {
        index = indexFill;
    }
    else if (rela->tipus == TREE_TIPUS_STSECTION) {
        if (Tree) {
            wxTreeItemId id = Tree->GetItemParent(rela->treeId);
            int aa = SearchIndx(id);
            index = BuscaEstatPare(aa);
        }
    }

    return index;
}

CData *CTreeCtrl::GetData(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vdades[rela->index]);
}

CGroup *CTreeCtrl::GetGroup(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vgrups[rela->index]);
}

//niroblock code
CGroup *CTreeCtrl::GetGroupByIndex(int aa)
{
    struct rel *rela;

    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vgrups[rela->index]);
}

CGroupFlash *CTreeCtrl::GetGroupFlash(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vflashgrups[rela->index]);
}

CModel *CTreeCtrl::GetModel(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    aa = SearchParentModel(aa);
    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vmodels[rela->index]);
}

CStatus *CTreeCtrl::GetStatus(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    aa = BuscaEstatPare(aa);
    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vestats[rela->index]);
}

int CTreeCtrl::GetSection(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vseccions[rela->index]);
}

CEventList *CTreeCtrl::GetEventList(wxTreeItemId &id)
{
    struct rel *rela;
    int aa;

    aa = SearchIndx(id);
    if (aa==-1)
        return 0;

    rela = table[aa];

    return (vevents[rela->index]);
}
