#include "mesura.h"
#include "taulatemp.h"
#include "util.h"
#define _wx(x) (wxString)((string)x).c_str()


CMesura::CMesura()
{
    formula=-1;
    vac = -1.0;
    vdd = -1.0;
    valorFinal = 0.0;
    valorMesura = 0;
    offset=0;
}

CMesura::~CMesura()
{
    //res
}

bool CMesura::SetOffset(int offset)
{
    if (offset <0)
        return false;

    this->offset = offset;
    return true;
}


bool CMesura::SetDescr(wxString &descr)
{
    this->descr = descr;
    return true;
}

bool CMesura::SetUnits(string &unitats)
{
    this->unitats = unitats;
    return true;
}

bool CMesura::SetValue(int valor)
{
    valorMesura = valor;
    return true;
}

bool CMesura::SetValue(unsigned char* valor)
{
    INT16_VAL   val;
    val.byte.LB=valor[0];
    val.byte.HB=valor[1];
    valorMesura = val.Val;
    return true;
}

bool CMesura::SetFormula(int formula)
{
    if ((formula == FORMULA_ZONES_A) ||
        (formula == FORMULA_ZONES_B) ||
        (formula == FORMULA_ALIM_050) ||
        (formula == FORMULA_ALIM_554) ||
        (formula == FORMULA_MBAR) ||
        (formula == FORMULA_TEMP16) ) {
        this->formula = formula;
        return true;
    }
    else {
        this->formula = -1;
        return false;
    }
}

bool CMesura::SetVDD(float vdd)
{
    this->vdd = vdd;
    return true;
}

bool CMesura::SetVAC(float vac)
{
    this->vac = vac;
    return true;
}

bool CMesura::Calcula()
{
    valorFinal = -1.0;

//    wxMessageBox("calcul");


    float LSB_VDD = vdd / 4096.0;
    float vZona;

    switch (formula) {
        case FORMULA_ZONES_A:
            vZona = (float)valorMesura * LSB_VDD;
            if (vdd - vZona) valorFinal = (vZona * 2200) / (vdd - vZona);
            else valorFinal = 999;
            break;
        case FORMULA_ALIM_050:
            valorFinal = (float)valorMesura * LSB_VDD * 0.5;     // Centrum PANELS
            vac = valorFinal;
            break;
        case FORMULA_ALIM_554:
            valorFinal = (float)valorMesura * LSB_VDD * 5.54;       // 4G panels and LT
            vac = valorFinal;
            break;
         case FORMULA_TEMP16:
            valorFinal=valorMesura*0.1;
            break;
         case FORMULA_MBAR:
            valorFinal=valorMesura;
            break;
        default:
            return false;
    }

    return true;
}

int CMesura::GetOffset()
{
    return offset;
}

wxString& CMesura::GetDescr()
{
    return descr;
}

string& CMesura::GetUnitats()
{
    return unitats;
}

float CMesura::GetValor()
{
    return valorFinal;
}

int CMesura::GetFormula()
{
    return formula;
}

string& CMesura::GetStringVal()
{
        char buffer[16];

        sprintf(buffer, "%.2f", valorFinal);
        stringVal = buffer;
        stringVal += " " + unitats;

    return stringVal;
}
