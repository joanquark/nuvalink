#ifndef KPADDRESS_H
#define KPADDRESS_H

// Redefinicio de taules de programaciķ.
   #define  MASK_MODE           0x00
   #define  DECIMAL_MODE        0x01
   #define  DECIMAL16_MODE      0x02
   #define  NIBBLE_MODE2        0x03
      // 1 byte, dos digits
   #define  NIBBLE_MODE4        0x04
      // 2 bytes, 4 digits
   #define  NIBBLE_MODE6        0x05
      // 3 bytes, 6 digits
   #define  NIBBLE_MODE8        0x06
   #define  NIBBLE_MODE28       0x07
      // 14 byte, 28 digits
   #define  HORAQUART_MODE      0x08
      // 1 byte, use TQuartHoraToTime - obte 2 bytes que cal convertir despres a decimal
   #define  RESEND_MODE         0x09
      // Es tracta del mode per programar reenvios de CRA-IP, adreces 0x0800...0x0FFF
   #define  ALIAS_MODE          0x0A           // no deixa de ser un nibble mode de 32 digits = 16 ascii
   #define  STRING_MODE         0x0B
   #define  STRING64_MODE       0x0C
   #define  STRING24_MODE       0x0D
   #define  STRING32_MODE       0x0E
   #define  IPV4_MODE           0x0F

//   #define  RES_KP_MODE2            0x78
// --------------------------------------------------- Māscares de les taules de teclat ---------
//   #define  M_ADDPROG_MODE          0x78
//   #define  M_ADDPROG_HEEPADD       0x07
//   #define  M_OFFSET_MODE           0x80
//   #define  M_RAM_MODE              0x80


// --------------------------------------------------------- RESEND ADDRESSES (CVGSM-CRA) ---
#define ADD_SUBSCRIBER_CFG_EEP     0x0800
   #define HADD_SUBSCRIBER_CFG_EEP    0x08
   #define LADD_SUBSCRIBER_CFG_EEP    0x00

#endif
