#ifndef FLASHCHECKSUMDLG_H
#define FLASHCHECKSUMDLG_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(FlashChecksumDlg)
	#include <wx/dialog.h>
	#include <wx/listctrl.h>
	#include <wx/sizer.h>
	//*)
#endif
//(*Headers(FlashChecksumDlg)
#include <wxSpeedButton.h>
//*)
#define _wx(x) (wxString)((string)x).c_str()
class FlashChecksumDlg: public wxDialog
{
	public:

		FlashChecksumDlg(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~FlashChecksumDlg();
        void ShowData(uint32_t add,uint16_t* source, uint16_t* dest,int num);
		//(*Declarations(FlashChecksumDlg)
		wxListCtrl* ListCtrl1;
		wxSpeedButton* SpeedButton1;
		wxSpeedButton* SpeedButton2;
		//*)

	protected:

		//(*Identifiers(FlashChecksumDlg)
		static const long ID_LISTCTRL1;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		//*)

	private:

		//(*Handlers(FlashChecksumDlg)
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnSpeedButton2LeftClick(wxCommandEvent& event);
		//*)

	protected:

		void BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size);

		DECLARE_EVENT_TABLE()
};

#endif
