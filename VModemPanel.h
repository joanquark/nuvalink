#ifndef VMODEMPANEL_H
#define VMODEMPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(VModemPanel)
	#include <wx/checkbox.h>
	#include <wx/choice.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	#include <wx/timer.h>
	//*)
#endif
//(*Headers(VModemPanel)
#include <wx/lcdwindow.h>
#include <wxSpeedButton.h>
//*)

#include "NIOT.h"
#include "protocolNIOT.h"
#include "config.h"
#include "serial/Serial.h"

class VModemPanel: public wxPanel
{
	public:

		VModemPanel(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~VModemPanel();

		void SetParams(CProtocolNIOT* protocolniot, CConfig* conf);
        void Start();
		//(*Declarations(VModemPanel)
		wxCheckBox* CheckLog;
		wxChoice* ChoiceCOM;
		wxChoice* ChoiceVModem;
		wxLCDWindow* LcdTime;
		wxSpeedButton* StartBt;
		wxSpeedButton* StopBt;
		wxStaticText* CurSt;
		wxStaticText* StaticText1;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxTextCtrl* TxtLog;
		wxTimer TimerCtrl;
		//*)

	protected:

		//(*Identifiers(VModemPanel)
		static const long ID_STATICTEXT4;
		static const long ID_CHOICE2;
		static const long ID_STATICTEXT3;
		static const long ID_CHOICE1;
		static const long ID_CHECKBOX1;
		static const long ID_TEXTCTRL2;
		static const long ID_STATICTEXT1;
		static const long ID_LCDWINDOW1;
		static const long ID_STATICTEXT2;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		static const long ID_TIMER1;
		//*)

	private:

		//(*Handlers(VModemPanel)
		void OnTimerCtrlTrigger(wxTimerEvent& event);
		void OnStartBtLeftClick(wxCommandEvent& event);
		void OnStopBtLeftClick(wxCommandEvent& event);
		void OnChoiceCOMSelect(wxCommandEvent& event);
		void OnChoiceVModemSelect(wxCommandEvent& event);
		void OnCheckLogClick(wxCommandEvent& event);
		//*)
        void SocketSModemEvent(wxSocketEvent &event);
        bool HayesDecode(char* Buff,int CntBRx);
        void HayesSendOk();
        void HayesSendNoCarrier();
        void HayesSendConnect();
        void HayesSendRing();
        void ShowRawFrame(unsigned char* fr, int len,bool rx);
        void ProcessFrameFromPC(unsigned char* buff, int len);
        wxSocketBase   *psocket;
        wxSocketServer *sockets;
        CSerial         serial;
        string          hayescmd;
        string          cmd;
        int             vmodemCOM;
        bool            enabled;
        bool            hconnected;
        bool            vconnected;
        bool            sconnected;
        bool            vtransparent;
        bool            verbose;
        bool            timeouthayes;
        bool            napcoquickloader;
        bool            iscallback;
        int             phayesrx;
        char            VModem;
		CProtocolNIOT*  protocolNIOT;
		CConfig*        config;
		int             eLapsed_t;
		int             cnt25ms;
	protected:

		void BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size);

		DECLARE_EVENT_TABLE()
};

#endif

