#ifndef MEMPANEL_H
#define MEMPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(MemPanel)
	#include <wx/choice.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	//*)
#endif
//(*Headers(MemPanel)
#include <wxSpeedButton.h>
//*)

#include "languages/CLanguage.h"
class Cnuvalink;

class MemPanel: public wxPanel
{
	public:

		MemPanel(wxWindow* parent);
		virtual ~MemPanel();

		//(*Declarations(MemPanel)
		wxChoice* choiceType;
		wxSpeedButton* BtDownload;
		wxSpeedButton* BtUpload;
		wxStaticText* StaticText1;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxTextCtrl* TxtData;
		wxTextCtrl* TxtLen;
		wxTextCtrl* TxtMemAdd;
		//*)
        void SetNuvaLink(Cnuvalink* nuvalink,CLanguage* lang);
        void ShowRawData(unsigned char* raw,int len,bool rx);
	protected:

		//(*Identifiers(MemPanel)
		static const long ID_STATICTEXT1;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT2;
		static const long ID_CHOICE1;
		static const long ID_STATICTEXT3;
		static const long ID_TEXTCTRL3;
		static const long ID_TEXTCTRL2;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		//*)

	private:
        Cnuvalink* nuvalink;
		//(*Handlers(MemPanel)
		void OnBtDownloadLeftClick(wxCommandEvent& event);
		void OnBtUploadLeftClick(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
