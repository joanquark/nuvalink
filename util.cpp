#include "util.h"
#include "crypto/AES_iv.h"
#include <wx/filename.h>

#define _wx(x) (wxString)((string)x).c_str()
extern void CipherChain(unsigned char* PChar, unsigned char* KeyVector,unsigned int num);
extern void UnCipherChain(unsigned char* PChar,unsigned char* KeyVector,unsigned int num);

//D'una cadena attributes="id='250' model='DomoVox' versio='510'" extreu
//els valor de la variable nom. Per exemple, si nom="model",
//retornara "DomoVox"
string& getAtrVal(string& attributes, const string& nom)
{
    string tmp, tmp2;
    int ini, fi;
    char cb;
    string *result = new string;

    tmp = " " + nom + "=";
    tmp2 = " " + attributes;
    ini = tmp2.find(tmp);
    if (ini == string::npos) {
        *result = "";
        return *result;
    }
    ini += tmp.length();

    if (tmp2[ini] == '\"' || tmp2[ini] == '\'') {
        cb = tmp2[ini];
        ini++;
    }
    else {
        cb = ' ';
    }

    fi = tmp2.size()-1;
    for (int i=ini; i<tmp2.size(); i++) {
        if (tmp2[i] == cb) {
            fi=i-1;
            break;
        }
    }

    //tmp = tmp2.substr(ini);
    //fi = tmp.find(cb);
    //fi--;
    *result = tmp2.substr(ini, fi-ini+1);
    return *result;
}

//Torna true si es tracta d'un string que conte numeros
//i lletres entre A i F
bool isHexString(string txt)
{
    for (int i=0; i<txt.size(); i++) {
        if ( !(txt[i]>='0' && txt[i]<='9') &&
        !(txt[i]>='a' && txt[i]<='f') &&
        !(txt[i]>='A' && txt[i]<='F') )
            return false;
    }
    return true;
}

//Torna true si es tracta d'un string que nom�s conte numeros
bool isNumberString(string txt,bool flt)
{
    for (int i=0; i<txt.size(); i++) {
        if ( !(txt[i]>='0' && txt[i]<='9') ){
            if (flt==true && (txt[i]=='.' || txt[i]=='-'))
                int j=0;
            else
                return false;
        }
    }
    return true;
}

//Transforma un nibble en un ascii. El nibble sempre
//l'espera a la part baixa (0x0F)
unsigned char nibble2ascii(unsigned char chNibble)
{
    //Ens assegurem que s'apliqui la mascara
    chNibble = chNibble&0x0F;
    if (chNibble<10) {
        return (chNibble+'0');
    }
    else {
        return (chNibble-10+'A');
    }
}

//Transforma un ascii en un nibble. El nibble sempre
//el retorna a la part baixa (0x0F).
//Si l'ascii no es pot transformar es torna el valor 0
unsigned char ascii2nibble(unsigned char chAscii)
{
    if ((chAscii >= '0') && (chAscii <= '9')) {
        return (chAscii - '0');
    }
    else if ((chAscii >= 'a') && (chAscii <= 'f')) {
        return (chAscii - 'a' + 10);
    }
    else if ((chAscii >= 'A') && (chAscii <= 'F')) {
        return (chAscii - 'A' + 10);
    }
    else {
        //Es tracta d'un ascii inconvertible a nibble,
        //per tant, li tornem un 0 i ja s'ho fara
        return 0;
    }
}

bool IsHexValue(unsigned char chAscii){
    if ((chAscii >= '0') && (chAscii <= '9'))
        return true;
    if ((chAscii >= 'a') && (chAscii <= 'f'))
        return true;
    if ((chAscii >= 'A') && (chAscii <= 'F'))
        return true;

    return false;

}

int sToI(string& txt)
{
    if (txt=="")
        return 0;

    int b;
    std::stringstream ss(txt);
    ss >> b;
    return b;
}


string iToS(long int num)
{
    char buf[32];
    sprintf(buf, "%d", num);
    string str = buf;
    return str;
}


string iToS(int num)
{
    char buf[32];
    sprintf(buf, "%d", num);
    string str = buf;
    return str;
}

string iToS(unsigned int num)
{
    char buf[32];
    sprintf(buf, "%u", num);
    string str = buf;
    return str;
}

int hToI(string& hex)
{
    int result=0;

    for (int i=hex.length()-1, j=0; i>=0; i--, j++) {
        if (IsHexValue(hex[i])){
            int dig = ascii2nibble(hex[i]);     // if it is not hex value, returns 0
            for (int k=0; k<j; k++)
                dig = dig*16;
            result += dig;
        }else{
            //wxMessageBox(_wx(&hex[i]),"no hex");
        }
    }

    return result;
}

string iToH(unsigned int num)
{
    char buf[32];
    sprintf(buf, "%X", num);
    string str = buf;
    return str;
}

string iToH(unsigned int num,int len)
{
    char buf[32];
    if (len==2)
        sprintf(buf, "%02hhX", num);
    else if (len==3)
        sprintf(buf, "%03hhX", num);
    else if (len==4)
        sprintf(buf, "%04hhX", num);
    else if (len==8)
        sprintf(buf, "%08hhX", num);

    string str = buf;
    return str;
}


char SetMinorCase(char ch){

	if ((ch>='A') && (ch<='Z')){       // Conversi� de car�cters a min�scules...
	   ch+=0x20;
   }
	return ch;
}
// ----------------------------------------------------------- isdigit ---------------
BOOL isdigitchar(char digit){
	if ((digit>='0') && (digit<='9'))
        return TRUE;

    return FALSE;
}
// ----------------------------------------------------------- ishex ------------------
BOOL ishexchar(char digit){
    digit=SetMinorCase(digit);
   if ((digit>='0') && (digit<='9')){
      return TRUE;
   }
   if ((digit>='a') && (digit<='f')){
      return TRUE;
   }
   return FALSE;
}


// ----------------------------------------------------------- isletter ----------------
BOOL isletterchar(char letter){
	letter=SetMinorCase(letter);
	return ((letter>='a') && (letter<='z'));
}

// ---------------------------------------------------------- isprnchar --------------
// Busca car�cters imprimibles, no de control.
BOOL isprnchar(char ch){
	return (ch>=' ');
}

int StrCmp(char *PChar,char *S){

	BYTE n=0;

   while ((isprnchar(*PChar)) && (isprnchar(*S)) ){
      if (*S++ != *PChar++){
         // Difer�ncia, en funci� de mode retornem NULL
      	return 0x00;
      }
      n++;
   }
   // Arriba a un caracter no imprimible, el StrCmp retorna el n� de coincidencies, si son cadenes null, retorna NUL.
   return n;
}
unsigned char Get_Mask(unsigned char ind)
{
    unsigned char c=0x01;
    return (c<<ind);
}
/*int sToI(string& txt)
{
    int b;
    std::stringstream ss(txt);
    ss >> b;
    return b;
}
*/
unsigned char TextEditAsciiToLcd(unsigned char winchar){
    return winchar;

    // El text edit ens retorna ISO8859-1 hem de convertilos correctament per LCD!

/*	if (winchar==255 || winchar==0){
		return 0x20;				// if not defined or EOS, return blank spaces.
 	}else if (winchar==224 || winchar==192){    // 133 �,� (0224,192)  +K11+
      return 133;
   }else if (winchar==186){    // 178 � (0186)
      return 178;
   }else if (winchar==226){    // 131 � (0226)
      return 131;
   }else if (winchar==231){    // 135 � (0231)
      return 135;
   }else if (winchar==233){    // 130 � (0233)
      return 130;
   }else if (winchar==232 || winchar==200 || winchar==212){    // 138 �,� (0232,0200) +K11+
      return 138;
   }else if (winchar==234 || winchar==202){    // 136 �,� (0234,0202) +K11+
      return 136;
   }else if (winchar==235 || winchar==203){    // 137 �,� (0235,0203) +K11+
      return 137;
   }else if (winchar==238 || winchar==206){    // 140 �,� (0238,0206) +K11+
      return 140;
   }else if (winchar==239 || winchar==207){    // 139 �,� (0239,0207) +K11+
      return 139;
   }else if (winchar==244){    // 147 � (0244)
      return 147;
   }else if (winchar==249 || winchar==217){    // 151 � (0249)
      return 151;
   }else if (winchar==251){    // 150 � (0251)
      return 150;
//	}else if (winchar==192){    // 183 � (0192)
//      return 183;
   }else if (winchar==194){    // 182 � (0194)
      return 143;//182;
   }else if (winchar==199){    // 128 � (0199)
      return 128;
//   }else if (winchar==200){    // 212 � (0200)
//      return 212;
   }else if (winchar==201){    // 144 � (0201)
      return 144;
//   }else if (winchar==202){    // 210 � (0202)
//      return 136; //->� 210;
//   }else if (winchar==203){    // 211 � (0203)
//      return 211;
//   }else if (winchar==206){    // 215 � (0206)
//      return 215;
//   }else if (winchar==207){    // 216 � (0207)
//      return 216;
   }else if (winchar==225){    // 160 � (0225)
      return 160;
   }else if (winchar==237){    // 161 � (0237)
      return 161;
   }else if (winchar==241){    // 164 � (0241) +K11+
      return 155;
   }else if (winchar==243){    // 162 � (0243)
      return 162;
   }else if (winchar==250){    // 163 � (0250)
      return 163;
   }else if (winchar==252 || winchar==220){    // 129 �,� (0252)  +K11+
      return 154;//129;
   }else if (winchar==209){    // 165 � (0209)	 +K11+
      return 156;
//   }else if (winchar==220){    // 154 � (0220)
//      return 154;
//   }else if (winchar==212){    // 226 � (0212)
//      return 226;
//   }else if (winchar==217){    // 235 + (0217)
//      return 235;
//   }else if (winchar==219){    // 234 � (0219)
//      return 234;
//   }else if (winchar==171){    // 174 � (0171)
//      return 174;
//   }else if (winchar==187){    // 175 � (0187)
//      return 175;
   }else if (winchar==236){    // 141 � (0236)
      return 141;
   }else if (winchar==242){    // 149 � (0242)
      return 149;
//   }else if (winchar==249){    // 151 � (0249)
//      return 151;
   }else if (winchar==227){    // 171 � (0227)
      return 181;//171;
//   }else if (winchar==195){    // 170 � (0195)
//      return 170;
   }else if (winchar==228){    // 132 � (0228)
      return 132;
   }else if (winchar==246){    // 148 � (0246)
      return 148;
   }else if (winchar==196){    // 142 � (0196)
      return 142;
   }else if (winchar==214){    // 153 � (0214)
      return 153;
//   }else if (winchar==216){    // 157 � (0216)
//      return 157;
//   }else if (winchar==229){    // 134 � (0229)
//      return 134;
//   }else if (winchar==248){    // 155 � (0248)
//      return 155;
//   }else if (winchar==197){    // 143 � (0197)
//      return 143;
   }else if (winchar==204){    // 141 � (0204)
      return 141;
   }else if (winchar==210){    // 149 � (0210)
      return 149;
//   }else if (winchar==217){    // 151 � (0217)
//      return 151;
   }else if (winchar==211){    // 162 � (0211)
      return 162;
//   }else if (winchar==245){    // 173 � (0245)
//      return 173;
//   }else if (winchar==213){    // 172 � (0213)
//      return 172;
   }else if (winchar==193){    //  160 � (0193)
      return 160;
   }else if (winchar==205){    //  161 � (0205)
      return 161;
   }else if (winchar==218){    //  163 � (0218)
      return 163;
   }else if (winchar==203){    //  137 � (0203)
      return 137;
   }else if (winchar==223){    //  225 � (0223)
      return 224;//225;
   }else if (winchar==198){    //  146 � (0198)
      return 146;
   }
   return winchar;
*/
}

char LcdtoLatin1(unsigned char ch)
{

    return ch;

/*   if (ch==133){    // 133 � (0224)
      return 224;
   }else if (ch==178){    // 178 � (0186)
      return 186;
   }else if (ch==131){    // 131 � (0226)
      return 226;
   }else if (ch==135){    // 135 � (0231)
      return 231;
   }else if (ch==130){    // 130 � (0233)
      return 233;
   }else if (ch==138){    // 138 � (0232)
      return 232;
   }else if (ch==136){    // 136 � (0234)
      return 234;
   }else if (ch==137){    // 137 � (0235)
      return 235;
   }else if (ch==140){    // 140 � (0238)
      return 238;
   }else if (ch==139){    // 139 � (0239)
      return 239;
   }else if (ch==147){    // 147 � (0244)
      return 244;
   }else if (ch==151){    // 151 � (0249)
      return 249;
   }else if (ch==150){    // 150 � (0251)
      return 251;
   }else if (ch==183){    // 183 � (0192)
      return 192;
   }else if (ch==182){    // 182 � (0194)
      return 194;
   }else if (ch==128){    // 128 � (0199)
      return 199;
   }else if (ch==212){    // 212 � (0200)
      return 200;
   }else if (ch==144){    // 144 � (0201)
      return 201;
   }else if (ch==210){    // 210 � (0202)
      return 202;
   }else if (ch==211){    // 211 � (0203)
      return 203;
   }else if (ch==215){    // 215 � (0206)
      return 206;
   }else if (ch==216){    // 216 � (0207)
      return 207;
   }else if (ch==160){    // 160 � (0225)
      return 225;
   }else if (ch==161){    // 161 � (0237)
      return 237;
   }else if (ch==155){    // 164 � (0241)
      return 241;
   }else if (ch==162){    // 162 � (0243)
      return 243;
   }else if (ch==163){    // 163 � (0250)
      return 250;
   }else if (ch==154){    // 129 � (0252)
      return 252;
   }else if (ch==156){    // 165 � (0209)
      return 209;
   }else if (ch==154){    // 154 � (0220)
      return 220;
   }else if (ch==226){    // 226 � (0212)
      return 212;
   }else if (ch==235){    // 235 + (0217)
      return 217;
   }else if (ch==234){    // 234 � (0219)
      return 219;
   }else if (ch==174){    // 174 � (0171)
      return 171;
   }else if (ch==175){    // 175 � (0187)
      return 187;
   }else if (ch==141){    // 141 � (0236)
      return 236;
   }else if (ch==149){    // 149 � (0242)
      return 242;
   }else if (ch==151){    // 151 � (0249)
      return 249;
   }else if (ch==171){    // 171 � (0227)
      return 227;
   }else if (ch==170){    // 170 � (0195)
      return 195;
   }else if (ch==132){    // 132 � (0228)
      return 228;
   }else if (ch==148){    // 148 � (0246)
      return 246;
   }else if (ch==142){    // 142 � (0196)
      return 196;
   }else if (ch==153){    // 153 � (0214)
      return 214;
   }else if (ch==157){    // 157 � (0216)
      return 216;
   }else if (ch==134){    // 134 � (0229)
      return 229;
   }else if (ch==155){    // 155 � (0248)
      return 248;
   }else if (ch==143){    // 143 � (0197)
      return 197;
   }else if (ch==151){    // 151 � (0217)
      return 217;
   }else if (ch==162){    // 162 � (0211)
      return 211;
   }else if (ch==173){    // 173 � (0245)
      return 245;
   }else if (ch==172){    // 172 � (0213)
      return 213;
   }else if (ch==160){    //  160 � (0193)
      return 193;
   }else if (ch==161){    //  161 � (0205)
      return 205;
   }else if (ch==163){    //  163 � (0218)
      return 218;
   }else if (ch==137){    //  137 � (0203)
      return 203;
   }else if (ch==225){    //  225 � (0223)
      return 223;
   }else if (ch==146){    //  146 � (0198)
      return 198;
   }else if (ch >= 0xF0){
        return ' ';
   }
   return ch;*/
}


int HexChainToAscii(unsigned char* ChainHex, char* ChainAscii,int num){
    int j=0;
    unsigned char k;

    while (num--){
        if (!(j&0x01)){
            k=SwapA(*ChainHex) & 0x0F;
        }else{
            k=*ChainHex & 0x0F;
            ChainHex++;
        }
//        if (k<=0x09){
            *ChainAscii++=nibble2ascii(k);
            j++;
//        }else{
//            break;
//        }
    }
    *ChainAscii=0;
    return j;
}


// ------------------------------------------------------------ AsciiChainToHex ----------
// Desde on apunta PChar en format Ascii, converteix els valors a una cadena destinaci� en format Hex, considera tan sols els valors num�rics, els altres s�n ignorats.
// PRE: Si Nascii=0, transfer mentre chars entre 0..9, else transferir� com a molt els Nascii
// POST : Retorna # Asciis convertits.

int AsciiChainToHex (char* ChainAscii, unsigned char* ChainHex,int num){

    int m=0;
    unsigned char n;
    char ch;

    while (ch=*ChainAscii++){
        n=ascii2nibble(ch);
        if (m&0x01){
            *ChainHex|=n;
            ChainHex++;
        }else{
            *ChainHex=SwapA(n);
        }
        m++;
        if (num){
            if (!--num){
                break;
            }
        }
    }
    return m;
}


void Push_ChainNibble(unsigned char* pch,int pos,unsigned char ch)
{
    if (pos&0x01){
        *pch|=ch;
    }else{
        *pch=(ch<<4);
    }
}


unsigned char SwapA (unsigned char A){
    unsigned char b;
    b=A>>4;
    b|=A<<4;
    return b;
}

// ---------------------------------------------------------
string MapEepToFlash(string addeep,bool ram)
{
    string addini;

    while (addeep.length()<4)
        addeep = "0" + addeep;

    if (ram)
        addini="FF80"+addeep;
    else
        addini="FF00"+addeep;

    return addini;
}


// ------------------------------------------------------------------------
// a word in little endian form must be get from an hex array of bytes of len nibbles.
int power16[8]={1,16,256,4096,16*16*16*16,16*16*16*16*16,16*16*16*16*16*16};

int GetWord(unsigned char* hex,int len){

    int res=0;
    int nbytes=len>>1;
    unsigned char nib;
    int power;

    while (nbytes){
        nib=(hex[nbytes-1]>>4);
        power=power16[len-1];
        res+=nib*power;
        len--;
        nib=(hex[nbytes-1]&0x0F);
        res+=nib*power16[len-1];
        len--;
        nbytes--;
    }
    return res;
}


// ------------------------------------------------------------------------
// input string : 10/12/10 10/12/10 10/12/10 29/9/10
int getver(string ver){

    char* pc=&ver[0];

    int v;

    v=atoi(pc);
    while (*pc!='/')
        pc++;

    v+=atoi(++pc)*31;

    while (*pc!='/')
        pc++;

    v+=atoi(++pc)*365;

    //v= atoi(pc+2)+atoi(pc+5)*100+atoi(pc+8)*1000;
    return v;

}
// -------------------------------------------------------------------------------------------------------------
//mirem si es un port USB fent de port COM.
//Ho mirem al registre dle windows, per tant, nomes funciona en windows
bool IsUSBPort(int com)
{
    string p = "COM" + iToS(com);
    string temp, port;

    const HKEY HK=HKEY_LOCAL_MACHINE;
    const char * cle="HARDWARE\\DEVICEMAP\\SERIALCOMM";

    //Obrim el registre
    HKEY ret;
    RegOpenKeyEx(HK,cle,0,KEY_ALL_ACCESS,&ret);

    unsigned long lNom=128, lVal=128, ty=0;
    char nomVal[128];
    unsigned char valor[128];

    long res;
    bool esUSB=false;
    int n=0;
    do {
        res = RegEnumValue( ret, n, nomVal, &lNom, 0, &ty, valor, &lVal);
        if (res == ERROR_NO_MORE_ITEMS)
            break;

        temp = nomVal;
        port = (char *)valor;

        if (port == p) {
            if (temp.find("VCP",0) != string::npos)
                esUSB=true;
            break;
        }
        n++;
    } while (res != ERROR_NO_MORE_ITEMS);

    RegCloseKey(ret);

    return esUSB;
}


#define H_POLINOMI 0x10
#define L_POLINOMI 0x21

void CalcCRC(unsigned char *trama, int lon, unsigned char *crc)
{
   	int j, i;
   	unsigned char k, w1;

    crc[0]=0;
    crc[1]=0;

	// Calcula el CRC amb la cadena passada
	for (j=0; j<lon; j++) {
		k=trama[j];
		for (i=0; i<8; i++, k<<=1) {
			w1=(crc[0] & 0x80);

			//crc<<=1
			crc[0]<<=1;
			if (crc[1]&0x80) crc[0] |= 0x01;
			crc[1]<<=1;

			if (k & 0x80) crc[1] |= 0x01;
			if (w1) {
			    crc[0]^=H_POLINOMI;
			    crc[1]^=L_POLINOMI;
			}
		}
	}
	// Acaba de fer els 16 bits del crc amb 2 bytes a 0x00
	for (j=0;j<16;j++) {
		i=crc[0]&0x80;
        //crc<<=1
        crc[0]<<=1;
        if (crc[1]&0x80) crc[0] |= 0x01;
        crc[1]<<=1;

        if (i) {
		    crc[0]^=H_POLINOMI;
		    crc[1]^=L_POLINOMI;
        }
	}
}

// -----------------------------------------------------------------------------------------------
static char sfK2[] = { 0x30,0x31,0x33,0x34,0x35,0x33,0x37,0x32,0x32,0x00,0x41,0x61,0x67,0x43,0x4E,0x51,0x00 };
static char sfC2[] = { 0x41,0x3A,0x41,0x3B,0x42,0x42,0x42,0x55,0x43,0xAA,0x43,0x43,0x00,0x34,0x31,0x31,0x00 };
static char sfK1[] = { 0x21,0x62,0x63,0x64,0x45,0x66,0x67,0x48,0x00 };
static char sfC1[] = { 0x38,0x32,0x32,0x34,0x00,0x40,0x20,0xBB,0x00 };

string CalcAppAes()
{
    unsigned char seedkey[16];
    char seedTxt[16];
    int i=0;
    char ch;
    for (i=0;i<8;i++){
        seedkey[i] = (sfC2[i] ^ sfK1[i]) | ((BYTE)sfC1[i]<<i) ^ ((BYTE)sfK2[i]>>i);
        ch=nibble2ascii(seedkey[i]>>4);
        seedTxt[i*2]=nibble2ascii(seedkey[i]>>4);
        ch=nibble2ascii(seedkey[i]);
        seedTxt[i*2+1]=nibble2ascii(seedkey[i]);
    }
    return seedTxt;
}

string LegacyCalcAppAes()
{
    char seedtxt[16];
    char seedkey[16];

    strcpy(&seedkey[0],&sfK1[0]);

    strcpy(&seedtxt[0],&sfC1[0]);

    strcpy(&seedkey[8],&sfK2[0]);

    strcpy(&seedtxt[4],&sfC2[0]);

    return seedkey;
}
// SN are 8 chars long,changes LE to BE and viceversa.
wxString LE2BEHexSN(wxString input)
{
    if (input.length()<8)
        return input;

    wxString Out="";
    Out.Append(input.GetChar(6));
    Out.Append(input.GetChar(7));
    Out.Append(input.GetChar(4));
    Out.Append(input.GetChar(5));
    Out.Append(input.GetChar(2));
    Out.Append(input.GetChar(3));
    Out.Append(input.GetChar(0));
    Out.Append(input.GetChar(1));
    return Out;

}

wxString LE2BEHexAPPID(wxString input)
{
    if (input.length()<8)
        return input;

    wxString Out="";
    Out.Append(input.GetChar(2));
    Out.Append(input.GetChar(3));
    Out.Append(input.GetChar(0));
    Out.Append(input.GetChar(1));
    return Out;

}

//El XMLParser crida aquesta funci� quan troba un element en un fitxer XML
wxString UnCipherTag(wxString tag,bool iscipher,bool legacy){   // aes ciphering

    if (iscipher==false)
        return tag;

    wxString AesKey;
    if (legacy)
        AesKey=LegacyCalcAppAes();
    else
        AesKey=CalcAppAes();
    unsigned char crc[2];
    unsigned char buff[1024];
    unsigned char buffout[1024];

    int taglen=tag.Len();            // 22 is lenght of header till '>'
    if (taglen < 16){
        int kk=0;
        return tag;
    }

    int tagoutlen=taglen>>1;
    AsciiChainToHex((char*)&tag[0],buff,taglen);

    CalcCRC(buff,tagoutlen-2,crc);
    if (crc[0]!=buff[tagoutlen-2] || crc[1]!=buff[tagoutlen-1])
        return "Tamper";

    tagoutlen-=2;       // strip crc away
    AES crypt;
    crypt.SetParameters(16*8,16*8); // 128 bit key and ciphered data
    crypt.StartDecryption((unsigned char*)&AesKey[0]);
    const unsigned char nullvect[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    crypt.Decrypt((unsigned char*)&buff[0],(unsigned char*)&buffout[0],tagoutlen/16,static_cast<AES::BlockMode>(1),(unsigned char*)nullvect);       // 1 = means CBC mode.

    wxString tagout="";
    for (int i=0;i<tagoutlen;i++){
        wxChar ch=(wxChar)buffout[i];
        if (ch!=0x00){
            tagout.Append(ch);
        }else{
            int kk=0;
            break;
        }
    }
    return tagout;
}

// ------------------------------------------------------------ CipherTag ---------------------------
wxString CipherTag(wxString tag,bool legacy){   // aes ciphering

    wxString AesKey;
    if (legacy)
        AesKey=LegacyCalcAppAes();
    else
        AesKey=CalcAppAes();

    unsigned char xmlcipher[1024];
//    char buff[4096000];

    int taglen=tag.Len();            // 22 is lenght of header till '>'
    int fill=0;
    if (int j=(taglen%16)){
        fill=16-j;
    }
    taglen+=fill;
    while (fill--)
        tag+=(wxChar)0x00;
        // apply cipher now.

    AES crypt;
    crypt.SetParameters(16*8,16*8); // 128 bit key and ciphered data
    crypt.StartEncryption((unsigned char*)&AesKey[0]);
    const unsigned char nullvect[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    crypt.Encrypt((unsigned char*)&tag[0],(unsigned char*)&xmlcipher[0],taglen/16,static_cast<AES::BlockMode>(1),(unsigned char*)nullvect);       // 1 = means CBC mode.

    unsigned char crc[2];               // protect from modify.
    CalcCRC(xmlcipher,taglen,crc);
    xmlcipher[taglen++]=crc[0];
    xmlcipher[taglen++]=crc[1];

    tag="";
    for (int i=0;i<taglen;i++){
        tag.Append((wxChar)nibble2ascii(xmlcipher[i]>>4));
        tag.Append((wxChar)nibble2ascii(xmlcipher[i]));
    }
    return tag;
}

// ---------------------------------------- Cipher File --------------------
bool CipherFile(wxString source,wxString dest,bool legacy)
{
    string filetmp = source.c_str();
    ifstream fileopen;
    ofstream filecipher;
    string line;

    fileopen.open( (char *) filetmp.c_str(), ifstream::in );
    filecipher.open((char* ) dest.c_str(), ifstream::out );
    if (fileopen.good() && filecipher.good()){
        //filecipher << "<?xml version=\"1.x\" encoding=\"ISO-8859-1\"?>\n";
        int cnt=0;
        while (!fileopen.eof()){
            wxString linenum=_wx(iToH(cnt));
            while (linenum.Len()<5) linenum = "0" + linenum;
            getline ( fileopen, line);
            wxString citag = CipherTag(linenum+_wx(line),false)+"\n";
            filecipher << citag;
            cnt++;
        }
    }else{
        return false;
    }
    fileopen.close();
    filecipher.close();
    return true;

}


// ---------------------------------------- Uncipher File -------------------
bool UnCipherFile(wxString fileName,wxString fileout,bool legacy)
{
    ifstream fitxer;
    fitxer.open( (char *) fileName.c_str() );
    if (fitxer.is_open()) {
        //Llegim el fitxer i el guardem en un buffer, despr�s el tornem a escriure en el fitxer amb control de errors xml
        wxString buffin="";
        wxString linewx;
        wxString linewxcipher;
        string line;
/*        getline (fitxer,line);
        linewx=_wx(line);
        buffin+=linewx+"\n";*/

        while (! fitxer.eof() )
        {
            getline (fitxer,line);
            linewxcipher=_wx(line);
            linewx=UnCipherTag(linewxcipher,true,legacy);
            if (true)
                linewx=linewx.Mid(5);               // extract line number from cipher.
            bool filerw=true;
            for (int i=0;i<linewx.Len();i++){        // if strange chars do not read the line.
                unsigned char uch=(unsigned char)line[i];
                if ((uch>=127 && uch<160) || (uch==255) || uch<=0x08)       // fucking chars.
                    filerw=false;
            }
            if (filerw)
                buffin += linewx+"\n";           // getline looses the \n
        }
        fitxer.close();
        ofstream filewr;
        filewr.open( (char *)fileout.c_str(), ifstream::out );
        if (filewr.good()) {
            filewr << buffin;
            filewr.close();
        }else{
            filewr.close();
            return false;
        }
        return true;
    }
    return false;
}


// ---------------------------------------- Cipher File --------------------
bool CipherBinFile(wxString source,wxString dest,bool legacy)
{
    string filetmp = source.c_str();
    ifstream fileopen;
    ofstream filecipher;
    string line;

    fileopen.open( (char *) filetmp.c_str(), ifstream::in );
    filecipher.open((char* ) dest.c_str(), ifstream::out );
    if (fileopen.good() && filecipher.good()){
        //filecipher << "<?xml version=\"1.x\" encoding=\"ISO-8859-1\"?>\n";
        int cnt=0;
        while (!fileopen.eof()){
            wxString linenum=_wx(iToH(cnt));
            while (linenum.Len()<5) linenum = "0" + linenum;
            getline ( fileopen, line);
            wxString citag = CipherTag(linenum+_wx(line),false)+"\n";
            filecipher << citag;
            cnt++;
        }
    }else{
        return false;
    }
    fileopen.close();
    filecipher.close();
    return true;

}


// ---------------------------------------- Uncipher File -------------------
bool UnCipherBinFile(wxString fileName,wxString fileout,bool legacy)
{
    ifstream fitxer;
    fitxer.open( (char *) fileName.c_str() );
    if (fitxer.is_open()) {
        //Llegim el fitxer i el guardem en un buffer, despr�s el tornem a escriure en el fitxer amb control de errors xml
        wxString buffin="";
        wxString linewx;
        wxString linewxcipher;
        string line;
/*        getline (fitxer,line);
        linewx=_wx(line);
        buffin+=linewx+"\n";*/

        while (! fitxer.eof() )
        {
            getline (fitxer,line);
            linewxcipher=_wx(line);
            linewx=UnCipherTag(linewxcipher,true,legacy);
            if (true)
                linewx=linewx.Mid(5);               // extract line number from cipher.
            bool filerw=true;
            for (int i=0;i<linewx.Len();i++){        // if strange chars do not read the line.
                unsigned char uch=(unsigned char)line[i];
                if ((uch>=127 && uch<160) || (uch==255) || uch<=0x08)       // fucking chars.
                    filerw=false;
            }
            if (filerw)
                buffin += linewx+"\n";           // getline looses the \n
        }
        fitxer.close();
        ofstream filewr;
        filewr.open( (char *)fileout.c_str(), ifstream::out );
        if (filewr.good()) {
            filewr << buffin;
            filewr.close();
        }else{
            filewr.close();
            return false;
        }
        return true;
    }
    return false;
}

string NowDate(string lang){

    wxDateTime now;
    int i;

    now=wxDateTime::Now();
    string day="";
    if (now.GetDay()<10){       // Torna de 1 a 31
        day+="0";
    }
    day+=iToS(now.GetDay());

    string month="";
    i=now.GetMonth()+1;         // GetMonth() torna de 0 a 11!!!
    if (i<10){
        month+="0";
    }
    month+=iToS(i);

    string year="";
    i=now.GetYear();            // Torna 2008!!
    if (i>2000){
        i-=2000;
    }
    if (i<10){
        year+="0";
    }
    year+=iToS(i);

    if (lang=="en")
        return (month + "/" + day + "/" + year);
    else
        return (day + "/" + month + "/" + year);
}


string NowTime(void){
    wxDateTime now;
    now=wxDateTime::Now();
    return ((string)now.FormatTime());
}

