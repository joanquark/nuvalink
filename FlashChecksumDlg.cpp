#include "wx_pch.h"
#include "util.h"
#include "FlashChecksumDlg.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(FlashChecksumDlg)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(FlashChecksumDlg)
//*)

//(*IdInit(FlashChecksumDlg)
const long FlashChecksumDlg::ID_LISTCTRL1 = wxNewId();
const long FlashChecksumDlg::ID_SPEEDBUTTON1 = wxNewId();
const long FlashChecksumDlg::ID_SPEEDBUTTON2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(FlashChecksumDlg,wxDialog)
	//(*EventTable(FlashChecksumDlg)
	//*)
END_EVENT_TABLE()

FlashChecksumDlg::FlashChecksumDlg(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	BuildContent(parent,id,pos,size);
}



void FlashChecksumDlg::BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(FlashChecksumDlg)
	wxBoxSizer* BoxSizer1;
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, wxID_ANY, _("Flash compare resutl"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(2, 1, 0, 0);
	ListCtrl1 = new wxListCtrl(this, ID_LISTCTRL1, wxDefaultPosition, wxSize(380,306), wxLC_REPORT|wxVSCROLL|wxHSCROLL, wxDefaultValidator, _T("ID_LISTCTRL1"));
	FlexGridSizer1->Add(ListCtrl1, 1, wxALL|wxEXPAND, 5);
	BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	wxBitmap SpeedButton1_BMP(_("./icons/toolbar/ok.png"), wxBITMAP_TYPE_ANY);
	SpeedButton1 = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, SpeedButton1_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(50,50), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	SpeedButton1->SetMinSize(wxSize(-1,-1));
	SpeedButton1->SetUserData(0);
	BoxSizer1->Add(SpeedButton1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	wxBitmap SpeedButton2_BMP(_("./icons/toolbar/exit.png"), wxBITMAP_TYPE_ANY);
	SpeedButton2 = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, SpeedButton2_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(50,50), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	SpeedButton2->SetUserData(0);
	BoxSizer1->Add(SpeedButton2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	FlexGridSizer1->Add(BoxSizer1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	SetSizer(FlexGridSizer1);
	FlexGridSizer1->Fit(this);
	FlexGridSizer1->SetSizeHints(this);

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&FlashChecksumDlg::OnSpeedButton1LeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&FlashChecksumDlg::OnSpeedButton2LeftClick);
	//*)
}



void FlashChecksumDlg::ShowData(uint32_t add,uint16_t* source, uint16_t* dest,int num)
{
    ListCtrl1->ClearAll();
    ListCtrl1->InsertColumn(0,"Address");
    ListCtrl1->InsertColumn(1,"Source Check");
    ListCtrl1->InsertColumn(2,"Flash Check");
    ListCtrl1->InsertColumn(3,"Result");

    ListCtrl1->SetColumnWidth(0,100);
    ListCtrl1->SetColumnWidth(1,100);
    ListCtrl1->SetColumnWidth(2,100);
    ListCtrl1->SetColumnWidth(3,80);
    int i;
    for (i=0;i<num;i++)
    {
        int tmp = ListCtrl1->InsertItem(i, "-", 0);
        ListCtrl1->SetItemData(tmp, i);
        ListCtrl1->SetItem(i, 0, _wx(iToH(add)));
        ListCtrl1->SetItem(i, 1, _wx(iToH(*source)));
        ListCtrl1->SetItem(i, 2, _wx(iToH(*dest)));
        if (*source==*dest)
            ListCtrl1->SetItem(i, 3, _wx("EQUAL"));
        else
            ListCtrl1->SetItem(i, 3, _wx("*"));
        ListCtrl1->EnsureVisible(i);
        add+=4096;
        source++;
        dest++;

    }
}


FlashChecksumDlg::~FlashChecksumDlg()
{
	//(*Destroy(FlashChecksumDlg)
	//*)
}


void FlashChecksumDlg::OnSpeedButton1LeftClick(wxCommandEvent& event)
{
    EndModal(wxID_OK);
}

void FlashChecksumDlg::OnSpeedButton2LeftClick(wxCommandEvent& event)
{
    EndModal(wxID_CANCEL);
}
