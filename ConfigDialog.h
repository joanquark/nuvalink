#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(ConfigDialog)
	#include <wx/choice.h>
	#include <wx/dialog.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	//*)
#endif
//(*Headers(ConfigDialog)
#include <wxSpeedButton.h>
//*)

#include "config.h"

class ConfigDialog: public wxDialog
{
	public:

		ConfigDialog(wxWindow* parent,wxWindowID id=wxID_ANY);
		virtual ~ConfigDialog();
        void SetCfg(CConfig *conf);
		//(*Declarations(ConfigDialog)
		wxChoice* Choice1;
		wxChoice* Choice2;
		wxChoice* choiceRS485Speed;
		wxSpeedButton* SpeedButton1;
		wxSpeedButton* SpeedButton2;
		wxStaticText* StaticText1;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxStaticText* StaticText4;
		wxStaticText* StaticText5;
		wxStaticText* StaticText6;
		wxStaticText* StaticText7;
		wxStaticText* StaticText9;
		wxTextCtrl* TxtIPCloud;
		wxTextCtrl* TxtIPPRN;
		wxTextCtrl* TxtPortCloud;
		wxTextCtrl* TxtPortPRN;
		wxTextCtrl* TxtVCOM;
		//*)

	protected:

		//(*Identifiers(ConfigDialog)
		static const long ID_STATICTEXT7;
		static const long ID_CHOICE3;
		static const long ID_STATICTEXT1;
		static const long ID_CHOICE1;
		static const long ID_STATICTEXT2;
		static const long ID_CHOICE2;
		static const long ID_STATICTEXT9;
		static const long ID_TEXTCTRL5;
		static const long ID_STATICTEXT3;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT4;
		static const long ID_TEXTCTRL2;
		static const long ID_STATICTEXT5;
		static const long ID_TEXTCTRL3;
		static const long ID_STATICTEXT6;
		static const long ID_TEXTCTRL4;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		//*)

	private:
        CConfig* config;
		//(*Handlers(ConfigDialog)
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnSpeedButton2LeftClick(wxCommandEvent& event);
		void OnChoice2Select(wxCommandEvent& event);
		void OnChoice3Select(wxCommandEvent& event);
		//*)

	protected:

		void BuildContent(wxWindow* parent,wxWindowID id);

		DECLARE_EVENT_TABLE()
};

#endif
