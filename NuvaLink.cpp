#include "nuvalink.h"
#include <wx/filename.h>
#include <wx/msgdlg.h>
#include "cid.h"
#include "jsonval.h"
#include "jsonwriter.h"
#include "jsonreader.h"
#include <wx/stdpaths.h>
#include "util.h"
#include "FlashChecksumDlg.h"

extern "C" {
    #include "SSL/SSLxfer.h"
}

Cnuvalink::Cnuvalink(CLanguage* lang)

{
    online = false;
    transmitting=false;
    refrescAuto = false;
    //Carreguem la configuraci�
    this->Lang=lang;
    config.Lang=Lang;
    install.Lang=Lang;
    Lang=Lang;
    if (!config.Load())
        wxMessageBox(_wx(config.GetLastError()), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));

    protocolNIOT = new CProtocolNIOT();
    protocolNIOT->myLog = &myLog;

    connecthandler.myLog = &myLog;
    connecthandler.Lang = Lang;

    modelSelect = 0;
    grupSelect = 0;
    progressDlg = 0;
  //  counterIdModified=0;
    estattmp=0;
    StorageAAliasGroup = 0;
    StorageZAliasGroup = 0;
    StorageOAliasGroup = 0;
    StorageUAliasGroup = 0;

    timerinactivity=0;
    LastFlashFile="";
    FFTSentPtr=0;
    this->SN="FFFFFFFF";

}

Cnuvalink::~Cnuvalink()
{
    if (online){
        Disconnect();
    }
}

void Cnuvalink::Init()
{
    //Carreguem els iconos del TreeView
    wxImageList *llistaImg = Tree->GetImageList();

    indImgCarpeta = llistaImg->Add(wxBitmap("./icons/folder.png", wxBITMAP_TYPE_PNG));

    indImgCarpetaOberta = llistaImg->Add(wxBitmap("./icons/open_folder.png", wxBITMAP_TYPE_PNG));

    indImgInstalacio = llistaImg->Add(wxBitmap("./icons/home.png", wxBITMAP_TYPE_PNG));

    indImgCentral = llistaImg->Add(wxBitmap("./icons/chip.png", wxBITMAP_TYPE_PNG));

    indImgDades = llistaImg->Add(wxBitmap("./icons/info.png", wxBITMAP_TYPE_PNG));

    indImgEvents = llistaImg->Add(wxBitmap("./icons/doc-list.png", wxBITMAP_TYPE_PNG));

    indImgEstat = llistaImg->Add(wxBitmap("./icons/info.png", wxBITMAP_TYPE_PNG));

    indImgVerify = llistaImg->Add(wxBitmap("./icons/clipboard_check.png", wxBITMAP_TYPE_PNG));

    indImgVmodem = llistaImg->Add(wxBitmap("./icons/modem.png", wxBITMAP_TYPE_PNG));

    indImgVCOM = llistaImg->Add(wxBitmap("./icons/RS232.png", wxBITMAP_TYPE_PNG));

    TreeCtr.Tree = Tree;

    protocolNIOT->Setconnecthandler(&connecthandler);
    protocolNIOT->timerHello = timerHello;
    protocolNIOT->myApp = myApp;
    connecthandler.myApp = myApp;
    // -------------------------------- start Receive thread -------------------------------
    protocolNIOT->Create();
    protocolNIOT->Run();

    privlevel=INSTAL_PRIVLEVEL;            // user priv level.
    IsChangeProg=false;
    F_INSTAL_LEVEL2=false;

}

bool Cnuvalink::SetDevSN(wxString serial)
{
    this->SN=serial;

}
wxString Cnuvalink::GetDevSN(){
    return this->SN;
}

//Crea una instalacio nova. Si li donen un model afegeix automaticament
//una central
bool Cnuvalink::NewInstal(wxString model, wxString versio,CData* dt)
{
    install.Clear();
    install.Lang=Lang;
    if (model != "") {
        string id="0";
        if (!install.SetControlPanel(id, model, versio)) {
            msgError = install.GetLastError();
            return false;
        }
    }
    NomInstal=_wx(dt->getname());
    install.SetDades(*dt);
    FillTreeView();
    return true;
}


bool Cnuvalink::OpenInstal(string& fileName)
{
    install.Clear();
    install.Lang=Lang;
//    if (cloudmode==true){

//    }else{
        if (ReadInstalXML(fileName)) {
            string codisn="SN";
            this->SN=LE2BEHexSN(install.GetCodiVal(codisn));
            string codikerInputs="6BC";
            this->panelActiveInputs=sToI(install.GetCodiVal(codikerInputs));
            // in cloud connection mode, need to
            panelEdit->Clear();
            install.SetFileName(fileName);
            CData *dades = install.GetDataObj();
            NomInstal=_wx(dades->getname());
            install.ClrChanges();
            FillTreeView();
            return true;
        } else {
            CloseInstal();
            return false;
        }
//    }
}

void Cnuvalink::CloseInstal()
{
    panelEdit->Clear();
    ClearTreeView();
    TreeCtr.Clear();
    install.Clear();
    LastFlashFile="";        // envia fitxer desde inici don�
    FFTSentPtr=0;
}

bool Cnuvalink::OpenInstalCloud(wxString json,wxString versio)
{
    if (ReadInstalJSON(json,versio)){
        // SN is taken from DDBB origina JSON inside ReadInstalJSON
        //string codisn="SN";
        //this->SN=LE2BEHexSN(installGetCodiVal(codisn));
        // in cloud connection mode, need to
        panelEdit->Clear();
        CData *dades = install.GetDataObj();
        NomInstal=_wx(dades->getname());
        install.ClrChanges();

        string codikerInputs="6BC";
        this->panelActiveInputs=sToI(install.GetCodiVal(codikerInputs));

        FillTreeView();
        // default connectio mode to be cloud
        config.SetConnectionType(TYPE_CLOUD_CONNECTION);
        dades->setconType(TYPE_CLOUD_CONNECTION);



        return true;
    } else {
        CloseInstal();
        return false;
    }
}

bool Cnuvalink::ReadInstalJSON(wxString jsontxt,wxString versio)
{
    if (versio=="000")
        versio="210";

    wxJSONReader reader(wxJSONREADER_TOLERANT);
    wxJSONValue valread;
    if (int errors=reader.Parse((const wxString)jsontxt,&valread)){
        wxMessageBox("Errors :"+_wx(iToS(errors)));
        return false;
    }
    //Ens assegurem que la instalacio la creem desde zero
    install.Clear();
    //debug
    install.control = this->control;
    if (install.ParseJSON(valread,versio)==false) {
        msgError = Lang->GetAppMiss("BAD JSON");
        return false;
    }
    if (!install.GettotOk()) {
        msgError = install.GetLastError();
        install.Clear();
        return false;
    }
    wxString jsSN=valread["SN"].AsString();
    if (jsSN.IsEmpty() || jsSN=="null")
        jsSN=valread["sn"].AsString();

    this->SN=jsSN;

    return true;
}

bool Cnuvalink::ReadInstalXML(string& fileName)
{
    if (UnCipherFile(fileName,"tmp.xml",false)){
        //Ens assegurem que la instalacio la creem desde zero
        install.Clear();
        //debug
        install.control = this->control;
        //Analitzem el XML
        myXmlParser xml;
        //Li diem al objecte que interpreta el xml que informi al objecte instalacio
        xml.SetSubscriber(install);
        bool docParsed = xml.parse( _T("tmp.xml"));
//        delete buffer;
        if (privlevel<DEBUG_PRIVLEVEL)
            wxRemoveFile( "tmp.xml" );      // if degug level, let the option of whatch file unciphered.

        if (!docParsed) {
            msgError = Lang->GetAppMiss("MISS_ERROR_BAD_XML");
            return false;
        }
        // Encara que el XML sigui correcte no vol dir que contingui tota
        // la informacio necessaria com per seguir funcionant o que les
        // dades siguin correctes. Li preguntem al objecte CInstalation si
        // ha estat capa� de crear-se be. Si no �s aix�, mostrem l'error
        // i donem l'operacio per cancelada
        if (!install.GettotOk()) {
            msgError = install.GetLastError();
            install.Clear();
            return false;
        }
    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_READ_FILE");
        msgError += fileName;
        return false;
    }
    return true;
}

void Cnuvalink::ClearTreeView()
{
    Tree->DeleteAllItems();
}

//Omple el TreeView amb les carpetes necessaries en funcio dels models
//que hi hagi a la instalacio
void Cnuvalink::FillTreeView()
{
    CData *dades;
    CModel *model;
    CStatus *estat;

    wxTreeItemId idTreeRoot;

    //Primer cal buidar l'Tree
    ClearTreeView();
    TreeCtr.Clear();

    dades = install.GetDataObj();
    if (dades) {
        idTreeRoot = Tree->AddRoot(_wx(dades->getname()), indImgInstalacio);
        GenDataBranch(idTreeRoot, dades);
    }
    else {
        idTreeRoot = Tree->AddRoot(Lang->GetAppMiss("MSG_DEF_NONAME"));
    }
    TreeCtr.SetRoot(idTreeRoot);

    model = install.GetControlPanel();
    GenModelBranch(idTreeRoot, model);
    Tree->Expand(idTreeRoot);
}


void Cnuvalink::GenDataBranch(wxTreeItemId& idTreeRoot, CData *dades)
{
    wxTreeItemId idTreeDades = Tree->AppendItem(idTreeRoot, Lang->GetAppMiss("MSG_DEF_DATA"), indImgDades);
    TreeCtr.Add(idTreeDades, dades);
}


string Cnuvalink::GetStrValID(wxTreeItemId idConvert) {
  string strReturn;

  std::stringstream out;
  out << idConvert;
  strReturn = out.str();

  return(strReturn);
}

void Cnuvalink::GenGroupBranch(wxTreeItemId& idTreePare, CGroup *grup)
{
    if (grup) {
        string compareStr;

        if (CheckPrivLevel(grup->GetUpl())==false)
            return;

        wxString catwx=_wx(grup->GetCat());
        if (config.IsCat(catwx)==false)
            return;

        wxTreeItemId id = Tree->AppendItem(idTreePare, _wx(grup->GetDescr()), indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(id, grup);

        *control << "\ngrup: " << _wx(grup->GetDescr());
        //sub grups
        if (grup->GetNumChildGroup()) {
            for (int j=0; j<grup->GetNumChildGroup(); j++) {
                CGroup *fill = grup->GetChildGroup(j);
                compareStr = fill->GetId();
                wxString grid=_wx(compareStr);


                if ((StorageNameGroup=="Alias")||(StorageNameGroup=="alias")||(StorageNameGroup=="Alias SMS")){
                    if((compareStr=="Alias-Zones")||(compareStr=="Zonas")){
                        StorageZAliasGroup = fill;
                        StorageID=idTreePare;
                    }
                    if((compareStr=="Alias-Areas")||(compareStr=="Secteurs")){
                        StorageZAliasGroup = fill;
                        StorageID=idTreePare;
                    }
                    if((compareStr=="Alias-Sortides")||(compareStr=="Outputs")||(compareStr=="Sorties")){
                        StorageOAliasGroup = fill;
                        StorageID=idTreePare;
                    }
                    if((compareStr=="Alias-Users")||(compareStr=="Users")||(compareStr=="Utilisateurs")){
                        StorageUAliasGroup = fill;
                        StorageID=idTreePare;
                    }
                }

                if (grid.Contains("ZNA")){

                    string gid2=grid.c_str();
                    string num=(string)&gid2[3];

                    int input=sToI(num);

                    if (input>panelActiveInputs)            // do not generate then
                        continue;
                }

                GenGroupBranch(id, fill);
            }
        }
    }
}

void Cnuvalink::GenGroupBranchFlash(wxTreeItemId& idTreePare, CGroupFlash *grupFlash)
{
    if (grupFlash) {
        wxTreeItemId id = Tree->AppendItem(idTreePare, _wx(grupFlash->GetDescr()), indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(id, grupFlash);
        *control << "\ngrupFlash: " << _wx(grupFlash->GetDescr());
    }
}

void Cnuvalink::GenModelBranch(wxTreeItemId& idTreeRoot, CModel *model)
{
    wxTreeItemId id = Tree->AppendItem(idTreeRoot, _wx(model->GetDescr()), indImgCentral);
    TreeCtr.Add(id, model);
    CGroup *grup = model->GetFatherGroup();
    if (grup) {
        for (int i=0; i<grup->GetNumChildGroup(); i++) {
            CGroup *fill = grup->GetChildGroup(i);
            StorageNameGroup = fill->GetId();
            GenGroupBranch(id, fill);
        }
    }
    for (int i=0; i< model->GetNumGrupsFlash(); i++) {
        CGroupFlash *grupFlash = model->GetGroupFlash(i);
        if (CheckPrivLevel(grupFlash->GetUpl())){
            wxString catwx=_wx(grupFlash->GetCat());
            if (config.IsCat(catwx)==true)
                GenGroupBranchFlash(id, grupFlash);
        }else{
            //wxMessageBox(_wx(grupFlash->GetId())+" FALSE priv level "+_wx(iToS(grupFlash->GetUpl())));
        }
    }

    if (privlevel!=PROD_PRIVLEVEL){
        GenStatusBranch(id, model);
        GenEventsBranch(id, model);
        GenVModemBranch(id, model);
        GenVCOMBranch(id, model);
    }
    if (privlevel>=ENG_PRIVLEVEL)
        GenVerifyBranch(id,model);

}

void Cnuvalink::GenStatusBranch(wxTreeItemId& idTreeModel, CModel *model)
{
    if (model->HasStatus()==false)
        return;

    wxTreeItemId estatId = Tree->AppendItem(idTreeModel, Lang->GetAppMiss("MSG_DEF_STATE"), indImgEstat);
    CStatus *estat = model->GetStatus();
    CData* data=install.GetDataObj();
    TreeCtr.Add(estatId, estat);
    if (estat->HasGeneral()) {
        wxTreeItemId brancaGeneralId = Tree->AppendItem(estatId, Lang->GetAppMiss("MSG_DEF_GENERAL"), indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaGeneralId, ID_SECTION_GEN_STATUS);
    }
    if (estat->HasBeacons()) {
        wxString becs=_T("BLE Devices");
        wxTreeItemId brancaBeaconsId = Tree->AppendItem(estatId, becs, indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaBeaconsId, ID_SECTION_BEACON_STATUS);
    }
    if (privlevel>=ENG_PRIVLEVEL) {
        wxString ramdbg=_T("Raw mem");
        wxTreeItemId brancaMemPanel = Tree->AppendItem(estatId, ramdbg, indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaMemPanel, ID_SECTION_MEM_PANEL);
    }
    if (data->gettype()=="AGR"){
        wxString agr=_T("Agro");
        wxTreeItemId brancaAgroPanel = Tree->AppendItem(estatId, agr, indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaAgroPanel, ID_SECTION_AGRO_PANEL);
    }
    if (estat->HasWireless()) {
        wxTreeItemId brancaViaRadioId = Tree->AppendItem(estatId, Lang->GetAppMiss("MSG_DEF_VIARADIO"), indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaViaRadioId, ID_SECTION_ZW_STATUS);
    }
    if (estat->HasWirelessUser()) {
        wxTreeItemId brancaViaRadioUserId = Tree->AppendItem(estatId, Lang->GetAppMiss("MSG_DEF_VIARADIOUSER"), indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaViaRadioUserId, ID_SECTION_UW_STATUS);
    }
    if (estat->HasWirelessOut()) {
        wxTreeItemId brancaViaRadioOutId = Tree->AppendItem(estatId, Lang->GetAppMiss("MSG_DEF_VIARADIOOUT"), indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaViaRadioOutId, ID_SECTION_OW_STATUS);
    }
#ifdef _PICTURES
    if (estat->HasPictures()){
        wxTreeItemId brancaPicturesId = Tree->AppendItem(estatId, Lang->GetAppMiss("MSG_DEF_PICTURES"), indImgCarpeta, indImgCarpetaOberta);
        TreeCtr.Add(brancaPicturesId, ID_SECTION_PIC_STATUS);
    }
#endif
    CGroup *grup = model->GetStatusGroup();
    if (grup) {
        for (int i=0; i<grup->GetNumChildGroup(); i++) {
            CGroup *fill = grup->GetChildGroup(i);
            GenGroupBranch(estatId, fill);
        }
    }
}

void Cnuvalink::GenEventsBranch(wxTreeItemId& idTreeModel, CModel *model)
{
    CEventList *lEvents = model->GetEventList();

    //Si maxPunter val -1 vol dir que els events no estan definits en el model
    if (lEvents->GetMaxPunter() != -1) {
        wxTreeItemId eventsId = Tree->AppendItem(idTreeModel, Lang->GetAppMiss("MSG_DEF_EVENTS"), indImgEvents);
        TreeCtr.Add(eventsId, lEvents);
    }
}

void Cnuvalink::GenVerifyBranch(wxTreeItemId& idTreeModel, CModel *model)
{
    wxTreeItemId verifId = Tree->AppendItem(idTreeModel, "Verify", indImgVerify);
    TreeCtr.AddVerify(verifId);
}

void Cnuvalink::GenVModemBranch(wxTreeItemId& idTreeModel, CModel *model)
{
    wxTreeItemId vmodemId = Tree->AppendItem(idTreeModel, "VModem", indImgVmodem);
    TreeCtr.AddVModem(vmodemId);
}

void Cnuvalink::GenVCOMBranch(wxTreeItemId& idTreeModel, CModel *model)
{
    wxTreeItemId vcomId = Tree->AppendItem(idTreeModel, "VComPort", indImgVCOM);
    TreeCtr.AddVCOM(vcomId);
}



bool Cnuvalink::GenInstalJSON(wxString json)
{
    CData *dades;
    CModel *model=install.GetControlPanel();
    dades = install.GetDataObj();

    wxJSONValue root;
    wxJSONValue data;
    wxJSONValue codis;
    //wxJSONValue grupflash;

    data["name"]=dades->getname();
    data["type"]=dades->gettype();
    data["cusname"]=dades->getname();
    data["address"]=dades->getaddress();
    data["phone"]=dades->getphone();
    data["email"]=dades->getemail();
    data["rule"]=dades->getrule();
    data["phonePstn"]=dades->getphonePstn();
    data["phoneCsd"]=dades->getphoneCsd();
    data["conType"]=iToS(dades->getconType());
    data["host"]=dades->gethost();
    data["port"]=iToS(dades->getport());
    data["extPower"]=dades->getextPower();
    data["tBatt"]=iToS(dades->gettBatt());
    data["hostCloud"]=dades->gethostCloud();
    data["portCloud"]=iToS(dades->getportCloud());
    data["simSolution"]=dades->getSimSolution();

    for (int i=0; i< model->GetNumCodis(); i++) {
        CCodi *codi = model->GetCodi(i);
        codis[codi->GetId()]=codi->GetStringVal();
        codi->SetStringIniVal(codi->GetStringVal());            // IMPORTANT PARA ENVIAR SOLO CAMBIOS.
    }
    root["dades"]=data;
    root["codis"]=codis;

/*    for (int i=0; i< model->GetNumGrupsFlash(); i++) {
        CGroupFlash *grupFlash = model->GetGroupFlash(i);
        wxJSONValue file;
        for (int j=0; j < grupFlash->GetNumFiles(); j++) {
            CFitxerFlash *fitxer = grupFlash->GetFile(j);
            file["id"]=fitxer->GetId();
            file["nom"]=fitxer->GetNomFitxer();
        }
        grupflash[grupFlash->GetId()]=file;
    }
    root["grup-flash"]=grupflash;
    wxJSONValue st;
    wxString dt;
    //estat
    CStatus *estat = model->GetStatus();

    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsDetecting(i)) dt.Append("1");
        else dt.Append("0");
    }
    st["detectz"]=dt;
    dt="";

    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsAlarm(i)) dt.Append("1");
        else dt.Append("0");
    }
    st["alarmz"]=dt;
    dt="";

    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsOmiting(i)) dt.Append("1");
        else dt.Append("0");
    }
    st["omitz"]=dt;
    dt="";

    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsAvery(i)) dt.Append("1");
        else dt.Append("0");
    }
    st["averiaz"]=dt;
    dt="";

    for (int i=0; i< 8; i++) {
        if (estat->GetAreaSt(i) & M_AREA_ARM)
            dt.Append("1");
        else
            dt.Append("0");;
    }
    st["areast"]=dt;
    dt="";

    char stpanel = estat->GetStPanel();
    unsigned char c = 0x01;
    for (int i=0; i< 8; i++) {
        if (stpanel & c) dt.Append("1");
        else dt.Append("0");
        c <<= 1;
    }
    st["stpanel"]=dt;
    dt="";

    st["ppe"]=iToS(estat->GetPunterEvents());
    dt="";

    root["estat"]=st;
    //events
*/
    root["Login"]=config.GetSysUser();
    root["pass"]=config.GetSysPass();
    root["SN"]=this->SN;
//    root["model"]=model->GetModel();
//    root["versio"]=model->GetVersio();
    root["Op"]="save";

//    wxJSONWriter writer1( wxJSONWRITER_NONE );
//    wxString jsontxt1;
//    writer1.Write(st,jsontxt1);

    wxJSONWriter writer( wxJSONWRITER_NONE );
    wxString jsontxt;
    writer.Write(root,jsontxt);
    jsontxt+="\r\n";
    string sport=config.GetParamTcpIp("ipcloudport").c_str();
    int port=sToI(sport)-1;
    //string IP=config.GetParamTcpIp("ipclouddest").c_str();
    string IP=config.GetSessionServer().c_str();
    char devices[512];
    int answersize;
    int jsonsize=jsontxt.size();

    UnCipherFile("nvt.nvk","nvt.pem",false);
    bool success=false;
    wxStandardPaths path;
    wxString filepem=path.GetDataDir();
    if (SSLtransfer(&IP[0],port,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsonsize,&devices[0],&answersize)==TRUE){
        success=true;
    }
    wxRemoveFile("nvt.pem");
    return success;
}

bool Cnuvalink::GenInstalXML(wxString *xml)
{
    CData *dades;
    CModel *model;

    *xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n<instalacio>\n";
//    *xml = "<instalacio>\n";
    //dades
    dades = install.GetDataObj();
    *xml += "\t<dades>\n";
    *xml += "\t\t<extpower>" + dades->getextPower() + "</extpower>\n";
    *xml += "\t\t<tbatt>" + iToS(dades->gettBatt()) + "</tbatt>\n";
    *xml += "\t\t<nominstal>" + dades->getname() + "</nominstal>\n";
    *xml += "\t\t<tipusinstal>" + dades->gettype() + "</tipusinstal>\n";
    *xml += "\t\t<nomclient>" + dades->getcusname() + "</nomclient>\n";
    *xml += "\t\t<direccio>" + dades->getaddress() + "</direccio>\n";
    *xml += "\t\t<telefon>" + dades->getphone() + "</telefon>\n";
    *xml += "\t\t<email>" + dades->getemail() + "</email>\n";
    *xml += "\t\t<rule>" + dades->getrule() + "</rule>\n";
    *xml += "\t\t<telfconexio>" + dades->getphonePstn() + "</telfconexio>\n";
    *xml += "\t\t<telfconexioCSD>" + dades->getphoneCsd() + "</telfconexioCSD>\n";
    *xml += "\t\t<ConnectionType>" + iToS(dades->getconType()) + "</ConnectionType>\n";
    *xml += "\t\t<ipdest>" + dades->gethost() + "</ipdest>\n";
    *xml += "\t\t<ipport>" + iToS(dades->getport()) + "</ipport>\n";
    *xml += "\t\t<simSolution>" + dades->getSimSolution() + "</simSolution>\n";
    *xml += "\t</dades>\n";

    model = install.GetControlPanel();
    wxString buffer;
    GenComponentXML(model, &buffer);
    *xml += buffer;
    *xml += "</instalacio>";

    return true;
}

bool Cnuvalink::GenComponentXML(CModel *model, wxString *xml)
{
    *xml = "\t<component ";
    string id = model->GetId();
    string smodel = model->GetModel();
    string versio = model->GetVersio();
    if (!id.empty()) *xml += "id=\"" + id + "\" ";
    if (!smodel.empty()) *xml += "model=\"" + smodel + "\" ";
    if (!versio.empty()) *xml += "versio=\"" + versio + "\"";
    *xml += ">\n";
    *xml += "\t\t<descr>" + model->GetDescr() + "</descr>\n";

    //Codis dins
    for (int i=0; i< model->GetNumCodis(); i++) {
        CCodi *codi = model->GetCodi(i);

        if (codi->GetId()=="SN"){
                int kk=0;
        }

        *xml += "\t\t<codi";
        string cid = codi->GetId();
        if (!cid.empty()) *xml += " id=\"" + cid + "\"";
        *xml += ">\n";
        *xml += "\t\t\t<val>" + codi->GetStringVal() + "</val>\n";
        *xml += "\t\t</codi>\n";
        codi->SetStringIniVal(codi->GetStringVal());            // IMPORTANT PARA ENVIAR SOLO CAMBIOS.
    }

    //Flash
    for (int i=0; i< model->GetNumGrupsFlash(); i++) {
        CGroupFlash *grupFlash = model->GetGroupFlash(i);
        *xml += "\t\t<grup-flash id=\"" + grupFlash->GetId() + "\">\n";
        if (grupFlash->GetTypeCon() == TIPUS_GRUPFLASH_COLUMNES) {
            for (int j=0; j < grupFlash->GetNumElements(); j++) {
                *xml += "\t\t\t<element>";
                for (int k=0; k< grupFlash->GetNumCols(); k++) {
                    *xml += "<col>" + grupFlash->GetElementColString(j, k) + "</col>";
                }
                *xml += "</element>\n";
            }
        }
        else {
            for (int j=0; j < grupFlash->GetNumFiles(); j++) {
                CFitxerFlash *fitxer = grupFlash->GetFile(j);
                *xml += "\t\t\t<fitxer id=\"" + fitxer->GetId() + "\">\n";
                *xml += "\t\t\t\t<nom>" + fitxer->GetNomFitxer() + "</nom>\n";
                *xml += "\t\t\t</fitxer>\n";
            }
        }
        *xml += "\t\t</grup-flash>\n";
    }

    //estat
    *xml += "\t\t<estat>\n";
    CStatus *estat = model->GetStatus();
    *xml += "\t\t\t<detectz>";
    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsDetecting(i)) *xml += "1";
        else *xml += "0";
    }
    *xml += "</detectz>\n";
    *xml += "\t\t\t<alarmz>";
    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsAlarm(i)) *xml += "1";
        else *xml += "0";
    }
    *xml += "</alarmz>\n";
    *xml += "\t\t\t<omitz>";
    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsOmiting(i)) *xml += "1";
        else *xml += "0";
    }
    *xml += "</omitz>\n";
    *xml += "\t\t\t<averiaz>";
    for (int i=0; i< estat->GetNumZones(); i++) {
        if (estat->IsAvery(i)) *xml += "1";
        else *xml += "0";
    }
    *xml += "</averiaz>\n";
    *xml += "\t\t\t<areast>";
//    char areast = estat->GetAreaSt(i);
//    char c = 0x01;
    for (int i=0; i< 8; i++) {
        if (estat->GetAreaSt(i) & M_AREA_ARM)
            *xml += "1";
        else
            *xml += "0";
//        if (areast & c) *xml += "1";
//        else *xml += "0";
//        c <<= 1;
    }
    *xml += "</areast>\n";
    *xml += "\t\t\t<stpanel>";
    char stpanel = estat->GetStPanel();
    unsigned char c = 0x01;
    for (int i=0; i< 8; i++) {
        if (stpanel & c) *xml += "1";
        else *xml += "0";
        c <<= 1;
    }
    *xml += "</stpanel>\n";
    char buf[64];
    sprintf(buf, "\t\t\t<ppe>%d</ppe>\n", estat->GetPunterEvents());
    *xml += buf;
    *xml += "\t\t</estat>\n";
    //Mesures

    //events
    *xml += "\t\t<events>\n";
    CEventList *events = model->GetEventList();
    sprintf(buf, "\t\t\t<myppe>%d</myppe>\n", events->GetPunter());
    *xml += buf;

    for (int i=0; i < events->GetNumEvents(); i++) {
        CEvent *event = events->GetEvent(i);
        *xml += "\t\t\t<event";
        char buf[64];
        sprintf(buf, " ord=\"%d\"", i);
        *xml += buf;
        *xml += ">\n";
        *xml += "\t\t\t\t<h>" + event->GetHora() + "</h>\n";
        *xml += "\t\t\t\t<dt>" + event->GetData() + "</dt>\n";
        *xml += "\t\t\t\t<cid>" + event->GetContactID() + "</cid>\n";

        if(event->GetZona()){
            sprintf(buf,"\t\t\t\t<z>%d</z>\n", event->GetZona());
            *xml += buf;
        }
        if (event->GetArea()){
            sprintf(buf,"\t\t\t\t<a>%d</a>\n", event->GetArea());
            *xml += buf;
        }
        if (event->GetAbonat()!="") {
            *xml += "\t\t\t\t<ab>" + event->GetAbonat() + "</ab>\n";
        }

        if (event->GetFix()!=""){
            *xml += "\t\t\t\t<loc>" + event->GetFix() + "</loc>\n";
        }

        if ( event->IsTipusBin()){
            *xml+= "\t\t\t\t<bn>" + iToS(event->GetBinNum()) + "</bn>\n";
            *xml+= "\t\t\t\t<bf>" + event->GetBinFile() + "</bf>\n";
        }
        *xml += "\t\t\t</event>\n";
    }
    *xml += "\t\t</events>\n";

    *xml += "\t</component>\n";

    return true;
}


bool Cnuvalink::Need2SaveInstal()
{
    return install.GetChanges();
}

//Save
bool Cnuvalink::SaveInstal()
{
    wxString buffer;
    if (cloudmode){

        //string codisn="SN";
        //wxString SNc=LE2BEHexSN(installGetCodiVal(codisn));
        //if (this->SN!=SNc)
        //    wxMessageBox("serial and file SN's");
        modelSelect=install.GetControlPanel();
        CData *dades = install.GetDataObj();

        wxString newSN=wxGetTextFromUser("Serial Number","Serial Number",this->SN);
        if (newSN.length()==8 && newSN!=this->SN){
            this->SN=newSN;
        }
        wxString newName=wxGetTextFromUser(_T("Name"),_T("Name"),_wx(dades->getname()));
            dades->setname(newName.c_str());

        bool res=GenInstalJSON("json.njs");
        string name=install.GetFileName();
        if (name==""){
            return res;
        }
    }
//    CipherFile("nuvathings.pem","nvt.nvk",false);       // just for creating ciphered pem.

    GenInstalXML(&buffer);
    panelEdit->ApplyValues();
    string fileName = install.GetFileName();
    string filetmp = "tmp.xml";
    ofstream fitxer;
    fitxer.open( (char *) filetmp.c_str(), ifstream::out );


    if (fitxer.good()) {
        fitxer << buffer;
        fitxer.close();
        if (CipherFile(filetmp,fileName,false)){
            install.ClrChanges();
            if (privlevel!=DEBUG_PRIVLEVEL)
                wxRemoveFile( "tmp.xml" );
        }else{
            msgError = Lang->GetAppMiss("MISS_ERROR_WRITE_FILE").c_str() + fileName;
            return false;
        }
    } else {
        msgError = Lang->GetAppMiss("MISS_ERROR_WRITE_FILE").c_str() + fileName;
        return false;
    }

    return true;
}

//Save as
bool Cnuvalink::SaveInstal(string& fileName)
{
    install.SetFileName(fileName);
    return SaveInstal();
}


// Guarda el config.xml
bool Cnuvalink::SaveConfig()
{
    if (!config.Save()) {
        msgError = config.GetLastError();
        return false;
    }
    return true;
}

//Es crida quan no hi ha res seleccionat
void Cnuvalink::SelectGroup()
{
    //Nomes voliem fer un clear del panelEdit
    panelEdit->ApplyValues();
    panelEdit->Clear();
}

void Cnuvalink::SelectGroupNum(int tipus){

//    modelSelect = TreeCtr.GetModel(id);
}

//Aquesta funcio �s cridada (desde main.cpp) quan es selecciona algo
//en el treeView. Aqui decidim que s'ha de mostrar en el panelEdit
void Cnuvalink::SelectGroup(wxTreeItemId &id)
{
    modelSelect = TreeCtr.GetModel(id);
    if (!modelSelect)
        modelSelect=install.GetControlPanel();

    panelEdit->ApplyValues();
    //Si surt del panelDades potser ha modificat el nom de la instalacio
    CData *dades = panelEdit->GetData();
    if (dades) {
        Tree->SetItemText(TreeCtr.GetRoot(), _wx(dades->getname()));
    }
    panelEdit->Clear();
    refrescAuto = false;

    int tipus = TreeCtr.GetTypeCon(id);

    if (tipus == TREE_TIPUS_CDATA) {
        CData *dades = TreeCtr.GetData(id);
        string tit = Lang->GetAppMiss("MSG_DEF_DATA").c_str();
        panelEdit->SetTitle(tit);
        this->Model=modelSelect->GetModel();
        this->Version=modelSelect->GetVersio();
        panelEdit->GenPanelDades(dades,&Model,&Version);
    }
    else if (tipus == TREE_TIPUS_CGROUP) {
        CGroup *grup = TreeCtr.GetGroup(id);
        string descr = grup->GetDescr();
        CStatus *estat = modelSelect->GetStatus();

        panelEdit->SetTitle(descr);

            wxString gid=_wx(grup->GetId());
            int zone=0;
            if (gid.Contains("ZNA")){
                string gid2=gid.c_str();
                string num=(string)&gid2[3];
                zone=sToI(num);
            }
            for (int i=0; i<grup->GetNumChildCodi(); i++) {
                CCodi *codi;

                codi=grup->GetChildCodi(i);
                wxString catwx=_wx(codi->GetCat());
                if ((CheckPrivLevel(codi->GetUpl())==true) && (config.IsCat(catwx)==true)){
                    unsigned int tzona=0;

                    int znum=0;
                    wxString codiid=codi->GetId();
                    if (codiid.Contains("sensorunits")){
                        string gid2=codiid.c_str();
                        string num=(string)&gid2[11];
                        znum=sToI(num);
                        if (znum>panelActiveInputs){
                            continue;
                        }
                    }else if (codiid.Contains("sensormaxmin")){
                        string gid2=codiid.c_str();
                        string num=(string)&gid2[12];
                        znum=sToI(num);
                        if (znum>panelActiveInputs){
                            continue;
                        }
                    }else if (codiid.Contains("sensorscale")){
                        string gid2=codiid.c_str();
                        string num=(string)&gid2[11];
                        znum=sToI(num);
                        if (znum>panelActiveInputs){
                            continue;
                        }
                    }


                    panelEdit->GenPanelCodi(codi,this,tzona,privlevel);
                }
            }

            if (gid.Contains("ZNA")){
                panelEdit->GenPanelCfgWireless(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageZAliasGroup,zone);
            }else if (gid.Contains("USR")){
                int user=-1;
                string gid2=gid.c_str();
                string num=(string)&gid2[3];
                user=sToI(num);
                panelEdit->GenPanelCfgWirelessUser(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageUAliasGroup,user);
            }else if (gid.Contains("O")){
                int out=-1;
                string gid2=gid.c_str();
                string num=(string)&gid2[4];
                out=sToI(num);
                panelEdit->GenPanelCfgWirelessOut(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageOAliasGroup,out);
            }

        //string sss = "Aqui surtira la llegenda";
        //panelLlegenda->SetLlegenda(sss);
        //panelLlegenda->Show(true);
    }
    else if (tipus == TREE_TIPUS_CFLASHGROUP) {
        CGroupFlash *grupFlash = TreeCtr.GetGroupFlash(id);
        string tit = grupFlash->GetDescr().c_str();
        panelEdit->SetTitle(tit);
        panelEdit->CreaPanelFlashGrup(grupFlash);
    }
    else if (tipus == TREE_TIPUS_CMODEL) {
        CModel *model = TreeCtr.GetModel(id);
        string descr = model->GetModel();
        panelEdit->SetTitle(descr);
    }
    else if (tipus == TREE_TIPUS_CSTATUS) {
        CStatus *estat = TreeCtr.GetStatus(id);
        string tit = Lang->GetAppMiss("MSG_DEF_STATE").c_str();
        panelEdit->SetTitle(tit);
        panelEdit->GenPanelStatus(estat, this,StorageZAliasGroup,StorageAAliasGroup,StorageOAliasGroup,modelSelect->GetVersio(),modelSelect->GetModel());
        refrescAuto = true;
    }
    else if (tipus == TREE_TIPUS_STSECTION) {
        int idSeccio = TreeCtr.GetSection(id);
        if (idSeccio == ID_SECTION_GEN_STATUS) {
            CStatus *estat = TreeCtr.GetStatus(id);
            CDevice device=modelSelect->GetDevice();
            estat->SetDev(device.GetId(),device.GetVersion(),device.GetDevInf().verdate ,device.GetDevInf().hard);
            string tit = Lang->GetAppMiss("MSG_DEF_GENERAL").c_str();
            panelEdit->SetTitle(tit);
            panelEdit->GenPanelStatus(estat, this,StorageZAliasGroup,StorageAAliasGroup,StorageOAliasGroup,modelSelect->GetVersio(),modelSelect->GetModel());
            refrescAuto = true;
        }
        else if (idSeccio == ID_SECTION_BEACON_STATUS) {
            CStatus *estat = TreeCtr.GetStatus(id);
            string tit = Lang->GetAppMiss("BEACONS").c_str();
            panelEdit->SetTitle(tit);
            panelEdit->GenPanelBeacons(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageZAliasGroup);
        }
        else if (idSeccio == ID_SECTION_MEM_PANEL) {
            CStatus *estat = TreeCtr.GetStatus(id);
            string tit = Lang->GetAppMiss("MEM").c_str();
            panelEdit->SetTitle(tit);
            panelEdit->GenPanelMem(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageZAliasGroup);
        }
        else if (idSeccio == ID_SECTION_AGRO_PANEL) {
            CStatus *estat = TreeCtr.GetStatus(id);
            string tit = Lang->GetAppMiss("AGRO").c_str();

            panelEdit->SetTitle(tit);
            panelEdit->GenPanelAgro(dades, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageZAliasGroup);
        }
        else if (idSeccio == ID_SECTION_ZW_STATUS) {
            CStatus *estat = TreeCtr.GetStatus(id);
            string tit = Lang->GetAppMiss("MSG_DEF_VIARADIO").c_str();
            panelEdit->SetTitle(tit);
            panelEdit->GenPanelWireless(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageZAliasGroup);
        }
        else if (idSeccio == ID_SECTION_UW_STATUS) {
            CStatus *estat = TreeCtr.GetStatus(id);
            string tit = Lang->GetAppMiss("MSG_DEF_VIARADIOUSER").c_str();
            panelEdit->SetTitle(tit);
            panelEdit->GenPanelWirelessUser(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageUAliasGroup);
        }
        else if (idSeccio == ID_SECTION_OW_STATUS) {
            CStatus *estat = TreeCtr.GetStatus(id);
            string tit = Lang->GetAppMiss("MSG_DEF_VIARADIOOUT").c_str();
            panelEdit->SetTitle(tit);
            panelEdit->GenPanelWirelessOut(estat, this,modelSelect->GetVersio(),modelSelect->GetModel(),StorageOAliasGroup);
        }
#ifdef ESTAT_GRAFIC
        else if (idSeccio == ID_SECTION_GRAPH_STATUS) {
            refrescAuto = true;
            CStatus *estat = TreeCtr.GetStatus(id);
            string tit = Lang->GetAppMiss("DEF_GRAFIC").c_str();
            panelEdit->SetTitle(tit);
            panelEdit->CreaPanelGrafic(estat, this, StorageZAliasGroup, StorageAAliasGroup, StorageOAliasGroup,modelSelect->GetVersio(),modelSelect->GetModel());

        }
#endif
    }
    else if (tipus == TREE_TIPUS_VERIFY){
        string tit = _T("VERIFY");
        panelEdit->SetTitle(tit);
        panelEdit->GenPanelVerify(this,modelSelect,modelSelect->GetVersio(),modelSelect->GetModel());
    }
    else if (tipus == TREE_TIPUS_VMODEM){
        string tit = _T("Virtual Modem");
        panelEdit->SetTitle(tit);
        panelEdit->GenPanelVModem(protocolNIOT,&config,modelSelect,modelSelect->GetVersio(),modelSelect->GetModel());
    }
    else if (tipus == TREE_TIPUS_VCOM){
        string tit = _T("Virtual Comport");
        panelEdit->SetTitle(tit);
        panelEdit->GenPanelVCOM(this,protocolNIOT,&config,modelSelect,modelSelect->GetVersio(),modelSelect->GetModel());
    }
    else if (tipus == TREE_TIPUS_EVENTS) {
        CEventList *lEvents = TreeCtr.GetEventList(id);
        string tit = Lang->GetAppMiss("MSG_DEF_EVENTS").c_str();
        panelEdit->SetTitle(tit);
        panelEdit->GenPanelEvents(this,lEvents,StorageZAliasGroup,StorageAAliasGroup,StorageUAliasGroup,StorageOAliasGroup,modelSelect->GetVersio(),modelSelect->GetModel());
    }
    else {
        panelEdit->Clear();
    }

}

CModel* Cnuvalink::GetModelSelec(wxTreeItemId &id)
{
    return TreeCtr.GetModel(id);
}

//Li diem a MyFrame quin menu ha de mostrar en funcio del que esta seleccionat
int Cnuvalink::FloatMnu(wxTreeItemId &id)
{
    int menu = MENU_FLOTANT_CAP;
    int tipus = TreeCtr.GetTypeCon(id);

    if ((tipus == TREE_TIPUS_CGROUP) || (tipus == TREE_TIPUS_CFLASHGROUP)) {
        menu = MENU_FLOTANT_GRUP;
    }
    else if (tipus == TREE_TIPUS_BASE) {
        menu = MENU_FLOTANT_INSTALACIO;
    }
    else if ( (tipus == TREE_TIPUS_CSTATUS) ||
        (tipus == TREE_TIPUS_STSECTION) ||
        (tipus == TREE_TIPUS_EVENTS) ) {
        menu = MENU_FLOTANT_REBRE;
    }
    else if (tipus == TREE_TIPUS_CMODEL) {
        menu = MENU_FLOTANT_MODEL;
    }
    return menu;
}

//Cada vegada que es produeix un error en la funcio Connect havia de fer
//tot el que hi ha aqui aixi que ho vaig agrupar en aquesta funcio
void Cnuvalink::CancelConnect()
{
    connecthandler.progress = 0;
    connecthandler.progressDlg = 0;
    connecthandler.Disconnect();
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    online=false;
    transmitting=false;
    conectant=false;
    delete progressDlg;
    progressDlg=0;
}

//Funcio que aplica les dades que hi ha a CConfig i a la propia instalacio
//a l'objecte connecthandler
bool Cnuvalink::ConfigConnection()
{
    string b;
    int ty = install.GetConnectionType();
    config.SetConnectionType(ty);
    bool res = connecthandler.SetType(ty);

    if ((ty == TYPE_RS485_CONNECTION)) {
        if (res){
            CData *dades = install.GetDataObj();
            if (ty == TYPE_RS485_CONNECTION){
                wxString sbaud=config.GetConnectionParam("baud");

                string ssbaud=sbaud.c_str();
                int baud=sToI(ssbaud);
                res = connecthandler.SetBaud(baud);
            }else{
                b=(string)config.GetConnectionParam("baud").c_str();
                res = connecthandler.SetBaud(sToI(b));
            }
        }

        b=(string)config.GetConnectionParam("bitsdata").c_str();
        if (res) res = connecthandler.SetBitsData(sToI(b));
        b=(string)config.GetConnectionParam("bitsstop").c_str();
        if (res) res = connecthandler.SetBitsStop(sToI(b));
        b=(string)config.GetConnectionParam("paritat").c_str();
        if (res) res = connecthandler.SetParity(sToI(b));
        b=(string)config.GetConnectionParam("portcom").c_str();
        if (res) res = connecthandler.SetPortCom(sToI(b));
    }
    if (ty == TYPE_USB_CONNECTION ) {
        // USB connexion always
    }
    else if (ty == TYPE_TCPIP_CONNECTION) {
        CData *dades = install.GetDataObj();
        if (res) res = connecthandler.SetDestIP(dades->gethost());
        if (res) res = connecthandler.SetIPPort(dades->getport());
    }
    else if (ty == TYPE_CLOUD_CONNECTION){
        //if (res) res = connecthandlerSetDestIP(config.GetParamTcpIp("ipclouddest").c_str());
        if (res) res = connecthandler.SetDestIP(config.GetSessionServer().c_str());
        string sport=config.GetParamTcpIp("ipcloudport").c_str();
        if (res) res = connecthandler.SetIPPort(sToI(sport));
        return true;
    }

    //Li preguntem al objecte conexio si te tots els paramtres de configuracio necessaris
    if (res) res = connecthandler.IsConfigured();

    return res;
}

// ---------------------------------------- Guarda els parametres que ens ha donat l'usuari
bool Cnuvalink::SetConnectionParams(int ty, int portCom, string& numTelf, bool callback, bool especial, string& host, int port, string& serverhost,int serverport)
{
    CData *dades = install.GetDataObj();
    if (!connecthandler.SetType(ty)) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_TYPE");
        return false;
    }

    dades->setconType(ty);
    config.SetConnectionType(ty);
    if (ty == TYPE_TCPIP_CONNECTION) {
        dades->sethost(host);
        dades->setport(port);
    }
    return true;
}

//Indica si l'objecte conexio te tots els parametres necessaris per Connect
bool Cnuvalink::Need2ConfigConnection()
{
    bool res = ConfigConnection();

    return !res;
}

bool Cnuvalink::AvailableComPort(int port)
{
    return connecthandler.AvailableComPort(port);
}

bool Cnuvalink::ParseConnectAnswer()
{
    if (!protocolNIOT->RxPacket(0xFF)) {
        msgError = protocolNIOT->GetLastError();
        return false;
    }
    // mirem la resposta de la central, sols en connexions remotes directes
    uint16_t l;
    unsigned char* dades = protocolNIOT->GetData(&l);
    // Retorna punter OFF_IDEN+1
    if (privlevel==PROD_PRIVLEVEL){
    }else{
        if (dades==0)
            return false;

        if (dades[0]==FALSE_ANSWER){
            if (privlevel>=ENG_PRIVLEVEL || connecthandler.GetTypeCon()==TYPE_USB_CONNECTION){
                wxMessageBox("WRONG Installer code");          // but continuing.
            }else{
                msgError = Lang->GetAppMiss("MISS_ERROR_CON_ESTABLISH");
                return false;
            }
        }else if (dades[0]==FALSE_ANSWER_L2){
            if (privlevel>=ENG_PRIVLEVEL){
                wxMessageBox("Level 3 not level 2 enabled!");          // but continuing.
            }else{
                msgError = Lang->GetAppMiss("MISS_ERROR_CON_ESTABLISH_L2");
                return false;
            }
        }else if (dades[0]==FALSE_ANSWER_L3){
            if (privlevel>=DEBUG_PRIVLEVEL){
                wxMessageBox("Level 4 not level 3 enabled!");          // but continuing.
            }else{
                msgError = Lang->GetAppMiss("MISS_ERROR_CON_ESTABLISH_L3");
                return false;
            }
        }
    }
    // -------------------------------------------- getting hard ID information -----------
    CDevice device;
    if (device.Construct(&dades[1],l-1)==false)
        return false;

    modelSelect->SetDevice(device);

    wxString found=_wx(device.GetDevInfo());
    int resultbox;
    wxString id=_wx(device.GetId());
    wxString ver=_wx(device.GetVersionFW());

    if (id!=modelSelect->GetModel()){
        if (privlevel>=ENG_PRIVLEVEL){
            if (noMessages==true)
                resultbox=2;
            else
                resultbox = wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_ASK_FOUND")+_wx(" ")+found+"\r\n"+Lang->GetAppMiss("MISS_DIALOG_ASK_EXPECTED")+_wx(modelSelect->GetModel())+"-"+_wx(modelSelect->GetVersio())+"\r\n"+Lang->GetAppMiss("MISS_DIALOG_ASK_NOMATCH"), Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);

            if (resultbox == 2) {//wxID_YES
                wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_ASK_SAVECLOSE"));
                modelSelect->SetModel(id);
                modelSelect->SetVersion(ver);
            }
        }else{         // standard access levels cannot change between models, only
            wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_ASK_FOUND")+_wx(" ")+found+"\r\n"+Lang->GetAppMiss("MISS_DIALOG_ASK_EXPECTED")+_wx(modelSelect->GetModel())+"-"+_wx(modelSelect->GetVersio())+"\r\n", Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"),wxICON_ERROR);
            msgError = "Model mismatch - Modelo no correponde";
            return false;
        }
    }else if (ver!=modelSelect->GetVersio()){
        resultbox = wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_ASK_FOUND")+_wx(" ")+found+"\r\n"+Lang->GetAppMiss("MISS_DIALOG_ASK_EXPECTED")+_wx(modelSelect->GetModel())+"-"+_wx(modelSelect->GetVersio())+"\r\n"+Lang->GetAppMiss("MISS_DIALOG_ASK_NOMATCH"), Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
        if (resultbox == 2) {//wxID_YES
            wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_ASK_SAVECLOSE"));
            modelSelect->SetVersion(ver);
        }
    }
    return true;
}

bool Cnuvalink::Connect()
{
    if (online)
        return true;

    connecthandler.FFT=false;

    CModel *central = install.GetControlPanel();
    modelSelect = install.GetControlPanel();

    if (!central) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_PANEL").c_str();
        return false;
    }
    // Configurem la conexio
    if (!ConfigConnection()) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_PARAMS").c_str();
        return false;
    }
    // Avisem a l'usuari que es far� conexi� CRA-LINK, cosa que pot arribar a confondre!!!

    //Som-hi!
    conectant=true;
    progress=0;
    int max=5;
    if (connecthandler.GetTypeCon() == TYPE_CLOUD_CONNECTION)
        max=250;        // Longest Connection.
    else if (connecthandler.GetTypeCon() == TYPE_TCPIP_CONNECTION)
        max=16;

    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_CONNECT"), Lang->GetAppMiss("MISS_DIALOG_TIT_CONNECT"),
    max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_ELAPSED_TIME | wxPD_SMOOTH);

    connecthandler.progressDlg = progressDlg;
    connecthandler.progress = &progress;
    online = connecthandler.Connect();
    if (!online){
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_START").c_str();
        msgError += connecthandler.GetLastError();
        CancelConnect();
    } else {
        // TCPSERVER a pesar de no establir, permet esperar la connexi�.
        if (!Progres(1, Lang->GetAppMiss("MISS_DIALOG_CON_ESTABLISH"))) {
            CancelConnect();
            return false;
        }
        if ((this->SN=="FFFFFFFF" || this->SN=="" || this->SN=="00000000" || this->SN=="null") && connecthandler.GetTypeCon()==TYPE_CLOUD_CONNECTION){
            this->SN=wxGetTextFromUser("Serial Number","Serial Number","");
        }
        protocolNIOT->SetSN(LE2BEHexSN(this->SN).c_str());
        protocolNIOT->Start(privlevel,0xFF,0);
        protocolNIOT->myLog = &myLog;
        string aeskey=install.GetCodiVal("6FA");
        if (aeskey=="                ")
            aeskey=CalcAppAes();

        protocolNIOT->SetAesKey(aeskey);            // obtenim la clau aes i la apliquem al aes.
        protocolNIOT->SetSubscriber(install.GetMainCodiAbonat());      // obtenim la clau aes i la apliquem al aes.
        protocolNIOT->UseAes(false);
        if (connecthandler.GetTypeCon() == TYPE_TCPIP_CONNECTION || connecthandler.GetTypeCon() == TYPE_CLOUD_CONNECTION
        ){     // all remote communications are ciphered.
            if (aeskey!="" && aeskey!="                ")
                protocolNIOT->UseAes(true);
        }

        if (config.GetConnectionParam("FFT")=="true")
            connecthandler.FFT=true;

        if (!online){
            // TCP Server solicitat per SMS, passem a esperar la connexi� entrant.
            delete progressDlg;
            progressDlg=0;
            progress=0;
            protocolNIOT->progressDlg = 0;
            protocolNIOT->progress = 0;
        }else{
            protocolNIOT->progressDlg = progressDlg;
            protocolNIOT->progress = &progress;
            transmitting=true;
                //Establiment a nivell de protocol (enviar codi d'instalador)
                //Si la central no ens accepta: online=false i connecthandlerDisconnect() penjar!
            CModel *c = install.GetControlPanel();
            if (privlevel==DEBUG_PRIVLEVEL && (connecthandler.GetTypeCon()==TYPE_RS485_CONNECTION || connecthandler.GetTypeCon()==TYPE_USB_CONNECTION))
                goto OpenSession;

            protocolNIOT->GenStablishPacket(privlevel, install.GetCodiInstalador());

            int tries=3;
            if (connecthandler.GetTypeCon()==TYPE_CLOUD_CONNECTION)
                tries=50;       // this yields to 50x4s = 200 sec.
            if (!protocolNIOT->SendPacket(tries)) {
                msgError = Lang->GetAppMiss("MISS_ERROR_CON_ESTABLISH_FAIL");
                msgError += protocolNIOT->GetLastError();
                CancelConnect();
                return false;
            }
            // if on FW do not show messages.
            noMessages=false;
            wxTreeItemId id = TreeCtr.Tree->GetSelection();
            int tipus = TreeCtr.GetTypeCon(id);
            if (tipus == TREE_TIPUS_CFLASHGROUP) {
                CGroupFlash *grupFlash = TreeCtr.GetGroupFlash(id);
                if (grupFlash->GetId() == "Update Firmware"){
                    noMessages=true;
                }
            }
            if (!ParseConnectAnswer()){
                CancelConnect();
                return false;
            }
        }
        // Enviem la data i l'hora de l'ordinador: NECESSARI PER AL CRA-LINK!!
OpenSession:
        IsChangeProg=false;
        transmitting=false;

        //Sleep(250);
        //SendDateTime();                       // Do not send datetime as clock is defined in Cloud TimeZone,
        /*if (!Progres(1, Lang->GetAppMiss("MISS_DIALOG_CON_CONNECTED"))) {
            CancelConnect();
            return false;
        }*/
        // Once stablish assign serial number if still not set, on non-cloud connections ( cloud needs this before start )
        if (this->SN=="FFFFFFFF" || this->SN=="00000000")
            this->SN=protocolNIOT->GetSN();

        delete progressDlg;

        progressDlg=0;
        progress=0;
        protocolNIOT->progressDlg = 0;
        protocolNIOT->progress = 0;
    }
    conectant=false;
    return online;
}


//Funcio Disconnect. Passi el que passi al final estarem desconectats. Si va be
//haurem desconectat sense problemes. Si va malament, la funcio donara error
//pero igualment estarem desconectats (online=false)
bool Cnuvalink::Disconnect()
{
    if (!online)
        return true;

    transmitting=true;

    CModel *central = install.GetControlPanel();

    if (protocolNIOT->NumError){

    }else{

        if ((noMessages==false && privlevel>=ENG_PRIVLEVEL && wxMessageBox( _wx("APPLY on exit?"), _wx("APPLY on exit?"), wxYES_NO | wxICON_QUESTION)!=wxYES)){
            IsChangeProg=false;
        }else{
            if (IsChangeProg){
                IsChangeProg=false;
                protocolNIOT->GenEventPacket(EVENT_PROG_CHANGE|M_EVENT_NEW,0xFF,0);
                protocolNIOT->SendPacket();
            }
            protocolNIOT->GenByePacket();
            if (protocolNIOT->NumError){

            }else{
                if (!protocolNIOT->SendPacket()) {
                    msgError = protocolNIOT->GetLastError();
                    online=false;
                    connecthandler.Disconnect();
                    transmitting=false;
                    return false;
                }
            }
        }
    }
    Sleep(500);
    protocolNIOT->Stop();    // inside it will stop the thread.
    online=false;
    transmitting=false;

    if (connecthandler.Disconnect()) {
        return true;
    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_STOP");
        msgError += connecthandler.GetLastError();
        return false;
    }

    return true;
}

//L'ultim missatge d'error sempre es guarda en msgError. Aixi quan el MyFrame
//en crida i li retornem error (false) cridara a aquesta funci� per mostrar
//l'error a l'usuari
string& Cnuvalink::GetLastError()
{
    return msgError;
}

//void Cnuvalink::TreeFloatMnu(wxTreeItemId &id)


//Enviem la data i hora del PC a la central
bool Cnuvalink::SendDateTime(unsigned char *datetime)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT");
        return false;
    }

    unsigned char data[6];
    if (datetime==0) {
        time_t rawtime;
        struct tm * timeinfo;

        //Posem la data i hora de l'ordinador a l'estructura data[]
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        data[0] = (unsigned char) timeinfo->tm_hour;
        data[1] = (unsigned char) timeinfo->tm_min;
        data[2] = (unsigned char) timeinfo->tm_sec;
        data[3] = (unsigned char) timeinfo->tm_mday;
        data[4] = (unsigned char) timeinfo->tm_mon+1;
        data[5] = (unsigned char) timeinfo->tm_year - 100; //any - 2000
    }
    else {
        for (int i=0; i<6; i++)
            data[i] = datetime[i];
    }
    //modelSelect ens diu a quin component del bus pertany el codi. Si no ho sabem
    //farem com si el codi fos de la central principal
    if (!modelSelect) modelSelect = install.GetControlPanel();
    string add="HOUR";
    CCodi *codi=install.GetCodiById(add);
    if (codi){
        protocolNIOT->GenSendCodiPacket(codi->GetAddress(),true,data,6,0);
        if (!protocolNIOT->SendPacket()) {
            msgError = protocolNIOT->GetLastError();
            TooMuchError(3);
            return false;
        }
    }
    protocolNIOT->GenOrderPacket(ORDER_CLR_ERRORTIME,NULL,0);
    if (!protocolNIOT->SendPacket()) {
        msgError = protocolNIOT->GetLastError();
        TooMuchError(3);
        return false;
    }
    return true;
}

//Envia un proces que fara que la central fagi un test telef�nico
bool Cnuvalink::SendReportTest()
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT");
        return false;
    }

    if (!modelSelect) modelSelect = install.GetControlPanel();

    protocolNIOT->GenReportTestPacket();

    if (!protocolNIOT->SendPacket()) {
        msgError = protocolNIOT->GetLastError();
        TooMuchError(3);
        return false;
    }

    return true;
}

//Transmet a la central el valor d'un camp
bool Cnuvalink::SendCodeValue(CCodi* codi,bool onlychanges)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT");
        return false;
    }
    if (codi->IsNotSend())
        return true;
    //modelSelect ens diu a quin component del bus pertany el codi. Si no ho sabem
    //farem com si el codi fos de la central principal
    if (!modelSelect) modelSelect = install.GetControlPanel();

    // ----------------------------- mirem privilegis per sobreescriure aquesta adre�a ------------------------------

    int longitud = codi->GetValLength();
    unsigned char *dades;
    dades = codi->GetVal();

    if (codi->GetStringIniVal()!=codi->GetStringVal()){
       // wxMessageBox(_wx(codi->GetStringIniVal()+"new:"+codi->GetStringVal()));
        codi->SetStringIniVal(codi->GetStringVal());
        IsChangeProg=true;
    }else if (onlychanges==true){
        return true;            // do not send as no change!
    }

    protocolNIOT->GenSendCodiPacket(codi->GetAddress(),  (codi->GetTypeConDir()==TIPUS_ADR_EEPROM), &dades[0], longitud, codi->GetOffset());
    if (!protocolNIOT->SendPacket()) {
        msgError = protocolNIOT->GetLastError();
        TooMuchError(3);
        return false;
    }
    string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_SEND").c_str();
    if (!Progres(0,appmiss+iToS(protocolNIOT->datatx)+" bytes")){
        return false;
    }

    if (!Progres(1,_wx("")))
        return false;

    return true;
}
// --------------------------------------------------------------- Enviar Valors seguits------
//Aquesta funcio envia tots els codis d'un mateix grup quan aquests van seguits
//en memoria
bool Cnuvalink::SendConsecutiveValues(CGroup* grup,bool onlychanges)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT");
        return false;
    }

    bool useeep2flashmap=false;
    int inctransaccions=1;

    //modelSelect ens diu a quina central pertany el codi. Si no ho sabem
    //farem com si el codi fos de la central
    if (!modelSelect) modelSelect = install.GetControlPanel();
    CModel *central = install.GetControlPanel();      // good to get central bus state.
    CStatus *estat = central->GetStatus();            // need to know if device is wireless
    CCodi *codiIni = grup->GetChildCodi(0);

    //Distancia (a la memoria) entre codi i codi
    int distancia;
    if (grup->GetDistancia() != 0)
        distancia = grup->GetDistancia();
    else
        distancia = codiIni->GetValLength();
    //Cada trama pot enviar com a molt 256 bytes.

    string sadr=codiIni->GetAddress();
    int adr=hToI(sadr);
    int lendata=connecthandler.GetMaxNIOTInfLen();

    int trames;
    int longitud = distancia * grup->GetNumChildCodi();
    trames = (longitud+lendata-1) / lendata;
    inctransaccions=(int)(grup->GetNumChildCodi() / trames);
    //Creem un array "valors" on posarem els valors de tots els codis guardant
    //la "distancia" adequada. De manera que l'array "valors" ser� com una c�pia
    //de la part de mem�ria que volem enviar a la central. Despres la enviarem
    //dividida en trames.
    unsigned char *dades;
    unsigned char *valors = new unsigned char [longitud];
    int k=0;
    bool ischangeseguits=false;
    int inioff=0;

    for (int i=0; i<grup->GetNumChildCodi(); i++) {
        CCodi *codi = grup->GetChildCodi(i);
        if (i==0)
            inioff=codi->GetOffset();
        if (codi->GetStringIniVal()!=codi->GetStringVal()){
            codi->SetStringIniVal(codi->GetStringVal());
            IsChangeProg=true;
            ischangeseguits=true;
        }
        dades = codi->GetVal();
        for (int j=0; j<codi->GetValLength(); j++)
            valors[k++] = dades[j];
        k += distancia - codi->GetValLength();
    }
    if ((ischangeseguits==true) || (onlychanges==false)){
        for (int i=0; i<trames; i++) {
            bool segueix;
            int intents=0;
            do {
                segueix=false;

                if (i < (trames-1))
                    protocolNIOT->GenSendCodiPacket( sadr, codiIni->GetTypeConDir()==TIPUS_ADR_EEPROM, &valors[i*lendata], lendata, inioff+i*lendata);
                else    //ultima trama
                    protocolNIOT->GenSendCodiPacket( sadr, codiIni->GetTypeConDir()==TIPUS_ADR_EEPROM, &valors[i*lendata], longitud-(i*lendata), inioff+i*lendata);

                if (!protocolNIOT->SendPacket()) {
                    if (protocolNIOT->EsPotReintentar() && intents<3) {
                        segueix=true;
                        intents++;
                    }
                    else {
                        msgError = protocolNIOT->GetLastError();
                        TooMuchError();
                        return false;
                    }
                }
            } while (segueix);

            string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_SEND").c_str();
            if (!Progres(inctransaccions/*grup->GetNumChildCodi()*/,appmiss+iToS(protocolNIOT->datatx)+" bytes"))
                return false;
        }
    }
    return true;
}
// ------------------------------------------------------- ReceiveCodeValue ----------
bool Cnuvalink::ReceiveCodeValue(CCodi* codi, bool verificar)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT");
        return false;
    }
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.

    rxcodi=true;
    //modelSelect ens diu a quina central pertany el codi. Si no ho sabem
    //farem com si el codi fos de la central
    if (!modelSelect) modelSelect = install.GetControlPanel();

    int espai = codi->GetValLength();
    bool segueix=false;
    int intents=0;
    do{
        protocolNIOT->GenRxCodiPacket( codi->GetAddress(), (codi->GetTypeConDir()==TIPUS_ADR_EEPROM) , espai, codi->GetOffset());

        if (protocolNIOT->SendPacket()) {
            uint16_t l, j;
            unsigned char *dades;
            unsigned char valors[MAX_LON_BYTES_CODI];

            for (int i=0;i<MAX_LON_BYTES_CODI;i++)
                valors[i]=0;            // precarga a null.
            j=0;
            int NumFrames=0;    // Compta paquets rebuts.
            while (protocolNIOT->RxPacket(0xFF)) {
                timerHello->Stop();
                timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
                //Omplir amb el valor que hem rebut
                dades = protocolNIOT->GetData(&l);
                //hem rebut una trama pero no es de dades
                if ((l<4) || dades==0) {
                    msgError = Lang->GetAppMiss("MISS_ERROR_CODI_VALUE");
                    rxcodi=false;
                    return false;
                }
                //Comprovem que l'adre�a es la que hem demanat
                string adr = codi->GetAddress();
                unsigned char caddress[4];
                while (adr.length()<8) adr = "0" + adr;

                for (int i=0; i<4; i++) {
                    caddress[3-i] = ascii2nibble(adr[i*2]);
                    caddress[3-i] <<= 4;
                    caddress[3-i] |= ascii2nibble(adr[(i*2)+1]);
                }
                //Si no hem rebut el codi que haviem demanat es que ens hem perdut algo pel cami i la conexio esta "dessincronitzada". Borrem la cuaRx i tornem
                //a demanar el codi anterior. No passa molt sovint pero pot desencadenar errors bastant greus (els valors estarien moguts).
                if ((dades[0] != caddress[0]) || (dades[1] != caddress[1]) || (dades[2] != caddress[2]) || (dades[3] != caddress[3])) {
                    if (++NumFrames<4){        // incrementem n�mero de trames rebudes
                        Sleep(100);             // Tornarem a escoltar (cridant a RxPacket)!
                    }else{
                        protocolNIOT->ClearRxQueue();
                        segueix=true;
                        intents++;
                        string r = "Address doesn't match";
                        myLog.MostraMsg(r);
                        msgError = Lang->GetAppMiss("MISS_ERROR_CODI_VALUE");
                        j=0;
                        break;
                    }
                }else{
                    for (int i=4; i<l; i++) {
                        valors[j++] = dades[i];
                        if (j >= espai) break;
                    }
                    if (j >= espai) break;
                }
            }
            if (j) {
                if (verificar) {
                    if ((CheckPrivLevel(codi->GetUpl())==true) && (!codi->VerifyVal(valors))) {
                        msgError = Lang->GetAppMiss("MISS_ERROR_VERIFY").c_str() + codi->GetKpAddress();
                        msgError += "\n";
                        msgError += Lang->GetAppMiss("MISS_ERROR_VERIFY_INFO").c_str();
                        rxcodi=false;
                        return false;
                    }
                } else {
                    codi->SetVal(valors);
                }
                intents++;
                string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_RECV").c_str();
                if (!Progres(1,appmiss+iToS(protocolNIOT->datarx)+" bytes")){
                    rxcodi=false;
                    return false;
                }
                if (j >= espai) break;
            } else {
                if (UserCancel()){
                    rxcodi=false;
                    return false;
                }

                if ((protocolNIOT->EsPotReintentar()) && (intents<3)) {
                    intents++;
                    string r = "retry no data";
                    myLog.MostraMsg(r);
                    segueix=true;
                } else if ((!segueix) || (intents>3)) {
                    msgError = protocolNIOT->GetLastError();
                    rxcodi=false;
                    return false;
                }
            }
        } else {
            TooMuchError(3);
            if (UserCancel())
                msgError = protocolNIOT->GetLastError();

            rxcodi=false;
            return false;
        }
    }while (intents<3);
    rxcodi=false;
    return true;
}
// ----------------------------------------------------------- SendGroupValues ------------
//Funcio recursiva. Es crida a si mateixa per tants grups fills com hi hagi en el grup fins que hi hagi codis. Quan hi ha codis crida a SendValuesCodi per
//cada codi o a SendConsecutiveValues
bool Cnuvalink::SendGroupValues(CGroup* grup,bool onlychanges)
{
    if ((grup->DontSend()) && (privlevel<DEBUG_PRIVLEVEL))
        return true;

    grupSelect = grup;

    wxString grid=grup->GetId();
    // do not send zones that are not enabled.
    if (grid.Contains("ZNA")){

        string gid2=grid.c_str();
        string num=(string)&gid2[3];

        int input=sToI(num);

        if (input>panelActiveInputs)            // do not generate then
            return true;
    }

    if (grup->FollowCodis()) {
        return SendConsecutiveValues(grup,onlychanges);
    }
    else {
        for (int i=0; i<grup->GetNumChildCodi(); i++) {
            CCodi* codi=grup->GetChildCodi(i);
            if (CheckPrivLevel(grup->GetChildCodi(i)->GetUpl())==true){
                wxString codiid=codi->GetId();
                int znum;
                if (codiid.Contains("sensorunits")){
                    string gid2=codiid.c_str();
                    string num=(string)&gid2[11];
                    znum=sToI(num);
                    if (znum>panelActiveInputs){
                        continue;
                    }
                }else if (codiid.Contains("sensormaxmin")){
                    string gid2=codiid.c_str();
                    string num=(string)&gid2[12];
                    znum=sToI(num);
                    if (znum>panelActiveInputs){
                        continue;
                    }
                }else if (codiid.Contains("sensorscale")){
                    string gid2=codiid.c_str();
                    string num=(string)&gid2[11];
                    znum=sToI(num);
                    if (znum>panelActiveInputs){
                        continue;
                    }
                }

                if (!SendCodeValue(grup->GetChildCodi(i),onlychanges))
                    return false;
            }
        }
    }

    for (int i=0; i<grup->GetNumChildGroup(); i++) {
        if (!SendGroupValues(grup->GetChildGroup(i),onlychanges))
            return false;
    }

    grupSelect = 0;
    return true;
}

// --------------------------------------------------------     Seguits ------------------
bool Cnuvalink::ReceiveConsecutiveValues(CGroup *grup, bool verificar)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    //modelSelect ens diu a quina central pertany l'estat. Si no ho sabem
    //farem com si el codi fos de la central
    if (!modelSelect) modelSelect = install.GetControlPanel();
    CCodi *codiIni = grup->GetChildCodi(0);

    int numCodis = grup->GetNumChildCodi();
    int valLength = codiIni->GetValLength();

    int distancia;
    if (grup->GetDistancia() > 0)
        distancia = grup->GetDistancia();
    else
        distancia = valLength;

    //espai en bytes que ocupen tots les codis del grup
    int espai = numCodis*distancia;
    //TODO: caldria mirar si espai>255. En aquest cas s'hauria de demanar per parts
    int totespai=espai;
    int codiini=0;
    int numcodisRx=numCodis;

    string IniAddress=codiIni->GetAddress();

    int intents=0;
    do {
        if (espai>128)
            espai=128;           // es rep en blocks de 128 bytes max

        if (espai>totespai)
            espai=totespai;

        numcodisRx=espai/distancia;
        protocolNIOT->GenRxGroupPacket( IniAddress, true, espai, codiIni->GetOffset());

        if (protocolNIOT->SendPacket()){
            uint16_t l, j;
            unsigned char *dades;
            //Potser rebem els valors en mes d'una trama, per aixo fem un while
            j=0;
            int partiallen=0;
            unsigned char *valors = new unsigned char [espai];
waitagain:
            while (protocolNIOT->RxPacket(0xFF)) {
                //Omplir amb el valor que hem rebut - Progres(numCodis,appmiss+iToS(protocolNIOT->datarx)+" bytes"))
                intents>>=1;                    // reduce errors!
                timerHello->Stop();
                timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
                dades = protocolNIOT->GetData(&l);
                for (int i=4; i<l; i++) {
                    valors[j++] = dades[i];
                    if (j >= espai) break;
                }
                if (j >= espai) break;

                string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_RECV").c_str();
                int cnt=0;
                if ((l+partiallen)>=distancia){
                    cnt=(l+partiallen)/distancia;
                    partiallen=0;
                    if (!Progres(cnt,appmiss+iToS(protocolNIOT->datarx)+" bytes"))
                        return false;
                }else{
                    partiallen=l;           // this is the case of receiving alias with 8 bytes while codi is 16 lenght
                }
            }
            if (j >= espai) {
                for (int i=codiini; i<(codiini+numcodisRx); i++) {
                    CCodi *codi = grup->GetChildCodi(i);
                    if (codi) {
                        if (verificar) {
                            if ((CheckPrivLevel(codi->GetUpl())==true)  && (!codi->VerifyVal(&valors[i*distancia]))) {
                                msgError = Lang->GetAppMiss("MISS_ERROR_VERIFY").c_str() + codi->GetKpAddress();
                                msgError += "\n";
                                msgError += Lang->GetAppMiss("MISS_ERROR_VERIFY_INFO").c_str();
                                delete [] valors;
                                return false;
                            }
                        }
                        else {
                            codi->SetVal(&valors[(i-codiini)*distancia]);
                        }
                    }
                }
                totespai-=espai;
                IniAddress=iToH(hToI(IniAddress)+espai);
                codiini+=numcodisRx;
            }

            if ( j< espai) {
                if (UserCancel()){
                    delete [] valors;
                    return false;
                }
                if (protocolNIOT->EsPotReintentar() && intents<3) {
                    intents++;
                } else {
                    msgError = protocolNIOT->GetLastError();
                    delete [] valors;
                    return false;
                }
            }
        } else {
            if (UserCancel())
                return false;


            if (protocolNIOT->EsPotReintentar() && intents<3) {
                intents++;
            } else {
                TooMuchError(3);
                msgError = protocolNIOT->GetLastError();
                return false;
            }
        }
    } while (totespai>0);

    return true;
}
// ----------------------------------------------------- ReveiveGroupValues ------------------
bool Cnuvalink::ReveiveGroupValues(CGroup* grup, bool verificar)
{
    if (grup->DontRx())
        return true;

    grupSelect = grup;

    if (grup->FollowCodis()) {
        return ReceiveConsecutiveValues(grup, verificar);
    }
    else {
        for (int i=0; i<grup->GetNumChildCodi(); i++) {
            if (!ReceiveCodeValue(grup->GetChildCodi(i), verificar))
                return false;
        }
        wxString gid=_wx(grup->GetId());
        int zone=0;
        if (gid.Contains("ZNA")){
            string gid2=gid.c_str();
            string num=(string)&gid2[3];
            zone=sToI(num);
            panelEdit->ReadPanelVR();
        }else if (gid.Contains("USR")){
            int user=-1;
            string gid2=gid.c_str();
            string num=(string)&gid2[3];
            user=sToI(num);
            //wxMessageBox(_wx(iToS(zone)));
            panelEdit->ReadPanelUserVR();
        }else if (gid.Contains("SLDA")){
            int out=-1;
            string gid2=gid.c_str();
            string num=(string)&gid2[3];
            out=sToI(num);
            //wxMessageBox(_wx(iToS(zone)));
            panelEdit->ReadPanelOutVR();
        }
    }

    for (int i=0; i<grup->GetNumChildGroup(); i++) {
        if (!ReveiveGroupValues(grup->GetChildGroup(i), verificar))
            return false;
    }

    grupSelect = 0;
    return true;
}

// ini Msk struct from file values, because device won't send 0xFF blocks ( useful for alias )
bool Cnuvalink::IniRawMsk(CGroup* group,int maxadd)
{
    CCodi* codi;
    int off;
    string sadd;
    unsigned char valors[256];

    for (int i=0; i<group->GetNumChildCodi(); i++) {
        codi = group->GetChildCodi(i);
        sadd=codi->GetAddress();
        if (sadd!=""){
            off=hToI(sadd);
            if (off<maxadd){
                memcpy(&RawMsk[off],codi->GetVal(),codi->GetValLength());
            }else{
            }
        }else{
        }
    }
    for (int j=0; j<group->GetNumChildGroup(); j++) {
        CGroup* childgrup=group->GetChildGroup(j);
        for (int i=0; i<childgrup->GetNumChildCodi(); i++) {
            codi = childgrup->GetChildCodi(i);
            sadd=codi->GetAddress();
            if (sadd!=""){
                off=hToI(sadd);
                if (off<maxadd){
                    memcpy(&RawMsk[off],codi->GetVal(),codi->GetValLength());
                }else{
                }
            }else{
            }
        }
    }
    return true;
}

// ------------------------------------------------------- UpdateGrupFromMem--------
bool Cnuvalink::UpdateGrupFromMem(CGroup* grup, unsigned char *valors, int maxadd)
{
    CCodi* codi;
    grupSelect = grup;
    int off;
    string sadd;

    for (int i=0; i<grup->GetNumChildCodi(); i++) {
        codi = grup->GetChildCodi(i);
        sadd=codi->GetAddress();
        if ((sadd!="") && (codi->GetTypeConDir()==TIPUS_ADR_EEPROM)){
            off=hToI(sadd);
            if (off<maxadd){
                codi->SetVal(&valors[off]);
            }else{
            }
        }else{
        }
    }
    for (int i=0; i<grup->GetNumChildGroup(); i++) {
        if (!UpdateGrupFromMem(grup->GetChildGroup(i),valors,maxadd))
            return false;
    }

    grupSelect = 0;
    return true;
}

// ---------------------------------------------- ReveiveGroupValuesFlash -------------------

bool Cnuvalink::ReveiveGroupValuesFlash(CGroupFlash *grupFlash, bool verificar)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    //modelSelect ens diu a quina central pertany l'estat. Si no ho sabem
    //farem com si el codi fos de la central
    if (!modelSelect) modelSelect = install.GetControlPanel();
    int numElements = grupFlash->GetNumElements();
    int bytesElement = grupFlash->GetBytesElement();

    //espai en bytes que ocupa tota la flash que volem demanar
    int count = numElements*bytesElement;
    int intents=0;

    protocolNIOT->GenRxFlashPacket(grupFlash->GetAddress(), count);
    if (!protocolNIOT->SendPacket()){
        msgError = protocolNIOT->GetLastError();
        TooMuchError();
        return false;
    }

    uint16_t l, j=0;
    unsigned char *dades;
    unsigned char *valors = new unsigned char [count];
    int baseadd=hToI(grupFlash->GetAddress());

    while (j<count) {
        timerHello->Stop();
        timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
        if (protocolNIOT->RxPacket(T_NIOT_PKT_FLASH)){
            intents=0;
            //Omplir amb el valor que hem rebut
            dades = protocolNIOT->GetData(&l);
            int offset=protocolNIOT->GetDataAddress()-baseadd;
            j=offset;
            for (int i=4; i<l; i++) {
                valors[j++] = dades[i];
                if (j >= count)
                    break;
            }
            if (j >= count){
                break;
            }
            string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_RECVFLASH").c_str();
            if (!Progres((l/8),appmiss+iToS(protocolNIOT->datarx)+" bytes")){
                break;
            }
        }else{
            if (++intents>=4){
                break;
            }
        }
    }
    //Potser rebem els valors en mes d'una trama, per aixo fem un while
    if (j >= count) {
        for (int i=0; i<numElements; i++) {
            if (verificar) {
                if (!grupFlash->VerificaElement(i,&valors[i*bytesElement])) {
                    delete [] valors;
                    msgError = Lang->GetAppMiss("MISS_ERROR_VERIFY").c_str() + iToS(i);
                    msgError += "\n";
                    msgError += Lang->GetAppMiss("MISS_ERROR_VERIFY_INFO").c_str();
                    return false;
                }
            } else {
                grupFlash->SetElement(i,&valors[i*bytesElement]);
            }
        }
    }else{        //Hem rebut una part o res pero ens hem quedat a mitges (timeout segurament)
        delete [] valors;
        msgError = protocolNIOT->GetLastError();
        return false;
    }

    delete [] valors;

    return true;
}

bool Cnuvalink::SendValuesModel(CModel *model)
{
    return SendGroupValues(model->GetFatherGroup(),false);
}

bool Cnuvalink::ReceiveValuesModel(CModel *model)
{
    return ReveiveGroupValues(model->GetFatherGroup());
}


// ------------------------------------------------------------------- Rx Flash address to Flash ----
// Llegeix count bytes de la flash/ram en certa adre�a, i ho guarda a un fitxer .flh
bool Cnuvalink::TxMemBlock(int count, string add, unsigned char* valors,bool isRam)
{
    // Generar petici�.
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    if (!modelSelect) modelSelect = install.GetControlPanel();

    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.

    transmitting=true;
    progress=0;
    protocolNIOT->datarx=0;

    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_RECV"), Lang->GetAppMiss("MISS_DIALOG_TIT_RECV")+_wx(iToS(protocolNIOT->datarx)),
    count, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;

    //Buidem la cua per no confondrens amb alguna trama d'alguna transmissio anterior
    protocolNIOT->ClearRxQueue();
    //modelSelect ens diu a quina central pertany l'estat. Si no ho sabem farem com si el codi fos de la central
    bool success=false;
    int tpkt=T_NIOT_PKT_FLASH;
    if (isRam)
        tpkt=T_NIOT_PKT_RAM;

    protocolNIOT->GenSendMemPacket(add,isRam,valors,count,0);
    int baseadd=hToI(add);
    if (protocolNIOT->SendPacket()){
        transmitting=false;
        success=false;
    }

    delete progressDlg;
    progressDlg=0;
    progress = 0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return success;
}

// ------------------------------------------------------------------- Rx Flash address to Flash ----
// Llegeix count bytes de la flash/ram en certa adre�a, i ho guarda a un fitxer .flh
bool Cnuvalink::RxMemBlock(int count, string add, unsigned char* valors,bool isRam)
{
    // Generar petici�.
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    if (!modelSelect) modelSelect = install.GetControlPanel();

    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.

    transmitting=true;
    progress=0;
    protocolNIOT->datarx=0;

    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_RECV"), Lang->GetAppMiss("MISS_DIALOG_TIT_RECV")+_wx(iToS(protocolNIOT->datarx)),
    count, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;

    //Buidem la cua per no confondrens amb alguna trama d'alguna transmissio anterior
    protocolNIOT->ClearRxQueue();
    //modelSelect ens diu a quina central pertany l'estat. Si no ho sabem farem com si el codi fos de la central
    bool success=false;
    int tpkt=T_NIOT_PKT_FLASH;
    if (isRam)
        tpkt=T_NIOT_PKT_RAM;

    protocolNIOT->GenRxMemPacket(add, isRam,count,0);
    int baseadd=hToI(add);
    if (!protocolNIOT->SendPacket()){
        msgError = protocolNIOT->GetLastError();
        TooMuchError();
        transmitting=false;
        success=false;
    }else{
        uint16_t l, j;
        uint16_t lastoffset=0;
        uint16_t progresslen;
        int intents=0;
        unsigned char *dades;
        //Potser rebem els valors en mes d'una trama, per aixo fem un while
        j=0;
        while (j<count) {
            timerHello->Stop();
            timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.

            if (protocolNIOT->RxPacket(tpkt)){
                intents=0;
                //Omplir amb el valor que hem rebut

                // Device will not send complete0xFF blocks, so we must apply to proper offsets.
                dades = protocolNIOT->GetData(&l);
                int offset=protocolNIOT->GetDataAddress()-baseadd;
                progresslen=offset-lastoffset;
                j=offset;
                for (int i=4; i<l; i++) {
                    valors[j++] = dades[i];
                    if (j >= count)
                        break;
                }
//                dades = protocolNIOT->GetData(&l);
//                for (int i=4; i<l; i++) {
//                    valors[j++] = dades[i];
//                    if (j >= count)
//                        break;
//                }
                if (j >= count){
                    success=true;
                    break;
                }
                string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_RECV").c_str();
                if (!Progres(progresslen,appmiss+iToS(protocolNIOT->datarx)+" bytes")){
                    break;
                }
                lastoffset=offset;
            }else{
                if (++intents>=4){
                    break;
                }
            }
        }
    }

    delete progressDlg;
    progressDlg=0;
    progress = 0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return success;
}

int Cnuvalink::RequestBleDevSt(unsigned char* buffer)
{
    int beaconlen=0;

    if (online){
        timerHello->Stop();
        timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.

        transmitting=true;
        protocolNIOT->datarx=0;

        protocolNIOT->GenPacketGetBeaconSt();
        if (protocolNIOT->SendPacket()){
            while (protocolNIOT->RxPacket(T_NIOT_PKT_RPC)){
                uint16_t l;
                unsigned char* dades = protocolNIOT->GetData(&l);
                while (l>0){
                    buffer[beaconlen++]=*dades++;
                    l--;
                }
                string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_RECV").c_str();
                if (!Progres(l,appmiss+iToS(protocolNIOT->datarx)+" bytes")){
                    break;
                }
            }
        }
        transmitting=false;
    }
    return beaconlen;

}


// ------------------------------------------------------------------- Rx Flash address to Flash ----
// Envia count bytes a la flash en certa adre�a, desde un fitxer .flh
bool Cnuvalink::TxFlashFile(int count, string address, unsigned char* valors)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    if (transmitting){
        Sleep(2000);
        if (transmitting){
            msgError=Lang->GetAppMiss("MISS_ERROR_AUT_PROCES").c_str();
            transmitting=false;
            return false;
        }
    }
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    transmitting=true;
    //modelSelect ens diu a quina central pertany el grup. Si no ho sabem farem com si el codi fos de la central
    CModel* central=install.GetControlPanel();
    CStatus* estat=central->GetStatus();
    if (!modelSelect) modelSelect = install.GetControlPanel();

    // Cada trama pot enviar com a molt 16 bytes o 256
    int trames;
    int sizef;
    int adr;

    faddress=address;
    adr=hToI(address);
    if (connecthandler.GetTypeCon() == TYPE_USB_CONNECTION){
        trames = (count+31) / 32;
        sizef=32;
    }else{
        trames = (count+255) / 256;
        sizef = 256;
    }
//    adr<<=4;            // 32 bits address are not using 16bytes to cluster address.
    progress=0;
    protocolNIOT->datatx=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"), Lang->GetAppMiss("MISS_DIALOG_TIT_SEND")+_wx(iToS(protocolNIOT->datatx)),
    trames, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;

    bool success=true;
    for (int i=0; i<trames; i++) {
        timerHello->Stop();
        timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
        address = iToH(adr+(i*sizef));
        bool segueix;
        int intents=0;
        do {
            segueix=false;
            protocolNIOT->GenTxFlashPacket(address, &valors[i*sizef],sizef,true);

            if (!protocolNIOT->SendPacket()) {
                if (protocolNIOT->EsPotReintentar() && intents<3) {
                    segueix=true;
                    intents++;
                } else {
                    msgError = protocolNIOT->GetLastError();
                    TooMuchError();
                    delete progressDlg;
                    progressDlg=0;
                    progress = 0;
                    protocolNIOT->progressDlg = 0;
                    protocolNIOT->progress = 0;
                    transmitting=false;
                    return false;
                }
            }
        } while (segueix);
        string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_SEND").c_str();
        if (!Progres(1,appmiss+iToS(protocolNIOT->datatx)+" bytes")){
            success=false;
            break;
        }
    }
    delete progressDlg;
    progressDlg=0;
    progress = 0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return success;
}

bool Cnuvalink::SendAsciiFrame(string frame,int len)
{
    protocolNIOT->SendAsciiFrame(frame,len);
}

// -------------------------------------------------------------------- TcPacket ---------------------
bool Cnuvalink::TxPacket(unsigned char tpkt,unsigned char* frame,int len){
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }

    protocolNIOT->GenCustomPacket(tpkt,frame, len);
    if (!protocolNIOT->SendPacket()) {
        msgError = protocolNIOT->GetLastError();
        TooMuchError();
        transmitting=false;
        return false;
    }
    transmitting=false;
    return true;
}
// ---------------------------------------------------------- Check Flash file signatures -------------
bool Cnuvalink::CheckFlashFile(CGroupFlash *grupFlash,CFitxerFlash *fitxer,bool show,bool WORDCHECK)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    if (!modelSelect) modelSelect = install.GetControlPanel();
    // --------------------------------------------- Read device ---------------------------
    CDevice device=modelSelect->GetDevice();

    wxString miss=device.GetDevInfo()+"\r\n";

    if (grupFlash->GetId() == "Update Firmware" && noMessages==false){
        int resultbox=wxMessageBox(miss, _wx("Confirm?"), wxYES_NO | wxICON_QUESTION);
        if (resultbox != 2)
            return false;

        if (device.GetFwDate()=="29/10/20"){
            wxMessageBox("Cannot update 291020");
            return false;
        }
    }
    flongitud = fitxer->GetBytesFitxer();
    unsigned char *dades = fitxer->GetFile();
    faddress = grupFlash->GetAddress();           // The file has no addrbase parameter so address="0"
    int poscheck=0;
    int numchecks=flongitud>>12;
    fchecksum=0;

    int step=1;
    if (WORDCHECK)
        step=2;

    for (int i=0;i<flongitud;i+=step){
        if (!(i&0x0FFF))
            cursectorchecksum=0;
        if (WORDCHECK){
            // BOOTLOADER 010520 - calculates checksum as WORD + BYTE sum
            //for (int j=0;j<4;j++){
                WORD_VAL wb;
                wb.v[0]=dades[i];
                wb.v[1]=dades[i+1];
                fchecksum+=wb.Val+wb.v[0]+wb.v[1];

                cursectorchecksum+=wb.Val+wb.v[0]+wb.v[1];
            //}
        }else{
            fchecksum+=dades[i];                // We have calculated the checksum file.
            cursectorchecksum+=dades[i];
        }
        int kk=0;
        ArrayFwCheckSums[i>>12]=cursectorchecksum;
    }

    if (grupFlash->GetId() == "Update Firmware"){
        faddress=iToH(device.GetUpdFwAddress());
    }
    unsigned int adr = hToI(faddress);
    adr += fitxer->GetSector()*4096;     // adr apunta a cluster de 16 bytes.

    if (grupFlash->GetId() == "Update Firmware"){
        protocolNIOT->GenPacketGetFlashSignatures(iToH(adr),flongitud>>12);
        if (protocolNIOT->SendPacket(1,true)){                              // SINGLE TRY and long timeout wait.
            while (protocolNIOT->RxPacket(T_NIOT_PKT_RPC)){
                uint16_t l;
                unsigned char* dades = protocolNIOT->GetData(&l);
                uint16_t* signatures=(uint16_t*)dades;
                while (l>0){
                    ArrayFwSignatures[poscheck++]=*signatures++;
                    l-=2;
                }
                if (poscheck>=numchecks)
                    break;
            }
        }
        transmitting=false;
        if (show==true && noMessages==false){
            FlashChecksumDlg* dlg=new FlashChecksumDlg(this->parent,wxID_ANY,wxDefaultPosition,wxDefaultSize);
            dlg->ShowData(adr,ArrayFwCheckSums,ArrayFwSignatures,poscheck);
            if (dlg->ShowModal() == wxID_OK){
                return true;
            }else{
                return false;
            }
        }
    }
    return true;
}
// ---------------------------------------------------------- Enviar Fitxer Flash a grupFlash ---------
bool Cnuvalink::SendFlashFile(CGroupFlash *grupFlash, CFitxerFlash *fitxer)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    if (!modelSelect) modelSelect = install.GetControlPanel();
    CDevice device=modelSelect->GetDevice();

    bool WORDCHECK=false;

    if (device.GetIntBootVersionFW()>=212){
        WORDCHECK=true;
    }

    bool show=true;
    if (noMessages==true)
        show=false;

    if (CheckFlashFile(grupFlash,fitxer,show,WORDCHECK)==false)
        return false;

    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.

    // --------------------------------------------- Read device ---------------------------

    int normalsize= fitxer->GetLon()*4096;
    unsigned char *dades = fitxer->GetFile();
    if (fitxer->GetFileExt()=="wav"){
        dades[flongitud]=0;                      // final 0 of the file, important for audio files.
        flongitud++;
    }
    string Filename= fitxer->GetNomFitxer();

    if ((flongitud>normalsize) || (dades==0)){
        wxMessageBox("BAD FILE:No data or too large",Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        transmitting=false;
        return true;
    }
    if (Filename!=LastFlashFile)        // canvi de fitxer, sempre desde inici don�
        FFTSentPtr=0;
    LastFlashFile=Filename;
    string ext=fitxer->GetFileExt();

    if (!strstr((char*)&Filename[0],(char*)&ext[0])) {
        wxMessageBox(_wx("BAD FILE:File extension is not ."+fitxer->GetFileExt()+"\n"+Filename),Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        transmitting=false;
        return true;
    }else if (ext=="bin"){
        // -------------------------------- check si firmware correspon al dispositiu ( engineer via lliure ) --------------
        if ((privlevel<DEBUG_PRIVLEVEL) && (device.CheckFirmwareFile(Filename,msgError)==false)){
            transmitting=false;
            return false;
        }
    }
    int trames, sizef, maskf;
    sizef=connecthandler.GetMaxNIOTInfLen();


//    wxString newsize=wxGetTextFromUser("transfer size","tranfer size 64,128,256?",iToS(sizef));
//    string newsizes=newsize.c_str();
//    int ins=sToI(newsizes);

//    if (ins<=256)
//        sizef=ins;
//wxString ress=wxGetTextFromUser("slow resend?","slow resend?",0);
 //   string sres=ress.c_str();
  //  int resendtrue=sToI(sres);


    trames = (flongitud + sizef-1) / sizef;
    if (sizef==32)
        maskf=0x007F;
    else if (sizef==64)
        maskf=0x003F;
    else if (sizef==128)
        maskf=0x001F;
    else                // this must be 256!
        maskf=0x000F;

    int ini=0;
    unsigned int adr = hToI(faddress);
    adr += fitxer->GetSector()*4096;     // adr apunta a byte
    ini=FFTSentPtr;                     // guardem punt d'inici anterior.
    bool inisector=false;
    bool blockisff=false;
    bool rsynch=false;
    int rxadd;
    int curadd;
    int progressinc=1;

    string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_SEND").c_str();
    Progres(FFTSentPtr,appmiss+iToS(protocolNIOT->datatx)+" bytes");      // progres a partir de FFTSentPtr.

    for (int i=ini; i<trames; i++) {
        timerHello->Stop();
        timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
        progressinc=1;
        faddress = iToH(adr+(i*sizef));
        inisector=false;
        if (!(i&maskf)){
            inisector=true;
        }
        blockisff=true;
        for (int c=(i*sizef);c<((i+1)*sizef);c++){
            if (dades[c]!=0xFF){
                blockisff=false;
                break;
            }
        }
        bool segueix;
        int intents=0;
        do {
            segueix=false;

            if ((blockisff==true) && (inisector==false)){
                protocolNIOT->datatx+=sizef;
            }else if ((grupFlash->GetId() == "Update Firmware") && (inisector == true) && (ArrayFwCheckSums[(i*sizef)>>12]==ArrayFwSignatures[(i*sizef)>>12])){
                protocolNIOT->datatx+=4096;
                i+=(4096/sizef)-1;      // -1 because the for will apply a post increment.
                progressinc=(4096/sizef);
            }else{
                if (i < (trames-1)){
                    protocolNIOT->GenTxFlashPacket( faddress, &dades[i*sizef],sizef,true);
                }else{           //ultima trama
                    protocolNIOT->GenTxFlashPacket( faddress, &dades[i*sizef], flongitud-(i*sizef),true);
                }
                bool longwait=false;
 //               if (connecthandlerGetTypeCon()==TYPE_CLOUD_CONNECTION && resendtrue==1)
  //                  longwait=true;

                if (!protocolNIOT->SendPacket(2,longwait)){
                    if (intents==0) {
                        segueix=true;
                        intents++;
                    } else {
                        msgError = protocolNIOT->GetLastError();
                        TooMuchError();
                        transmitting=false;
                        return false;
                    }
                }else{
                    if (inisector==true){         // cada sector enviat es guarda a FFSentPtr
                        FFTSentPtr=i;
                    }
                }
            }
            if (UserCancel()){
                return false;
            }

        } while (segueix);
        string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_SEND").c_str();
        if (!Progres(progressinc,appmiss+iToS(protocolNIOT->datatx)+" bytes")){
            transmitting=false;
            return false;
        }
/*        if ((connecthandlerGetTypeCon()==TYPE_RS485_CONNECTION) || (connecthandlerGetAvTimeOut()>700)){
            // en local deixem un forat per tal que els altres pugui parlar, en GPRS/CSD ajuda a resincronitzar a inici de bloc
            if ((sizef==256) && (!(i&0x07))){
                Sleep(250);
            }else if (!(i&0x3F)){
                Sleep(250);
            }
        }*/
    }

    SoConnect.Stop();
    SoConnect.Play( _wx("tada.wav") , wxSOUND_ASYNC );

    FFTSentPtr=0;           // FINISH

    int resultbox;

    if (grupFlash->GetId() == "Update Firmware"){
        if (noMessages==true)
            resultbox=2;
        else
            resultbox = wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_ASK_FWUPDATE"), Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);

        if (resultbox == 2/*wxYES*/) {
            protocolNIOT->GenFwActionPacket(fchecksum,FW_ACTION_UPDATE);
            protocolNIOT->SendPacket();
            if (noMessages==false)
                wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_FIRMWAREACTION"),Lang->GetAppMiss("MISS_DIALOG_TIT_INFO"));
        }
    }else if (grupFlash->GetId() == "BLE Firmware"){
            protocolNIOT->GenFwActionPacket(flongitud,FW_ACTION_UPDATEBLE);       // send file length
            protocolNIOT->SendPacket();
    }

    transmitting=false;
    return true;
}

// ---------------------------------------------------------------------------------------------------------------------
bool Cnuvalink::SendGroupValuesFlash(CGroupFlash* grupFlash)
{
    //Els grups flash poden ser de dos tipus: de columnes (llistat grid) o de fitxers.
    //Quan es tracta de fitxers, cridarem la funcio SendFlashFile tantes vegades
    //com calgui. En canvi, quan es tracta de "columnes" podem enviar-ho tot de cop en
    //la mateixa funcio

    if (grupFlash->GetTypeCon() == TIPUS_GRUPFLASH_FITXERS) {
        bool res=true;
        for (int i=0; i<grupFlash->GetNumFiles(); i++) {
            CFitxerFlash *fitxer = grupFlash->GetFile(i);
            if (fitxer->IsSelec()) {
                res = SendFlashFile(grupFlash, fitxer);
                if (!res)
                    break;
            }
        }
        return res;
    }

    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        transmitting=false;
        return false;
    }
    //modelSelect ens diu a quina central pertany el grup. Si no ho sabem
    //farem com si el codi fos de la central
    if (!modelSelect) modelSelect = install.GetControlPanel();

    string address = grupFlash->GetAddress();

    int longitud;
    unsigned char *valors, *dades;
    //Distancia (a la memoria) entre elements
    int bytesElement = grupFlash->GetBytesElement();
    int numElements = grupFlash->GetNumElements();

    longitud = bytesElement * numElements;

    valors = new unsigned char [longitud];
    int k=0;
    for (int i=0; i<numElements; i++) {
        dades = grupFlash->GetElement(i);
        for (int j=0; j<bytesElement; j++)
            valors[k++] = dades[j];
    }

    // Cada trama pot enviar com a molt 16 bytes.
    int trames;
    int sizef;
    int adr;
    adr=hToI(address);
    adr<<=4;            // 32 bits address are not using 16bytes to cluster address.
    if ((connecthandler.GetTypeCon()==TYPE_USB_CONNECTION)){
        trames = (longitud+63) / 64;
        sizef = 32;
    }else{
        trames = (longitud+255) / 256;
        sizef = 256;
    }
    //wxMessageBox(_wx(address),_wx(iToH(adr)));

    for (int i=0; i<trames; i++) {
        timerHello->Stop();
        timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.

        address = iToH(adr+(i*sizef));
        bool segueix;
        int intents=0;
        do {
            segueix=false;

            if (i < (trames-1)) {
                protocolNIOT->GenTxFlashPacket( address, &valors[i*sizef],sizef,true);
            } else {
                protocolNIOT->GenTxFlashPacket( address, &valors[i*sizef], longitud-(i*sizef),true);
            }

            if (!protocolNIOT->SendPacket()) {
                if (protocolNIOT->EsPotReintentar() && intents<3) {
                    segueix=true;
                    intents++;
                }
                else {
                    msgError = protocolNIOT->GetLastError();
                    TooMuchError();
                    transmitting=false;
                    return false;
                }
            }

            if (UserCancel()){
                return false;
            }

        } while (segueix);
        string appmiss=Lang->GetAppMiss("MISS_DIALOG_SENDFLASH").c_str();
        if (!Progres(1,appmiss+iToS(protocolNIOT->datatx)+" bytes")){
            transmitting=false;
            return false;
        }
       if (connecthandler.GetTypeCon()==TYPE_RS485_CONNECTION){
            // en local deixem un forat per tal que els altres pugui parlar!!!
            if ((sizef==256) && (!(i&0x07))){
                Sleep(200);
            }else if (!(i&0x3F)){
                Sleep(200);
            }
        }
    }

    transmitting=false;
    return true;
}

//SendValues. Funcio que es crida desde MyFrame quan l'usuari fa click al bot� ENVIAR.
//Aquesta funci� decideix qu� �s el que s'ha d'enviar en funci� del que estigui
//seleccionat al TreeView.
bool Cnuvalink::SendValues(wxTreeItemId &id,bool onlychanges)
{

    if (transmitting){
        // if active proces, wait anyway
        msgError=Lang->GetAppMiss("MISS_ERROR_AUT_PROCES").c_str();
        return false;
    }

    int tipus = TreeCtr.GetTypeCon(id);
    bool result=false;

    panelEdit->ApplyValues();
    CModel* central=install.GetControlPanel();
    CStatus* estat=central->GetStatus();

    //Obtenim el CModel pare
    modelSelect = TreeCtr.GetModel(id);

    int sizef=connecthandler.GetMaxNIOTInfLen();

    protocolNIOT->datatx=0;
    int max = NumComPackets(id, false, sizef);
    if (max <= 0) {
        msgError = Lang->GetAppMiss("MISS_ERROR_SEND_0").c_str();
        return false;
    }
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    transmitting=true;

    progress=0;

    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"), Lang->GetAppMiss("MISS_DIALOG_TIT_SEND")+_wx(iToS(protocolNIOT->datatx)),
    max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);

    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;

    //Buidem la cua per no confondrens amb alguna trama d'alguna transmissio anterior
    protocolNIOT->ClearRxQueue();

    if (tipus == TREE_TIPUS_CDATA) {
        //de moment res
        result = true;
    } else if (tipus == TREE_TIPUS_CGROUP) {
        CGroup *grup = TreeCtr.GetGroup(id);
        result = SendGroupValues(grup,onlychanges);
    } else if (tipus == TREE_TIPUS_CFLASHGROUP) {
        CGroupFlash *grupFlash = TreeCtr.GetGroupFlash(id);
        result = SendGroupValuesFlash(grupFlash);
    }

    delete progressDlg;
    progressDlg=0;
    progress=0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return result;
}

// Funcio especial. �s equivalent a SendGroupValues pero crea per si mateixa
// la finestra de dialeg
bool Cnuvalink::SendValues(CGroup *grup,bool onlychanges)
{
    bool result=false;

    panelEdit->ApplyValues();

    // Mostrem barra de progres. Ens cal saber quants codis enviarem
    // aixi marquem el maxim i nem augmentant valors
    int max = NumCodisInside(grup, false);
    if (max <= 0) {
        msgError = Lang->GetAppMiss("MISS_ERROR_SEND_0").c_str();
        return false;
    }

    if (transmitting){
        Sleep(2000);
        if (transmitting){
            msgError=Lang->GetAppMiss("MISS_ERROR_AUT_PROCES").c_str();
            transmitting=false;
            return false;
        }
    }

    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    transmitting=true;

    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"), Lang->GetAppMiss("MISS_DIALOG_TIT_SEND")+_wx(iToS(protocolNIOT->datatx)),
    max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;

    modelSelect = install.GetControlPanel();

    result = SendGroupValues(grup,onlychanges);

    delete progressDlg;
    progressDlg=0;
    progress=0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return result;
}

bool Cnuvalink::SendAreaArm(WORD areesMask, bool Connect)
{
    bool result;

    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    protocolNIOT->datarx=0;
    protocolNIOT->datatx=0;

    transmitting=true;
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"), Lang->GetAppMiss("MISS_DIALOG_TIT_SEND")+_wx(iToS(protocolNIOT->datatx)),
    1, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);

    if (!modelSelect) modelSelect = install.GetControlPanel();

    if (Connect)
        protocolNIOT->GenArmAreaPacket( areesMask );
    else
        protocolNIOT->GenDisarmAreaPacket( areesMask);

    if (!protocolNIOT->SendPacket()) {
        delete progressDlg;
        progressDlg=0;
        progress=0;
        msgError = protocolNIOT->GetLastError();
        TooMuchError();
        transmitting=false;
        return false;
    }
    string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_SEND").c_str();
    if (!Progres(1,appmiss+iToS(protocolNIOT->datatx)+" bytes")){
        transmitting=false;
        return false;
    }
    string ver=modelSelect->GetVersio();
    int iver=sToI(ver);
    if (protocolNIOT->RxPacket(T_NIOT_PKT_DEVST)){
        uint16_t len;
        unsigned char* dades=protocolNIOT->GetData(&len);
        CStatus *estat = modelSelect->GetStatus();
        estat->parseDevStPkt(dades,len);
        result=true;
    }else{
        result= RxStatus(ID_ESTAT_AREAS);
    }

    delete progressDlg;
    progressDlg=0;
    progress=0;
    transmitting=false;

    return result;
}


bool Cnuvalink::SendZBypass(unsigned char zone)
{
    bool result;
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }

    protocolNIOT->datarx=0;
    protocolNIOT->datatx=0;

    transmitting=true;
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"), Lang->GetAppMiss("MISS_DIALOG_TIT_SEND")+_wx(iToS(protocolNIOT->datatx)),
    1, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);

    if (!modelSelect) modelSelect = install.GetControlPanel();

    protocolNIOT->GenBypassZPacket( zone, ON_USER_NUVALINK);
    if (!protocolNIOT->SendPacket()) {
        delete progressDlg;
        progressDlg=0;
        progress=0;
        transmitting=false;
        msgError = protocolNIOT->GetLastError();
        TooMuchError();
        return false;
    }
    string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_SEND").c_str();
    if (!Progres(1,appmiss+iToS(protocolNIOT->datatx)+" bytes")) {
        delete progressDlg;
        progressDlg=0;
        progress=0;
        transmitting=false;
        return false;
    }
    if (protocolNIOT->RxPacket(T_NIOT_PKT_DEVST)){
        uint16_t len;
        unsigned char* dades=protocolNIOT->GetData(&len);
        CStatus *estat = modelSelect->GetStatus();
        estat->parseDevStPkt(dades,len);
        result=true;
    }else{
        result= RxStatus(ID_ESTAT_INPUT);
    }
    delete progressDlg;
    progressDlg=0;
    progress=0;
    transmitting=false;

    return result;
}


bool Cnuvalink::RecordWave(uint16_t channel,uint16_t samples)
{
    bool result;
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }

    protocolNIOT->datarx=0;
    protocolNIOT->datatx=0;
    if (transmitting){
        Sleep(2000);
        if (transmitting){
            msgError=Lang->GetAppMiss("MISS_ERROR_AUT_PROCES").c_str();
            transmitting=false;
            return false;
        }
    }

    transmitting=true;
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"), Lang->GetAppMiss("MISS_DIALOG_TIT_SEND")+_wx(iToS(protocolNIOT->datatx)),
    1, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);

    if (!modelSelect) modelSelect = install.GetControlPanel();

    protocolNIOT->GenWaveRecordPacket( channel, samples);
    if (!protocolNIOT->SendPacket()) {
        transmitting=false;
        msgError = protocolNIOT->GetLastError();
        TooMuchError();
        return false;
    }

    delete progressDlg;
    progressDlg=0;
    progress=0;
    transmitting=false;

    return true;
}











bool Cnuvalink::SendOutputAct(int sortida, bool activar)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    protocolNIOT->datarx=0;
    protocolNIOT->datatx=0;

    transmitting=true;
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    if (!modelSelect) modelSelect = install.GetControlPanel();

    if (sortida<0) sortida=0;
    unsigned char sortidaMask = (unsigned char)sortida;

    if (activar)
        protocolNIOT->GenActOutputPacket( sortidaMask);
    else
        protocolNIOT->GenDeactOutputPacket( sortidaMask);

    if (!protocolNIOT->SendPacket()) {
        msgError = protocolNIOT->GetLastError();
        TooMuchError();
        transmitting=false;
        return false;
    }
    bool res;
    if (protocolNIOT->RxPacket(T_NIOT_PKT_DEVST)){
        uint16_t len;
        unsigned char* dades=protocolNIOT->GetData(&len);
        CStatus *estat = modelSelect->GetStatus();
        estat->parseDevStPkt(dades,len);
        res=true;
    }else{
        res= RxStatus(ID_ESTAT_OUTPUT);
    }
    progressDlg=0;
    progress=0;
    transmitting=false;
    return res;
}

// ------------------------------------------------------ RxStatus -----------------
bool Cnuvalink::RxStatus(int SeccioEstat)
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    protocolNIOT->datarx=0;
    protocolNIOT->datatx=0;
    transmitting=true;
    //modelSelect ens diu a quina central pertany l'estat. Si no ho sabem
    //farem com si el codi fos de la central
    if (!modelSelect) modelSelect = install.GetControlPanel();
    CStatus *estat = modelSelect->GetStatus();
    string add=estat->GetStEeprom(SeccioEstat);
    int stlen=estat->GetStMemLen(SeccioEstat);
    int off=hToI(add);
    if (RxMemBlock(stlen,add,&RawMsk[off],true)){
        // need to update group from RawRam
        UpdateStatusFromRawMem();
    } else {
        msgError = protocolNIOT->GetLastError();
        transmitting=false;
        return false;
    }

    transmitting=false;
    return true;
}
// --------------------------------------------------------------------- UpdateStatusFromRawMem------
bool Cnuvalink::UpdateStatusFromRawMem()
{
/*  ID_ESTAT_INPUT  =   1,
    ID_ESTAT_OUTPUT =   2,
    ID_ESTAT_AREAS  =   3,
    ID_ESTAT_SYS    =   4 */
    CStatus *estat = modelSelect->GetStatus();
    // Upgrade estat of Inputs
    estat->UpdateFromRawMem(RawMsk,ID_ESTAT_INPUT);
    estat->UpdateFromRawMem(RawMsk,ID_ESTAT_OUTPUT);
    estat->UpdateFromRawMem(RawMsk,ID_ESTAT_AREAS);
    estat->UpdateFromRawMem(RawMsk,ID_ESTAT_SYS);

}
// ------------------------------------------------------------RxEvents--------------------
bool Cnuvalink::RxEvents()
{
    if (!online) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_NOT").c_str();
        return false;
    }
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    //modelSelect ens diu a quina central pertany la llista d'events. Si no ho
    // sabem farem com si el codi fos de la central
    if (!modelSelect) modelSelect = install.GetControlPanel();
    CEventList *llistaEvents = modelSelect->GetEventList();
    CStatus *estat = modelSelect->GetStatus();
    estat->SetPunterEvents(estat->STSYS.SysSt.PPE);

    //Num d'events que ens falten per rebre?
    int myPointer = llistaEvents->GetPunter();
    int punterCentral = estat->GetPunterEvents();

    //Opcio rebreTots. Rep tots els events de la memoria
    int events2Request, events2Request2, requested, maxPunter;
    maxPunter=llistaEvents->GetMaxPunter();

    if (llistaEvents->rebreTots) {
        myPointer = punterCentral+1;
        if (myPointer >= maxPunter)
            myPointer = 0;
    }

    if (myPointer <= punterCentral) {
        events2Request = punterCentral - myPointer;
        events2Request2 = 0;
    } else {
        events2Request = maxPunter - myPointer;
        events2Request2 = punterCentral;
    }
    // ---------------------------------------- limitar n�mero d'events a rebre --------------------
    if ((events2Request+events2Request2)>100){        // si molts events per demanar....
        wxString demand = wxGetTextFromUser(Lang->GetAppMiss("MISS_DIALOG_TTI_RXLASTEV"), Lang->GetAppMiss("MISS_DIALOG_TTI_RXLASTEV"), _wx(iToS(events2Request+events2Request2)));
        string demands=demand.c_str();
        int todemand=sToI(demands);
        if (todemand>llistaEvents->GetMaxPunter())
            todemand=llistaEvents->GetMaxPunter();

        if (todemand>punterCentral){
            int dif=todemand-punterCentral;
            myPointer=llistaEvents->GetMaxPunter()-dif;
        }else{
            myPointer=punterCentral-todemand;
        }

        if (myPointer <= punterCentral) {
            events2Request = punterCentral - myPointer;
            events2Request2 = 0;
        } else {
            events2Request = llistaEvents->GetMaxPunter() - myPointer;
            events2Request2 = punterCentral;
        }
        llistaEvents->SetPunter(myPointer);
    }
// --------------------------------------- progress with amount of data to receive, we know  now ---

    int lendata=connecthandler.GetMaxNIOTInfLen();

    int max=events2Request+events2Request2;
    max /= lendata/llistaEvents->GetBytesEvent();
    max += 1;
    max++;

    transmitting=true;
    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_RECV"), Lang->GetAppMiss("MISS_DIALOG_TIT_RECV")+_wx(iToS(protocolNIOT->datarx)),max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;

// -------------------------------------------------------------------------------------------------
    for (int ii=0; ii<2; ii++) {
        if (ii==1)
            events2Request = events2Request2;
        //Anem demanant tots els events que volem, en grups de 64 bytes
        while (events2Request) {
            //Els requested en grups de 15
            if (events2Request*llistaEvents->GetBytesEvent() > lendata)
                requested = lendata/llistaEvents->GetBytesEvent();
            else
                requested = events2Request;

            string addqueue=llistaEvents->GetDirFlash().c_str();
            protocolNIOT->GenRxEventsPacket( addqueue, false, llistaEvents->GetPunter(),llistaEvents->GetBytesEvent(), requested);

            if (protocolNIOT->SendPacket()) {
                uint16_t l, j, bytesARebre;
                unsigned char *dades;
                bytesARebre = requested*llistaEvents->GetBytesEvent();
                unsigned char *valors = new unsigned char [bytesARebre];
                // Inicialitzem el buffer a FF, d'aquesta manera si es perd un
                // event,( no es modifica 0xFF) no farem push de qualsevol porquer�a.
                for (j=0;j<bytesARebre;j++){
                    valors[j]=0xFF;
                }
                //Rebem els valors en mes d'una trama
                j=0;
                while (protocolNIOT->RxPacket(T_NIOT_PKT_FLASH)) {
                    timerHello->Stop();
                    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
                    // Sflash tb, don� cra-mcp t� els events en "flash simulada sobre EEP"
                    dades = protocolNIOT->GetData(&l);
                    if (l>bytesARebre+4){
                        l=bytesARebre+4;
                    }
                    int ini = 4;
                    for (int i=ini; i<l; i++) {
                        valors[j++] = dades[i];
                        if (j >= bytesARebre) break;
                    }
                    if (j >= bytesARebre) break;
                }
                if (j) {
                    myPointer = llistaEvents->GetPunter();
                    for (int i=0; i<requested; i++) {
                        CEvent *event = new CEvent(Lang); //llistaEvents->GetEvent(i+myPointer);

                        if (event) {
                            bool afegir=false;
                            int pos=i*llistaEvents->GetBytesEvent();
                            if ((valors[pos]==0xFF) && (valors[pos+1]==0xff) && (valors[pos+2]==0xFF)){
                                // realment no hem rebut l'event ( PROBLEMA AMB GPRSLT ABANS DE 11/01/08 )
                            }else{
                                if (!IsReportEvent( valors[pos] )){
                                    afegir=event->SetReportEvent(&valors[pos]);
                                }else if (IsPhoneCallEvent( valors[pos] )){
                                    afegir=event->SetPhoneCallEvent(&valors[pos]);
                                }else{
                                    // Event already in CID format
                                    afegir=event->SetContactIdEvent(&valors[pos]);
                                }
                            }
                            if (afegir==true){
                                llistaEvents->AddEvent(event);
                            }
                        }
                    }
                    llistaEvents->SetPunter(myPointer+requested);
                    events2Request -= requested;
                    string appmiss=Lang->GetAppMiss("MISS_DIALOG_TIT_RECV").c_str();
                    if (!Progres(1,appmiss+iToS(protocolNIOT->datarx)+" bytes"))
                        return false;
                }
            }
            else {
                msgError = protocolNIOT->GetLastError();
                return false;
            }
        }
    }

    return true;
}

bool Cnuvalink::ApplyProg(wxTreeItemId &id)
{
    bool result=false;

    transmitting=true;
    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_APPLY"), Lang->GetAppMiss("MISS_DIALOG_TIT_APPLY"),
    2, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);

    protocolNIOT->GenTramaApplyProg();                   // 09/02/12 . aplicar programaci� es global a tots els elements del bus

    if (!Progres(1,_wx(""))) {
        delete progressDlg;
        progressDlg=0;
        progress=0;
        transmitting=false;
        return false;
    }

    if (protocolNIOT->SendPacket()) {
        result=true;
        if (!Progres(1,_wx(""))) {
            delete progressDlg;
            progressDlg=0;
            progress=0;
            transmitting=false;
            return false;
        }
    }
    else {
        msgError = protocolNIOT->GetLastError();
        result = false;
    }

    delete progressDlg;
    progressDlg=0;
    progress=0;
    transmitting=false;

    return result;
}

int Cnuvalink::NumCodisInside(CGroup *grup, bool rebre)
{
    int result = 0;

    if (grup->DontRx() && rebre) {
        result = 0;
    }
    else if ((grup->DontSend()) && (!rebre) && (privlevel<DEBUG_PRIVLEVEL)) {
        result = 0;
    }
    else {
        result += grup->GetNumChildCodi();

        for (int i = 0; i<grup->GetNumChildGroup(); i++)
            result += NumCodisInside(grup->GetChildGroup(i), rebre);
    }
    protocolNIOT->datatx=0;
    return result;
}

//Ens diu + o - el num de trames que haurem d'enviar/rebre en funci� del que
//estem a punt de transmetre (ho sabem pel que hi ha seleccionat al treeview
int Cnuvalink::NumComPackets(wxTreeItemId &id, bool rebre, int sizef)
{
    int tipus = TreeCtr.GetTypeCon(id);
    int result = 0;

    if (tipus == TREE_TIPUS_CGROUP) {
        CGroup *grup = TreeCtr.GetGroup(id);
        if (grup->DontRx() && rebre) {
            result = 0;
        }
        else if ((grup->DontSend()) && (!rebre) && (privlevel<DEBUG_PRIVLEVEL)) {
            result = 0;
        } else {
            result = NumCodisInside(grup, rebre);
        }
    }
    else if (tipus == TREE_TIPUS_CFLASHGROUP) {
        CGroupFlash *grupFlash = TreeCtr.GetGroupFlash(id);
        if (grupFlash->GetTypeCon() == TIPUS_GRUPFLASH_COLUMNES) {
            result = (grupFlash->GetBytesElement() * grupFlash->GetNumElements() + (sizef-1)) / sizef;
        } else {
            if (rebre && privlevel<ENG_PRIVLEVEL)
                result=0;
            else
                result = (grupFlash->GetBytesFitxers() + (sizef-1)) / sizef;
        }
    }
    else if (tipus == TREE_TIPUS_CMODEL) {
        result = NumCodisInside(modelSelect->GetFatherGroup(), rebre);
    }
    else if (tipus == TREE_TIPUS_CSTATUS) {
        if (rebre)
            result = 3+1+1+1;
        else
            result = 0;
    }
    else if (tipus == TREE_TIPUS_STSECTION) {
        if (rebre) {
            int idSeccio = TreeCtr.GetSection(id);
            if (idSeccio == ID_SECTION_GEN_STATUS) {
                result = 3+1;
            }
            else if (idSeccio == ID_SECTION_ANA_STATUS) {
                result = 1;
            }
            else if (idSeccio == ID_SECTION_OW_STATUS) {
                result = 1;
            }
            else if (idSeccio == ID_SECTION_ZW_STATUS) {
                result = 1;
            }
            else if (idSeccio == ID_SECTION_UW_STATUS) {
                result = 1;
            }
#ifdef ESTAT_GRAFIC
            else if (idSeccio == ID_SECTION_GRAPH_STATUS) {
                result = 3;
            }
#endif
        }
        else
            result = 0;
    }
    else if (tipus == TREE_TIPUS_EVENTS) {
        if (rebre) {
            CEventList *llistaEvents = TreeCtr.GetEventList(id);
            CModel *m = TreeCtr.GetModel(id);
            CStatus *estat = m->GetStatus();

            if (llistaEvents->rebreTots) {
                result = llistaEvents->GetMaxPunter();
            }  else {
                result = estat->GetPunterEvents() - llistaEvents->GetPunter();
                if (result < 0) result += llistaEvents->GetMaxPunter();
            }

            if (result) {
                result /= 64/llistaEvents->GetBytesEvent();
                result += 1;
            }
            result++;
        }
        else
            result = 0;
    }
    else result = -1;

    return result;
}

// -------------------------------------------------------------------------- ask for receive prog at connect ------
bool Cnuvalink::AskReceiveProg()
{
    wxString miss=_wx(Lang->GetAppMiss("MISS_DIALOG_CON_CONNECTED"))+":\r"+_wx(modelSelect->GetDevice().GetDevInfo())+"\r\n";

    modelSelect=install.GetControlPanel();
    wxTreeItemId id=TreeCtr.GetRoot();
    CStatus *estat = modelSelect->GetStatus();

    if ((privlevel==PROD_PRIVLEVEL) || (connecthandler.GetTypeCon()==TYPE_USB_CONNECTION && cloudmode==true)){
/*        miss += _wx("desea enviar la configuraci�n al equipo?");
        int resultbox = wxMessageBox(miss, Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
        if (resultbox == 2)
            return SendValues(id,false);

  */      return true;
    }
    miss += _wx(Lang->GetAppMiss("MISS_DIALOG_ASK_RXPROG"));
    int resultbox = wxMessageBox(miss, Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
    if (resultbox == 2)
        return FastRxProg(id,true);

    return false;

}



// --------------------------------------------------------------------------- Fast Receive programming -------------
bool Cnuvalink::FastRxProg(wxTreeItemId &id,bool afterconnect)
{

    if (!modelSelect)
        modelSelect=install.GetControlPanel();

    string vf=modelSelect->GetDevice().GetVersionFW().c_str();
    int fw=sToI(vf);
    uint16_t lenmsk=modelSelect->GetsizeofMsk();

    grupSelect = TreeCtr.GetGroup(id);

    IniRawMsk(grupSelect,16384);                                    // before receive put values from File Codis to RawMsk ( avoid cancel )

    bool result=RxMemBlock(lenmsk,"0000",RawMsk,true);              // reads from RAM-EEP
    if (result){
        UpdateStatusFromRawMem();

        if (modelSelect->TeAlias() ){
            int resultbox = wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_ASK_RXALIAS"), Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
            if (resultbox == 2){
                if (RxMemBlock(4096,"2000",&RawMsk[0x2000],true))   // reads Alias mem
                    lenmsk+=4096;
            }
        }
        UpdateGrupFromMem(grupSelect,RawMsk,65536);                  // all MSK!
        panelEdit->UpdateValues();
    }else{
        protocolNIOT->GenOrderPacket(ORDER_CANCEL_TRANSFER,NULL,0);
        protocolNIOT->SendPacket();
        wxMessageBox(_wx(Lang->GetAppMiss("MISS_ERROR_TIMEOUT")), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
    return result;
}

// -------------------------------------------------------------------------------------------------------------------
//ReceiveValues. Funcio que es crida desde MyFrame quan l'usuari fa click al bot�
//REBRE.
//Aquesta funci� decideix qu� �s el que s'ha de rebre en funci� del que estigui
//seleccionat al TreeView.
bool Cnuvalink::ReceiveValues(wxTreeItemId &id)
{
    if (transmitting){
        // if active proces, wait anyway
        msgError=Lang->GetAppMiss("MISS_ERROR_AUT_PROCES").c_str();
        return false;
    }

    bool result=false;
    msgError="";
    //Mostrem barra de progres. Ens cal saber quants codis rebrem
    //aixi marquem el maxim i nem augmentant valors
    //Obtenim el CModel pare
    int tipus = TreeCtr.GetTypeCon(id);
    if (tipus == TREE_TIPUS_CDATA || tipus==TREE_TIPUS_VERIFY) {
        //de moment res
        return true;
    }
    if (tipus  == TREE_TIPUS_VMODEM || tipus== TREE_TIPUS_VCOM)
        return false;

    modelSelect = TreeCtr.GetModel(id);
    string idseccio=TreeCtr.GetGroup(id)->GetId();
    // ---------------------------------------- sistema per rebre fitxer flash a array de dades i despr�s aplicar als codis ------------
    if ((idseccio=="Programacio") && (tipus == TREE_TIPUS_CGROUP)){
        return FastRxProg(id,false);
    }

    int max = NumComPackets(id, true,8);          // en principi calculat a 8 bytes rebuts / paquet.
    if (max <= 0) {
        msgError = Lang->GetAppMiss("MISS_ERROR_RECV_0");
        return false;
    }
    progress=0;
    protocolNIOT->datarx=0;
    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    transmitting=true;
    //Buidem la cua per no confondrens amb alguna trama d'alguna transmissio anterior
    protocolNIOT->ClearRxQueue();

    if (tipus == TREE_TIPUS_CGROUP) {
        progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_RECV"), Lang->GetAppMiss("MISS_DIALOG_TIT_RECV")+_wx(iToS(protocolNIOT->datarx)),
        max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
        protocolNIOT->progressDlg = progressDlg;
        protocolNIOT->progress = &progress;
        CGroup *grup = TreeCtr.GetGroup(id);
        result = ReveiveGroupValues(grup);
    }
    else if (tipus == TREE_TIPUS_CFLASHGROUP) {

        CGroupFlash *grupFlash = TreeCtr.GetGroupFlash(id);
        // ------------------------------------- check flash file signatures ----------------------------
        if (grupFlash->GetTypeCon() == TIPUS_GRUPFLASH_FITXERS) {
            bool res=true;
            CDevice device=modelSelect->GetDevice();
            bool WORDCHECK=false;
            if (device.GetIntBootVersionFW()>=212){
                WORDCHECK=true;
            }
            for (int i=0; i<grupFlash->GetNumFiles(); i++) {
                CFitxerFlash *fitxer = grupFlash->GetFile(i);
                if (fitxer->IsSelec()) {
                    res = CheckFlashFile(grupFlash, fitxer,true,WORDCHECK);
                }
            }
            return res;
        }
        progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_RECV"), Lang->GetAppMiss("MISS_DIALOG_TIT_RECV")+_wx(iToS(protocolNIOT->datarx)),
        max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
        protocolNIOT->progressDlg = progressDlg;
        protocolNIOT->progress = &progress;
        result = ReveiveGroupValuesFlash(grupFlash);
    }
    else if (tipus == TREE_TIPUS_CMODEL) {
        progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_RECV"), Lang->GetAppMiss("MISS_DIALOG_TIT_RECV")+_wx(iToS(protocolNIOT->datarx)),
        max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
        protocolNIOT->progressDlg = progressDlg;
        protocolNIOT->progress = &progress;
        result = ReceiveValuesModel(modelSelect);
    }
    else if (tipus == TREE_TIPUS_CSTATUS) {
        result = RxStatus(ID_ESTAT_ALL);
        CStatus *estat = TreeCtr.GetStatus(id);
    }
    else if (tipus == TREE_TIPUS_STSECTION) {
        int idSeccio = TreeCtr.GetSection(id);
        if (idSeccio == ID_SECTION_GEN_STATUS) {
            CStatus *estat = TreeCtr.GetStatus(id);
            result=RxStatus(ID_ESTAT_ALL);
        }
        else if (idSeccio == ID_SECTION_BEACON_STATUS){
            unsigned char valors[4096];
            result=RequestBleDevSt(valors);
        }
    }
    else if (tipus == TREE_TIPUS_EVENTS) {
        result = RxStatus(ID_ESTAT_SYS);
        if (result) {
            if (progressDlg)
                delete progressDlg;
            progressDlg=0;                  // progress de rebre estat redu�t
            result = RxEvents();
        }
    }
    panelEdit->UpdateValues();
    delete progressDlg;
    progressDlg=0;
    progress = 0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return result;
}

//VerifyValues. Funcio que es crida desde MyFrame quan l'usuari fa click al bot�
//VERIFICAR.
//Aquesta funci� decideix qu� �s el que s'ha de rebre en funci� del que estigui
//seleccionat al TreeView. Crida a les rutines de rebre pero amb el flag de verificar
//activat
bool Cnuvalink::VerifyValues(wxTreeItemId &id)
{
    int tipus = TreeCtr.GetTypeCon(id);
    bool result=false;
    msgError="";
    //Mostrem barra de progres. Ens cal saber quants codis rebrem
    //aixi marquem el maxim i nem augmentant valors
    int max = NumComPackets(id, true,8);
    if (max <= 0) {
        msgError = Lang->GetAppMiss("MISS_ERROR_RECV_0");
        return false;
    }

    panelEdit->ApplyValues();

    transmitting=true;
    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_VERIFY"), Lang->GetAppMiss("MISS_DIALOG_TIT_VERIFY"),
    max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;
    protocolNIOT->datarx=0;

    //Obtenim el CModel pare
    modelSelect = TreeCtr.GetModel(id);

    //Buidem la cua per no confondrens amb alguna trama d'alguna transmissio anterior
    protocolNIOT->ClearRxQueue();

    if (tipus == TREE_TIPUS_CGROUP) {
        CGroup *grup = TreeCtr.GetGroup(id);
        result = ReveiveGroupValues(grup, true);
    }
    else if (tipus == TREE_TIPUS_CFLASHGROUP) {
        CGroupFlash *grupFlash = TreeCtr.GetGroupFlash(id);
        result = ReveiveGroupValuesFlash(grupFlash, true);
    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_OPTION");
        result = false;
    }

    panelEdit->UpdateValues();

    delete progressDlg;
    progressDlg=0;
    progress = 0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return result;
}

// La crida el PanelKEyPad. Es l'unica cas en que no volem rebre el que esta
// seleccionat en el treeview
bool Cnuvalink::ReceiveValues(CGroup *grup)
{
    bool result=false;
    msgError="";
    //Mostrem barra de progres. Ens cal saber quants codis rebrem
    //aixi marquem el maxim i nem augmentant valors
    int max = NumCodisInside(grup, true);
    if (max <= 0) {
        msgError = Lang->GetAppMiss("MISS_ERROR_RECV_0");
        return false;
    }

    if (transmitting){
        Sleep(2000);
        if (transmitting){
            msgError=Lang->GetAppMiss("MISS_ERROR_AUT_PROCES");
            transmitting=false;
            return false;
        }
    }

    timerHello->Stop();
    timerHello->Start();            // intentarem evitar que el hello no col�lisioni amb nosaltres.
    transmitting=true;

    progress=0;
    progressDlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_RECV"), Lang->GetAppMiss("MISS_DIALOG_TIT_RECV")+_wx(iToS(protocolNIOT->datarx)),
    max, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    protocolNIOT->progressDlg = progressDlg;
    protocolNIOT->progress = &progress;

    modelSelect = install.GetControlPanel();

    result = ReveiveGroupValues(grup);

    panelEdit->UpdateValues();

    delete progressDlg;
    progressDlg=0;
    progress = 0;
    protocolNIOT->progressDlg = 0;
    protocolNIOT->progress = 0;
    transmitting=false;

    return result;
}

//S'envia un 'hello' cada 20 segons (per que la central no ens penji) a no ser
// que estiguem transmeten algo
//Aprofitem el timerHello per fer un autorefres de l'estat en cas d'estar en
//l'estat grafic o algun altre estat que requereixi l'estat sempre actualitzat
//La variable refrescAuto ens dira si cal fer refresc d'estat o si enviem un hello
//normal i corrent
bool Cnuvalink::TimerHello()
{
    bool success;

    if (OnTimerHello){
        OnTimerHello=false;
        return true;
    }

    if (protocolNIOT->LongPBus)
        return false;

    if ((privlevel==DEBUG_PRIVLEVEL) && (connecthandler.GetTypeCon()<=TYPE_RS485_CONNECTION)){     // at debug mode there is no timerhello.
        transmitting=false;
        return true;
    }
    OnTimerHello=true;
    if (online) {

        if (protocolNIOT->IsProcesBinRx()){
            timerHello->Start();
            OnTimerHello=false;
            return true;
        }
        if (!transmitting) {

            transmitting=true;
            timerHello->Stop();
            //CModel *model = install.GetControlPanel();
            if (!refrescAuto) {
                // --------------------------- S'envia trama HELLO -----------
//                wxMessageBox("timerhello before send");
//                bool send=protocolNIOT->SendHello(protocolNIOT->portaEntrada);
//                wxMessageBox("timerhello after send");
 //               if (send==false){
 //                   if (TooMuchError(2))
//                        return false;
 //               }else{
 //                   protocolNIOT->NumError=0;
 //               }
            } else {
                // --------------------------- Refresc autom�tic d'estat ------
                // uns cops refresca el general uns cops les sortides de manera alterna.
                    if (progressDlg){
                        progressDlg=0;
                        progress = 0;
                        protocolNIOT->progressDlg = 0;
                        protocolNIOT->progress = 0;
                        transmitting=false;
                        delete progressDlg;
                    }
                    success=RxStatus(ID_ESTAT_SYS);
                    if (success) {
                        protocolNIOT->NumError=0;
                        panelEdit->UpdateValues();
                    } else {
                        if (TooMuchError(2))
                            return false;
                    }

            }
        }else{
            // next time will be free.
        }
        transmitting=false;
        timerHello->Start();
    } else{
        transmitting=false;
        timerHello->Stop();
    }
    OnTimerHello=false;
    return true;
}

void Cnuvalink::GetAbout(string *about)
{
    *about = config.GetParamOem("nom-programa");
    *about += Lang->GetAppMiss("MISS_DIALOG_ABO_VERSION");
    *about += MSG_VERSION;//Lang->GetAppMiss("MSG_VERSION");
    *about += "\n";
    *about += "\n" + config.GetParamOem("about") + "\n";
    *about += "\n" + config.GetParamOem("empresa") + "\n" + config.GetParamOem("direccio") +"\n";
    *about += Lang->GetAppMiss("MISS_DIALOG_ABO_PHONE") + config.GetParamOem("telefon") + "\n";
    *about += Lang->GetAppMiss("MISS_DIALOG_ABO_EMAIL") + config.GetParamOem("email") + "\n";
    *about += Lang->GetAppMiss("MISS_DIALOG_ABO_WEBSITE") + config.GetParamOem("website");
}

bool Cnuvalink::Progres (int inc, wxString msg)
{
    string m=msg.c_str();
    return Progres(inc,m);

}

bool Cnuvalink::Progres (int inc, string msg)
{
    if (progressDlg) {
        progress += inc;
        bool res;
        if (msg == "")
            res = progressDlg->Update(progress);
        else
            res = progressDlg->Update(progress, _wx(msg));

        if (res==false) {
            msgError = Lang->GetAppMiss("MISS_ERROR_USER_ABORT");
            return false;
        }
    }
    else {
        //Cridem a l'aplicacio pq processi els missatges del windows
        myApp->Yield();
    }

    return true;
}

bool Cnuvalink::UserCancel()
{
    if (progressDlg) {
        if (!progressDlg->Update(progress)) {
            transmitting=false;
            msgError = Lang->GetAppMiss("MISS_ERROR_USER_ABORT");
            return true;
        }
    }
    else {
        //Cridem a l'aplicacio pq processi els missatges del windows
        myApp->Yield();
    }

    return false;
}

// only programming saved!
bool Cnuvalink::SaveEepToFlash(void)
{
    // Enviem la ordre per tal que el panel copii tota la eeprom a la flash ( RAMNV i EEP )
    protocolNIOT->GenOrderPacket( ORDER_SFLASH_SAVEFACTORYCFG, false);
    protocolNIOT->SendPacket();
    return true;
}

// firmware and programming saved!
bool Cnuvalink::SetEepToFlash(void)
{
    // Enviem la ordre per tal que el panel copii tota la eeprom a la flash ( RAMNV i EEP )
    protocolNIOT->GenOrderPacket( ORDER_SFLASH_FACTORY, NULL,0);
    protocolNIOT->SendPacket();
    return true;
}


bool Cnuvalink::SetFlashToEep(void)
{
    protocolNIOT->GenOrderPacket( ORDER_READ_SFLASH_FACTORY, NULL,0);
    protocolNIOT->SendPacket();
    return true;
}


bool Cnuvalink::SendOrderPacket(unsigned char order, unsigned char param1=0, unsigned char param2=0, unsigned char param3=0, unsigned char param4=0, unsigned char param5=0,int tries=3)
{
    unsigned char params[5];
    params[0]=param1;
    params[1]=param2;
    params[2]=param3;
    params[3]=param4;
    params[4]=param5;

    bool noack=false;
    if (tries==1)
        noack=true;

    protocolNIOT->GenOrderPacket( order, params,4,noack);
    protocolNIOT->SendPacket(tries);
    return true;
}


bool Cnuvalink::SetEepReset(void)
{
    protocolNIOT->GenOrderPacket( ORDER_FACTORY_PROGRAMMING, NULL,0);
    protocolNIOT->SendPacket();
    return true;
}

bool Cnuvalink::SetBattmAhCntReset(void)
{
    protocolNIOT->GenOrderPacket( ORDER_RESETBATTERY_mACnt, false);
    protocolNIOT->SendPacket();
    return true;
}


bool Cnuvalink::ResetGroup(wxTreeItemId &id)
{
    int tipus = TreeCtr.GetTypeCon(id);
    panelEdit->ApplyValues();

    if (tipus == TREE_TIPUS_CGROUP) {
        bool res=false;
        CGroup *grup = TreeCtr.GetGroup(id);
        if (grup) {
            res = grup->Reset();
        }
        panelEdit->UpdateValues();
        return res;
    }
    else if (tipus == TREE_TIPUS_CFLASHGROUP) {
        bool res=false;
        CGroupFlash *grupFlash = TreeCtr.GetGroupFlash(id);
        if (grupFlash) {
            res = grupFlash->Reset();
        }
        panelEdit->UpdateValues();
        return res;
    }

    msgError = Lang->GetAppMiss("MISS_ERROR_OPTION");
    //Quan s'esta en un panell que no es un grup (dades, events, etc) el boto
    //no fa res. No se que es millor, si fer return false i mostrar missatge d'error
    //o simplement no fer res i tornar true
    return false;
}

int Cnuvalink::GetCobertura()
{
    return -1;
/*    if (!modelSelect) modelSelect = install.GetControlPanel();
    CStatus *estat = modelSelect->GetStatus();
    int c=estat->GetCobertura();

    return c;*/
}

bool Cnuvalink::TeCobertura(){
    if (!modelSelect) modelSelect = install.GetControlPanel();
    CStatus *estat = modelSelect->GetStatus();
//    if (estat->TeCobertura())
//        return true;
    return false;
}


bool Cnuvalink::CheckPrivLevel(int upl)
{

    if (upl<=privlevel)
        return true;

    return false;
}

bool Cnuvalink::TooMuchError(int errors=3)
{
    if (privlevel<DEBUG_PRIVLEVEL){
        protocolNIOT->NumError+=errors;
        if (protocolNIOT->NumError>=NUM_FATAL_ERROR){
            msgError = "\n" + Lang->GetAppMiss("MISS_ERROR_CON_LOST");
            online=false;
            transmitting=false;
            return true;
        }
    }
    return false;
}

bool Cnuvalink::IsOnline()
{
    return online;
}
bool Cnuvalink::IsTxOn()
{
    return transmitting;
}

bool Cnuvalink::IsRxCodi()
{
    return rxcodi;
}

CProtocolNIOT* Cnuvalink::GetProtocolNIOT(){
    return protocolNIOT;
}
