#include "event.h"
#include "contactid.h"
#include <wx/datetime.h>
#define _wx(x) (wxString)((string)x).c_str()

CEvent::CEvent(CLanguage *lang)
{
    zona=0;
    area=0;
    tipusEvent = 0;
    binnum = 0;
    binfile = "";
    CSReport=false;
    Lang = lang;
    Lat=Long=360;
    Analog[0]=Analog[1]=Analog[2]=Analog[3]=0;
    sAbonat="";
}

CEvent::~CEvent()
{
}

bool CEvent::SetTypeEvent(int tipus)
{
    if ((tipus == TIPUS_EVENT_MSK)
//        || (tipus == TIPUS_EVENT_CONTACTIDEXTES)
    ){
        tipusEvent = tipus;
        return true;
    }

    return false;
}

// PRE : Tenim un buffer amb un MSKEV.
// POST : retorna un Report Event!
// 1: Index Event = B7 nou/restauracio, B6..0 Index
// 2: Who = B7, B6 reservats. B5..0 Zona/Area
// 3: Date/Time i algunes opcions
bool CEvent::SetReportEvent(unsigned char *data)
{

    if (data[0]==0xFF){
        return false;
    }
    unsigned char contactID[2];

    TEventMsk* pe=(TEventMsk*)&data[0];
    string cID="";
    int tipusPropi;
    //Si entra dades per aquesta funcio es que es tracta d'un report event de JR
    tipusEvent = TIPUS_EVENT_MSK;
    //Primer bit del primer byte. indica alarma o restauracio
    if (pe->CID.Val & M_EVENT_NEW) alarma = true;
    else alarma = false;

    area = (int)(pe->ANUM.byte.HB>>4); // (data[1]&0x0F);
    area++;
    // negative logic on flash events
    if (!(pe->CID.Val & M_EV_NOTREPORT))
        this->CSReport=true;
    //seguents 7 bits del primer byte: Index
    //tipusPropi = (int)(data[0]&0x7F);

    contactID[0]=data[1]&0x0F;
    contactID[1]=data[0];

    sContactID = "qeee";
    if (alarma) sContactID[0] = '1';
    else sContactID[0] = '3';
    sContactID[1] = nibble2ascii(contactID[0]&0x0F);
    sContactID[2] = nibble2ascii((contactID[1]&0xF0)>>4);
    sContactID[3] = nibble2ascii(contactID[1]&0x0F);
    sContactID[4] ='\0';
    cID=&sContactID[1];
    int i = SearchIndx(cID);
    if (i==-1){
        return false;
    }
    tipus = taulaCID[i].tipus;
    string indDescripcio = taulaCID[i].descr;
    sDescripcio=Lang->GetAppMiss(_wx(indDescripcio)).c_str();

    time_t  poll=pe->epoch;
    wxDateTime time (poll);

    int hora = time.GetHour();
    int minut = time.GetMinute();
    int segon = time.GetSecond();
    int dia = time.GetDay();
    int mes = time.GetMonth()+1;        // returns 0..11
    int any = time.GetYear();
    if (any>2130 || any<2017)
        any=2017;
//    any += 2000;
    char buffer[16];
    sprintf(buffer, "%02d:%02d:%02d", hora, minut, segon);
    sHora = buffer;

    sprintf(buffer, "%02d/%02d/%d", dia, mes, any);
    sData = buffer;


    if ((pe->ANUM.Val&M_EVENT_NUM)==M_EVENT_NUM){
        zona=0;
    }else{
        zona = (int)((pe->ANUM.Val&M_EVENT_NUM) + 1);   // max of 32768 zones / users.
    }
    if (pe->CID.Val & M_GEO_EVENT){
        TEventMskGeo *peg=(TEventMskGeo*)&data[0];
        this->Lat=peg->Lat;
        this->Long=peg->Long;
        SetFix(this->Lat,this->Long);
    }else if (pe->CID.Val & M_AN_EVENT){
        TEventMskAn *pea=(TEventMskAn*)&data[0];
        Analog[0]=(float)pea->Analog[0]*0.1;
        Analog[1]=(float)pea->Analog[1]*0.1;
        SetAnalog(this->Analog[0],this->Analog[1]);
    }else{
        sAbonat="    ";
        sAbonat[0]= nibble2ascii(pe->Abonat[0]>>4);
        sAbonat[1]= nibble2ascii(pe->Abonat[0]&0x0F);
        sAbonat[2]= nibble2ascii(pe->Abonat[1]>>4);
        sAbonat[3]= nibble2ascii(pe->Abonat[1]&0x0F);
        sAbonat[4]='\0';
    }

    return true;
}

bool CEvent::SetHora(string& hora)
{
    sHora = hora;
    return true;
}

bool CEvent::SetData(string& data)
{
    sData = data;
    return true;
}

bool CEvent::SetContactID(string& contID)
{
    unsigned char contactID[2];
    string cID;

    if (contID.length() == 4) {
        sContactID = contID;
        if (contID[0] == '1') alarma = true;
        else alarma = false;
        contactID[0] = ascii2nibble(contID[1]);
        contactID[1] = ascii2nibble(contID[2]);
        contactID[1] <<= 4;
        contactID[1] |= ascii2nibble(contID[3]);
        contID[4]='\0';
        cID = &contID[1];

//        cID = (int)contactID[0]*100 + (int)((contactID[1]&0xF0)>>4) * 10 + (int)(contactID[1]&0x0F);
        int i = SearchIndx(cID);
        if (i==-1){
            return false;
        }
        tipus = taulaCID[i].tipus;
        string indDescripcio = taulaCID[i].descr;

        sDescripcio=Lang->GetAppMiss(_wx(indDescripcio)).c_str();
        //sDescripcio = taulaCID[i].descr;
    }
    else return false;

    return true;
}

bool CEvent::SetZona(int zona)
{
    this->zona = zona;
    return true;
}


bool CEvent::SetBinNum(int binnum){
    this->binnum = binnum;
    return true;
}

bool CEvent::SetBinFile(string& file){
    this->binfile=file;
}


//S'espera una trama de 8 bytes
//+0x00	FE	Phone call event
//+0x01	TT	Phone call tipus
//+0x02	DD  Phone digits
//+0x03	DD  Phone digits
//+0x04	DD  Phone digits
//+0x05	DD  Phone digits
//+0x06	DD  Phone digits
//+0x07	DD  Phone digits

bool CEvent::SetPhoneCallEvent(unsigned char *data)
{
    tipus=TIPUS_EVENT_PHONECALL;
    sContactID="1900";
    sTipus="PhoneCall";
    tipusEvent = TIPUS_EVENT_MSK;

    string calldirection,modecall,statuscall;

    if(data[1]&T_INCOMING_CALL)
    {
        calldirection="Incoming";
    }else{
        calldirection="Outgoing";
    }
    switch (data[1]&0xF0)
    {
        case T_PSTN_AUDIO:
            modecall="-PSTN AUDIO";
            break;
        case T_PSTN_DATA:
            modecall="-PSTN DATA";
            break;
        case T_GSM_AUDIO:
            modecall="-GSM AUDIO";
            break;
        case T_GSM_DATA:
            modecall="-GSM DATA";
            break;
        case T_GSM_FAX:
            modecall="-GSM FAX";
            break;
        case T_GSM_SMS:
            modecall="-GSM SMS";
            break;
        default:
            modecall="unknow";
            break;

    }
    switch (data[1]&0x07)
    {
        case T_INI_CALL:
            statuscall="-INI";
            break;
        case T_SUCCESS_CALL:
            statuscall="-OK";
            break;
        case T_BUSY_CALL:
            statuscall="-BUSY";
            break;
        case T_FAIL_CALL:
            statuscall="-FAIL";
            break;
        case T_REJECT_CALL:
            statuscall="-REJECT";
            break;
        default:
            statuscall="unknow";
            break;

    }
    sDescPhoneCall=calldirection+modecall+statuscall;

    char buff[12];
    numtel="";
    char *pch=&buff[0];
    for (int i=0;i<16;i++){
        *(pch+i)='\0';
    }
    HexChainToAscii(&data[2], pch,12);

    for (int i=0;i<12;i++)
        numtel+=buff[i];

    zona=sToI(numtel);


        linia = (int)(data[8]&0xF0)>>4;
        numero = (int)(data[8]&0x0F);

        int dia = (int)data[9];
        int mes = (int)data[10];
        int any = (int)data[11];
        int hora = (int)data[12];
        int minut = (int)data[13];
        int segon = (int)data[14];
        any += 2000;

        char buffer[16];
        sprintf(buffer, "%02d:%02d:%02d", hora, minut, segon);
        sHora = buffer;

        sprintf(buffer, "%02d/%02d/%d", dia, mes, any);
        sData = buffer;


    return true;
}
//S'espera una trama de 8 � 16 bytes 8ext�s):
//+0x00	CC	High Nibbles of Client Code
//+0x01	CC	Low Nibbles of Client code
//+0x02	CID	CID Identifier (0x18)
//+0x03	QE	CID Qualifier and high nibble of event code
//+0x04	EE	Low nibbles of event code
//+0x05	GG	Area
//+0x06	SS	High nibbles of zone
//+0x07	SP	Low nibbles of zone and Checksum
// - si es ext�s s'esperen 16 bytes +
//+0x08	High Nibble  : Receiver Line
//      Low Nibble   : Receiver Number
//+0x09	Day
//+0x0A	Month
//+0x0B	Year
//+0x0C	Hour
//+0x0D	Minute
//+0x0E	Second
//+0x0F	reserved
bool CEvent::SetContactIdEvent(unsigned char *data)
{
    int cID, tipusPropi;

    //Si entra dades per aquesta funcio es que es tracta d'un event contactID
    tipusEvent = TIPUS_EVENT_MSK;

    //+0x00	CC	High Nibbles of Client Code
    //+0x01	CC	Low Nibbles of Client code
    sAbonat = "xxxx";
    sAbonat[0] = nibble2ascii((data[0]&0xf0)>>4);
    sAbonat[1] = nibble2ascii(data[0]&0x0f);
    sAbonat[2] = nibble2ascii((data[1]&0xf0)>>4);
    sAbonat[3] = nibble2ascii(data[1]&0x0f);

    //+0x03	QE	CID Qualifier and high nibble of event code
    //+0x04	EE	Low nibbles of event code
    string contactID = "qeee";
    contactID[0] = nibble2ascii((data[3]&0xF0)>>4);
    contactID[1] = nibble2ascii(data[3]&0x0F);
    contactID[2] = nibble2ascii((data[4]&0xF0)>>4);
    contactID[3] = nibble2ascii(data[4]&0x0F);

    if (SetContactID(contactID)==false)             // no l'afegir� al no trobar descripci�
        return false;

    //+0x05	GG	Area
    area = (int)( (data[5]>>4)*10 + data[5]&0x0f);

    //+0x06	SS	High nibbles of zone
    //+0x07	SP	Low nibbles of zone and Checksum
    zona = (int) ((data[6]&0xF0)>>4)*100 + (data[6]&0x0F)*10 + ((data[7]&0xF0)>>4);

        //+0x08	High Nibble  : Receiver Line
        //      Low Nibble   : Receiver Number
        linia = (int)(data[8]&0xF0)>>4;
        numero = (int)(data[8]&0x0F);

        //+0x09	Day
        //+0x0A	Month
        //+0x0B	Year
        //+0x0C	Hour
        //+0x0D	Minute
        //+0x0E	Second
        int dia = (int)(data[9]&0x1F);
        int mes = (int)(data[10]&0x0F);
        int any = (int)data[11];
        int hora = (int)(data[12]&0x1F);
        int minut = (int)(data[13]&0x3F);
        int segon = (int)(data[14]&0x3F);
        any += 2000;

        char buffer[16];
        sprintf(buffer, "%02d:%02d:%02d", hora, minut, segon);
        sHora = buffer;

        sprintf(buffer, "%02d/%02d/%d", dia, mes, any);
        sData = buffer;

    return true;
}

bool CEvent::SetAbonat(string& abonat)
{
    sAbonat = abonat;
    return true;
}

bool CEvent::SetArea(int area)
{
    this->area = area;
    return true;
}


int CEvent::GetTypeConEvent()
{
    return tipusEvent;
}

string& CEvent::GetHora()
{
    return sHora;
}

string& CEvent::GetData()
{
    return sData;
}

string& CEvent::GetNumTel()
{
    return numtel;
}

string& CEvent::GetBinFile()
{
    return binfile;
}

int CEvent::GetBinNum()
{
    return binnum;
}


string& CEvent::GetDescr()
{
    return sDescripcio;
}

string& CEvent::GetContactID()
{

    return sContactID;
}

int CEvent::GetZona()
{
    return zona;
}

int CEvent::GetArea()
{
    return area;
}

int CEvent::GetReceiverLine()
{
    return linia;
}

int CEvent::GetReceiverNumber()
{
    return numero;
}

string& CEvent::GetAbonat()
{
    return sAbonat;
}

bool CEvent::IsAlarm()
{
    if (tipus == TIPUS_EVENT_ALARMA)
        return alarma;
    else
        return false;
}

bool CEvent::IsTipusBin()
{
    return (sContactID=="1609");
}

string& CEvent::GetTypeCon()
{
    switch (tipus) {
        case TIPUS_EVENT_ALARMA:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_ALARM");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_RESTORE");
            break;
        case TIPUS_EVENT_ONOFF:
            // JVD:03/10/06 - L'armat es '3' i el desarmat '1'.
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_OFF");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_ON");
            break;

        case TIPUS_EVENT_SUPERVISIO:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_SUPERVISIO_ALARMA");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_SUPERVISIO_REST");
            break;
        case TIPUS_EVENT_AVERIA:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_AVERIA");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_RESTORE");
            break;
        case TIPUS_EVENT_OMISIO:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_OMISIO");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_RESTORE");
            break;
        case TIPUS_EVENT_TEST:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_TEST_ALARMA");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_TEST_REST");
            break;
        case TIPUS_EVENT_RELE:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_RELE_ALARM");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_RELE_REST");
            break;
            break;
        case TIPUS_EVENT_OUTPUT:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_OUTPUT_ALARM");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_OUTPUT_REST");
            break;
        case TIPUS_EVENT_BUS:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_BUS_ALARM");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_BUS_REST");
            break;
        case TIPUS_EVENT_ANALOG:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_ANALOG");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_ANALOG");
            break;
        case TIPUS_EVENT_GEO:
            if (alarma)
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_GEO");
            else
                sTipus = Lang->GetAppMiss("MSG_EVT_TIPUS_GEO");
            break;

    }

    return sTipus;
}

int CEvent::SearchIndx(string cID)
{

    int i = 0;
    while (taulaCID[i].cID != "0") {
        if (taulaCID[i].cID == cID) return i;
        i++;
    }

    return -1;
}

bool CEvent::IsTipusAlarm()
{
    if (tipus == TIPUS_EVENT_ALARMA)
        return true;
    else
        return false;
}

bool CEvent::IsTipusAveria()
{
    if (tipus == TIPUS_EVENT_AVERIA)
        return true;
    else
        return false;
}

bool CEvent::IsTipusONOFF()
{
    if (tipus == TIPUS_EVENT_ONOFF)
        return true;
    else
        return false;
}

bool CEvent::IsTipusSupervisio()
{
    if (tipus == TIPUS_EVENT_SUPERVISIO)
        return true;
    else
        return false;
}

bool CEvent::IsTipusTest()
{
    if (tipus == TIPUS_EVENT_TEST || tipus==TIPUS_EVENT_ANALOG)
        return true;
    else
        return false;
}


bool CEvent::IsTipusOmisio()
{
    if (tipus == TIPUS_EVENT_OMISIO)
        return true;
    else
        return false;
}

bool CEvent::IsTipusRele(){
    return (tipus == TIPUS_EVENT_RELE);
}


bool CEvent::IsTipusOutput(){
    return (tipus == TIPUS_EVENT_OUTPUT);
}

bool CEvent::IsTipusBus(){
    return (tipus==TIPUS_EVENT_BUS);
}

bool CEvent::IsTipusAnalog(){
    return (tipus==TIPUS_EVENT_ANALOG);
}

bool CEvent::IsTipusGeo(){
    return (tipus==TIPUS_EVENT_GEO);
}


string& CEvent::GetBusStringID(unsigned int id){
    id&=0x3F;            // max = 64 terminals.
    return BusID[id];
}

bool CEvent::IsCSReported(){
    return CSReport;
}



bool CEvent::SetFix(float Lat,float Long)
{
    if (Lat<90.1 && Long<180.1){
        sFix=wxString::Format(wxT("%f"), Lat)+","+wxString::Format(wxT("%f"), Long);
        //sprintf (&sFix[0], "%f,%f",Lat,Long);
    }
}
bool CEvent::SetFix(wxString fix)
{
    sFix=fix;
}
wxString CEvent::GetFix()
{
    return sFix;
}


bool CEvent::SetAnalog(wxString an,int n)
{
    sAnalog[n]=an;
}

bool CEvent::SetAnalog(float an1,float an2)
{
    sAnalog[0]=wxString::Format(wxT("%f"), an1);
    sAnalog[1]=wxString::Format(wxT("%f"), an2);
}

wxString CEvent::GetAnalog(int n)
{
    return sAnalog[n];
}
