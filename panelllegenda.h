#ifndef PANELLLEGENDA_H
#define PANELLLEGENDA_H

#include <iostream>
#include <string>
using namespace std;
#include "msgs.h"
#include <wx/panel.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>

#define _wx(x) (wxString)((string)x).c_str()

class CPanelLlegenda: public wxPanel
{
    friend class CPanelEditEvt;
    protected:
        wxStaticText *llegenda;
        wxBoxSizer *hSizer;

    public:
        CPanelLlegenda(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage* lang=0);
        void Clear();
        ~CPanelLlegenda();
        CLanguage*    Language;
        bool SetLlegenda(string &llegenda);

};
// end CPanelLlegenda

#endif
