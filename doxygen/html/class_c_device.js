var class_c_device =
[
    [ "CheckFirmwareFile", "class_c_device.html#aa7c34c4c7c85fabd6745481657b52eae", null ],
    [ "Construct", "class_c_device.html#a54267a5b7daf0c01b3bf454beb850172", null ],
    [ "Construct", "class_c_device.html#a806ae40925d8241c66e28825340f378f", null ],
    [ "GetDevInf", "class_c_device.html#a29ff69d1f6836a45963320b6ca6a9774", null ],
    [ "GetDevInfo", "class_c_device.html#a449ebf914b1fc1b057b624eabc46ed51", null ],
    [ "GetDevRev", "class_c_device.html#a598fb54c90fcbe37b314b4f9845e8769", null ],
    [ "GetId", "class_c_device.html#a42e59a26cb08cf4de18ce9d35e2c725e", null ],
    [ "GetIntVersionFW", "class_c_device.html#a1aab500afe02213167be731f6a927c54", null ],
    [ "GetPanelDescr", "class_c_device.html#a51eb3da4a3435711f6b3a7ad50a099fb", null ],
    [ "GetVersion", "class_c_device.html#a23962625bf52676bff4c57d2045e77e0", null ],
    [ "GetVersionFW", "class_c_device.html#adad84d593cb222d6d219260a6617efa1", null ]
];