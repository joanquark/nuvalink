var class_c_panel_bit_grup =
[
    [ "CPanelBitGrup", "class_c_panel_bit_grup.html#af4c1c4981a55954cb58354dbc7caa7ef", null ],
    [ "~CPanelBitGrup", "class_c_panel_bit_grup.html#a7f5a10c0ab4de24ffa34061c075d1f7d", null ],
    [ "ActualitzaValors", "class_c_panel_bit_grup.html#a8b48f0f485ae41af309f9422ebe23275", null ],
    [ "AplicaValors", "class_c_panel_bit_grup.html#a8a3d55ecc50fa2f74ec056bf1a0660be", null ],
    [ "CanviaSeleccio", "class_c_panel_bit_grup.html#aec35c66764fe8e1db220ed365f3f7d84", null ],
    [ "CanviaValorCela", "class_c_panel_bit_grup.html#a3f547fa148e4bac12ba662f9e7d59e89", null ],
    [ "Clear", "class_c_panel_bit_grup.html#a3dc7fb1ac174a988079148f15a31dcf5", null ],
    [ "DECLARE_EVENT_TABLE", "class_c_panel_bit_grup.html#a9e9b229b92f73caa557087e21d33263a", null ],
    [ "GeneraComponents", "class_c_panel_bit_grup.html#a752d63638bfd29615917e1c2b3424e12", null ],
    [ "GetBitGrup", "class_c_panel_bit_grup.html#a37d3c32312b0ec7de91548070bd8b6d3", null ],
    [ "OnClickRight", "class_c_panel_bit_grup.html#aa7fa82d0a460e8368b533b62999e2cc4", null ],
    [ "OnDClick", "class_c_panel_bit_grup.html#a00cf9b183349f79ef904e7a6cb62cbc7", null ],
    [ "OnKey", "class_c_panel_bit_grup.html#a5bf63e4e3131707ec7d251a7ef32b346", null ],
    [ "SetBitGrup", "class_c_panel_bit_grup.html#a472753ad1192ab343b40b843c3c7a282", null ],
    [ "UnSetBitGrup", "class_c_panel_bit_grup.html#a7fe5441aa70789371e4a96a354ebf715", null ],
    [ "CPanelEditEvt", "class_c_panel_bit_grup.html#aaaf8346752cc6c27d95928043cf8a1e2", null ],
    [ "codiList", "class_c_panel_bit_grup.html#a1669caabb9f705e2461f84a4c413c499", null ],
    [ "grid", "class_c_panel_bit_grup.html#aa374cbbd521896998f39ba8e334ef190", null ],
    [ "grup", "class_c_panel_bit_grup.html#a4d07de4503dcb8bab6d98b419b5bf380", null ],
    [ "UnLang", "class_c_panel_bit_grup.html#af445244b825fd26bf14cb1266f016994", null ],
    [ "vSizer", "class_c_panel_bit_grup.html#a3f53c7858e2f367037089f8ee3917404", null ]
];