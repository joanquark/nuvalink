var class_c_panel_fitxer =
[
    [ "CPanelFitxer", "class_c_panel_fitxer.html#a259cdce94b52a6037989385967d70e2e", null ],
    [ "~CPanelFitxer", "class_c_panel_fitxer.html#adf1617290fdc8516b55fdb4dc02c99f0", null ],
    [ "ActualitzaValors", "class_c_panel_fitxer.html#aae937e3c826542de8ecdf0bde89bc1cb", null ],
    [ "AplicaValors", "class_c_panel_fitxer.html#a43bee6aed758b68817d15b7231113a6d", null ],
    [ "Clear", "class_c_panel_fitxer.html#a618f8c12d006529dee1dba5396dd6e07", null ],
    [ "DECLARE_EVENT_TABLE", "class_c_panel_fitxer.html#a3efbc6ba01a2974c057b19a832e58166", null ],
    [ "GeneraComponents", "class_c_panel_fitxer.html#a32e372bea49811c8c6e3c0a5ab9f2270", null ],
    [ "GetFitxer", "class_c_panel_fitxer.html#a833f845484b019fa9cdcbf1656fcd380", null ],
    [ "OnBotoExaminar", "class_c_panel_fitxer.html#a294556d7341bdd5b8cdb9e4ed12e1013", null ],
    [ "SetFitxer", "class_c_panel_fitxer.html#ab15f759631eeff8ab38439fd89c89cc8", null ],
    [ "UnSetFitxer", "class_c_panel_fitxer.html#ad107f1db23e87f130bdec2380e7b17f3", null ],
    [ "CPanelEditEvt", "class_c_panel_fitxer.html#aaaf8346752cc6c27d95928043cf8a1e2", null ],
    [ "botoExaminar", "class_c_panel_fitxer.html#a5a89caabe673e2ea34dcaf6d6a04f5cc", null ],
    [ "check", "class_c_panel_fitxer.html#a569888c6603b95c267646381ba49f8da", null ],
    [ "fitxer", "class_c_panel_fitxer.html#a619dfdc9f99579d5c1bc400f5ca199bd", null ],
    [ "hSizer", "class_c_panel_fitxer.html#ac3c51dabb16608893f9cafcdfcf5c3a5", null ],
    [ "label", "class_c_panel_fitxer.html#ad928386c0e9a1b2c33e07086e4ae31f2", null ],
    [ "text", "class_c_panel_fitxer.html#ae5a78c3bfb12a7c2b9c653530f98020c", null ],
    [ "UnLang", "class_c_panel_fitxer.html#a9b37608bac07bf4815c42a0b79396810", null ],
    [ "vSizer", "class_c_panel_fitxer.html#aa0e1de97aae79aa6fa4ad49ac26eddb0", null ]
];