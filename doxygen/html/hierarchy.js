var hierarchy =
[
    [ "_BYTE_VAL", "union___b_y_t_e___v_a_l.html", null ],
    [ "_DWORD_VAL", "union___d_w_o_r_d___v_a_l.html", null ],
    [ "_INT16_VAL", "union___i_n_t16___v_a_l.html", null ],
    [ "_TIIDFr", "struct___t_i_i_d_fr.html", null ],
    [ "_TJRHard", "union___t_j_r_hard.html", null ],
    [ "_WORD_VAL", "union___w_o_r_d___v_a_l.html", null ],
    [ "AddrRange", "struct_addr_range.html", null ],
    [ "AddrStruct", "struct_addr_struct.html", null ],
    [ "AES", "class_a_e_s.html", null ],
    [ "BYTE_BITS", "struct_b_y_t_e___b_i_t_s.html", null ],
    [ "callback_functions", "structcallback__functions.html", null ],
    [ "CArbreCtr", "class_c_arbre_ctr.html", null ],
    [ "CCodi", "class_c_codi.html", null ],
    [ "CCodiBit", "class_c_codi_bit.html", null ],
    [ "CComponentGrafic", "class_c_component_grafic.html", null ],
    [ "CConexio", "class_c_conexio.html", null ],
    [ "CDades", "class_c_dades.html", null ],
    [ "CDevice", "class_c_device.html", null ],
    [ "CEstat", "class_c_estat.html", null ],
    [ "CEstatGrafic", "class_c_estat_grafic.html", null ],
    [ "CEvent", "class_c_event.html", null ],
    [ "CFitxerFlash", "class_c_fitxer_flash.html", null ],
    [ "CGrup", "class_c_grup.html", null ],
    [ "CGrupFlash", "class_c_grup_flash.html", null ],
    [ "CIntegrationID", "class_c_integration_i_d.html", null ],
    [ "CLlistaEvents", "class_c_llista_events.html", null ],
    [ "CLogger", "class_c_logger.html", null ],
    [ "CMesura", "class_c_mesura.html", null ],
    [ "CMIB", "class_c_m_i_b.html", null ],
    [ "Cmlink", "class_cmlink.html", null ],
    [ "CModelsDir", "class_c_models_dir.html", null ],
    [ "CProtocolMIB", "class_c_protocol_m_i_b.html", null ],
    [ "CSerial", "class_c_serial.html", null ],
    [ "elementoArrayChoicesSBSDC", "structelemento_array_choices_s_b_s_d_c.html", null ],
    [ "func_args", "structfunc__args.html", null ],
    [ "G500", "class_g500.html", null ],
    [ "hid_device_info", "structhid__device__info.html", null ],
    [ "jsbyte", "unionjsbyte.html", null ],
    [ "myXmlParser", "classmy_xml_parser.html", null ],
    [ "rel", "structrel.html", null ],
    [ "TArray2", "union_t_array2.html", null ],
    [ "TArray3", "union_t_array3.html", null ],
    [ "TArray4", "union_t_array4.html", null ],
    [ "TArray8", "union_t_array8.html", null ],
    [ "TASt", "struct_t_a_st.html", null ],
    [ "TContactIDdescr", "struct_t_contact_i_ddescr.html", null ],
    [ "TCoord", "union_t_coord.html", null ],
    [ "tcp_ready", "structtcp__ready.html", null ],
    [ "TDateTime", "struct_t_date_time.html", null ],
    [ "TDevInf", "struct_t_dev_inf.html", null ],
    [ "TDyn", "struct_t_dyn.html", null ],
    [ "TEventMsk", "struct_t_event_msk.html", null ],
    [ "TEventMskGeo", "struct_t_event_msk_geo.html", null ],
    [ "TGpsSt", "struct_t_gps_st.html", null ],
    [ "TGsmSt", "struct_t_gsm_st.html", null ],
    [ "TInSt", "struct_t_in_st.html", null ],
    [ "TIPSt", "struct_t_i_p_st.html", null ],
    [ "TiXmlAttributeSet", "class_ti_xml_attribute_set.html", null ],
    [ "TiXmlBase", "class_ti_xml_base.html", [
      [ "TiXmlAttribute", "class_ti_xml_attribute.html", null ],
      [ "TiXmlNode", "class_ti_xml_node.html", [
        [ "TiXmlComment", "class_ti_xml_comment.html", null ],
        [ "TiXmlDeclaration", "class_ti_xml_declaration.html", null ],
        [ "TiXmlDocument", "class_ti_xml_document.html", null ],
        [ "TiXmlElement", "class_ti_xml_element.html", null ],
        [ "TiXmlText", "class_ti_xml_text.html", null ],
        [ "TiXmlUnknown", "class_ti_xml_unknown.html", null ]
      ] ]
    ] ],
    [ "TiXmlCursor", "struct_ti_xml_cursor.html", null ],
    [ "TiXmlHandle", "class_ti_xml_handle.html", null ],
    [ "TiXmlParsingData", "class_ti_xml_parsing_data.html", null ],
    [ "TiXmlString", "class_ti_xml_string.html", [
      [ "TiXmlOutStream", "class_ti_xml_out_stream.html", null ]
    ] ],
    [ "TiXmlVisitor", "class_ti_xml_visitor.html", [
      [ "TiXmlPrinter", "class_ti_xml_printer.html", null ]
    ] ],
    [ "TMIBFr", "struct_t_m_i_b_fr.html", null ],
    [ "TMIBHeader", "struct_t_m_i_b_header.html", null ],
    [ "ToLower", "struct_to_lower.html", null ],
    [ "TOSt", "struct_t_o_st.html", null ],
    [ "ToUpper", "struct_to_upper.html", null ],
    [ "TRomInfo", "struct_t_rom_info.html", null ],
    [ "TSTSYS", "struct_t_s_t_s_y_s.html", null ],
    [ "TSysInd", "struct_t_sys_ind.html", null ],
    [ "TSysSt", "struct_t_sys_st.html", null ],
    [ "wxApp", null, [
      [ "MyApp", "class_my_app.html", null ]
    ] ],
    [ "wxControl", null, [
      [ "wxSpeedButton", "classwx_speed_button.html", null ]
    ] ],
    [ "wxDialog", null, [
      [ "AJRE", "class_a_j_r_e.html", null ],
      [ "CatDialog", "class_cat_dialog.html", null ],
      [ "CloudDialog", "class_cloud_dialog.html", null ],
      [ "DManageFrame", "class_d_manage_frame.html", null ],
      [ "LoginDlg", "class_login_dlg.html", null ]
    ] ],
    [ "wxDigitData", "structwx_digit_data.html", null ],
    [ "wxEvtHandler", null, [
      [ "CPanelEditEvt", "class_c_panel_edit_evt.html", null ]
    ] ],
    [ "wxFrame", null, [
      [ "MyFrame", "class_my_frame.html", null ]
    ] ],
    [ "wxJSONReader", "classwx_j_s_o_n_reader.html", null ],
    [ "wxJSONRefData", "classwx_j_s_o_n_ref_data.html", null ],
    [ "wxJSONValue", "classwx_j_s_o_n_value.html", null ],
    [ "wxJSONValueHolder", "unionwx_j_s_o_n_value_holder.html", null ],
    [ "wxJSONWriter", "classwx_j_s_o_n_writer.html", null ],
    [ "wxNotifyEvent", null, [
      [ "wxThumbnailEvent", "classwx_thumbnail_event.html", null ]
    ] ],
    [ "wxObject", null, [
      [ "CMyObj", "class_c_my_obj.html", null ],
      [ "wxThumbnailItem", "classwx_thumbnail_item.html", [
        [ "wxImageThumbnailItem", "classwx_image_thumbnail_item.html", null ]
      ] ]
    ] ],
    [ "wxPanel", null, [
      [ "CPanelBitGrup", "class_c_panel_bit_grup.html", null ],
      [ "CPanelCfgVROut", "class_c_panel_cfg_v_r_out.html", null ],
      [ "CPanelCfgVRUser", "class_c_panel_cfg_v_r_user.html", null ],
      [ "CPanelCodi", "class_c_panel_codi.html", null ],
      [ "CPanelDades", "class_c_panel_dades.html", null ],
      [ "CPanelEstat", "class_c_panel_estat.html", null ],
      [ "CPanelEvents", "class_c_panel_events.html", null ],
      [ "CPanelFitxer", "class_c_panel_fitxer.html", null ],
      [ "CPanelFlashGrup", "class_c_panel_flash_grup.html", null ],
      [ "CPanelGrafic", "class_c_panel_grafic.html", null ],
      [ "CPanelKeyPad", "class_c_panel_key_pad.html", null ],
      [ "CPanelLlegenda", "class_c_panel_llegenda.html", null ],
      [ "CPanelVerify", "class_c_panel_verify.html", null ],
      [ "CPanelWebserver", "class_c_panel_webserver.html", null ]
    ] ],
    [ "wxScrolledWindow", null, [
      [ "CPanelEdit", "class_c_panel_edit.html", null ],
      [ "CPanelMapa", "class_c_panel_mapa.html", null ],
      [ "wxThumbnailCtrl", "classwx_thumbnail_ctrl.html", null ]
    ] ],
    [ "wxWindow", null, [
      [ "wxLCDWindow", "classwx_l_c_d_window.html", null ]
    ] ],
    [ "XmlNotify", "class_xml_notify.html", [
      [ "CConfig", "class_c_config.html", null ],
      [ "CInstalacio", "class_c_instalacio.html", null ],
      [ "CModel", "class_c_model.html", null ],
      [ "CUnLang", "class_c_un_lang.html", null ]
    ] ]
];