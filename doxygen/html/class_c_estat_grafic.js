var class_c_estat_grafic =
[
    [ "CEstatGrafic", "class_c_estat_grafic.html#a3323be103f7614a3435a45eab7c2482f", null ],
    [ "~CEstatGrafic", "class_c_estat_grafic.html#ab39b6367b483a8d0e1fb9aff10a027f8", null ],
    [ "AddComponent", "class_c_estat_grafic.html#a09a261d061a48083d5319dad59f594fe", null ],
    [ "Clear", "class_c_estat_grafic.html#ab1918470e721cd8feb8801b76dbad35c", null ],
    [ "DelComponent", "class_c_estat_grafic.html#a8de16fa47f49ae2ade873fad45c1a483", null ],
    [ "DelComponent", "class_c_estat_grafic.html#a7336a5a677c26a9d22269fbfa5c8e733", null ],
    [ "GetCompByRele", "class_c_estat_grafic.html#a8759ce44687cdf30129b2d40b9e4a60c", null ],
    [ "GetCompBySortida", "class_c_estat_grafic.html#adb8582ff4b23aef8734c283aaca49beb", null ],
    [ "GetComponent", "class_c_estat_grafic.html#ac6f9c0856bdcb42345af6f255273e0a2", null ],
    [ "GetFitxerMapa", "class_c_estat_grafic.html#a9f749058c863e5c2ee152e4455da6b60", null ],
    [ "GetNumComponents", "class_c_estat_grafic.html#a64fa31920e700ff4f9cecbe3b5e0db3d", null ],
    [ "GetSensorByZona", "class_c_estat_grafic.html#ae8b0889ae1a06ac08a4feef7dbea474e", null ],
    [ "SetFitxerMapa", "class_c_estat_grafic.html#a1be4986933bb5bc6b276722320e9b6e6", null ]
];