var class_c_llista_events =
[
    [ "CLlistaEvents", "class_c_llista_events.html#ac15b771dc2fe9eb2258d12e360509726", null ],
    [ "~CLlistaEvents", "class_c_llista_events.html#a1b99626e7b3eaf3b7c0856edae6f3244", null ],
    [ "AddEvent", "class_c_llista_events.html#aed7a456e32ada8f30864223e55984f16", null ],
    [ "Clear", "class_c_llista_events.html#a26438158f71b7554b9eb56db8c726c16", null ],
    [ "DeleteEvent", "class_c_llista_events.html#acb36d1dee9d3726593fc89873e0848c6", null ],
    [ "GetBytesEvent", "class_c_llista_events.html#ab6fb3a715822cf5ff2c0aaa6a84ea71e", null ],
    [ "GetDirFlash", "class_c_llista_events.html#aba1b8e94f6ce5855691ed4a788b557f3", null ],
    [ "GetEvent", "class_c_llista_events.html#a3934c522217a99f898b3ccff7a978358", null ],
    [ "GetMaxPunter", "class_c_llista_events.html#a617fb768e66b7049e24ed87ec88c6e53", null ],
    [ "GetNumEvents", "class_c_llista_events.html#aa81d7b00a8a546e62eff8381466231b9", null ],
    [ "GetPunter", "class_c_llista_events.html#a93a8f6319f774d6e6c538fac4529abd9", null ],
    [ "GetTipusEvents", "class_c_llista_events.html#adda7845ea7163c8f51760d6f17eb7bb3", null ],
    [ "IsEeprom", "class_c_llista_events.html#a19d3beddcd7e381a21872ce6132e6563", null ],
    [ "ModifyEvent", "class_c_llista_events.html#ad16880223fcdd17bfde4c499ef8f3bf8", null ],
    [ "SetBytesEvent", "class_c_llista_events.html#acd22bdfe70dadf6cc41efeff75818589", null ],
    [ "SetDirFlash", "class_c_llista_events.html#aec1e1442f956800592af39b65cdfc2cc", null ],
    [ "SetMaxPunter", "class_c_llista_events.html#a44a3db854eb0adb31dd45ee7f1474b2a", null ],
    [ "SetPunter", "class_c_llista_events.html#a47221849999ef195acbe7287d8c90772", null ],
    [ "SetTipusEvents", "class_c_llista_events.html#a99bc20628e3ba6f1c979a2d59895582a", null ],
    [ "rebreTots", "class_c_llista_events.html#a6e8b0f48a550827b160f314d339be169", null ]
];