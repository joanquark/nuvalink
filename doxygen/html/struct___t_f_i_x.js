var struct___t_f_i_x =
[
    [ "Accuracy", "struct___t_f_i_x.html#a6d1b30247e01917f3de84bf7e3732081", null ],
    [ "Height", "struct___t_f_i_x.html#a87bcb49e254357899a4bb2fe09be5e4d", null ],
    [ "Lat", "struct___t_f_i_x.html#a7e008647e9bc8cb24d6ec13b4a8e5571", null ],
    [ "Long", "struct___t_f_i_x.html#af68c39267e9b18a16855c9d20f2a1b54", null ],
    [ "resGps", "struct___t_f_i_x.html#a08a765922f002973ca1c40ea6e1fae02", null ],
    [ "Satellite", "struct___t_f_i_x.html#a79f81824784184c6a9dbeea60fc76d7f", null ],
    [ "Speed", "struct___t_f_i_x.html#ad192439bcea48a156d7a55e6cf8ddf61", null ]
];