var class_c_panel_cfg_v_r_out =
[
    [ "ID_TXTBATT", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba49b7e7e4b9e87195f8ca47fa16061e67", null ],
    [ "ID_TXTTEMP", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4baf954aaa9e3b469868a381b03d65667a1", null ],
    [ "ID_TXTBATTVAL", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba446bac33b33b058d7f3460e3f44172c3", null ],
    [ "ID_TXTTEMPVAL", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4baeda317e5caa94984fe7b222e9a457711", null ],
    [ "ID_TEMPGAUGE", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba57800c0e0966cf21ef7f30389e5f3f3b", null ],
    [ "ID_BATTGAUGE", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba01f4420cf4e5763911dde2864e3d9942", null ],
    [ "ID_MINUTS", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba5b247995279d4f7a56442f5b9b527d5b", null ],
    [ "ID_DBM", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba2a3cd56dc22eb7a583c139abc84d1a73", null ],
    [ "ID_NIVEL", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba574c988046079625d121814442bc9c1e", null ],
    [ "ID_TEST", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4baf97bd0ebf7d50de84003dcbe5f3c12be", null ],
    [ "ID_TESTEDIT", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba56a773d69e81152af23beb45d5c41c59", null ],
    [ "ID_RFGAUGE", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba10726b97bbca863d427519f03535f681", null ],
    [ "ID_BOTODELETE", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba878ecf37012825acf957e40f3755947f", null ],
    [ "ID_BOTOREAD", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba6c8b6596a0d10d07336f64fb05861ca4", null ],
    [ "ID_BOTOADD", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4bae197b50974c5daa0c2b66376d8549183", null ],
    [ "ID_WXSTATICTEXT1", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba36817cf1b3e40aa46c9e62f20f166daf", null ],
    [ "ID_WXCOMBOBOX1", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba18a38219f3d508b61269aed4392b03f2", null ],
    [ "ID_ZONECODEEDIT", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba2e327c654bdd01f157cbf2d6ee87d25b", null ],
    [ "ID_ZONENUM", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4bae25093e1b0b2f073a3543ed2b4b9b4e8", null ],
    [ "ID_OutEdit", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4baf8d7dcedaf48cd97ada55d29958ecb5f", null ],
    [ "ID_ZONAS", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba5477407e89f2e8fb02539d67f4866836", null ],
    [ "ID_CONFIGVR", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4bac56dbbcdaeb439d14f4631b2d6f33020", null ],
    [ "ID_WXPANEL1", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba2abe9b2b0a953277031c7492d35f5fa0", null ],
    [ "ID_DUMMY_VALUE_", "class_c_panel_cfg_v_r_out.html#aec1f71319eabe570b9b3c69ae613ca4ba717a96b50f6ea318b5e25db3467b72ea", null ],
    [ "CPanelCfgVROut", "class_c_panel_cfg_v_r_out.html#a81e14ee68b47bc0a6c8729e2d0b2613d", null ],
    [ "~CPanelCfgVROut", "class_c_panel_cfg_v_r_out.html#ae53040f34440f1705ce0d9a70db0d66a", null ],
    [ "ActualitzaValors", "class_c_panel_cfg_v_r_out.html#afe148b50ea6a7f942d0fc383770e102a", null ],
    [ "AplicaValors", "class_c_panel_cfg_v_r_out.html#a1ee19a4162bf39a9e977d9a060b6c5ab", null ],
    [ "BotoAddClick", "class_c_panel_cfg_v_r_out.html#a36917ea367911d1b2dd3139325b287ca", null ],
    [ "BotoDeleteClick", "class_c_panel_cfg_v_r_out.html#ae2660bb5fb6964f442a928d773e2e999", null ],
    [ "BotoReadClick", "class_c_panel_cfg_v_r_out.html#ada93dbf4b37578bf4d90ee3967831e52", null ],
    [ "Clear", "class_c_panel_cfg_v_r_out.html#a6e925b04a0eeb85df881244fd17ef5f3", null ],
    [ "GeneraComponents", "class_c_panel_cfg_v_r_out.html#a5c9a8f8d5cdbfe0fa23f0bdfd49d8231", null ],
    [ "GetEstat", "class_c_panel_cfg_v_r_out.html#afffc2dc40d2f29325ed9fc7dea983c58", null ],
    [ "Read", "class_c_panel_cfg_v_r_out.html#a83d3ab9a23ee7f9f7b0fe9e3ac606019", null ],
    [ "SetEstat", "class_c_panel_cfg_v_r_out.html#ab9e7c519e1685c64995e7a9b7b6c1d88", null ],
    [ "SetGrups", "class_c_panel_cfg_v_r_out.html#aae9a980645010417bfec60df1df194c3", null ],
    [ "UnSetEstat", "class_c_panel_cfg_v_r_out.html#adfe0fc6eb5af6f201d9e70074ca8a8ee", null ],
    [ "VwXSetwxPoint", "class_c_panel_cfg_v_r_out.html#ac62f7b16ceb0418c7ac415a93e162c3c", null ],
    [ "VwXSetwxSize", "class_c_panel_cfg_v_r_out.html#a68ccc204a60aa28c010b7b40b3b3c9ad", null ],
    [ "CPanelEditEvt", "class_c_panel_cfg_v_r_out.html#aaaf8346752cc6c27d95928043cf8a1e2", null ],
    [ "BattGauge", "class_c_panel_cfg_v_r_out.html#a9f876b041aa19d2b8e7eb2d5cb8130cd", null ],
    [ "Batttxt", "class_c_panel_cfg_v_r_out.html#abe34d6241330bd280fe27add08fc887b", null ],
    [ "BatttxtVal", "class_c_panel_cfg_v_r_out.html#ae1dba26d6afa7dbc101d01653a09e49f", null ],
    [ "BotoAdd", "class_c_panel_cfg_v_r_out.html#a772e166fbd864cafc113a7b82f179319", null ],
    [ "BotoDelete", "class_c_panel_cfg_v_r_out.html#a94b9befd26b8547549a75058721d6fc2", null ],
    [ "BotoRead", "class_c_panel_cfg_v_r_out.html#a3f5de338667861127bd9a5da2e8b9e59", null ],
    [ "COalias", "class_c_panel_cfg_v_r_out.html#accaed83736136ba3acb098ef7b2ee87a", null ],
    [ "codi", "class_c_panel_cfg_v_r_out.html#acba997fa39a05b5e52a88b4af457091e", null ],
    [ "ConfigVR", "class_c_panel_cfg_v_r_out.html#ab437a32e8b769241b72f343fee579146", null ],
    [ "Dbm", "class_c_panel_cfg_v_r_out.html#a82dcd7e816c8d24708f2a9224b188d0d", null ],
    [ "estat", "class_c_panel_cfg_v_r_out.html#a4f97ed6922a4f9fc8bb7d54dbb8f2abe", null ],
    [ "fixout", "class_c_panel_cfg_v_r_out.html#acff21b06d65baead6f0aedbafe562b8f", null ],
    [ "m_tmppoint", "class_c_panel_cfg_v_r_out.html#a21052cc9da45a068626731207d15a9f3", null ],
    [ "m_tmpsize", "class_c_panel_cfg_v_r_out.html#a1690428ea06bd5061ebd4ca5292a5e6d", null ],
    [ "minuts", "class_c_panel_cfg_v_r_out.html#ad2f774292dbf1eeaf7e728c5a39b2990", null ],
    [ "mlink", "class_c_panel_cfg_v_r_out.html#ae56a5c468b78cce57811c600f5fa6c59", null ],
    [ "OutEdit", "class_c_panel_cfg_v_r_out.html#ab200c527b1fd7dc5aff2105f6fb633cc", null ],
    [ "PanelId", "class_c_panel_cfg_v_r_out.html#af64c76bd207e31d0dc3969d37721a0c1", null ],
    [ "PanelVersion", "class_c_panel_cfg_v_r_out.html#acd1b1bb0d1a95a7030a2317a95d041d6", null ],
    [ "RfGauge", "class_c_panel_cfg_v_r_out.html#a1e01a813582c15c106489c5ca25f4e06", null ],
    [ "RfLevel", "class_c_panel_cfg_v_r_out.html#a99590f3272de84e554f6a47d139c2b95", null ],
    [ "TempGauge", "class_c_panel_cfg_v_r_out.html#a424d36f73e9697d158c0a78d83034a5b", null ],
    [ "Temptxt", "class_c_panel_cfg_v_r_out.html#a1770b6eb9d428dfa8616bd688ce59217", null ],
    [ "TemptxtVal", "class_c_panel_cfg_v_r_out.html#ab32257c3f82ba3503568dbfc4bf2109f", null ],
    [ "Test", "class_c_panel_cfg_v_r_out.html#a15fe49b7f49dda4db3ec45c3df20ea6c", null ],
    [ "TestEdit", "class_c_panel_cfg_v_r_out.html#aabc987c0546c2272824d2a23af020ca4", null ],
    [ "UnLang", "class_c_panel_cfg_v_r_out.html#a419ee1d2f633b7302da4ae3f783feef4", null ],
    [ "WxComboBox1", "class_c_panel_cfg_v_r_out.html#a47f2c1b9430404fc2829aba4cc12285e", null ],
    [ "WxPanel1", "class_c_panel_cfg_v_r_out.html#a0da358fd99d90051e5fd490a590d1c82", null ],
    [ "WxStaticText1", "class_c_panel_cfg_v_r_out.html#a863be7ef44a589e3e8b51357b024eab1", null ],
    [ "Zona", "class_c_panel_cfg_v_r_out.html#a7e3fe0a3ce35954edd305ed0a43f3e9e", null ],
    [ "ZoneCodeEdit", "class_c_panel_cfg_v_r_out.html#a74b49b3b63c3a3f055611117d6a2d1a1", null ],
    [ "ZoneNum", "class_c_panel_cfg_v_r_out.html#a6eca99c48db61a0a0598e75b13c3ff1d", null ]
];