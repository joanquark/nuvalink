var NAVTREE =
[
  [ "MLink", "index.html", [
    [ "Deprecated List", "deprecated.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Enumerations", "functions_enum.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"_a_e_s__iv_8h_source.html",
"class_c_config.html#a001294224b051a9168db254d6d567bf5",
"class_c_fitxer_flash.html#a07621ece52e435887ba2b0f29e25ca5c",
"class_c_model.html#a1ff8fe0445359f60cf5896265e253452",
"class_c_panel_codi.html#af4fd512380b61d5dc88106bb9e60ba73",
"class_c_panel_fitxer.html",
"class_c_protocol_m_i_b.html#a5623e978baf188463f53767c37e8a7f1",
"class_cmlink.html#ae272e3becaa8dc7866c8f96fd75d6fdb",
"class_ti_xml_node.html#a3d7897999f99cf4870dd59df6331d7ff",
"classwx_j_s_o_n_value.html#a2178b6fa0ca8bb4d7b29bd4251f09b35",
"classwx_thumbnail_ctrl.html#a354d8a1eba6b5c3f8a132d586eea4e6e",
"struct_addr_struct.html#af54a17953ff2a2f48b34a937ccd1026c",
"union___d_w_o_r_d___v_a_l.html#a13455f36721514e0bdf6f1caa7f15ab9"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';