var struct_t_gsm_st =
[
    [ "CIMI", "struct_t_gsm_st.html#ad03cf609a16eada7e9f35e72960beae2", null ],
    [ "CntPhoneCalls", "struct_t_gsm_st.html#a54e0eaf713997b26f5649eab3906ff54", null ],
    [ "CntSmsSent24h", "struct_t_gsm_st.html#a4fc7f8f830f0d0d188f4da45f6d2bdc2", null ],
    [ "Coverture", "struct_t_gsm_st.html#af36905671cac8230554cd50a5c4714c8", null ],
    [ "DevInf", "struct_t_gsm_st.html#a53499e99461c5c3a811c071ca23aa943", null ],
    [ "FGsmSt", "struct_t_gsm_st.html#ab4216711228b8ea6370d5fb369084f0c", null ],
    [ "NetSt", "struct_t_gsm_st.html#ab83b23d4ed698dd61ec3b5b9ebd0ebac", null ],
    [ "res", "struct_t_gsm_st.html#a46771c50b46901f4ea4e58e98756af71", null ],
    [ "rssi", "struct_t_gsm_st.html#a0c9c13e1cf79f2f3cc69ea4c9c03c725", null ]
];