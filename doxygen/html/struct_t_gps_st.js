var struct_t_gps_st =
[
    [ "AvSNR", "struct_t_gps_st.html#ae2827748f5f874ffbe862e8092d68cc7", null ],
    [ "Height", "struct_t_gps_st.html#a5f2915546891085e7319452e6191918e", null ],
    [ "Lat", "struct_t_gps_st.html#a6bf24d482c3575ea9372d9645f2db5fa", null ],
    [ "Long", "struct_t_gps_st.html#aa86b785af9a2ba3bc2bc388d42004567", null ],
    [ "Satellite", "struct_t_gps_st.html#afef7ddbccd66bc64a5ed3b22a5ab6055", null ],
    [ "Speed", "struct_t_gps_st.html#a189286f633354e434f84ef922e47c0bf", null ],
    [ "TAvSearchSat", "struct_t_gps_st.html#ac31704efe8bd2c00c35ada338aebf19a", null ]
];