var struct_t_sys_st =
[
    [ "AcLevel", "struct_t_sys_st.html#abfa54dfbae2d84d7887efc93a4602b46", null ],
    [ "BattLevel", "struct_t_sys_st.html#a02ff0f3e937c640fe9cb1414619411fa", null ],
    [ "BoardTemp", "struct_t_sys_st.html#a759d66b8fcfe35faaacc84f55730e769", null ],
    [ "BoardVer", "struct_t_sys_st.html#a5c6e353c5dbf0893759e21840e662827", null ],
    [ "dt", "struct_t_sys_st.html#a16391d510d4d5f2f743741ebf24e3020", null ],
    [ "FlashID", "struct_t_sys_st.html#a5c752e1af18b07ab3068305a58018827", null ],
    [ "FwVerH", "struct_t_sys_st.html#a13b3fd449689847f0b648c1e8dfa9a26", null ],
    [ "FwVerL", "struct_t_sys_st.html#a7cdc1fd6325ee792a42b9c05031cdf59", null ],
    [ "PDE", "struct_t_sys_st.html#a6b7aa73bda7150b5b39dfb8b7bfe5ab2", null ],
    [ "PPE", "struct_t_sys_st.html#a3742c901fbaa4d11219b495c14804fdc", null ],
    [ "PRE1", "struct_t_sys_st.html#a625bf620e0c1a674e7bc8e7b57bf1326", null ],
    [ "PRE2", "struct_t_sys_st.html#aac5a9d9d27ccf334aa6f37f9ca1c7479", null ],
    [ "PRE3", "struct_t_sys_st.html#aa9abf312ce7c21ecd4f98a7b2c289d30", null ],
    [ "StAreas", "struct_t_sys_st.html#a8b5f9875f8d000af296c5239ee29f63e", null ],
    [ "StPanel", "struct_t_sys_st.html#ad1d7a9b852cbfce5b7d37f2a731daaeb", null ],
    [ "SysHard", "struct_t_sys_st.html#adb117cffa46a384c65cdea2d2f839805", null ],
    [ "TDevice", "struct_t_sys_st.html#a1cc081c9bb4181665d7e1cc01fc848cf", null ],
    [ "TimerReplaceBatt", "struct_t_sys_st.html#a713af3f7f97879b74d9a1a6a6be1bc57", null ],
    [ "TReportTest", "struct_t_sys_st.html#ad423bfc43c696793c28535f162d5db1a", null ],
    [ "TZAC", "struct_t_sys_st.html#a25d562fa35ff968b27dc67812ca77f1d", null ],
    [ "TZBatt", "struct_t_sys_st.html#ac6abb75cd1dfa0935b183ed2767a0221", null ]
];