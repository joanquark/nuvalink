var class_c_panel_events =
[
    [ "CPanelEvents", "class_c_panel_events.html#aeeee627d11efde37636d5cc79106005b", null ],
    [ "~CPanelEvents", "class_c_panel_events.html#a09fac861410b28b803b0c9e4db50f6e9", null ],
    [ "ActualitzaValors", "class_c_panel_events.html#a2a5f1c649ace3ddd216c560072e72f82", null ],
    [ "AplicaValors", "class_c_panel_events.html#a5c64c76d275c359860e6fb4dede54892", null ],
    [ "Clear", "class_c_panel_events.html#aba892ca7f57864b44bdef057a492a99b", null ],
    [ "DECLARE_EVENT_TABLE", "class_c_panel_events.html#af6d217919378b7cd3ce26c7a12d3bf0f", null ],
    [ "ExportarEvents", "class_c_panel_events.html#ade02568bb421ecca5f17f654c6608b21", null ],
    [ "GeneraComponents", "class_c_panel_events.html#a6d1cb8f0d6fc43e2583317e8ee257fe7", null ],
    [ "GeneraCSVEvents", "class_c_panel_events.html#a39509b9f8f645acf54058703374acf12", null ],
    [ "GetLlista", "class_c_panel_events.html#a4655d21d2b0b5fb623a80cc84fc5bc14", null ],
    [ "OmpleLlista", "class_c_panel_events.html#a8bc62c31c726dc5f1328026c0a062c48", null ],
    [ "OnBotoBuidar", "class_c_panel_events.html#a51210d8841b196d189796cc28ef64297", null ],
    [ "OnBotoExportar", "class_c_panel_events.html#acf7574b8b03aa875c522b96b8f5e9b97", null ],
    [ "OnBotoViewBin", "class_c_panel_events.html#a348b7a8ffde83cf34f1a6d548f38fd8d", null ],
    [ "OnCheckAlarm", "class_c_panel_events.html#a6482012dcd26603c7429967948c49c02", null ],
    [ "OnCheckAveria", "class_c_panel_events.html#a25931a00f7629beebd6bf28d49a36368", null ],
    [ "OnCheckInvert", "class_c_panel_events.html#a1ec428690b63dd36408121b147e833f7", null ],
    [ "OnCheckOmisio", "class_c_panel_events.html#ab96b30c492ac9c2061b739ba8716e24e", null ],
    [ "OnCheckONOFF", "class_c_panel_events.html#a1b6256b29756dd984fa29c01943258bd", null ],
    [ "OnCheckPhoneCall", "class_c_panel_events.html#a1e82ebd4a75af6f6d20cb176489c2bee", null ],
    [ "OnCheckRebreTot", "class_c_panel_events.html#aec56cd0947b52634f45757a56a152e68", null ],
    [ "OnCheckSupervisio", "class_c_panel_events.html#a4d3c9ceecb4dc6f5a48194fa22f0d612", null ],
    [ "OnCheckTest", "class_c_panel_events.html#a05ee69a4e6c22e23d2583892d3394036", null ],
    [ "OnDeletelist", "class_c_panel_events.html#a5ee646c3dbed78878401ba61fab2c126", null ],
    [ "SetGrups", "class_c_panel_events.html#ad1768332f0cded3869e6fc84a87f0264", null ],
    [ "SetLlista", "class_c_panel_events.html#a56a17eb9e75c607f8899b2f951e2a04f", null ],
    [ "UnSetLlista", "class_c_panel_events.html#aa303774610cb6c91cf1eeb86c8cdf4c9", null ],
    [ "abonat", "class_c_panel_events.html#a1de8530145c24d6024d348186c1e57b1", null ],
    [ "CAalias", "class_c_panel_events.html#a96359a46eb4bdad3be84d3829fb6331d", null ],
    [ "checkAlarm", "class_c_panel_events.html#a15759180587f6ae6121f21ed3db264b2", null ],
    [ "checkAveria", "class_c_panel_events.html#ae1def9621dad05dc334001885ce2b154", null ],
    [ "checkInvert", "class_c_panel_events.html#aa7cb4653bb7ec0862dd2878efa060be5", null ],
    [ "checkOmisio", "class_c_panel_events.html#a85114923f5d75039f2eb150700ae78ce", null ],
    [ "checkONOFF", "class_c_panel_events.html#a911fe2c1017eca175ba07c9ce73cc88e", null ],
    [ "checkPhoneCall", "class_c_panel_events.html#a011bb0a8562c54a8cc60869c3307805d", null ],
    [ "checkRebreTot", "class_c_panel_events.html#a9e9e07b0bf784ccb24d00d7676992a02", null ],
    [ "checkSupervisio", "class_c_panel_events.html#ab3703e66713c6809e98bf3f4cf096e69", null ],
    [ "checkTest", "class_c_panel_events.html#a0ab73eb2a61ec44cc3e4edf45dbb3001", null ],
    [ "COalias", "class_c_panel_events.html#a7168c35fd80e462e197fe3b63e452b5f", null ],
    [ "codi", "class_c_panel_events.html#ae6bee164f0a005880c84fc4fd1f6ed1d", null ],
    [ "CUalias", "class_c_panel_events.html#a42e1ddfd1868e10906585857cd28b3d4", null ],
    [ "CurYear", "class_c_panel_events.html#a1daeae0439205e856cfeba7c7418bb1d", null ],
    [ "CZalias", "class_c_panel_events.html#aaa0401c0cc6c6d8b41fee9b57e1b166b", null ],
    [ "events", "class_c_panel_events.html#a2681ee6f747d4f89c2b12b52af330adc", null ],
    [ "Filter", "class_c_panel_events.html#a41d3bd056b18ff5e2135cf4da93731b0", null ],
    [ "invert", "class_c_panel_events.html#a42646e13f43ca82d8199cf0845e82118", null ],
    [ "llista", "class_c_panel_events.html#ad10d672e66c9f3a62e84ba1d5f663357", null ],
    [ "mlink", "class_c_panel_events.html#a0e56ed891ab11e138e4ab4eed44ec10e", null ],
    [ "msgError", "class_c_panel_events.html#ac8fa6bf7b27f09f476dce511ad03f0b9", null ],
    [ "PanelId", "class_c_panel_events.html#a52a87bf052a00899d42560a1678caec7", null ],
    [ "PanelVersion", "class_c_panel_events.html#ad3dfb8cba10e9da625658f74e1531cc0", null ],
    [ "RefEvent", "class_c_panel_events.html#ae60a3f5b8f95aa6dda62418ce69923af", null ],
    [ "showAlarm", "class_c_panel_events.html#a6fc7fe0869748c95b8482e1ce1751163", null ],
    [ "showAveria", "class_c_panel_events.html#a7c6e1160e0e5320cc844a66359385daa", null ],
    [ "showOmisio", "class_c_panel_events.html#a1f732f3fb05d35e1518c1805c54a7076", null ],
    [ "showONOFF", "class_c_panel_events.html#aded95913b5105781fbb8fc3b6e4f9273", null ],
    [ "showPhoneCall", "class_c_panel_events.html#af3603e436a0c3369e9a29ca2dfeaf9a1", null ],
    [ "showSupervisio", "class_c_panel_events.html#aeb864da470785e50217d952016ad0c5e", null ],
    [ "showTest", "class_c_panel_events.html#a0b790b2ff98d11e6546e2dc1ee05cbbc", null ],
    [ "thumbnail", "class_c_panel_events.html#aa9ed120ea7b4acf69384a927123ebaa7", null ],
    [ "UnLang", "class_c_panel_events.html#ae1a2dcec1566345151395922a2886e4a", null ],
    [ "viewButton", "class_c_panel_events.html#a55166f0d5a298a2ff5420f4fc42f4fdf", null ],
    [ "vSizer", "class_c_panel_events.html#a071d602fa4d067269d6216670c3c40b9", null ]
];