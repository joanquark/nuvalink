var classwx_j_s_o_n_ref_data =
[
    [ "wxJSONRefData", "classwx_j_s_o_n_ref_data.html#ab601862e6d07c645f7252aca674949b8", null ],
    [ "~wxJSONRefData", "classwx_j_s_o_n_ref_data.html#af8848a87769f9d5c6f103f94ca755bd8", null ],
    [ "GetRefCount", "classwx_j_s_o_n_ref_data.html#a3836005b759d9c18111fe8ed045e92f9", null ],
    [ "wxJSONValue", "classwx_j_s_o_n_ref_data.html#ae53a89bc748ca16bf262b3656b8a55fe", null ],
    [ "wxJSONWriter", "classwx_j_s_o_n_ref_data.html#aaba216688840d3b7dde869c1c383b400", null ],
    [ "m_commentPos", "classwx_j_s_o_n_ref_data.html#a774b0a59994ca07ae84bf2e24095d5d7", null ],
    [ "m_comments", "classwx_j_s_o_n_ref_data.html#afa12212866f449402d60ea5f3f8a0ef1", null ],
    [ "m_lineNo", "classwx_j_s_o_n_ref_data.html#a967940d63b1b3edfa682acc206a4d413", null ],
    [ "m_memBuff", "classwx_j_s_o_n_ref_data.html#ae4d744770d25c7434c8550de6acb895e", null ],
    [ "m_refCount", "classwx_j_s_o_n_ref_data.html#a7bda10b1fd11f0042c1d04ecb643c386", null ],
    [ "m_type", "classwx_j_s_o_n_ref_data.html#a89f69b67f70fa867e4d863bedcdaf851", null ],
    [ "m_valArray", "classwx_j_s_o_n_ref_data.html#a7d0e5698feb099dff5c1bdf64e02bee9", null ],
    [ "m_valMap", "classwx_j_s_o_n_ref_data.html#a315af8d84ace16f8ec2bf559ac8abadd", null ],
    [ "m_valString", "classwx_j_s_o_n_ref_data.html#ad0be34037ba6b23907687e95d9f466ed", null ],
    [ "m_value", "classwx_j_s_o_n_ref_data.html#ae3530ec0f22af54222a9344780121395", null ]
];