var class_c_panel_grafic =
[
    [ "CPanelGrafic", "class_c_panel_grafic.html#aba9d02fbc1d2e391f304d237ac5a887f", null ],
    [ "~CPanelGrafic", "class_c_panel_grafic.html#adbe212f256d19f049c9417188d09e088", null ],
    [ "ActualitzaValors", "class_c_panel_grafic.html#abb12a228c02ee0e97416c71d344d3b41", null ],
    [ "AplicaValors", "class_c_panel_grafic.html#acb1fba2398b1091e7cb4afb76a5f4165", null ],
    [ "Clear", "class_c_panel_grafic.html#a7b54749d436a392c37543ddf3d71f7f5", null ],
    [ "DECLARE_EVENT_TABLE", "class_c_panel_grafic.html#ad778c1d4e347b8543dd22ecaccf54329", null ],
    [ "GeneraComponents", "class_c_panel_grafic.html#aa594bac2f4d4bafd18e4916d5b1c73c7", null ],
    [ "GetEstat", "class_c_panel_grafic.html#aa9c4ea2b512a83b9186eaa35510fcd64", null ],
    [ "OnBotoBrowse", "class_c_panel_grafic.html#af932f56652d98fd2afd1413571ed93f6", null ],
    [ "OnBotoEdicio", "class_c_panel_grafic.html#a53ead9fc55e062334d7eaa7d49e7a935", null ],
    [ "OnComboMapa", "class_c_panel_grafic.html#ad692de930069db25b42edde655ef5404", null ],
    [ "OnToolBorrar", "class_c_panel_grafic.html#a739a2270643611ac96b41bd0ea2ed0af", null ],
    [ "OnToolRele", "class_c_panel_grafic.html#a9dac1e5f35f5442671801aaeaf54a614", null ],
    [ "OnToolSensor", "class_c_panel_grafic.html#a1b42b76bc1aef837ccd55cb17b685300", null ],
    [ "OnToolSortida", "class_c_panel_grafic.html#ab7ae843327f90c198f69148164859e49", null ],
    [ "SetEstat", "class_c_panel_grafic.html#a7888a6850d13415f3a8ca0b736a23e54", null ],
    [ "SetGrups", "class_c_panel_grafic.html#a6a132c30b3279c2a29d42388f06bef39", null ],
    [ "Setmlink", "class_c_panel_grafic.html#a53a43968b52a6134a3122ff9ffe8bd70", null ],
    [ "SetModeEdicio", "class_c_panel_grafic.html#a1e9ceac54a91fbe8f54af223ba31b62a", null ],
    [ "UnSetEstat", "class_c_panel_grafic.html#a07014084d899f1efac5af62a102826a5", null ],
    [ "CPanelEditEvt", "class_c_panel_grafic.html#aaaf8346752cc6c27d95928043cf8a1e2", null ],
    [ "botoEdicio", "class_c_panel_grafic.html#a8c7586b606e1bad3204537c9314d9683", null ],
    [ "botoExaminar", "class_c_panel_grafic.html#a5fa629a5885909b9953a9c4142f0de28", null ],
    [ "CAalias", "class_c_panel_grafic.html#aebd1b095b360e92ce3653f8691c3e53b", null ],
    [ "COalias", "class_c_panel_grafic.html#af143e0bbea408a1106baba3f238ddcc2", null ],
    [ "codi", "class_c_panel_grafic.html#a5f0da64a4ffe59ebf5f5b9b9fa9cd658", null ],
    [ "comboSelMapa", "class_c_panel_grafic.html#ad0cb5872b572a68b86a2a1f115ee9a82", null ],
    [ "CZalias", "class_c_panel_grafic.html#aa335dd9421300ac5336c07697a444525", null ],
    [ "estat", "class_c_panel_grafic.html#a0e75fa5ed189596a3921bd457bbb8eb2", null ],
    [ "generat", "class_c_panel_grafic.html#a8896c2e27cd8ec4346da72101e959b44", null ],
    [ "gSizer", "class_c_panel_grafic.html#a76a689f43e4777f78b957bd3f424152d", null ],
    [ "mlink", "class_c_panel_grafic.html#a7fab7d7ec22787253564afe914c324a3", null ],
    [ "modeEdicio", "class_c_panel_grafic.html#a0ccd35f139963ca1807f9a1bd3109599", null ],
    [ "PanelId", "class_c_panel_grafic.html#ac5c527da8191e1c5e6eddb5ae249b5c6", null ],
    [ "panelMapa1", "class_c_panel_grafic.html#aabfe80682810b76a556c4cb026c1b48a", null ],
    [ "panelMapa2", "class_c_panel_grafic.html#a83061b80d3fcbd936aafc8a3ef657f5e", null ],
    [ "panelMapa3", "class_c_panel_grafic.html#aa748d95c97d0c661cc9fd2ce268ae0a7", null ],
    [ "panelMapa4", "class_c_panel_grafic.html#a1e93a8c2541266b2b889a6c2e3a6a802", null ],
    [ "PanelVersion", "class_c_panel_grafic.html#adb3ee684ad2d4ad33647b618b9699ec1", null ],
    [ "tb", "class_c_panel_grafic.html#a357d4bc673aa98cd0db3de5aa831f882", null ],
    [ "text", "class_c_panel_grafic.html#a8928e5dfbb97dc7436bc22cbf373fa60", null ],
    [ "UnLang", "class_c_panel_grafic.html#a0e33d7ecdc088d3e08b6e69e23e354c1", null ],
    [ "vSizer3", "class_c_panel_grafic.html#ac8907bb328825e10c204510228c2af15", null ]
];