var struct_t_rom_info =
[
    [ "DataSectors", "struct_t_rom_info.html#af370959643cdabe1e766abf37acc7101", null ],
    [ "DataSize", "struct_t_rom_info.html#ab5a068f923886d310f821f48083ec6b7", null ],
    [ "DataStart", "struct_t_rom_info.html#a40a71ddd6cb10b07760f1f763107734a", null ],
    [ "date", "struct_t_rom_info.html#adaa053fc9c1087720ed034f1255ff01e", null ],
    [ "FatSize", "struct_t_rom_info.html#a97311cda5825d159e9d1db0f3b9a68ce", null ],
    [ "FileCount", "struct_t_rom_info.html#a5d012eaf135a010ebce65b7154757d15", null ],
    [ "HiVer", "struct_t_rom_info.html#a8f5f1380b1a2d496ecb78e288541ed82", null ],
    [ "ID", "struct_t_rom_info.html#a5c67555dc715c3f861ac39f472212b6d", null ],
    [ "LoVer", "struct_t_rom_info.html#ada29a73dae52b63c78e1f1afb6acccd6", null ],
    [ "reserved", "struct_t_rom_info.html#af3cbd4c923286503875a443323402f42", null ],
    [ "reserved2", "struct_t_rom_info.html#a01885a10e3b06ac8d74be97a68447b5b", null ],
    [ "time", "struct_t_rom_info.html#acc8aeb4e63dfd35b9d77bf823a526f4c", null ]
];