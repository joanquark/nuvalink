var indexSectionsWithContent =
{
  0: "_abcdefghijlmnopqrstuvwx~",
  1: "_abcdefghjlmrtwx",
  2: "h",
  3: "acdefghilmnopqrstuvw~",
  4: "imnprsuv",
  5: "h",
  6: "n",
  7: "h",
  8: "h",
  9: "d"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "defines",
  8: "groups",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Macros",
  8: "Modules",
  9: "Pages"
};

