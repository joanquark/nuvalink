var class_c_mesura =
[
    [ "CMesura", "class_c_mesura.html#a9122b346ccafc5a6944c5759f02375ed", null ],
    [ "~CMesura", "class_c_mesura.html#ac15b6f4f888c59577a0dd5bb3172bfc4", null ],
    [ "Calcula", "class_c_mesura.html#a4bb9532992230b071e236cf249a5d22d", null ],
    [ "GetDescr", "class_c_mesura.html#ae212ba591a5f0ee60c20ff7afcc140c1", null ],
    [ "GetFormula", "class_c_mesura.html#a8509ded95c2d4f15fbbbf547dfa594a0", null ],
    [ "GetOffset", "class_c_mesura.html#a3f2ba72d42427bd9a0b0a30678d48fb9", null ],
    [ "GetStringVal", "class_c_mesura.html#a2441c075d83187402e88516524a70a0a", null ],
    [ "GetUnitats", "class_c_mesura.html#a7412154a72d306bde1088ac267980c6e", null ],
    [ "GetValor", "class_c_mesura.html#afbb83b5920079d0486d454586178342b", null ],
    [ "SetDescr", "class_c_mesura.html#a4e47a73c6d34243b621f1c508029cadc", null ],
    [ "SetFormula", "class_c_mesura.html#a9a0ba4c1455745adbd74774cce71ab4a", null ],
    [ "SetOffset", "class_c_mesura.html#af817ccac068aae3c12ddbe3000ec4eee", null ],
    [ "SetUnitats", "class_c_mesura.html#a69eb9799edef2b25fab6f7dba35e0cf7", null ],
    [ "SetVAC", "class_c_mesura.html#a1c21b668a24a068004d94d73a24ab9a1", null ],
    [ "SetValor", "class_c_mesura.html#af86a763405656fd3db78e356210796d0", null ],
    [ "SetValor", "class_c_mesura.html#adc68482c31b72f3dc11508e909bc700e", null ],
    [ "SetVDD", "class_c_mesura.html#ab23d9763f48f4757785a78d1809e1bdd", null ]
];