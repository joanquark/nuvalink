var class_c_serial =
[
    [ "CSerial", "class_c_serial.html#a767640ffe058af4dc546022022297780", null ],
    [ "~CSerial", "class_c_serial.html#aff5444dd7e6a9ddc43cbce0e959edf7a", null ],
    [ "Close", "class_c_serial.html#a20a2386b1cf32166f8914c12da73c4b8", null ],
    [ "IsAvailable", "class_c_serial.html#a328165ccb9bf71c270169c5db03489b5", null ],
    [ "IsOpened", "class_c_serial.html#a94d47d1b4efea5ac77fcf497c025a246", null ],
    [ "Open", "class_c_serial.html#a2a61e663d870a6426c3fb2fb5f8d9ceb", null ],
    [ "ReadData", "class_c_serial.html#ab1f5a89e337af3cde758ecb5c61a8458", null ],
    [ "ReadDataWaiting", "class_c_serial.html#a7f4d503f8cac5caf97b14d3359ab996e", null ],
    [ "SendData", "class_c_serial.html#a16d062c27f460778bf1d0aa28cfcc878", null ],
    [ "SetRTS", "class_c_serial.html#a53c3f07c372ffab7aac049acb35589d3", null ],
    [ "SetRTSToggle", "class_c_serial.html#a01d814fea9503bc9a4f7e9ecb6b9f913", null ],
    [ "WriteCommBlock", "class_c_serial.html#acf0c2fa2bf979bb57ec7c41f44988410", null ],
    [ "WriteCommByte", "class_c_serial.html#abe8cc7512f1ea2a35de64739c36945e2", null ],
    [ "m_bOpened", "class_c_serial.html#a1abb750ba5932a2867c50093219b2551", null ],
    [ "m_hIDComDev", "class_c_serial.html#a587c77012dc6befb78ef7dff74f2821a", null ],
    [ "m_OverlappedRead", "class_c_serial.html#a7b86601493a91d7cb7c019e5a7c896d2", null ],
    [ "m_OverlappedWrite", "class_c_serial.html#a1ac5e6c06e125c7b6ea2d1e074e4a21e", null ]
];