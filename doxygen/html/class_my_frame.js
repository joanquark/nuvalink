var class_my_frame =
[
    [ "MyFrame", "class_my_frame.html#a1d9a10e5e3149b49f0e0970c618c2896", null ],
    [ "~MyFrame", "class_my_frame.html#a711b2bda77494841f2e582982b80c356", null ],
    [ "NeedLogin", "class_my_frame.html#a25c9b68d56458ea294204c17eba69cc8", null ],
    [ "ObrirInstalacio", "class_my_frame.html#ae159eb3ca65eac3594a77e741f9c10dc", null ],
    [ "Setmlink", "class_my_frame.html#a026d6c954b5229baf0825e9e2b947b9c", null ],
    [ "SetTitleBar", "class_my_frame.html#aeb11d52b561f77b3ee20949bd4d567a4", null ],
    [ "arbre", "class_my_frame.html#a70469b53872c50ee1661a2367583ad3e", null ],
    [ "Clientaddr", "class_my_frame.html#aed7bb79c1b9914f0ccbf30a61cfb8b94", null ],
    [ "control", "class_my_frame.html#a4a7d22e1737ef214bd8e40f3f9b1f6d4", null ],
    [ "DirectoriBase", "class_my_frame.html#a1634ae741760f417f3d057b911f54134", null ],
    [ "fileName", "class_my_frame.html#a9f30b689d313f08445eb6614c5e93c5b", null ],
    [ "panel", "class_my_frame.html#a2c05f28266e5ca4b59875b70fcfb65b1", null ],
    [ "panelEdit", "class_my_frame.html#ab7257f81b95a3b361f97f7008f2c3d11", null ],
    [ "panelLlegenda", "class_my_frame.html#a7dcadb22d979e3930aa5b92ce9bea5aa", null ],
    [ "ServerAddress", "class_my_frame.html#aaf8fe3ec8dd4e9dbda225b88c4ec26aa", null ],
    [ "ServerPort", "class_my_frame.html#a815ef7eba1a853dc2c8155119c0642e6", null ],
    [ "socket", "class_my_frame.html#ad1e99f7cc9e0df9bd5abd4ee974becdc", null ],
    [ "sockets", "class_my_frame.html#a6e78f718acc43f6d10c86ff6075ffe6e", null ],
    [ "tb", "class_my_frame.html#a16448087bbb4737025fb491c27cbbff3", null ],
    [ "timerConexio", "class_my_frame.html#a620e4e3eb977bcd97454fd30a7c2dbd6", null ],
    [ "timerHello", "class_my_frame.html#a3f2a9ed3d810779dfc85fd972627ec90", null ]
];