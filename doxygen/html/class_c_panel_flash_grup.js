var class_c_panel_flash_grup =
[
    [ "CPanelFlashGrup", "class_c_panel_flash_grup.html#ac4e075c751f5197454911c02d9032029", null ],
    [ "~CPanelFlashGrup", "class_c_panel_flash_grup.html#aa242dfdbdc7122e26cd4925e95f7ecc5", null ],
    [ "ActualitzaValors", "class_c_panel_flash_grup.html#aa0baa9b1857d58ba60bd8f78995dd480", null ],
    [ "AplicaValors", "class_c_panel_flash_grup.html#a9c75991c8b07f63effc902fd80959bc7", null ],
    [ "Clear", "class_c_panel_flash_grup.html#afe9b53dc2d747a2108fda4fa433f7f12", null ],
    [ "GeneraComponents", "class_c_panel_flash_grup.html#a96254a6478ece1692c097fc29a5db095", null ],
    [ "GetFlashGrup", "class_c_panel_flash_grup.html#a1843478f396fcd76a4e4ae935c8494ef", null ],
    [ "SetFlashGrup", "class_c_panel_flash_grup.html#a8b989ee0b38674776deb299ec4493a69", null ],
    [ "UnSetFlashGrup", "class_c_panel_flash_grup.html#a3fc165b780853239f4f6f3eaa3756e7b", null ],
    [ "CPanelEditEvt", "class_c_panel_flash_grup.html#aaaf8346752cc6c27d95928043cf8a1e2", null ],
    [ "grid", "class_c_panel_flash_grup.html#a4709cc8093f24d2d93c78f5fdf875d4c", null ],
    [ "grupFlash", "class_c_panel_flash_grup.html#a7a7d0df79a809539f58b59322b8a2537", null ],
    [ "panelFitxer", "class_c_panel_flash_grup.html#a8f850381bf9242497acbfa95b83008fe", null ],
    [ "panelWebserver", "class_c_panel_flash_grup.html#a3c2d9d5fe7bf7c2bba92e8ac67b6656a", null ],
    [ "UnLang", "class_c_panel_flash_grup.html#a062b863a9f04586e3b6943507770116d", null ],
    [ "vSizer", "class_c_panel_flash_grup.html#ae9ac41d0188c1bef464bef432e834269", null ]
];