
#include "devices.h"
#include "util.h"
#include <stdio.h>
#include <string.h>
#define _wx(x) (wxString)((string)x).c_str()

bool CDevice::Construct(BYTE* fr,int len)
{
    TNuvaHard nv;

    for (int i=0;i<sizeof(TNuvaHard);i++)
        nv.v[i]=fr[i];

    return Construct(nv,len);
}

bool CDevice::Construct(TNuvaHard hard,int len)
{
    this->device=hard;

    // from 01/05/20, DEVICEbootconst is delivered, so we know which checksum algth will implement.
    if (len<sizeof(TNuvaHard) || device.Field.bVERH==0xFF  || device.Field.bVERL==0xFF){
        // this is a device older than 01/05/20
        device.Field.bVERH=0x02;
        device.Field.bVERL=0x10;
        device.Field.bDD=1;
        device.Field.bMM=1;
        device.Field.bYY=19;
    }

    this->DevInf.id=GetPanelDescr(device.Field.TPanel);

    string rev="";
    rev+=nibble2ascii(device.Field.BOARDV>>4);
    rev+=nibble2ascii(device.Field.BOARDV);
    this->DevInf.hard=rev;

    string ver="";
    ver+=nibble2ascii(device.Field.VERH);
    ver+=".";
    ver+=nibble2ascii(device.Field.VERL>>4);
    ver+=nibble2ascii(device.Field.VERL);             // ho deixcem com 525 per tal de poder crear models nuvalink m�s f�cilment.

    this->DevInf.version=ver;

    string dat;
    dat+=iToS(device.Field.DD);
    dat+="/";
    dat+=iToS(device.Field.MM);
    dat+="/";
    dat+=iToS(device.Field.YY);

    this->DevInf.verdate=dat;


    string bver="";
    bver+=nibble2ascii(device.Field.bVERH);
    bver+=".";
    bver+=nibble2ascii(device.Field.bVERL>>4);
    bver+=nibble2ascii(device.Field.bVERL);             // ho deixcem com 525 per tal de poder crear models nuvalink m�s f�cilment.

    this->DevInf.bversion=bver;

    string bdat;
    bdat+=iToS(device.Field.bDD);
    bdat+="/";
    bdat+=iToS(device.Field.bMM);
    bdat+="/";
    bdat+=iToS(device.Field.bYY);

    this->DevInf.bverdate=bdat;

    this->DevInf.flashid=nibble2ascii(device.Field.FLASHID>>4);
    this->DevInf.flashid+=nibble2ascii(device.Field.FLASHID);

    return true;
}

string CDevice::GetPanelDescr(unsigned char TPanel)
{
    string Panel="unknown";
    Panel="Cirrus4T";
    if (TPanel==TPANEL_CIRRUS4T){
        Panel="Cirrus4T";
    }else if (TPanel==TPANEL_CIRRUS4A6){
        Panel="Cirrus4A6";
    }else if (TPanel==TPANEL_STR4TA){
        Panel="Str4ta";
    }else if (TPanel==TPANEL_STR4TA4A6){
        Panel="Str4ta4A6";
    }else if (TPanel==TPANEL_LTM400){
        Panel="LTM400";
    }else if (TPanel==TPANEL_LTIO){
        Panel="LTIO";
    }else if (TPanel==TPANEL_NUVATRACK){
        Panel="NuvaTrack";
    }else if (TPanel==TPANEL_NUVATRACK4A6){
        Panel="NuvaTrack4A6";
    }
    return Panel;

}

string CDevice::GetDevRev()
{
    string rev="FW:";
    rev+=nibble2ascii(device.Field.VERH);
    rev+=".";
    rev+=nibble2ascii(device.Field.VERL>>4);
    rev+=nibble2ascii(device.Field.VERL);
    rev+=" ";
    rev+=iToS(device.Field.DD);
    rev+="/";
    rev+=iToS(device.Field.MM);
    rev+="/";
    rev+=iToS(device.Field.YY);
    rev+=" B:";
    rev+=nibble2ascii(device.Field.BOARDV>>4);
    rev+=nibble2ascii(device.Field.BOARDV);
    rev+=" F:";
    rev+=nibble2ascii(device.Field.FLASHID>>4);
    rev+=nibble2ascii(device.Field.FLASHID);

    return rev;
}

string CDevice::GetDevInfo()
{
    string inf;

    inf =  "Device   : "+DevInf.id+"\r";
    inf += "soft ver : "+DevInf.version+"\r";
    inf += "soft date: "+DevInf.verdate+"\r";
    inf += "hard id  : "+DevInf.hard+"\r";
    inf += "flash id : "+DevInf.flashid+"\r";
    inf += "boot ver : "+DevInf.bversion+"\r";
    inf += "boot date: "+DevInf.bverdate+"\n";
    return inf;
}


TDevInf CDevice::GetDevInf()
{
    return DevInf;
}

string CDevice::GetId()
{
    return DevInf.id;
}

string CDevice::GetVersion()
{
    return DevInf.version;
}

string CDevice::GetBootVersion()
{
    return DevInf.bversion;
}


string CDevice::GetVersionFW()
{
    string ver="";
    ver+=nibble2ascii(device.Field.VERH);
    ver+=nibble2ascii(device.Field.VERL>>4);
    ver+=nibble2ascii(device.Field.VERL);             // ho deixcem com 525 per tal de poder crear models nuvalink m�s f�cilment.

    return ver;
}

string CDevice::GetBootVersionFW()
{
    string ver="";
    ver+=nibble2ascii(device.Field.bVERH);
    ver+=nibble2ascii(device.Field.bVERL>>4);
    ver+=nibble2ascii(device.Field.bVERL);             // ho deixcem com 525 per tal de poder crear models nuvalink m�s f�cilment.

    return ver;
}

string CDevice::GetFwDate()
{
    return DevInf.verdate;
}

int CDevice::GetIntVersionFW()
{
    string ver="";
    ver+=nibble2ascii(device.Field.VERH);
    ver+=nibble2ascii(device.Field.VERL>>4);
    ver+=nibble2ascii(device.Field.VERL);

    return sToI((ver));
}


int CDevice::GetIntBootVersionFW()
{
    string ver="";
    ver+=nibble2ascii(device.Field.bVERH);
    ver+=nibble2ascii(device.Field.bVERL>>4);
    ver+=nibble2ascii(device.Field.bVERL);

    return sToI((ver));
}

int CDevice::GetIntDateFW()
{
    int date=0;
    date+=device.Field.YY*10000;
    date+=device.Field.MM*100;
    date+=device.Field.DD;

    return date;
}

int CDevice::GetIntBootDateFW()
{
    int date=0;
    date+=device.Field.bYY*10000;
    date+=device.Field.bMM*100;
    date+=device.Field.bDD;

    return date;
}


bool CDevice::CheckFirmwareFile(string file,string& error)
{
    char* pc;

    string devid=this->GetPanelDescr(this->device.Field.TPanel);
    wxString version=_wx(this->GetVersionFW());
    wxString wxfile=_wx(file);
    int vpos=wxfile.Find("V0");
    wxString filever=wxfile.substr(vpos+2,3);
    if (wxfile.Contains(version)==false){
        error="Version compatibility ERROR!";
        if (version=="205")
            return false;
        if (version=="206" && filever=="210")
            int kk=0;
        else if (version=="210" && filever=="220")
            int kkk=0;
        else if (version=="220" && filever=="210")
            int kkkk=0;
        else
            return false;
    }

    if (!(pc=strstr((char*)&file[0],(char*)&devid[0]))){
        error="Device ERROR!";
        return false;
    }
    pc+=Strlen((char*)&devid[0]);
    pc+=7;              // point to boardV (2A,...1A etc...)

    TNuvaHard filehard;

    filehard.Field.BOARDV=ascii2nibble(*pc++)<<4;
    filehard.Field.BOARDV|=ascii2nibble(*pc++);

    return true;
}


int CDevice::GetEventQueueSize()
{
    int res = LONG_FLASH_EVENTQUEUE_8MB;

    if (device.Field.FLASHID==SFLASH_16MBIT)
        res = LONG_FLASH_EVENTQUEUE_16MB;
    else if (device.Field.FLASHID==SFLASH_32MBIT)
        res = LONG_FLASH_EVENTQUEUE_32MB;
    else if (device.Field.FLASHID==SFLASH_64MBIT)
        res = LONG_FLASH_EVENTQUEUE_64MB;
    else if (device.Field.FLASHID==SFLASH_128MBIT)
        res = LONG_FLASH_EVENTQUEUE_128MB;
    else if (device.Field.FLASHID==SFLASH_256MBIT)
        res = LONG_FLASH_EVENTQUEUE_256MB;

    return res;
}

int CDevice::GetUpdFwAddress()
{
    int res = ADD_INI_FIRMWARE_8MB;
    if (device.Field.FLASHID==SFLASH_16MBIT)
        res = ADD_INI_FIRMWARE_16MB;
    else if (device.Field.FLASHID==SFLASH_32MBIT)
        res = ADD_INI_FIRMWARE_32MB;
    else if (device.Field.FLASHID==SFLASH_64MBIT)
        res = ADD_INI_FIRMWARE_64MB;
    else if (device.Field.FLASHID==SFLASH_128MBIT)
        res = ADD_INI_FIRMWARE_128MB;
    else if (device.Field.FLASHID==SFLASH_256MBIT)
        res = ADD_INI_FIRMWARE_256MB;

    return res;
}
