/*
AES.h:  C++ implementation of the AES(Rijndael) algorithm.
                (Borland C++Builder - attention, use AnsiString, TStream,
                TFileStream!)
                Base on Java code,
                authors: Raif S. Naffah, Paulo S. L. M. Barreto

Copyright (C) 2007 by Valery Maksimov

This implementation on C++ is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This is code (AES.cpp) is top optimized for file read-write operation.

                                            -- Maksimov V.A.
*/

/*
Rijndael (pronounced Reindaal) is a block cipher, designed by Joan Daemen and Vincent Rijmen as a candidate algorithm for the AES.
The cipher has a variable block length and key length. The authors currently specify how to use keys with a length
of 128, 192, or 256 bits to encrypt blocks with al length of 128, 192 or 256 bits (all nine combinations of
key length and block length are possible). Both block length and key length can be extended very easily to
multiples of 32 bits.
Rijndael can be implemented very efficiently on a wide range of processors and in hardware.
This implementation is based on the Java Implementation used with the Cryptix toolkit found at:
http://www.esat.kuleuven.ac.be/~rijmen/rijndael/rijndael.zip
Java code authors: Raif S. Naffah, Paulo S. L. M. Barreto
This Implementation was tested against KAT test published by the authors of the method and the
results were identical.
*/

//---------------------------------------------------------------------------

#ifndef AES_UnitH
#define AES_UnitH
//---------------------------------------------------------------------------
#endif

//#include <system.hpp>
//#include <classes.hpp>

using  namespace std;

/*
OS_Buffer_Size - value, depending of OS and optimal for file operations,
in Win32 - 64K (65536 byte - file IO-buffer size).
Attention! OS_Buffer_Size must be only a multiple of 16 bytes !
Actually, optimal: 1024 (and above: 2048, 3072, ...), optimum-maximum is
65536 (no more than 65536...)
                                                        --MVA
*/
#define OS_Buffer_Size  65536

class TAES
{

private:

	enum { ECB=0, CBC=1, CFB=2 };
	enum { DEFAULT_BLOCK_SIZE=16 };
	enum { MAX_BLOCK_SIZE=32, MAX_ROUNDS=14, MAX_KC=8, MAX_BC=8 };

	static int Mul(int a, int b)   	//Auxiliary Functions: Multiply two elements of GF(2^m)
	{
		return (a != 0 && b != 0) ? sm_alog[(sm_log[a & 0xFF] + sm_log[b & 0xFF]) % 255] : 0;
	}

	static int Mul4(int a, char b[]) 	//Convenience method used in generating Transposition Boxes
	{
		if(a == 0)
			return 0;
		a = sm_log[a & 0xFF];
		int a0 = (b[0] != 0) ? sm_alog[(a + sm_log[b[0] & 0xFF]) % 255] & 0xFF : 0;
		int a1 = (b[1] != 0) ? sm_alog[(a + sm_log[b[1] & 0xFF]) % 255] & 0xFF : 0;
		int a2 = (b[2] != 0) ? sm_alog[(a + sm_log[b[2] & 0xFF]) % 255] & 0xFF : 0;
		int a3 = (b[3] != 0) ? sm_alog[(a + sm_log[b[3] & 0xFF]) % 255] & 0xFF : 0;
		return a0 << 24 | a1 << 16 | a2 << 8 | a3;
	}

	int MakeKey(char const* key, int keylength=DEFAULT_BLOCK_SIZE, int blockSize=DEFAULT_BLOCK_SIZE);
	int DefEncryptBlock(char const* in, char* result);
	int DefDecryptBlock(char const* in, char* result);


public:

        TAES();
        virtual ~TAES();

//        bool SetPassword(AnsiString Password);
//        bool ResetPassword();
        bool ECB_BlockEncipher(char* InBuff,char* OutBuff, unsigned int Size);
        bool ECB_BlockDecipher(char* InBuff,char* OutBuff, unsigned int Size);
//        void ECB_FileEncipher(TStream *InFile, TStream *OutFile);   // ECB mode
//        int  ECB_FileDecipher(TStream *InFile, TStream *OutFile);   // ECB mode
        bool BlockEncipher(char* InBuff,char* OutBuff, unsigned int Size);  // CBC - mode
        bool BlockDecipher(char* InBuff,char* OutBuff, unsigned int Size);  // CBC - mode
//        void FileEncipher(TStream *InFile, TStream *OutFile);       // CBC mode - base mode
//        int  FileDecipher(TStream *InFile, TStream *OutFile);        // CBC mode - base mode

private:
        unsigned char Sin[16], Sout[16];        // internal input-output buffer

        static const int sm_alog[256];
        static const int sm_log[256];
        static const char sm_S[256];
        static const char sm_Si[256];
        static const int sm_T1[256];
        static const int sm_T2[256];
        static const int sm_T3[256];
        static const int sm_T4[256];
        static const int sm_T5[256];
        static const int sm_T6[256];
        static const int sm_T7[256];
        static const int sm_T8[256];
        static const int sm_U1[256];
        static const int sm_U2[256];
        static const int sm_U3[256];
        static const int sm_U4[256];
        static const char sm_rcon[30];
        static const int sm_shifts[3][4][2];

        bool m_bKeyInit;                	    //Key Initialization Flag
        int m_Ke[MAX_ROUNDS+1][MAX_BC];         //Encryption (m_Ke) round key
        int m_Kd[MAX_ROUNDS+1][MAX_BC];         //Decryption (m_Kd) round key
        unsigned int m_keylength;             	//Key Length
        unsigned int m_blockSize;            	//Block Size
        unsigned int m_iROUNDS;                 //Number of Rounds
        int tk[MAX_KC];                        	//Auxiliary private use buffers
        int a[MAX_BC];
        int t[MAX_BC];
};


/*
Internal ERROR Code:

-11 Object not Initialized
-12 Data not multiple of Block Size
-13 Empty key
-14 Incorrect key length
-15 Incorrect bloc length
*/

/*
ERROR Code:

int FileDecipher(TStream *InFile, TStream *OutFile)

 1: - all Ok
-1:ERROR! incorrect input file - size must be a multiple of 16 bytes
-2:ERROR! File-additional not digit!
-3:ERROR! incorrect file-additional - must be  0-16
-10: terminated by user (No error)

int  ECB_FileDecipher(TStream *InFile, TStream *OutFile)

 1: - all Ok
-1:ERROR! incorrect input file - size must be a multiple of 16 bytes
-2:ERROR! Fileadditional not digit!
-3:ERROR! incorrect fileAdditional - must be  0-16


bool SetPassword(AnsiString Password)
        -false - internal error in MakeKey function

bool TAES::BlockEncipher(char* InBuff,char* OutBuff, unsigned int Size)
bool TAES::BlockDecipher(char* InBuff,char* OutBuff, unsigned int Size)
bool TAES::ECB_BlockEncipher(char* InBuff,char* OutBuff, unsigned int Size)
bool TAES::ECB_BlockDecipher(char* InBuff,char* OutBuff, unsigned int Size)

        -false - ERROR! incorrect input buffer - size must be a multiple of 16 bytes!
*/
