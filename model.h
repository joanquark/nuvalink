#ifndef MODEL_H
#define MODEL_H

// basic file operations
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#include "myXmlParser\myxmlparser.h"
#include "myXmlParser\XmlNotify.h"
#include "codi.h"
#include "grup.h"
#include "grupflash.h"
#include "util.h"
#include "Status.h"
#include "mesura.h"
#include "llistaevents.h"
#include "msgs.h"
#include "fitxerflash.h"
#include "devices.h"

#define _wx(x) (wxString)((string)x).c_str()

class CModel: public XmlNotify
{
    CDevice device;
    string id;              // these are read from modelfile.
    string model;
    string versio;
    string verdate;
    string busRemot, busDigital;
    int numZones,numUsers,numUsersExt,numSortides;
    int vrxZona,wrxUsers,sizeofMsk;
    bool teAlias;

    CGroup grupPare, grupEstat;

    CEventList lEvents;

    vector<CCodi *> codis;
    vector<CGroup *> grups;
    vector<CGroupFlash *> grupsFlash;

    CGroup *tmpGrup;
    CCodi *tmpCodi;
    CGroupFlash *tmpGrupFlash;
    bool dinsNodeEstat;

    int tmpCol;
    CMesura *tmpMesura;
    string lastNode;
    int formula;
    CFitxerFlash *tmpFitxer;
    bool teEstat;

    public:
        CLanguage *Lang;
        CModel();
        CModel(CLanguage *lang);
        CModel(string &id, string &model, string &versio,CLanguage* lang);

        void Clear();
        ~CModel();
        CStatus estat;
        bool Genera();

        bool SetDevice(CDevice device);
        CDevice GetDevice();
        bool SetModel(wxString model);
        bool SetId(wxString id);
        bool SetDescr(wxString descr);
        bool SetVersion(wxString Versio);

        bool SetNumZ(int numZones);
        bool SetNumUsers(int numUsers);
        bool SetNumOuts(int sortides);
        bool SetNumUsersExt(int numUsers);
        int  GetNumUsersExt();
        bool SetNumVRxZona(int vrxZona);
        bool HasStatus();
        bool SetStatus();
        bool TeAlias();
        bool SetTeAlias(bool alias);
        bool SetsizeofMsk(int size);
        int GetsizeofMsk();

        string& GetModel();
        string& GetVersio();
        string& GetId();
        string& GetBusLocal();
        string& GetBusRemot();
        string& GetBusDigital();
        string& GetDescr();
        CStatus *GetStatus();
        string GetModelByDevice(unsigned char dev);
        CEventList *GetEventList();
        int GetNumZones();
        int GetNumUsers();
        int GetNumSortides();
        int GetNumVRxZona();

        int AddCodi(CCodi *codi);
        int AddGrup(CGroup *grup);
        int AddGrupFlash(CGroupFlash *grupFlash);
        int GetNumCodis();
        int GetNumGrups();
        int GetNumGrupsFlash();
        CCodi *GetCodi(int index);
        CCodi *GetCodiById(wxString id);
        CCodi *GetCodiByAdd(wxString add);
        CGroup *GetGroup(int index);
        CGroup *GetGroupById(wxString id);
        CGroup *GetFatherGroup();
        CGroup *GetStatusGroup();
        CGroupFlash *GetGroupFlash(int index);
        CGroupFlash *GetGroupFlashById(wxString id);

        bool GetChanges();
        void ClrChanges();

        //XmlNotify
        void foundNode		( wxString & name, wxXmlNode* attributes );
        void foundElement	( wxString & name, wxString & value, wxXmlNode* attributes );


};


#endif
