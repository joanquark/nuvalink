#ifndef CLOUDDIALOG_H
#define CLOUDDIALOG_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(CloudDialog)
	#include <wx/button.h>
	#include <wx/dialog.h>
	#include <wx/listctrl.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	//*)x
#endif

#define _wx(x) (wxString)((string)x).c_str()

//(*Headers(CloudDialog)
#include <wx/lcdwindow.h>
#include <wxSpeedButton.h>
//*)

class CloudDialog: public wxDialog
{
	public:

		CloudDialog(wxWindow* parent,wxWindowID id=wxID_ANY);
		virtual ~CloudDialog();

		//(*Declarations(CloudDialog)
		wxButton* Back;
		wxButton* Forward;
		wxLCDWindow* PosLcd;
		wxLCDWindow* TotalLcd;
		wxListCtrl* Listab;
		wxSpeedButton* Close;
		wxSpeedButton* Find;
		wxSpeedButton* Open;
		wxStaticText* StaticText1;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxTextCtrl* TextFilter;
		wxTextCtrl* TxtFilterGr;
		//*)
        void SetParams(wxString user,wxString pass,int cnt,wxString IP,int port,wxString *ver);
        wxString GetJSONDev();
	private:
        wxString SysUser;
        wxString SysPass;
        wxString DevIP;
        wxString JSONDev;
        wxString *versio;
        int DevCnt;
        int DevIndx;
        int DevPort;
		//(*Identifiers(CloudDialog)
		static const long ID_LISTCTRL1;
		static const long ID_LCDWINDOW1;
		static const long ID_STATICTEXT2;
		static const long ID_LCDWINDOW2;
		static const long ID_BUTTON2;
		static const long ID_BUTTON1;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		static const long ID_STATICTEXT1;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT3;
		static const long ID_TEXTCTRL2;
		static const long ID_SPEEDBUTTON3;
		//*)

		//(*Handlers(CloudDialog)
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnSpeedButton2LeftClick(wxCommandEvent& event);
		void OnSpeedButton4LeftClick(wxCommandEvent& event);
		void OnForwardClick(wxCommandEvent& event);
		void OnBackClick(wxCommandEvent& event);
		void OnFindLeftClick(wxCommandEvent& event);
		void OnListabBeginDrag(wxListEvent& event);
		//*)

		void Query();

	protected:

		void BuildContent(wxWindow* parent,wxWindowID id);
		DECLARE_EVENT_TABLE()
};

#endif
