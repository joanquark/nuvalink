#ifndef PANELMESURA_H
#define PANELMESURA_H

#include <iostream>
#include <string>
using namespace std;

#include <wx/panel.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>

#include "mesura.h"
//#include "paneledit.h"

#define _wx(x) (wxString)((string)x).c_str()

class CPanelMesura: public wxPanel
{
    friend class CPanelEditEvt;
    protected:
        wxPoint m_tmppoint;
        wxSize  m_tmpsize;
        wxPoint& VwXSetwxPoint(long x,long y);
        wxSize& VwXSetwxSize(long w,long h);

        CMesura *mesura;
        wxStaticText *label;
        wxStaticText *valor;
        wxBoxSizer *hSizer;

    public:
        CPanelMesura(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage* lang=0);
        void Clear();
        ~CPanelMesura();
        CLanguage*    Language;
        bool SetMesura(CMesura *mesra);
        CMesura *GetMesura();
        void UnSetMesura();
        bool AplicaValors();
        bool ActualitzaValors();

    protected:
        void GeneraComponents();
};
// end CPanelMesura

#endif
