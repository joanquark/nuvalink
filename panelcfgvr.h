#ifndef PANELCFGVR_H
#define PANELCFGVR_H

#include <iostream>
using namespace std;

class Cmlink;

#include <wx/panel.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include "estat.h"
#include "msgs.h"
#include "grup.h"
#include "codi.h"

typedef struct{
    unsigned char num;
    unsigned char Tsens;
    unsigned char power;
    unsigned char lasttest;
    unsigned char code[3];
    unsigned char BattLevel;
    unsigned char TempLevel;
}TConfigVRdata;


#define M_TYPE_SENSORCODE			0xF00000
	#define 	M_TYPE_SC_DEFAULT			0x000000			// LEGACY codes, no type information
	#define 	M_TYPE_SC_PIR				0x100000			// PIR-W PIR-BUS
	#define 	M_TYPE_SC_PIRCAM			0x200000			// PIRCAM-W, PIRCAM-BUS
		#define MSB_PIRCAM				0x20
	#define 	M_TYPE_SC_CN				0x300000			// CN-W
		#define MSB_CN						0x30
	#define  M_TYPE_SC_CNCAM			0x400000			// ya se ver� si sale.
	#define 	M_TYPE_SC_PIRCN			0x500000			// PIR-CN-W, PIR-CN-BUS
	#define 	M_TYPE_SC_PIRCAMCN		0x600000			// PIRCAM-CN-W, PIRCAM-CN-BUS
		#define MSB_PIRCAMCN				0x60
	#define  M_TYPE_SC_PTX				0x700000			// TR-P4
		#define MSB_PTX					0x70
	#define  M_TYPE_SC_EMG				0x800000			// TR-EMG
		#define MSB_EMG					0x80
	#define  M_TYPE_SC_ARES				0x900000			// ARES-BUS/W
	#define  M_TYPE_SC_ARESCAM			0xA00000			// ARESCAM-BUS/W
		#define MSB_ARESCAM				0xA0
	#define  M_TYPE_SC_SIR				0xB00000			// this is a SIREN (BELL...)
	#define  M_TYPE_SC_CAME				0xC00000			// CAM-ext
		#define MSB_CAME					0xC0
	#define  M_TYPE_SC_RES4				0xD00000			// RESERVED
	#define  M_TYPE_SC_DEFKEYPAD		0xE00000			// LEGACY KEYPADS (TCL-SENSE-W/BUS:ICON)
	#define  M_TYPE_SC_FIREDET 		0xF00000			    // TRFUM
		#define MSB_FUM						0xF0




#define MAX_PANELCFGVR_ZONE     64
#define MIN_PANELCFGVR_ZONE     1

class CPanelCfgVR: public wxPanel
{
    friend class CPanelEditEvt;
    protected:
        wxPoint m_tmppoint;
        wxSize  m_tmpsize;
        wxPoint& VwXSetwxPoint(long x,long y);
        wxSize& VwXSetwxSize(long w,long h);

//        wxGrid *grid;
//        wxBoxSizer *vSizer;
        CStatus *estat;
        CCodi *codi;
        int   fixzone;

		wxStaticText *minuts;
		wxStaticText *Dbm;
		wxStaticText *RfLevel;
		wxStaticText *Test;
		wxStaticText *Batttxt;
		wxStaticText *Temptxt;
		wxStaticText *BatttxtVal;
		wxStaticText *TemptxtVal;

        wxGauge *BattGauge;
        wxGauge *TempGauge;

		wxTextCtrl *TestEdit;
		wxGauge *RfGauge;

		wxBitmapButton *BotoDelete;
		wxBitmapButton *BotoRead;
		wxBitmapButton *BotoAdd;
		wxStaticText *WxStaticText1;
		wxComboBox *WxComboBox1;
		wxTextCtrl *ZoneCodeEdit;
		wxStaticText *ZoneNum;
		wxTextCtrl *ZoneEdit;
		wxStaticText *Zona;

		wxStaticBox *ConfigVR;
		wxPanel *WxPanel1;

		enum
		{
			////GUI Enum Control ID Start
            ID_TXTBATT  = 1030,
            ID_TXTTEMP  = 1029,
            ID_TXTBATTVAL=1028,
            ID_TXTTEMPVAL=1027,
            ID_TEMPGAUGE = 1026,
            ID_BATTGAUGE = 1025,
			ID_MINUTS = 1024,
			ID_DBM = 1023,
			ID_NIVEL = 1022,
			ID_TEST = 1021,
			ID_TESTEDIT = 1020,
			ID_RFGAUGE = 1019,
			ID_BOTODELETE = 1018,
			ID_BOTOREAD = 1017,
			ID_BOTOADD = 1016,
			ID_WXSTATICTEXT1 = 1015,
			ID_WXCOMBOBOX1 = 1014,
			ID_ZONECODEEDIT = 1013,
			ID_ZONENUM = 1012,
			ID_ZONEEDIT = 1010,
			ID_ZONAS = 1009,
			ID_CONFIGVR = 1008,
			ID_WXPANEL1 = 1007,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};

    public:

        Cmlink *mlink;
        string PanelVersion;
        string PanelId;
        CLanguage*    Language;
        bool SetGrups(CGroup *grupZAlias);
        string CZalias[64];

        CPanelCfgVR(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage *lang=0,Cmlink* eli=0,int fzone=-1);
        void Clear();
        ~CPanelCfgVR();

        bool SetEstat(CStatus *estat);
        CStatus *GetEstat();
        void UnSetEstat();
        bool AplicaValors();
        bool ActualitzaValors();
        void BotoAddClick(wxCommandEvent& event);
        void BotoDeleteClick(wxCommandEvent& event);
        void BotoReadClick(wxCommandEvent& event);
        void Read();

    protected:
        void GeneraComponents();

	private:
		DECLARE_EVENT_TABLE();
};

#endif
