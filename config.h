#ifndef CONFIG_H
#define CONFIG_H

#include <iostream>
#include <fstream>
#include <map>
#include <string>
#include <vector>
using namespace std;

class CConfig;
#include "myXmlParser\myxmlparser.h"
#include "myXmlParser\XmlNotify.h"
#include "util.h"
#include "Connection.h"
#include "msgs.h"
#include "cipher.h"

#define MAX_LAST_FILESOPEN    4

class CConfig: public XmlNotify
{
    map<wxString, wxString> *tmpMap;
    wxString msgError, lastTag, blank, SysUser,SysPass;
    wxString sessionServer;
    bool tagConexions;
    int ConnectionType;
    wxString jsonwithdevices;
    wxString CodeLot;
    wxString user;

    public:
        int clouddevices;
        map <wxString, wxString> oem, conLocal, conModem, conCsd, conTcpip, eng;
        vector <wxString> ultims;

        string login;
        wxString cat;             // categorias.
        wxString lang;
        CLanguage *Lang;

        CConfig();
        ~CConfig();
        void Clear();
        bool Load();
        bool Save();

        wxString& GetConnectionParam(const wxString& param);
        wxString& GetParamOem(const wxString& param);
        wxString GetParamEng(wxString param);
        wxString  GetJSONDevices();
        wxString  GetCodeLot();
        void      SetCodeLot(wxString lot);
        wxString  GetSysUser();
        wxString  GetSysPass();
        wxString  GetSessionServer();
        void      SetRememberUser(wxString user);
        void      SetRememberPass(wxString pass);
        void      SetRememberServer(wxString server);
        void      SetSessionServer(wxString s);
        bool      SetParamConexio(wxString param, wxString valor);
        int       GetConnectionType();
        bool      SetConnectionType(int tipus);

        wxString& GetLast(int index);
        bool AddLast(wxString& nomFitxer);

        wxString& GetLastError();
        wxString& GetEngCode();
        int Login(wxString user,wxString codin);
        int privlevel;
        bool cloudmode;

        bool SetParamTcpIp(wxString param,wxString ip);
        bool IsCat(wxString cat);
        bool SetCat(wxString cat);
        wxString GetParamTcpIp(wxString param);
        //XmlNotify
        void foundNode		( wxString & name, wxXmlNode* attributes );
        void foundElement	( wxString & name, wxString & value, wxXmlNode* attributes );

    private:
        bool GenConfigXML(string *xml);
};

#endif
