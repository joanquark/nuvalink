// Serial.cpp

#include "../serial/Serial.h"


#define TYPE_USB_CONNECTION       0
#define TYPE_RS485_CONNECTION     1
#define TYPE_TCPIP_CONNECTION     2
#define TYPE_CLOUD_CONNECTION     3

CSerial::CSerial()
{
	memset( &m_OverlappedRead, 0, sizeof( OVERLAPPED ) );
 	memset( &m_OverlappedWrite, 0, sizeof( OVERLAPPED ) );
	m_hIDComDev = NULL;
	m_bOpened = FALSE;

}

CSerial::~CSerial()
{

	Close();

}

//Funcio per saber quins ports hi ha disponibles
BOOL CSerial::IsAvailable( int nPort )
{
    HANDLE handleCom;
    char szPort[15];

    wsprintf( szPort, "COM%d", nPort );
	handleCom = CreateFile( szPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED, NULL );
	if ((handleCom == NULL) || (handleCom == INVALID_HANDLE_VALUE)) {
        if (GetLastError() == ERROR_FILE_NOT_FOUND)
            return( FALSE );
    }
    else
        CloseHandle( handleCom );

    return ( TRUE );
}

BOOL CSerial::Open( int nPort, int nBaud, int bitsData, int bitsStop, int bitsParitat,bool RTSCtrl,bool VCP )
{

	if( m_bOpened ) return( TRUE );

	char szPort[15];
	char szComParams[50];

    //no se perque pero pels ports COM10 i superiors la funcio normal
    //no funcionava be. He trobat per Inet un programa d'un paio que li passava
    //lo mateix i feia servir la segona opci� \\.\COMx pels ports COM10 i superiors
    if (nPort < 10)
        wsprintf( szPort, "COM%d", nPort );
    else
        wsprintf( szPort, "\\\\.\\COM%d", nPort );


    int attrib=0;
//    if (VCP)
//        attrib=FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED;

	m_hIDComDev = CreateFile( szPort, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, attrib, NULL );

    DCB dcb;
    GetCommState(m_hIDComDev,&dcb);

	if( m_hIDComDev == NULL ) return( FALSE );
/*    if (VCP){
        memset( &m_OverlappedRead, 0, sizeof( OVERLAPPED ) );
        memset( &m_OverlappedWrite, 0, sizeof( OVERLAPPED ) );
    }*/

	COMMTIMEOUTS CommTimeOuts;
	CommTimeOuts.ReadIntervalTimeout = 0xFFFFFFFF;
	CommTimeOuts.ReadTotalTimeoutMultiplier = 0;
	CommTimeOuts.ReadTotalTimeoutConstant = 0;
	CommTimeOuts.WriteTotalTimeoutMultiplier = 0;
	CommTimeOuts.WriteTotalTimeoutConstant = 5000;
	SetCommTimeouts( m_hIDComDev, &CommTimeOuts );

    char cp;
    if (bitsParitat==0) cp = 'n';
    else cp = 'e';

	wsprintf( szComParams, "COM%d:%d,%c,%d,%d", nPort, nBaud, cp, bitsData, bitsStop);

/*    if (VCP){
        m_OverlappedRead.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
        m_OverlappedWrite.hEvent = CreateEvent( NULL, TRUE, FALSE, NULL );
    }*/
	dcb.DCBlength = sizeof( DCB );
	GetCommState( m_hIDComDev, &dcb );
	dcb.BaudRate = nBaud;
	dcb.ByteSize = bitsData;    // n�mero de bits por byte, 4-8
    dcb.Parity = bitsParitat;   // 0-4=no, impar, par, marca, espacio
    dcb.StopBits = bitsStop;    // 0,1,2 = 1, 1.5, 2

    //Li diem que no volem cap tipus de control de fluxe pero ens agrada tenir
    //el DTR ences aix� l'usuari veu el led enc�s
      dcb.fOutxCtsFlow = FALSE;
      dcb.fOutxDsrFlow = FALSE;
      dcb.fOutX = FALSE;
      dcb.fInX = FALSE;

    dcb.fDtrControl = DTR_CONTROL_ENABLE;
    if (RTSCtrl)
        dcb.fRtsControl = RTS_CONTROL_TOGGLE;//RTS_CONTROL_HANDSHAKE;
    else
        dcb.fRtsControl = RTS_CONTROL_ENABLE;

    bool setcom=SetCommState( m_hIDComDev, &dcb );
    bool setupcom=SetupComm( m_hIDComDev, 10000, 10000 );

    GetCommState(m_hIDComDev,&dcb);

/*	if( !SetCommState( m_hIDComDev, &dcb )
		|| !SetupComm( m_hIDComDev, 10000, 10000 )
        || (m_OverlappedRead.hEvent == NULL && VCP)
		|| (m_OverlappedWrite.hEvent == NULL && VCP)
    ){
		DWORD dwError = GetLastError();
		if( m_OverlappedRead.hEvent != NULL ) CloseHandle( m_OverlappedRead.hEvent );
		if( m_OverlappedWrite.hEvent != NULL ) CloseHandle( m_OverlappedWrite.hEvent );
		CloseHandle( m_hIDComDev );
		return( FALSE );
    }*/

	m_bOpened = TRUE;

	return( m_bOpened );

}


//Added
BOOL CSerial::SetRTS( bool value , int TCon)
{
    if( !m_bOpened || m_hIDComDev == NULL ) return( FALSE );

    DCB dcb;

	dcb.DCBlength = sizeof( DCB );
	GetCommState( m_hIDComDev, &dcb );

    //L'activacio del RTS va al reves
	if (value==true){
        if (TCon==0xFE){
            dcb.fRtsControl = RTS_CONTROL_ENABLE;
        }else{
            dcb.fRtsControl = RTS_CONTROL_TOGGLE;
        }
    }else{
        if (TCon==0xFF){
            dcb.fRtsControl = RTS_CONTROL_DISABLE;
        }else{
            dcb.fRtsControl = RTS_CONTROL_HANDSHAKE;        // D'aquesta manera funciona el CSD-COM232
        }
    }
    return SetCommState( m_hIDComDev, &dcb );
}

BOOL CSerial::SetRTSToggle()
{
    if( !m_bOpened || m_hIDComDev == NULL ) return( FALSE );

    DCB dcb;

	dcb.DCBlength = sizeof( DCB );
	GetCommState( m_hIDComDev, &dcb );

    dcb.fRtsControl = RTS_CONTROL_TOGGLE;

    return SetCommState( m_hIDComDev, &dcb );
}

//Added by Joan Vidal
BOOL CSerial::SetDTR( bool value )
{
    if( !m_bOpened || m_hIDComDev == NULL ) return( FALSE );

    DCB dcb;

	dcb.DCBlength = sizeof( DCB );
	GetCommState( m_hIDComDev, &dcb );

    //L'activacio del RTS va al reves
	if (value==true)
        dcb.fDtrControl = DTR_CONTROL_ENABLE;
    else
        dcb.fDtrControl = RTS_CONTROL_DISABLE;

    return SetCommState( m_hIDComDev, &dcb );
}


BOOL CSerial::Close( void )
{

	if( !m_bOpened || m_hIDComDev == NULL ) return( TRUE );

	if( m_OverlappedRead.hEvent != NULL ) CloseHandle( m_OverlappedRead.hEvent );
	if( m_OverlappedWrite.hEvent != NULL ) CloseHandle( m_OverlappedWrite.hEvent );
	CloseHandle( m_hIDComDev );
	m_bOpened = FALSE;
	m_hIDComDev = NULL;

	return( TRUE );

}

BOOL CSerial::WriteCommByte( unsigned char ucByte )
{
	BOOL bWriteStat;
	DWORD dwBytesWritten;

	bWriteStat = WriteFile( m_hIDComDev, (LPSTR) &ucByte, 1, &dwBytesWritten, &m_OverlappedWrite );
	if( !bWriteStat && ( GetLastError() == ERROR_IO_PENDING ) ){
		if( WaitForSingleObject( m_OverlappedWrite.hEvent, 500 ) ) dwBytesWritten = 0;
		else{
			GetOverlappedResult( m_hIDComDev, &m_OverlappedWrite, &dwBytesWritten, FALSE );
			m_OverlappedWrite.Offset += dwBytesWritten;
			}
		}

	return( TRUE );

}

BOOL CSerial::WriteCommBlock( unsigned char* ucBlock, int lon, bool wait )
{
	BOOL bWriteStat;
	DWORD dwBytesWritten=0;
	COMSTAT ComStat;
	DWORD dwErrorFlags=0;

	bWriteStat = WriteFile( m_hIDComDev, (LPSTR) ucBlock, lon, &dwBytesWritten, &m_OverlappedWrite );

	if ((!bWriteStat) && (GetLastError() == ERROR_IO_PENDING)){

		if (WaitForSingleObject( m_OverlappedWrite.hEvent, 500 ) ) dwBytesWritten = 0;

		else{
			GetOverlappedResult( m_hIDComDev, &m_OverlappedWrite, &dwBytesWritten, FALSE );
			m_OverlappedWrite.Offset += dwBytesWritten;
		}
    }

	return( TRUE );

}

int CSerial::SendData( const char *buffer, int size,bool wait=false )
{

	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );

	DWORD dwBytesWritten = 0;

    WriteCommBlock((unsigned char *)buffer, size, wait);
    dwBytesWritten += size;

	return( (int) dwBytesWritten );

}

int CSerial::ReadDataWaiting( void )
{

	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );

	DWORD dwErrorFlags;
	COMSTAT ComStat;

	ClearCommError( m_hIDComDev, &dwErrorFlags, &ComStat );

	return( (int) ComStat.cbInQue );

}

int CSerial::ReadData( void *buffer, int limit )
{

	if( !m_bOpened || m_hIDComDev == NULL ) return( 0 );

	BOOL bReadStatus;
	DWORD dwBytesRead, dwErrorFlags;
	COMSTAT ComStat;


	ClearCommError( m_hIDComDev, &dwErrorFlags, &ComStat );
	if (!ComStat.cbInQue ) return( 0 );

	dwBytesRead = (DWORD) ComStat.cbInQue;
	if( limit < (int) dwBytesRead ) dwBytesRead = (DWORD) limit;

	bReadStatus = ReadFile( m_hIDComDev, buffer, dwBytesRead, &dwBytesRead, &m_OverlappedRead );
	if( !bReadStatus ){
		if( GetLastError() == ERROR_IO_PENDING ){
			WaitForSingleObject( m_OverlappedRead.hEvent, 500 );
			return( (int) dwBytesRead );
			}
		return( 0 );
		}

	return( (int) dwBytesRead );

}

