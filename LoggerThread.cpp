#include "LoggerThread.h"
#include <wx/datetime.h>
#include "privlevel.h"

CLoggerThread::CLoggerThread(wxTextCtrl* txt,unsigned char* fr,int l,int priv)
{
    text=txt;
    trama=fr;
    len=l;
    privlevel=priv;
}

void *CLoggerThread::Entry()
{
    bool ascii=false;
    PosaHora();
    if (trama[0]==SOH){
        CNIOT niot;
        text->SetDefaultStyle(wxTextAttr(*wxBLUE, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (niot.GetTNIOTFr(trama)==T_NIOT_PKT_ACK)
            text->SetDefaultStyle(wxTextAttr(*wxGREEN, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));

        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<SOH>");
        else
            text->AppendText("niotFr");
    }else if (trama[0]==0x16){
        text->SetDefaultStyle(wxTextAttr(*wxBLUE, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<SYN>");
        else
            text->AppendText("Info fr");
    }else if (trama[0]==0x02){
        text->SetDefaultStyle(wxTextAttr(*wxCYAN, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<STX>");
        else
            text->AppendText("Info fr");

    }else if (trama[0]==0x06){
        text->SetDefaultStyle(wxTextAttr(*wxGREEN, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<ACK>");
        else
            text->AppendText("ACK fr");

    }else if (trama[0]==0x15){
        text->SetDefaultStyle(wxTextAttr(*wxRED, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<NAK>");
        else
            text->AppendText("NAK fr");

    }else if (trama[0]==0x14){
        text->SetDefaultStyle(wxTextAttr(*wxBLUE, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<DC4>");
        else
            text->AppendText("Video fr");

    }else if (trama[0]==0x07){
        text->SetDefaultStyle(wxTextAttr(*wxGREEN, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<ACKW>");
        else
            text->AppendText("ACKW fr");

    }else if (trama[0]==0x13){
        text->SetDefaultStyle(wxTextAttr(*wxRED, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<NAKW>");
        else
            text->AppendText("NAKW fr");

    }else if (trama[0]==0x12){
        text->SetDefaultStyle(wxTextAttr(*wxBLUE, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        if (privlevel>=ENG_PRIVLEVEL)
            text->AppendText("<DC2>");
        else
            text->AppendText("DC2 fr");

    }else if ((trama[0]>=0x20) && (trama[0]<0x81)){
        text->SetDefaultStyle(wxTextAttr(wxColor(0xcc,0xcc,0xff), *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        *text<<"ascii: ";
        *text<<(char)trama[0];
        ascii=true;
    }else{
        *text << (char)nibble2ascii((trama[0]&0xF0)>>4);
        *text << (char)nibble2ascii((trama[0]&0x0F));
        *text << ", ";
    }
    if (!ascii)
        text->SetDefaultStyle(wxTextAttr(*wxBLACK, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL)));

    wxString datafr="";

    // #WARNING: writting a 266 byte char by char frame into log takes up to 150ms!!!

    for (int i=1; i<len; i++) {
        if (ascii){
            datafr.Append((wxChar)trama[i]);
            //*text<< (char)trama[i];
        }else if (privlevel>=ENG_PRIVLEVEL){
            datafr.Append((wxChar)nibble2ascii((trama[i]&0xF0)>>4));
            datafr.Append((wxChar)nibble2ascii((trama[i]&0x0F)));
            if (i<len-1){ //*text << ", ";
                datafr.Append(',');
                datafr.Append(' ');
            }
        }
    }
    datafr+="\n";
    text->AppendText(datafr);
    LimitaLog();
}

void CLoggerThread::OnExit()
{

}


CLoggerThread::~CLoggerThread()
{
    //res
}

//Vigilem que la mida del log no augmenti massa
void CLoggerThread::LimitaLog()
{
    if (text->GetLastPosition() > MAX_LOG_CARACTERS) {
        text->Remove(0, LOG_ESBORRAR);
    }
}

void CLoggerThread::PosaHora()
{
    wxDateTime now=wxDateTime::UNow();
    wxString timewxs=now.FormatTime()+","+wxString::Format("%03d",now.GetMillisecond())+"ms";
    text->SetDefaultStyle(wxTextAttr(*wxBLACK, wxColour(214,218,234), wxFont(8,wxFONTFAMILY_TELETYPE,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL)));

    while (timewxs.Len()<14)
        timewxs.Append(" ");

    timewxs.Append(" |");
    *text << timewxs;
    text->SetDefaultStyle(wxTextAttr(*wxBLACK, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL)));

}
