#ifndef LOGGERTHREAD_H
#define LOGGERTHREAD_H

#include <iostream>
#include <string>
using namespace std;

#include <wx/wx.h>
#include <wx/wxprec.h>
#include <wx/string.h>
#include <wx/textctrl.h>

#include "util.h"
#include "NIOT.h"

#define _wx(x) (wxString)((string)x).c_str()
#define MAX_LOG_CARACTERS   10000000
#define LOG_ESBORRAR        10000

class CLoggerThread : public wxThread
{
    wxTextCtrl* text;
    unsigned char* trama;
    int len;
    int privlevel;

    public:

        bool    refresh;

        virtual void *Entry();

        virtual void OnExit();

        CLoggerThread(wxTextCtrl* txt, unsigned char* fr, int l,int priv);

        ~CLoggerThread();
        void LimitaLog();
        void PosaHora();

};

#endif
