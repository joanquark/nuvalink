//---------------------------------------------------------------------------
//
// Name:        eLigthApp.cpp
// Author:      Emili
// Created:     12/1/2009 9:41:51
// Description: 
//
//---------------------------------------------------------------------------

#include "eLigthApp.h"
#include "eLigthFrm.h"

IMPLEMENT_APP(eLigthFrmApp)

bool eLigthFrmApp::OnInit()
{
    eLigthFrm* frame = new eLigthFrm(NULL);
    SetTopWindow(frame);
    frame->Show();
    return true;
}
 
int eLigthFrmApp::OnExit()
{
	return 0;
}
