#ifndef AGROCHANNELS_H_INCLUDED
#define AGROCHANNELS_H_INCLUDED

#include <iostream>
#include <string>
using namespace std;
#include "msgs.h"

class CAgroChannels
{   // certified rule(s)

    string InA1;
    string InA2;
    string InA3;
    string InD1;
    string InD2;
    string InCC;
    string I2C;

    public:
        CAgroChannels();
        void Clear();
        ~CAgroChannels();

        bool setInA1(string in);
        bool setInA2(string in);
        bool setInA3(string in);
        bool setInD1(string in);
        bool setInD2(string in);
        bool setInCC(string in);
        bool setI2C(string in);

        string& getInA1();
        string& getInA2();
        string& getInA3();
        string& getInD1();
        string& getInD2();
        string& getInCC();
        string& getI2C();


        CAgroChannels operator= (CAgroChannels param);
};


#endif // AGROCHANNELS_H_INCLUDED
