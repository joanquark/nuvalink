#ifndef NIOT_H
#define NIOT_H

#include <string>
#include <vector>
#include "util.h"
#include "crypto/AES_iv.h"

using namespace std;

#define     SOH     0x01
#define     ACK     0x06
#define     NAK     0x15

// ---------------------------------------------------------- JR IID  protocol -----
// --------------------- packet header flags ---------------------------------------

enum{
	// ------------------- TF field ---------------------------------------------------------------------------
	M_NIOT_TPKT				=	0xF0,
	T_NIOT_PKT_COMPOSITE	=	0x00,		// packet with internal ID-LEN-DATA blocks->Composite Packets
	T_NIOT_PKT_FLASH 		=	0x10,		// Flash transfer packet.
	T_NIOT_PKT_RAM			=	0x20,		// Ram transfer packet. ( only offset from accesible ram )
	T_NIOT_PKT_REQFLASH 	= 	0x30,		// Flash Request packet.
	T_NIOT_PKT_REQRAM		=	0x40,		// Ram Request packet.
	T_NIOT_PKT_5			=	0x50,
	T_NIOT_PKT_RPC			=	0x60,		// Remote procedure call
	T_NIOT_PKT_BUSPROTO		=	0x70,
	T_NIOT_PKT_DEVST		=	0x80,		// +K23+ DeviceStatus packet.
	T_NIOT_PKT_CRYPTO		=	0x90,
	T_NIOT_PKT_STREAM		=	0xA0,		// stream packet, payload contains need information, high level knows about.
	T_NIOT_PKT_BUSIO		=	0xB0,		// BusIO packets.
	T_NIOT_PKT_ENCAPSULATED	=	0xC0,		// enCapsulated mode, this packet has another internal NIOT encapsulated packet .
	T_NIOT_PKT_TXT			=	0xD0,		// +K05+ Txt data such JSON or XML.
	T_NIOT_PKT_EVENTREPORT	=	0xE0,		// Event/Reports NIOT packets.
	T_NIOT_PKT_ACK			=	0xF0,		// default control packet.

	M_NIOT_CIPHER			=	0x0C,
		M_NIOT_AESCBC_ENTRY		= 	0x04,	// Using Entry key.
		M_NIOT_AESCBC_DYN		= 	0x08,	// Using Dynamic server generated key (Diffie Hellman mode )
		M_NIOT_AESCBC_RES		= 	0x0C,	// Reserved mode.
	M_NIOT_CRC_ADD			=	0x02,
	M_NIOT_NEED_ACK			=	0x01,		// For all packets except ACK indicates need to receive ACK for this packet.
	M_NIOT_TRUEACK			= 	0x01,		// this field in an ACK packet indicates is is positive ACK,otherwise it is negative.

	// ---------------- Flg Field ------------------------------------------------------
	M_NIOT_FLG_BROADCAST	=   0x01,		// Broadcast messages go through all masters and hoppers.
	M_NIOT_FLG_DOWNLINK		=	0x02,		// Downlink messages need to be indentified at Hopping nodes.
	M_NIOT_FLG_ACKLASTPKT	=	0x04,		// Acknowledges last packet.
	M_NIOT_FLG_CLOUDPKT		= 	0x08,		// Marks the packet as coming from cloud ( different than NuvaLink using cloud connection )
	M_NIOT_FLG_POLLING		= 	0x10,		// indicates the report is a polling!
	M_NIOT_FLG_R2			= 	0x20,
	M_NIOT_FLG_EMERGENCY	= 	0x40,		// EMERGENCY PACKET using NARROWBAND channel, it is limited to 6 byte data ( 1 block AES only )-> 24 byte packet.
	M_NIOT_FLG_LOCALACK		=	0x80,		// Does not need confirmation from Cloud (polling signal) default Master can ACK locally itself (once acquired)
};

enum{
	// ---------------------- PANEL RPC ---------------------
	T_NIOT_RPC_ORDER        		=	0,
	T_NIOT_RPC_EVENT        		=   1,
	T_NIOT_RPC_ONUSER       		=   2,
	T_NIOT_RPC_BYPASS       		=   3,
	T_NIOT_RPC_ARM          		=   4,
	T_NIOT_RPC_DISARM       		=   5,
	T_NIOT_RPC_SETTRT				=   6,
	T_NIOT_RPC_CLRTRT				=   7,
	T_NIOT_RPC_STABLISH				=	8,
	T_NIOT_RPC_ACDC					=	9,
	T_NIOT_RPC_FIRMWARE				=	10,
	T_NIOT_RPC_GETFLASHSIGNATURE	= 	11,
	T_NIOT_RPC_ZVID         		=   12,
	T_NIOT_RPC_BEACON_SOLVED		=	13,
	T_NIOT_RPC_BEACON_STATUS		=	14,
	T_NIOT_RPC_INPUT_DETECT			= 	15,		// +K25+ to send VideoAnalisys detections back to device
    T_NIOT_RPC_RECORDANALOG			=	16,		// records an amount of input signal samples on a channel as ADPCM signal on flash.

	T_NIOT_RPC_MODBUS_REQUEST		= 	20,		// +C1A+
	T_NIOT_RPC_MODBUSTCP_REQUEST	= 	21		// +C1A+

};



#define  LENNIOTPKT(inf)		(inf+4+16+2)
#define  LENNIOTINF(mtu)		(mtu-4-16-16-2)		// Two times 16, to provide ROOM for padding ( max=15)!
#define  LENNIOTREP(mtu)		(mtu-16-16-2)		// Two times 16, to provide ROOM for padding!

#define  SIZE_UNCIPHERED_NIOTHEADER		8
#define  SIZE_CIPHERED_NIOTHEADER		8
#define  LEN_RD_DATA_NIOT				512
#define  LEN_DATA_NIOT					LEN_RD_DATA_NIOT+4		// +K02+ (256 again, 512 caused error after NFC write )

// ------------------------------ Type enums --------------------------------
enum{
	// ---------------------- TXT Subtypes ---------------------
	T_NIOT_TXT_ADDDATA				= 	0x00,
	// -- SIA event subtypes --
	T_NIOT_TXT_SIADC03        		=	0x03,
	T_NIOT_TXT_SIADC04        		=	0x04,
	T_NIOT_TXT_SIADC05        		=	0x05,
	T_NIOT_TXT_SIADC09        		=	0x09,
	// -- json subtypes --
	T_NIOT_TXT_JSON	        		=	0x20,
	// -- xml subtypes --
	T_NIOT_TXT_XML	        		=	0x40,
};


typedef struct{
	BYTE 		Header;			// 0
	BYTE		TF;				// 1
	BYTE 		Flg;			// 2
	BYTE		Try;			// 3
	WORD_VAL	Ctrl;			// 4
	WORD_VAL	AppId;			// 6 AppID number, depending on packet type it can be frame data.
	DWORD_VAL  	SN;				// 8 usually organizes as 1BYTE type + 3 byte serial up to 16 million devices.
	BYTE 		Hops;			// 12 Hops request(H):Hops counter(L)
	BYTE		subType;		// 13 it is subtype packet
	WORD_VAL	SeqNum;			// 14
}TNIOTHeader;

typedef struct{
	BYTE 		Header;				// 0
	BYTE		TF;					// 1
	BYTE 		Flg;				// 2
	BYTE		Try;				// 3
	WORD_VAL	Ctrl;				// 4
	WORD_VAL	AppId;				// 6 App ID number, depending on packet type it can be frame data.
	DWORD_VAL  	SN;					// 8 usually organizes as 1BYTE type + 3 byte serial up to 16 million devices.
	BYTE 		Hops;				// 12 Hops request(H):Hops counter(L)
	BYTE		subType;			// 13 it is subtype packet
	WORD_VAL	SeqNum;				// 14
	BYTE  		Buff[LEN_DATA_NIOT];// 16
	BYTE		Crc[2];		    	// necessary to establish correct Maximum NIOTFr buffer.
}TNIOTFr;

const DWORD NIOT_ALL_SUBSCRIBERS[]={0xFFFFFFFF};


enum{

	ORDER_DUMMY              	=	0x00,
	ORDER_VMODEM				=   0x01,
	ORDER_TEST                 	=	0x02,
	ORDER_RTCLOUD_UPDATE  		=	0x03,
	ORDER_REQUEST_ALIAS         =	0x04,
	ORDER_FACTORY_PROGRAMMING  	=	0x05,
	ORDER_RUTA_ANDADO          	=	0x07,
	ORDER_FINISH_COMM          	=	0x08,
	ORDER_REFRESH_PROG         	=	0x09,
	ORDER_CONNECT		     	=	0x0A,
	ORDER_DISCONNECT	     	=	0x0B,
	ORDER_DISL3ACCESS			=	0x0C,
	ORDER_DISL4ACCESS			=	0x0D,
	ORDER_DO_CALLBACK          	=	0x0F,
	ORDER_EN_BIDIUSER          	=	0x10,
	ORDER_CLR_MEM_ALARM        	=	0x11,
	ORDER_CLR_ERRORTIME        	=	0x12,
	ORDER_SET_VERIFY_MODE      	=	0x15,
	ORDER_GET_WCRYPT_CODE      	=	0x16,
	ORDER_SOFT_RING_RX         	=	0x17,
	ORDER_SOFT_ONOFFHOOK_CHANGE =	0x18,
	ORDER_HEAVY_SATRF           =  	0x19,
	ORDER_CS_SUCCESS          	=	0x1A,
    ORDER_SFLASH_SAVEFACTORYCFG	=	0x1B,
	ORDER_SFLASH_FACTORY       	=	0x1C,
	ORDER_READ_SFLASH_FACTORY  	=	0x1D,
    ORDER_RESET_ANCNT			= 	0x1E,		// +M1D+
	ORDER_MEASURE_AN			=	0x1F,
	ORDER_GET_SIGNPOST			=	0x20,
	ORDER_REFRESH_SIGNPOST		=	0x21,
	ORDER_GEOLOCATE				=	0x22,
	ORDER_SET_LONGPBUS			=	0x23,
	ORDER_CANCEL_TRANSFER		=	0x24,
	ORDER_RESET_GPS				=	0x25,
	ORDER_ENABLE_DEBUG			=	0x26,
	ORDER_POWEROFF_GPS          =   0x27,
	ORDER_CELLLOCATE			=	0x28,
    ORDER_RESETBATTERY_mACnt	=	0x29,
    ORDER_SDI12_CHECK           =   0x2A
};


		// --------------- connection queries ---------------
#define             DIRECT_COMM             	    0
#define     		CALLBACK_COMM              		1
#define     		M_LEVEL4_ACCESS					0x80
#define     		M_ACCEPT_EXT_ANSWER         	0x40
		// --------------- connection answers ---------------
#define     		TRUE_ANSWER                		1
#define     		FALSE_ANSWER               		0
#define     		FALSE_ANSWER_L2					2			// Level 3 not enabled from Level2.
#define     		FALSE_ANSWER_L3					3			// Level 4 not enabled from Level3.
#define     		M_INSTAL_LEVEL2            		0x80
#define     		M_FLASH_LEVEL3					0x40


// ------------------------------ Defines for the Boot actions the Nuvalink can ----
#define FW_ACTION_NOP1          	0x00
#define FW_ACTION_NOP2          	0xFF
   // no action
#define FW_ACTION_UPDATE        	0x01
   // nuvalink has send update firmware and it must be flashed.
#define FW_ACTION_UPDATEBLE        	0x02
   // nuvalink has send BLE firmware to flash.


class CNIOT
{
    TNIOTFr NIOTFrTx;
    int frtxlen;
    TNIOTFr NIOTFrRx;
    TNIOTFr NIOTFrAck;
    int NIOTinflen;
    int ppush;
    int ppop;
    int datalen;
    int NIOTptr;
    int NIOTPending;
    string aeskey;
    unsigned char padbyte;
    unsigned int ns;
    string SN;

    public:

        CNIOT();
        ~CNIOT();
        bool SetAesKey(string key);
        bool SetSN(string sn);
        string GetSN();
        bool FormatHeader(unsigned char flags,unsigned char hops=0,unsigned char subtype=0,string code="");
        bool AddBlock(unsigned char TID,unsigned char* buff,int len);
        int CloseFr(unsigned char* raw=NULL);
        int UpdateTry(int tries,int len,unsigned char* raw);
        unsigned char* GetFrameBuff();
        unsigned char* GetAckBuff();
        void Reset();
        unsigned char GetTNIOTFr(unsigned char* buff);
        unsigned char GetTNIOTAck(unsigned char* fr);
        WORD GetLenNIOTFr(unsigned char* buff);
        bool IsBroadCast(unsigned char* buff);
        int ProcessNIOTRx(unsigned char* buff,BYTE* subscriber,int cnt);
        int  PopBlock(unsigned char wantedID,unsigned char* popbuff);
        int NIOTAck(BYTE CHACK);
        int GetFlags();
        int NeedAck();
        int GetHops();
};

#endif
