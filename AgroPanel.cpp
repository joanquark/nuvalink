#include "wx_pch.h"
#include "AgroPanel.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(AgroPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(AgroPanel)
//*)

//(*IdInit(AgroPanel)
const long AgroPanel::ID_STATICTEXT1 = wxNewId();
const long AgroPanel::ID_CHOICE1 = wxNewId();
const long AgroPanel::ID_STATICTEXT2 = wxNewId();
const long AgroPanel::ID_TEXTCTRL1 = wxNewId();
const long AgroPanel::ID_STATICTEXT3 = wxNewId();
const long AgroPanel::ID_TEXTCTRL2 = wxNewId();
const long AgroPanel::ID_BUTTON1 = wxNewId();
const long AgroPanel::ID_STATICTEXT4 = wxNewId();
const long AgroPanel::ID_CHOICE2 = wxNewId();
const long AgroPanel::ID_STATICTEXT5 = wxNewId();
const long AgroPanel::ID_TEXTCTRL3 = wxNewId();
const long AgroPanel::ID_STATICTEXT6 = wxNewId();
const long AgroPanel::ID_TEXTCTRL4 = wxNewId();
const long AgroPanel::ID_BUTTON2 = wxNewId();
const long AgroPanel::ID_STATICTEXT7 = wxNewId();
const long AgroPanel::ID_CHOICE3 = wxNewId();
const long AgroPanel::ID_STATICTEXT8 = wxNewId();
const long AgroPanel::ID_TEXTCTRL5 = wxNewId();
const long AgroPanel::ID_STATICTEXT9 = wxNewId();
const long AgroPanel::ID_TEXTCTRL6 = wxNewId();
const long AgroPanel::ID_BUTTON3 = wxNewId();
const long AgroPanel::ID_STATICTEXT10 = wxNewId();
const long AgroPanel::ID_CHOICE4 = wxNewId();
const long AgroPanel::ID_STATICTEXT11 = wxNewId();
const long AgroPanel::ID_TEXTCTRL7 = wxNewId();
const long AgroPanel::ID_STATICTEXT12 = wxNewId();
const long AgroPanel::ID_TEXTCTRL8 = wxNewId();
const long AgroPanel::ID_BUTTON4 = wxNewId();
const long AgroPanel::ID_STATICTEXT13 = wxNewId();
const long AgroPanel::ID_CHOICE5 = wxNewId();
const long AgroPanel::ID_STATICTEXT14 = wxNewId();
const long AgroPanel::ID_TEXTCTRL9 = wxNewId();
const long AgroPanel::ID_STATICTEXT15 = wxNewId();
const long AgroPanel::ID_TEXTCTRL10 = wxNewId();
const long AgroPanel::ID_BUTTON5 = wxNewId();
const long AgroPanel::ID_STATICTEXT16 = wxNewId();
const long AgroPanel::ID_CHOICE6 = wxNewId();
const long AgroPanel::ID_STATICTEXT17 = wxNewId();
const long AgroPanel::ID_TEXTCTRL11 = wxNewId();
const long AgroPanel::ID_STATICTEXT18 = wxNewId();
const long AgroPanel::ID_TEXTCTRL12 = wxNewId();
const long AgroPanel::ID_BUTTON6 = wxNewId();
const long AgroPanel::ID_STATICTEXT19 = wxNewId();
const long AgroPanel::ID_CHOICE7 = wxNewId();
const long AgroPanel::ID_STATICTEXT20 = wxNewId();
const long AgroPanel::ID_TEXTCTRL13 = wxNewId();
const long AgroPanel::ID_STATICTEXT21 = wxNewId();
const long AgroPanel::ID_TEXTCTRL14 = wxNewId();
const long AgroPanel::ID_BUTTON7 = wxNewId();
const long AgroPanel::ID_SPEEDBUTTON1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(AgroPanel,wxPanel)
	//(*EventTable(AgroPanel)
	//*)
END_EVENT_TABLE()

AgroPanel::AgroPanel(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(AgroPanel)
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, wxID_ANY, wxDefaultPosition, wxSize(751,428), wxDOUBLE_BORDER|wxRAISED_BORDER|wxTAB_TRAVERSAL, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(6, 7, 0, 0);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("An1"), wxDefaultPosition, wxSize(20,13), 0, _T("ID_STATICTEXT1"));
	FlexGridSizer1->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceAn1 = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxSize(129,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	ChoiceAn1->Append(_("-NONE-"));
	ChoiceAn1->Append(_("TEROS10"));
	ChoiceAn1->Append(_("TERRA31"));
	ChoiceAn1->Append(_("10HS"));
	ChoiceAn1->Append(_("PULSE"));
	ChoiceAn1->Append(_("4..20"));
	ChoiceAn1->Append(_("0_3"));
	FlexGridSizer1->Add(ChoiceAn1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Alias"), wxDefaultPosition, wxSize(20,13), 0, _T("ID_STATICTEXT2"));
	FlexGridSizer1->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	AliasAn1 = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	FlexGridSizer1->Add(AliasAn1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Raw"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	FlexGridSizer1->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	RawAn1 = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxSize(200,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	FlexGridSizer1->Add(RawAn1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ButtAn1 = new wxButton(this, ID_BUTTON1, _("Read"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON1"));
	FlexGridSizer1->Add(ButtAn1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText4 = new wxStaticText(this, ID_STATICTEXT4, _("An2"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
	FlexGridSizer1->Add(StaticText4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceAn2 = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxSize(131,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
	ChoiceAn2->Append(_("-NONE-"));
	ChoiceAn2->Append(_("TEROS10"));
	ChoiceAn2->Append(_("TERRA31"));
	ChoiceAn2->Append(_("10HS"));
	ChoiceAn2->Append(_("PULSE"));
	ChoiceAn2->Append(_("4..20"));
	ChoiceAn2->Append(_("0_3"));
	FlexGridSizer1->Add(ChoiceAn2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText5 = new wxStaticText(this, ID_STATICTEXT5, _("Alias"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT5"));
	FlexGridSizer1->Add(StaticText5, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	AliasAn2 = new wxTextCtrl(this, ID_TEXTCTRL3, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
	FlexGridSizer1->Add(AliasAn2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText6 = new wxStaticText(this, ID_STATICTEXT6, _("Raw"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT6"));
	FlexGridSizer1->Add(StaticText6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	RawAn2 = new wxTextCtrl(this, ID_TEXTCTRL4, wxEmptyString, wxDefaultPosition, wxSize(202,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL4"));
	FlexGridSizer1->Add(RawAn2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ButtAn2 = new wxButton(this, ID_BUTTON2, _("Read"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON2"));
	FlexGridSizer1->Add(ButtAn2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText7 = new wxStaticText(this, ID_STATICTEXT7, _("An3"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT7"));
	FlexGridSizer1->Add(StaticText7, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceAn3 = new wxChoice(this, ID_CHOICE3, wxDefaultPosition, wxSize(129,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE3"));
	ChoiceAn3->Append(_("-NONE-"));
	ChoiceAn3->Append(_("TEROS10"));
	ChoiceAn3->Append(_("TERRA31"));
	ChoiceAn3->Append(_("10HS"));
	ChoiceAn3->Append(_("PULSE"));
	ChoiceAn3->Append(_("4..20"));
	ChoiceAn3->Append(_("0_3"));
	FlexGridSizer1->Add(ChoiceAn3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText8 = new wxStaticText(this, ID_STATICTEXT8, _("Alias"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT8"));
	FlexGridSizer1->Add(StaticText8, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	AliasAn3 = new wxTextCtrl(this, ID_TEXTCTRL5, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL5"));
	FlexGridSizer1->Add(AliasAn3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText9 = new wxStaticText(this, ID_STATICTEXT9, _("Raw"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT9"));
	FlexGridSizer1->Add(StaticText9, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	RawAn3 = new wxTextCtrl(this, ID_TEXTCTRL6, wxEmptyString, wxDefaultPosition, wxSize(202,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL6"));
	FlexGridSizer1->Add(RawAn3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ButtAn3 = new wxButton(this, ID_BUTTON3, _("Read"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON3"));
	FlexGridSizer1->Add(ButtAn3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText10 = new wxStaticText(this, ID_STATICTEXT10, _("Ind1"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT10"));
	FlexGridSizer1->Add(StaticText10, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceD1 = new wxChoice(this, ID_CHOICE4, wxDefaultPosition, wxSize(130,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE4"));
	ChoiceD1->Append(_("-NONE-"));
	ChoiceD1->Append(_("TEROS12"));
	ChoiceD1->Append(_("5TE"));
	ChoiceD1->Append(_("ES2"));
	ChoiceD1->Append(_("TERRA33"));
	ChoiceD1->Append(_("TERRA34"));
	ChoiceD1->Append(_("ENVIROPRO40"));
	ChoiceD1->Append(_("SENTEK60"));
	ChoiceD1->Append(_("SENTEK90"));
	ChoiceD1->Append(_("ATMOS14"));
	ChoiceD1->Append(_("ATMOS41"));
	FlexGridSizer1->Add(ChoiceD1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText11 = new wxStaticText(this, ID_STATICTEXT11, _("Alias"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT11"));
	FlexGridSizer1->Add(StaticText11, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	AliasD1 = new wxTextCtrl(this, ID_TEXTCTRL7, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL7"));
	FlexGridSizer1->Add(AliasD1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText12 = new wxStaticText(this, ID_STATICTEXT12, _("Raw"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT12"));
	FlexGridSizer1->Add(StaticText12, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	RawD1 = new wxTextCtrl(this, ID_TEXTCTRL8, wxEmptyString, wxDefaultPosition, wxSize(200,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL8"));
	FlexGridSizer1->Add(RawD1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ButtD1 = new wxButton(this, ID_BUTTON4, _("Read"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON4"));
	FlexGridSizer1->Add(ButtD1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText13 = new wxStaticText(this, ID_STATICTEXT13, _("Ind2"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT13"));
	FlexGridSizer1->Add(StaticText13, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceD2 = new wxChoice(this, ID_CHOICE5, wxDefaultPosition, wxSize(131,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE5"));
	ChoiceD2->Append(_("-NONE-"));
	ChoiceD2->Append(_("TEROS12"));
	ChoiceD2->Append(_("5TE"));
	ChoiceD2->Append(_("ES2"));
	ChoiceD2->Append(_("TERRA33"));
	ChoiceD2->Append(_("TERRA34"));
	ChoiceD2->Append(_("ENVIROPRO40"));
	ChoiceD2->Append(_("SENTEK60"));
	ChoiceD2->Append(_("SENTEK90"));
	ChoiceD2->Append(_("ATMOS14"));
	ChoiceD2->Append(_("ATMOS41"));
	FlexGridSizer1->Add(ChoiceD2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText14 = new wxStaticText(this, ID_STATICTEXT14, _("Alias"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT14"));
	FlexGridSizer1->Add(StaticText14, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	AliasD2 = new wxTextCtrl(this, ID_TEXTCTRL9, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL9"));
	FlexGridSizer1->Add(AliasD2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText15 = new wxStaticText(this, ID_STATICTEXT15, _("Raw"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT15"));
	FlexGridSizer1->Add(StaticText15, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	RawD2 = new wxTextCtrl(this, ID_TEXTCTRL10, wxEmptyString, wxDefaultPosition, wxSize(200,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL10"));
	FlexGridSizer1->Add(RawD2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ButtD2 = new wxButton(this, ID_BUTTON5, _("Read"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON5"));
	FlexGridSizer1->Add(ButtD2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText16 = new wxStaticText(this, ID_STATICTEXT16, _("Pulse"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT16"));
	FlexGridSizer1->Add(StaticText16, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceC = new wxChoice(this, ID_CHOICE6, wxDefaultPosition, wxSize(131,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE6"));
	ChoiceC->Append(_("-NONE-"));
	ChoiceC->Append(_("PULSE"));
	FlexGridSizer1->Add(ChoiceC, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText17 = new wxStaticText(this, ID_STATICTEXT17, _("Alias"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT17"));
	FlexGridSizer1->Add(StaticText17, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	AliasC = new wxTextCtrl(this, ID_TEXTCTRL11, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL11"));
	FlexGridSizer1->Add(AliasC, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText18 = new wxStaticText(this, ID_STATICTEXT18, _("Raw"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT18"));
	FlexGridSizer1->Add(StaticText18, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	RawC = new wxTextCtrl(this, ID_TEXTCTRL12, wxEmptyString, wxDefaultPosition, wxSize(198,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL12"));
	FlexGridSizer1->Add(RawC, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ButtC = new wxButton(this, ID_BUTTON6, _("Read"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON6"));
	FlexGridSizer1->Add(ButtC, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText19 = new wxStaticText(this, ID_STATICTEXT19, _("I2C"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT19"));
	FlexGridSizer1->Add(StaticText19, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice1 = new wxChoice(this, ID_CHOICE7, wxDefaultPosition, wxSize(132,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE7"));
	Choice1->Append(_("NONE"));
	Choice1->Append(_("SHT21"));
	Choice1->Append(_("BME280"));
	FlexGridSizer1->Add(Choice1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText20 = new wxStaticText(this, ID_STATICTEXT20, _("Alias"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT20"));
	FlexGridSizer1->Add(StaticText20, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	AliasI2C = new wxTextCtrl(this, ID_TEXTCTRL13, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL13"));
	FlexGridSizer1->Add(AliasI2C, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText21 = new wxStaticText(this, ID_STATICTEXT21, _("Raw"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT21"));
	FlexGridSizer1->Add(StaticText21, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	RawI2C = new wxTextCtrl(this, ID_TEXTCTRL14, wxEmptyString, wxDefaultPosition, wxSize(195,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL14"));
	FlexGridSizer1->Add(RawI2C, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ButtI2C = new wxButton(this, ID_BUTTON7, _("Read"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_BUTTON7"));
	FlexGridSizer1->Add(ButtI2C, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(0,0,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(0,0,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(0,0,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	wxBitmap SpeedButton1_BMP(_("./icons/toolbar/upload.png"), wxBITMAP_TYPE_ANY);
	SpeedButton1 = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, SpeedButton1_BMP, 0, 2, -1, true, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	SpeedButton1->SetUserData(0);
	FlexGridSizer1->Add(SpeedButton1, 1, wxALL, 5);
	FlexGridSizer1->Add(0,0,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(0,0,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(0,0,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(FlexGridSizer1);
	SetSizer(FlexGridSizer1);
	Layout();

	Connect(ID_TEXTCTRL2,wxEVT_COMMAND_TEXT_UPDATED,(wxObjectEventFunction)&AgroPanel::OnReadAn1Text);
//	Connect(ID_BUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&AgroPanel::OnButtAn1Click);
	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&AgroPanel::OnSpeedButton1LeftClick);
	//*)
}

AgroPanel::~AgroPanel()
{
	//(*Destroy(AgroPanel)
	//*)
}

void AgroPanel::SetNuvaLink(Cnuvalink* nuvalink,CData* data)
{
    this->nuvalink = nuvalink;
    this->dades=data;


    return true;
}

void AgroPanel::OnReadAn1Text(wxCommandEvent& event)
{

}


void AgroPanel::OnSpeedButton1LeftClick(wxCommandEvent& event)
{
    int sensor=0;
    int zona=0;
 /*   switch (ChoiceAn1->GetStringSelection()){
        case "TEROS10":
            break;
        case "10HS":
            break;
        case "PULSE":
            break;
        case "4_20":
            break;
        case "0_3":
            break;

        case
    }*/

}


