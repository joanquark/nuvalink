#ifndef EVENT_H
#define EVENT_H

#define TIPUS_EVENT_MSK             1
//#define TIPUS_EVENT_MSK_AN          2
//#define TIPUS_EVENT_CONTACTIDEXTES  3

#define T_PSTN_AUDIO             0x00
#define T_PSTN_DATA              0x10
#define T_GSM_AUDIO              0x20
#define T_GSM_DATA               0x30
#define T_GSM_FAX                0x40
#define T_GSM_SMS                0x50

#define T_INCOMING_CALL          0x08
#define T_OUTGOING_CALL          0x00

#define T_INI_CALL               0x00
#define T_SUCCESS_CALL           0x01
#define T_BUSY_CALL              0x02
#define T_FAIL_CALL              0x03
#define T_REJECT_CALL            0x04

#define IsReportEvent(i)        (i==0xFF)
#define IsPhoneCallEvent(i)     (i==0xFE)

#include <iostream>
#include <string>
using namespace std;

#include "util.h"
#include "msgs.h"
#include "cid.h"


   #define CONST_FIELD_CID_II           0x18

// ------------------------------------------------------ MskEvent define -------

// ------------------------------------------------------ Event define -------
typedef struct{
	WORD_VAL 	CID;		//  0		- CID + flags
	WORD_VAL	ANUM;		//	2		- Area + number
	uint32_t	epoch;		//  4   	- TimeStamp.
	BYTE		Abonat[3];	//  8..A
	BYTE 		PanelSt;	//  B
	WORD_VAL	AreaSt;		//  C..D
	WORD 		res;		// 	E..F
}TEventMsk;		// 16

typedef struct{
	WORD_VAL	CID;		//  0	- CID + flags
 	WORD_VAL 	ANUM;		//	2	- Area + number
 	uint32_t  	epoch;		//  4   - TimeStamp.
 	uint32_t	Analog[2];	//	8..16	- Analog 32 bit values.
}TEventMskAn;		// 16

typedef struct{
	WORD_VAL	CID;		//  0	- CID + flags
 	WORD_VAL 	ANUM;		//	2	- Area + number
 	uint32_t  	epoch;		//  4   - TimeStamp.
	float		Lat;		// 	8
	float		Long;		//  12
}TEventMskGeo;		// 16



class CEvent
{
    bool alarma; //false=restauracio
    string sHora, sData, sDescripcio,sDescPhoneCall, sContactID, sTipus;

    float       Analog[8];
    wxString    sAnalog[8];
    int Sat,Speed;
    float Lat,Long;
    wxString sFix;
    int zona, tipus;
    int tipusEvent;
    bool CSReport;
    //PhoneCallEvent
    string numtel;
    //extes/contactID
    string sAbonat;
    int area, linia, numero;
    int binnum;
    string binfile;             // link to the binary file
    public:

        CEvent(CLanguage *lang);
        ~CEvent();
        CLanguage *Lang;
        bool SetTypeEvent(int tipus);
        bool SetReportEvent(unsigned char *data);
        bool SetPhoneCallEvent(unsigned char *data);
        bool SetHora(string& hora);
        bool SetData(string& data);
        bool SetContactID(string& contactID);
        bool SetZona(int zona);
        bool SetBinNum(int binnum);
        bool SetBinFile(string& file);

        bool SetContactIdEvent(unsigned char *data);
        bool SetAbonat(string& abonat);
        bool SetArea(int area);

        int GetTypeConEvent();
        string& GetHora();
        string& GetData();
        string& GetDescr();
        string& GetContactID();
        string& GetBusStringID(unsigned int id);
        string& GetTypeCon();
        int GetBinNum();
        string& GetBinFile();
        string& GetNumTel();

        int GetZona();
        bool IsAlarm();
        bool IsTipusAlarm();
        bool IsTipusAveria();
        bool IsTipusONOFF();
        bool IsTipusSupervisio();
        bool IsTipusTest();
        bool IsTipusOmisio();
        bool IsTipusOutput();
        bool IsTipusRele();
        bool IsTipusBin();
        bool IsTipusBus();
        bool IsTipusAnalog();
        bool IsTipusGeo();
        bool IsCSReported();

        //Extesos en contactID
        string& GetAbonat();
        int GetArea();
        int GetReceiverLine();
        int GetReceiverNumber();
        wxString GetFix();
        bool   SetFix(wxString fix);
        bool   SetFix(float Lat,float Long);

        bool    SetAnalog(wxString an,int n);
        bool    SetAnalog(float an1,float an2);
        wxString    GetAnalog(int n);

    private:
        int SearchIndx(string cID);
};

#endif
