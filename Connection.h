#ifndef CONEXIO_H
#define CONEXIO_H

/*
 * Classe Connection
 * Crea conexions, rep i envia dades a nivell baix (trames fetes). La classe
 * s'encarrega d'utilitzar el tipus de conexio establert, de manera que, per
 * la resta del programa, tot funciona igual tant si la conexio es local, com
 * per modem, com per internet.
 */

#include <iostream>
#include <string>
using namespace std;

#include <wx/progdlg.h> //Pel progress Dialog

#define TYPE_USB_CONNECTION       0
#define TYPE_RS485_CONNECTION     1
#define TYPE_TCPIP_CONNECTION     2
#define TYPE_CLOUD_CONNECTION     3

// 0-4=no, impar, par, marca, espacio
#define NO_PARITY      0
#define ODD_PARITY   1
#define EVEN_PARITY     2

#define USBD_VID     					1155
#define USBD_LANGID_STRING     			1033
#define USBD_MANUFACTURER_STRING     	"ST Microelectronics"
#define USBD_PID_FS     				22352
#define USBD_PRODUCT_STRING_FS     		"Nuvathings"
#define USBD_SERIALNUMBER_STRING_FS  	"00000000001A"
#define USBD_CONFIGURATION_STRING_FS 	"Custom HID Config"
#define USBD_INTERFACE_STRING_FS     	"Custom HID Interface"
#define USB_SIZ_BOS_DESC            	0x0C

#include "serial/hidapi.h"
#include "serial/serial.h"
#include <wx/socket.h>
#include "logger.h"
#include "config.h"
#include "msgs.h"
#include "myapp.h"

class CConnection
{
    int type;
    int baud, bitsData, bitsStop, paritat;
    int portCom;
    int cntmiss;
    CSerial serial;
    wxSocketBase   *psocket;
    wxSocketClient *socketc;
    wxSocketServer *sockets;
    wxIPV4address IPADDR;
    bool IsSocketServer;

    string host;
    int port;
    int avTimeout;

    string msgError;
    string tcpipBuf;
    hid_device *uhandle;
    bool reliefmode;
	// Enumerate and print the HID devices on the system
	struct hid_device_info *devs, *cur_dev;

    public:
        wxProgressDialog *progressDlg;
        MyApp *myApp;
        CLanguage *Lang;
        int *progress;
        CLogger *myLog;
        bool conectat;
        bool FFT;
        CConnection();
        ~CConnection();

        bool Connect();
        bool ConnectTCPIPServer(wxString IP,int port);
        bool Disconnect();
        bool SendData(unsigned char *trama, int length);
        int  ReceiveData(unsigned char *trama, int length);
        bool AvailableComPort(int port);
        bool SetBaud(int baud);
        bool SetBitsData(int bits);
        bool SetBitsStop(int bits);
        bool SetParity(int par);
        bool SetType(int tip);
        bool SetPortCom(int port);
        bool SetDestIP(const string& ip);
        bool SetIPPort(int port);

        bool IsConfigured();
        int GetMaxNIOTInfLen();
        int GetBaud();
        int GetBitsData();
        int GetBitsStop();
        int GetParity();
        int GetTypeCon();
        int GetPortCom();
        string& GetPhNum();
        int GetAnswerTimeOut();
        int GetTimeoutAck();
        int CalcAvTimeOut(int time);
        int GetAvTimeOut();
        int GetResolucioProcessRx();
        string& GetIPDesti();
        int GetIPPort();
        string& GetLastError();
        bool SetSocketTCPIP(wxSocketBase *sock);
        bool SocketLost();
        bool IsReliefMode();
        bool SetReliefMode(bool mode);
        int checkUSB();
    private:
        bool ConnectRS485(bool rts=false,bool vcp=true);
        bool ConnectUSB();

        bool ConnectTCPIP();
        bool ConnectCLOUD();
        bool DisConnectRS485();
        bool DisConnectUSB();
        bool DisConnectTCPIP();
        bool DisConnectCLOUD();
        int ReceiveDataUSB(unsigned char *trama, int len);
        int ReceiveDataLocal(unsigned char *trama, int length);
        int ReceiveDataTCPIP(unsigned char *trama, int length);
        int ReceiveDataCLOUD(unsigned char *trama, int length);
        bool SendDataUSB(unsigned char *trama, int length);
        bool SendDataRS485(unsigned char *trama, int length,bool wait);
        bool SendDataTCPIP(unsigned char *trama, int length);
        bool SendDataCLOUD(unsigned char *trama, int length);
        bool SendDataGPRS(unsigned char *trama, int length);


        bool SetRTS(bool valor, int TCon);
        bool SetRTSToggle();

        bool Progres (string msg);
        bool Progres (wxString msg);
        bool UserCancel();
};

#endif
