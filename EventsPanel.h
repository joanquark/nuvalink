#ifndef EVENTSPANEL_H
#define EVENTSPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(EventsPanel)
	#include <wx/checklst.h>
	#include <wx/listctrl.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	//*)
#endif
//(*Headers(EventsPanel)
#include <wxSpeedButton.h>
//*)

class EventsPanel: public wxPanel
{
	public:

		EventsPanel(wxWindow* parent,wxWindowID id=wxID_ANY,Cnuvalink *nuvalink=NULL,CLanguage* lang=NULL,CEventList *events=NULL,string idp="",string ver="");
		virtual ~EventsPanel();

		//(*Declarations(EventsPanel)
		wxCheckListBox* CheckListBox1;
		wxListCtrl* llista;
		wxSpeedButton* ButClear;
		wxSpeedButton* ButExport;
		//*)

        bool SetGrups(CGroup *grupZAlias,CGroup *grupAAlias, CGroup *grupUAlias, CGroup *grupOAlias);
		void ListFill();
		CEventList *GetLlista();
		bool UpdateValues();
		bool ApplyValues();
		void Clear();
		bool GeneraCSVEvents(string *csv);
        bool ExportarEvents(string& fileName);
	protected:

		//(*Identifiers(EventsPanel)
		static const long ID_LISTCTRL1;
		static const long ID_CHECKLISTBOX1;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		//*)
		static const long idBotoViewBin;

	private:

		//(*Handlers(EventsPanel)
		void OnButExportLeftClick(wxCommandEvent& event);
		void OnButClearLeftClick(wxCommandEvent& event);
		void OnCheckListBox1Toggled(wxCommandEvent& event);
		//*)
        string CZalias[64];
        string CAalias[64];
        string CUalias[64];
        string COalias[64];
        wxBitmapButton* viewButton;
        CEventList *events;
        CCodi *codi;
        Cnuvalink *nuvalink;
        CLanguage *Lang;
        wxThumbnailCtrl* thumbnail;
        string PanelVersion;
        string PanelId;
        string msgError;
        bool showAlarm, showONOFF, showAveria, showTest, showSupervision, showBypass;

	protected:

		void BuildContent(wxWindow* parent,wxWindowID id);

		DECLARE_EVENT_TABLE()
};

#endif
