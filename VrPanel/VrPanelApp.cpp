//---------------------------------------------------------------------------
//
// Name:        VrPanelApp.cpp
// Author:      Joan
// Created:     29/01/2010 10:29:50
// Description: 
//
//---------------------------------------------------------------------------

#include "VrPanelApp.h"
#include "VrPanelDlg.h"

IMPLEMENT_APP(VrPanelDlgApp)

bool VrPanelDlgApp::OnInit()
{
	VrPanelDlg* dialog = new VrPanelDlg(NULL);
	SetTopWindow(dialog);
	dialog->Show(true);		
	return true;
}
 
int VrPanelDlgApp::OnExit()
{
	return 0;
}
