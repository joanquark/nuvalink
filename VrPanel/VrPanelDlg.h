//---------------------------------------------------------------------------
//
// Name:        VrPanelDlg.h
// Author:      Joan
// Created:     29/01/2010 10:29:51
// Description: VrPanelDlg class declaration
//
//---------------------------------------------------------------------------

#ifndef __VRPANELDLG_h__
#define __VRPANELDLG_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/dialog.h>
#else
	#include <wx/wxprec.h>
#endif

//Do not add custom headers between 
//Header Include Start and Header Include End.
//wxDev-C++ designer will remove them. Add custom headers after the block.
////Header Include Start
#include <wx/gauge.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/textctrl.h>
#include <wx/stattext.h>
#include <wx/statbox.h>
#include <wx/panel.h>
////Header Include End

////Dialog Style Start
#undef VrPanelDlg_STYLE
#define VrPanelDlg_STYLE wxCAPTION | wxSYSTEM_MENU | wxDIALOG_NO_PARENT | wxMINIMIZE_BOX | wxCLOSE_BOX
////Dialog Style End

class VrPanelDlg : public wxDialog
		void BotoAddClick(wxCommandEvent& event);
{
	private:
		DECLARE_EVENT_TABLE();
		
	public:
		VrPanelDlg(wxWindow *parent, wxWindowID id = 1, const wxString &title = wxT("VrPanel"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = VrPanelDlg_STYLE);
		virtual ~VrPanelDlg();
	
	private:
		//Do not add custom control declarations between 
		//GUI Control Declaration Start and GUI Control Declaration End.
		//wxDev-C++ will remove them. Add custom code after the block.
		////GUI Control Declaration Start
		wxStaticText *minuts;
		wxStaticText *Dbm;
		wxStaticText *RfLevel;
		wxStaticText *Test;
		wxTextCtrl *TestEdit;
		wxGauge *RfGauge;
		wxBitmapButton *BotoDelete;
		wxBitmapButton *BotoRead;
		wxBitmapButton *BotoAdd;
		wxStaticText *WxStaticText1;
		wxComboBox *WxComboBox1;
		wxTextCtrl *ZoneCodeEdit;
		wxStaticText *ZoneNum;
		wxTextCtrl *ZoneEdit;
		wxStaticText *Zona;
		wxStaticBox *ConfigVR;
		wxPanel *WxPanel1;
		////GUI Control Declaration End
		
	private:
		//Note: if you receive any error with these enum IDs, then you need to
		//change your old form code that are based on the #define control IDs.
		//#defines may replace a numeric value for the enum names.
		//Try copy and pasting the below block in your old form header files.
		enum
		{
			////GUI Enum Control ID Start
			ID_MINUTS = 1024,
			ID_DBM = 1023,
			ID_NIVEL = 1022,
			ID_TEST = 1021,
			ID_TESTEDIT = 1020,
			ID_RFGAUGE = 1019,
			ID_BOTODELETE = 1018,
			ID_BOTOREAD = 1017,
			ID_BOTOADD = 1016,
			ID_WXSTATICTEXT1 = 1015,
			ID_WXCOMBOBOX1 = 1014,
			ID_ZONECODEEDIT = 1013,
			ID_ZONENUM = 1012,
			ID_ZONEEDIT = 1010,
			ID_ZONAS = 1009,
			ID_CONFIGVR = 1008,
			ID_WXPANEL1 = 1007,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};
	
	private:
		void OnClose(wxCloseEvent& event);
		void CreateGUIControls();
};

#endif
