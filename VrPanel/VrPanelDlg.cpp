//---------------------------------------------------------------------------
//
// Name:        VrPanelDlg.cpp
// Author:      Joan
// Created:     29/01/2010 10:29:51
// Description: VrPanelDlg class implementation
//
//---------------------------------------------------------------------------

#include "VrPanelDlg.h"

//Do not add custom headers
//wxDev-C++ designer will remove them
////Header Include Start
#include "Images/VrPanelDlg_BotoDelete_XPM.xpm"
#include "Images/VrPanelDlg_BotoRead_XPM.xpm"
#include "Images/VrPanelDlg_BotoAdd_XPM.xpm"
////Header Include End

//----------------------------------------------------------------------------
// VrPanelDlg
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(VrPanelDlg,wxDialog)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(VrPanelDlg::OnClose)
END_EVENT_TABLE()
////Event Table End

VrPanelDlg::VrPanelDlg(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxDialog(parent, id, title, position, size, style)
{
	CreateGUIControls();
}

VrPanelDlg::~VrPanelDlg()
{
} 

void VrPanelDlg::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End.
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(8, 8), wxSize(496, 136));

	ConfigVR = new wxStaticBox(WxPanel1, ID_CONFIGVR, wxT("Configurar V�a Radio"), wxPoint(2, -1), wxSize(487, 129));

	Zona = new wxStaticText(WxPanel1, ID_ZONAS, wxT("Zona"), wxPoint(16, 30), wxDefaultSize, 0, wxT("Zona"));

	ZoneEdit = new wxTextCtrl(WxPanel1, ID_ZONEEDIT, wxT("1"), wxPoint(42, 28), wxSize(51, 20), wxTE_CENTRE, wxDefaultValidator, wxT("ZoneEdit"));

	ZoneNum = new wxStaticText(WxPanel1, ID_ZONENUM, wxT("C�digo Sensor"), wxPoint(114, 30), wxDefaultSize, 0, wxT("ZoneNum"));

	ZoneCodeEdit = new wxTextCtrl(WxPanel1, ID_ZONECODEEDIT, wxT("000000"), wxPoint(198, 28), wxSize(94, 20), 0, wxDefaultValidator, wxT("ZoneCodeEdit"));

	/* PIR
	TRCN
	TR-P4
	*/
	wxArrayString arrayStringFor_WxComboBox1;
	arrayStringFor_WxComboBox1.Add(wxT("PIR"));
	arrayStringFor_WxComboBox1.Add(wxT("TRCN"));
	arrayStringFor_WxComboBox1.Add(wxT("TR-P4"));
	WxComboBox1 = new wxComboBox(WxPanel1, ID_WXCOMBOBOX1, wxT(""), wxPoint(358, 25), wxSize(105, 23), arrayStringFor_WxComboBox1, 0, wxDefaultValidator, wxT("WxComboBox1"));

	WxStaticText1 = new wxStaticText(WxPanel1, ID_WXSTATICTEXT1, wxT("Tipo Zona"), wxPoint(300, 28), wxDefaultSize, 0, wxT("WxStaticText1"));

	wxBitmap BotoAdd_BITMAP (VrPanelDlg_BotoAdd_XPM);
	BotoAdd = new wxBitmapButton(WxPanel1, ID_BOTOADD, BotoAdd_BITMAP, wxPoint(26, 71), wxSize(37, 37), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoAdd"));

	wxBitmap BotoRead_BITMAP (VrPanelDlg_BotoRead_XPM);
	BotoRead = new wxBitmapButton(WxPanel1, ID_BOTOREAD, BotoRead_BITMAP, wxPoint(128, 72), wxSize(42, 35), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoRead"));

	wxBitmap BotoDelete_BITMAP (VrPanelDlg_BotoDelete_XPM);
	BotoDelete = new wxBitmapButton(WxPanel1, ID_BOTODELETE, BotoDelete_BITMAP, wxPoint(77, 72), wxSize(38, 35), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoDelete"));

	RfGauge = new wxGauge(WxPanel1, ID_RFGAUGE, 100, wxPoint(263, 66), wxSize(159, 19), wxGA_HORIZONTAL, wxDefaultValidator, wxT("RfGauge"));
	RfGauge->SetRange(100);
	RfGauge->SetValue(0);

	TestEdit = new wxTextCtrl(WxPanel1, ID_TESTEDIT, wxT("1"), wxPoint(264, 91), wxSize(51, 20), wxTE_CENTRE, wxDefaultValidator, wxT("TestEdit"));

	Test = new wxStaticText(WxPanel1, ID_TEST, wxT("�ltimo Test"), wxPoint(197, 94), wxDefaultSize, 0, wxT("Test"));

	RfLevel = new wxStaticText(WxPanel1, ID_NIVEL, wxT("Nivel RF"), wxPoint(197, 68), wxDefaultSize, 0, wxT("RfLevel"));

	Dbm = new wxStaticText(WxPanel1, ID_DBM, wxT("-114 dbm"), wxPoint(426, 67), wxDefaultSize, 0, wxT("Dbm"));

	minuts = new wxStaticText(WxPanel1, ID_MINUTS, wxT("minutos"), wxPoint(430, 91), wxDefaultSize, 0, wxT("minuts"));

	SetTitle(wxT("VrPanel"));
	SetIcon(wxNullIcon);
	SetSize(8,8,525,186);
	Center();
	
	////GUI Items Creation End
}

void VrPanelDlg::OnClose(wxCloseEvent& /*event*/)
{
	Destroy();
}

/*
 * BotoAddClick
 */
void VrPanelDlg::BotoAddClick(wxCommandEvent& event)
{
	// insert your code here
}
