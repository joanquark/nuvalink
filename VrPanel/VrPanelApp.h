//---------------------------------------------------------------------------
//
// Name:        VrPanelApp.h
// Author:      Joan
// Created:     29/01/2010 10:29:50
// Description: 
//
//---------------------------------------------------------------------------

#ifndef __VRPANELDLGApp_h__
#define __VRPANELDLGApp_h__

#ifdef __BORLANDC__
	#pragma hdrstop
#endif

#ifndef WX_PRECOMP
	#include <wx/wx.h>
#else
	#include <wx/wxprec.h>
#endif

class VrPanelDlgApp : public wxApp
{
	public:
		bool OnInit();
		int OnExit();
};

#endif
