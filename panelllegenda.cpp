#include "panelllegenda.h"

CPanelLlegenda::CPanelLlegenda(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CUnLang* lang)
{
    Create(parent,id,pos,size,style,name);

    /*if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,25);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,25);
    }*/

    llegenda=0;
    UnLang=lang;
    hSizer = new wxBoxSizer( wxVERTICAL );
    SetSizer( hSizer );
}

void CPanelLlegenda::Clear()
{
    if (llegenda) delete llegenda;
    llegenda=0;

    int i=0;
    while (hSizer->Detach(i++));
    hSizer->Layout();
    Layout();
    FitInside();
}

CPanelLlegenda::~CPanelLlegenda()
{
    Clear();
}

bool CPanelLlegenda::SetLlegenda(string &sLlegenda)
{
    if (!llegenda) {
        llegenda = new wxStaticText(this,-1,wxT(""), wxDefaultPosition, wxDefaultSize, wxSIMPLE_BORDER|wxALIGN_CENTER);
        hSizer->Add( llegenda , wxSizerFlags(1).Expand().Border(wxALL, 5) );
    }
    llegenda->SetLabel(_wx(sLlegenda));
    llegenda->SetFont(wxFont(12,74,90,90,0,wxT("Arial")));
    hSizer->Layout();
    Refresh();
    FitInside();
}

