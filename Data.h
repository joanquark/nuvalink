#ifndef DADES_H
#define DADES_H

#include <iostream>
#include <string>
using namespace std;
#include "msgs.h"

class CData
{
    string name;
    string type;
    string cusname;
    string address;
    string phone;
    string email;
    string phonePstn;
    string phoneCsd;
    string host;
    string hostCloud;
    string rule;            // certified rule(s)
    string simSolution;

    int    port;
    int    portCloud;
    string extPower;
    int    tBatt;
    int    conType;

    public:
        CData();
        void Clear();
        ~CData();
        CLanguage*    Lang;

        bool setname(const string& instal);
        bool settype(const string& instal);
        bool setcusname(const string& client);
        bool setaddress(const string& dir);
        bool setphone(const string& telf);
        bool setemail(const string& email);
        bool setphonePstn(const string& num);
        bool setphoneCsd(const string& num);
        bool setrule(const string& rule);
        bool setconType(int tipus);
        bool sethost(const string& ip);
        bool setport(int port);
        bool setextPower(string ext);
        bool settBatt(int tb);
        bool setSimSolution(string sol);

        string& getname();
        string& gettype();
        string& getcusname();
        string& getaddress();
        string& getphone();
        string& getemail();
        string& getphonePstn();
        string& getphoneCsd();
        string& getextPower();
        string& getrule();
        string& getSimSolution();


        int getconType();
        int getport();
        string& gethost();
        int getportCloud();
        string& gethostCloud();
        int gettBatt();
        string sgettBatt();

        CData operator= (CData param);
};

#endif
