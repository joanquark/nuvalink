#ifndef GRUP_H
#define GRUP_H

#include <iostream>
#include <vector>
#include <string>
using namespace std;

#include "codi.h"

class CGroup
{
    string descr;
    string id;
    CGroup *grupPare;
    vector<CCodi*> codis;
    vector<CGroup*> grups;
    vector<string *> bitdescr;
    bool isBitGroup, isFollowCodis, dontRx, dontSend;
    int distancia;
    int upl;
    string cat;

    public:
        CGroup();
        CGroup(CGroup *grupPare, string& descr);
        void Clear();
        ~CGroup();

        bool SetDescr(wxString &descr);
        bool SetId(string &id);
        bool SetFather(CGroup *grupPare);
        bool SetBitDescr(int bit, wxString& descr);
        bool SetBitGroup();
        bool IsBitGroup();
        bool SetUpl(int upl);
        int GetUpl(void);
        string& GetCat();
        bool SetCat(string& cat);
        bool SetFollowCodis();
        bool FollowCodis();
        bool SetDistance(int d);
        int  GetDistancia();
        bool Reset();
        bool SetNotRx();
        bool SetNotSend();
        bool DontRx();
        bool DontSend();

        int AddChild(CCodi *codiFill);
        int AddChild(CGroup *grupFill);
        int GetChildNum();
        int GetNumChildCodi();
        int GetNumChildGroup();
        CGroup* GetChildGroup(int index);
        CCodi* GetChildCodi(int index);
        CGroup* GetFather();
        string& GetDescr();
        string& GetBitDescr(int bit);
        string& GetId();
        bool GetChanges();
        void ClrChanges();
};

#endif
