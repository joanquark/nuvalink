#include "wx_pch.h"
#include "BeaconPanel.h"
#include "NuvaLink.h"


#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(BeaconPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(BeaconPanel)
//*)

//(*IdInit(BeaconPanel)
const long BeaconPanel::ID_LISTCTRL1 = wxNewId();
const long BeaconPanel::ID_SPEEDBUTTON1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(BeaconPanel,wxPanel)
	//(*EventTable(BeaconPanel)
	//*)
END_EVENT_TABLE()

BeaconPanel::BeaconPanel(wxWindow* parent)
{
	//(*Initialize(BeaconPanel)
	wxBoxSizer* BoxSizer1;
	wxStaticBoxSizer* StaticBoxSizer1;

	Create(parent, wxID_ANY, wxDefaultPosition, wxSize(544,524), wxTAB_TRAVERSAL, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(3, 1, 0, 0);
	StaticBoxSizer1 = new wxStaticBoxSizer(wxVERTICAL, this, _("BLE Devices"));
	ListBeacon = new wxListCtrl(this, ID_LISTCTRL1, wxDefaultPosition, wxSize(672,595), wxLC_REPORT|wxVSCROLL|wxHSCROLL, wxDefaultValidator, _T("ID_LISTCTRL1"));
	 ListBeacon->ClearAll();
	 ListBeacon->InsertColumn(0,"BLE-MAC");
	 ListBeacon->InsertColumn(1,"TTL");
	 ListBeacon->InsertColumn(2,"Status");
	 ListBeacon->InsertColumn(3,"RSSI");
	 ListBeacon->InsertColumn(4,"acc.");

	 ListBeacon->SetColumnWidth(0,100);
	 ListBeacon->SetColumnWidth(1,90);
	 ListBeacon->SetColumnWidth(2,90);
	 ListBeacon->SetColumnWidth(3,90);
	 ListBeacon->SetColumnWidth(4,90);
	StaticBoxSizer1->Add(ListBeacon, 1, wxALL|wxEXPAND, 5);
	BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	wxBitmap btRx_BMP(_("./icons/toolbar/download.png"), wxBITMAP_TYPE_ANY);
	btRx = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, btRx_BMP, 0, 2, -1, true, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	btRx->SetUserData(0);
	BoxSizer1->Add(btRx, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer1->Add(BoxSizer1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(FlexGridSizer1);
	SetSizer(FlexGridSizer1);
	Layout();

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&BeaconPanel::OnbtRxLeftClick);
	//*)
}

void BeaconPanel::SetNuvaLink(Cnuvalink* nuvalink,CLanguage* lang)
{
    this->nuvalink = nuvalink;
    this->Lang= lang;
    return true;
}

BeaconPanel::~BeaconPanel()
{
	//(*Destroy(BeaconPanel)
	//*)
}


void BeaconPanel::OnbtRxLeftClick(wxCommandEvent& event)
{

    nuvalink->progress=0;
    wxString m="MISS_DIALOG_TIT_RECV";
    wxString recv=Lang->GetAppMiss(m);
    wxProgressDialog *myPD = new wxProgressDialog(recv, recv,
        32, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);

    nuvalink->progressDlg = myPD;

    int len=nuvalink->RequestBleDevSt(&BleDevWhiteList[0].Mac[0]);
    if (len>0){
        int numbeacons=(int)len/sizeof(TBleDevSt);
        int CntPush=0;
        ListBeacon->ClearAll();
        ListBeacon->InsertColumn(0,"indx");
        ListBeacon->InsertColumn(1,"BLE-MAC");
        ListBeacon->InsertColumn(2,"TTL");
        ListBeacon->InsertColumn(3,"Status");
        ListBeacon->InsertColumn(4,"RSSI");
        ListBeacon->InsertColumn(5,"acc");

        ListBeacon->SetColumnWidth(0,90);
        ListBeacon->SetColumnWidth(1,120);
        ListBeacon->SetColumnWidth(2,90);
        ListBeacon->SetColumnWidth(3,90);
        ListBeacon->SetColumnWidth(4,90);
        ListBeacon->SetColumnWidth(5,90);
        for (int i=0;i<numbeacons;i++){
            int n=ListBeacon->InsertItem(CntPush, "-", 0);

            string cod="";
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[0]>>4);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[0]);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[1]>>4);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[1]);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[2]>>4);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[2]);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[3]>>4);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[3]);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[4]>>4);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[4]);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[5]>>4);
            cod+=nibble2ascii(BleDevWhiteList[i].Mac[5]);
            ListBeacon->SetItem(CntPush, 0, _wx(iToS(i)));
            ListBeacon->SetItem(CntPush, 1, _wx(cod));
            ListBeacon->SetItem(CntPush, 2, _wx(iToS(BleDevWhiteList[i].TTL)));
            ListBeacon->SetItem(CntPush, 3, _wx(iToS(BleDevWhiteList[i].opt)));
            ListBeacon->SetItem(CntPush, 4, _wx(iToS(BleDevWhiteList[i].rssi[1])));
            ListBeacon->SetItem(CntPush, 5, _wx(iToS(BleDevWhiteList[i].acc)));
            ListBeacon->EnsureVisible(CntPush);
        }
    }
    delete myPD;
    nuvalink->progressDlg=0;
    nuvalink->progress=0;
}


