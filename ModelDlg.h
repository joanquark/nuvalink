#ifndef MODELDLG_H
#define MODELDLG_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(ModelDlg)
	#include <wx/dialog.h>
	#include <wx/sizer.h>
	//*)
#endif
//(*Headers(ModelDlg)
#include <wxSpeedButton.h>
//*)

#include "languages/CLanguage.h"
#include "Data.h"
#include "DadesPanel.h"

class ModelDlg: public wxDialog
{
	public:

		ModelDlg(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize,wxString *model=NULL,wxString* ver=NULL,CLanguage *lang=NULL,CData *dades=NULL);
		virtual ~ModelDlg();

		//(*Declarations(ModelDlg)
		wxSpeedButton* ButtCancel;
		wxSpeedButton* ButtOk;
		//*)

	protected:


		//(*Identifiers(ModelDlg)
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		//*)

	private:

		//(*Handlers(ModelDlg)
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnButtCancelLeftClick(wxCommandEvent& event);
		//*)

		wxString *model;
		wxString *version;
		DadesPanel *panel;
	protected:

		void BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,wxString *model=NULL, wxString *ver=NULL,CLanguage *lang=NULL,CData *dades=NULL);

		DECLARE_EVENT_TABLE()
};

#endif
