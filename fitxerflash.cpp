#include "fitxerflash.h"

CFitxerFlash::CFitxerFlash()
{
    id="";
    descr="";
    lon=0;
    sector=0;
    index=0;
    nomFile="";
    fitxer=0;
    fitxerLength=-1;
    selec=false;

    //els webserver son ROM
    isROM=false;
}

CFitxerFlash::~CFitxerFlash()
{
    Llibera();
}

bool CFitxerFlash::SetId(string &id)
{
    this->id = id;
    return true;
}

bool CFitxerFlash::SetDescr(wxString &descr)
{
    this->descr = descr;
    return true;
}

bool CFitxerFlash::SetNomFitxer(string &nomFitxer)
{
    if (nomFile != nomFitxer) {
        Llibera();
        nomFile = nomFitxer;
    }

    return true;
}

bool CFitxerFlash::SetIndex(int index)
{
    if (index<0)
        return false;

    this->index = index;
    return true;
}

bool CFitxerFlash::SetSector(int sector)
{
    if (sector<0)
        return false;

    this->sector = sector;
    return true;
}

bool CFitxerFlash::SetLon(int lon)
{
    if (lon < 0)
        return false;

    this->lon = lon;
    return true;
}

bool CFitxerFlash::SetFileExt(string &sFileExt)
{
    this->FileExt=sFileExt;
}

string& CFitxerFlash::GetId()
{
    return id;
}

wxString& CFitxerFlash::GetDescr()
{
    return descr;
}

string& CFitxerFlash::GetNomFitxer()
{
    return nomFile;
}

int CFitxerFlash::GetIndex()
{
    return index;
}

int CFitxerFlash::GetSector()
{
    return sector;
}

int CFitxerFlash::GetLon()
{
    return lon;
}

string &CFitxerFlash::GetFileExt(){
    return FileExt;
}

unsigned char *CFitxerFlash::GetFile()
{
    //Si criden la funci� varies vegades ja tindre el fitxer en memoria i no em caldra
    //llegirlo cada cop
    if (fitxer==0) {
        if (nomFile != "") {
            ifstream fs;
            fs.open( (char *) nomFile.c_str() );
            if (fs.is_open()) {
                int length = GetBytes();

                // get length of file:
                fs.seekg (0, ios::end);
                int fLength = fs.tellg();
                fs.seekg (0, ios::beg);

                if (fLength > length)
                    fitxerLength = length;
                else
                    fitxerLength = fLength;

                fitxer = new unsigned char [fitxerLength+1];
                fs.read ((char *)fitxer,fitxerLength);

            }
            fs.close();
        }
    }

    if (!isROM)
        return fitxer;
    else
        return (fitxer+sizeof(TRomInfo));
}

void CFitxerFlash::Llibera()
{
    if (fitxer) {
        delete fitxer;
        fitxer=0;
    }
}

int CFitxerFlash::GetBytes()
{
    return lon*256*16;
}

int CFitxerFlash::GetBytesFitxer()
{
    //Cridem a la funcio que obre el fitxer si encara no l'haviem obert
    if (fitxerLength==-1)
        unsigned char *re = GetFile();

    return fitxerLength;
}

bool CFitxerFlash::SetSelec(bool selec)
{
    this->selec = selec;
    return true;
}

bool CFitxerFlash::IsSelec()
{
    return selec;
}

bool CFitxerFlash::Reset()
{
    Llibera();
    nomFile="";
    selec=false;
}

bool CFitxerFlash::SetROM(bool rom)
{
    isROM = rom;
    return true;
}

bool CFitxerFlash::IsROM()
{
    return isROM;
}

TRomInfo *CFitxerFlash::GetROMInfo()
{
    if (isROM) {
        //Cridem a la funcio que obre el fitxer si encara no l'haviem obert
        if (fitxer==0)
            unsigned char *re = GetFile();
        return (TRomInfo *)fitxer;
    }
    else {
        return 0;
    }
}
