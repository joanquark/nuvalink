#include "wx_pch.h"
#include "MemPanel.h"
#include "NuvaLink.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(MemPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(MemPanel)
//*)

//(*IdInit(MemPanel)
const long MemPanel::ID_STATICTEXT1 = wxNewId();
const long MemPanel::ID_TEXTCTRL1 = wxNewId();
const long MemPanel::ID_STATICTEXT2 = wxNewId();
const long MemPanel::ID_CHOICE1 = wxNewId();
const long MemPanel::ID_STATICTEXT3 = wxNewId();
const long MemPanel::ID_TEXTCTRL3 = wxNewId();
const long MemPanel::ID_TEXTCTRL2 = wxNewId();
const long MemPanel::ID_SPEEDBUTTON1 = wxNewId();
const long MemPanel::ID_SPEEDBUTTON2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(MemPanel,wxPanel)
	//(*EventTable(MemPanel)
	//*)
END_EVENT_TABLE()

MemPanel::MemPanel(wxWindow* parent)
{
	//(*Initialize(MemPanel)
	wxFlexGridSizer* FlexGridSizer1;
	wxStaticBoxSizer* StaticBoxSizer1;
	wxStaticBoxSizer* StaticBoxSizer2;

	Create(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(2, 1, 0, 0);
	StaticBoxSizer1 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Mem debug"));
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Mem Address"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	StaticBoxSizer1->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtMemAdd = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	StaticBoxSizer1->Add(TxtMemAdd, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Mem Type"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	StaticBoxSizer1->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	choiceType = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	choiceType->Append(_("RAM"));
	choiceType->Append(_("FLASH"));
	StaticBoxSizer1->Add(choiceType, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Length"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	StaticBoxSizer1->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtLen = new wxTextCtrl(this, ID_TEXTCTRL3, _("16"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
	StaticBoxSizer1->Add(TxtLen, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtData = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxSize(632,164), wxTE_MULTILINE|wxTE_RICH, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	FlexGridSizer1->Add(TxtData, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("actions"));
	wxBitmap BtDownload_BMP(_("./icons/toolbar/download.png"), wxBITMAP_TYPE_ANY);
	BtDownload = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, BtDownload_BMP, 0, 2, -1, true, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	BtDownload->SetUserData(0);
	StaticBoxSizer2->Add(BtDownload, 1, wxALL, 5);
	wxBitmap BtUpload_BMP(_("./icons/toolbar/upload.png"), wxBITMAP_TYPE_ANY);
	BtUpload = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, BtUpload_BMP, 0, 2, -1, true, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	BtUpload->SetUserData(0);
	StaticBoxSizer2->Add(BtUpload, 1, wxALL, 5);
	FlexGridSizer1->Add(StaticBoxSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(FlexGridSizer1);
	FlexGridSizer1->Fit(this);
	FlexGridSizer1->SetSizeHints(this);

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&MemPanel::OnBtDownloadLeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&MemPanel::OnBtUploadLeftClick);
	//*)
}

MemPanel::~MemPanel()
{
	//(*Destroy(MemPanel)
	//*)
}

void MemPanel::SetNuvaLink(Cnuvalink* nuvalink,CLanguage* lang)
{
    this->nuvalink = nuvalink;
    return true;
}

void MemPanel::ShowRawData(unsigned char* raw,int len,bool rx)
{
    if (rx){
        TxtData->SetDefaultStyle(wxTextAttr(*wxBLUE, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        TxtData->AppendText(">");
    }else{
        TxtData->SetDefaultStyle(wxTextAttr(*wxRED, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        TxtData->AppendText(">");
    }
    wxString datafr="";
    for (int i=0; i<len; i++) {
        datafr.Append((wxChar)nibble2ascii((raw[i]&0xF0)>>4));
        datafr.Append((wxChar)nibble2ascii((raw[i]&0x0F)));
        if (i<len-1){ //*text << ", ";
            datafr.Append(',');
            datafr.Append(' ');
        }
    }
    datafr+="\n";
    TxtData->AppendText(datafr);
}


void MemPanel::OnBtDownloadLeftClick(wxCommandEvent& event)
{
//    nuvalink->progress=0;
//    wxProgressDialog *myPD = new wxProgressDialog("RxMem", "RxMem",
//        32, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);

//    nuvalink->progressDlg = myPD;

    unsigned char data[64000];

    bool ram=true;
    if (choiceType->GetSelection()!=0)
        ram=false;

    long numbyte=0;
    TxtLen->GetValue().ToULong(&numbyte);

    if (nuvalink->RxMemBlock(numbyte,TxtMemAdd->GetValue().c_str(),data,ram)){
        ShowRawData(data,numbyte,true);
    }
//    delete myPD;
//    nuvalink->progressDlg=0;
//    nuvalink->progress=0;

}

void MemPanel::OnBtUploadLeftClick(wxCommandEvent& event)
{
    wxString data=TxtData->GetValue();
    string dt=data.c_str();
    BYTE buff[512];

    AsciiChainToHex(&dt[0],buff,data.length());

    bool ram=true;
    if (choiceType->GetSelection()!=0)
        ram=false;

    long numbyte=0;
    TxtLen->GetValue().ToULong(&numbyte);

    nuvalink->TxMemBlock(numbyte,TxtMemAdd->GetValue().c_str(),buff,ram);

}



