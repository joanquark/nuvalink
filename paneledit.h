#ifndef PANELEDIT_H
#define PANELEDIT_H

//#define _ALIAS_ESTAT

#include <wx/wxprec.h>
#include <wx/textctrl.h>
#include <wx/panel.h>
#include <wx/scrolwin.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include <iostream>
#include <string>
#include <vector>
using namespace std;

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "codi.h"
#include "model.h"
#include "grup.h"
#include "Status.h"
#include "panelcodi.h"
#include "panelmesura.h"
#include "panelverify.h"
#include "VModemPanel.h"
#include "VCOMPanel.h"
#include "MemPanel.h"
#include "AgroPanel.h"
#include "panelvr.h"
#include "panelcfgvr.h"
#include "panelvruser.h"
#include "panelcfgvruser.h"
#include "panelvrout.h"
#include "panelcfgvrout.h"
#include "EstatPanel.h"
#include "EventsPanel.h"
#include "BeaconPanel.h"
#include "DadesPanel.h"
#include "panelflashgrup.h"
#include "privlevel.h"


class Cnuvalink;

class CPanelEdit;

class CPanelEditEvt: public wxEvtHandler
{
    public:
        CPanelEditEvt(CPanelEdit *parent){ptr_winPan=parent;}

    protected:
        CPanelEdit *ptr_winPan;
        DECLARE_EVENT_TABLE()
};

class CPanelEdit: public wxScrolledWindow
{
    wxBoxSizer *hSizer, *vSizer;
    friend class CPanelEditEvt;
    protected:

        Cnuvalink* nuvalink;
        wxPoint m_tmppoint;
        wxSize  m_tmpsize;
        wxPoint& VwXSetwxPoint(long x,long y);
        wxSize& VwXSetwxSize(long w,long h);

        wxStaticText *titol;
        vector<CPanelCodi *> vPanelsCodi;
        vector<CPanelMesura *> vPanelsMesura;
        EstatPanel* panelEstat;
        BeaconPanel* panelBeaconSt;
        MemPanel* panelMem;
        AgroPanel* panelAgro;
        CPanelVR *panelVR;
        CPanelCfgVR *panelCfgVR;
        CPanelVRUser *panelVRUser;
        CPanelCfgVRUser *panelCfgVRUser;
        CPanelVROut *panelVROut;
        CPanelCfgVROut *panelCfgVROut;
        EventsPanel *panelEvents;
        DadesPanel *panelDades;
        CPanelVerify *panelVerify;
        VModemPanel *panelVModem;
        VCOMPanel *panelVCOM;
        CPanelFlashGrup *panelFlashGrup;
        int x_pos;

        //para asistente
        string model;
        string versio;
        int privlevel;
        string PanelVersion;
        string PanelId;


    public:

        CPanelEdit(wxWindow* parent, Cnuvalink* nuvalink, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage* lang=0);
        void Clear();
        ~CPanelEdit();
        CLanguage*    Lang;
        void init();

        void SetTitle(string &stitol);
        void ClrTitle();
        int GenPanelCodi(CCodi *codi,Cnuvalink* nuva, unsigned int tzona,int privlevel);
        CCodi *GetCodi(int index);
        bool GenPanelStatus(CStatus *estat, Cnuvalink *nuvalink, CGroup *grupZAlias, CGroup *grupAAlias, CGroup *grupOAlias,string ver,string id);
        CStatus *GetStatus();
        bool GenPanelBeacons(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias);       // passem els alias.. quedar� millor!
        bool GenPanelMem(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias);
        bool GenPanelAgro(CData *data,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias);
        bool GenPanelWireless(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias);       // passem els alias.. quedar� millor!
        bool GenPanelCfgWireless(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias,int fzone);       // passem els alias.. quedar� millor!
        CStatus *GetStatusViaRadios();
        bool GenPanelWirelessUser(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupUAlias);
        bool GenPanelCfgWirelessUser(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupUAlias,int user);
        bool GenPanelWirelessOut(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupOAlias);
        bool GenPanelCfgWirelessOut(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupOAlias,int out);
        bool GenPanelEvents(Cnuvalink *nuvalink,CEventList *events,CGroup *grupZAlias,CGroup *grupAAlias, CGroup *grupUAlias, CGroup *grupOAlias,string ver,string id);
        bool GenPanelVerify(Cnuvalink *nuvalink,CModel *sel,string ver,string id);
        bool GenPanelVModem(CProtocolNIOT* protocolniot, CConfig* conf,CModel* sel,string ver,string id);
        bool GenPanelVCOM(Cnuvalink* nuva, CProtocolNIOT* protocolniot, CConfig* conf,CModel* sel,string ver,string id);
        CEventList *GetEventList();
        bool GenPanelDades(CData *dades,wxString *model, wxString *ver);
        CData *GetData();
        bool CreaPanelFlashGrup(CGroupFlash *grupFlash);
        CGroupFlash *GetGroupFlash();
        void UpdateValues();
        void ApplyValues();
        void ReadPanelVR();
        void ReadPanelUserVR();
        void ReadPanelOutVR();

        void ReadCodi(CCodi* codi);
        void SendCodi(CCodi* codi);

    protected:
        // DECLARE_EVENT_TABLE()C:\LOCALPRJ\01.Cpp\NuvaLink\.objs\01.Cpp\NuvaLink\SSL
};
// end CPanelEdit

#endif



