#include "config.h"
#include "privlevel.h"
#include <wx/tokenzr.h>
#include <wx/filename.h>
#include "util.h"
#include "jsonval.h"
#include "jsonwriter.h"
#include "jsonreader.h"
#include <wx/stdpaths.h>

extern "C" {
    #include "SSL/SSLxfer.h"
}

CConfig::CConfig()
{
    tagConexions=false;
    ConnectionType=0;
    tmpMap=0;
    blank="";
    login="true";
    privlevel=-1;       // till login invalid value.

    eng["code"]="n1v23t45";
    eng["instal"]="instal";
    eng["master"]="master";
    eng["flash"]="f1l23h45";
    eng["admin"]="d1m23n45";
    eng["produccion"]="produccion";
    eng["defuser"]="master";
    eng["defpass"]="master";

    cat=_T("ESC;MODOS;AREAAUT;OUTAUT;CRA;VOX;TXT;IP;ADV;TBFLASH");
    lang="es";
    cloudmode=false;
    jsonwithdevices="";
    clouddevices=0;
    SysUser="";
    SysPass="";
}

CConfig::~CConfig()
{
    Clear();
}

void CConfig::Clear()
{
    conLocal.clear();
    conModem.clear();
    conCsd.clear();
    conTcpip.clear();
    ultims.clear();
    tagConexions=false;
    ConnectionType=0;
    tmpMap=0;

}


void CConfig::SetRememberUser(wxString user){
    eng["defuser"]=user;
}

void CConfig::SetRememberPass(wxString pass){
    eng["defpass"]=pass;
}

//Obre l'arxiu config.xml i agafa els valors interpretant el XML
bool CConfig::Load()
{
    wxFileName filewx;
    wxString fileName="config.xml";

    if (filewx.FileExists(fileName)){
        ifstream fitxer;
        fitxer.open( (char *) fileName.c_str() );
        if (fitxer.is_open()) {
            wxString buffin;
            string line;
            while (! fitxer.eof() )
            {
                getline (fitxer,line);
                buffin += _wx(line)+"\n";           // getline looses the \n
            }
            fitxer.close();
            if (buffin.Contains("xml version")==false){
                ofstream filewr;
                filewr.open( (char *) fileName.c_str(), ifstream::out );
                if (filewr.good()) {
                    filewr <<"<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
                    filewr << buffin;
                    filewr.close();
                }else{
                    filewr.close();
                    return false;
                }
            }
        }else{
            msgError = Lang->GetAppMiss("MISS_ERROR_READ_FILE");
            msgError += _wx(fileName);
            return false;
        }
        //Analitzem el XML
        myXmlParser xml;
        //Li diem al objecte que interpreta el xml que informi al objecte instalacio
        xml.SetSubscriber(*this);
        bool docParsed = xml.parse(fileName);

        if (!docParsed) {
            msgError = Lang->GetAppMiss("MISS_ERROR_BAD_XML").c_str();
            return false;
        }

    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_READ_FILE");
        msgError += _wx(fileName);
        return false;
    }

    Lang->SetDefLang(lang);       // default lang if not UN model used.

    return true;
}


bool CConfig::Save()
{
    ofstream fitxer;
    string buffer;
    string fileName = "config.xml";

    fitxer.open( (char *) fileName.c_str(), ifstream::out );
    if (fitxer.good()) {
        GenConfigXML(&buffer);
        fitxer << buffer;
        fitxer.close();
    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_WRITE_FILE") + _wx(fileName);
        return false;
    }

    return true;
}

bool CConfig::GenConfigXML(string *xml)
{
    std::map<wxString, wxString>::iterator iter;

    *xml = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n";
    *xml += "<config>\n";
    //Info OEM
    *xml += "\t<oem>\n";
    for (iter = oem.begin(); iter != oem.end(); iter++) {
        *xml += "\t\t<" + iter->first + ">" + iter->second + "</" + iter->first + ">\n";
    }
    *xml += "\t</oem>\n";
    // info eng
    *xml += "\t<eng>\n";

    *xml+= "\t\t<login>"+login+"</login>\n";
    *xml+= "\t\t<cat>"+cat+"</cat>\n";
    *xml+= "\t\t<lang>"+lang+"</lang>\n";

    for (iter = eng.begin(); iter != eng.end(); iter++) {
        //wxString codesave=codify(iter->second);
        wxString codesave = CipherTag(iter->second,true);
        *xml += "\t\t<" + iter->first + ">" + codesave/* iter->second*/ + "</" + iter->first + ">\n";
    }
    *xml += "\t</eng>\n";

    //Info inicial
    *xml += "\t<ini>\n";
    for (int i=0; i<ultims.size(); i++)
        *xml += "\t\t<ultim>" + ultims[i] + "</ultim>\n";
    *xml += "\t</ini>\n";

    //Conexions
    *xml += "\t<conexions>\n";

    if (!conLocal.empty()) {
        *xml += "\t\t<local>\n";
        for(iter = conLocal.begin(); iter != conLocal.end(); iter++) {
            *xml += "\t\t\t<" + iter->first + ">" + iter->second + "</" + iter->first + ">\n";
        }
        *xml += "\t\t</local>\n";
    }

    if (!conModem.empty()) {
        *xml += "\t\t<modem>\n";
        for(iter = conModem.begin(); iter != conModem.end(); iter++) {
            *xml += "\t\t\t<" + iter->first + ">" + iter->second + "</" + iter->first + ">\n";
        }
        *xml += "\t\t</modem>\n";
    }

    if (!conCsd.empty()) {
        *xml += "\t\t<modem-gsm>\n";
        for(iter = conCsd.begin(); iter != conCsd.end(); iter++) {
            *xml += "\t\t\t<" + iter->first + ">" + iter->second + "</" + iter->first + ">\n";
        }
        *xml += "\t\t</modem-gsm>\n";
    }

    if (!conTcpip.empty()) {
        *xml += "\t\t<tcpip>\n";
        for (iter = conTcpip.begin(); iter != conTcpip.end(); iter++) {
            wxString name=iter->first;
            wxString value=iter->second;
            if (name=="ipclouddest"){
                if (value!="")
                    value=CipherTag(value,true);

            }else if (name=="ipcloudport"){
                if (value!="")
                    value=CipherTag(value,true);

            }
            *xml += "\t\t\t<" + name + ">" + value + "</" + name + ">\n";
        }
        *xml += "\t\t</tcpip>\n";
    }

    *xml += "\t</conexions>\n";
    *xml += "</config>\n";

    return true;
}

void CConfig::foundNode ( wxString & name, wxXmlNode* attributes )
{
    if (name=="conexions") {
        tagConexions=true;
    }
    else if (name=="local") {
        tmpMap = &conLocal;
    }
    else if (name=="modem") {
        tmpMap = &conModem;
    }
    else if (name=="modem-gsm") {
        tmpMap = &conCsd;
    }
    else if (name=="tcpip") {
        tmpMap = &conTcpip;
    }
    else {
        tagConexions = false;
    }

    lastTag = name;
}

void CConfig::foundElement ( wxString & name, wxString & value, wxXmlNode* attributes )
{
    wxString tmp;

    if (tagConexions) {
        if (tmpMap){
            if (name=="ipclouddest"){
                if (value!="")
                    value=UnCipherTag(value,true,true);
                else
                    value="127.0.0.1";
            }else if (name=="ipcloudport"){
                if (value!="")
                    value=UnCipherTag(value,true,true);
                else
                    value="11111";
            }
            (*tmpMap)[name] = value;
        }
    }
    else if (lastTag == "oem") {
        oem[name] = value;
    }
    else if (lastTag == "eng") {
        if (name=="login"){
            login=value;
        }else if (name=="cat"){
            cat=value;
        }else if (name=="lang"){
            lang=value;
        }else{
            tmp=UnCipherTag(_wx(value),true,true);
            if (tmp!="")
                eng[name]=tmp;
        }
    }
    else if (lastTag == "ini") {
        if (name == "ultim") {
            if (ultims.size() < MAX_LAST_FILESOPEN)
                ultims.push_back(value);
        }
    }
}

bool CConfig::IsCat(wxString catval)
{
    wxString tok;
    if (catval=="ON")
        return true;


    wxStringTokenizer arrTok(cat, wxT(";"));
    while ( arrTok.HasMoreTokens() )
    {
        tok=arrTok.GetNextToken();
        if (catval==tok)
            return true;
    }
    return false;
}

bool CConfig::SetCat(wxString newcat)
{
    this->cat=newcat;
}


wxString& CConfig::GetConnectionParam(const wxString& param)
{
    wxString blanc="";

    if (ConnectionType == TYPE_RS485_CONNECTION || ConnectionType == TYPE_USB_CONNECTION) {
        tmpMap = &conLocal;
    }
    else if (ConnectionType == TYPE_TCPIP_CONNECTION || /*ConnectionType == TIPUS_CONEXIO_ACDC || */ConnectionType==TYPE_CLOUD_CONNECTION) {
        tmpMap = &conTcpip;
    }
    else tmpMap = 0;

    if (tmpMap)
        return (*tmpMap)[param];
    else
        blanc;
}

wxString& CConfig::GetParamOem(const wxString& param)
{
    return oem[param];
}

wxString  CConfig::GetJSONDevices(void)
{
    return jsonwithdevices;
}

wxString  CConfig::GetSysUser(void)
{
    return SysUser;
}

wxString  CConfig::GetSysPass(void)
{
    return SysPass;
}

void  CConfig::SetCodeLot(wxString lot)
{
    CodeLot=lot;
}

wxString  CConfig::GetCodeLot(void)
{
    return CodeLot;
}

bool CConfig::SetParamConexio(wxString param, wxString valor)
{
    if (ConnectionType == TYPE_RS485_CONNECTION  || ConnectionType==TYPE_USB_CONNECTION) {
        tmpMap = &conLocal;
    }
/*    else if (ConnectionType == TIPUS_CONEXIO_MODEM) {
        tmpMap = &conModem;
    }
    else if (ConnectionType == TIPUS_CONEXIO_CSD) {
        tmpMap = &conCsd;
    }*/
    else if (ConnectionType == TYPE_TCPIP_CONNECTION /*|| ConnectionType == TIPUS_CONEXIO_ACDC */ || ConnectionType==TYPE_CLOUD_CONNECTION) {
        tmpMap = &conTcpip;
    }
    else {
        tmpMap = 0;
        return false;
    }

    (*tmpMap)[param] = valor;
    return true;
}

bool CConfig::SetParamTcpIp(wxString param,wxString ip)
{
    conTcpip[param]=ip;
}

wxString CConfig::GetParamTcpIp(wxString param)
{
    return conTcpip[param];
}

wxString& CConfig::GetEngCode()
{
    return eng["code"];
}

wxString CConfig::GetParamEng(wxString param)
{
    return eng[param];
}

wxString CConfig::GetSessionServer()
{
    if (sessionServer.IsEmpty())
        sessionServer=GetParamTcpIp("ipclouddest");

    return sessionServer;
}

void CConfig::SetSessionServer(wxString s)
{
    sessionServer=s;
}


int CConfig::Login(wxString user,wxString codein){


    if (user.Contains("@")){
        // this is cloud mode login, so validate login in server's DB.
        wxJSONValue root;
        int answersize=0;
        char devices[32768];
        root["Login"]=user;
        root["pass"]=codein;
        root["Op"]="getcount";

        wxJSONWriter writer( wxJSONWRITER_NONE );
        wxString jsontxt;
        writer.Write(root,jsontxt);
        jsontxt+="\r\n";
        string portcloud=GetParamTcpIp("ipcloudport").c_str();
        //string ipcloud=GetParamTcpIp("ipclouddest").c_str();
        string ipcloud=GetSessionServer().c_str();
        if (wxFileExists("nuvathings.pem")){
            CipherFile("nuvathings.pem","nvt.nvk",false);
            wxRemoveFile( "nuvathings.pem" );
        }
        UnCipherFile("nvt.nvk","nvt.pem",false);
        wxStandardPaths path;
        wxString filepem=path.GetDataDir();

        if (SSLtransfer(ipcloud.c_str(),sToI(portcloud)-1,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsontxt.size(),&devices[0],&answersize)==TRUE){
            wxRemoveFile("nvt.pem");
            jsonwithdevices="";
            for (int i=0;i<answersize;i++){
                jsonwithdevices.Append(wxChar(devices[i]));
            }
            wxJSONReader reader;

            int numErrors = reader.Parse( jsonwithdevices, &root );
            if ( numErrors > 0 )  {
                // if there are errors in the JSON document return
                return;
            }
            // now display the column names;
            clouddevices = root["count"].AsInt();
            privlevel = root["privlevel"].AsInt();
            cloudmode=true;
            //privlevel=ENG_PRIVLEVEL;
            //wxMessageBox(jsonwithdevices);
            //jsonwithdevices=_wx(devices);
            if (privlevel>=0){
                SysUser=user;
                SysPass=codein;
                if (privlevel>=DEBUG_PRIVLEVEL)
                    privlevel=ENG_PRIVLEVEL;
            }
            return privlevel;
        }
        wxRemoveFile("nvt.pem");
        privlevel=-1;
        return -1;

    }
    cloudmode=false;
    if ((codein=="d1b23g45") && (user=="engineer")){
        privlevel=DEBUG_PRIVLEVEL;
        SysUser=user;
        SysPass=codein;
        return DEBUG_PRIVLEVEL;
    }
    if (codein==eng["code"] && user=="engineer"){
        privlevel=ENG_PRIVLEVEL;
        SysUser=user;
        SysPass=codein;
        return ENG_PRIVLEVEL;
    }else if (codein==eng["produccion"] && user=="produccion"){
        SysUser=user;
        SysPass=codein;
        privlevel=PROD_PRIVLEVEL;
        return PROD_PRIVLEVEL;
    }else if (codein==eng["admin"] && user=="admin"){
        SysUser=user;
        SysPass=codein;
        privlevel=ADMIN_PRIVLEVEL;
        return ADMIN_PRIVLEVEL;
    }else if (codein==eng["master"] && user=="master"){
        SysUser=user;
        SysPass=codein;
        privlevel=MASTER_PRIVLEVEL;
        return MASTER_PRIVLEVEL;
    }else if (codein==eng["instal"]){
        SysUser=user;
        SysPass=codein;
        privlevel=INSTAL_PRIVLEVEL;
        return INSTAL_PRIVLEVEL;
    }

    privlevel=-1;
    return -1;
}


int CConfig::GetConnectionType()
{
    return ConnectionType;
}

bool CConfig::SetConnectionType(int tipus)
{
    ConnectionType = tipus;
    return true;
}

wxString& CConfig::GetLastError()
{
    return msgError;
}

wxString& CConfig::GetLast(int index)
{
    if ((index<0) || (index >= ultims.size()))
        return blank;

    return ultims[index];
}

bool CConfig::AddLast(wxString& nomFitxer)
{
    bool trobat=false;

    //Si ja hi es a la llista el coloquem el primer
    for (int i=0; i<ultims.size(); i++) {
        if (ultims[i] == nomFitxer) {
            trobat=true;
            if (i>0) {
                for (int j=i; j>0; j--)
                    ultims[j] = ultims[j-1];
                ultims[0] = nomFitxer;
            }
            break;
        }
    }

    //Si no hi es, l'afegim el primer
    if (!trobat) {
        if (ultims.size() == MAX_LAST_FILESOPEN) {
            for (int i=MAX_LAST_FILESOPEN-1; i>0; i--)
                ultims[i] = ultims[i-1];
        }
        else if (ultims.size() == 0) {
            ultims.push_back(nomFitxer);
        }
        else {
            ultims.push_back("");
            for (int i=ultims.size()-1; i>0; i--)
                ultims[i] = ultims[i-1];
        }

        ultims[0] = nomFitxer;
    }
}
