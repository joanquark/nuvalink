#ifndef PANELCFGVRUSER_H
#define PANELCFGVRUSER_H

#include <iostream>
using namespace std;

class Cnuvalink;

#include <wx/panel.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include "Status.h"
#include "msgs.h"
#include "grup.h"
#include "codi.h"

#define MAX_PANELCFGVR_USER    16
#define MIN_PANELCFGVR_USER     1


class CPanelCfgVRUser: public wxPanel
{
    friend class CPanelEditEvt;
    protected:
        wxPoint m_tmppoint;
        wxSize  m_tmpsize;
        wxPoint& VwXSetwxPoint(long x,long y);
        wxSize& VwXSetwxSize(long w,long h);

/*        wxGrid *grid;
        wxBoxSizer *vSizer;*/
        CStatus *estat;
        CCodi *codi;
        int   fixuser;

		wxStaticText *minuts;
		wxStaticText *Dbm;
		wxStaticText *RfLevel;
		wxStaticText *Test;
		wxTextCtrl *TestEdit;
		wxGauge *RfGauge;

		wxBitmapButton *BotoDelete;
		wxBitmapButton *BotoRead;
		wxBitmapButton *BotoAdd;
		wxStaticText *WxStaticText1;
		wxTextCtrl *ZoneCodeEdit;
		wxStaticText *ZoneNum;
		wxTextCtrl *ZoneEdit;
		wxStaticText *Zona;
		wxStaticBox *ConfigVR;
		wxPanel *WxPanel1;

		enum
		{
			////GUI Enum Control ID Start
			ID_BOTODELETE = 1018,
			ID_BOTOREAD = 1017,
			ID_BOTOADD = 1016,
			ID_WXSTATICTEXT1 = 1015,
			ID_ZONECODEEDIT = 1013,
			ID_ZONENUM = 1012,
			ID_ZONEEDIT = 1010,
			ID_ZONAS = 1009,
			ID_CONFIGVR = 1008,
			ID_WXPANEL1 = 1007,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};

    public:

        Cnuvalink *nuvalink;
        string PanelVersion;
        string PanelId;
        CLanguage*    Language;
        bool SetGrups(CGroup *grupUAlias);
        string CUalias[24];

        CPanelCfgVRUser(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage *lang=0,Cnuvalink* eli=0,int fzone=-1);
        void Clear();
        ~CPanelCfgVRUser();

        bool SetStatus(CStatus *estat);
        CStatus *GetStatus();
        void UnSetStatus();
        bool ApplyValues();
        bool UpdateValues();
        void ButtAddClick(wxCommandEvent& event);
        void ButtDeleteClick(wxCommandEvent& event);
        void ButtReadClick(wxCommandEvent& event);
        void Read();

    protected:
        void GenComponents();

	private:
		DECLARE_EVENT_TABLE();
};

#endif
