#include "panelcfgvr.h"
#include "nuvalink.h"
#include "mesura.h"

BEGIN_EVENT_TABLE(CPanelCfgVR,wxPanel)
	////Manual Code Start
	////Manual Code End
	EVT_BUTTON(ID_BOTOADD,CPanelCfgVR::BotoAddClick)
	EVT_BUTTON(ID_BOTODELETE,CPanelCfgVR::BotoDeleteClick)
	EVT_BUTTON(ID_BOTOREAD,CPanelCfgVR::BotoReadClick)

END_EVENT_TABLE()


CPanelCfgVR::CPanelCfgVR(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CUnLang* lang,Cnuvalink* eli,int fzone)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,150);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,150);
    }

    UnLang=lang;
    fixzone=fzone;
    //punters a 0
    estat=0;
    nuvalink=eli;
    WxPanel1=0;

    GeneraComponents();
}

void CPanelCfgVR::Clear()
{

}

CPanelCfgVR::~CPanelCfgVR()
{
    Clear();
    estat=0;
}

wxPoint& CPanelCfgVR::VwXSetwxPoint(long x,long y)
{
    m_tmppoint.x=x;
    m_tmppoint.y=y;
    return m_tmppoint;
}

wxSize& CPanelCfgVR::VwXSetwxSize(long w,long h){
    m_tmpsize.SetWidth(w);
    m_tmpsize.SetHeight(h);
    return m_tmpsize;
}

void CPanelCfgVR::GeneraComponents()
{

        Clear();

        WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(8, 8), wxSize(700, 140));

        ConfigVR = new wxStaticBox(WxPanel1, ID_CONFIGVR, UnLang->GetAppMiss("MSG_PANVR_TITLE"), wxPoint(4, 1), wxSize(665, 135));

        Zona = new wxStaticText(WxPanel1, ID_ZONAS, UnLang->GetAppMiss("MSG_PAN_COL_ZONE"), wxPoint(16, 30), wxDefaultSize, 0, wxT("Zone"));

        ZoneEdit = new wxTextCtrl(WxPanel1, ID_ZONEEDIT, wxT("1"), wxPoint(52, 29), wxSize(51, 20), wxTE_CENTRE, wxDefaultValidator, wxT("ZoneEdit"));
        if ((fixzone<=MAX_PANELCFGVR_ZONE) && (fixzone>=MIN_PANELCFGVR_ZONE)){
            ZoneEdit->SetValue(_wx(iToS(fixzone)));
            ZoneEdit->SetEditable(false);
        }

        ZoneNum = new wxStaticText(WxPanel1, ID_ZONENUM, UnLang->GetAppMiss("MSG_PANVR_SENSOR_CODE"), wxPoint(114, 30), wxDefaultSize, 0, wxT("ZoneNum"));

        ZoneCodeEdit = new wxTextCtrl(WxPanel1, ID_ZONECODEEDIT, wxT("000000"), wxPoint(198, 28), wxSize(94, 20), 0, wxDefaultValidator, wxT("ZoneCodeEdit"));

        wxArrayString arrayStringFor_WxComboBox1;
        arrayStringFor_WxComboBox1.Add(wxT("PIR"));
        arrayStringFor_WxComboBox1.Add(wxT("CN"));
        arrayStringFor_WxComboBox1.Add(wxT("Keyfob"));
        arrayStringFor_WxComboBox1.Add(wxT("Keypad"));
        arrayStringFor_WxComboBox1.Add(wxT("Analog"));
        arrayStringFor_WxComboBox1.Add(wxT("Keypad"));
        arrayStringFor_WxComboBox1.Add(wxT("Smoke/Gas"));

        WxComboBox1 = new wxComboBox(WxPanel1, ID_WXCOMBOBOX1, wxT("PIR"), wxPoint(470, 25), wxSize(105, 23), arrayStringFor_WxComboBox1, 0, wxDefaultValidator, wxT("WxComboBox1"));
        WxComboBox1->Select(0);

        WxStaticText1 = new wxStaticText(WxPanel1, ID_WXSTATICTEXT1, UnLang->GetAppMiss("MSG_PANVR_TSENSOR") , wxPoint(375, 28), wxDefaultSize, 0, wxT("WxStaticText1"));

        BotoAdd = new wxBitmapButton(WxPanel1, ID_BOTOADD, wxBitmap("icons/toolbar/edit_add.png", wxBITMAP_TYPE_PNG), wxPoint(20, 60), wxSize(45, 45), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoAdd"));

        BotoDelete = new wxBitmapButton(WxPanel1, ID_BOTODELETE, wxBitmap("icons/toolbar/edit_remove.png", wxBITMAP_TYPE_PNG), wxPoint(70, 60), wxSize(45, 45), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoDelete"));

        BotoRead = new wxBitmapButton(WxPanel1, ID_BOTOREAD, wxBitmap("icons/toolbar/download.png", wxBITMAP_TYPE_PNG), wxPoint(120, 60), wxSize(45, 45), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoRead"));

        RfGauge = new wxGauge(WxPanel1, ID_RFGAUGE, 200, wxPoint(245, 66), wxSize(140, 19), wxGA_HORIZONTAL, wxDefaultValidator, wxT("RfGauge"));
        RfGauge->SetRange(200);
        RfGauge->SetValue(10);

        BattGauge = new wxGauge(WxPanel1, ID_BATTGAUGE, 256, wxPoint(475, 66), wxSize(140, 19), wxGA_HORIZONTAL, wxDefaultValidator, wxT("BattGauge"));
        BattGauge->SetRange(256);
        BattGauge->SetValue(10);

        TempGauge = new wxGauge(WxPanel1, ID_TEMPGAUGE, 256, wxPoint(475, 91), wxSize(140, 19), wxGA_HORIZONTAL, wxDefaultValidator, wxT("BattGauge"));
        TempGauge->SetRange(256);
        TempGauge->SetValue(10);

        TestEdit = new wxTextCtrl(WxPanel1, ID_TESTEDIT, wxT("1"), wxPoint(245, 91), wxSize(51, 20), wxTE_CENTRE, wxDefaultValidator, wxT("TestEdit"));

        Test = new wxStaticText(WxPanel1, ID_TEST, UnLang->GetAppMiss("MSG_PANVR_LASTTEST"), wxPoint(180, 94), wxDefaultSize, 0, wxT("Test"));

        RfLevel = new wxStaticText(WxPanel1, ID_NIVEL, UnLang->GetAppMiss("MSG_PANVR_RFLEVEL"), wxPoint(180, 68), wxDefaultSize, 0, wxT("RfLevel"));

        Dbm = new wxStaticText(WxPanel1, ID_DBM, wxT("    dbm"), wxPoint(390, 67), wxDefaultSize, 0, wxT("Dbm"));

        minuts = new wxStaticText(WxPanel1, ID_MINUTS, UnLang->GetAppMiss("MSG_PANVR_MINUTS"), wxPoint(320, 93), wxDefaultSize, 0, wxT("minuts"));

        Batttxt = new wxStaticText(WxPanel1, ID_TXTBATT, "Batt", wxPoint(445, 67), wxDefaultSize, 0, wxT("Batt"));
        BatttxtVal = new wxStaticText(WxPanel1, ID_TXTBATTVAL, "V", wxPoint(620, 67), wxDefaultSize, 0, wxT("BattVal"));
        Temptxt = new wxStaticText(WxPanel1, ID_TXTTEMP, "Temp", wxPoint(445, 91), wxDefaultSize, 0, wxT("Temp"));
        TemptxtVal = new wxStaticText(WxPanel1, ID_TXTTEMPVAL, "�C", wxPoint(620, 91), wxDefaultSize, 0, wxT("TempVal"));

}

bool CPanelCfgVR::SetEstat(CEstat *estat)
{
    this->estat = estat;
    GeneraComponents();
    return true;
}

void CPanelCfgVR::UnSetEstat()
{
    estat=0;
    Clear();
}

CEstat *CPanelCfgVR::GetEstat()
{
    return estat;
}

//Aplica el valor del camp de text al CCodi corresponent
bool CPanelCfgVR::AplicaValors()
{
    if (!estat)
        return false;

    //No apliquem valors perque o son editables
    return true;
}

//Actualitza els valors del CEstat al camp de text
//(es el contrari que AplicaValors()
bool CPanelCfgVR::ActualitzaValors()
{
    if (!estat)
        return false;

    //La millor manera d'actualitzar els valors es fent un clear i tornant a generar-ho
    Clear();
    GeneraComponents();

    return true;
}

// -----------------------------------------------------------------------
// jvd code
#ifdef _ALIAS_ESTAT
bool CPanelCfgVR::SetGrups(CGrup *grupZAlias)
{

    for (int i=0;i<64;i++)
    {
        string temp=MSG_PAN_COL_ZONE;
        temp+=" ";
        temp+=iToS(i+1);
        CZalias[i]=temp;
    }
    if (grupZAlias!=0)
    {
        for (int i=0; i<grupZAlias->GetNumCodiFills(); i++) {
            this->codi = grupZAlias->GetCodiFill(i);
            CZalias[i]= codi->GetStringVal();
        }
    }


   return true;
}
#endif

/*
 * BotoAddClick
 */
void CPanelCfgVR::BotoAddClick(wxCommandEvent& event)
{

    string ze=ZoneEdit->GetValue().c_str();
    int z=sToI(ze);
    string cod=ZoneCodeEdit->GetValue().c_str();

    if (cod[0]==CH_KEYPAD){
        WxComboBox1->Select(T_TX_KEYPADPIR);
    }else if (cod[0]==CH_PIR || cod[0]==CH_PIRCAM || cod[0]==CH_PIRCN || cod[0]==CH_PIRCAMCN || cod[0]==CH_ARESCAM){
        WxComboBox1->Select(T_TX_INFRARED);
    }else if (cod[0]==CH_CN || cod[0]==CH_CNCAM || cod[0]==CH_CAME){
        WxComboBox1->Select(T_TX_CONTACT);
    }else if (cod[0]==CH_PTX || cod[0]==CH_EMG){
        WxComboBox1->Select(T_TX_COMMAND);
    }

    unsigned int sens=WxComboBox1->GetSelection();
    unsigned char tsens=(unsigned char)sens;

        int resultbox = wxMessageBox(UnLang->GetAppMiss("MSG_DLG_ASK_VR"), UnLang->GetAppMiss("MSG_DLG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
        if (resultbox == 2) {
            nuvalink->progress=0;
            wxProgressDialog *myPD = new wxProgressDialog(UnLang->GetAppMiss("MSG_DLG_TIT_SEND"), UnLang->GetAppMiss("MSG_DLG_TIT_SEND"),
            13, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
            nuvalink->progressDlg = myPD;
//            nuvalink->transmetent = true;


            if ((z<=MAX_PANELCFGVR_ZONE) && (z>=MIN_PANELCFGVR_ZONE)){
                if (!nuvalink->EnviarConfigVR((z-1),tsens,ZoneCodeEdit->GetValue().c_str())){
                    string err = nuvalink->GetUltimError();
                    wxMessageBox(_wx(err), UnLang->GetAppMiss("MSG_DLG_TIT_ERROR"));
                }
            }else{
                wxMessageBox("Zone don't exist!");
            }
            delete myPD;
            nuvalink->progressDlg=0;
            nuvalink->progress=0;

        }

}

void CPanelCfgVR::BotoDeleteClick(wxCommandEvent& event)
{
    string ze=ZoneEdit->GetValue().c_str();

    int z=sToI(ze);
/*    unsigned int sens=WxComboBox1->GetSelection();
    unsigned char tsens=(unsigned char)sens;*/

//    int ver=sToI(PanelVersion);

//    if (ver>=520){
        int resultbox = wxMessageBox(UnLang->GetAppMiss("MSG_DLG_ASK_VR"), UnLang->GetAppMiss("MSG_DLG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
        if (resultbox == 2) {
            nuvalink->progress=0;
            wxProgressDialog *myPD = new wxProgressDialog(UnLang->GetAppMiss("MSG_DLG_TIT_SEND"), UnLang->GetAppMiss("MSG_DLG_TIT_SEND"),
            13, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
            nuvalink->progressDlg = myPD;
//            nuvalink->transmetent = true;

            if ((z<=MAX_PANELCFGVR_ZONE) && (z>=MIN_PANELCFGVR_ZONE)){
                if (!nuvalink->EnviarConfigVR((z-1),0xFF,ZoneCodeEdit->GetValue().c_str())){
                    string err = nuvalink->GetUltimError();
                    wxMessageBox(_wx(err), MSG_DLG_TIT_ERROR);
                }
            }else{
                wxMessageBox("Zone don't exist!");
            }
            delete myPD;
            nuvalink->progressDlg=0;
            nuvalink->progress=0;
        }

/*    }else{
        wxMessageBox("This model don't supports remote programming");
    }*/
}

void CPanelCfgVR::Read()
{
    TConfigVRdata *frame;

    string ze=ZoneEdit->GetValue().c_str();

    int z=sToI(ze);
    unsigned char tsens;

//    int ver=sToI(PanelVersion);

//    if (ver>=520){

        nuvalink->progress=0;
        wxProgressDialog *myPD = new wxProgressDialog(UnLang->GetAppMiss("MSG_DLG_TIT_RECV"), UnLang->GetAppMiss("MSG_DLG_TIT_RECV"),
        13, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
        nuvalink->progressDlg = myPD;
//        nuvalink->transmetent = true;


        if ((z<=MAX_PANELCFGVR_ZONE) && (z>=MIN_PANELCFGVR_ZONE)){
            unsigned char *p=nuvalink->DemanarConfigVR(z-1);
            if (p!=NULL){
                frame=(TConfigVRdata*)p;

                string cod="";
                cod+=nibble2ascii(frame->code[0]>>4);
                cod+=nibble2ascii(frame->code[0]);

                cod+=nibble2ascii(frame->code[1]>>4);
                cod+=nibble2ascii(frame->code[1]);

                cod+=nibble2ascii(frame->code[2]>>4);
                cod+=nibble2ascii(frame->code[2]);

//                HexChainToAscii(&frame->code[0],&cod[0],6);

                ZoneCodeEdit->SetValue(_wx(cod));
                WxComboBox1->Select(frame->Tsens);

                if (cod[0]==CH_KEYPAD){
                    WxComboBox1->Select(T_TX_KEYPADPIR);
                }else if (cod[0]==CH_PIR || cod[0]==CH_PIRCAM || cod[0]==CH_PIRCN || cod[0]==CH_PIRCAMCN || cod[0]==CH_ARESCAM){
                    WxComboBox1->Select(T_TX_INFRARED);
                }else if (cod[0]==CH_CN || cod[0]==CH_CNCAM || cod[0]==CH_CAME){
                    WxComboBox1->Select(T_TX_CONTACT);
                }else if (cod[0]==CH_PTX || cod[0]==CH_EMG){
                    WxComboBox1->Select(T_TX_COMMAND);
                }

                RfGauge->SetValue(frame->power);
                TestEdit->SetValue(_wx(iToS(frame->lasttest)));

                int db=-127;            // Base power. ( for 24 must be -115 > -127 + 24/2 = -115)
                db+=(frame->power>>1);
                if (frame->power==0xFF){
                    Dbm->SetLabel("Bus 485");
                }else{
                    Dbm->SetLabel(_wx(iToS(db)) +" dBm");
                }

                if (frame->BattLevel>=10){
                    if (frame->Tsens==T_TX_CONTACT){
                        // 10 = 2.2V, 12=2V4 16 = 2.5V , 18=2.6V


                    }else{

                    }

                    BattGauge->SetValue(frame->BattLevel);
                }else{
                    BattGauge->SetValue(0);
                    BatttxtVal->SetLabel("?");
                }


                if (frame->TempLevel>10){


                    TempGauge->SetValue((256-frame->TempLevel));        // NTC!!!
                }else{
                    TempGauge->SetValue(0);
                    TemptxtVal->SetLabel("?");
                }

            }else{
                string err = nuvalink->GetUltimError();
                wxMessageBox(_wx(err), UnLang->GetAppMiss("MSG_DLG_TIT_ERROR"));
            }
        }else{
            wxMessageBox("Zone don't exist!");
        }

        delete myPD;
        nuvalink->progressDlg=0;
        nuvalink->progress=0;
}


void CPanelCfgVR::BotoReadClick(wxCommandEvent& event)
{
    this->Read();
}
