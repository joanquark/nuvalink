#include "wx_pch.h"
#include "LoginDlg.h"
#include "privlevel.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(LoginDlg)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(LoginDlg)
#include <wx/bitmap.h>
#include <wx/font.h>
#include <wx/image.h>
//*)

//(*IdInit(LoginDlg)
const long LoginDlg::ID_STATICTEXT1 = wxNewId();
const long LoginDlg::ID_COMBOBOX1 = wxNewId();
const long LoginDlg::ID_STATICTEXT2 = wxNewId();
const long LoginDlg::ID_TEXTCTRL1 = wxNewId();
const long LoginDlg::ID_STATICTEXT3 = wxNewId();
const long LoginDlg::ID_TEXTCTRL2 = wxNewId();
const long LoginDlg::ID_CHECKBOX1 = wxNewId();
const long LoginDlg::ID_BITMAPBUTTON1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(LoginDlg,wxDialog)
	//(*EventTable(LoginDlg)
	//*)
END_EVENT_TABLE()

LoginDlg::LoginDlg(wxWindow* parent,CLanguage* lang)
{
    Lang=lang;
	//(*Initialize(LoginDlg)
	wxBoxSizer* BoxSizer1;
	wxFlexGridSizer* FlexGridSizer1;
	wxStaticBoxSizer* StaticBoxSizer1;

	Create(parent, wxID_ANY, _("Nuva Login"), wxDefaultPosition, wxDefaultSize, wxSTAY_ON_TOP|wxDEFAULT_DIALOG_STYLE|wxCLOSE_BOX|wxRAISED_BORDER, _T("wxID_ANY"));
	SetClientSize(wxSize(450,250));
	Move(wxPoint(-1,-1));
	SetMinSize(wxSize(-1,-1));
	StaticBoxSizer1 = new wxStaticBoxSizer(wxVERTICAL, this, _("Login"));
	FlexGridSizer1 = new wxFlexGridSizer(2, 3, 10, 5);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("email/user"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	wxFont StaticText1Font(10,wxFONTFAMILY_SWISS,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,false,_T("Consolas"),wxFONTENCODING_DEFAULT);
	StaticText1->SetFont(StaticText1Font);
	FlexGridSizer1->Add(StaticText1, 0, wxALL|wxALIGN_LEFT|wxALIGN_TOP, 5);
	ComboBox1 = new wxComboBox(this, ID_COMBOBOX1, wxEmptyString, wxDefaultPosition, wxSize(180,21), 0, 0, 0, wxDefaultValidator, _T("ID_COMBOBOX1"));
	ComboBox1->SetSelection( ComboBox1->Append(_("instal")) );
	ComboBox1->Append(_("master"));
	wxFont ComboBox1Font(10,wxFONTFAMILY_SWISS,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,false,_T("Consolas"),wxFONTENCODING_DEFAULT);
	ComboBox1->SetFont(ComboBox1Font);
	FlexGridSizer1->Add(ComboBox1, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 6);
	FlexGridSizer1->Add(-1,-1,2, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 6);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Password"), wxDefaultPosition, wxSize(58,20), 0, _T("ID_STATICTEXT2"));
	wxFont StaticText2Font(10,wxFONTFAMILY_SWISS,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_NORMAL,false,_T("Consolas"),wxFONTENCODING_DEFAULT);
	StaticText2->SetFont(StaticText2Font);
	FlexGridSizer1->Add(StaticText2, 0, wxALL|wxEXPAND, 5);
	Pass = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxSize(180,21), wxTE_PROCESS_ENTER|wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	FlexGridSizer1->Add(Pass, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 6);
	FlexGridSizer1->Add(-1,-1,7, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 6);
	StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Server"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	FlexGridSizer1->Add(StaticText3, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	TxtServer = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxSize(180,22), wxTE_PROCESS_ENTER|wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	FlexGridSizer1->Add(TxtServer, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	checkRemeber = new wxCheckBox(this, ID_CHECKBOX1, _("Remember me"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_CHECKBOX1"));
	checkRemeber->SetValue(false);
	FlexGridSizer1->Add(checkRemeber, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer1->Add(FlexGridSizer1, 2, wxALL|wxEXPAND, 15);
	BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	BitmapButton1 = new wxBitmapButton(this, ID_BITMAPBUTTON1, wxBitmap(wxImage(_T(".\\icons\\toolbar\\ok.png"))), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON1"));
	BitmapButton1->SetDefault();
	BitmapButton1->SetFocus();
	BoxSizer1->Add(BitmapButton1, 1, wxALL|wxALIGN_TOP, 5);
	StaticBoxSizer1->Add(BoxSizer1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, wxDLG_UNIT(this,wxSize(10,0)).GetWidth());
	SetSizer(StaticBoxSizer1);
	SetSizer(StaticBoxSizer1);
	Layout();

	Connect(ID_COMBOBOX1,wxEVT_COMMAND_TEXT_ENTER,(wxObjectEventFunction)&LoginDlg::OnBitmapButton1Click);
	Connect(ID_TEXTCTRL1,wxEVT_COMMAND_TEXT_ENTER,(wxObjectEventFunction)&LoginDlg::OnBitmapButton1Click);
	Connect(ID_BITMAPBUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&LoginDlg::OnBitmapButton1Click);
	Connect(wxEVT_KEY_DOWN,(wxObjectEventFunction)&LoginDlg::OnEnter);
	//*)


    StaticText1->SetLabel(Lang->GetAppMiss("MISS_DIALOG_SELECTUSER"));
    StaticText2->SetLabel(Lang->GetAppMiss("MISS_DIALOG_PASS"));

//        StaticText3->SetLabel(Lang->GetAppMiss("MISS_DIALOG_NEWPASS"));
//        StaticText4->SetLabel(Lang->GetAppMiss("MISS_DIALOG_REPEATPASS"));

}

LoginDlg::~LoginDlg()
{
	//(*Destroy(LoginDlg)
	//*)
}

void LoginDlg::SetConfig(CConfig* conf)
{
    this->config=conf;
//    config->privlevel=-1;
    wxString ipcloud=conf->GetParamTcpIp("ipclouddest");
    config->SetSessionServer(ipcloud);
    TxtServer->SetValue(ipcloud);

    if (config->GetParamEng("defuser").IsEmpty()==false)
        ComboBox1->SetValue(config->GetParamEng("defuser"));

    if (config->GetParamEng("defpass").IsEmpty()==false)
        Pass->SetValue(config->GetParamEng("defpass"));

    //TxtServer->SetValue(ipcloud);

}


void LoginDlg::OnBitmapButton2Click(wxCommandEvent& event)
{
    Close();
}

void LoginDlg::OnBitmapButton1Click(wxCommandEvent& event)
{
    this->Log();

}


void LoginDlg::Log()
{

    if (TxtServer->GetValue().IsEmpty()==false){
        config->SetSessionServer(TxtServer->GetValue());
    }

    int lev=config->Login(ComboBox1->GetValue(),Pass->GetValue().c_str());

    if (lev>=0){
        if (checkRemeber->IsChecked()){
            config->SetParamTcpIp("ipclouddest",TxtServer->GetValue());
            config->SetRememberPass(Pass->GetValue());
            config->SetRememberUser(ComboBox1->GetValue());
            config->Save();
        }
/*        if (NewPass1->GetValue()!=""){
            if (NewPass1->GetValue()==NewPass2->GetValue()){
                if (ComboBox1->GetValue()=="instal"){
                    config->eng["instal"]=NewPass1->GetValue();
                    wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_PASSCHANGED"));
                }else if (ComboBox1->GetValue()=="master"){
                    if (lev>=MASTER_PRIVLEVEL){
                        config->eng["master"]=NewPass1->GetValue();
                        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_PASSCHANGED"));
                    }
                }else if (ComboBox1->GetValue()=="admin"){
                    if (lev>=ADMIN_PRIVLEVEL){
                        config->eng["admin"]=NewPass1->GetValue();
                        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_PASSCHANGED"));
                    }
                }else if (ComboBox1->GetValue()=="engineer"){
                    if (lev>= ENG_PRIVLEVEL){
                        config->eng["code"]=NewPass1->GetValue();
                        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_PASSCHANGED"));
                    }
                }
            }else{
                wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_PASS_DIFFERENT"));
            }
        }*/
        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_LOGGED")+ComboBox1->GetValue());
    }else{
        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_CODEERROR"));
    }
    Close();

}

void LoginDlg::OnButton1(wxKeyEvent& event)
{
}

void LoginDlg::OnEnter(wxKeyEvent& event)
{
    if (event.GetKeyCode() == WXK_RETURN)
        this->Log();

}

