#include "instalacio.h"

#define _wx(x) (wxString)((string)x).c_str()

CInstalation::CInstalation()
{
    totOk=true;
    canvis=false;
    fileName="";
    //Tots els altres components son objectes i ja saben inicializar-se sols
}

void CInstalation::Clear()
{
    tmpModel=NULL;
    tmpCodi=NULL;
    tmpEvent=NULL;
    central.Clear();
    dades.Clear();
    dades.Lang=Lang;
    totOk=true;
    lastNode="";
    msgError="";
    canvis=false;
    fileName="";
}

CInstalation::~CInstalation()
{
    Clear();
}

string& CInstalation::GetLastError()
{
    return msgError;
}

bool CInstalation::GettotOk()
{
    return totOk;
}

bool CInstalation::ParseJSON(wxJSONValue root,wxString versio)
{
    central.Lang=this->Lang;
    tmpModel = &central;

    int devmodel = root["tdevice"].AsInt();
    CDevice cdev;
    wxString Model=cdev.GetPanelDescr(devmodel);
    //wxString Versio = root["versio"].AsString();
    //wxString Versio = "200";
    wxString strid="0";
    tmpModel->SetModel(Model);
    tmpModel->SetId(strid);
    if ( versio == "") {
        wxJSONValue status=root["status"];
        wxJSONValue value=status["techSt0"];
        if (value.HasMember("fwvh") && value.HasMember(("fwvl"))){
            int fwv=value["fwvh"].AsInt();
            wxString sfh=_wx(iToS(fwv));
            fwv=value["fwvl"].AsInt();
            wxString psfl=_wx(iToH(fwv));
            wxString sfl;
            if (psfl.size()>1){
                sfl=psfl;
            }else{
                sfl="0"+psfl;
            }
            versio=sfh+sfl;
        }else{
            msgError = Lang->GetAppMiss("MISS_ERROR_INST_VERSION");
            return false;
        }
    }
    string isversio=versio.c_str();
    int iversio=sToI(isversio);
    if (iversio<206 || iversio>210){
        versio="210";
    }
    tmpModel->SetVersion(versio);

    if (!tmpModel->Genera()) {
        //oops, no s'ha pogut generar el model
        //potser no existeix el fitxer que el defineix
        //o la versio i/o el model son incorrectes
        totOk = false;
        msgError = Lang->GetAppMiss("MISS_ERROR_INST_CREATEMODEL") + Model + "-" + versio/*Versio*/;
        return false;
    }
    // ------------------------------------- parsing codis --------------
    wxJSONValue val=root["codis"];
    int k=val.Size();
    for (int n=0; n< tmpModel->GetNumCodis(); n++) {
        CCodi *codi = tmpModel->GetCodi(n);
        wxString id=codi->GetId();
        string cv=val[id].AsString().c_str();
        codi->SetStringVal(cv);
    }
    // ------------------------------------ parsing State ----------------
    //val=root["State"];        -- no need!!!

    // ------------------------------------ parsing dades ----------------
    val=root["dades"];
    string strval=val["phone"].AsString().c_str();
    dades.setphone(strval);
    strval=val["cusname"].AsString().c_str();
    dades.setcusname(strval);
    strval=val["name"].AsString().c_str();
    dades.setname(strval);
    strval=val["type"].AsString().c_str();
    dades.settype(strval);
    strval=val["address"].AsString().c_str();
    dades.setaddress(strval);
    strval=val["email"].AsString().c_str();
    dades.setemail(strval);
    strval=val["phonePstn"].AsString().c_str();
    dades.setphonePstn(strval);
    strval=val["phoneCsd"].AsString().c_str();
    dades.setphoneCsd(strval);
    strval=val["conType"].AsString().c_str();
    dades.setconType(sToI(strval));
    strval=val["host"].AsString().c_str();
    dades.sethost(strval);
    strval=val["port"].AsString().c_str();
    dades.setport(sToI(strval));
    strval=val["extPower"].AsString().c_str();
    dades.setextPower(strval);
    strval=val["tBatt"].AsString().c_str();
    dades.settBatt(sToI(strval));
    strval=val["simSolution"].AsString().c_str();
    dades.setSimSolution(strval);
    return true;
}

//El XMLParser crida aquesta funci� quan troba un node en un fitxer XML
//Un node es un tag XML que cont� m�s tags XML dins seu
//<node atribut1="valor1">
//  <element>valor</element>
//</node>
void CInstalation::foundNode ( wxString & name, wxXmlNode* attributes )
{

    wxString id;

    attributes->GetPropVal("id",&id);

    if (totOk) {
        if (name == "component") {

            if (id=="0") {
                central.Lang=this->Lang;
                tmpModel = &central;
            }

            tmpModel->SetId(id);

            wxString Model;
            wxString Versio;
            attributes->GetPropVal("model",&Model);
            attributes->GetPropVal("versio",&Versio);
            if (Model == "") {
                totOk = false;
                msgError = Lang->GetAppMiss("MISS_ERROR_INST_MODEL");
            } else {
                tmpModel->SetModel(Model);
            }
            if (Versio == "") {
                totOk = false;
                msgError = Lang->GetAppMiss("MISS_ERROR_INST_VERSION");
            } else {
                tmpModel->SetVersion(Versio);
            }

            if (!tmpModel->Genera()) {
                //oops, no s'ha pogut generar el model
                //potser no existeix el fitxer que el defineix
                //o la versio i/o el model son incorrectes
                totOk = false;
                msgError = Lang->GetAppMiss("MISS_ERROR_INST_CREATEMODEL") + Model + "-" + Versio;
            }
        }
        else if (name=="codi") {
            tmpCodi = tmpModel->GetCodiById(id);

            //Si el codi no existeix en aquets model
            //ignorarem tot lo referent a aquest en el fitxer
            if (tmpCodi == NULL) {
                name = "IGNORAR";
            }
        }
        else if (name=="grup-flash") {
            tmpGrupFlash = tmpModel->GetGroupFlashById(id);
            tmpElement = -1;
            tmpCol = 0;
            if (tmpGrupFlash == NULL) {
                name = "IGNORAR";
            }
        }
        else if (name=="element")
        {
            tmpElement++;
            tmpCol=0;
        }
        else if (name=="fitxer") {
            if (tmpGrupFlash){
                tmpFitxer = tmpGrupFlash->GetFileById(id);
                if (tmpFitxer==0)
                    name = "IGNORAR";
            }
        }
#ifdef ESTAT_GRAFIC
        else if (name=="compgraf") {
            tmpCompGraf = new CComponentGrafic();
            CStatus *estat = tmpModel->GetStatus();
            CStatusGrafic *g = estat->GetStatusGrafic();
            g->AddComponent(tmpCompGraf);
        }
#endif
        else if (name=="event") {
            tmpEvent = new CEvent(Lang);
            CEventList *llistaEvents = tmpModel->GetEventList();
            llistaEvents->AddEvent(tmpEvent);
        }

        lastNode = name;
        canvis=true;
    }
}


void CInstalation::foundElement ( wxString & name, wxString & value, wxXmlNode* attributes )
{

    string strval=(string)value.c_str();

    if (totOk) {
        if (lastNode=="codi") {
            if (name=="val") {

                tmpCodi->SetStringVal(strval);
                tmpCodi->SetStringIniVal(strval);
            }
        }
        else if (lastNode=="dades") {
            if (name=="telefon") {
                dades.setphone(strval);
            }
            else if (name=="nomclient") {
                dades.setcusname(strval);
            }
            else if (name=="nominstal") {
                dades.setname(strval);
            }
            else if (name=="tipusinstal") {
                dades.settype(strval);
            }
            else if (name=="direccio") {
                dades.setaddress(strval);
            }
            else if (name=="email") {
                dades.setemail(strval);
            }
            else if (name=="rule") {
                dades.setrule(strval);
            }
            else if (name=="telfconexio") {
                dades.setphonePstn(strval);
            }
            else if (name=="telfconexioCSD") {
                dades.setphoneCsd(strval);
            }
            else if (name=="ConnectionType") {
                dades.setconType(sToI(strval));
            }
            else if (name=="ipdest") {
                dades.sethost(strval);
            }
            else if (name=="ipport") {
                dades.setport(sToI(strval));
            }else if (name=="extpower"){
                dades.setextPower(strval);
            }else if (name=="tbatt"){
                dades.settBatt(sToI(strval));
            }
            else if (name=="simSolution") {
                dades.setSimSolution(strval);
            }
        }
        else if (lastNode=="component") {
            if (name=="descr") {
                tmpModel->SetDescr(value);
            }
        }
        else if (lastNode=="element") {
            if (name=="col") {
                if (tmpGrupFlash) {
                    tmpGrupFlash->SetElementCol(tmpElement, tmpCol, strval);
                    tmpCol++;
                }
            }
        }
        else if (lastNode=="fitxer") {
            if (name=="nom") {
                if (tmpFitxer) {
                    tmpFitxer->SetNomFitxer(strval);
                }
            }
        }
        else if (lastNode=="estat") {
            if (name=="detectz") {
                CStatus *estat = tmpModel->GetStatus();
                for (int i=0; i<strval.length(); i++) {
                    if (strval[i] == '1') estat->SetDetectZ(i);
                }
            }
            else if (name=="alarmz") {
                CStatus *estat = tmpModel->GetStatus();
                for (int i=0; i<strval.length(); i++) {
                    if (strval[i] == '1') estat->SetAlarmZ(i);
                }
            }
            else if (name=="omitz") {
                CStatus *estat = tmpModel->GetStatus();
                for (int i=0; i<strval.length(); i++) {
                    if (strval[i] == '1') estat->SetOmitZ(i);
                }
            }
            else if (name=="averiaz") {
                CStatus *estat = tmpModel->GetStatus();
                for (int i=0; i<strval.length(); i++) {
                    if (strval[i] == '1') estat->SetAveriaZ(i);
                }
            }
            else if (name=="areast") {
                CStatus *estat = tmpModel->GetStatus();
                char buf=0x00, c=0x01;
                for (int i=0; i<8; i++) {
                    if (strval[i] == '1') buf |= c;
                    c <<= 1;
                }
                estat->SetAreaSt(buf);
            }
            else if (name=="stpanel") {
                CStatus *estat = tmpModel->GetStatus();
                char buf=0x00, c=0x01;
                for (int i=0; i<8; i++) {
                    if (strval[i] == '1') buf |= c;
                    c <<= 1;
                }
                estat->SetStPanel(buf);
            }
            else if (name=="ppe") {
                CStatus *estat = tmpModel->GetStatus();
                int ppe;
                std::stringstream ss(strval);
                ss >> ppe;
                estat->SetPunterEvents(ppe);
            }
        }
#ifdef ESTAT_GRAFIC
        else if (lastNode=="grafic") {
            if (name=="mapa") {
                CStatus *estat = tmpModel->GetStatus();
                CStatusGrafic *g = estat->GetStatusGrafic();
                wxString snum=attributes->GetPropVal(wxT("num"),wxT("default-value"));
                long int num;
                snum.ToLong(&num);
                //int num = sToI(getAtrVal(stratt, "num"));
                g->SetFitxerMapa(strval, num);
            }
        }
        else if (lastNode=="compgraf") {
            if (name=="nummapa") {
                tmpCompGraf->SetMapa(sToI(strval));
            }
            else if (name=="posx") {
                tmpCompGraf->SetPosX(sToI(strval));
            }
            else if (name=="posy") {
                tmpCompGraf->SetPosY(sToI(strval));
            }
            else if (name=="zona") {
                tmpCompGraf->SetZona(sToI(strval));
            }
            else if (name=="area") {
                tmpCompGraf->SetArea(sToI(strval));
            }
            else if (name=="sortida") {
                tmpCompGraf->SetSortida(sToI(strval));
            }
            else if (name=="rele") {
                tmpCompGraf->SetRele(sToI(strval));
            }
            else if (name=="type") {
                tmpCompGraf->SetType(sToI(strval));
            }
        }
#endif
        else if (lastNode=="events") {
            if (name=="myppe") {
                CEventList *llista = tmpModel->GetEventList();
                int myppe;
                std::stringstream ss(strval);
                ss >> myppe;
                llista->SetPunter(myppe);
            }
        }
        else if (lastNode=="event") {
            if (name=="h") {
                tmpEvent->SetHora(strval);
            }else if (name=="dt") {
                tmpEvent->SetData(strval);
            }else if (name=="cid") {
                tmpEvent->SetContactID(strval);
            }else if (name=="z") {
                int zona;
                std::stringstream ss(strval);
                ss >> zona;
                tmpEvent->SetZona(zona);
            }else if (name=="a") {
                tmpEvent->SetArea(sToI(strval));
            }else if (name=="ab") {
                tmpEvent->SetAbonat(strval);
            }else if (name=="bn"){
                tmpEvent->SetBinNum(sToI(strval));
            }else if (name=="bf"){
                tmpEvent->SetBinFile(strval);
/*            }else if (name=="t1"){
                tmpEvent->SetTemp1(strval);
            }else if (name=="t2"){
                tmpEvent->SetTemp2(strval);
            }else if (name=="h1"){
                tmpEvent->SetHumidity(strval);*/
            }else if (name=="loc"){             // keeps location As Temp1;Temp2;Higro
                tmpEvent->SetFix(strval);
            }
        }
        canvis=true;
    }
}


bool CInstalation::SetControlPanel(wxString id, wxString model, wxString versio)
{
    central.Lang=this->Lang;

    if (!central.SetId(id)) return false;
    if (!central.SetModel(model)) return false;
    if (!central.SetVersion(versio)) return false;

    canvis=true;
    return central.Genera();
}


void CInstalation::ClearControlPanel()
{
    central.Clear();
}

bool CInstalation::SetDades(CData &dades)
{
    canvis=true;
    this->dades = dades;
}

void CInstalation::ClearDades()
{
    canvis=true;
    dades.Clear();
}

CData *CInstalation::GetDataObj()
{
    return &dades;
}

CModel *CInstalation::GetControlPanel()
{
    return &central;
}


bool CInstalation::SetFileName(string &fileName)
{
    this->fileName = fileName;
    return TRUE;
}

string& CInstalation::GetFileName()
{
    return fileName;
}

bool CInstalation::GetChanges()
{
    if (central.GetChanges())
        canvis=true;

    return canvis;
}

void CInstalation::ClrChanges()
{
    canvis=false;
    central.ClrChanges();
}

CCodi* CInstalation::GetCodiById(wxString id)
{
    return central.GetCodiById(id);
}

//Funci� que retorna el codi d'instalador de la central principal de la
//instalacio. Si no el troba (xungo) torna el codi per defecte 0011
string& CInstalation::GetCodiInstalador()
{
    string id="400";
    CCodi *codiInst = central.GetCodiById(id);
    if (codiInst)
        return codiInst->GetStringVal();
    else {
        codiDefecte = "0011";
        return codiDefecte;
    }
}

//Funci� que retorna el codi d'abonat principal de la central principal de la
//instalacio. Si no el troba (xungo)???
string& CInstalation::GetMainCodiAbonat()
{
    string id="426";
    CCodi *codiAb = central.GetCodiById(id);
    if (codiAb)
        return codiAb->GetStringVal();
    else {
        codiDefecte = "FFFF";       //  Wrong Subscriber!
        return codiDefecte;
    }
}

string& CInstalation::GetCodiVal(string add)
{
    CCodi *codi = central.GetCodiById(add);
    if (codi)
        return codi->GetStringVal();
    else {
        codiDefecte = "FFFF";       //  Wrong Subscriber!
        return codiDefecte;
    }
}


string CInstalation::GetPhNum()
{
    return dades.getphonePstn();
}

string CInstalation::GetPhNumCSD()
{
    return dades.getphoneCsd();
}


int CInstalation::GetConnectionType() {
    return dades.getconType();
}

