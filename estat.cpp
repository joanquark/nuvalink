#include "estat.h"

#define _wx(x) (wxString)((string)x).c_str()


CStatus::CStatus(unsigned char TPanel)
{
    Clear();
    this->busadd=0x12;
    isPSTN=false;
    isETH=false;
//    this->mode=MODE_ALL;

    switch (TPanel){

        case TPANEL_STRATUS3T:
        case TPANEL_NIMBUS3T:
            this->numAreas=8;
            this->numZones=64;
            this->numOuts=8;
            break;
        case TPANEL_CIRRUS4T:
            this->numAreas=32;
            this->numZones=256;
            this->numOuts=32;
            break;
        default:
            this->numAreas=1;
            this->numZones=2;
            this->numOuts=2;
    }
}

void CStatus::Clear()
{

    punterEvents=0;

    vdd=3.3;
    numZones=16;
    numAreas=4;

    punterEvents=0;
    dateTime.hours=0;
    dateTime.minuts=0;
    dateTime.seconds=0;
    dateTime.day=0;
    dateTime.month=0;
    dateTime.year=0;

    hasGsmRssi=false;
    hasGeneral=true;
    hasOuts=true;
    hasAnalog=true;
    hasWireless=true;
    hasWirelessUser=true;
    hasWirelessOut=true;
    hasReportTest=true;

#ifdef ESTAT_GRAFIC
    teEstatGrafic=true;
    estatGrafic.Clear();
#endif
#ifdef _PICTURES
    hasPics=false;
#endif
}

CStatus::~CStatus()
{
    Clear();
}

bool CStatus::UpdateFromRawMem(unsigned char* data,int id)
{

    string add=GetStEeprom(id);
    int addint=hToI(add);
    int len=GetStMemLen(id);
    switch(id){
        case ID_ESTAT_INPUT:
            memcpy(&InSt[0].Status1,&data[addint],len);
            break;
        case ID_ESTAT_OUTPUT:
            memcpy(&OSt[0].Status,&data[addint],len);
            break;
        case ID_ESTAT_AREAS:
            memcpy(&ASt[0].Status,&data[addint],len);
            break;
        case ID_ESTAT_SYS:
            memcpy(&STSYS.SysInd.PowerFail,&data[addint],len);
            break;

    }
}


WORD CStatus::GetAreaSt(int area)
{
    if (area<numAreas)
        return ASt[area].Status;
    else
        return 0;
}

WORD CStatus::GetOutSt(int out)
{
    if (out<numSortides)
        return OSt[out].TimerT;
    else
        return 0;
}

unsigned char CStatus::GetStPanel()
{
    return STSYS.SysSt.StPanel.Val;
}

bool CStatus::SetNumZ(int numZones)
{
    this->numZones = numZones;
    return true;
}

bool CStatus::SetNumUsers(int numUsers)
{
    this->numUsers = numUsers;
    return true;
}

int CStatus::GetNumZones()
{
    return numZones;
}

int CStatus::GetNumUsers()
{
    return numUsers;
}

bool CStatus::SetStatus()
{

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetStEeprom(int sec,string &val)
{
    this->StEeprom[sec]=val;
}
/*bool CStatus::SetZEeprom(string &val)
{
    this->ZEeprom=val;
}
bool CStatus::SetOEeprom(string &val)
{
    this->OEeprom=val;
}
bool CStatus::SetAEeprom(string &val)
{
    this->AEeprom=val;
}
bool CStatus::SetSysEeprom(string &val)
{
    this->SysEeprom=val;
}*/
bool CStatus::SetStMemLen(int sec,int len)
{
    this->StMemLen[sec]=len;
}
/*bool CStatus::SetAMemLen(int len)
{
    this->AMemLen=len;
}
bool CStatus::SetOMemLen(int len)
{
    this->OMemLen=len;
}
bool CStatus::SetZMemLen(int len)
{
    this->ZMemLen=len;
}
bool CStatus::SetSysMemLen(int len)
{
    this->SysMemLen=len;
}*/
string CStatus::GetStEeprom(int sec)
{
    return this->StEeprom[sec];
}
/*string CStatus::GetSysEeprom()
{
    return this->SysEeprom;
}
string CStatus::GetZEeprom()
{
    return this->ZEeprom;
}
string CStatus::GetOEeprom()
{
    return this->OEeprom;
}
string CStatus::GetAEeprom()
{
    return this->AEeprom;
}*/
int CStatus::GetStMemLen(int sec)
{
    return this->StMemLen[sec];
}
/*int CStatus::GetSysMemLen()
{
    return this->SysMemLen;
}
int CStatus::GetAMemLen()
{
    return this->AMemLen;
}
int CStatus::GetOMemLen()
{
    return this->OMemLen;
}
int CStatus::GetZMemLen()
{
    return this->ZMemLen;
}*/

bool CStatus::SetDev(string curmode,string curver,string curverdate,string curhard){
    this->curmodel=curmode;
    this->curver=curver;
    this->curverdate=curverdate;
    this->curhard=curhard;

}

bool CStatus::IsDetecting(int zona)
{
    if ((zona < 0) || (zona >= numZones))
        return false;

    if (InSt[zona].Status1&M_Z_DETECT)
        return true;

    return false;

}

bool CStatus::IsAlarm(int zona)
{
    if ((zona < 0) || (zona >= numZones))
        return false;

    if (InSt[zona].Status1&M_Z_ALARM)
        return true;

    return false;

}

bool CStatus::IsOmiting(int zona)
{
    if ((zona < 0) || (zona >= numZones)){
        return false;
    }

    if (InSt[zona].Status1&M_Z_BYPASS)
        return true;
    return false;

}

bool CStatus::IsAvery(int zona)
{
    if ((zona < 0) || (zona >= numZones))
        return false;

    if (InSt[zona].Status1&M_Z_AVERY)
        return true;

    return false;

}

bool CStatus::SetDateTime(unsigned char *data)
{
    STSYS.SysSt.dt.hours = data[3];
    STSYS.SysSt.dt.minuts = data[4];
    STSYS.SysSt.dt.seconds = data[5];
    STSYS.SysSt.dt.day = data[6];
    STSYS.SysSt.dt.month = data[7];
    STSYS.SysSt.dt.year = data[8];
    return true;
}

unsigned char *CStatus::GetDateTime()
{
    return &dateTime.hours;
}

string &CStatus::GetTime()
{
    std::string *time = new std::string;
    int hores = (int)STSYS.SysSt.dt.hours;
    int minuts = (int)STSYS.SysSt.dt.minuts;
    int segons = (int)STSYS.SysSt.dt.seconds;
    char buffer[16];
    sprintf(buffer, "%02d:%02d:%02d", hores, minuts, segons);
    *time = buffer;
    return *time;
}

string &CStatus::GetDate()
{
    std::string *time = new std::string;
    int dia = (int)STSYS.SysSt.dt.day;
    int mes = (int)STSYS.SysSt.dt.month;
    int any = (int)STSYS.SysSt.dt.year;
    char buffer[16];
    sprintf(buffer, "%02d/%02d/%02d", dia, mes, any);
    *time = buffer;
    return *time;
}

bool CStatus::SetVDD(float vdd)
{
    if (vdd < 0.0) return false;

    this->vdd = vdd;
    return true;
}

int CStatus::GetNumSortides()
{
    return numSortides;
}

bool CStatus:: SetNumOuts(int num)
{
    numSortides=num;
}


bool CStatus::SetPunterEvents(int punter)
{
    punterEvents=punter;
    return true;
}

int CStatus::GetPunterEvents()
{
    return punterEvents;
}

bool CStatus::SetAlarmZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status1|=M_Z_ALARM;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetDetectZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status1|=M_Z_DETECT;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetAveriaZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status1|=M_Z_AVERY;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetOmitZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status1|=M_Z_BYPASS;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetAreaSt(WORD AreaState)
{
    STSYS.SysSt.StAreas.Val=AreaState;
    return true;
}

bool CStatus::SetStPanel(unsigned char StPanel)
{
    STSYS.SysSt.StPanel.Val=StPanel;
    return true;
}

bool CStatus::SetNumAreas(int arees)
{
    if (arees >= 0) {
        numAreas = arees;
        return true;
    }
    return false;
}

int CStatus::GetNumAreas()
{
    return numAreas;
}

bool CStatus::SetHasGeneral(bool general)
{
    hasGeneral = general;
    return true;
}

bool CStatus::HasGeneral()
{
    return hasGeneral;
}

bool CStatus::SetHasWireless(bool Radio)
{
    hasWireless = Radio;
    return true;
}

bool CStatus::HasWireless()
{
    return hasWireless;
}

bool CStatus::SetHasWirelessUser(bool Radio)
{
    hasWirelessUser = Radio;
    return true;
}

bool CStatus::HasWirelessUser()
{
    return hasWirelessUser;
}


bool CStatus::SetHasWirelessOut(bool Radio)
{
    hasWirelessOut = Radio;
    return true;
}

bool CStatus::HasWirelessOut()
{
    return hasWirelessOut;
}


bool CStatus::SetHasReportTest( bool test)
{
    hasReportTest = test;
    return true;
}

bool CStatus::HasReportTest()
{
    return hasReportTest;
}

#ifdef _PICTURES
bool CStatus::HasPictures(){
    return hasPics;
}

bool CStatus::SetHasPictures(bool pictures)
{
    hasPics=pictures;
    return true;
}
#endif

#ifdef ESTAT_GRAFIC
bool CStatus::TeEstatGrafic()
{
    return teEstatGrafic;
}

bool CStatus::SetTeEstatGrafic(bool estatGrafic)
{
    teEstatGrafic = estatGrafic;
    return true;
}

CStatusGrafic* CStatus::GetStatusGrafic()
{
    return &estatGrafic;
}

void CStatus::ActualitzaCompGrafics()
{
    unsigned char c = 0x01;

    for (int i=0; i<estatGrafic.GetNumComponents(); i++) {
        CComponentGrafic *comp = estatGrafic.GetComponent(i);

        //Es sensor de zona
        if (comp->GetZona() != -1) {
            int zona = comp->GetZona();
            int b;
            if (zona < 8) {
                b=0;
                c = 0x01 << zona;
            }
            else {
                b=1;
                c = 0x01 << (zona-8);
            }

/*            if (panelState.DetectZ[b] & c)
                comp->SetDetecta(true);
            else
                comp->SetDetecta(false);

            if (panelState.AlarmZ[b] & c)
                comp->SetAlarma(true);
            else
                comp->SetAlarma(false);

            if (panelState.OmitZ[b] & c)
                comp->SetOmit(true);
            else
                comp->SetOmit(false);

            if (panelState.AveriaZ[b] & c)
                comp->SetAveria(true);
            else
                comp->SetAveria(false);
        }
        //Sortida
        else if (comp->GetSortida() != -1) {
            int stda = comp->GetSortida();
            if (stda < sortides.size()) {
                if (sortides[stda]->GetValor() != 0.0)
                    comp->SetActiva(true);
                else
                    comp->SetActiva(false);
            }
        */
        }
    }
}


#endif

