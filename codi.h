#ifndef CODI_H
#define CODI_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
using namespace std;

struct ToLower
   {
     char operator() (char c) const  { return std::tolower(c); }
   };

   struct ToUpper
   {
     char operator() (char c) const  { return std::toupper(c); }
   };

#include "util.h"
#include "TypeAdd.h"
#include "msgs.h"

#define M_HADDRESS_EEP      0x1F

#define TIPUS_ADR_FLASH     1
#define TIPUS_ADR_EEPROM    2

enum{
    TDATA_BITMASK      =0,
    TDATA_UINT8        =1,
    TDATA_INT8         =2,
    TDATA_UINT16       =3,
    TDATA_INT16        =4,
    TDATA_NIBBLE2      =5,  //1 byte en format BSD
    TDATA_NIBBLE4      =6,  //2 bytes en format BSD
    TDATA_NIBBLE6      =7,  //3 bytes en format BSD
    TDATA_NIBBLE8      =8,  //4 bytes en format BSD
    TDATA_NIBBLE14     =9,  //7 bytes hexa
    TDATA_NIBBLE16     =10,  // 16 nibles en format Hexa
    TDATA_NIBBLE28     =11,  // 16 nibles en format Hexa
    TDATA_NIBBLE32     =12,  // 16 nibles en format Hexa
    TDATA_HOURQUARTER  =13, //1 byte
    TDATA_OUTPUT       =14, //1 byte, valor=0 -> Disabled, valor!=0 -> Enabled (no editable)
    TDATA_IPV4         =15,
    TDATA_STRING       =16,  //16 caracters en ascii
    TDATA_STRING24     =17,  // only for GPRS aliases.
    TDATA_STRING32     =18,  // only for GPRS aliases.
    TDATA_STRING64     =19,
    TDATA_FLOAT16      =20,
    TDATA_ALIM050      =21,
    TDATA_ALIM018      =22,
    TDATA_ALIM100      =23,
    TDATA_CH32         =24,
    TDATA_ALIMMV       =25,
    TDATA_MBAR         =26,
    TDATA_FLOAT        =27,
    TDATA_INT32        =28,
};


#define BYTELEN_BITMASK       1
#define BYTELEN_UINT8         1
#define BYTELEN_INT8          1
#define BYTELEN_UINT16        2
#define BYTELEN_INT16         2
#define BYTELEN_NIBBLE2       1
#define BYTELEN_NIBBLE4       2
#define BYTELEN_NIBBLE6       3
#define BYTELEN_NIBBLE8       4
#define BYTELEN_NIBBLE14      7
#define BYTELEN_NIBBLE16      8
#define BYTELEN_NIBBLE28      14
#define BYTELEN_NIBBLE32      16
#define BYTELEN_HOURQUARTER   1
#define BYTELEN_OUTPUT        1
#define BYTELEN_STRING        16
#define BYTELEN_STRING64      62          // for
#define BYTELEN_STRING32      32          // for
#define BYTELEN_STRING24      24          // for
#define BYTELEN_NIBBLE16      8
#define BYTELEN_IPV4          4
#define BYTELEN_TEMP16        2
#define BYTELEN_ALIM050       2
#define BYTELEN_ALIM018       2
#define BYTELEN_ALIM100       2
#define BYTELEN_CH32          4
#define BYTELEN_ALIMMV        2
#define BYTELEN_MBAR          2
#define BYTELEN_FLOAT         4
#define BYTELEN_INT32         4

#define MAX_LON_BYTES_CODI      BYTELEN_STRING64

#define STRLEN_BITMASK      8
#define STRLEN_UINT8        3
#define STRLEN_INT8         4
#define STRLEN_UINT16       5
#define STRLEN_INT16        5
#define STRLEN_MBAR         5
#define STRLEN_NIBBLE2      2
#define STRLEN_NIBBLE4      4
#define STRLEN_NIBBLE6      6
#define STRLEN_NIBBLE8      8
#define STRLEN_NIBBLE14     14
#define STRLEN_NIBBLE16     16
#define STRLEN_NIBBLE28     28
#define STRLEN_NIBBLE32     32
#define STRLEN_HOURQUARTER  5
#define STRLEN_OUTPUT       11 //strlen("Desactivada")
#define STRLEN_NIBBLE2_1N   2
#define STRLEN_NIBBLE16     16
#define STRLEN_STRING       16
#define STRLEN_STRING24     24
#define STRLEN_STRING32     32
#define STRLEN_STRING64     62
#define STRLEN_IPV4         15          // 3+1+3+1+3+1+3
#define STRLEN_TEMP         5       // -99.9
#define STRLEN_ALIM018      4       // 12.4
#define STRLEN_ALIMMV       4       // 12.4
#define STRLEN_ALIM050      4       // 12.4
#define STRLEN_ALIM100      4       // 12.4
#define STRLEN_CH32         16
#define STRLEN_FLOAT        16
#define STRLEN_INT32        8

//#define MAX_LON_STRING_CODI STRLEN_NIBBLE28
#define MAX_LON_STRING_CODI     STRLEN_STRING64


class CCodi
{
    int tipusDir;
    int tipusData;
    int32_t intVal;
    string address, kpAddress, val, id, defVal, iniVal;
    string wizzh,wizzl;
    wxString descr;
    char relleno;
    bool omplirDreta;

    vector<string *> bitdescr;
    bool tipusDataValid, defAssignat, canvis, editable;
    int valLength, offset;
    int vMin,vMax,verify;          // minimun and maximun allowable values.
    string vMatch;
    int upl;
    int wizz;
    string cat;

    bool DontSend;
    unsigned char buffvalue[MAX_LON_BYTES_CODI];

    public:
        CCodi();
        ~CCodi();
        void Clear();

        bool editchange;
        bool SetAddress(string &address);
        bool SetKpAddress(string &kpa);
        bool SetId(string &id);
        bool SetTypeAdd(int tipus);
        bool SetTypeData(int tipus);
        bool SetDescr(wxString& descr);
        bool SetBitDescr(int bit, wxString& descr);
        bool SetStringVal(string& val);
        bool SetStringIniVal(string &inival);
        int  GetUpl();
        string& GetCat();
        bool SetCat(string& cat);
        bool SetVal(unsigned char *val);
        bool VerifyVal(unsigned char *val);
        bool SetOmplir(char caract, bool dreta);
        bool SetDefVal(unsigned char *val);
        bool SetDefVal(string& val);
        bool SetOffset(int offset);
        bool SetEditable(bool editable);
        bool SetUpl(int upl);
        bool SetMin(int val);
        int GetMin();
        string& GetMatch();
        bool SetMatch(string& m);
        bool SetMax(int val);
        int GetMax();
        bool SetVerify(int val);
        int GetVerify();
        bool SetWizz(int val);
        bool SetWizzh(string val);
        bool SetWizzl(string val);
        int GetWizz();
        string GetWizzl();
        string GetWizzh();
        bool SetNotSend();
        bool IsNotSend();
        bool Reset();

        string& GetAddress();
        string& GetKpAddress();
        int GetTypeConDir();
        int GetTypeConData();
        string& GetId();
        wxString& GetDescr();
        string& GetBitDescr(int bit);
        string& GetStringVal();
        string& GetStringIniVal();
        string& GetDefVal();
        unsigned char *GetVal();
        int32_t GetIntVal();
        int  GetValLength();
        int  GetValStringLength();
        bool IsEditable();
        int GetOffset();

        bool GetChanges();
        void ClrChanges();

        //CCodi CCodi::operator= (CCodi param);
        CCodi operator= (CCodi param);
        float boardpowerfactor;
    private:
        void AssignLength();
};

#endif
