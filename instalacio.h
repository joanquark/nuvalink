#ifndef INSTALACIO_H
#define INSTALACIO_H

#include "jsonval.h"
#include "jsonwriter.h"
#include "jsonreader.h"
/* ****************************************************************************
 * Classe CInstalation
 *
 * Classe que cont� tota la informaci� i objectes referents a una instalaci�.
 * + T� un objecte CData amb les dades del client, nom de la instalacio, telefons, etc..
 * + T� un CModel com a central principal de la instalaci�. �s important que
 *   l'objecte "central" estigui separat dels altres components del bus ja que
 *   moltes vegades serveix de referencia per defecte.
 * + T� un vector de components. Tamb� son del tipus CModel peor son components
 *   afegits al bus: gsm-pro, lite, moduls, etc..
 *
 * Deriva de la classe XmlNotify perque quan es crea una instalaci� a partir d'un
 * fitxer XML (Open Instalacio) el XMLParser cridar� a les funcions foundNode i
 * foundElement.
 * ************************************************************************** */

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "myXmlParser\XmlNotify.h"
#include "model.h"
#include "Data.h"
#include "codi.h"
#include "util.h"

#include <wx/textctrl.h>
#include <wx/string.h>


//#define _DEBUG_XMLPARSER

class CInstalation: public XmlNotify
{
    CData dades;
    CModel central;
    string fileName, codiDefecte;
    bool canvis;
    string msgError;

    CModel *tmpModel;
    CCodi *tmpCodi;
    CEvent *tmpEvent;
    CGroupFlash *tmpGrupFlash;
    CFitxerFlash *tmpFitxer;
    int tmpElement, tmpCol;
    string lastNode;
    bool totOk;
#ifdef ESTAT_GRAFIC
    CComponentGrafic *tmpCompGraf;
#endif

    public:
        wxTextCtrl *control;
        CInstalation();
        void Clear();
        ~CInstalation();
        CLanguage *Lang;
        string& GetLastError();
        bool SetControlPanel(wxString id, wxString model, wxString versio);
        void ClearControlPanel();
        bool SetDades(CData & dades);
        void ClearDades();
        CData *GetDataObj();
        CModel *GetControlPanel();
        CModel *GetComponent(int index);
        CCodi* GetCodiById(wxString di);
        string& GetCodiInstalador();
        string& GetMainCodiAbonat();
        string& GetCodiVal(string add);
        string& GetAliasZ(int z);
        string& GetAliasA(int z);
        string& GetAliasU(int z);
        string& GetAliasO(int z);
        string& GetAliasR(int z);
        string GetPhNum();
        string GetPhNumCSD();
        bool GetCallback();
        int GetConnectionType();

        bool SetFileName(string &fileName);
        string& GetFileName();

        bool GettotOk();
        bool GetChanges();
        void ClrChanges();

        bool ParseJSON(wxJSONValue root,wxString versio);
        //XmlNotify
        void foundNode		( wxString & name, wxXmlNode* attributes );
        void foundElement	( wxString & name, wxString & value, wxXmlNode* attributes );
};

#endif
