#ifndef FITXERFLASH_H
#define FITXERFLASH_H

#include <iostream>
#include <fstream>
#include <string>
#include <wx/string.h>
using namespace std;


//Els fitxers ROM (els webservers) tenen aquesta cap�alera
struct TRomInfo{
    char	ID[4];                  //+0x00
    unsigned char	HiVer;          //+0x04
	unsigned char	LoVer;          //+0x05
	unsigned char	reserved[2];    //+0x06
	unsigned char	date[4];        //+0x08
	unsigned char	time[4];        //+0x0C
	unsigned int	FatSize;        //+0x10
	unsigned int	FileCount;      //+0x14
	unsigned int	DataSize;       //+0x18
	unsigned int 	DataSectors;    //+0x1C
	unsigned int 	DataStart;      //+0x20
	unsigned char   reserved2[12];  //+0x24
}; //+0x30 = 48 bytes!

class CFitxerFlash
{
    string id, nomFile, FileExt;
    wxString descr;
    int index, sector, lon, fitxerLength;
    unsigned char *fitxer;
    bool selec, isROM;

    public:
        CFitxerFlash();
        ~CFitxerFlash();

        bool SetId(string &id);
        bool SetDescr(wxString &descr);
        bool SetNomFitxer(string &nomFitxer);
        bool SetIndex(int index);
        bool SetSector(int sector);
        bool SetLon(int lon);
        bool SetSelec(bool selec);
        bool IsSelec();
        bool SetFileExt(string &sFileExt);

        string& GetId();
        wxString& GetDescr();
        string& GetNomFitxer();
        string& GetFileExt(void);
        int GetIndex();
        int GetSector();
        int GetLon();
        int GetBytes();
        int GetBytesFitxer();
        unsigned char *GetFile();
        void Llibera();
        bool Reset();

        bool SetROM(bool rom);
        bool IsROM();
        TRomInfo *GetROMInfo();
};

#endif
