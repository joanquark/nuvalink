#include "model.h"
#include <wx/filename.h>
#include <wx/stdpaths.h>
#include <wx/tokenzr.h>
#include "DadesPanel.h"

CModel::CModel(CLanguage *lang)
{
    tmpCodi=0;
    tmpGrup=0;
    tmpGrupFlash=0;
    numZones=0;
    numSortides=0;
    vrxZona=0;
    formula=-1;
    dinsNodeEstat=false;
    teEstat=false;
    this->Lang=lang;
}

CModel::CModel()
{
    tmpCodi=0;
    tmpGrup=0;
    tmpGrupFlash=0;
    numZones=0;
    numSortides=0;
    vrxZona=0;
    formula=-1;
    dinsNodeEstat=false;
    teEstat=false;
}

void CModel::Clear()
{

    vector<CGroup *> tmp1;
    vector<CCodi *> tmp2;
    vector<CGroupFlash *> tmp3;

    for (int i=0; i<grups.size(); i++)
        delete grups[i];
    grups.clear();
    tmp1.swap(grups);
    //std::swap( grups, tmp1 );

    for (int i=0; i<codis.size(); i++)
        delete codis[i];
    codis.clear();
    tmp2.swap(codis);
//    std::swap( codis, tmp2 );

    for (int i=0; i<grupsFlash.size(); i++)
        delete grupsFlash[i];
    grupsFlash.clear();
    //std::swap( grupsFlash, tmp3 );
    tmp3.swap(grupsFlash);

    estat.Clear();
    lEvents.Clear();
    tmpCodi=0;
    tmpGrup=0;
    tmpGrupFlash=0;
    id="";
    model="";
    versio="";
    formula=-1;
    numZones=0;
    numSortides=0;
    vrxZona=0;
    grupPare.Clear();
    grupEstat.Clear();
}

CModel::~CModel()
{
    Clear();
}

bool CModel::SetVersion(wxString versio)
{
    this->versio = versio;
    return true;
}



bool CModel::SetModel(wxString model)
{
    this->model = model;
    return true;
}


bool CModel::SetId(wxString id)
{
    this->id = id;
    return true;
}


bool CModel::HasStatus(void)
{
    return teEstat;
}

bool CModel::SetStatus()
{
   // return estat.SetStatus(nouEstat);
   return true;
}

bool CModel::TeAlias(void)
{
    return teAlias;
}

bool CModel::SetTeAlias(bool alias)
{
   teAlias=alias;
   return true;
}


bool CModel::SetNumZ(int numZones)
{
    if (numZones < 0)
        return false;

    this->numZones = numZones;
    return true;
}

bool CModel::SetNumUsers(int numUsers)
{
    if (numUsers < 0)
        return false;

    this->numUsers = numUsers;
    return true;
}

bool CModel::SetNumUsersExt(int numUsers)
{
    if (numUsers < 0)
        return false;

    this->numUsersExt = numUsers;
    return true;
}

int CModel::GetNumUsersExt()
{
    return numUsersExt;
}

bool CModel::SetNumVRxZona(int vrxZona)
{
    if (vrxZona < 0)
        return false;

    this->vrxZona = vrxZona;
    return true;
}

bool CModel::SetsizeofMsk(int size)
{
    sizeofMsk=size;
}

int CModel::GetsizeofMsk()
{
    return sizeofMsk;
}

int CModel::GetNumSortides()
{
    return numSortides;
}

bool CModel:: SetNumOuts(int num)
{
    numSortides=num;
}

//Aquesta funcio es cridada pel XMLParser que esta llegint
//el XML de configuracio cada cop que obre un node <panel> <grup>, etc..
void CModel::foundNode ( wxString & name, wxXmlNode* attributes )
{
    wxString Id;
    attributes->GetPropVal("id",&Id);
    string strId=Id.c_str();

    if (name == "model") {
        grupPare.Clear();

        wxString Versio;

        attributes->GetPropVal("versio",&Versio);
        string strVersio=Versio.c_str();

        SetModel(strId);
        SetVersion(strVersio);
    }
    else if (name=="grup") {
        CGroup *grup = new CGroup();
        tmpGrup = grup;
        if (!dinsNodeEstat) {
            tmpGrup->SetFather(&grupPare);
            grupPare.AddChild(tmpGrup);
            AddGrup(tmpGrup);
        }
        else {
            tmpGrup->SetFather(&grupEstat);
            grupEstat.AddChild(tmpGrup);
            AddGrup(tmpGrup);
        }
        tmpGrup->SetId(strId);
        wxString val;
        if (attributes->GetPropVal("type",&val)){
            if (val=="bits") tmpGrup->SetBitGroup();
        }
        if (attributes->GetPropVal("caract",&val)){
            if (val=="seguits") tmpGrup->SetFollowCodis();
            else if (val=="norecv") tmpGrup->SetNotRx();
            else if (val=="nosend") tmpGrup->SetNotSend();
        }
        if (attributes->GetPropVal("distancia",&val)){
            int d;
            if (val != "") {
                std::stringstream ss(val.c_str());
                ss >> d;
            }
            tmpGrup->SetDistance(d);
        }
    }
    else if (name=="subgrup") {
        CGroup *grup = new CGroup();
        if (!dinsNodeEstat) {
            tmpGrup = grupPare.GetChildGroup(grupPare.GetNumChildGroup()-1);
        }
        else {
            tmpGrup = grupEstat.GetChildGroup(grupEstat.GetNumChildGroup()-1);
        }
        grup->SetFather(tmpGrup);
        tmpGrup->AddChild(grup);
        tmpGrup = grup;
        tmpGrup->SetId(strId);
        wxString val;
        if (attributes->GetPropVal("type",&val)){
            if (val=="bits") tmpGrup->SetBitGroup();
        }
        if (attributes->GetPropVal("caract",&val)){
            if (val=="seguits") tmpGrup->SetFollowCodis();
            else if (val=="norecv") tmpGrup->SetNotRx();
            else if (val=="nosend") tmpGrup->SetNotSend();
        }
        if (attributes->GetPropVal("distancia",&val)){
            int d=0;
            if (val != "") {
                std::stringstream ss(val.c_str());
                ss >> d;
            }
            tmpGrup->SetDistance(d);
        }
        AddGrup(tmpGrup);
    }
    else if (name=="intgrup") {
        CGroup *grup = new CGroup();
        if (!dinsNodeEstat) {
            tmpGrup = grupPare.GetChildGroup(grupPare.GetNumChildGroup()-1);
            tmpGrup = tmpGrup->GetChildGroup(tmpGrup->GetNumChildGroup()-1);
        }
        else {
            tmpGrup = grupEstat.GetChildGroup(grupEstat.GetNumChildGroup()-1);
            tmpGrup = tmpGrup->GetChildGroup(tmpGrup->GetNumChildGroup()-1);
        }
        grup->SetFather(tmpGrup);
        tmpGrup->AddChild(grup);
        tmpGrup = grup;

        tmpGrup->SetId(strId);
        wxString val;
        if (attributes->GetPropVal("type",&val)){
            if (val=="bits") tmpGrup->SetBitGroup();
        }
        if (attributes->GetPropVal("caract",&val)){
            if (val=="seguits") tmpGrup->SetFollowCodis();
            else if (val=="norecv") tmpGrup->SetNotRx();
            else if (val=="nosend") tmpGrup->SetNotSend();
        }
        if (attributes->GetPropVal("distancia",&val)){
            int d=0;
            if (val != "") {
                std::stringstream ss(val.c_str());
                ss >> d;
            }
            tmpGrup->SetDistance(d);
        }

        AddGrup(tmpGrup);
    }
    else if (name=="grup-flash") {
        CGroupFlash *grupFlash = new CGroupFlash();
        tmpGrupFlash = grupFlash;
        tmpGrupFlash->SetFather(&grupPare);
        tmpGrupFlash->SetId(strId);
        //grupPare.AddChild(tmpGrup);
        AddGrupFlash(tmpGrupFlash);
    }
    else if (name=="columna") {
        tmpCol = tmpGrupFlash->AddColumna();
    }
    else if (name=="codi") {
        tmpCodi = GetCodiById(strId);
        if (!tmpCodi) {
            CCodi *codi = new CCodi();
            tmpCodi = codi;
            tmpCodi->SetId(strId);
            tmpCodi->SetKpAddress(strId);           //at least to have kp address equal to codi
            AddCodi(tmpCodi);
        }
        tmpGrup->AddChild(tmpCodi);
        wxString val;
        if (attributes->GetPropVal("caract",&val)){
            if (val=="noedit") tmpCodi->SetEditable(false);
            else if (val=="nosend") tmpCodi->SetNotSend();
        }
    }
    else if (name=="estat") {
        dinsNodeEstat = true;
        teEstat = true;
    }
    else if (name=="tcvzona") {
        wxString sformula;
        attributes->GetPropVal("formula",&sformula);
        if (sformula == "A"){
            formula = FORMULA_ZONES_A;
        }else if (sformula == "B"){
            formula = FORMULA_ZONES_B;
        }else{
            formula = -1;
        }
    }
    else if (name=="tcvalim") {
//        wxMessageBox("tvcalim");
        wxString sformula;
        attributes->GetPropVal("formula",&sformula);
        if (sformula == "5.54"){
            formula = FORMULA_ALIM_554;
        }else if (sformula == "0.5"){
            formula = FORMULA_ALIM_050;
        }else{
            formula = -1;
        }
    }
    else if (name=="fitxer") {
        tmpFitxer = new CFitxerFlash();
        tmpFitxer->SetId(strId);
        tmpGrupFlash->AddFitxer(tmpFitxer);
        tmpFitxer->SetROM(false);                // dafault no ROM
        wxString caract;
        attributes->GetPropVal("caract",&caract);
        if (caract == "ROM")
            tmpFitxer->SetROM(true);

    }

    lastNode = name;
}

/*void CModel::endNode (wxString& name, wxString& attributes)
{
    if (name == "estat")
        dinsNodeEstat = false;
}*/

//Aquesta funcio �s cridada pel XMLParser que esta llegint
//el XML de configuracio cada cop que troba un element
void CModel::foundElement ( wxString & name, wxString & value, wxXmlNode* attributes )
{

    string strval=(string)value.c_str();
    wxString Id;
    attributes->GetPropVal("id",&Id);
    string strId=Id.c_str();
    wxString wxlang;
    wxlang="un";

    string lang = wxlang.c_str();

    if (lastNode=="model") {
        if (name=="descr") {
            grupPare.SetDescr(Lang->GetModelMiss(value));
        }
        else if (name=="zones") {
            int b;
            std::stringstream ss(strval);
            ss >> b;
            SetNumZ(b);
            estat.SetNumZ(b);
        }
        else if (name=="users") {
            int b;
            std::stringstream ss(strval);
            ss >> b;
            SetNumUsers(b);
            estat.SetNumUsers(b);
        }
        else if (name=="usersext") {
            int b;
            std::stringstream ss(strval);
            ss >> b;
            SetNumUsersExt(b);
        }
        else if (name=="arees") {
            int b;
            std::stringstream ss(strval);
            ss >> b;
            estat.SetNumAreas(b);
        }
        else if (name=="outputs") {
            int b;
            std::stringstream ss(strval);
            ss >> b;
            SetNumOuts(b);
            estat.SetNumOuts(b);
        }
        else if (name=="alias") {
            int b;
            std::stringstream ss(strval);
            ss >> b;
            SetTeAlias(false);
            if (b)
                SetTeAlias(true);
        }

        else if (name=="sizeofMsk"){
            int b;
            std::stringstream ss(strval);
            ss >> b;
            SetsizeofMsk(b);
        }

    }
    else if (lastNode=="codi") {
        if (name=="val") {
            string miss=(string)Lang->GetModelMiss(value).c_str();
            tmpCodi->SetDefVal(miss);
        }

        else if (name=="type") {
            int tipus=0; //0 indeterminat
            if (strval=="nibble2") tipus = TDATA_NIBBLE2;
            else if (strval=="nibble4") tipus = TDATA_NIBBLE4;
            else if (strval=="nibble6") tipus = TDATA_NIBBLE6;
            else if (strval=="nibble8") tipus = TDATA_NIBBLE8;
            else if (strval=="nibble14") tipus = TDATA_NIBBLE14;
            else if (strval=="nibble16") tipus = TDATA_NIBBLE16;
            else if (strval=="nibble28") tipus = TDATA_NIBBLE28;
            else if (strval=="nibble32") tipus = TDATA_NIBBLE32;
            else if (strval=="ipv4") tipus = TDATA_IPV4;
            else if (strval=="string") tipus = TDATA_STRING;
            else if (strval=="string24") tipus = TDATA_STRING24;
            else if (strval=="string32") tipus = TDATA_STRING32;
            else if (strval=="string64") tipus = TDATA_STRING64;
            else if (strval=="bitmask") tipus = TDATA_BITMASK;
            else if (strval=="int8") tipus = TDATA_INT8;
            else if (strval=="uint8") tipus = TDATA_UINT8;
            else if (strval=="uint16") tipus = TDATA_UINT16;
            else if (strval=="int16") tipus = TDATA_INT16;
            else if (strval=="horaquart") tipus = TDATA_HOURQUARTER;
            else if (strval=="float16") tipus = TDATA_FLOAT16;
            else if (strval=="alim050") tipus = TDATA_ALIM050;
            else if (strval=="alim018") tipus = TDATA_ALIM018;
            else if (strval=="alim100") tipus = TDATA_ALIM100;
            else if (strval=="ch32") tipus = TDATA_CH32;
            else if (strval=="alimmv") tipus = TDATA_ALIMMV;
            else if (strval=="mbar") tipus = TDATA_MBAR;
            else if (strval=="float") tipus = TDATA_FLOAT;
            else if (strval=="int32") tipus = TDATA_INT32;
            else tipus=0;

            tmpCodi->SetTypeData(tipus);
        }
        else if (name=="address") {
            tmpCodi->SetTypeAdd(TIPUS_ADR_EEPROM);
            tmpCodi->SetAddress(strval);
        }
        else if (name=="flash") {
            tmpCodi->SetTypeAdd(TIPUS_ADR_FLASH);
            tmpCodi->SetAddress(strval);
        }
        else if (name=="offset") {
            tmpCodi->SetOffset(sToI(strval));
        }
        else if (name=="upl"){
            tmpCodi->SetUpl(sToI(strval));
        }
        else if (name=="min"){
            tmpCodi->SetMin(sToI(strval));
        }
        else if (name=="max"){
            tmpCodi->SetMax(sToI(strval));
        }
        else if (name=="match"){
            tmpCodi->SetMatch(strval);
        }
        else if (name=="verify"){
            tmpCodi->SetVerify(sToI(strval));
        }
        else if (name=="wizz"){
            tmpCodi->SetWizz(sToI(strval));
        }
        else if (name=="wizzh"){
            tmpCodi->SetWizzh(strval);
        }
        else if (name=="wizzl"){
            tmpCodi->SetWizzl(strval);
        }
        else if (name=="cat"){
            tmpCodi->SetCat(strval);
        }
        else if (name=="descr") {
            tmpCodi->SetDescr(Lang->GetModelMiss(value));
        }
        else if (name=="bitdescr") {
            wxStringTokenizer arrTok(value, wxT(";"));
            int bit=0;
            while ( arrTok.HasMoreTokens() )
            {
                wxString tok=arrTok.GetNextToken();
                tmpCodi->SetBitDescr(bit++,Lang->GetModelMiss(tok));
            }
        }
        else if (name=="omplir") {
            if (strval.length() == 1) {
                tmpCodi->SetOmplir(strval[0], true);
            }
            else if (strval.length() == 2) {
                if (strval[0] == '+')
                    tmpCodi->SetOmplir(strval[1], true);
                else
                    tmpCodi->SetOmplir(strval[0], false);
            }
        }

    }
    else if (lastNode=="grup") {
        if (name=="descr") {
            tmpGrup->SetDescr(Lang->GetModelMiss(value));
        }else if (name=="upl"){
            tmpGrup->SetUpl(sToI(strval));
        }else if (name=="cat"){
            tmpGrup->SetCat(strval);
        }
    }
    else if ((lastNode=="subgrup") || (lastNode=="intgrup")) {
        if (name=="descr") {
            tmpGrup->SetDescr(Lang->GetModelMiss(value));
        }
        else if (name=="bitdescr") {
            wxStringTokenizer arrTok(value, wxT(";"));
            int bit=0;
            while ( arrTok.HasMoreTokens() )
            {
                wxString tok=arrTok.GetNextToken();
                tmpCodi->SetBitDescr(bit++,Lang->GetModelMiss(tok));
            }
        }else if (name=="upl"){
            tmpGrup->SetUpl(sToI(strval));
        }else if (name=="cat"){
            tmpGrup->SetCat(strval);
        }
    }
    else if (lastNode=="grup-flash") {
        if (name=="descr") {
            tmpGrupFlash->SetDescr(Lang->GetModelMiss(value));
        }
        else if (name=="elements") {
            tmpGrupFlash->SetNumElements(sToI(strval));
        }
        else if (name=="bytes-element") {
            tmpGrupFlash->SetBytesElement(sToI(strval));
        }
        else if (name=="addrbase") {
            tmpGrupFlash->SetAddress(strval);

        }else if (name=="upl"){
            tmpGrupFlash->SetUpl(sToI(strval));

        }else if (name=="cat"){
            tmpGrupFlash->SetCat(strval);
        }

    }
    else if (lastNode=="columna") {
        if (name=="descr") {
            tmpGrupFlash->SetColumnaDescr(tmpCol,Lang->GetModelMiss(value));
        }
        else if (name=="type") {
            int tipus=0; //0 indeterminat
            if (strval=="nibble2") tipus = TDATA_NIBBLE2;
            else if (strval=="nibble4") tipus = TDATA_NIBBLE4;
            else if (strval=="nibble6") tipus = TDATA_NIBBLE6;
            else if (strval=="nibble8") tipus = TDATA_NIBBLE8;
            else if (strval=="nibble14") tipus = TDATA_NIBBLE14;
            else if (strval=="nibble16") tipus = TDATA_NIBBLE16;
            else if (strval=="nibble28") tipus = TDATA_NIBBLE28;
            else if (strval=="nibble32") tipus = TDATA_NIBBLE32;
            else if (strval=="ipv4") tipus = TDATA_IPV4;
            else if (strval=="string") tipus = TDATA_STRING;
            else if (strval=="string24") tipus = TDATA_STRING24;
            else if (strval=="string32") tipus = TDATA_STRING32;
            else if (strval=="string64") tipus = TDATA_STRING64;
            else if (strval=="bitmask") tipus = TDATA_BITMASK;
            else if (strval=="int8") tipus = TDATA_INT8;
            else if (strval=="uint8") tipus = TDATA_UINT8;
            else if (strval=="uint16") tipus = TDATA_UINT16;
            else if (strval=="int16") tipus = TDATA_INT16;
            else if (strval=="horaquart") tipus = TDATA_HOURQUARTER;
            else if (strval=="float16") tipus = TDATA_FLOAT16;
            else if (strval=="alim100") tipus = TDATA_ALIM100;
            else if (strval=="ch32") tipus = TDATA_CH32;
            else if (strval=="alim050") tipus = TDATA_ALIM050;
            else if (strval=="alim018") tipus = TDATA_ALIM018;
            else if (strval=="alimmv") tipus = TDATA_ALIMMV;
            else if (strval=="mbar") tipus = TDATA_MBAR;
            else if (strval=="float") tipus = TDATA_FLOAT;
            else if (strval=="int32") tipus = TDATA_INT32;
            else tipus=0;

            tmpGrupFlash->SetColumnaTipus(tmpCol, tipus);
        }
        else if (name=="val") {
            tmpGrupFlash->SetColumnaVal(tmpCol, strval);
            CCodi *codi = tmpGrupFlash->GetColumnaCodi(tmpCol);
            codi->SetDefVal(strval);
        }
        else if (name=="omplir") {
            CCodi *mytmpcodi = tmpGrupFlash->GetColumnaCodi(tmpCol);
            if (mytmpcodi) {
                if (strval.length() == 1) {
                    mytmpcodi->SetOmplir(strval[0], true);
                }
                else if (strval.length() == 2) {
                    if (strval[0] == '+')
                        mytmpcodi->SetOmplir(strval[1], true);
                    else
                        mytmpcodi->SetOmplir(strval[0], false);
                }
            }
        }
    }
    else if (lastNode=="fitxer") {
        if (name=="index") {
            tmpFitxer->SetIndex(sToI(strval));
        }
        else if (name=="long") {
            tmpFitxer->SetLon(sToI(strval));
        }
        else if (name=="sector") {
            tmpFitxer->SetSector(sToI(strval));
        }
        else if (name=="fileext"){
            tmpFitxer->SetFileExt(strval);
        }
        else if (name=="descr") {
            tmpFitxer->SetDescr(Lang->GetModelMiss(value));
        }
    }
    //Mesures analogiques (part de l'estat)
    else if (lastNode=="estat") {
        if (name=="noviaradio") {
            estat.SetHasWireless(false);
        }
        else if (name=="noviaradiouser"){
            estat.SetHasWirelessUser(false);
        }
        else if (name=="noviaradioout"){
            estat.SetHasWirelessOut(false);
        }
        else if (name=="nobeacons"){
            estat.SetHasBeacons(false);
        }
        if (name=="steep"){
            estat.SetStEeprom(ID_ESTAT_ALL,strval);
        }else if (name=="stlen"){
            estat.SetStMemLen(ID_ESTAT_ALL,sToI(strval));
        }else if (name=="zeep"){
            estat.SetStEeprom(ID_ESTAT_INPUT,strval);
        }else if (name=="zlen"){
            estat.SetStMemLen(ID_ESTAT_INPUT,sToI(strval));
        }else if (name=="oeep"){
            estat.SetStEeprom(ID_ESTAT_OUTPUT,strval);
        }else if (name=="olen"){
            estat.SetStMemLen(ID_ESTAT_OUTPUT,sToI(strval));
        }else if (name=="aeep"){
            estat.SetStEeprom(ID_ESTAT_AREAS,strval);
        }else if (name=="alen"){
            estat.SetStMemLen(ID_ESTAT_AREAS,sToI(strval));
        }else if (name=="syseep"){
            estat.SetStEeprom(ID_ESTAT_SYS,strval);
        }else if (name=="syslen"){
            estat.SetStMemLen(ID_ESTAT_SYS,sToI(strval));
        }else if (name=="nosortides") {
            estat.SetNumOuts(0);
        }else if (name=="nogeneral") {
            estat.SetHasGeneral(false);
        }else if (name=="notesttelefonic") {
            estat.SetHasReportTest(false);
        }
#ifdef ESTAT_GRAFIC
        else if (name=="noestatgrafic") {
            estat.SetTeEstatGrafic(false);
        }
#endif
#ifdef _PICTURES
        else if (name=="pictures"){
            estat.SetHasPictures(true);
        }
#endif
    }
    else if (lastNode=="events") {
        if (name=="address") {
            lEvents.SetDirFlash(strval);
        }
        else if (name=="maxevents") {
            int max;
            std::stringstream ss(strval);
            ss >> max;
            lEvents.SetMaxPunter(max);
        }
        else if (name=="bytesevent") {
            int bytes;
            std::stringstream ss(strval);
            ss >> bytes;
            lEvents.SetBytesEvent(bytes);
        }
        else if (name=="type") {
            int tipus=0;

            if (strval=="mskev") {
                tipus = TIPUS_EVENT_MSK;
            }
            lEvents.SetTypeEvents(tipus);
        }
    }
}

int CModel::AddGrup(CGroup *grup)
{
    grups.push_back(grup);
    return (grups.size()-1);
}

int CModel::AddGrupFlash(CGroupFlash *grupFlash)
{
    grupsFlash.push_back(grupFlash);
    return (grupsFlash.size()-1);
}

int CModel::AddCodi(CCodi *codi)
{
    codis.push_back(codi);
    string pp=codi->GetId();

    return (codis.size()-1);
}


CCodi *CModel::GetCodi(int index)
{
    return (codis[index]);
}

// ----------------------------------------- GetCodiById -------------------
//Retorna la referencia al codi que correspongui amb la id donada
//Si no es troba cap, es retorna un 0
CCodi *CModel::GetCodiById(wxString id)
{
    int j;
    j=codis.size();

    for (int i=0; i<j; i++) {
        if (codis[i]->GetId()==id) {
            return (codis[i]);
        }
    }
    return 0;
}

// ----------------------------------------- GetCodiById -------------------
//Retorna la referencia al codi que correspongui amb l'adre�a eep donada.
CCodi *CModel::GetCodiByAdd(wxString add)
{
    int j;
    j=codis.size();

    for (int i=0; i<j; i++) {
        if (codis[i]->GetAddress() ==add) {
            return (codis[i]);
        }
    }
    return 0;
}


CGroup *CModel::GetGroup(int index)
{
    return (grups[index]);
}


CGroupFlash *CModel::GetGroupFlash(int index)
{
    return (grupsFlash[index]);
}


//Retorna la referencia al codi que correspongui amb la id donada
//Si no es troba cap, es retorna un 0
CGroup *CModel::GetGroupById(wxString id)
{
    for (int i=0; i<grups.size(); i++) {
        if (grups[i]->GetId()==id) {
            return (grups[i]);
        }
    }
    return 0;
}

//Retorna la referencia al codi que correspongui amb la id donada
//Si no es troba cap, es retorna un 0
CGroupFlash *CModel::GetGroupFlashById(wxString id)
{
    for (int i=0; i<grupsFlash.size(); i++) {
        if (grupsFlash[i]->GetId()==id) {
            return (grupsFlash[i]);
        }
    }
    return 0;
}

CGroup *CModel::GetFatherGroup()
{
    return (&grupPare);
}

CGroup *CModel::GetStatusGroup()
{
    return (&grupEstat);
}

CStatus *CModel::GetStatus()
{
    return &estat;
}


string& CModel::GetId()
{
    return id;
}

string& CModel::GetModel()
{
    return model;
}

string CModel::GetModelByDevice(unsigned char dev)
{
    string res;
    if (dev==TPANEL_STR4TA) res="Str4ta";
    else if (dev==TPANEL_STR4TA4A6) res="Str4ta4A6";
    else if (dev==TPANEL_CIRRUS4T) res="Cirrus4T";
    else if (dev==TPANEL_CIRRUS4A6) res="Cirrus4A6";
    else if (dev==TPANEL_PILEUS4T) res="Pileus4T";
    else if (dev==TPANEL_LTM400) res="LTM400";
    else if (dev==TPANEL_LTIO) res="LTIO";
    else if (dev==TPANEL_NUVATRACK) res="NuvaTrack";
    else if (dev==TPANEL_NUVATRACK4A6) res="NuvaTrack4A6";
    else res=model;

    return res;
}

bool CModel::SetDevice(CDevice dev){
    this->device=dev;
    this->lEvents.SetMaxPunter(this->device.GetEventQueueSize());
    return true;
}

CDevice CModel::GetDevice()
{
    return this->device;
}

string& CModel::GetVersio()
{
    return versio;
}

int CModel::GetNumZones()
{
    return numZones;
}

int CModel::GetNumVRxZona()
{
    return vrxZona;
}

//Genera el Model obrint l'arxiu XML adecuat

bool CModel::Genera()
{
    ifstream fitxer;
    string fileName;
    wxFileName filewx;
    int cnt;

    if ((model=="") || (versio==""))
        return false;

    wxStandardPaths path;
    string strpath=path.GetDataDir().c_str();

    for (cnt=0;cnt<NUM_MODEL_FOLDERS+1;cnt++){
        wxString modelpath=ModelFolders[cnt];
        fileName = strpath + modelpath /* "/models/SPT/"*/ +  model + "-" + versio + ".xml";

        if (filewx.FileExists(_wx(fileName))){
            //Ens assegurem que el creem desde zero
            grups.clear();
            codis.clear();
            tmpCodi=0;
            tmpGrup=0;

            //Analitzem el XML
            myXmlParser xml;
            //Li diem al objecte que interpreta el xml que informi al objecte CModel
            xml.SetSubscriber(*this);
            dinsNodeEstat=false;
            bool docParsed = xml.parse(_wx(fileName)/* (wxChar*)buffer->ToAscii(), 0, buffer->size() - 1*/ );
            //XML defectuos
            if (!docParsed) {
                //wxMessageBox("doc not parsed: "+fileName);
                return false;
            }
            return true;
        }else{
            //wxMessageBox("not exist: "+fileName);
        }
    }
    return false;
}

int CModel::GetNumCodis()
{
    return codis.size();
}

int CModel::GetNumGrups()
{
    return grups.size();
}

int CModel::GetNumGrupsFlash()
{
    return grupsFlash.size();
}

CEventList *CModel::GetEventList()
{
    return &lEvents;
}

bool CModel::GetChanges()
{
    return grupPare.GetChanges();
}

void CModel::ClrChanges()
{
    grupPare.ClrChanges();
}

string& CModel::GetDescr()
{
    return grupPare.GetDescr();
}

bool CModel::SetDescr(wxString descr)
{
    return grupPare.SetDescr(descr);
}



