#include "wx_pch.h"
#include "ModelDlg.h"


#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(ModelDlg)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(ModelDlg)
//*)

//(*IdInit(ModelDlg)
const long ModelDlg::ID_SPEEDBUTTON1 = wxNewId();
const long ModelDlg::ID_SPEEDBUTTON2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(ModelDlg,wxDialog)
	//(*EventTable(ModelDlg)
	//*)
END_EVENT_TABLE()

ModelDlg::ModelDlg(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,wxString *model,wxString* ver,CLanguage *lang,CData *dades)
{
    this->version=ver;
    this->model=model;
	BuildContent(parent,id,pos,size,model,ver,lang,dades);
}

void ModelDlg::BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,wxString* model, wxString* ver,CLanguage *lang,CData *dades)
{
	//(*Initialize(ModelDlg)
	wxStaticBoxSizer* StaticBoxSizer1;
	wxStaticBoxSizer* StaticBoxSizer2;

	Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
	SetClientSize(wxDefaultSize);
	Move(wxDefaultPosition);
	StaticBoxSizer1 = new wxStaticBoxSizer(wxVERTICAL, this, _("New Model"));
	StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Action"));
	wxBitmap ButtOk_BMP(_("./icons/toolbar/ok.png"), wxBITMAP_TYPE_ANY);
	ButtOk = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, ButtOk_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(40,40), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	ButtOk->SetUserData(0);
	StaticBoxSizer2->Add(ButtOk, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	wxBitmap ButtCancel_BMP(_("./icons/toolbar/exit.png"), wxBITMAP_TYPE_ANY);
	ButtCancel = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, ButtCancel_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(40,40), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	ButtCancel->SetUserData(0);
	StaticBoxSizer2->Add(ButtCancel, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	StaticBoxSizer1->Add(StaticBoxSizer2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(StaticBoxSizer1);
	StaticBoxSizer1->Fit(this);
	StaticBoxSizer1->SetSizeHints(this);

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ModelDlg::OnSpeedButton1LeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ModelDlg::OnButtCancelLeftClick);
	//*)
    panel=new DadesPanel(this,wxID_ANY,wxDefaultPosition,wxDefaultSize,model,ver,lang,dades);
	StaticBoxSizer1->Add(panel, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	StaticBoxSizer1->Add(StaticBoxSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(StaticBoxSizer1);
	StaticBoxSizer1->Fit(this);
	StaticBoxSizer1->SetSizeHints(this);

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ModelDlg::OnSpeedButton1LeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ModelDlg::OnButtCancelLeftClick);
	//*)

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ModelDlg::OnSpeedButton1LeftClick);
	//*)

	panel->UpdateValues();
}

ModelDlg::~ModelDlg()
{
    if (panel)
        delete panel;
    panel=0;
	//(*Destroy(ModelDlg)
	//*)
}


void ModelDlg::OnSpeedButton1LeftClick(wxCommandEvent& event)
{
    panel->ApplyValues();
    delete panel;
    panel=0;

    EndModal(wxID_OK);
}

void ModelDlg::OnButtCancelLeftClick(wxCommandEvent& event)
{
    delete panel;
    panel=0;

    EndModal(-1);
}
