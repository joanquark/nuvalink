#include "codi.h"
#include "string.h"
#include "util.h"
#include <winsock2.h>
/* *****************************************************************
* Class CCodi
* Estructura que enmagatzema el valor, el tipus de dades i la
* direccio en memoria (a la central) de cada adre�a de programacio
* ****************************************************************** */

CCodi::CCodi()
{
    Clear();
}

void CCodi::Clear()
{
    address="";
    kpAddress="";
    id="";
    descr="";
    tipusDir=0;
    tipusData=0;
    val=iniVal="";
    relleno='F';
    omplirDreta=true;
    tipusDataValid=false;
    for (int i=0; i<8; i++) {
        bitdescr.push_back(new string(""));
    }
    defVal = "";
    for (int i=0; i<MAX_LON_BYTES_CODI; i++)
        buffvalue[i]=0;
    canvis=false;
    editchange=false;
    offset=0;
    editable=true;
    upl=0;
    vMin=0;
    vMax=0;
    vMatch="";
    verify=0;
    wizz=0;
    cat="ON";
    DontSend=false;
    boardpowerfactor=0.18;

}

CCodi CCodi::operator= (CCodi param)
{
    this->address=param.address;
    this->kpAddress=param.kpAddress;
    this->id=param.id;
    this->descr=param.descr;
    this->tipusDir=param.tipusDir;
    this->tipusData=param.tipusData;
    this->val=param.val;
    this->iniVal=param.iniVal;
    this->tipusDataValid=param.tipusDataValid;
    this->bitdescr=param.bitdescr;
    this->relleno=param.relleno;
    this->omplirDreta=param.omplirDreta;
    this->defVal = param.defVal;
    for (int i=0; i<MAX_LON_BYTES_CODI; i++)
        this->buffvalue[i]=param.buffvalue[i];
    this->canvis=param.canvis;
    this->offset=param.offset;
    this->editable=param.editable;
    this->editchange=param.editchange;
    this->upl=param.upl;
    this->vMax=param.vMax;
    this->vMin=param.vMin;
    this->verify=param.verify;
    this->wizz=param.wizz;
    this->cat=param.cat;
    this->DontSend=param.upl;
    return *this;
}

CCodi::~CCodi()
{

    vector<string *> tmp;
    for (int i=0; i<bitdescr.size(); i++) {
        delete bitdescr[i];
    }
    bitdescr.clear();
    tmp.swap(bitdescr);
//    Clear();
}

bool CCodi::SetAddress(string& address)
{
    if (isHexString(address)) {
        this->address = address;
        return true;
    }
    else return false;
}

bool CCodi::SetNotSend()
{
    DontSend=true;
    return true;
}

bool CCodi::IsNotSend()
{
    return DontSend;
}

bool CCodi::SetOffset(int offset)
{
    if (offset >= 0) {
        this->offset = offset;
        return true;
    }
    return false;
}

bool CCodi::SetKpAddress(string& kpa)
{
    if (isHexString(address)) {
        this->kpAddress = kpa;
        return true;
    }
    else return false;
}

bool CCodi::SetId(string& id)
{
    this->id = id;
    return true;
}

bool CCodi::SetTypeAdd(int tipus)
{
//    if ( (tipus == TIPUS_ADR_RAM) ||
//    (tipus == TIPUS_ADR_EEPROM) ) {
        this->tipusDir = tipus;
        return true;
//    }
//    else {
//        return false;
//    }
}

bool CCodi::SetTypeData(int tipus)
{
    if ( (tipus == TDATA_BITMASK) ||
    (tipus == TDATA_UINT8) ||
    (tipus == TDATA_INT8) ||
    (tipus == TDATA_UINT16) ||
    (tipus == TDATA_INT16) ||
    (tipus == TDATA_NIBBLE2) ||
    (tipus == TDATA_NIBBLE4) ||
    (tipus == TDATA_NIBBLE6) ||
    (tipus == TDATA_NIBBLE8) ||
    (tipus == TDATA_NIBBLE16) ||
    (tipus == TDATA_NIBBLE14) ||
    (tipus == TDATA_NIBBLE28) ||
    (tipus == TDATA_NIBBLE32) ||
    (tipus == TDATA_STRING) ||
    (tipus == TDATA_STRING24) ||
    (tipus == TDATA_STRING32) ||
    (tipus == TDATA_STRING64) ||
    (tipus == TDATA_HOURQUARTER) ||
    (tipus == TDATA_IPV4) ||
    (tipus == TDATA_ALIM018) ||
    (tipus == TDATA_ALIM050) ||
    (tipus == TDATA_ALIM100) ||
    (tipus == TDATA_ALIMMV) ||
    (tipus == TDATA_CH32) ||
    (tipus == TDATA_MBAR) ||
    (tipus == TDATA_FLOAT16) ||
    (tipus == TDATA_INT32) ||
    (tipus == TDATA_FLOAT) ) {
        this->tipusData = tipus;
        tipusDataValid = true;

        //Potser ens havien dit un valor abans de saber el tipus.
        //ara que sabem el tipus conve revisar que el valor sigui correcte
        SetStringVal(val);
        SetStringIniVal(val);

        if (tipus == TDATA_OUTPUT)
            editable = false;

        return true;
    }
    else {
        return false;
    }
}

bool CCodi::SetOmplir(char caract, bool dreta)
{
    relleno = caract;
    omplirDreta = dreta;
    return true;
}

bool CCodi::SetDescr(wxString& descr)
{
    this->descr = descr;
}

bool CCodi::SetBitDescr(int bit, wxString& descr)
{
    if ((bit<0) || (bit>=8))
        return false;

    *bitdescr[bit] = iToS(bit+1)+".-"+descr;
    return true;
}

int CCodi::GetOffset()
{
    return offset;
}

string& CCodi::GetAddress()
{
    return address;
}

string& CCodi::GetKpAddress()
{
    return kpAddress;
}

int CCodi::GetTypeConDir()
{
    return tipusDir;
}

int CCodi::GetTypeConData()
{
    return tipusData;
}

string& CCodi::GetId()
{
    return id;
}

wxString& CCodi::GetDescr()
{
    return descr;
}

string& CCodi::GetBitDescr(int bit)
{
    if ((bit<0) || (bit>=8))
        return *bitdescr[0];

    return *bitdescr[bit];
}

string& CCodi::GetStringVal()
{
    string ret="";
    try{
        ret=val;
    }
    catch (std::string error) {
        val="";
    }

    return val;
}

string& CCodi::GetStringIniVal()
{
    return iniVal;
}


string& CCodi::GetDefVal()
{
    return defVal;
}

int32_t CCodi::GetIntVal()
{
    return intVal;
}

//Retorna el valor en funcio del format
unsigned char* CCodi::GetVal()
{
    //unsigned char buftmp[MAX_LON_STRING_CODI+1];
    unsigned char c;
    INT16_VAL iv;
    DWORD_VAL dv;
    double d;
    int intpart;

    if (val.length() > MAX_LON_STRING_CODI)
        val.resize(MAX_LON_STRING_CODI);

    //strcpy((char *)buftmp, val.c_str());

    switch (tipusData) {
        case TDATA_BITMASK:
            c = 0x01;
            buffvalue[0]=0;
            for (int j=0; j<STRLEN_BITMASK; j++) {
                if (val[j] == '1') {
                    buffvalue[0] |= c;
                }
                c <<= 1;
            }
        break;

        case TDATA_UINT8:
            if (val.length() == 1) {
                buffvalue[0] = val[0] - '0';
            }
            else if (val.length() == 2) {
                buffvalue[0] = (val[0] - '0') * 10;
                buffvalue[0] += val[1] - '0';
            }
            else if (val.length() == 3) {
                buffvalue[0] = (val[0] - '0') * 100;
                buffvalue[0] += (val[1] - '0') * 10;
                buffvalue[0] += val[2] - '0';
            }
            else buffvalue[0] = 0;
            break;
        case TDATA_INT8:
            iv.Val=atoi((char*)&val[0]);
            buffvalue[0]=iv.byte.LB;
            break;
        case TDATA_UINT16:
        case TDATA_MBAR:
            WORD_VAL wb;
            wb.Val=atoi((char*)&val[0]);
            buffvalue[0]=wb.byte.LB;
            buffvalue[1]=wb.byte.HB;
            break;
        case TDATA_INT16:
            iv.Val=atoi((char*)&val[0]);
            buffvalue[0]=iv.byte.LB;
            buffvalue[1]=iv.byte.HB;
            break;
        case TDATA_INT32:
            _INT32_VAL v32;
            v32.Val=atoi((char*)&val[0]);
            buffvalue[0]=v32.v[0];
            buffvalue[1]=v32.v[1];
            buffvalue[2]=v32.v[2];
            buffvalue[3]=v32.v[3];
            break;
        case TDATA_NIBBLE2:
            buffvalue[0] = ascii2nibble(val[0]);
            buffvalue[0] <<= 4;
            buffvalue[0] |= ascii2nibble(val[1]);
            break;
        case TDATA_NIBBLE4:
            for (int i=0; i<BYTELEN_NIBBLE4; i++) {
                buffvalue[i] = ascii2nibble(val[i*2]);
                buffvalue[i] <<= 4;
                buffvalue[i] |= ascii2nibble(val[(i*2)+1]);
            }
            break;
        case TDATA_NIBBLE6:
            for (int i=0; i<BYTELEN_NIBBLE6; i++) {
                buffvalue[i] = ascii2nibble(val[i*2]);
                buffvalue[i] <<= 4;
                buffvalue[i] |= ascii2nibble(val[(i*2)+1]);
            }
            break;
        case TDATA_IPV4:
            IN_ADDR ipv4;
            ipv4.S_un.S_addr=inet_addr(&val[0]);
            if ( ipv4.S_un.S_addr == INADDR_NONE ){
                buffvalue[0]=0;
                buffvalue[1]=0;
                buffvalue[2]=0;
                buffvalue[3]=0;
            } else{
                buffvalue[0]=ipv4.S_un.S_un_b.s_b1;
                buffvalue[1]=ipv4.S_un.S_un_b.s_b2;
                buffvalue[2]=ipv4.S_un.S_un_b.s_b3;
                buffvalue[3]=ipv4.S_un.S_un_b.s_b4;
            }
            break;

        case TDATA_NIBBLE8:
            for (int i=0; i<BYTELEN_NIBBLE8; i++) {
                buffvalue[i] = ascii2nibble(val[i*2]);
                buffvalue[i] <<= 4;
                buffvalue[i] |= ascii2nibble(val[(i*2)+1]);
            }
            break;
        case TDATA_NIBBLE14:
            for (int i=0; i<BYTELEN_NIBBLE14; i++) {
                buffvalue[i] = ascii2nibble(val[i*2]);
                buffvalue[i] <<= 4;
                buffvalue[i] |= ascii2nibble(val[(i*2)+1]);
            }
            break;
        case TDATA_NIBBLE16:
            for (int i=0; i<BYTELEN_NIBBLE16; i++) {
                buffvalue[i] = ascii2nibble(val[i*2]);
                buffvalue[i] <<= 4;
                buffvalue[i] |= ascii2nibble(val[(i*2)+1]);
            }
            break;

        case TDATA_NIBBLE28:
            for (int i=0; i<BYTELEN_NIBBLE28; i++) {
                buffvalue[i] = ascii2nibble(val[i*2]);
                buffvalue[i] <<= 4;
                buffvalue[i] |= ascii2nibble(val[(i*2)+1]);
            }
            break;

        case TDATA_NIBBLE32:
            for (int i=0; i<BYTELEN_NIBBLE32; i++) {
                buffvalue[i] = ascii2nibble(val[i*2]);
                buffvalue[i] <<= 4;
                buffvalue[i] |= ascii2nibble(val[(i*2)+1]);
            }
            break;

        case TDATA_STRING:
            for (int i=0; i<STRLEN_STRING; i++) {
                if (i<val.length())
                    buffvalue[i] = TextEditAsciiToLcd(val[i]);
                else
                    buffvalue[i] = ' ';
            }
            //strcpy((char *)buffer, (char *)buftmp);
            break;
        case TDATA_STRING24:
            for (int i=0; i<STRLEN_STRING24; i++) {
                if (i<val.length())
                    buffvalue[i] = TextEditAsciiToLcd(val[i]);
                else
                    buffvalue[i] = ' ';
            }
            break;
        case TDATA_STRING32:
            for (int i=0; i<STRLEN_STRING32; i++) {
                if (i<val.length())
                    buffvalue[i] = TextEditAsciiToLcd(val[i]);
                else
                    buffvalue[i] = ' ';
            }
            //strcpy((char *)buffer, (char *)buftmp);
            break;
        case TDATA_STRING64:
            for (int i=0; i<STRLEN_STRING64; i++) {
                if (i<val.length())
                    buffvalue[i] = TextEditAsciiToLcd(val[i]);
                else
                    buffvalue[i] = ' ';
            }
            //strcpy((char *)buffer, (char *)buftmp);
            break;

        case TDATA_FLOAT16:
            d=atof((char*)&val[0]);
            d*=10;
            intpart = (int)d;
            iv.Val=(int16_t)intpart;
            buffvalue[0]=iv.byte.LB;
            buffvalue[1]=iv.byte.HB;
            break;
        case TDATA_ALIM018:
            d=atof((char*)&val[0]);
            d*=boardpowerfactor;// 0.18;
            d*=4096;
            d/=3.3;
            intpart = (int)d;
            iv.Val=(int16_t)intpart;
            buffvalue[0]=iv.byte.LB;
            buffvalue[1]=iv.byte.HB;
            break;
        case TDATA_ALIM050:
            d=atof((char*)&val[0]);
            d*=0.5;
            d*=4096;
            d/=3.3;
            intpart = (int)d;
            iv.Val=(int16_t)intpart;
            buffvalue[0]=iv.byte.LB;
            buffvalue[1]=iv.byte.HB;
            break;
        case TDATA_ALIM100:
            d=atof((char*)&val[0]);
            d*=4096;
            d/=3.3;
            intpart = (int)d;
            iv.Val=(int16_t)intpart;
            buffvalue[0]=iv.byte.LB;
            buffvalue[1]=iv.byte.HB;
            break;
        case TDATA_CH32:
            d=atof((char*)&val[0]);
            d*=4096;
            d/=3.3;
            dv.Val=(int32_t)d;
            buffvalue[0]=dv.byte.LB;
            buffvalue[1]=dv.byte.HB;
            buffvalue[2]=dv.byte.UB;
            buffvalue[3]=dv.byte.MB;
            break;
        case TDATA_ALIMMV:
            d=atof((char*)&val[0]);
            d*=1000;
            intpart = (int)d;
            iv.Val=(int16_t)intpart;
            buffvalue[0]=iv.byte.LB;
            buffvalue[1]=iv.byte.HB;
            break;
        case TDATA_FLOAT:
            d=atof((char*)&val[0]);
            dv.Val=(int32_t)d;
            buffvalue[0]=dv.byte.LB;
            buffvalue[1]=dv.byte.HB;
            buffvalue[2]=dv.byte.UB;
            buffvalue[3]=dv.byte.MB;
            break;
        case TDATA_HOURQUARTER:
            //Esperem un string amb aquest format: HH:MM
            //Els minuts s'aproximaran al quart d'hora m�s proper
            //Si el format no �s l'esperat tornem un 0
            int hora, minut;
            if (val == "63:45") buffvalue[0] = 0xFF;
            else {
                if (val.length() == 5) {
                    hora = (val[0] - '0')*10 + (val[1] - '0');
                    minut = (val[3] - '0')*10 + (val[4] - '0');
                    if ((hora <0) || (hora>24)) hora=0;
                    if ((minut <0) || (minut>59)) minut=0;
                }
                else if ((val.length() == 4) && (val[1] = ':')) {
                    hora = val[0] - '0';
                    minut = (val[2] - '0')*10 + (val[3] - '0');
                    if ((hora <0) || (hora>24)) hora=0;
                    if ((minut <0) || (minut>59)) minut=0;
                }
                else {
                    hora=0;
                    minut=0;
                }
                buffvalue[0] = (unsigned char)hora;
                buffvalue[0] <<= 2;


                if (minut <= 7) c = 0x00;
                else if ((minut > 7) && (minut <= 22)) c = 0x01;
                else if ((minut > 22) && (minut <= 37)) c = 0x02;
                else c = 0x03;
                buffvalue[0] |= c;
            }
            break;

        default:
            for (int i=0; i<MAX_LON_BYTES_CODI; i++) buffvalue[i] = 0;
        break;
    }

    return buffvalue;
}




bool CCodi::SetStringVal(string& val)
{
    switch (tipusData) {
        case TDATA_BITMASK:
            if (val.length()==2 && isHexString(val)){
                // 25/03/14 -> a nibble address in the model has been converted to bitmask, rearrange the value then
                unsigned char nib2 = ascii2nibble(val[0]) << 4;
                nib2 |= ascii2nibble(val[1]);
                val.resize(STRLEN_BITMASK);

                unsigned char c = 0x01;
                for (int j=0; j<8; j++) {
                    if (nib2 & c)
                        val[j] = '1';
                    else
                        val[j] = '0';
                    c <<= 1;
                }
                val[8] = '\0';
            }else if (val.length() > STRLEN_BITMASK){
                val.resize(STRLEN_BITMASK);
            }

            while (val.length() < STRLEN_BITMASK)
                val += '0';

            for (int i=0; i<STRLEN_BITMASK; i++) {
                if ((val[i] != '0') && (val[i] != '1'))
                    val[i] = '0';
            }
            break;
        case TDATA_UINT8:
            if (val.length() > STRLEN_UINT8)
                val.resize(STRLEN_UINT8);

            if (!isNumberString(val,false))
                val = "0";
            break;
        case TDATA_INT8:
            if (val.length() > STRLEN_INT8)
                val.resize(STRLEN_INT8);

            if (!isNumberString(val,true))
                val = "0";
            break;

        case TDATA_MBAR:
        case TDATA_UINT16:
            if (!isNumberString(val,false))
                val = "0";
            break;
        case TDATA_INT16:
            if (!isNumberString(val,true))
                val = "0";
            break;
        case TDATA_INT32:
            if (!isNumberString(val,true))
                val = "0";
            break;
        case TDATA_ALIM018:
        case TDATA_ALIM050:
        case TDATA_ALIM100:
        case TDATA_ALIMMV:
            if (!isNumberString(val,true))  // contains float.
                val = "0";
            break;
        case TDATA_CH32:
        case TDATA_FLOAT16:
        case TDATA_FLOAT:
            if (!isNumberString(val,true))
                val = "0";
            break;
        case TDATA_NIBBLE2:
            if (val.length() > STRLEN_NIBBLE2_1N)
                val.resize(STRLEN_NIBBLE2_1N);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE2) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }

            break;

        case TDATA_NIBBLE4:
            if (val.length() > STRLEN_NIBBLE4)
                val.resize(STRLEN_NIBBLE4);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE4) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }

            break;
        case TDATA_NIBBLE6:
            if (val.length() > STRLEN_NIBBLE6)
                val.resize(STRLEN_NIBBLE6);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE6) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }

            break;

        case TDATA_IPV4:
            break;

        case TDATA_NIBBLE8:
            if (val.length() > STRLEN_NIBBLE8)
                val.resize(STRLEN_NIBBLE8);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE8) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }

            break;
        case TDATA_NIBBLE14:
            if (val.length() > STRLEN_NIBBLE14)
                val.resize(STRLEN_NIBBLE14);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE14) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }
            break;

        case TDATA_NIBBLE16:
            if (val.length() > STRLEN_NIBBLE16)
                val.resize(STRLEN_NIBBLE16);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE16) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }
            break;

        case TDATA_NIBBLE28:
            if (val.length() > STRLEN_NIBBLE28)
                val.resize(STRLEN_NIBBLE28);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE28) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }

            break;

        case TDATA_NIBBLE32:
            if (val.length() > STRLEN_NIBBLE32)
                val.resize(STRLEN_NIBBLE32);

            if (!isHexString(val))
                val = "";

            while (val.length() < STRLEN_NIBBLE32) {
                if (omplirDreta)
                    val += relleno;
                else
                    val = relleno + val;
            }

            break;

        case TDATA_STRING:
            if (val.length() > STRLEN_STRING)
                val.resize(STRLEN_STRING);

            while (val.length() < STRLEN_STRING)
                val += ' ';

            break;
        case TDATA_STRING24:
            if (val.length() > STRLEN_STRING24)
                val.resize(STRLEN_STRING24);

            while (val.length() < STRLEN_STRING24)
                val += ' ';

            break;
        case TDATA_STRING32:
            if (val.length() > STRLEN_STRING32)
                val.resize(STRLEN_STRING32);

            while (val.length() < STRLEN_STRING32)
                val += ' ';

            break;
        case TDATA_STRING64:
            if (val.length() > STRLEN_STRING64)
                val.resize(STRLEN_STRING64);

            while (val.length() < STRLEN_STRING64)
                val += ' ';

            break;
        case TDATA_HOURQUARTER:
            if (val.length() > STRLEN_HOURQUARTER)
                val.resize(STRLEN_HOURQUARTER);

            if (val.find(":") == string::npos)
                val = "00:00";

            break;
    }

    if (this->val != val) {
        canvis = true;
        this->val = val;
    }


    return true;
}


bool CCodi::SetStringIniVal(string& inival){
    this->iniVal=inival;
}


// Ens entren les dades tal com les manipulen les centrals
// i les convertim a un string per guardar-ho
bool CCodi::SetVal(unsigned char *val)
{
    INT16_VAL iv;
    float ff;
    unsigned char buftmp[MAX_LON_STRING_CODI+1];
    unsigned char c;
    int num,i;
    int pos;
    intVal=(int32_t)val[0];         //default for verify purposes.
    if (tipusDataValid) {
        switch (tipusData) {
            case TDATA_BITMASK:
                c = 0x01;
                for (int j=0; j<8; j++) {
                    if (val[0] & c) {
                        buftmp[j] = '1';
                    }
                    else {
                        buftmp[j] = '0';
                    }
                    c <<= 1;
                }
                buftmp[8] = '\0';
                break;

            case TDATA_UINT8:

                num = (int)val[0];

                buftmp[0] = '0' + num / 100;
                num %= 100;
                buftmp[1] = '0' + num / 10;
                num %= 10;
                buftmp[2] = '0' + num;
                buftmp[3] = '\0';

                break;

            case TDATA_INT8:
                iv.v[0]=val[0];
                iv.v[1]=0;
                if (iv.v[0]&0x80)
                    iv.v[1]=0xFF;

                itoa(iv.Val,buftmp,10);
                intVal=(int32_t)iv.Val;
                break;

            case TDATA_MBAR:
            case TDATA_UINT16:
                WORD_VAL wb;
                wb.v[0]=val[0];
                wb.v[1]=val[1];

                intVal=(int32_t)wb.Val;

                buftmp[0] = '0' + wb.Val / 10000;
                wb.Val %= 10000;
                buftmp[1] = '0' + wb.Val / 1000;
                wb.Val %= 1000;
                buftmp[2] = '0' + wb.Val / 100;
                wb.Val %= 100;
                buftmp[3] = '0' + wb.Val / 10;
                wb.Val %= 10;
                buftmp[4] = '0' + wb.Val;
                buftmp[5] = '\0';
                break;
            case TDATA_INT16:
                iv.v[0]=val[0];
                iv.v[1]=val[1];
                itoa(iv.Val,buftmp,10);
                intVal=(int32_t)iv.Val;
                break;
            case TDATA_INT32:
                _INT32_VAL v32;
                v32.v[0]=val[0];
                v32.v[1]=val[1];
                v32.v[2]=val[2];
                v32.v[3]=val[3];
                itoa(v32.Val,buftmp,10);
                break;
            case TDATA_NIBBLE2:
                buftmp[0] = nibble2ascii( (val[0]&0xF0)>>4 );
                buftmp[1] = nibble2ascii( val[0]&0x0F );
                buftmp[2] = '\0';
                break;

            case TDATA_NIBBLE4:
                for (i=0; i<BYTELEN_NIBBLE4; i++) {
                    buftmp[i*2] = nibble2ascii( (val[i]&0xF0)>>4 );
                    buftmp[(i*2)+1] = nibble2ascii( val[i]&0x0F );
                }
                buftmp[STRLEN_NIBBLE4] = '\0';
                break;

            case TDATA_NIBBLE6:
                for (i=0; i<BYTELEN_NIBBLE6; i++) {
                    buftmp[i*2] = nibble2ascii( (val[i]&0xF0)>>4 );
                    buftmp[(i*2)+1] = nibble2ascii( val[i]&0x0F );
                }
                buftmp[STRLEN_NIBBLE6] = '\0';
                break;
            case TDATA_NIBBLE8:
                for (i=0; i<BYTELEN_NIBBLE8; i++) {
                    buftmp[i*2] = nibble2ascii( (val[i]&0xF0)>>4 );
                    buftmp[(i*2)+1] = nibble2ascii( val[i]&0x0F );
                }
                buftmp[STRLEN_NIBBLE8] = '\0';
                break;
            case TDATA_NIBBLE14:
                for (i=0; i<BYTELEN_NIBBLE14; i++) {
                    buftmp[i*2] = nibble2ascii( (val[i]&0xF0)>>4 );
                    buftmp[(i*2)+1] = nibble2ascii( val[i]&0x0F );
                }
                buftmp[STRLEN_NIBBLE14] = '\0';
                break;
            case TDATA_NIBBLE16:
                for (i=0; i<BYTELEN_NIBBLE16; i++) {
                    buftmp[i*2] = nibble2ascii( (val[i]&0xF0)>>4 );
                    buftmp[(i*2)+1] = nibble2ascii( val[i]&0x0F );
                }
                buftmp[STRLEN_NIBBLE16] = '\0';
                break;

            case TDATA_NIBBLE28:
                for (i=0; i<BYTELEN_NIBBLE28; i++) {
                    buftmp[i*2] = nibble2ascii( (val[i]&0xF0)>>4 );
                    buftmp[(i*2)+1] = nibble2ascii( val[i]&0x0F );
                }
                buftmp[STRLEN_NIBBLE28] = '\0';
                break;
            case TDATA_NIBBLE32:
                for (i=0; i<BYTELEN_NIBBLE32; i++) {
                    buftmp[i*2] = nibble2ascii( (val[i]&0xF0)>>4 );
                    buftmp[(i*2)+1] = nibble2ascii( val[i]&0x0F );
                }
                buftmp[STRLEN_NIBBLE32] = '\0';
                break;
            case TDATA_IPV4:
                pos=0;
                for (i=0;i<BYTELEN_IPV4;i++){
                    string dec=iToS(val[i]);
                    for (int j=0;j<dec.size();j++)
                        buftmp[pos++]=(unsigned char)dec[j];
                    if (i<3)
                        buftmp[pos++]='.';
                }
                buftmp[pos]='\0';
                break;
            case TDATA_STRING:
                for (i=0; i<BYTELEN_STRING; i++) {
                    if (val[i]<0x20)
                        val[i]=' ';
                    buftmp[i] = LcdtoLatin1(val[i]);
//                    if (buftmp[i] >= 0xB0) buftmp[i] = ' ';
                }
                buftmp[STRLEN_STRING] = '\0';
                break;
            case TDATA_STRING32:
                for (int i=0; i<BYTELEN_STRING32; i++) {
                    if (val[i]<0x20)
                        val[i]=' ';
                    buftmp[i] = LcdtoLatin1(val[i]);
//                    if (buftmp[i] >= 0xB0) buftmp[i] = ' ';
                }
                buftmp[STRLEN_STRING32] = '\0';
                break;
            case TDATA_STRING24:
                for (int i=0; i<BYTELEN_STRING24; i++) {
                    if (val[i]<0x20)
                        val[i]=' ';
                    buftmp[i] = LcdtoLatin1(val[i]);
                }
                buftmp[STRLEN_STRING24] = '\0';
                break;
            case TDATA_STRING64:
                for (int i=0; i<BYTELEN_STRING64; i++) {
                    if (val[i]<0x20)
                        val[i]=' ';
                    buftmp[i] = LcdtoLatin1(val[i]);
                }
                buftmp[STRLEN_STRING64] = '\0';
                break;
            case TDATA_FLOAT16:
                iv.byte.LB=val[0];
                iv.byte.HB=val[1];
                intVal=(int32_t)iv.Val;
                ff=(float)(iv.Val)/10;
                sprintf(buftmp, "%3.2f", ff);
                break;
            case TDATA_ALIM018:
                iv.byte.LB=val[0];
                iv.byte.HB=val[1];
                intVal=(int32_t)iv.Val;
                ff=(float)((iv.Val*3.3)/(4096*boardpowerfactor/*0.18*/));
                sprintf(buftmp, "%3.2f", ff);
                break;
            case TDATA_ALIM050:
                iv.byte.LB=val[0];
                iv.byte.HB=val[1];
                intVal=(int32_t)iv.Val;
                ff=(float)((iv.Val*3.3)/(4096*0.5));
                sprintf(buftmp, "%3.2f", ff);
                break;
            case TDATA_ALIMMV:
                iv.byte.LB=val[0];
                iv.byte.HB=val[1];
                intVal=iv.Val;
                ff=(float)((iv.Val)/(1000));
                sprintf(buftmp, "%3.2f", ff);
                break;
            case TDATA_ALIM100:
                iv.byte.LB=val[0];
                iv.byte.HB=val[1];
                intVal=(int32_t)iv.Val;
                ff=(float)((iv.Val*3.3)/(4096));
                sprintf(buftmp, "%3.2f", ff);
                break;
            case TDATA_CH32:
                _DWORD_VAL d32;
                d32.v[0]=val[0];
                d32.v[1]=val[1];
                d32.v[2]=val[2];
                d32.v[3]=val[3];
                iv.byte.LB=val[0];
                iv.byte.HB=val[1];
                intVal=iv.Val;
                ff=(float)((d32.Val*3.3)/(4096));
                sprintf(buftmp, "%4.4f", ff);
                break;
            case TDATA_FLOAT:
                _FLOAT_VAL f32;
                f32.v[0]=val[0];
                f32.v[1]=val[1];
                f32.v[2]=val[2];
                f32.v[3]=val[3];
                //intVal=(int32_t)f32.Val;
                sprintf(buftmp, "%4.4f", f32.Val);
                break;
            case TDATA_HOURQUARTER:
                c = (val[0] & 0xFC)>>2;
                int p;
                if (c>=10) {
                    buftmp[0] = '0' + c/10;
                    buftmp[1] = '0' + c % 10;
                    buftmp[2] = ':';
                    p=3;
                } else {
                    buftmp[0] = '0' + c;
                    buftmp[1] = ':';
                    p=2;
                }
                c = val[0] & 0x03;
                if (c==0x00) {
                    buftmp[p] = '0';
                    buftmp[p+1] = '0';
                } else if (c==0x01) {
                    buftmp[p] = '1';
                    buftmp[p+1] = '5';
                } else if (c==0x02) {
                    buftmp[p] = '3';
                    buftmp[p+1] = '0';
                } else {
                    buftmp[p] = '4';
                    buftmp[p+1] = '5';
                }
                buftmp[p+2] = '\0';
                break;

            case TDATA_OUTPUT:
                if (val[0] == 0x00)
                    strcpy((char *)buftmp, MISS_PANEL_OFF);
                else
                    strcpy((char *)buftmp, MISS_PANEL_ON);
                break;

            default:
                return false;
                break;
        }
    }
    else {
        return false;
    }

    string nouVal = (char *)buftmp;
    if (this->val != nouVal) {
        canvis = true;
        this->val = nouVal;
    }

    AssignLength();

    //for (int i=0; i<valLength; i++)
    //    buffvalue[i] = val[i];

    return true;
}

bool CCodi::VerifyVal(unsigned char *val)
{
    unsigned char *tmp = GetVal();
    AssignLength();

    for (int i=0; i<valLength; i++) {
        if (tmp[i] != val[i])
            return false;
    }

    return true;
}

int CCodi::GetValLength()
{
    AssignLength();
    return valLength;
}

void CCodi::AssignLength()
{
    switch (tipusData) {
        case TDATA_BITMASK:
            valLength = BYTELEN_BITMASK;
            break;
        case TDATA_UINT8:
            valLength = BYTELEN_UINT8;
            break;
        case TDATA_INT8:
            valLength = BYTELEN_UINT8;
            break;
        case TDATA_UINT16:
            valLength = BYTELEN_UINT16;
            break;
        case TDATA_INT16:
            valLength = BYTELEN_UINT16;
            break;
        case TDATA_NIBBLE2:
            valLength = BYTELEN_NIBBLE2;
            break;
        case TDATA_NIBBLE4:
            valLength = BYTELEN_NIBBLE4; break;
        case TDATA_NIBBLE6:
            valLength = BYTELEN_NIBBLE6; break;
        case TDATA_NIBBLE8:
            valLength = BYTELEN_NIBBLE8; break;
        case TDATA_NIBBLE14:
            valLength = BYTELEN_NIBBLE14; break;
        case TDATA_NIBBLE16:
            valLength = BYTELEN_NIBBLE16; break;
        case TDATA_NIBBLE28:
            valLength = BYTELEN_NIBBLE28; break;
        case TDATA_NIBBLE32:
            valLength = BYTELEN_NIBBLE32; break;
        case TDATA_IPV4:
            valLength = BYTELEN_IPV4; break;
        case TDATA_STRING:
            valLength = BYTELEN_STRING; break;
        case TDATA_STRING24:
            valLength = BYTELEN_STRING24; break;
        case TDATA_STRING32:
            valLength = BYTELEN_STRING32; break;
        case TDATA_STRING64:
            valLength = BYTELEN_STRING64; break;
        case TDATA_HOURQUARTER:
            valLength = BYTELEN_HOURQUARTER; break;
        case TDATA_OUTPUT:
            valLength = BYTELEN_OUTPUT; break;
        case TDATA_ALIM018:
            valLength = BYTELEN_ALIM018; break;
        case TDATA_ALIM050:
            valLength = BYTELEN_ALIM050; break;
        case TDATA_ALIM100:
            valLength = BYTELEN_ALIM050; break;
        case TDATA_ALIMMV:
            valLength = BYTELEN_ALIMMV; break;
        case TDATA_CH32:
            valLength = BYTELEN_CH32; break;
        case TDATA_FLOAT:
            valLength = BYTELEN_FLOAT; break;
        case TDATA_MBAR:
            valLength = BYTELEN_MBAR; break;
        case TDATA_FLOAT16:
            valLength = BYTELEN_TEMP16; break;
        case TDATA_INT32:
            valLength = BYTELEN_INT32; break;
        default: valLength = 0; break;
    }
}

int CCodi::GetValStringLength()
{
    int strLen=0;
    switch (tipusData) {
        case TDATA_BITMASK:
            strLen = STRLEN_BITMASK; break;
        case TDATA_UINT8:
            strLen = STRLEN_UINT8; break;
        case TDATA_INT8:
            strLen = STRLEN_INT8; break;
        case TDATA_INT16:
            strLen = STRLEN_INT16; break;
        case TDATA_UINT16:
            strLen = STRLEN_UINT16; break;
        case TDATA_NIBBLE2:
            strLen = STRLEN_NIBBLE2; break;
        case TDATA_NIBBLE4:
            strLen = STRLEN_NIBBLE4; break;
        case TDATA_IPV4:
            strLen = STRLEN_IPV4; break;
        case TDATA_NIBBLE6:
            strLen = STRLEN_NIBBLE6; break;
        case TDATA_NIBBLE8:
            strLen = STRLEN_NIBBLE8; break;
        case TDATA_NIBBLE16:
            strLen = STRLEN_NIBBLE16; break;
        case TDATA_NIBBLE28:
            strLen = STRLEN_NIBBLE28; break;
        case TDATA_STRING:
            strLen = STRLEN_STRING; break;
        case TDATA_STRING24:
            strLen = STRLEN_STRING24; break;
        case TDATA_STRING32:
            strLen = STRLEN_STRING32; break;
        case TDATA_STRING64:
            strLen = STRLEN_STRING64; break;
        case TDATA_HOURQUARTER:
            strLen = STRLEN_HOURQUARTER; break;
        case TDATA_OUTPUT:
            strLen = STRLEN_OUTPUT; break;
        case TDATA_ALIM018:
            valLength = STRLEN_ALIM018; break;
        case TDATA_ALIM050:
            valLength = STRLEN_ALIM050; break;
        case TDATA_ALIM100:
            valLength = STRLEN_ALIM050; break;
        case TDATA_ALIMMV:
            valLength = STRLEN_ALIMMV; break;
        case TDATA_CH32:
             valLength = STRLEN_CH32; break;
        case TDATA_FLOAT:
             valLength = STRLEN_FLOAT; break;
        case TDATA_MBAR:
            valLength = STRLEN_MBAR; break;
        case TDATA_FLOAT16:
            valLength = STRLEN_TEMP; break;
        case TDATA_INT32:
            valLength = STRLEN_INT32; break;
        default: strLen = 0; break;
    }

    return strLen;
}
//Assigna valor i el marca com a valor per defecte. Si mes endavant es fa un "Reset"
//agafara aquest valor una altra vegada
bool CCodi::SetDefVal(unsigned char *val)
{
    if (!SetVal(val))
        return false;
    defVal = this->val;
    return true;
}

bool CCodi::SetDefVal(string& val)
{
    if (!SetStringVal(val))
        return false;
    defVal = this->val;
    return true;
}

//Aplica el valor per defecte
bool CCodi::Reset()
{
    val = defVal;
    return true;
}


bool CCodi::GetChanges()
{
    return canvis;
}

int CCodi::GetUpl()
{
    return this->upl;
}

bool CCodi::SetUpl(int up)
{
    this->upl=up;
}

string& CCodi::GetCat()
{
    return this->cat;
}

bool CCodi::SetCat(string& catval)
{
    this->cat=catval;
    return true;
}

 bool CCodi::SetMin(int val)
 {
     vMin=val;
 }
int CCodi::GetMin()
{
    return vMin;
}
bool CCodi::SetMax(int val)
{
    vMax=val;
}
int CCodi::GetMax()
{
    return vMax;
}
bool CCodi::SetMatch(string& m)
{
    vMatch=m;
}
string& CCodi::GetMatch()
{
    return vMatch;
}
bool CCodi::SetWizz(int val)
{
    wizz=val;
}
bool CCodi::SetWizzh(string val)
{
    wizzh=val;
}
bool CCodi::SetWizzl(string val)
{
    wizzl=val;
}
int CCodi::GetWizz()
{
    return wizz;
}
string CCodi::GetWizzh()
{
    return wizzh;
}
string CCodi::GetWizzl()
{
    return wizzl;
}
bool CCodi::SetVerify(int val)
{
    verify=val;
}
int CCodi::GetVerify()
{
    return verify;
}

void CCodi::ClrChanges()
{
    canvis=false;
}

bool CCodi::SetEditable(bool editable)
{
    this->editable = editable;
    return true;
}

bool CCodi::IsEditable()
{
    return editable;
}
