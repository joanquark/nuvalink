#ifndef PANELFLASHGRUP_H
#define PANELFLASHGRUP_H

#include <iostream>
#include <sstream>
#include <string>
using namespace std;

#include <wx/panel.h>
#include <wx/grid.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include "grupflash.h"
#include "panelfitxer.h"

#define _wx(x) (wxString)((string)x).c_str()


class CPanelFlashGrup: public wxPanel
{
    friend class CPanelEditEvt;
    protected:
        CGroupFlash *grupFlash;
        wxGrid *grid;
        wxBoxSizer *vSizer;
        vector<CPanelFile *> panelFitxer;

    public:
        CPanelFlashGrup(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage *lang=0);
        void Clear();
        ~CPanelFlashGrup();
        CLanguage*    Lang;
        bool SetFlashGrup(CGroupFlash *grupFlash);
        CGroupFlash *GetFlashGrup();
        void UnSetFlashGrup();
        bool ApplyValues();
        bool UpdateValues();

    protected:
        void GenComponents();
};

#endif
