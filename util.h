#ifndef UTIL_H
#define UTIL_H

#include <fstream>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <string.h>

#ifndef WX_PRECOMP
	#include <wx/wx.h>
	#include <wx/frame.h>
#else
	#include <wx/wxprec.h>
#endif

using namespace std;

typedef unsigned char BYTE;
typedef uint16_t TICK16;
typedef uint32_t TICK;

typedef struct {
	BYTE b0 :1;
	BYTE b1 :1;
	BYTE b2 :1;
	BYTE b3 :1;
	BYTE b4 :1;
	BYTE b5 :1;
	BYTE b6 :1;
	BYTE b7 :1;
} BYTE_BITS;

typedef union _BYTE_VAL {
	BYTE Val;
	BYTE_BITS bits;
} BYTE_VAL;


typedef union _WORD_VAL
{
    WORD Val;
    unsigned char v[2];
    struct
    {
        unsigned char LB;
        unsigned char HB;
    } byte;
}WORD_VAL;

typedef union _DWORD_VAL {
	float fVal;
	DWORD Val;
	WORD w[2];
	BYTE v[4];
	struct {
		WORD LW;
		WORD HW;
	} word;
	struct {
		BYTE LB;
		BYTE HB;
		BYTE UB;
		BYTE MB;
	} byte;
	struct {
		WORD_VAL low;
		WORD_VAL high;
	} wordUnion;
	struct {
		unsigned char b0 :1;
		unsigned char b1 :1;
		unsigned char b2 :1;
		unsigned char b3 :1;
		unsigned char b4 :1;
		unsigned char b5 :1;
		unsigned char b6 :1;
		unsigned char b7 :1;
		unsigned char b8 :1;
		unsigned char b9 :1;
		unsigned char b10 :1;
		unsigned char b11 :1;
		unsigned char b12 :1;
		unsigned char b13 :1;
		unsigned char b14 :1;
		unsigned char b15 :1;
		unsigned char b16 :1;
		unsigned char b17 :1;
		unsigned char b18 :1;
		unsigned char b19 :1;
		unsigned char b20 :1;
		unsigned char b21 :1;
		unsigned char b22 :1;
		unsigned char b23 :1;
		unsigned char b24 :1;
		unsigned char b25 :1;
		unsigned char b26 :1;
		unsigned char b27 :1;
		unsigned char b28 :1;
		unsigned char b29 :1;
		unsigned char b30 :1;
		unsigned char b31 :1;
	} bits;
} DWORD_VAL;

typedef union _INT16_VAL
{
    int16_t Val;
    unsigned char v[2];
    struct
    {
        unsigned char LB;
        unsigned char HB;
    } byte;
}INT16_VAL;

typedef union _INT32_VAL
{
    int32_t Val;
    unsigned char v[4];
}INT32_VAL;

typedef union _FLOAT_VAL
{
    float Val;
    unsigned char v[4];
}FLOAT_VAL;

typedef union {
	struct {
		BYTE b0;
		BYTE b1;
	} byte;
	BYTE v[2];
	WORD w;
}TArray2;

typedef union {
	struct {
		BYTE b0;
		BYTE b1;
		BYTE b2;
	} byte;
	BYTE v[3];
}TArray3;

typedef union {
	struct {
		BYTE b0;
		BYTE b1;
		BYTE b2;
		BYTE b3;
	} byte;
	BYTE v[4];
}TArray4;

typedef union {
	struct {
		BYTE b0;
		BYTE b1;
		BYTE b2;
		BYTE b3;
		BYTE b4;
		BYTE b5;
		BYTE b6;
		BYTE b7;
	} byte;
	BYTE v[8];
}TArray8;

typedef struct {
    unsigned char hours;
    unsigned char minuts;
    unsigned char seconds;
    unsigned char day;
    unsigned char month;
    unsigned char year;
} TDateTime;

string& getAtrVal(string& atributes, const string &nom);
bool isHexString(string txt);
bool isNumberString(string txt,bool isfloat);
unsigned char ascii2nibble(unsigned char chAscii);
unsigned char nibble2ascii(unsigned char chNibble);
int sToI(string& txt);
string iToS(long int num);
string iToS(int num);
string iToS(unsigned int num);
int hToI(string& txt);
void Push_ChainNibble(unsigned char* pch,int pos,unsigned char ch);
string iToH(unsigned int num);
string iToH(unsigned int num,int len);
//int sToI(string& txt);
bool IsUSBPort(int com);
unsigned char TextEditAsciiToLcd(unsigned char ch);
int getver(string ver);
BOOL ishexchar(char digit);
BOOL isdigitchar(char digit);
BOOL isletterchar(char letter);
unsigned char SwapA (unsigned char A);
char SetMinorCase(char ch);
BOOL isprnchar(char ch);
int StrCmp(char *PChar,char *S);
char LcdtoLatin1(unsigned char ch);
wxString LE2BEHexSN(wxString input);
wxString LE2BEHexAPPID(wxString input);
wxString codify(wxString codein);
wxString uncodify(wxString codein);
int HexChainToAscii(unsigned char* ChainHex,  char* ChainAscii,int num);
int AsciiChainToHex (char* ChainAscii, unsigned char* ChainHex,int num);
string MapEepToFlash(string addeep,bool ram);
unsigned char SwapA (unsigned char A);
int GetWord(unsigned char* hex,int len);
unsigned char Get_Mask(unsigned char ind);
void CalcCRC(unsigned char *trama, int lon, unsigned char *crc);
string CalcAppAes();
wxString UnCipherTag(wxString tag,bool iscipher,bool legacy);
wxString CipherTag(wxString tag,bool legacy);
bool CipherFile(wxString fileName,wxString buffer,bool legacy);
bool UnCipherFile(wxString fileName,wxString fileout,bool legacy);
string NowDate(string lang);
string NowTime(void);
#endif
