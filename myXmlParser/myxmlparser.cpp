#include "myxmlparser.h"
#define _wx(x) (wxString)((string)x).c_str()
#include <iostream>
#include <fstream>
#include <sstream>
#include <wx/filename.h>


myXmlParser::myXmlParser()
{
    //no cal fer res
}

myXmlParser::myXmlParser(XmlNotify &subscriber)
{
    SetSubscriber(subscriber);
}

myXmlParser::~myXmlParser()
{
    //no cal fer res
}

void myXmlParser::SetSubscriber(XmlNotify &subscriber)
{
    this->subscriber = &subscriber;
}

//Funcio parsing mare, desde fitxer xml

bool myXmlParser::parse(wxString fileName/*wxChar *buf, int inici, int fi*/)
{
    wxString name;
    wxString data;
    bool iscipher=false;

    if (!subscriber)
        return false;

    wxFileName filex(fileName);
    if (filex.FileExists()==false)
        return false;

    wxXmlDocument doc;

    doc.Load(fileName,wxT("ISO-8859-1"));
    doc.SetEncoding(wxT("UTF-8"));

//    wxMessageBox(doc.GetVersion());
//    if (doc.GetVersion()=="1.1")
//        iscipher=true;

    wxXmlNode *child;
    wxXmlNode *child2;
    wxXmlNode *child3;
    wxXmlNode *child4;
    wxXmlNode *child5;
    wxXmlNode *child6;
    if (doc.IsOk()){
        try{
            child = doc.GetRoot()->GetChildren();
        }catch (std::string error) {
            return false;
        }
    }else{
        return false;
    }
    while (child){

        name=wxString::From8BitData(child->GetName().c_str());
        data=wxString::From8BitData(child->GetNodeContent());

        child2=child->GetChildren();
        if (child2 && child2->GetName()!="text"){
            subscriber->foundNode(name, child);
            while (child2){
                name=wxString::From8BitData(child2->GetName().c_str());
                data=wxString::From8BitData(child2->GetNodeContent());
                child3=child2->GetChildren();
                if ((child3) && (child3->GetName()!="text")){
                    subscriber->foundNode(name, child2);
                    while (child3){
                        name=wxString::From8BitData(child3->GetName().c_str());
                        data=wxString::From8BitData(child3->GetNodeContent());
                        child4=child3->GetChildren();
                        if ((child4) && (child4->GetName()!="text")){
                            subscriber->foundNode(name, child3);
                            while (child4){
                                name=wxString::From8BitData(child4->GetName().c_str());
                                data=wxString::From8BitData(child4->GetNodeContent());
                                child5=child4->GetChildren();
                                if ((child5) && (child5->GetName()!="text")){
                                    subscriber->foundNode(name,child4);
                                    while (child5){
                                        name=wxString::From8BitData(child5->GetName().c_str());
                                        data=wxString::From8BitData(child5->GetNodeContent());
                                        child6=child5->GetChildren();
                                        if ((child6) && (child6->GetName()!="text")){
                                            subscriber->foundNode(name,child5);
                                            while (child6){
                                                name=wxString::From8BitData(child6->GetName().c_str());
                                                data=wxString::From8BitData(child6->GetNodeContent());
                                                subscriber->foundElement(name,data,child6);
                                                child6=child6->GetNext();
                                            }
                                        }else{
                                            subscriber->foundElement(name,data,child5);
                                        }
                                        child5=child5->GetNext();
                                    }
                                }else{
                                    subscriber->foundElement(name,data,child4);
                                }
                                child4=child4->GetNext();
                            }
                        }else{
                            subscriber->foundElement(name,data,child3);
                        }
                        child3=child3->GetNext();
                    }
                }else{
                    subscriber->foundElement(name,data,child2);
                }
                child2=child2->GetNext();
            }
        }else{
            subscriber->foundElement(name,data,child);
        }
        child=child->GetNext();
    }
    return true;
}

