#ifndef MYXMLPARSER_H
#define MYXMLPARSER_H

/* ***************************************************************************
 *
 * myXmlParser
 * Emili Sapena
 * 15-Mar-2006
 *
//TODO: hi ha un tal TinyXML que sembla senzill de fer servir
 *
 * El meu propi XML parser. Interpreta un buffer de text en format
 * XML i crida a les funcions foundNode i foundElement del "subscriber"
 * per informar cada vegada que troba un Node XML o un element.
 *
 * Un Node es un tag <node> contingut </node> que t� altres nodes o
 * elements en el seu contingut.
 *
 * Un element es un tag <element> contingut </element> que el seu contingut
 * no te cap altre tag a dins.
 *
 * exemple:
 * <node1 atribut1="valor1" atribut2="valor2">
 *      <node2>
 *          <element1 atribut1="valor1">contingut 1</element1>
 *          <element2>contingut 2</element2>
 *          <element3 atribut1="valor1"/>
 *      </node2>
 * </node1>
 *
 *

 *
 * El subscriber ha d'heredar de la classe XmlNotify
 *************************************************************************** */


#include <iostream>
#include <string>
using namespace std;

#include "XmlNotify.h"

class myXmlParser
{
    XmlNotify *subscriber;

    public:
        myXmlParser();
        myXmlParser(XmlNotify &subscriber);
        ~myXmlParser();
        void SetSubscriber(XmlNotify &subcriber);
        bool parse(wxString filename);

};


#endif
