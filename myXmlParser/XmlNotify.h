#if !defined(XmlNotify_H)
#define XmlNotify_H
#include <wx/xml/xml.h>
/////////////////////////////////////////////////////////////////////////////////
// XmlNotify
//
// Purpose:		abstract class used to notify about xml tags found.

class XmlNotify
{
public:


	XmlNotify ()
	{}

	// notify methods
	virtual void foundNode		( wxString & name, wxXmlNode *attributes ) = 0;
	virtual void foundElement	( wxString & name, wxString & value, wxXmlNode *attributes ) = 0;

};

#endif
