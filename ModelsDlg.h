#ifndef MODELSDLG_H
#define MODELSDLG_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(ModelsDlg)
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/choice.h>
	#include <wx/dialog.h>
	//*)
#endif
//(*Headers(ModelsDlg)
#include <wxSpeedButton.h>
//*)

class ModelsDlg: public wxDialog
{
	public:

		ModelsDlg(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize,wxString *model,wxString *version);
		virtual ~ModelsDlg();

		//(*Declarations(ModelsDlg)
		wxStaticText* StaticText2;
		wxStaticText* StaticText1;
		wxSpeedButton* SpeedButton1;
		wxChoice* Choice1;
		wxChoice* Choice2;
		//*)

	protected:

		//(*Identifiers(ModelsDlg)
		static const long ID_STATICTEXT1;
		static const long ID_CHOICE1;
		static const long ID_STATICTEXT2;
		static const long ID_CHOICE2;
		static const long ID_SPEEDBUTTON1;
		//*)

	private:

		//(*Handlers(ModelsDlg)
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		//*)

		wxString *model;
		wxString *version;

	protected:

		void BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size);

		DECLARE_EVENT_TABLE()
};

#endif
