#include "wx_pch.h"
#include "ModelsDlg.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(ModelsDlg)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(ModelsDlg)
//*)

//(*IdInit(ModelsDlg)
const long ModelsDlg::ID_STATICTEXT1 = wxNewId();
const long ModelsDlg::ID_CHOICE1 = wxNewId();
const long ModelsDlg::ID_STATICTEXT2 = wxNewId();
const long ModelsDlg::ID_CHOICE2 = wxNewId();
const long ModelsDlg::ID_SPEEDBUTTON1 = wxNewId();
//*)

BEGIN_EVENT_TABLE(ModelsDlg,wxDialog)
	//(*EventTable(ModelsDlg)
	//*)
END_EVENT_TABLE()

ModelsDlg::ModelsDlg(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	BuildContent(parent,id,pos,size);
}

void ModelsDlg::BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(ModelsDlg)
	wxStaticBoxSizer* StaticBoxSizer1;
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, id, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("id"));
	SetClientSize(wxDefaultSize);
	Move(wxDefaultPosition);
	StaticBoxSizer1 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Model"));
	FlexGridSizer1 = new wxFlexGridSizer(0, 2, 0, 0);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Family"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	FlexGridSizer1->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice1 = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxSize(87,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	Choice1->Append(_("ATS"));
	FlexGridSizer1->Add(Choice1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Model"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	FlexGridSizer1->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice2 = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxSize(87,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
	Choice2->Append(_("Stratus3T"));
	Choice2->Append(_("Nimbus3T"));
	FlexGridSizer1->Add(Choice2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(0,0,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	wxBitmap SpeedButton1_BMP(_("./icons/toolbar/ok.png"), wxBITMAP_TYPE_ANY);
	SpeedButton1 = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, SpeedButton1_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(50,50), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	SpeedButton1->SetUserData(0);
	FlexGridSizer1->Add(SpeedButton1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	StaticBoxSizer1->Add(FlexGridSizer1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(StaticBoxSizer1);
	StaticBoxSizer1->Fit(this);
	StaticBoxSizer1->SetSizeHints(this);

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ModelsDlg::OnSpeedButton1LeftClick);
	//*)
}

ModelsDlg::~ModelsDlg()
{
	//(*Destroy(ModelsDlg)
	//*)
}


void ModelsDlg::OnSpeedButton1LeftClick(wxCommandEvent& event)
{
}
