#include "mlink.h"
#include "panelevents.h"
#define _wx(x) (wxString)((string)x).c_str()

int idCheckAlarm = wxNewId();
//int idCheckPhoneCall = wxNewId();
int idCheckAveria = wxNewId();
int idCheckONOFF = wxNewId();
int idCheckSupervisio = wxNewId();
int idCheckTest = wxNewId();
int idCheckOmisio = wxNewId();
int idCheckInvert = wxNewId();
//int idCheckRebreTot = wxNewId();
int idBotoExportar = wxNewId();
int idBotoBuidar = wxNewId();
#ifdef _FILTER_CLIENT
int idCheckFilter = wxNewId();
int idTextAbonat = wxNewId();
int idTextAbonatTitle = wxNewId();
#endif
int idwxlist = wxNewId();
//int idListSelect = wxNewId();
int idBotoViewBin = wxNewId();
int ID_THUMBNAILEV = wxNewId();

BEGIN_EVENT_TABLE(CPanelEvents, wxPanel)
    EVT_CHECKBOX(idCheckAlarm, CPanelEvents::OnCheckAlarm)
    EVT_CHECKBOX(idCheckAveria, CPanelEvents::OnCheckAveria)
    EVT_CHECKBOX(idCheckONOFF, CPanelEvents::OnCheckONOFF)
    EVT_CHECKBOX(idCheckSupervisio, CPanelEvents::OnCheckSupervisio)
    EVT_CHECKBOX(idCheckTest, CPanelEvents::OnCheckTest)
    EVT_CHECKBOX(idCheckOmisio, CPanelEvents::OnCheckOmisio)
    EVT_CHECKBOX(idCheckInvert, CPanelEvents::OnCheckInvert)
    //EVT_CHECKBOX(idCheckRebreTot, CPanelEvents::OnCheckRebreTot)
    //EVT_CHECKBOX(idCheckPhoneCall, CPanelEvents::OnCheckPhoneCall)
#ifdef _FILTER_CLIENT
    EVT_CHECKBOX(idCheckFilter, CPanelEvents::OnCheckFilter)
#endif
    //EVT_BUTTON(idBotoExportar, CPanelEvents::OnBotoExportar)
    //EVT_BUTTON(idBotoBuidar, CPanelEvents::OnBotoBuidar)

    EVT_BUTTON(idBotoViewBin, CPanelEvents::OnBotoViewBin)
    EVT_LIST_KEY_DOWN(idwxlist, CPanelEvents::OnDeletelist)

//    EVT_LIST_ITEM_SELECTED(idListSelect, CPanelEvents::OnListSelect)

END_EVENT_TABLE()

CPanelEvents::CPanelEvents(Cmlink *mlink,wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CUnLang* lang)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition) && (size==wxDefaultSize)) {
        SetSize(0,0,300,150);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,150);
    }
    UnLang=lang;
    //punters a 0
    llista=0;
    thumbnail=0;
    events=0;
    vSizer = new wxBoxSizer( wxVERTICAL );
    SetSizer( vSizer );
    showAlarm=true;
    showONOFF=true;
    showSupervisio=true;
    showAveria=true;
    showTest=true;
    showOmisio=true;
    //showPhoneCall=true;
#ifdef _FILTER_CLIENT
    Filter=false;
    abonat="";
#endif
    //invert=false;
    this->mlink=mlink;
}

void CPanelEvents::Clear()
{
    if (llista) {
        delete llista;
        llista=0;
    }

    if (thumbnail){
        delete thumbnail;
        thumbnail=0;
    }
    vSizer->Detach(0);
    vSizer->Remove(0);
}

CPanelEvents::~CPanelEvents()
{
    Clear();
    events=0;
    mlink=0;
}

void CPanelEvents::GeneraComponents()
{
    if (events) {
        Clear();

        if (isSecPanel(PanelId)){
            thumbnail = new wxThumbnailCtrl( this, ID_THUMBNAILEV, wxPoint(0,0) , wxSize(300,150), wxSUNKEN_BORDER|wxHSCROLL|wxVSCROLL|wxTH_TEXT_LABEL|wxTH_IMAGE_LABEL|wxTH_EXTENSION_LABEL|wxTH_MULTIPLE_SELECT );
            thumbnail->SetThumbnailImageSize(wxSize(105,105));
        }
        llista = new wxListCtrl(this, idwxlist, wxPoint(0,0),wxSize(300,150), wxLC_REPORT|wxLC_HRULES );

        //OmpleLlista();

        wxBoxSizer *sizerChecks = new wxBoxSizer( wxVERTICAL );
        wxBoxSizer *sizerOpcions = new wxBoxSizer( wxHORIZONTAL );

        checkAlarm = new wxCheckBox(this, idCheckAlarm, UnLang->GetAppMiss("MISS_PANEL_CHK_ALARMS"));
        checkSupervisio = new wxCheckBox(this, idCheckSupervisio, UnLang->GetAppMiss("MISS_PANEL_CHK_SUPERVISIO"));
        checkONOFF = new wxCheckBox(this, idCheckONOFF, UnLang->GetAppMiss("MISS_PANEL_CHK_ONOFF"));
        checkAveria = new wxCheckBox(this, idCheckAveria, UnLang->GetAppMiss("MISS_PANEL_CHK_AVERY"));
        checkTest = new wxCheckBox(this, idCheckTest, UnLang->GetAppMiss("MISS_PANEL_CHK_TEST"));
        checkOmisio = new wxCheckBox(this, idCheckOmisio, UnLang->GetAppMiss("MISS_PANEL_CHK_OMISIO"));
        //checkInvert = new wxCheckBox(this, idCheckInvert, UnLang->GetAppMiss("MISS_PANEL_CHK_INVERT"));
        //checkPhoneCall = new wxCheckBox(this,idCheckPhoneCall, UnLang->GetAppMiss("MISS_PANEL_CHK_PHCALL"));
        //checkRebreTot = new wxCheckBox(this, idCheckRebreTot, UnLang->GetAppMiss("MISS_PANEL_CHK_ALLEVENTS"));

#ifdef _FILTER_CLIENT
        if (events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES) {
            checkFilter = new wxCheckBox(this, idCheckFilter, MISS_PANEL_CHK_FILTER);
            TextAbonat = new wxTextCtrl(this, idTextAbonat, "", wxPoint(0,30) , wxSize(50,20));
        }
#endif
        checkAlarm->SetValue(showAlarm);
        checkAveria->SetValue(showAveria);
        checkONOFF->SetValue(showONOFF);
        checkSupervisio->SetValue(showSupervisio);
        checkTest->SetValue(showTest);
        checkOmisio->SetValue(showOmisio);
        //checkInvert->SetValue(invert);
        //checkPhoneCall->SetValue(showPhoneCall);
        //checkRebreTot->SetValue(events->rebreTots);
#ifdef _FILTER_CLIENT
        if ((events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES)) {
            checkFilter->SetValue(Filter);
        }
#endif
        sizerChecks->Add(checkAlarm);
        sizerChecks->Add(checkAveria);
        sizerChecks->Add(checkONOFF);
        sizerChecks->Add(checkSupervisio);
        sizerChecks->Add(checkTest);
        sizerChecks->Add(checkOmisio);
        //sizerChecks->Add(checkPhoneCall);
#ifdef _FILTER_CLIENT
        if ((events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES)) {
            sizerChecks->Add(checkFilter);
            sizerChecks->Add(TextAbonat);
        }
#endif
        sizerOpcions->Add(sizerChecks);
        //sizerOpcions->Add(checkInvert);
        //sizerOpcions->Add(checkRebreTot, wxSizerFlags(0).Border(wxRIGHT|wxLEFT,5));
//#ifdef _FILTER_CLIENT
//        if ((events->GetTipusEvents() == TIPUS_EVENT_CONTACTID) || (events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES)) {
//            sizerOpcions->Add(TextAbonatTitle);
//            sizerOpcions->Add(TextAbonat);
//        }
//#endif
        wxBitmap Export_BMP(_("./icons/export.png"), wxBITMAP_TYPE_ANY);
        sizerOpcions->Add(new wxSpeedButton(this, idBotoExportar, UnLang->GetAppMiss("MISS_PANEL_EXPORT"), Export_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(60,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1")));
        wxBitmap Clear_BMP(_("./icons/clear.png"), wxBITMAP_TYPE_ANY);
        sizerOpcions->Add(new wxSpeedButton(this, idBotoBuidar, UnLang->GetAppMiss("MISS_PANEL_CLEAR"), Clear_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(60,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1")));
    	Connect(idBotoExportar,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CPanelEvents::OnBotoExportar);
    	Connect(idBotoBuidar,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&CPanelEvents::OnBotoBuidar);

        if (isSecPanel(PanelId)){
            viewButton = new wxBitmapButton(this, idBotoViewBin, wxBitmap(wxImage(wxT("icons/estat/cam_32.png"))), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator);
            sizerOpcions->Add( viewButton );
        }

        vSizer->Add( llista, wxSizerFlags(1).Align(0).Expand());
        if (isSecPanel(PanelId)){
            vSizer->Add( thumbnail ,wxSizerFlags(0).Center().Expand().Border(wxTOP|wxBOTTOM, 3));
        }
        vSizer->Add( sizerOpcions, wxSizerFlags(0).Align(0).Expand().Border(wxALL,5));
        vSizer->Layout();

        OmpleLlista();
    }
}

void CPanelEvents::OmpleLlista()
{
    CurYear="2011";
    int col=0;

    if ((llista) && (events)) {
        llista->ClearAll();
        if (isSecPanel(PanelId)){
            thumbnail->Clear();
        }

        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_DATE"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_TIME"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_CODE"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_TYPE"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_DESCR"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_ZONEUSER"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_AREA"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_CLIENT"));
        if (isSecPanel(PanelId)){
            llista->InsertColumn(col++, "Bin Num");
            llista->InsertColumn(col++, "Bin File");
        }
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_REPORTED"));
/*        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_T1"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_T2"));
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_H"));*/
        llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_GEO"));
  /*      if (events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES) {
            llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_RECVLINE"));
            llista->InsertColumn(col++, UnLang->GetAppMiss("MISS_PANEL_COL_RECVNUMBER"));
        }*/
        int fi = events->GetNumEvents(), i, j=0;
        for ( int rr = 0; rr < fi; rr++ ) {
            /*if (invert==false)*/ i = fi - 1 - rr;
            //else i = rr;

            CEvent *event = events->GetEvent(i);
#ifdef _FILTER_CLIENT
            bool IsAbonat=true;         // Showing all clients
            if ((events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES)) {
                string str1=event->GetAbonat();
                string str2=(string)TextAbonat->GetValue();

                if ( (str1 != str2 ) && (Filter) ){
                    IsAbonat = false;
                }
            }
#endif
            if ( ( (event->IsTipusAlarm() && showAlarm)
                || (event->IsTipusONOFF() && showONOFF)
                || (event->IsTipusSupervisio() && showSupervisio)
                || (event->IsTipusTest() && showTest)
                || (event->IsTipusOmisio() && showOmisio)
                || (event->IsTipusAveria() && showAveria)
                /*|| (event->IsTipusPhoneCall() && showPhoneCall)*/
                || (event->IsTipusOutput() && showSupervisio)
                || (event->IsTipusBus() && showSupervisio))
#ifdef _FILTER_CLIENT
                && ( IsAbonat )
#endif
            ) {
                RefEvent[j]=i;
                long tmp = llista->InsertItem(j, "-", 0);
                llista->SetItemData(tmp, j);
                if (event->IsTipusAlarm()) {
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(0xff,150,120));  // vermell
                    llista->SetItem( item );
                }else if (event->IsTipusBin()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(123,180,206    ));  // blau mar�
                    llista->SetItem( item );
                }else if (event->IsTipusONOFF()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(100,176,40));  // verd
                    llista->SetItem( item );
                }else if (event->IsTipusAveria()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(0xcc,0xcc,0xcc));  // gris
                    llista->SetItem( item );
                }else if ( event->IsTipusOmisio()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(163,123,50));  // marron
                    llista->SetItem( item );
                }

                string text = event->GetData();
                CurYear=&text[6];
                // ------------------------------------------ reported -------------------------
                string reported="";
                if (event->IsCSReported())
                    reported=UnLang->GetAppMiss("MISS_PANEL_CENTRAL_STATION");

                int colreported=8;
                if (isSecPanel(PanelId))
                    reported=10;

                llista->SetItem(j, colreported,_wx(reported));

                llista->SetItem(j, colreported+1,_wx(event->GetFix()));

                llista->SetItem(j, 0, _wx(text));
                text = event->GetHora();
                llista->SetItem(j, 1, _wx(text));
                text = event->GetContactID();
                llista->SetItem(j, 2, _wx(text));
                char buf[8];


                    text = event->GetTipus();
                    llista->SetItem(j, 3, _wx(text));
                    text = event->GetDescr();
                    llista->SetItem(j, 4, _wx(text));
                    unsigned int zona = event->GetZona();
                    sprintf(buf, "%d", zona);
                    text = buf;
#ifdef _ALIAS_ESTAT
//                    if (events->GetTipusEvents()!=TIPUS_EVENT_CONTACTIDEXTES){
                        if (event->IsTipusOutput()){
                            if (zona)   zona--;
                            if (zona<=63){
                                text += " : ";
                                text += COalias[zona];
                            }
                        }
                        else if (event->IsTipusRele()){
                            if (zona)   zona--;
                            if (zona<=63){
                                text += " : ";
                                text += COalias[4+(zona)];
                            }
                        }
                        else if (event->IsTipusONOFF())
                        {
                            if (zona<=63){
                                text += " : ";
                                text += CUalias[zona];
                            }
                        }
                        else if (event->IsTipusBus())
                        {
                            if (zona)   zona--;
                            text = iToS(zona);
                            text += " : ";
                            text += event->GetBusStringID(zona);
                        }
                        else if (zona>=1 && zona<=64)
                        {
                            if (zona)   zona--;
                            text += " : ";
                            text += CZalias[zona];
                        }
 //                   }
#endif
                    llista->SetItem(j, 5, _wx(text));
                    int area = event->GetArea();
                    text = iToS(area);
                    if (area)
                        area--;
#ifdef _ALIAS_ESTAT
                    if (/*(events->GetTipusEvents() != TIPUS_EVENT_CONTACTIDEXTES) && */(area>=0) && (area<4))
                    {
                        text += " : ";
                        text += CAalias[area];
                    }
#endif
                    llista->SetItem(j, 6, _wx(text));

                //llista->SetColumnWidth(6,wxLIST_AUTOSIZE)
                text = event->GetAbonat();
                llista->SetItem(j, 7, _wx(text));
                j++;
            }
        }
        if (fi) {
            llista->SetColumnWidth( 0, wxLIST_AUTOSIZE );
            llista->SetColumnWidth( 1, wxLIST_AUTOSIZE );
            llista->SetColumnWidth( 4, wxLIST_AUTOSIZE );
            llista->SetColumnWidth( 5, wxLIST_AUTOSIZE_USEHEADER);
            llista->SetColumnWidth( 6, wxLIST_AUTOSIZE_USEHEADER);
            if ((events->GetTipusEvents() == TIPUS_EVENT_MSK)/* || (events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES)*/) {
                llista->SetColumnWidth( 7, wxLIST_AUTOSIZE_USEHEADER);
            }
            //llista->Fit();
        }
    }
}

bool CPanelEvents::SetLlista(CLlistaEvents *llistaEvents)
{
    this->events = llistaEvents;
    GeneraComponents();
    return true;
}

bool CPanelEvents::SetGrups(CGrup *grupZAlias,CGrup *grupAAlias, CGrup *grupUAlias, CGrup *grupOAlias)
{

    for (int i=0;i<64;i++)
    {
        string temp=UnLang->GetAppMiss("MISS_PANEL_COL_ZONE").c_str();
        temp+=" ";
        temp+=iToS(i+1);
        CZalias[i]=temp;
    }
    if(grupZAlias!=0)
    {
        for (int i=0; i<grupZAlias->GetNumCodiFills(); i++) {
            this->codi = grupZAlias->GetCodiFill(i);
            CZalias[i]= codi->GetStringVal();
        }
    }

    for (int i=0;i<64;i++){
        string temp =UnLang->GetAppMiss("MISS_PANEL_COL_AREA").c_str();
        temp +=" ";
        temp +=iToS(i+1);
        CAalias[i]=temp;
    }
    if(grupAAlias!=0)
    {
        for (int i=0; i<grupAAlias->GetNumCodiFills(); i++) {
            this->codi = grupAAlias->GetCodiFill(i);
            CAalias[i]= codi->GetStringVal();
        }
    }
    for (int i=0;i<64;i++){
        string temp =UnLang->GetAppMiss("MISS_PANEL_COL_USER").c_str();
        temp +=" ";
        temp +=iToS(i+1);
        CUalias[i]=temp;
    }
    if (grupUAlias!=0)
    {
        if ((grupUAlias->GetNumCodiFills()==9) || (grupUAlias->GetNumCodiFills()==17)){
            // hi ha codi de instalador.
            for (int i=0; i<grupUAlias->GetNumCodiFills(); i++) {
                this->codi = grupUAlias->GetCodiFill(i);
                CUalias[i]= codi->GetStringVal();
            }
        }else{
            CUalias[0]="System";
            // no hi ha codi de instalador.
            for (int i=1,j=0; i<grupUAlias->GetNumCodiFills(); i++,j++) {
                this->codi = grupUAlias->GetCodiFill(j);
                CUalias[i]= codi->GetStringVal();
            }
        }
    }
    CUalias[63]=CUalias[62]="mlink";

    for (int i=0;i<64;i++){
        string temp=UnLang->GetAppMiss("MISS_PANEL_OUTPUT").c_str();
        temp+=" ";
        temp+=iToS(i+1);
        COalias[i]=temp;
    }
    if(grupOAlias!=0)
    {
        for (int i=0; i<grupOAlias->GetNumCodiFills(); i++) {
            this->codi = grupOAlias->GetCodiFill(i);
            COalias[i]= codi->GetStringVal();
        }
    }



    return true;
}

void CPanelEvents::UnSetLlista()
{
    events=0;
    Clear();
}

CLlistaEvents *CPanelEvents::GetLlista()
{
    return events;
}

bool CPanelEvents::AplicaValors()
{
    if (!events)
        return false;

    //No apliquem valors perque o son editables
    return true;
}

//Actualitza els valors per mostrar
//(es el contrari que AplicaValors()
bool CPanelEvents::ActualitzaValors()
{
    if (!events)
        return false;

    OmpleLlista();

    return true;
}

void CPanelEvents::OnCheckAlarm(wxCommandEvent &ev)
{
    showAlarm = checkAlarm->GetValue();
    OmpleLlista();
}

//void CPanelEvents::OnCheckPhoneCall(wxCommandEvent &ev)
//{
//    showPhoneCall = checkPhoneCall->GetValue();
//    OmpleLlista();
//}

void CPanelEvents::OnCheckAveria(wxCommandEvent &ev)
{
    showAveria = checkAveria->GetValue();
    OmpleLlista();
}
void CPanelEvents::OnCheckONOFF(wxCommandEvent &ev)
{
    showONOFF = checkONOFF->GetValue();
    OmpleLlista();
}
void CPanelEvents::OnCheckSupervisio(wxCommandEvent &ev)
{
    showSupervisio = checkSupervisio->GetValue();
    OmpleLlista();
}
void CPanelEvents::OnCheckTest(wxCommandEvent &ev)
{
    showTest = checkTest->GetValue();
    OmpleLlista();
}
void CPanelEvents::OnCheckOmisio(wxCommandEvent &ev)
{
    showOmisio = checkOmisio->GetValue();
    OmpleLlista();
}
void CPanelEvents::OnCheckInvert(wxCommandEvent &ev)
{
//    invert = checkInvert->GetValue();
//    OmpleLlista();
}

#ifdef _FILTER_CLIENT
void CPanelEvents::OnCheckFilter(wxCommandEvent &ev)
{
    Filter = checkFilter->GetValue();
    OmpleLlista();
}
#endif

void CPanelEvents::OnCheckRebreTot(wxCommandEvent &ev)
{
    /*events->rebreTots = checkRebreTot->GetValue();
    if (events->rebreTots) {
        int answer = wxMessageBox(UnLang->GetAppMiss("MISS_PANEL_CHK_ALLEVENTS_SURE"), UnLang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
        if (answer == wxNO) {
            events->rebreTots = false;
            checkRebreTot->SetValue(false);
        }
    }*/
}

// --------------------------- on char -------------------------------------------------------
void CPanelEvents::OnDeletelist(wxListEvent& event)
{

    wxString info;
    string str;
    wxListItem item;
    int filenum;
    long itemIndex=-1;



    //wxMessageBox(_wx(iToS(event.GetKeyCode())));

    if (event.GetKeyCode() == 389){

        while ((itemIndex=llista->GetNextItem(itemIndex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))!=-1){

            //wxMessageBox(_wx(iToS(itemIndex)));
            events->DeleteEvent(RefEvent[itemIndex]);

        }
        OmpleLlista();
        llista->EnsureVisible(itemIndex);
    }
}


void CPanelEvents::OnBotoViewBin(wxCommandEvent &ev)
{

    wxString info;
    string str;
    wxListItem item;
    int filenum;
    long itemIndex=-1;


    if (!isSecPanel(PanelId))
        return;

    //Primer mirem quin �s l'element seleccionat de la llista.
    itemIndex=llista->GetNextItem(itemIndex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED);

    item.m_itemId = itemIndex;
    item.m_col = 8;
    item.m_mask = wxLIST_MASK_TEXT;
    llista->GetItem(item);
    str=item.m_text.c_str();

    filenum=sToI(str);

    item.m_itemId = itemIndex;
    item.m_col = 9;
    item.m_mask = wxLIST_MASK_TEXT;
    llista->GetItem(item);

    if (item.m_text!=""){
        ShellExecute(NULL, "open", item.m_text , NULL, NULL, SW_SHOWNORMAL);
    }else{
//        llista->SetItem(itemIndex, 9, "hola");
        if ((str=mlink->RebreBinFile(filenum))!=""){
            // hem rebut un fitxer, cal que s'actualitzi a la CLlistaEvents i tb a la visual.
            llista->SetItem(itemIndex, 9, _wx(str));

            CEvent *event = events->GetEvent(RefEvent[itemIndex]);
            event->SetBinFile(str);
            events->ModifyEvent(RefEvent[itemIndex],event);

            //ShellExecute(NULL, "open", _wx(str) , NULL, NULL, SW_SHOWNORMAL);

            string text = event->GetData();
            text[6]=CurYear[0];
            text[7]=CurYear[1];
            text[8]=CurYear[2];
            text[9]=CurYear[3];

            thumbnail->Insert(new wxImageThumbnailItem(_wx(event->GetBinFile()),_wx(text)/*_wx(event->GetData())*/,_wx(event->GetHora())));

        }
    }
}

void CPanelEvents::OnBotoExportar(wxCommandEvent &ev)
{
    //Obre finestra de seleccionar fitxers
    wxFileDialog* fileDlg = new wxFileDialog(this, MISS_DIALOG_TIT_EXPORTCSV, "", "", "*.csv", wxSAVE);

    if (fileDlg->ShowModal() == wxID_OK) {
        //TODO: si el fitxer seleccionat ja existeix hauriem de dir-li
        wxString wxNomFitxer = fileDlg->GetFilename();
        wxString wxNomDirectori = fileDlg->GetDirectory();
        string nomFitxer = wxNomDirectori.c_str();
        nomFitxer += '\\';
        nomFitxer += wxNomFitxer.c_str();

        SetCursor(wxCURSOR_WAIT);
        bool result = ExportarEvents(nomFitxer);
        SetCursor(wxNullCursor);
        if (result) {
            wxMessageBox(_(MISS_DIALOG_EXP_EXPORTED), _(MISS_DIALOG_TIT_INFO));
        } else {
            wxMessageBox(_wx(msgError), _(MISS_DIALOG_TIT_ERROR));
        }
    }
}

bool CPanelEvents::ExportarEvents(string& fileName)
{
    ofstream fitxer;
    string buffer;

    fitxer.open( (char *) fileName.c_str(), ifstream::out );
    if (fitxer.good()) {
        GeneraCSVEvents(&buffer);
        fitxer << buffer;
        fitxer.close();
    }
    else {
        msgError = MISS_ERROR_WRITE_FILE + fileName;
        return false;
    }

    return true;
}

bool CPanelEvents::GeneraCSVEvents(string *csv)
{
    string separador = ";";

    *csv = UnLang->GetAppMiss("MISS_PANEL_COL_DATE").c_str() + separador;
    *csv += UnLang->GetAppMiss("MISS_PANEL_COL_TIME").c_str() + separador;
    *csv += UnLang->GetAppMiss("MISS_PANEL_COL_CODE").c_str() + separador;
    *csv += UnLang->GetAppMiss("MISS_PANEL_COL_TYPE").c_str() + separador;
    *csv += UnLang->GetAppMiss("MISS_PANEL_COL_DESCR").c_str() + separador;
    *csv += UnLang->GetAppMiss("MISS_PANEL_COL_ZONEUSER").c_str() + separador;
    *csv += UnLang->GetAppMiss("MISS_PANEL_COL_AREA").c_str() + separador;
    *csv += UnLang->GetAppMiss("MISS_PANEL_COL_CLIENT").c_str() + separador;

/*    if (events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES) {
        *csv += UnLang->GetAppMiss("MISS_PANEL_COL_RECVLINE").c_str() + separador;
        *csv += UnLang->GetAppMiss("MISS_PANEL_COL_RECVNUMBER").c_str() + separador;
    }*/
    *csv += "\n";
    //*csv = "Date\tTime\tCode\tType\tDescription\tZone/User\n";

    int fi = events->GetNumEvents(), i;
    for ( int rr = 0; rr < fi; rr++ ) {
        /*if (invert==false)*/ i = fi - 1 - rr;
        //else i = rr;

        CEvent *event = events->GetEvent(i);

#ifdef _FILTER_CLIENT
        bool IsAbonat=true;         // Showing all clients

        if ((events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES)) {
            string str1=event->GetAbonat();
            string str2=(string)TextAbonat->GetValue();

            if ( (str1 != str2 ) && (Filter) ){
                IsAbonat = false;
            }
        }
#endif

        if ( ((event->IsTipusAlarm() && showAlarm)
            || (event->IsTipusONOFF() && showONOFF)
            || (event->IsTipusSupervisio() && showSupervisio)
            || (event->IsTipusTest() && showTest)
            || (event->IsTipusOmisio() && showOmisio)
            || (event->IsTipusAveria() && showAveria)
            || (event->IsTipusBus() && showSupervisio))
#ifdef _FILTER_CLIENT
            && ( IsAbonat )
#endif
        ){

            *csv += event->GetData() + separador;
            *csv += event->GetHora() + separador;
            *csv += event->GetContactID() + separador;
            *csv += event->GetTipus() + separador;
            *csv += "\"" + event->GetDescr() + "\"" + separador;

            char buf[8];
            int zona = event->GetZona();
            sprintf(buf, "%d", zona);
            *csv += buf + separador;
            *csv += iToS(event->GetArea()) + separador;
            *csv += event->GetAbonat() + separador;

/*            if (events->GetTipusEvents() == TIPUS_EVENT_CONTACTIDEXTES) {
                *csv += iToS(event->GetReceiverLine()) + separador;
                *csv += iToS(event->GetReceiverNumber()) + separador;
            }*/
            *csv += "\n";
        }
    }
    return true;
}


void CPanelEvents::OnBotoBuidar(wxCommandEvent &ev)
{
    wxMessageDialog *msgDlg = new wxMessageDialog(this, UnLang->GetAppMiss("MISS_DIALOG_ASK_CLEAR"),
    UnLang->GetAppMiss("MISS_DIALOG_TIT_CLEAR"), wxYES_NO);

    int result = msgDlg->ShowModal();

    if (result == wxID_YES) {
        events->Clear();
        ActualitzaValors();
    }
}
