#ifndef AGROPANEL_H
#define AGROPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(AgroPanel)
	#include <wx/button.h>
	#include <wx/choice.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	//*)
#endif
//(*Headers(AgroPanel)
#include <wxSpeedButton.h>
//*)

#include "Data.h"
#include "languages/CLanguage.h"
class Cnuvalink;

class AgroPanel: public wxPanel
{
	public:

		AgroPanel(wxWindow* parent,wxWindowID id=wxID_ANY);
		virtual ~AgroPanel();

		//(*Declarations(AgroPanel)
		wxButton* ButtAn1;
		wxButton* ButtAn2;
		wxButton* ButtAn3;
		wxButton* ButtC;
		wxButton* ButtD1;
		wxButton* ButtD2;
		wxButton* ButtI2C;
		wxChoice* Choice1;
		wxChoice* ChoiceAn1;
		wxChoice* ChoiceAn2;
		wxChoice* ChoiceAn3;
		wxChoice* ChoiceC;
		wxChoice* ChoiceD1;
		wxChoice* ChoiceD2;
		wxSpeedButton* SpeedButton1;
		wxStaticText* StaticText10;
		wxStaticText* StaticText11;
		wxStaticText* StaticText12;
		wxStaticText* StaticText13;
		wxStaticText* StaticText14;
		wxStaticText* StaticText15;
		wxStaticText* StaticText16;
		wxStaticText* StaticText17;
		wxStaticText* StaticText18;
		wxStaticText* StaticText19;
		wxStaticText* StaticText1;
		wxStaticText* StaticText20;
		wxStaticText* StaticText21;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxStaticText* StaticText4;
		wxStaticText* StaticText5;
		wxStaticText* StaticText6;
		wxStaticText* StaticText7;
		wxStaticText* StaticText8;
		wxStaticText* StaticText9;
		wxTextCtrl* AliasAn1;
		wxTextCtrl* AliasAn2;
		wxTextCtrl* AliasAn3;
		wxTextCtrl* AliasC;
		wxTextCtrl* AliasD1;
		wxTextCtrl* AliasD2;
		wxTextCtrl* AliasI2C;
		wxTextCtrl* RawAn1;
		wxTextCtrl* RawAn2;
		wxTextCtrl* RawAn3;
		wxTextCtrl* RawC;
		wxTextCtrl* RawD1;
		wxTextCtrl* RawD2;
		wxTextCtrl* RawI2C;
		//*)
        void SetNuvaLink(Cnuvalink* nuvalink,CData* data);

	protected:

		//(*Identifiers(AgroPanel)
		static const long ID_STATICTEXT1;
		static const long ID_CHOICE1;
		static const long ID_STATICTEXT2;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT3;
		static const long ID_TEXTCTRL2;
		static const long ID_BUTTON1;
		static const long ID_STATICTEXT4;
		static const long ID_CHOICE2;
		static const long ID_STATICTEXT5;
		static const long ID_TEXTCTRL3;
		static const long ID_STATICTEXT6;
		static const long ID_TEXTCTRL4;
		static const long ID_BUTTON2;
		static const long ID_STATICTEXT7;
		static const long ID_CHOICE3;
		static const long ID_STATICTEXT8;
		static const long ID_TEXTCTRL5;
		static const long ID_STATICTEXT9;
		static const long ID_TEXTCTRL6;
		static const long ID_BUTTON3;
		static const long ID_STATICTEXT10;
		static const long ID_CHOICE4;
		static const long ID_STATICTEXT11;
		static const long ID_TEXTCTRL7;
		static const long ID_STATICTEXT12;
		static const long ID_TEXTCTRL8;
		static const long ID_BUTTON4;
		static const long ID_STATICTEXT13;
		static const long ID_CHOICE5;
		static const long ID_STATICTEXT14;
		static const long ID_TEXTCTRL9;
		static const long ID_STATICTEXT15;
		static const long ID_TEXTCTRL10;
		static const long ID_BUTTON5;
		static const long ID_STATICTEXT16;
		static const long ID_CHOICE6;
		static const long ID_STATICTEXT17;
		static const long ID_TEXTCTRL11;
		static const long ID_STATICTEXT18;
		static const long ID_TEXTCTRL12;
		static const long ID_BUTTON6;
		static const long ID_STATICTEXT19;
		static const long ID_CHOICE7;
		static const long ID_STATICTEXT20;
		static const long ID_TEXTCTRL13;
		static const long ID_STATICTEXT21;
		static const long ID_TEXTCTRL14;
		static const long ID_BUTTON7;
		static const long ID_SPEEDBUTTON1;
		//*)

	private:
        Cnuvalink* nuvalink;
        CData* dades;

		//(*Handlers(AgroPanel)
		void OnReadAn1Text(wxCommandEvent& event);
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnButtAn1Click(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
