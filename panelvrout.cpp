#include "panelvrout.h"
#include "mlink.h"
#include "mesura.h"

/*BEGIN_EVENT_TABLE(CPanelVROut,wxPanel)
	////Manual Code Start
	////Manual Code End
	EVT_BUTTON(ID_BOTOADD,CPanelVROut::BotoAddClick)
	EVT_BUTTON(ID_BOTODELETE,CPanelVROut::BotoDeleteClick)
	EVT_BUTTON(ID_BOTOREAD,CPanelVROut::BotoReadClick)

END_EVENT_TABLE()*/


CPanelVROut::CPanelVROut(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CLanguage* lang,Cnuvalink* eli,CStatus* est,string pver,string pid)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,150);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,150);
    }

    UnLang=lang;
    //punters a 0
    estat=0;
    grid=0;
    WxPanel1=0;
    vSizer = new wxBoxSizer( wxVERTICAL );
    SetSizer( vSizer );

    mlink=eli;
    PanelVersion=pver;
    PanelId=pid;
    SetEstat(est);
}

void CPanelVROut::Clear()
{
    if (grid) {
        delete grid;
        grid=0;
    }

    vSizer->Detach(0);
}

CPanelVROut::~CPanelVROut()
{
    Clear();
    estat=0;
}

wxPoint& CPanelVROut::VwXSetwxPoint(long x,long y)
{
    m_tmppoint.x=x;
    m_tmppoint.y=y;
    return m_tmppoint;
}

wxSize& CPanelVROut::VwXSetwxSize(long w,long h){
    m_tmpsize.SetWidth(w);
    m_tmpsize.SetHeight(h);
    return m_tmpsize;
}

void CPanelVROut::GeneraComponents()
{
if (estat) {
    if (estat->GetNumSortides()>0) {
        Clear();

        grid = new wxGrid(this, -1, wxPoint(0,0),wxSize(300,150));
        grid->CreateGrid(4, estat->GetNumSortides());
        grid->SetRowLabelValue( 0, UnLang->GetAppMiss("MSG_PAN_ENABLED"));
        grid->SetRowLabelValue( 1, UnLang->GetAppMiss("MSG_PAN_FAILTEST"));
        grid->SetRowLabelValue( 2, UnLang->GetAppMiss("MSG_PAN_LOWBATT"));
        grid->SetRowLabelValue( 3, UnLang->GetAppMiss("MSG_PAN_TAMPER"));

        int v = estat->GetNumSortides() / estat->GetNumSortides(), z=1, j=0;

        for (int i=0; i<estat->GetNumSortides(); i++) {
            wxString text;
            if (estat->IsVROutEnabled(i)) text = "X";
            else text = "-";
            grid->SetCellValue( 0, i, text);
            if (estat->IsVROutFailTest(i)) text = "X";
            else text = "-";
            grid->SetCellValue( 1, i, text);
            if (estat->IsVROutLowBatt(i)) text = "X";
            else text = "-";
            grid->SetCellValue( 2, i, text);
            if (estat->IsVROutTamper(i)) text = "X";
            else text = "-";
            grid->SetCellValue( 3, i, text);
            grid->SetCellAlignment(0, i, wxALIGN_CENTRE, wxALIGN_CENTRE);
            grid->SetCellAlignment(1, i, wxALIGN_CENTRE, wxALIGN_CENTRE);
            grid->SetCellAlignment(2, i, wxALIGN_CENTRE, wxALIGN_CENTRE);
            grid->SetCellAlignment(3, i, wxALIGN_CENTRE, wxALIGN_CENTRE);
            j++;

            if (v==1)
                text = _wx(iToS(z));
            else
                text = wxString::Format("%d.%d", z, j);

            if (j==v) {
                z++;
                j=0;
            }
            grid->SetColLabelValue(i, text);
        }

        grid->AutoSizeColumns();
        //grid->Fit();

        grid->DisableDragGridSize();
        grid->DisableDragColSize();
        grid->DisableDragRowSize();
        grid->EnableEditing(false);

        grid->ForceRefresh();

        }
        vSizer->Add( grid, wxSizerFlags(1).Align(0).Expand());
        vSizer->Layout();
    }

}

bool CPanelVROut::SetEstat(CEstat *estat)
{
    this->estat = estat;
    GeneraComponents();
    return true;
}

void CPanelVROut::UnSetEstat()
{
    estat=0;
    Clear();
}

CEstat *CPanelVROut::GetEstat()
{
    return estat;
}

//Aplica el valor del camp de text al CCodi corresponent
bool CPanelVROut::AplicaValors()
{
    if (!estat)
        return false;

    //No apliquem valors perque o son editables
    return true;
}

//Actualitza els valors del CEstat al camp de text
//(es el contrari que AplicaValors()
bool CPanelVROut::ActualitzaValors()
{
    if (!estat)
        return false;

    //La millor manera d'actualitzar els valors es fent un clear i tornant a generar-ho
    Clear();
    GeneraComponents();

    return true;
}

// -----------------------------------------------------------------------
// jvd code
#ifdef _ALIAS_ESTAT
bool CPanelVROut::SetGrups(CGrup *grupOAlias)
{

    for (int i=0;i<4;i++)
    {
        string temp=MSG_PAN_COL_OUTPUT;
        temp+=" ";
        temp+=iToS(i+1);
        COalias[i]=temp;
    }
    if (grupOAlias!=0)
    {
        for (int i=0; i<grupOAlias->GetNumCodiFills(); i++) {
            this->codi = grupOAlias->GetCodiFill(i);
            COalias[i]= codi->GetStringVal();
        }
    }


   return true;
}
#endif
