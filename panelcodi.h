#ifndef PANELCODI_H
#define PANELCODI_H

#include <iostream>
#include <string>
using namespace std;
#include <wx/panel.h>
#include <wx/choice.h>
#include <wx/bmpbuttn.h>
#include <wxSpeedButton.h>
#include "codi.h"

#define _wx(x) (wxString)((string)x).c_str()

class Cnuvalink;

class CMyObj : public wxObject
{
    public:
        CMyObj();
        ~CMyObj();
        CMyObj (void *in_data);
        void *data;
};

class CPanelCodi: public wxPanel
{
    friend class CPanelEditEvt;
    protected:

        Cnuvalink *nuvalink;
        CCodi *codi;
        wxChoice *choiceh;
        wxChoice *choicel;

        wxTextCtrl *text;
        wxStaticText *label;
        wxStaticText *labelKpA;
        vector<wxCheckBox *> check;
        wxBoxSizer *hSizer, *vSizer;
        wxStaticBoxSizer *hBoxSizer;

        wxBitmapButton* RxButton;
		wxBitmapButton* TxButton;

        //asistente
        wxBitmapButton *wizardButton;
        string versio;
        string model;
        int privlevel;
        unsigned int tzona;
        CLanguage *Lang;

    public:
        CMyObj MyObj;
        wxTextCtrl *m_this;
        wxString currentText;
        CPanelCodi(wxWindow* parent, wxWindowID id = -1,Cnuvalink* nuva=NULL, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage *lang=0);

        void Clear();
        ~CPanelCodi();
        void ChangeText(wxCommandEvent& event);
        void OnButtRx(wxCommandEvent& event);
        void OnButtTx(wxCommandEvent& event);
        void LostFocusText(wxFocusEvent& event);
        //asistente
        void ClickWizardButton(wxCommandEvent& event);
        //bool SetCodi(CCodi *codi);
        bool SetCodi(CCodi *codi, const string &model="", const string &versio="",int privlevel=0,unsigned int tzona=0);
        CCodi *GetCodi();
        void UnSetCodi();
        bool ApplyValues();
        bool UpdateValues();
 //       bool Setnuvalink(Cnuvalink *nuvalink);

    protected:
        void GenComponents(/*AddrRange *Addr*/);

    private:
        DECLARE_EVENT_TABLE();

};
// end CPanelCodi

#endif
