#include "wx_pch.h"
#include "nuvalink.h"
#include "VCOMPanel.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(VCOMPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(VCOMPanel)
#include <wx/bitmap.h>
#include <wx/image.h>
//*)

//(*IdInit(VCOMPanel)
const long VCOMPanel::ID_STATICTEXT4 = wxNewId();
const long VCOMPanel::ID_CHOICECOM = wxNewId();
const long VCOMPanel::ID_STATICTEXT3 = wxNewId();
const long VCOMPanel::ID_TEXTCTRL2 = wxNewId();
const long VCOMPanel::ID_CHECKBOX1 = wxNewId();
const long VCOMPanel::ID_CHECKBOX2 = wxNewId();
const long VCOMPanel::ID_STATICTEXT5 = wxNewId();
const long VCOMPanel::ID_TEXTCTRL3 = wxNewId();
const long VCOMPanel::ID_TEXTCTRL1 = wxNewId();
const long VCOMPanel::ID_STATICTEXT1 = wxNewId();
const long VCOMPanel::ID_LCDWINDOW = wxNewId();
const long VCOMPanel::ID_STATICTEXT2 = wxNewId();
const long VCOMPanel::ID_SPEEDBUTTONCOM = wxNewId();
const long VCOMPanel::ID_SPEEDBUTTON1 = wxNewId();
const long VCOMPanel::ID_SPEEDBUTTON2 = wxNewId();
const long VCOMPanel::ID_BITMAPBUTTON1 = wxNewId();
const long VCOMPanel::ID_BITMAPBUTTON2 = wxNewId();
const long VCOMPanel::ID_TIMERCOM = wxNewId();
//*)

int SOCKETSCOM_ID = wxNewId();


BEGIN_EVENT_TABLE(VCOMPanel,wxPanel)
	//(*EventTable(VCOMPanel)
	//*)
	EVT_SOCKET  (SOCKETSCOM_ID , VCOMPanel::SocketSModemEvent)

END_EVENT_TABLE()

VCOMPanel::VCOMPanel(Cnuvalink *nuva,wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
    this->nuvalink=nuva;
	BuildContent(parent,id,pos,size);
}

void VCOMPanel::BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(VCOMPanel)
	wxBoxSizer* BoxSizer1;
	wxFlexGridSizer* FlexGridSizer1;
	wxStaticBoxSizer* StaticBoxSizer2;
	wxStaticBoxSizer* StaticBoxSizer3;

	Create(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(3, 1, 0, 0);
	StaticBoxSizer3 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Config"));
	StaticText3 = new wxStaticText(this, ID_STATICTEXT4, _("Serial Port"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
	StaticBoxSizer3->Add(StaticText3, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	choiceCOM = new wxChoice(this, ID_CHOICECOM, wxDefaultPosition, wxSize(126,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICECOM"));
	choiceCOM->Append(_("COM1"));
	choiceCOM->Append(_("COM2"));
	choiceCOM->Append(_("COM3"));
	choiceCOM->Append(_("COM4"));
	choiceCOM->Append(_("COM5"));
	choiceCOM->Append(_("COM6"));
	choiceCOM->Append(_("COM7"));
	choiceCOM->Append(_("COM8"));
	choiceCOM->Append(_("COM9"));
	choiceCOM->Append(_("COM10"));
	choiceCOM->Append(_("COM11"));
	choiceCOM->Append(_("COM12"));
	choiceCOM->Append(_("COM13"));
	choiceCOM->Append(_("COM14"));
	choiceCOM->Append(_("COM15"));
	choiceCOM->Append(_("COM16"));
	choiceCOM->Append(_("COM17"));
	choiceCOM->Append(_("COM18"));
	choiceCOM->Append(_("COM19"));
	choiceCOM->Append(_("COM20"));
	StaticBoxSizer3->Add(choiceCOM, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT3, _("TCPServer VCOM"), wxDefaultPosition, wxSize(38,14), 0, _T("ID_STATICTEXT3"));
	StaticBoxSizer3->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TcpServerPort = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	StaticBoxSizer3->Add(TcpServerPort, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	CheckLog = new wxCheckBox(this, ID_CHECKBOX1, _("Loggin Off"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_CHECKBOX1"));
	CheckLog->SetValue(false);
	StaticBoxSizer3->Add(CheckLog, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	CheckTxt = new wxCheckBox(this, ID_CHECKBOX2, _("Text Mode"), wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_CHECKBOX2"));
	CheckTxt->SetValue(false);
	StaticBoxSizer3->Add(CheckTxt, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText4 = new wxStaticText(this, ID_STATICTEXT5, _("send hexdata"), wxDefaultPosition, wxSize(2,14), 0, _T("ID_STATICTEXT5"));
	StaticBoxSizer3->Add(StaticText4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtHex = new wxTextCtrl(this, ID_TEXTCTRL3, wxEmptyString, wxDefaultPosition, wxSize(123,22), 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
	StaticBoxSizer3->Add(TxtHex, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer3, 1, wxALL|wxEXPAND, 5);
	StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Logging"));
	TxtLogCOM = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxSize(697,342), wxTE_AUTO_SCROLL|wxTE_MULTILINE|wxTE_RICH, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	StaticBoxSizer2->Add(TxtLogCOM, 1, wxALL|wxEXPAND, 5);
	FlexGridSizer1->Add(StaticBoxSizer2, 1, wxALL|wxEXPAND, 5);
	BoxSizer1 = new wxBoxSizer(wxHORIZONTAL);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Time Elapsed"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	BoxSizer1->Add(StaticText1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	LcdTime = new wxLCDWindow(this,wxDefaultPosition,wxSize(110,34));
	LcdTime->SetNumberDigits( 5);
	BoxSizer1->Add(LcdTime, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	CurSt = new wxStaticText(this, ID_STATICTEXT2, _("Current Status"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	BoxSizer1->Add(CurSt, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	wxBitmap StartButton_BMP(_("./icons/toolbar/connect_creating.png"), wxBITMAP_TYPE_ANY);
	StartButton = new wxSpeedButton(this, ID_SPEEDBUTTONCOM, wxEmptyString, StartButton_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(30,30), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTONCOM"));
	StartButton->SetMinSize(wxSize(0,0));
	StartButton->SetMaxSize(wxSize(0,0));
	StartButton->SetUserData(0);
	BoxSizer1->Add(StartButton, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	wxBitmap StopButton_BMP(_("./icons/toolbar/connect_no.png"), wxBITMAP_TYPE_ANY);
	StopButton = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, StopButton_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(30,30), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	StopButton->SetMaxSize(wxSize(0,0));
	StopButton->SetUserData(0);
	BoxSizer1->Add(StopButton, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	wxBitmap BtSend_BMP(_("./icons/toolbar/upload.png"), wxBITMAP_TYPE_ANY);
	BtSend = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, BtSend_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(30,30), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	BtSend->SetMaxSize(wxSize(0,0));
	BtSend->SetUserData(0);
	BoxSizer1->Add(BtSend, 1, wxALL, 5);
	BtCh1 = new wxBitmapButton(this, ID_BITMAPBUTTON1, wxBitmap(wxImage(_T(".\\icons\\estat\\ok.png"))), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON1"));
	BtCh1->SetToolTip(_("TEST CH1"));
	BoxSizer1->Add(BtCh1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	BtCh2 = new wxBitmapButton(this, ID_BITMAPBUTTON2, wxBitmap(wxImage(_T(".\\icons\\estat\\ok.png"))), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator, _T("ID_BITMAPBUTTON2"));
	BtCh2->SetToolTip(_("TEST CH2"));
	BoxSizer1->Add(BtCh2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	FlexGridSizer1->Add(BoxSizer1, 1, wxALL|wxEXPAND, 5);
	SetSizer(FlexGridSizer1);
	TimerCtrlCOM.SetOwner(this, ID_TIMERCOM);
	TimerCtrlCOM.Start(15, false);
	FlexGridSizer1->Fit(this);
	FlexGridSizer1->SetSizeHints(this);

	Connect(ID_CHOICECOM,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&VCOMPanel::OnChoiceCOMSelect);
	Connect(ID_CHECKBOX1,wxEVT_COMMAND_CHECKBOX_CLICKED,(wxObjectEventFunction)&VCOMPanel::OnCheckLogClick);
	Connect(ID_SPEEDBUTTONCOM,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&VCOMPanel::OnStartBtLeftClick);
	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&VCOMPanel::OnStopBtLeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&VCOMPanel::OnBtSendLeftClick);
	Connect(ID_BITMAPBUTTON1,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&VCOMPanel::OnBtCh1Click);
	Connect(ID_BITMAPBUTTON2,wxEVT_COMMAND_BUTTON_CLICKED,(wxObjectEventFunction)&VCOMPanel::OnBtCh2Click);
	Connect(ID_TIMERCOM,wxEVT_TIMER,(wxObjectEventFunction)&VCOMPanel::OnTimerCtrlTrigger);
	//*)

	sockets=psocket=0;
	cnt25ms=0;

}


VCOMPanel::~VCOMPanel()
{
	//(*Destroy(VCOMPanel)
	//*)
    TimerCtrlCOM.Stop();

	if (sockets)
        sockets->Destroy();
    if (psocket)
        psocket->Destroy();

    psocket=sockets=0;
    serial.Close();

}

void VCOMPanel:: SetParams(CProtocolNIOT* proto,CConfig* conf)
{
    this->protocolNIOT=proto;
    this->config=conf;

    Start();
}

void VCOMPanel::Start()
{

    if (protocolNIOT->IsOnline()==false){
        TxtLogCOM->AppendText("** Need to be Online before Start Virtual COMPORT **\r\n");
        return;
    }

    TxtLogCOM->AppendText("** DEVICE ONLINE **\r\n");
    string scom=config->GetConnectionParam("vmodemCOM").c_str();
    VCOMCOM=sToI(scom);
    if (VCOMCOM<=0)
        VCOMCOM=1;
    choiceCOM->Select(VCOMCOM-1);

    vconnected=sconnected=hconnected=false;

    wxIPV4address   addr;
    addr.AnyAddress();
    string sport=config->GetParamTcpIp("ipvmodemport").c_str();
    TcpServerPort->SetValue(_wx(sport));
    VCOMTcpPort=sToI(sport);
    addr.Service(VCOMTcpPort);

    if (sockets){
        sockets->Destroy();
        sockets=0;
    }
    try {
        sockets = new wxSocketServer(addr,wxSOCKET_NOWAIT);
    }
    catch (std::string error) {
        error="UNABLE TO CREATE VCOM SOCKET\r\n";
        TxtLogCOM->AppendText(_wx(error));
        return;
    }
    sockets->Discard();
    sockets->SetEventHandler(*this, SOCKETSCOM_ID);
    sockets->SetNotify(wxSOCKET_CONNECTION_FLAG | wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG);
    sockets->Notify(true);

    eLapsed_t=0;
    LcdTime->SetValue(iToS(eLapsed_t));

    if (serial.Open(VCOMCOM,115200,8,1,0,false,true)==false){
        TxtLogCOM->AppendText("COMPORT open failure\r\n");
    }else{
        //serial.SetDTR(true);               // DTR as connected
        //serial.SetRTS(true,0xFF);          // RTS mode mode.
        sconnected=vconnected=true;
    }

    TimerCtrlCOM.Start(10, false);
}
// -------------------------------------------------------------------------
// -------------------- interrupts every 15ms ------------------
void VCOMPanel::OnTimerCtrlTrigger(wxTimerEvent& event)
{
    if (++cnt25ms>=66){
        cnt25ms=0;
        LcdTime->SetValue(iToS(++eLapsed_t));
    }
    char buff[512];
    uint16_t len;
    protocolNIOT->DisThreadPoll(true);
    if (len=serial.ReadData(buff,512)){
        sconnected=true;
        ProcessFrameFromPC(buff,len);
    }else{
            // means the peer is connected.
            protocolNIOT->ProcessRx(10);
            CNIOT tr;
            if (protocolNIOT->SearchAnswer(true)){
                unsigned char* dades=protocolNIOT->GetData(&len);
                if (tr.GetTNIOTFr(protocolNIOT->GetTramaRx())==T_NIOT_PKT_STREAM || tr.GetTNIOTFr(protocolNIOT->GetTramaRx())==T_NIOT_PKT_BUSPROTO){
                    if (sconnected){
                        serial.SendData(dades,len);
                    }
                    if (psocket!=NULL){
                        psocket->Write(dades,len);
                    }
                    if (CheckLog->IsChecked()==false)
                        ShowRawFrame(dades,len,true);
                }else if (tr.GetTNIOTFr(protocolNIOT->GetTramaRx())==T_NIOT_PKT_RPC){
                    if (dades[0]==T_NIOT_RPC_ORDER){
                        if (dades[1]==ORDER_DISCONNECT || dades[1]==ORDER_FINISH_COMM){
                            vconnected=false;
                            serial.SetDTR(false);
                            serial.SetRTS(false,0xFF);
                        }else if (dades[1]==ORDER_CONNECT){
                            vconnected=true;
                            serial.SetDTR(true);
                            serial.SetRTS(true,0xFF);
                        }
                    }
                }
            }
    }
}

void VCOMPanel::SocketSModemEvent(wxSocketEvent &event)
{
    wxSocketBase *tmpsock;
    char buff[512];
    tmpsock = event.GetSocket();

    int i;
    switch (event.GetSocketEvent()){
        case wxSOCKET_CONNECTION:
        {  // When Connecting from VSP3.
            if (hconnected==false){
                hconnected=vconnected=true;
                sconnected=false;
                psocket=sockets->Accept(false);
                psocket->SetEventHandler(*this, SOCKETSCOM_ID);
                psocket->SetNotify( wxSOCKET_INPUT_FLAG | wxSOCKET_LOST_FLAG | wxSOCKET_OUTPUT_FLAG);
                psocket->Notify(true);
                TxtLogCOM->AppendText("***Client TCP Connected***\r\n");
            }
            break;
        }
        case wxSOCKET_INPUT:
        {   // When receiving something from the port
            if (hconnected){
                psocket->Read(buff,512);
                int cnt=psocket->LastCount();
                ProcessFrameFromPC(buff,cnt);
            }
            break;
        }

        case wxSOCKET_LOST:
        {
            if (hconnected){
                psocket->Destroy();
                psocket=0;
                hconnected=false;
                TxtLogCOM->AppendText("***Client Disconnected***\r\n");
            }
            break;
        }
        case wxSOCKET_OUTPUT:
        {
            break;
        }
    }
}


void VCOMPanel::ProcessFrameFromPC(unsigned char* buff, int len)
{
    char endchar;
    if (vconnected){
        // data mode, transparent data to the peer.
        protocolNIOT->SendStreamPkt(buff,len);
        if (CheckLog->IsChecked()==false)
            ShowRawFrame(buff,len,false);
    }
}

void VCOMPanel::ShowRawFrame(unsigned char* frame,int len,bool rx)
{

    if (rx){
        TxtLogCOM->SetDefaultStyle(wxTextAttr(*wxBLUE, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        TxtLogCOM->AppendText(">");
    }else{
        TxtLogCOM->SetDefaultStyle(wxTextAttr(*wxRED, *wxWHITE, wxFont(8,wxFONTFAMILY_DEFAULT,wxFONTSTYLE_NORMAL,wxFONTWEIGHT_BOLD)));
        TxtLogCOM->AppendText(">");
    }
    wxString datafr="";
    if (CheckTxt->IsChecked()){
        for (int i=0; i<len; i++) {
            datafr.Append((wxChar)(frame[i]));
        }
    }else{
        for (int i=0; i<len; i++) {
            datafr.Append((wxChar)nibble2ascii((frame[i]&0xF0)>>4));
            datafr.Append((wxChar)nibble2ascii((frame[i]&0x0F)));
            if (i<len-1){ //*text << ", ";
                datafr.Append(',');
                datafr.Append(' ');
            }
        }
    }
    datafr+="\n";
    TxtLogCOM->AppendText(datafr);
}

void VCOMPanel::OnStartBtLeftClick(wxCommandEvent& event)
{
    Start();
    wxString Schannel=wxGetTextFromUser("deviceCOM","deviceCOM","2");
    string sh=Schannel.c_str();
    int ch=sToI(sh);
    BYTE param[2];
    param[0]=(BYTE)ch;


    protocolNIOT->GenOrderPacket(ORDER_CONNECT,param,1);
    protocolNIOT->SendPacket();
}

void VCOMPanel::OnStopBtLeftClick(wxCommandEvent& event)
{
    TimerCtrlCOM.Stop();
    protocolNIOT->GenOrderPacket(ORDER_DISCONNECT, 0,0);
    protocolNIOT->SendPacket(1,false);              // single try for disconnection.
    serial.Close();
	if (sockets)
        sockets->Destroy();
    if (psocket)
        psocket->Destroy();


    psocket=sockets=0;

}

void VCOMPanel::OnChoiceCOMSelect(wxCommandEvent& event)
{
    VCOMCOM=choiceCOM->GetSelection()+1;
    config->SetParamConexio("vmodemCOM",_wx(iToS(VCOMCOM)));

    string stcpserver=TcpServerPort->GetValue().c_str();
    VCOMTcpPort=sToI(stcpserver);
    config->SetParamConexio("ipvmodemport", _wx(iToS(VCOMTcpPort)));

	//config->Save();

}

void VCOMPanel::OnCheckLogClick(wxCommandEvent& event)
{
    if (CheckLog->IsChecked())
        int jj=0;
}

void VCOMPanel::OnBtSendLeftClick(wxCommandEvent& event)
{
    string val=TxtHex->GetValue().c_str();
    unsigned char raw[256];
    int len=strlen((char*)&val[0]);
    if (len>1){
        AsciiChainToHex((char*)&val[0],raw,len);
        if (vconnected){
            // data mode, transparent data to the peer.
            protocolNIOT->SendStreamPkt(raw,len>>1);
            if (CheckLog->IsChecked()==false)
                ShowRawFrame(raw,len>>1,false);
        }
    }
}

void VCOMPanel::OnBtCh1Click(wxCommandEvent& event)
{
    wxString addcmd=TxtHex->GetValue();
    string saddcmd=addcmd.c_str();
    BYTE add=0x35;
    BYTE cmd=0x52;
    if (addcmd.Length()>=2){
        AsciiChainToHex(&saddcmd[0],&add,2);
    }
    if (addcmd.Length()>=4){
        AsciiChainToHex(&saddcmd[2],&cmd,2);
    }
    nuvalink->SendOrderPacket(ORDER_SDI12_CHECK,0,add,cmd,0x30,0x00,1);
}

void VCOMPanel::OnBtCh2Click(wxCommandEvent& event)
{
    wxString addcmd=TxtHex->GetValue();
    string saddcmd=addcmd.c_str();
    BYTE add=0x35;
    BYTE cmd=0x52;
    if (addcmd.Length()>=2){
        AsciiChainToHex(&saddcmd[0],&add,2);
    }
    if (addcmd.Length()>=4){
        AsciiChainToHex(&saddcmd[2],&cmd,2);
    }
    nuvalink->SendOrderPacket(ORDER_SDI12_CHECK,1,add,cmd,0x30,0x00,1);
}
