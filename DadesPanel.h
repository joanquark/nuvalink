#ifndef DADESPANEL_H
#define DADESPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(DadesPanel)
	#include <wx/choice.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	//*)
#endif
//(*Headers(DadesPanel)
//*)

#include "Data.h"
#include "msgs.h"
#include "util.h"

#define _wx(x) (wxString)((string)x).c_str()


#define NUM_MODEL_FOLDERS  4
const wxString  ModelFolders[NUM_MODEL_FOLDERS]={
    "/models/SPT/",
    "/models/SEC/",
    "/models/LIFT/",
    "/models/TRACK/"
};

const wxString  modelfamily[NUM_MODEL_FOLDERS]={
    "SPT",
    "Panels",
    "Liftel",
    "Track"
};


#define PATRO_FITXERS_MODELS "*-*.xml"

class DadesPanel: public wxPanel
{
	public:

		DadesPanel(wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize,wxString *model=NULL,wxString *ver=NULL,CLanguage *lang=NULL,CData *dades=NULL);
		virtual ~DadesPanel();
        CLanguage* Lang;
        CData* dades;
        //bool SetDades(CData *dades);
        CData *GetData();
        void UnSetDades();
        bool ApplyValues();
        bool UpdateValues();
        void UpdateModels(wxString dir);
        bool ModelExist(wxString name);
        bool UpdateModelVer(wxString model);

		//(*Declarations(DadesPanel)
		wxChoice* BattChoice;
		wxChoice* ChoiceFamily;
		wxChoice* ChoiceModel;
		wxChoice* ChoiceVer;
		wxChoice* ConChoice;
		wxChoice* TypeChoice;
		wxStaticText* StaticText10;
		wxStaticText* StaticText11;
		wxStaticText* StaticText12;
		wxStaticText* StaticText13;
		wxStaticText* StaticText14;
		wxStaticText* StaticText15;
		wxStaticText* StaticText16;
		wxStaticText* StaticText17;
		wxStaticText* StaticText18;
		wxStaticText* StaticText1;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxStaticText* StaticText4;
		wxStaticText* StaticText5;
		wxStaticText* StaticText6;
		wxStaticText* StaticText7;
		wxStaticText* StaticText8;
		wxStaticText* StaticText9;
		wxTextCtrl* ExtPTxt;
		wxTextCtrl* TextRule;
		wxTextCtrl* TxtCSDPh;
		wxTextCtrl* TxtCusAdd;
		wxTextCtrl* TxtCusName;
		wxTextCtrl* TxtCusPh;
		wxTextCtrl* TxtIP;
		wxTextCtrl* TxtNom;
		wxTextCtrl* TxtPhModem;
		wxTextCtrl* TxtPort;
		wxTextCtrl* TxtSimSolution;
		wxTextCtrl* Txtemail;
		//*)

	protected:

		//(*Identifiers(DadesPanel)
		static const long ID_STATICTEXT1;
		static const long ID_CHOICE1;
		static const long ID_STATICTEXT13;
		static const long ID_CHOICE4;
		static const long ID_STATICTEXT14;
		static const long ID_CHOICE5;
		static const long ID_STATICTEXT3;
		static const long ID_CHOICE2;
		static const long ID_STATICTEXT15;
		static const long ID_TEXTCTRL10;
		static const long ID_STATICTEXT16;
		static const long ID_CHOICE6;
		static const long ID_STATICTEXT17;
		static const long ID_TEXTCTRL11;
		static const long ID_STATICTEXT2;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT4;
		static const long ID_CHOICE3;
		static const long ID_STATICTEXT5;
		static const long ID_TEXTCTRL2;
		static const long ID_STATICTEXT6;
		static const long ID_TEXTCTRL3;
		static const long ID_STATICTEXT7;
		static const long ID_TEXTCTRL4;
		static const long ID_STATICTEXT8;
		static const long ID_TEXTCTRL5;
		static const long ID_STATICTEXT18;
		static const long ID_TEXTCTRL12;
		static const long ID_STATICTEXT9;
		static const long ID_TEXTCTRL6;
		static const long ID_STATICTEXT10;
		static const long ID_TEXTCTRL7;
		static const long ID_STATICTEXT11;
		static const long ID_TEXTCTRL8;
		static const long ID_STATICTEXT12;
		static const long ID_TEXTCTRL9;
		//*)

	private:

		//(*Handlers(DadesPanel)
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnChoiceFamilySelect(wxCommandEvent& event);
		void OnChoiceModelSelect(wxCommandEvent& event);
		void OnChoiceVerSelect(wxCommandEvent& event);
		//*)
		wxString* model;
		wxString* versio;
        wxArrayString llistaModels, DirList;

	protected:

		void BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size);

		DECLARE_EVENT_TABLE()
};

#endif
