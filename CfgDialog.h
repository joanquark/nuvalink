#ifndef CFGDIALOG_H
#define CFGDIALOG_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(CfgDialog)
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	#include <wx/panel.h>
	#include <wx/choice.h>
	#include <wx/dialog.h>
	//*)
#endif
//(*Headers(CfgDialog)
#include <wx/notebook.h>
#include <wxSpeedButton.h>
//*)

#include "config.h"

class CfgDialog: public wxDialog
{
	public:

		CfgDialog(wxWindow* parent);
		virtual ~CfgDialog();
		void SetCfg(CConfig* con);

		//(*Declarations(CfgDialog)
		wxStaticText* StaticText9;
		wxNotebook* Notebook1;
		wxStaticText* StaticText2;
		wxChoice* Choice3;
		wxStaticText* StaticText6;
		wxStaticText* StaticText8;
		wxPanel* Panel1;
		wxStaticText* StaticText1;
		wxTextCtrl* TxtPortPRN;
		wxStaticText* StaticText3;
		wxSpeedButton* SpeedButton2;
		wxStaticText* StaticText7;
		wxSpeedButton* SpeedButton1;
		wxTextCtrl* TxtIPCloud;
		wxPanel* Panel2;
		wxChoice* Choice1;
		wxTextCtrl* TxtIPPRN;
		wxTextCtrl* TxtPortCloud;
		wxChoice* Choice2;
		//*)

	protected:

		//(*Identifiers(CfgDialog)
		static const long ID_STATICTEXT1;
		static const long ID_CHOICE1;
		static const long ID_STATICTEXT2;
		static const long ID_CHOICE2;
		static const long ID_STATICTEXT3;
		static const long ID_CHOICE3;
		static const long ID_PANEL1;
		static const long ID_STATICTEXT6;
		static const long ID_TEXTCTRL3;
		static const long ID_STATICTEXT7;
		static const long ID_TEXTCTRL4;
		static const long ID_STATICTEXT8;
		static const long ID_TEXTCTRL5;
		static const long ID_STATICTEXT9;
		static const long ID_TEXTCTRL6;
		static const long ID_PANEL2;
		static const long ID_NOTEBOOK1;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		//*)

	private:

		//(*Handlers(CfgDialog)
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnSpeedButton2LeftClick(wxCommandEvent& event);
		//*)
        CConfig* config;
	protected:

		void BuildContent(wxWindow* parent);

		DECLARE_EVENT_TABLE()
};

#endif
