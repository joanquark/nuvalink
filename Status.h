#ifndef ESTAT_H
#define ESTAT_H

#include <iostream>
#include <string>
#include <vector>
using namespace std;
#include "devices.h"
#include "mesura.h"
#include "codi.h"
#include "event.h"


enum{
    ID_ESTAT_ALL    =   0,
    ID_ESTAT_INPUT  =   1,
    ID_ESTAT_OUTPUT =   2,
    ID_ESTAT_AREAS  =   3,
    ID_ESTAT_SYS =   4,
};
// ---------------------------- --- Input States -----------------------
typedef struct{
	WORD 	Status;			// 930 standard z status
	BYTE	MStatus;		// 932 Memory Status
	BYTE	IOStatus;		// 933 BusIO Status.
	WORD	TimerD;			// 934 Detection timer.
	WORD 	TimerS;			// 936 Supervision timer.
	int16_t Analog;			// 938 Main analog Detection value
	/* In case of pir this will be the PYRO voltage,or CN input voltage, level, Temperature will be sent as second channel (another zone)*/
	BYTE 	Voltage;		// 93A Voltage Level
	BYTE 	rssi;			// 93B rssi.
}TInSt;						// 12 byte long


// Status Z definition
enum{
	M_IN_DETECT			=	0x0001,
	M_IN_ALARM			=	0x0002,
	M_IN_MEMALARM		=	0x0004,
	M_IN_DELAY			= 	0x0008,
	M_IN_BYPASS			=	0x0010,
	M_IN_FAILURE		=	0x0020,
	M_IN_MASKING		= 	0x0040,
	M_IN_TAMPER			= 	0x0080,

	M_IN_CNT_AUTBY		=	0x0700,

		INC_CNT_AUTBY		= 	0x0100,
		CNT_AUTBY			=  	0x0500,		// this are 5 times, but in its position of the counter.

	M_IN_AUTOBYPASS		=	0x0800,
	M_IN_RES1000		= 	0x1000,
	M_IN_RES2000		=  	0x2000,
	M_IN_RES4000		= 	0x4000,
	M_IN_ARM			=	0x8000
};
#define 	StatusZ(z)		Msk.InSt[z].Status1
#define		IsPhyDet(z)		Msk.InSt[z].TimerD
#define  	ClrPhyDet(z)	Msk.InSt[z].TimerD=0
#define		SetPhyDet(z)	Msk.InSt[z].TimerD=0xFFFF
#define  	ClrDetZ(z)		Msk.InSt[z].Status1&=~M_IN_DETECT

// ------------------------------------------------ BusIO Status ----------------------------
enum{
	M_STIN_DETECT			= 0x01,
	M_STIN_TAMPER			= 0x02,
	M_STIN_LOWBATT			= 0x04,
	M_STIN_FAILTEST			= 0x08,
	M_STIN_SELFTESTFAIL		= 0x10,
	M_STIN_FAILURE			= 0x20,
	M_STIN_MASKING			= 0x40,
	M_STIN_SETUP			= 0x80,
};
// -------------------------------------------------------------- OuputStatus -----------
typedef struct{
	BYTE		IOStatus;	// C30
	BYTE		Event;		// C31 PGM ouput event status.
	WORD 		TimerT;		// C32 Output timer
	WORD		TimerS;		// C34 Supervision timer
	BYTE		Voltage;	// C36 voltage Level for BUSIO powered output, or dutty cycle.
	BYTE		rssi;		// C37 Signal RSSI.
}TOSt;		// 8 BYTE long

enum{
	M_STOUT_ON				= 0x01,		// activated deactivated
	M_STOUT_TAMPER			= 0x02,
	M_STOUT_LOWBATT			= 0x04,
	M_STOUT_FAILTEST		= 0x08,
	M_STOUT_FAILSEFLTEST	= 0x10,
	M_STOUT_FAILURE			= 0x20,
	M_STOUT_SILENT			= 0x40,
	M_STOUT_SETUP			= 0x80,		// there is related Bus Output.

};
// ----------------------------------------------------- AreaSt --------------------
typedef struct{
	BYTE 		Status;
	BYTE		Status1;
	WORD_VAL	Timer;
}TASt;		// 4 BYTE Long

enum{
	M_AREA_ENABLED		= 0x01,
	M_AREA_ARM			= 0x02,
	M_AREA_NIGHT		= 0x04,
	M_AREA_PERIMETER 	= 0x08,
	M_AREA_IN			= 0x10,
	M_AREA_OUT			= 0x20,
};
// ---------------------------------------------------------------------------------
// this structure has a own flash sector to be recorded to.
typedef struct{
	BYTE	    PowerFail;				// failure status
	BYTE  	    ComFail;
	BYTE  	    ExtraSign;
	BYTE  	    ZoneFail;

	BYTE  	    PowerFailAck;			// failures acknowledged
	BYTE 	    ComFailAck;
	BYTE	    ExtraSignAck;
	BYTE	    ZoneFailAck;

	BYTE  	    PowerFailBy;			// failures bypassed
	BYTE 	    ComFailBy;
	BYTE	    ExtraSignBy;
	BYTE	    ZoneFailBy;

	BYTE	    PowerFailEn;			// These are the enabled systems.
	BYTE  	    ComFailEn;
	BYTE  	    ExtraSignEn;
	BYTE  	    ZoneEn;					// This keeps how many zones are enabled.

	BYTE  	    PowerFailSt;			// s'hi guarda l'estat en temps real.
	BYTE 	    ComFailSt;
	BYTE    	ExtraSignSt;
	BYTE    	ZoneSt;

	BYTE	    resSysInd[10];
	WORD        CheckSum;

}TSysInd;		// 32 byte

 // M�scaras del StPanel
enum{
	M_STPANEL_MEM_ALARM	   	=	0x01,
	M_STPANEL_PREFIRE      	=   0x02,
	M_STPANEL_ERROR_TIME   	=   0x04,
	M_STPANEL_FAIL_ALARMTX 	=   0x08,
	M_STPANEL_LEVEL3DIS	   	=	0x10,
	M_STPANEL_LEVEL4DIS    	=   0x20,
	M_STPANEL_ACFAIL		= 	0x40,
	M_STPANEL_LOWBATT		=	0x80
};

typedef struct{
	DWORD		PPE;				// CB0 0 P.Push Ev ( 32 bits queue )
	DWORD		PDE;				// CB4  P.Display Ev ( 32 bit queue )
	DWORD       PRE1;				// CB8 .Report Ev	 ( 32 bit queue )
	DWORD       PRE2;				// CBC P.Report Ev	 ( 32 bit queue )
	DWORD       PRE3;				// CC0 P.Report Ev	 ( 32 bit queue )
	BYTE_VAL	StAreas;			// CC4
	BYTE_VAL	StPanel;			// CC5 Estado de panel ( clave alarma )
	TDateTime	dt;					// CC6 Datetime , can be UTC / GSM / User set
	WORD        TZBatt;				// CCC contador bater�a baja , tiempo en segundos.
	WORD		TimerReplaceBatt;	// CCE
	WORD        TZAC;				// CD0
	WORD		BattLevel;			// CD2 conversion AD 12 bits, reducci�n canal bater�a
	WORD		AcLevel;			// CD4 conversion AD 12bits reducci�n canal alimentaci�n externa.
	int16_t		BoardTemp;			// CD6 Board CPU temperature. Conversion de STM32F0 or si7020
	WORD		TReportTest;		// CD8
	BYTE_VAL	SysHard;			// CDA
	BYTE		FwVerH;				// CDB
	BYTE		FwVerL;				// CDC
	BYTE		BoardVer;			// CDD
	BYTE 		TDevice;			// CDE
	BYTE		FlashID;			// CDF
}TSysSt;			// 48 BYTE;

// ----------------------------------------------- TGsmSt ----------------
typedef struct{
	BYTE 	    NetSt;				// CE0
	BYTE 	    rssi;				// CE1
	BYTE	    Coverture;			// CE2
	BYTE 	    DevInf;            	// CE3 Device info = modem model.
	BYTE 	    ntSmsSent24h;		// CE4 can be reused
	BYTE_VAL	FGsmSt;			// CE5 GsmSt Flags.
	BYTE	    res;				// CE6
	BYTE 	    CntPhoneCalls;		// CE7 Comptador de events call Outgoing.
	TArray8	    CIMI;				// CE8 CIMI/IMEI to be read from APP.
}TGsmSt;		// 16 BYTE
// ---------------------------------------------------- Estructura de RamNv para IP COM -------------

typedef struct{
	BYTE 	StIP;						// CF0
	BYTE 	StIP1;						// CF1
	BYTE 	StIP2;						// CF2
	BYTE 	AddTestIP;					// CF3
	WORD 	TTestIP;					// CF4
	TArray4 DestIP;						// CF6
	TArray4 GprsIP;						// CFA
	BYTE 	TimeOutSDoTestIP;			// CFE
	BYTE 	EthSt;				        // CFF
	BYTE 	Wifirssi;					// D00
	BYTE 	LanPingError;				// D01
	TArray4 LanIP;						// D02
	WORD 	DestPort;					// D06
	WORD 	TSocket;					// D08
	TArray2 RollCode;					// D0A
	BYTE 	resIPSt[32-28];				// D0C
}TIPSt;

#define MAC_OK                  0x0
#define MAC_LINKED              0x1
#define MAC_RESTART             0xFE
#define MAX_ERROR               0xFF

	#define OFF_ROLLCODE_IP          10
	#define M_STIP_GPRSON            0x01
	#define M_STIP_GPRSERROR         0x02
	#define M_STIP_ISETH    		 0x04
	#define M_STIP_ETHERROR	         0x08
	#define M_STIP_ISWIFI            0x10
	#define M_STIP_WIFIERROR         0x20

     #define AND_MASK_STIP_AT_STARTUP    0xD8     // conservem LINKFAIL,LINKFAIL2,GPRSERROR,MACLINKED

#define Is_GPRSOn()                   	(Msk.IPSt.StIP&M_STIP_GPRSON)
   #define Set_GPRSOn()              	Msk.IPSt.StIP|=M_STIP_GPRSON
   #define Clr_GPRSOn()              	Msk.IPSt.StIP&=~(M_STIP_GPRSON)

#define Set_IPST_ServerSocket()        	Msk.IPSt.StIP|=M_STIP_SERVERGPRS
   #define Clr_IPST_ServerSocket()     	Msk.IPSt.StIP&=~M_STIP_SERVERGPRS
   #define Set_IPST_ClientSocket()    	Msk.IPSt.StIP&=~M_STIP_SERVERGPRS
   #define Is_IPST_ServerSocket()      	(Msk.IPSt.StIP&M_STIP_SERVERGPRS)

#define Is_EthPingError()				(Msk.IPSt.EthPingError>2)

#define Set_IPST_IsETH()				Msk.IPSt.StIP|=M_STIP_ISETH
	#define Clr_IPST_IsETH()			Msk.IPSt.StIP&=~M_STIP_ISETH
	#define Is_IPST_IsETH()				(Msk.IPSt.StIP&M_STIP_ISETH)

#define Set_IPST_LinkFailure()         	Msk.IPSt.StIP|=M_STIP_LINKFAIL
   #define Clr_IPST_LinkFailure()      	Msk.IPSt.StIP&=~(M_STIP_LINKFAIL|M_STIP_LINKFAIL2)
   #define Is_IPST_LinkFailure()       	(Msk.IPSt.StIP&M_STIP_LINKFAIL)

#define Set_IPST_LinkFailure2()         Msk.IPSt.StIP|=(M_STIP_LINKFAIL|M_STIP_LINKFAIL2)
   #define Is_IPST_LinkFailure2()       (Msk.IPSt.StIP&M_STIP_LINKFAIL2)

	#define M_STIP_TOTALLINKFAILURE 			(M_STIP_LINKFAIL|M_STIP_LINKFAIL2)
	#define Is_IPST_CompleteLinkFailure()	((Msk.IPSt.StIP&M_STIP_TOTALLINKFAILURE)==M_STIP_TOTALLINKFAILURE)


// ----------------------------------------------------- TFIX define --------
typedef union{
	DWORD	ival;
	float	fval;
}TCoord;

typedef struct{
	TCoord		Lat;			// D10
	TCoord		Long;			// D14
	BYTE		Satellite;		// D18	Sensor : Satellites
	BYTE 		AvSNR;			// D19	AvSNR
	WORD 		Height;			// D1A
	WORD		Speed;			// D1C
	WORD		TAvSearchSat;	// D1E
}TGpsSt;

// -------------------------------------------------------- TDyn ---- Dynamic state ---------
typedef enum{
	ST_DYN_STOP				= 0,
	ST_DYN_STORAGE 			= 1,
	ST_DYN_MOV				= 2,
	ST_DYN_FLIGHT_ACC 		= 3,
	ST_DYN_FLIGHT		 	= 4,
};

#define T_LIVE_FINDNET			20000
#define T_LIVE_FINDNET_MOV		25000
#define T_LIVE_FINDNET_MOVROAM	120000
#define T_LIVE_ST				30000
#define T_LIVE_LONG				50000

#define T_TIMEOUT_MOV_GPS	600			// 10 minuts at 1s.
#define T_TIMEOUT_MOV_ACC	300			// 5 minuts poll at 1s.

typedef struct{
	int16_t		AccX;			// D20
	int16_t		AccY;			// D22
	int16_t		AccZ;			// D24
	int16_t		MAccX;			// D26
	int16_t		MAccY;			// D28
	int16_t		MAccZ;			// D2A
	BYTE		MShock;			// D2C
	BYTE		DynSt;			// D2D
	BYTE 		res1;			// D2E
	BYTE		res2;			// D2F
	TICK16 		TMov;			// D30
	TICK16		TFlight;		// D32
	TICK		TShip;			// D34 Ship time.
	TICK		TOnSt;			// D38 measures time On Stop , MOV or Flight
	BYTE		GeoFenceEntryEv;// D3C
	BYTE		GeoFenceExitEv;	// D3D
	BYTE 		res3;			// D3E
	BYTE		res4;			// D3F
}TDyn;				// 32 byte

typedef struct{
	TSysInd				SysInd;
	TSysSt				SysSt;
	TGsmSt				GsmSt;
	TIPSt				IPSt;
	TGpsSt				GpsSt;
	TDyn				Dyn;
}TSTSYS;

// --------------------------- Beacon struct ------------------------------
typedef struct{				// 16 bytes long.
	BYTE	Mac[6];			// BLE MAC
	//BYTE	Name[4];		// +B05+ keep small name for integration in bidi software.
	WORD	TTL;			// Tick from last validation in minutes.
	TICK16	TLastSignal;	// keeps ms from last signal.
	int16_t acc;
	int8_t 	rssi[2];		// +B04+ track latest 3 rssi signals, to see if there is continuous variation ( so movement track )
	BYTE	opt;
}TBleDevSt;

enum{
	M_BEACON_BUTTON1		=	0x01,
	M_BEACON_BUTTON2		=	0x02,
	M_BEACON_BUTTON3		=	0x03,
	M_BEACON_BUTTON_PANIC	=	0x08,
	M_BEACON_MOVEMENT		=	0x10,
	M_BEACON_LOWBATT		=	0x20,
	M_BEACON_ONSCENE		=	0x80
};


class CStatus
{
    TDateTime dateTime;
    int numZones, numUsers, numAreas, punterEvents, numSortides;
    int numOuts;
    float vdd;
    CCodi cobertura;
    bool hasGsmRssi;
    bool hasGeneral, hasOuts, hasAnalog, hasWireless, hasWirelessUser,hasWirelessOut, hasReportTest, hasPics, hasBeacons;

        string StEeprom[5];

        int StMemLen[5];

    public:

        TInSt   InSt[256];
        TOSt    OSt[64];
        TASt    ASt[64];
        TSTSYS  STSYS;


        string curver;
        string curverdate;
        string curmodel;
        string curhard;
        unsigned char busadd;
        string version;
        string source;

        wxString code;
        int mode;
        int intcobertura;              // in % value
        bool isPSTN,isETH;

        CStatus(unsigned char TPanel=TPANEL_CIRRUS4T);
        void Clear();
        ~CStatus();

        bool parseDevStPkt(unsigned char* data,int remaining);
        bool UpdateFromRawMem(unsigned char* data,int id);
        WORD GetAreaSt(int area);
        bool SetStatus();
        bool SetNumZ(int numZones);
        int GetNumZones();
        bool SetNumUsers(int numUsers);
        int GetNumUsers();
        bool SetNumAreas(int arees);
        int GetNumAreas();
        bool SetNumOuts(int sortides);
        int GetNumSortides();
        WORD GetOutSt(int out);
        bool SetPunterEvents(int punter);
        int GetPunterEvents();

        bool SetDetectZ(int zona);
        bool UnSetDetectZ(int zona);
        bool SetDetectZ(unsigned char detect[2]);
        void GetDetectZ(unsigned char *detectZ);

        bool SetAlarmZ(int zona);
        bool UnSetAlarmZ(int zona);
        bool SetAlarmZ(unsigned char alarm[2]);
        void GetAlarmZ(unsigned char *alarmZ);

        bool SetOmitZ(int zona);
        bool UnSetOmitZ(int zona);
        bool SetOmitZ(unsigned char omit[2]);
        void GetOmitZ(unsigned char *omitZ);

        bool SetAveriaZ(int zona);
        bool UnSetAveriaZ(int zona);
        bool SetAveriaZ(unsigned char averia[2]);
        bool GetAveriaZ(unsigned char *averiaZ);

        bool SetAreaSt(WORD AreaState);
        bool SetStPanel(unsigned char StPanel);
        unsigned char GetStPanel();

        bool SetStEeprom(int seccio,string &val);
/*        bool SetZEeprom(string &val);
        bool SetOEeprom(string &val);
        bool SetAEeprom(string &val);
        bool SetSysEeprom(string &val);*/
        bool SetStMemLen(int seccio,int len);
        /*bool SetAMemLen(int len);
        bool SetOMemLen(int len);
        bool SetZMemLen(int len);
        bool SetSysMemLen(int len);*/
        string GetStEeprom(int seccio);
        /*string GetZEeprom();
        string GetOEeprom();
        string GetAEeprom();
        string GetSysEeprom();*/
        int GetStMemLen(int seccio);
        /*int GetAMemLen();
        int GetOMemLen();
        int GetZMemLen();
        int GetSysMemLen();*/

        bool SetDev(string curmode,string curver,string curverdate,string curhard);
        bool IsDetecting();
        bool IsDetecting(int zona);
        bool IsAlarm();
        bool IsAlarm(int zona);
        bool IsOmiting();
        bool IsOmiting(int zona);
        bool IsAvery();
        bool IsAvery(int zona);

        bool SetDateTime(unsigned char *data);
        unsigned char *GetDateTime();
        string &GetTime();
        string &GetDate();

        bool SetVDD(float vdd);

        bool SetHasGeneral(bool general);
        bool HasGeneral();
        bool HasAnalog();
        bool HasBeacons();
        bool SetHasBeacons(bool beacons);
        bool SetHasWireless(bool Radio);
        bool HasWireless();
        bool SetHasWirelessUser(bool Radio);
        bool HasWirelessUser();
        bool SetHasWirelessOut(bool Radio);
        bool HasWirelessOut();
        bool SetHasReportTest( bool test);
        bool HasReportTest();

#ifdef _PICTURES
        bool SetHasPictures(bool pictures);
        bool HasPictures();
#endif


};

#endif
