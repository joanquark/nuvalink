#ifndef PANELVERIF_H
#define PANELVERIF_H

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#include <wx/panel.h>
#include <wx/listctrl.h>
#include <wx/string.h>
#include <wx/checkbox.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>

//#include "event.h"
#include "msgs.h"
#include "model.h"
#include "codi.h"
#include "grup.h"
#include "Data.h"
#include "thumbnail\thumbnailctrl.h"

class Cnuvalink;
//#define _FILTER_CLIENT
//#define _ALIAS_ESTAT

//TODO: mirar com es comporta quan la llista d'events es molt gran

class CPanelVerify: public wxPanel
{
    //friend class CPanelEditEvt;
    protected:
        string msgError;

        wxListCtrl *llista;
        wxBoxSizer *vSizer;
//        CEventList *events;
//        int RefEvent[65000];        // Guarda per cada item de la llista el index en CEventList.

        string CZalias[64];
        string CAalias[64];
        string CUalias[64];
        string COalias[64];

        CCodi *codi;
        Cnuvalink *nuvalink;
        CModel *modelSelect;
        wxBitmapButton* viewButton;
        wxString    codeLot;
        wxString    voltage;
        wxString    boardV;
        wxString    user;
        wxString    model;
        bool        onlyCheck;
    public:
        string PanelVersion;
        string PanelId;
        string CurYear;

        CPanelVerify(Cnuvalink *nuvalink,CModel *select,wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage* lang=0);
        void Clear();
        ~CPanelVerify();
        CLanguage*    Lang;
        bool ApplyValues();
        bool UpdateValues();
        void OnButtVerify(wxCommandEvent &ev);
        void OnButtCheck(wxCommandEvent &ev);
        void OnButtLabel(wxCommandEvent &ev);
//        void OnDeletelist(wxListEvent &ev);
        bool checkfeatures(CCodi* codi,CData *profile);
        DECLARE_EVENT_TABLE();

    protected:
        void GenComponents();
        void ListFill();
        bool GeneraCSVVerify(string *csv);
};

#endif
