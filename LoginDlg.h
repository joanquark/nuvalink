#ifndef LOGINDLG_H
#define LOGINDLG_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(LoginDlg)
	#include <wx/bmpbuttn.h>
	#include <wx/checkbox.h>
	#include <wx/combobox.h>
	#include <wx/dialog.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	//*)
#endif
//(*Headers(LoginDlg)
//*)
#include "msgs.h"
#include "config.h"


class LoginDlg: public wxDialog
{
	public:

		LoginDlg(wxWindow* parent,CLanguage *lang);
		CLanguage* Lang;
		virtual ~LoginDlg();

		//(*Declarations(LoginDlg)
		wxBitmapButton* BitmapButton1;
		wxCheckBox* checkRemeber;
		wxComboBox* ComboBox1;
		wxStaticText* StaticText1;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxTextCtrl* Pass;
		wxTextCtrl* TxtServer;
		//*)
        CConfig* config;
        void SetConfig(CConfig* config);
		//(*Identifiers(LoginDlg)
		static const long ID_STATICTEXT1;
		static const long ID_COMBOBOX1;
		static const long ID_STATICTEXT2;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT3;
		static const long ID_TEXTCTRL2;
		static const long ID_CHECKBOX1;
		static const long ID_BITMAPBUTTON1;
		//*)

	private:

		//(*Handlers(LoginDlg)
		void OnBitmapButton2Click(wxCommandEvent& event);
		void OnBitmapButton1Click(wxCommandEvent& event);
		void OnButton1(wxKeyEvent& event);
		void OnEnter(wxKeyEvent& event);
		void Log();
		void Log(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
