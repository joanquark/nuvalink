#ifndef LOGGER_H
#define LOGGER_H

#include <iostream>
#include <string>
using namespace std;

#include "LoggerThread.h"

class CLogger
{
    bool calHora;

    public:
//        wxString    code;
        int     privlevel;
        wxTextCtrl *text;
        bool    refresh;

        CLogger();
        ~CLogger();

        void MostraTrama(unsigned char*trama, int len);
        void MostraMsg(string &msg);
        void MostraChar(char c);
        void LimitaLog();
        void PosaHora();

};

#endif
