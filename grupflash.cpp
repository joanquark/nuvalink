#include "grupflash.h"

CGroupFlash::CGroupFlash()
{
    flash=0;
    upl=0;
    Clear();
    cat="ON";
}

CGroupFlash::CGroupFlash(CGroup *grupPare, wxString& descr)
{
    flash=0;
    Clear();
    SetFather(grupPare);
    SetDescr(descr);
    upl=0;
}

void CGroupFlash::Clear()
{
    descr="";
    id="";
    address="";
    grupPare=0;

    bytesElement=-1;
    numElements=-1;
    if (flash)
        delete [] flash;
    flash=0;
    for (int i=0; i<colCodi.size(); i++)
        delete colCodi[i];

    colCodi.clear();
    vector<CCodi *> temp;
    temp.swap(colCodi);

    for (int i=0; i<fitxers.size(); i++){
        delete fitxers[i];
    }
    fitxers.clear();
    vector<CFitxerFlash *> tmp;
    tmp.swap(fitxers);

    tipus=0;
    upl=0;
    cat="ON";
}

CGroupFlash::~CGroupFlash()
{
    Clear();
}

bool CGroupFlash::SetDescr(wxString &descr)
{
    this->descr = descr;
    return true;
}

bool CGroupFlash::SetFather(CGroup *grupPare)
{
    this->grupPare = grupPare;
    return true;
}

bool CGroupFlash::SetBytesElement(int bytes)
{
    if (bytes <0)
        return false;

    bytesElement = bytes;

    if (numElements != -1)
        Create();

    return true;
}

bool CGroupFlash::SetNumElements(int n)
{
    if (n < 0)
        return false;

    numElements = n;

    if (bytesElement != -1)
        Create();

    return true;
}

bool CGroupFlash::SetAddress(string& address)
{
    if (isHexString(address)) {
        this->address = address;
        return true;
    }
    return false;
}

int CGroupFlash::AddColumna()
{
    colCodi.push_back(new CCodi());
    //colTipus.push_back(0);
    //colDescr.push_back("");
    tipus = TIPUS_GRUPFLASH_COLUMNES;
    return colCodi.size()-1;
}

bool CGroupFlash::SetColumnaTipus(int col, int tipus)
{
    //if ((col < 0) || (col > colTipus.size()))
    if ((col < 0) || (col > colCodi.size()))
        return false;

    return colCodi[col]->SetTypeData(tipus);
}

bool CGroupFlash::SetColumnaDescr(int col, wxString& descr)
{
    if ((col < 0) || (col > colCodi.size()))
        return false;

    return colCodi[col]->SetDescr(descr);
}

bool CGroupFlash::Create()
{
    if ( (numElements != -1) && (bytesElement != -1)) {
        if (flash==0) {
            flash = new unsigned char [numElements * bytesElement];
        }
        return true;
    }
    return false;
}

bool CGroupFlash::SetColumnaVal(int col, string& data)
{
    for (int i=0; i<numElements; i++) {
        if (!SetElementCol(i, col, data))
            return false;
    }
    return true;
}

bool CGroupFlash::SetElement(int numElement, unsigned char *data)
{
    int index = numElement*bytesElement;
    if ( (index < 0) || (index+bytesElement > numElements*bytesElement))
        return false;

    for (int i=0; i<bytesElement; i++)
        flash[index+i] = data[i];

    return true;
}

bool CGroupFlash::VerificaElement(int numElement, unsigned char *data)
{
    int index = numElement*bytesElement;
    if ( (index < 0) || (index+bytesElement > numElements*bytesElement))
        return false;

    for (int i=0; i<bytesElement; i++) {
        if (flash[index+i] != data[i])
            return false;
    }

    return true;
}

bool CGroupFlash::SetElementCol(int numElement, int col, unsigned char *data)
{
    if ((col < 0) || (col > colCodi.size()))
        return false;

    int index = numElement*bytesElement;
    for (int i=0; i<col; i++)
        index += colCodi[i]->GetValLength();

    if ( (index < 0) || (index+colCodi[col]->GetValLength() > numElements*bytesElement))
        return false;

    for (int i=0; i<colCodi[col]->GetValLength(); i++)
        flash[index+i] = data[i];

    return true;
}

bool CGroupFlash::SetElementCol(int numElement, int col, string& data)
{
    if ((col < 0) || (col > colCodi.size()))
        return false;

    //Fem servir un CCodi per passar de string a valor en unsigned char
    //CCodi codi;
    //codi.SetTypeData(colTipus[col]);
    //codi.SetStringVal(data);
    colCodi[col]->SetStringVal(data);
    colCodi[col]->SetStringIniVal(data);
    return SetElementCol(numElement, col, colCodi[col]->GetVal());
}


int CGroupFlash::GetNumElements()
{
    return numElements;
}

int CGroupFlash::GetBytesElement()
{
    return bytesElement;
}

int CGroupFlash::GetNumCols()
{
    return colCodi.size();
}

CGroup* CGroupFlash::GetFather()
{
    return grupPare;
}

wxString& CGroupFlash::GetDescr()
{
    return descr;
}

wxString& CGroupFlash::GetColDescr(int col)
{
    if ((col<0) || (col>=colCodi.size()))
        return colCodi[0]->GetDescr();

    return colCodi[col]->GetDescr();
}

bool CGroupFlash::SetId(string& id)
{
    this->id = id;
    return true;
}

string& CGroupFlash::GetId()
{
    return id;
}


bool CGroupFlash::SetUpl(int nupl)
{
    this->upl = nupl;
}

int CGroupFlash::GetUpl(void)
{
    return this->upl;
}


string& CGroupFlash::GetCat()
{
    return cat;
}

bool CGroupFlash::SetCat(string& catval)
{
    this->cat=catval;
    return true;
}

string& CGroupFlash::GetAddress()
{
    return address;
}

unsigned char* CGroupFlash::GetElement(int numElement)
{
    int index = numElement*bytesElement;
    if ( (index < 0) || (index+bytesElement > numElements*bytesElement))
        return 0;

    return &flash[index];
}

unsigned char* CGroupFlash::GetElementCol(int numElement, int col)
{
    if ((col < 0) || (col > colCodi.size()))
        return 0;

    int index = numElement*bytesElement;
    for (int i=0; i<col; i++)
        index += colCodi[i]->GetValLength();

    if ( (index < 0) || (index+colCodi[col]->GetValLength() > numElements*bytesElement))
        return 0;

    return &flash[index];
}

string CGroupFlash::GetElementColString(int numElement, int col)
{
    unsigned char *data = GetElementCol(numElement,col);
    if (data==0)
        return 0;

    //CCodi codi;
    //codi.SetTypeData(colTipus[col]);
    colCodi[col]->SetVal(data);
    string strVal = colCodi[col]->GetStringVal();
    return strVal;
}

CCodi *CGroupFlash::GetColumnaCodi(int col)
{
    if ((col < 0) || (col > colCodi.size()))
        return 0;

    return colCodi[col];
}

int CGroupFlash::AddFitxer(CFitxerFlash *fitxer)
{
    if (fitxer)
        fitxers.push_back(fitxer);

    tipus = TIPUS_GRUPFLASH_FITXERS;
    return fitxers.size()-1;
}

int CGroupFlash::GetNumFiles()
{
    return fitxers.size();
}

CFitxerFlash* CGroupFlash::GetFile(int index)
{
    if ((index < 0) || (index >= fitxers.size()))
        return 0;

    return fitxers[index];
}

CFitxerFlash* CGroupFlash::GetFileById(wxString id)
{
    for (int i=0; i<fitxers.size(); i++) {
        CFitxerFlash *fitxer = fitxers[i];
        if (fitxer->GetId() == id)
            return fitxer;
    }

    return 0;
}

int CGroupFlash::GetTypeCon()
{
    return tipus;
}

int CGroupFlash::GetBytesFitxers()
{
    int total=0;

    for (int i=0; i<fitxers.size(); i++) {
        CFitxerFlash *fitxer = fitxers[i];
        if (fitxer->IsSelec())
            total += fitxer->GetBytesFitxer();
    }

    return total;
}

bool CGroupFlash::Reset()
{
    if (tipus == TIPUS_GRUPFLASH_COLUMNES) {
        for (int i=0; i<colCodi.size(); i++) {
            colCodi[i]->Reset();
            if (!SetColumnaVal(i, colCodi[i]->GetStringVal()))
                return false;
        }
    }
    else if (tipus == TIPUS_GRUPFLASH_FITXERS) {
        for (int i=0; i<fitxers.size(); i++) {
            if (!fitxers[i]->Reset())
                return false;
        }
    }

    return true;
}

