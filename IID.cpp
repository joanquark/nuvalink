#include "IID.h"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>     /* srand, rand */
#include <string.h>
//#define _wx(x) (wxString)((string)x).c_str()

CIntegrationID::CIntegrationID()
{
    IIDFrTx.ns.Val=0;
    ppush=0;
    ns=0;
}

CIntegrationID::~CIntegrationID()
{
    Reset();
}

void CIntegrationID::Reset()
{
    ppush=0;
}

bool CIntegrationID::ProcessIIDRx(unsigned char* buff,int cnt)
{
    TIIDFr* pf;
    pf=(TIIDFr*)buff;


    AES crypt;
    crypt.SetParameters(16*8,16*8); // 128 bit key and ciphered data.

    iidinflen=(pf->Flags&M_IID_EXTRALEN)*256 + pf->Ctrl;

    if (pf->Flags&M_IID_CRC_ADD){
        unsigned char crc[2];
        CalcCRC(buff, cnt-2, crc);

        if ((crc[0] != buff[cnt-2]) || (crc[1] != buff[cnt-1]))
            return false;
    }

    if (pf->Flags&M_IID_CIPHER){
        string keys=aeskey;
        char* key=&keys[0];
        crypt.StartDecryption((unsigned char*)key);
        unsigned char iv[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        unsigned char uncipherbuff[1024];
        crypt.Decrypt(&pf->buff[0],uncipherbuff,iidinflen/16,crypt.CBC,iv);
        memcpy(&pf->buff[0],uncipherbuff,iidinflen);
    }

    ns++;

    IIDFrRx=*pf;            // copy the frame to the class rx struct.
    ppop=0;                 // pop pointer to 0.
    return true;

}

// the bool indicates if more data has to be received, poplen, indicates about lenght of poped data.
int CIntegrationID::PopBlock(unsigned char wantedID,unsigned char* popbuff)
{

	int blklen;
	ppop=0;
	int iidlen=iidinflen;

	while (iidlen>0/*ppop<iidinflen*/){

		blklen=IIDFrRx.buff[ppop+1];
		if (!blklen){
			blklen=iidlen-2;             // null block is in fact total fr infolen-2!
		}
		unsigned char id=IIDFrRx.buff[ppop];

		if ((wantedID==id) || ((wantedID==0xFF) && (id!=EB_PADDING))){
            memcpy((void*)popbuff,(void*)&IIDFrRx.buff[ppop+2],blklen);
            ppop+=(blklen+2);
            return blklen;
		}
        ppop+=(blklen+2);
        iidlen-=(blklen+2);      // +L0D+
    }
    return 0;
}


bool CIntegrationID::FormatHeader(unsigned char flags,string cccccc)
{
    ppush=0;
    IIDFrTx.Header=PROTOJR_DC2;
    IIDFrTx.Flags=flags;
    IIDFrTx.ns.Val=ns;

    while (cccccc.length()<6)
        cccccc = "0" + cccccc;

    for (int i=0; i<3; i++) {
        IIDFrTx.CC[i] = ascii2nibble(cccccc[i*2]);
        IIDFrTx.CC[i] <<= 4;
        IIDFrTx.CC[i] |= ascii2nibble(cccccc[(i*2)+1]);
    }
}

bool CIntegrationID::AddBlock(unsigned char TID,unsigned char* buff,int len)
{
    int putlen;
    if (len>=256)
        putlen=0;
    else
        putlen=len;

    IIDFrTx.buff[ppush++]=TID;
    IIDFrTx.buff[ppush++]=(unsigned char)putlen;

    memcpy(&IIDFrTx.buff[ppush],buff,len);

    ppush+=len;
}

int CIntegrationID::CloseFr(unsigned char *raw)
{
    if (IIDFrTx.Flags&M_IID_CIPHER){
        int fill=0;
        if (int j=(ppush%16)){
            fill=16-j;
        }

		if (fill){

            if (fill<4)
                fill+=16;

            // let's insert the padding block.
            for (int i=ppush-1;i>=0;i--)              // copy from final 0
                IIDFrTx.buff[fill+i]=IIDFrTx.buff[i];

            ppush+=fill;

            fill-=2;
            IIDFrTx.buff[0]=EB_PADDING;
            IIDFrTx.buff[1]=(unsigned char)fill;

            int cnt=2;
            while (fill){
                IIDFrTx.buff[cnt++]=padbyte;
                fill--;
                padbyte+=(unsigned char)rand();
            }
        }
        IIDFrTx.Flags|=(unsigned char)((ppush>>8)&0x0003);
        IIDFrTx.Ctrl=(unsigned char)(ppush&0x00FF);
        if (raw)
            memcpy(raw,&IIDFrTx.Header,ppush+8);


        // apply cipher now.
        AES crypt;
        crypt.SetParameters(16*8,16*8); // 128 bit key and ciphered data.

        string keys=aeskey;
        char* key=&keys[0];
        crypt.StartEncryption((unsigned char*)key);
        unsigned char iv[16]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};

        unsigned char cipherbuff[1024];

        crypt.Encrypt((unsigned char*)&IIDFrTx.buff[0],cipherbuff,ppush/16,crypt.CBC,iv);
        memcpy((unsigned char*)&IIDFrTx.buff[0],cipherbuff,ppush);

    }else{
        IIDFrTx.Flags|=(unsigned char)((ppush>>8)&0x0003);
        IIDFrTx.Ctrl=(unsigned char)(ppush&0x00FF);
        if (raw)
            memcpy(raw,&IIDFrTx.Header,ppush+8);
    }

    frtxlen=ppush+8;

    if (IIDFrTx.Flags&M_IID_CRC_ADD){
        CalcCRC(&IIDFrTx.Header, frtxlen, &IIDFrTx.buff[ppush]);
        frtxlen+=2;
    }

    return frtxlen;
}

bool CIntegrationID::SetAesKey(string key)
{
    aeskey=key;
}

unsigned char* CIntegrationID::GetFrameBuff()
{
    return &IIDFrTx.Header;
}


void CIntegrationID::Ack()
{
    ns++;
}
