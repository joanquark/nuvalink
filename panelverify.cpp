#include "nuvalink.h"
#include "panelverify.h"
#include "jsonval.h"
#include "jsonwriter.h"
#define _wx(x) (wxString)((string)x).c_str()
#include "G500.h"
#include <wx/stdpaths.h>
extern "C" {
    #include "SSL/SSLxfer.h"
}

int idBotoVerify = wxNewId();
int idBotoLabel = wxNewId();
int idBotoCheck = wxNewId();
int idlist = wxNewId();

BEGIN_EVENT_TABLE(CPanelVerify, wxPanel)
    EVT_BUTTON(idBotoVerify, CPanelVerify::OnButtVerify)
    EVT_BUTTON(idBotoLabel, CPanelVerify::OnButtLabel)
    EVT_BUTTON(idBotoCheck, CPanelVerify::OnButtCheck)
//    EVT_LIST_KEY_DOWN(idwxlist, CPanelVerify::OnDeletelist)
END_EVENT_TABLE()

CPanelVerify::CPanelVerify(Cnuvalink *nuvalink,CModel *select,wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CLanguage* lang)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition) && (size==wxDefaultSize)) {
        SetSize(0,0,300,150);
    }

    if((pos!=wxDefaultPosition) && (size==wxDefaultSize)) {
        SetSize(300,150);
    }
    modelSelect=select;
    Lang=lang;
    //punters a 0
    llista=0;
//    thumbnail=0;
//    events=0;
    vSizer = new wxBoxSizer( wxVERTICAL );
    SetSizer( vSizer );
//    invert=false;
    this->nuvalink=nuvalink;

    codeLot=wxGetTextFromUser("Lot Code","Lot Code",nuvalink->Lot);
    nuvalink->Lot=codeLot;

    voltage=wxGetTextFromUser("Power Supply UKAS tester measure","Voltage",nuvalink->voltage);
    nuvalink->voltage=voltage;

    CCodi* board=modelSelect->GetCodiById("CDD");
    if (nuvalink->boardV.IsEmpty())
        nuvalink->boardV=board->GetStringVal();

    boardV=wxGetTextFromUser("Board Version","xx",nuvalink->boardV);
    nuvalink->boardV=boardV;

    onlyCheck=false;
    GenComponents();
}

void CPanelVerify::Clear()
{
    if (llista) {
        delete llista;
        llista=0;
    }

    vSizer->Detach(0);
    vSizer->Remove(0);

}

CPanelVerify::~CPanelVerify()
{
    Clear();
    nuvalink=0;
}

void CPanelVerify::GenComponents()
{
//    if (events) {
        Clear();

        llista = new wxListCtrl(this, idlist, wxPoint(0,0),wxSize(300,150), wxLC_REPORT|wxLC_HRULES );

        wxBoxSizer *sizerOpcions = new wxBoxSizer( wxHORIZONTAL );
        sizerOpcions->Add( new wxButton(this, idBotoVerify, _T("Verify")));
        sizerOpcions->Add( new wxButton(this, idBotoLabel, _T("Label")));
        sizerOpcions->Add( new wxButton(this, idBotoCheck, _T("Check")));

        vSizer->Add( llista, wxSizerFlags(1).Align(0).Expand());
        vSizer->Add( sizerOpcions, wxSizerFlags(0).Align(0).Expand().Border(wxALL,5));
        vSizer->Layout();

        ListFill();
//    }
}

void CPanelVerify::ListFill()
{

    int col=0;

    if (llista) {
        llista->ClearAll();

        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_DATE"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_TIME"));
        llista->InsertColumn(col++, _T("Param"));
        llista->InsertColumn(col++, _T("Max"));
        llista->InsertColumn(col++, _T("Min"));
        llista->InsertColumn(col++, _T("Val"));
        llista->InsertColumn(col++, _T("Result"));

        llista->SetColumnWidth( 0, 100 );
        llista->SetColumnWidth( 1, 100 );
        llista->SetColumnWidth( 2, 150 );
        llista->SetColumnWidth( 3, 100 );
        llista->SetColumnWidth( 4, 100 );
        llista->SetColumnWidth( 5, 100 );
        llista->SetColumnWidth( 6, 100 );
    }
}

bool CPanelVerify::ApplyValues()
{
    return true;
}

bool CPanelVerify::UpdateValues()
{
    return true;
}

// --------------------------- on char -------------------------------------------------------


bool CPanelVerify::checkfeatures(CCodi* codi,CData *profile)
{
    wxString modelName=_wx(profile->getname());
    wxString suffix=modelName.AfterFirst('-');
    wxString codides=codi->GetDescr();

    if (codides.Contains("Gsm") && (suffix.Contains("G")==false) && (suffix.Contains("T")==false))
        return false;
    if (codides.Contains("LRW") && suffix.Contains("L")==false)
        return false;
    if (codides.Contains("Ethernet") && suffix.Contains("E")==false)
        return false;
    if (codides.Contains("Wifi") && suffix.Contains("W")==false)
        return false;
    if (codides.Contains("BLE") && suffix.Contains("B")==false)
        return false;
    if (codides.Contains("Satellite") && suffix.Contains("P")==false)
        return false;
    if (codides.Contains("Z-Axis") && suffix.Contains("P")==false)
        return false;

    if (codides.Contains("VCNL")==true){
        if (modelName.Contains("B-")==false && modelName.Contains("EBF-")==false)
            return false;
    }

    if (codides.Contains("BATT")){
        if (modelName.Contains("B-")==false && modelName.Contains("EBF-")==false && modelName.Contains("EB-")==false
            && modelName.Contains("BSP-")==false && modelName.Contains("BSR-")==false && modelName.Contains("HELIUS-")==false){

            return false;
        }
    }
    if (codides.Contains("sensor")){        // SDI12 check
        if (profile->gettype()!="AGR")
            return false;
    }


    return true;

}

void CPanelVerify::OnButtCheck(wxCommandEvent &ev)
{
    onlyCheck=true;
    OnButtVerify(ev);
}

void CPanelVerify::OnButtVerify(wxCommandEvent &ev)
{
    int i=0;
    int j=0;
    int fails=0;
    int tries=0;
    GenComponents();
    wxListItem item;
    wxJSONValue root;
    wxJSONValue codis;
    wxJSONValue data;
    wxString SN;
    wxString IMEI="";
    CData* dataprofile=nuvalink->install.GetDataObj();
    wxString modelName=dataprofile->getname();

    // -------- force Board Version ----------------
    CCodi* board=modelSelect->GetCodiById("CDD");
    string bv=boardV.c_str();
    board->SetStringVal(bv);
    nuvalink->SendCodeValue(board,false);

    // Force appid depending on chosen profile for verification.
    CCodi* appid=modelSelect->GetCodiById("AppID");
    nuvalink->SendCodeValue(appid,false);

//    CCodi* codipowercfg=modelSelect->GetCodiById("6B6");
//    unsigned char poweropt=*(codipowercfg->GetVal());

 //   llista->ClearAll();
    for (i=0;i<modelSelect->GetNumCodis();i++){
        CCodi *codi=modelSelect->GetCodi(i);
        if (codi->GetVerify()!=0 && checkfeatures(codi,dataprofile)==true ){
reverify:
            // 24V power have 11.0 reduction ratio instead of 5.54. -> Change as circuits will carry out conversion
/*            if (codi->GetDescr()=="BATT" || codi->GetDescr()=="VCC"){
                if (poweropt&0x01){
                    codi->boardpowerfactor=0.09;
                }
            }*/
            int veriflevel=codi->GetVerify();
            if (dataprofile->gettype()=="AGR"){
                // channels verify
                if (codi->GetDescr().Contains("sensor")){
                    wxString name=codi->GetDescr();
                    char num=name.GetChar(6);
                    int canal=ascii2nibble(num);
                    if (canal>0)
                        canal--;

                    nuvalink->SendOrderPacket(ORDER_SDI12_CHECK,canal,0x35,0x52,0x30,0x00);
                    Sleep(2000);
                }
            }

            if (nuvalink->ReceiveCodeValue(codi,false))
            {
                long tmp;

                if (tries==0){

                    tmp = llista->InsertItem(j, "-", 0);
                    llista->SetItemData(tmp, j);

                    item.SetId(tmp);
                    wxString text = codi->GetDescr();

                    llista->SetItem(j, 0,_wx(NowDate("es")));
                    llista->SetItem(j, 1,_wx(NowTime()));
                    llista->SetItem(j, 2,_wx(text));
                    llista->SetItem(j, 3,_wx(iToS(codi->GetMax())));
                    llista->SetItem(j, 4,_wx(iToS(codi->GetMin())));
                }

                if (veriflevel>=2){          // means codi need limits to be check
                    if (codi->GetTypeConData()==TDATA_STRING64 || codi->GetTypeConData()==TDATA_STRING32 || codi->GetTypeConData()==TDATA_STRING24 || codi->GetTypeConData()==TDATA_STRING){
                        wxString val=codi->GetStringVal();
                        if (val.Contains(_wx(codi->GetMatch()))){
                            item.SetBackgroundColour(wxColor(100,176,40));      // verd
                            llista->SetItem(j, 6, "OK");
                            codis[codi->GetDescr()]=codi->GetStringVal();
                        }else{
                            item.SetBackgroundColour(wxColor(0xff,150,120));  // vermell
                            codis[codi->GetDescr()]="*fail*:"+codi->GetStringVal();
                            fails++;
                        }
                    }else{
                        int codival=codi->GetIntVal();
                        if (codi->GetDescr()=="BATT" || codi->GetDescr()=="VCC"){
//                            if (poweropt&0x01){
//                                codival<<=1;
//                            }
                        }
                        if ((codi->GetMax()>=codival) && (codi->GetMin()<=codival)){
                            item.SetBackgroundColour(wxColor(100,176,40));      // verd
                            llista->SetItem(j, 6, "OK");
                            codis[codi->GetDescr()]=codi->GetStringVal();
                        }else{
                            item.SetBackgroundColour(wxColor(0xff,150,120));  // vermell
                            if (codi->GetVerify()==3){
                                wxMessageBox("Active función para lectura de "+codi->GetDescr());
                                if (++tries<3){
                                    goto reverify;
                                }

                                int resultbox = wxMessageBox("Ignorar parámetro?", Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
                                codis[codi->GetDescr()]="*fail*:"+codi->GetStringVal();
                                if (resultbox != 2) {//wxID_YES
                                    fails++;
                                    llista->SetItem(j, 6, "FAIL");
                                }else{
                                llista->SetItem(j, 6, "IGNORED");
                                }
                            }else{
                                int resultbox=0;
                                if (nuvalink->config.GetSysUser()=="engineer" && onlyCheck==false){
                                    resultbox = wxMessageBox("Ignorar parámetro?", Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
                                }
                                codis[codi->GetDescr()]="*fail*:"+codi->GetStringVal();
                                if (resultbox != 2) {//wxID_YES
                                    fails++;
                                    llista->SetItem(j, 6, "FAIL");
                                }else{
                                    //continue;
                                    llista->SetItem(j, 6, "IGNORED");
                                }
                            }
                        }
                    }
                }else{
                    item.SetBackgroundColour(wxColor(100,176,40));      // verd
                    llista->SetItem(j, 6, "INFO");
                    codis[codi->GetDescr()]=codi->GetStringVal();
                    if (codi->GetDescr()=="Serial"){
                        SN=_wx(codi->GetStringVal());
                    }
                    if (codi->GetDescr()=="IMEI"){
                        IMEI=_wx(codi->GetStringVal());
                    }
                }
                llista->SetItem(j, 5, _wx(codi->GetStringVal()));
                llista->SetItem( item );
                j++;
                Sleep(200);
                tries=0;
            }else{
                wxMessageBox("*** FALLO AL COMUNICAR :-( ***");          // but continuing.
                onlyCheck=false;
                return;
            }
        }
    }
    if (fails){
        wxMessageBox("*** ERROR EN LA VERIFICACION!!! :-( ***");          // but continuing.
        onlyCheck=false;
    }else{

        if (onlyCheck==true){
            onlyCheck=false;
            return;
        }
        SN=LE2BEHexSN(SN);
        root["SN"]= SN;
        // addred VC as big endian.

        unsigned char vc[2];
        unsigned char vcc[4];
        time_t rawtime;
        struct tm * timeinfo;
        //Posem la data i hora de l'ordinador a l'estructura vcc[]
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        vcc[0] = (unsigned char) timeinfo->tm_hour;
        vcc[1] = (unsigned char) timeinfo->tm_min;
        vcc[2] = (unsigned char) timeinfo->tm_sec;
        vcc[3] = (unsigned char) timeinfo->tm_mday;

        CalcCRC(vcc,4,vc);
        string VC="";
        for (int i=0;i<2;i++){
            VC+=nibble2ascii(vc[i]>>4);
            VC+=nibble2ascii(vc[i]&0x0F);
        }
        root["VC"]=_wx(VC);             // Added verification code.
        data["name"]=dataprofile->getname();
        data["cusname"]=dataprofile->getcusname();
        data["rule"]=dataprofile->getrule();
        data["extPower"]=dataprofile->getextPower();
        data["tBatt"]=iToS(dataprofile->gettBatt());
        root["dades"]=data;
        root["company"]=dataprofile->getaddress();  // we force company DDBB that way id!
        root["appId"]=appid->GetStringVal();
        root["tdevice"]=modelSelect->GetDescr();
        root["version"]=modelSelect->GetVersio();
        root["codeLot"]=codeLot;                                   // adding lot code.
        root["voltage"]=voltage;
        root["user"]=nuvalink->config.GetSysUser();
        root["codis"]=codis;
        wxString simiccid=wxGetTextFromUser("Scan Iccid","Scan Iccid","");
        root["iccid"]=simiccid;                                    // register ICCID at production time
        root["Op"]="Production";

        wxJSONWriter writer( wxJSONWRITER_NONE );
        wxString jsontxt;
        writer.Write(root,jsontxt);
        jsontxt+="\r\n";
        wxString IP=nuvalink->config.GetParamTcpIp("ipprndest");
        string port=nuvalink->config.GetParamTcpIp("ipprnport").c_str();
        string portcloud=nuvalink->config.GetParamTcpIp("ipcloudport").c_str();
        char answer[32768];
        int answersize;
        UnCipherFile("nvt.nvk","nvt.pem",false);
        wxStandardPaths path;
        wxString filepem=path.GetDataDir();
        if (SSLtransfer(nuvalink->config.GetParamTcpIp("ipclouddest"),sToI(portcloud)-1,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsontxt.size(),&answer[0],&answersize)==FALSE){
            wxRemoveFile("nvt.pem");
            wxMessageBox("*** ERROR AL Connect CON EL SERVIDOR COMPRUEBE CONEXION IP :-( ***");         // but continuing.
        }else{
            wxRemoveFile("nvt.pem");
            if (answersize>0){
                wxString jsonwithdevices="";
                for (int i=0;i<answersize;i++){
                    jsonwithdevices.Append(wxChar(answer[i]));
                }
                wxJSONValue read;
                wxJSONReader reader(wxJSONREADER_TOLERANT);
                int numErrors = reader.Parse( jsonwithdevices, &read );
                if ( numErrors > 0 )  {
                    // if there are errors in the JSON document return
                    return;
                }
                wxString newSN=LE2BEHexSN(_wx(read["SN"].AsString()));
                if (newSN.length()==8){
                    string stdSN=newSN.c_str();
                    //LE2BEHexSN(instalacio.GetCodiVal(codisn));
                    CCodi* codi=modelSelect->GetCodiById("SN");
                    codi->SetStringVal(stdSN);
                    nuvalink->SendCodeValue(codi,false);
                    SN=LE2BEHexSN(newSN);
                }
                nuvalink->SetDevSN(SN);
                nuvalink->GenInstalJSON("json.njs");        // will be saved to cloud.
                nuvalink->SendDateTime();                   // this will force device to save RamNv to flash
            }
            wxMessageBox("*** CIRCUITO VERIFICADO!!! :-) ***\r\n SN:"+SN+" VC:"+_wx(VC));             // but continuing
            int resultbox = wxMessageBox("Imprimir etiquetas?", Lang->GetAppMiss("MISS_DIALOG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);

            nuvalink->SaveEepToFlash();         // save only config to factory flash without storing firmware.

            if (resultbox != 2) {//wxID_YES

            }else{
                wxString rule;
                rule=_wx(dataprofile->getrule());
                if (rule=="IMEI"){
                    rule+=":"+IMEI;
                    wxMessageBox("imei is "+rule);
                }

                G500* label=new G500();
                //bool G500LabelPrint(int copies,wxString host,int port,wxString Model,wxString SN,wxString FW,wxString website,wxString logo,wxString power,wxString battery,wxString rules);
                bool print=label->G500LabelPrint(3,IP,sToI(port),_wx(dataprofile->getname())+" "+LE2BEHexAPPID(appid->GetStringVal()),SN+" VC:"+VC,modelSelect->GetVersio()+"  Lot:"+codeLot,_wx(dataprofile->getaddress()),_wx(dataprofile->getcusname()),_wx(dataprofile->getextPower()),_wx(dataprofile->sgettBatt()),rule);
                //delete label;
                if (print==true)
                    wxMessageBox("*** PEGATINAS IMPRESAS !!! :-) ***");
                else
                    wxMessageBox("*** FALLO AL IMPRIMIR !!! :-) ***");
            }
        }
    }
}

// ----------------------------- PRINT LABEL BUTTONG ------------------------


void CPanelVerify::OnButtLabel(wxCommandEvent &ev)
{
    int i=0;
    int j=0;
    int fails=0;
    int tries=0;
    GenComponents();
    wxListItem item;
    wxJSONValue root;
    wxJSONValue codis;
    wxJSONValue data;
    wxString SN;
    wxString IMEI="";
    CData* dataprofile=nuvalink->install.GetDataObj();
    wxString modelName=dataprofile->getname();

    CCodi* appid=modelSelect->GetCodiById("AppID");
    nuvalink->SendCodeValue(appid,false);

        for (i=0;i<modelSelect->GetNumCodis();i++){
        CCodi *codi=modelSelect->GetCodi(i);
        if (codi->GetVerify()!=0 && checkfeatures(codi,dataprofile)==true ){
reverify:
            if (nuvalink->ReceiveCodeValue(codi,false))
            {
                long tmp;

                if (tries==0){

                    tmp = llista->InsertItem(j, "-", 0);
                    llista->SetItemData(tmp, j);

                    item.SetId(tmp);
                    wxString text = codi->GetDescr();

                    llista->SetItem(j, 0,_wx(NowDate("es")));
                    llista->SetItem(j, 1,_wx(NowTime()));
                    llista->SetItem(j, 2,_wx(text));
                    llista->SetItem(j, 3,_wx(iToS(codi->GetMax())));
                    llista->SetItem(j, 4,_wx(iToS(codi->GetMin())));
                }

                item.SetBackgroundColour(wxColor(100,176,40));      // verd
                llista->SetItem(j, 6, "INFO");
                codis[codi->GetDescr()]=codi->GetStringVal();
                if (codi->GetDescr()=="Serial"){
                    SN=_wx(codi->GetStringVal());
                }
                if (codi->GetDescr()=="IMEI"){
                    IMEI=_wx(codi->GetStringVal());
                }

                llista->SetItem(j, 5, _wx(codi->GetStringVal()));
                llista->SetItem( item );
                j++;
                Sleep(50);
                tries=0;
            }else{
                wxMessageBox("*** FALLO AL COMUNICAR :-( ***");          // but continuing.
                return;
            }
        }
    }
    SN=LE2BEHexSN(SN);
    root["SN"]= SN;
    // addred VC as big endian.

    unsigned char vc[2];
    unsigned char vcc[4];
    time_t rawtime;
    struct tm * timeinfo;
    //Posem la data i hora de l'ordinador a l'estructura vcc[]
    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    vcc[0] = (unsigned char) timeinfo->tm_hour;
    vcc[1] = (unsigned char) timeinfo->tm_min;
    vcc[2] = (unsigned char) timeinfo->tm_sec;
    vcc[3] = (unsigned char) timeinfo->tm_mday;

    CalcCRC(vcc,4,vc);
    string VC="";
    for (int i=0;i<2;i++){
        VC+=nibble2ascii(vc[i]>>4);
        VC+=nibble2ascii(vc[i]&0x0F);
    }
    root["VC"]=_wx(VC);             // Added verification code.

    data["name"]=dataprofile->getname();
    data["cusname"]=dataprofile->getcusname();
    data["rule"]=dataprofile->getrule();
    data["extPower"]=dataprofile->getextPower();
    data["tBatt"]=iToS(dataprofile->gettBatt());
    root["dades"]=data;
    root["appId"]=appid->GetStringVal();
    root["tdevice"]=modelSelect->GetDescr();
    root["version"]=modelSelect->GetVersio();
    root["codeLot"]=codeLot;                                        // adding lot code.
    root["voltage"]=voltage;
    root["user"]=nuvalink->config.GetSysUser();
    root["codis"]=codis;
    root["Op"]="Production";

    wxJSONWriter writer( wxJSONWRITER_NONE );
    wxString jsontxt;
    writer.Write(root,jsontxt);
    jsontxt+="\r\n";
    wxString IP=nuvalink->config.GetParamTcpIp("ipprndest");
    string port=nuvalink->config.GetParamTcpIp("ipprnport").c_str();
    string portcloud=nuvalink->config.GetParamTcpIp("ipcloudport").c_str();
    char answer[32768];
    int answersize;
    UnCipherFile("nvt.nvk","nvt.pem",false);
    wxStandardPaths path;
    wxString filepem=path.GetDataDir();
    if (SSLtransfer(nuvalink->config.GetParamTcpIp("ipclouddest"),sToI(portcloud)-1,"nvt.pem",filepem.c_str(),jsontxt.c_str(),jsontxt.size(),&answer[0],&answersize)==FALSE){
        wxRemoveFile("nvt.pem");
        wxMessageBox("*** ERROR AL Connect CON EL SERVIDOR COMPRUEBE CONEXION IP :-( ***");         // but continuing.
    }else{
        wxRemoveFile("nvt.pem");
        if (answersize>0){
            wxString jsonwithdevices="";
            for (int i=0;i<answersize;i++){
                jsonwithdevices.Append(wxChar(answer[i]));
            }
            wxJSONValue read;
            wxJSONReader reader(wxJSONREADER_TOLERANT);
            int numErrors = reader.Parse( jsonwithdevices, &read );
            if ( numErrors > 0 )  {
                // if there are errors in the JSON document return
                return;
            }
            wxString newSN=LE2BEHexSN(_wx(read["SN"].AsString()));
            if (newSN.length()==8){
                string stdSN=newSN.c_str();
                //LE2BEHexSN(instalacio.GetCodiVal(codisn));
                CCodi* codi=modelSelect->GetCodiById("SN");
                codi->SetStringVal(stdSN);
                nuvalink->SendCodeValue(codi,false);
                SN=LE2BEHexSN(newSN);
            }
            nuvalink->SetDevSN(SN);
            nuvalink->GenInstalJSON("json.njs");        // will be saved to cloud.
            nuvalink->SendDateTime();                   // this will force device to save RamNv to flash
        }
        wxString rule;
        rule=_wx(dataprofile->getrule());
        if (rule=="IMEI"){
            rule+=":"+IMEI;
            wxMessageBox("imei is "+rule);
        }

        G500* label=new G500();
        //bool G500LabelPrint(int copies,wxString host,int port,wxString Model,wxString SN,wxString FW,wxString website,wxString logo,wxString power,wxString battery,wxString rules);
        bool print=label->G500LabelPrint(3,IP,sToI(port),_wx(dataprofile->getname()),SN+" VC:"+VC,modelSelect->GetVersio()+"  Lot:"+codeLot,_wx(dataprofile->getaddress()),_wx(dataprofile->getcusname()),_wx(dataprofile->getextPower()),_wx(dataprofile->sgettBatt()),rule);
        //delete label;
        if (print==true)
            wxMessageBox("*** PEGATINAS IMPRESAS !!! :-) ***");
        else
            wxMessageBox("*** FALLO AL IMPRIMIR !!! :-) ***");
    }
}
