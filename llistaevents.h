#ifndef LLISTAEVENTS_H
#define LLISTAEVENTS_H

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "event.h"

class CEventList
{
    vector<CEvent *> events;
    wxString addressIni, addressFlash;  //adre�a eeprom inicial
    int punter, bytesEvent, maxPunter, tipusEvents;

    public:
        CEventList();
        void Clear();
        ~CEventList();

        bool rebreTots;
        bool SetDirFlash(wxString adr);
        bool SetPunter(int punter);
        bool SetBytesEvent(int bytes);
        bool SetMaxPunter(int max);
        bool SetTypeEvents(int tipus);
        wxString &GetDirFlash();
        bool IsEeprom();
        int GetPunter();
        int GetBytesEvent();
        int GetMaxPunter();
        int AddEvent(CEvent *event);
        CEvent *GetEvent(int index);
        bool ModifyEvent(int index,CEvent *event);
        bool DeleteEvent(int index);
        int GetNumEvents();
        int GetTypeConEvents();

};

#endif
