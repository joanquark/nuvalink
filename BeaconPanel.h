#ifndef BEACONPANEL_H
#define BEACONPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(BeaconPanel)
	#include <wx/listctrl.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	//*)
#endif
//(*Headers(BeaconPanel)
#include <wxSpeedButton.h>
//*)

class Cnuvalink;

#include "languages/CLanguage.h"
#include "Status.h"

class BeaconPanel: public wxPanel
{
	public:

		BeaconPanel(wxWindow* parent);
		virtual ~BeaconPanel();
		void SetNuvaLink(Cnuvalink* nuvalink,CLanguage* lang);

		//(*Declarations(BeaconPanel)
		wxFlexGridSizer* FlexGridSizer1;
		wxListCtrl* ListBeacon;
		wxSpeedButton* btRx;
		//*)

	protected:
        Cnuvalink* nuvalink;
        CLanguage* Lang;

        TBleDevSt  BleDevWhiteList[1024];

		//(*Identifiers(BeaconPanel)
		static const long ID_LISTCTRL1;
		static const long ID_SPEEDBUTTON1;
		//*)

	private:

		//(*Handlers(BeaconPanel)
		void OnbtRxLeftClick(wxCommandEvent& event);
		void OnbtRxLeftClick1(wxCommandEvent& event);
		//*)

		DECLARE_EVENT_TABLE()
};

#endif
