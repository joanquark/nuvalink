#include "G500.h"
#include "util.h"

G500::G500()
{
    //ctor
}


G500::~G500()
{
    //dtor
}


bool G500::G500LabelPrint(int num,wxString host,int port,wxString Model,wxString SN,wxString FW,wxString website,wxString logo,wxString power,wxString battery,wxString rules)
{


//    num=1;

    string error;
    bool succes=true;
    IPADDR.Hostname(_wx(host));
    IPADDR.Service(_wx(iToS(port)));

        time_t rawtime;
        struct tm * timeinfo;

        //Posem la data i hora de l'ordinador a l'estructura data[]
        time ( &rawtime );
        timeinfo = localtime ( &rawtime );
        string wdate="-";                 // 2249
        wdate+=iToS(timeinfo->tm_year - 100);
        wdate+=iToS((timeinfo->tm_yday/7)+1);

        wxMessageBox("Manf date:" + _wx(wdate));


    wxSocketClient* sock;
    try {
        sock = new wxSocketClient(wxSOCKET_NONE);
    }
    catch (std::string error) {
        return false;
    }
    sock->Connect(IPADDR,false);
    Sleep(500);

            string cmd="^Q30,3,0,0\r^H12\r^S3\r^C"+iToS(num)+"\r";   // setup(30,12,4,1,3,0);
            sock->Write((const void*)&cmd[0],24);
            cmd="^L\r\n\r\n";
            sock->Write((const void*)&cmd[0],6);                 // sendcommand("^L\r\n");
            string tmp;
//            string tmp=website.c_str();
//            cmd="AT,5,0,25,25,3,0,0,0,"+tmp+"\r\n";                 // ecTextOut(5,2,25,"Arial",,"www.nuvasafe.com");
//            sock->Write((const void*)&cmd[0],21+website.Len()+2);

            tmp=Model.c_str();
            cmd="AT,7,5,24,24,2,0,0,0,Mod:"+tmp+"\r\n";          // ecTextOut(5,27,25,"Arial","Model:xxxxxx");
            sock->Write((const void*)&cmd[0],25+Model.Len()+2);

            tmp=SN.c_str();
            cmd="AT,7,28,24,24,2,0,0,0,SN:"+tmp+"\r\n";           // ecTextOut(5,52,25,"Arial","SN : 0000041E VC:AC4D");
            sock->Write((const void*)&cmd[0],25+SN.Len()+2);

            FW+=(wxString)wdate;
            tmp=FW.c_str();
            //tmp+=wdate;
            cmd="AT,7,53,24,24,2,0,0,0,FW:"+tmp+"\r\n";           // ecTextOut(5,77,25,"Arial","FW : 1.1.2");
            sock->Write((const void*)&cmd[0],25+FW.Len()+2);

            tmp=battery.c_str();
            cmd="AT,7,78,24,24,2,0,0,0,Batt:"+tmp+"\r\n";          // ecTextOut(5,102,25,"Arial","Battery 3x3.6V LiSO2Cl 7800mA");
            sock->Write((const void*)&cmd[0],27+battery.Len()+2);

            tmp=power.c_str();
            cmd="AT,7,103,24,24,2,0,0,0,Input:"+tmp+"\r\n";          // ecTextOut(5,102,25,"Arial","Power 3x3.6V LiSO2Cl 7800mA");
            sock->Write((const void*)&cmd[0],29+power.Len()+2);

            tmp=rules.c_str();
            cmd="AT,7,128,24,24,2,0,0,0,"+tmp+"\r\n";               // ecTextOut(5,77,25,"Arial","EN 13485 EN 12820");
            sock->Write((const void*)&cmd[0],23+rules.Len()+2);

            //tmp=wdate;
           // cmd="AT,200,128,24,24,2,0,0,0,"+tmp+"\r\n";               // ecTextOut(5,102,25,"Arial","mdt:2246");
            //sock->Write((const void*)&cmd[0],24+7+2);

            tmp=logo.c_str();
            cmd="Y245,2,"+tmp+"\r\n";
            sock->Write((const void*)&cmd[0],9+logo.Length());        // sendcommand("Y275,2,logo");

            if (rules.Contains("EN54-21")==false){
                cmd="Y5,156,CE\r\n";
                sock->Write((const void*)&cmd[0],11);        // sendcommand("Y320,155,CE");
                cmd="Y110,160,rohs\r\n";
                sock->Write((const void*)&cmd[0],15);        // sendcommand("Y120,150,rohs");
                cmd="Y200,156,waste\r\n";
                sock->Write((const void*)&cmd[0],16);        // sendcommand("Y215,150,waste");
            }else{
                cmd="Y40,160,rohs\r\n";
                sock->Write((const void*)&cmd[0],15);        // sendcommand("Y120,150,rohs");
                cmd="Y140,156,waste\r\n";
                sock->Write((const void*)&cmd[0],16);        // sendcommand("Y215,150,waste");
                cmd="Y250,130,cpr34\r\n";
                sock->Write((const void*)&cmd[0],16);        // sendcommand("Y320,150,cpr0034");
            }

            if (rules.Contains("EN50136-2")){
                cmd="Y280,165,imqa1\r\n";
                sock->Write((const void*)&cmd[0],16);        // sendcommand("Y320,150,imqa");
            }

            if (power.Contains("Vdc")){
                cmd="Y170,110,DC\r\n";
                sock->Write((const void*)&cmd[0],13);        // sendcommand("Y320,155,CE");
            }else if (power.Contains("Vac")){
                cmd="Y170,110,AC11\r\n";
                sock->Write((const void*)&cmd[0],15);        // sendcommand("Y320,155,CE");
            }
//            cmd="Y320,155,UP\r\n";
//            sock->Write((const void*)&cmd[0],13);        // sendcommand("Y320,155,UP");
            cmd="E\r\n\r\n";
            sock->Write((const void*)&cmd[0],5);         // sendcommand("E\r\n");
            Sleep(100);

            sock->Discard();         // Discard any pending data!
            sock->Close();
            sock->Destroy();
//        }
//   }

    return succes;

}
