#include "Connection.h"
#include "protocolNIOT.h"
#include "strings.h"
#include "util.h"
#include <wx/stdpaths.h>

extern "C" {
    #include "SSL/SSLxfer.h"
}
//#define _wx(x) (wxString)((string)x).c_str()

CConnection::CConnection()
{

    type=0;
    baud=-1;
    bitsData=-1;
    bitsStop=0;
    paritat=-1;
    portCom=-1;
    conectat=false;
    progressDlg=0;
    progress=0;
    host="";
    port=0;
    psocket=0;
    socketc=0;
    sockets=0;
    FFT=true;
    reliefmode=false;
    avTimeout=TIMEOUT_ACK_RS485;
    cntmiss=0;

//    socket=0;
}

CConnection::~CConnection()
{
    if (conectat){
        Disconnect();
    }
}

bool CConnection::AvailableComPort(int port)
{
    return serial.IsAvailable(port);
}


bool CConnection::Connect()
{

    avTimeout=TIMEOUT_ACK_RS485;
    if (!IsConfigured())
        return false;

    if (conectat)
        return true;

    if (type == TYPE_USB_CONNECTION) conectat = ConnectUSB();
    else if (type == TYPE_RS485_CONNECTION) conectat = ConnectRS485(false);
    else if (type == TYPE_TCPIP_CONNECTION) conectat = ConnectTCPIP();
    else if (type == TYPE_CLOUD_CONNECTION) conectat = ConnectCLOUD();
    else conectat = false;

    return conectat;
}


int CConnection::GetMaxNIOTInfLen()
{
//    if (type==type_CONEXIO_MODEM)  return 64;
    /*else*/
    if (type==TYPE_USB_CONNECTION)
        return 32;
    else if (reliefmode==true)
        return 128;

    return 256;
}


int CConnection::checkUSB()
{
    const HKEY HK=HKEY_LOCAL_MACHINE;
    const char * cle="HARDWARE\\DEVICEMAP\\SERIALCOMM";

    //Obrim el registre
    HKEY ret;
    RegOpenKeyEx(HK,cle,0,KEY_ALL_ACCESS,&ret);

    unsigned long lNom=128, lVal=128, ty=0;
    char nomVal[128];
    unsigned char valor[128];

    long res;
    int n=0;
    int comport=255;
    string temp;
    do {
        lNom=128;
        lVal=128;
        res = RegEnumValue( ret, n, nomVal, &lNom, 0, &ty, valor, &lVal);
        if (res == ERROR_NO_MORE_ITEMS)
            break;

        temp = nomVal;
        wxString np = (char *)valor;

        if (temp.find("VCP",0) != string::npos){
            comport = atoi(&valor[3]);
            break;
        }else if (temp.find("USB",0) != string::npos){
            comport = atoi(&valor[3]);
            break;
        }
        //wxMessageBox(np);

        n++;
    } while (res != ERROR_NO_MORE_ITEMS && n<128);

    RegCloseKey(ret);
    return comport;
}

bool CConnection::ConnectUSB()
{
    int comport=checkUSB();
    if (comport==255)
        return false;

    if (serial.Open(comport, 115200, 8, 1, 0,false,true)) {
        return true;
    }
    else {
        wxMessageBox(Lang->GetAppMiss("MISS_ERROR_CON_LOCAL"));
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_LOCAL").c_str();
        return false;
    }

}


bool CConnection::ConnectRS485(bool rts,bool vcp)
{
    if (serial.Open(portCom, baud, bitsData, bitsStop, paritat,rts,true/*vcp*/)) {
        return true;
    }
    else {
        wxMessageBox(Lang->GetAppMiss("MISS_ERROR_CON_LOCAL"));
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_LOCAL").c_str();
        return false;
    }
}

bool CConnection::ConnectTCPIP()
{

    avTimeout=TIMEOUT_ACK_TCPIP;
    string error;
    IPADDR.Hostname(_wx(host));
    IPADDR.Service(_wx(iToS(port)));
    try {
        socketc = new wxSocketClient(wxSOCKET_NOWAIT/*wxSOCKET_NONE*/);
    }
    catch (std::string error) {
        msgError = error;
        return false;
    }
    socketc->SetFlags(wxSOCKET_NOWAIT);
    socketc->Connect(IPADDR,false);
    IsSocketServer=false;
    for (int i=0;i<40;i++){
        //socketc->WaitOnConnect(2);           // wait 20 seconds for connection ( GPRS Devices might not be listening for small period)
        Sleep(500);
        if (socketc->IsConnected()){
            psocket=socketc;
            return true;
        }
        if (!Progres(Lang->GetAppMiss("MISS_DIALOG_CON_TCPCLIENT"))){
            break;
        }
    }

    socketc->Destroy();
    return false;
}


bool CConnection::ConnectCLOUD()
{
    avTimeout=TIMEOUT_ACK_TCPIP;

    UnCipherFile("nvt.nvk","nvt.pem",false);
    bool success=false;
    wxStandardPaths path;
    wxString filepem=path.GetDataDir();
    if (SSLConnect((char*)&host[0],port,"nvt.pem",filepem.c_str())){
        success=true;
    }
    wxRemoveFile("nvt.pem");
    return success;
}

bool CConnection::Progres (string msg)
{
    if (progressDlg && progress) {
        *progress += 1;
        if (!progressDlg->Update(*progress,_wx(msg))) {
            msgError = Lang->GetAppMiss("MISS_ERROR_USER_ABORT").c_str();
            return false;
        }
    }
    else {
        //Cridem a l'aplicacio pq processi els missatges del windows
        if (myApp)
            myApp->Yield();
    }
    return true;
}

bool CConnection::Progres (wxString msg)
{
    if (progressDlg && progress) {
        *progress += 1;
        if (!progressDlg->Update(*progress,msg)) {
            msgError = Lang->GetAppMiss("MISS_ERROR_USER_ABORT").c_str();
            return false;
        }
    }
    else {
        //Cridem a l'aplicacio pq processi missatges del sistema
        if (myApp)
            myApp->Yield();
    }
    return true;
}

bool CConnection::UserCancel()
{
    if (progressDlg && progress) {
        if (!progressDlg->Update(*progress)) {
            msgError = Lang->GetAppMiss("MISS_ERROR_USER_ABORT").c_str();
            return true;
        }
    }
    else {
        if (myApp)
            myApp->Yield();
    }

    return false;
}

bool CConnection::SetRTS(bool valor, int TCon)
{
    return serial.SetRTS(valor, TCon);
}

bool CConnection::SetRTSToggle()
{
    return serial.SetRTSToggle();
}


bool CConnection::Disconnect()
{
    bool res=false;

    if (!conectat) {
        return true;
    }

//    wxMessageBox("D1");
    if (type == TYPE_USB_CONNECTION) res = DisConnectUSB();
    else if (type == TYPE_RS485_CONNECTION) res = DisConnectRS485();
    else if (type == TYPE_TCPIP_CONNECTION) res = DisConnectTCPIP();
    else if (type == TYPE_CLOUD_CONNECTION) res = DisConnectCLOUD();
    else{
        res = false;
    }
    conectat = false;

    return res;
}

bool CConnection::DisConnectUSB()
{
    return DisConnectRS485();

/*    if (uhandle) {
        hid_close(uhandle);
        return true;
    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_CLOSEPORT").c_str();
        return false;
    }*/
}

bool CConnection::DisConnectRS485()
{
    if (serial.Close()) {
        return true;
    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_CLOSEPORT").c_str();
        return false;
    }
}



bool CConnection::SocketLost()
{
    psocket=0;
    socketc=0;
    sockets=0;

    return true;
}


bool CConnection::DisConnectTCPIP()
{

    psocket->Discard();         // Discard any pending data!
    psocket->Close();
    psocket->Destroy();
    if (IsSocketServer){
        sockets->Destroy();
    }else{
        socketc->Destroy();
    }
    psocket=0;
    socketc=0;
    sockets=0;

    return true;
}

bool CConnection::DisConnectCLOUD()
{

    SSLSend("\0",1);
    Sleep(500);
    SSLClose();
    return false;
}


int CConnection::ReceiveData(unsigned char *trama, int length)
{
    int res;

    if (type == TYPE_USB_CONNECTION) res = ReceiveDataUSB(trama,length);
    else if (type == TYPE_RS485_CONNECTION) res = ReceiveDataLocal(trama, length);
    else if (type == TYPE_TCPIP_CONNECTION) res = ReceiveDataTCPIP(trama, length);
    else if (type == TYPE_CLOUD_CONNECTION) res = ReceiveDataCLOUD(trama, length);
    else res = -1;

    return res;
}

int CConnection::ReceiveDataUSB(unsigned char *trama, int length)
{
    return ReceiveDataLocal(trama,length);
/*    unsigned char usbb[256];
    int res=hid_read_timeout(uhandle,usbb,length,RESOLUTION_PROCESRX_LOCAL);
    if (res){
        memcpy(trama,&usbb[1],--res);
    }
    return res;*/
}

int CConnection::ReceiveDataLocal(unsigned char *trama, int length)
{
     return serial.ReadData(trama, length);
}

// ---------------------------------- Rep dades del socket TLScloud --------------
int CConnection::ReceiveDataCLOUD(unsigned char *trama, int length)
{
    return SSLReceive(trama,length);
}

// Rep dades del socket. En principi l'�nic que caldria fer es cridar la funci�
// socket->ReceiveBytes(); pero com hem posat un limit de dades, si en rebem masses
// les guardem en un buffer nostre
int CConnection::ReceiveDataTCPIP(unsigned char *trama, int length)
{
    int limit, ini, r=0;
    if (psocket){
        psocket->Read(trama,length);
        return psocket->LastCount();
    }
}


bool CConnection::SendData(unsigned char *trama, int length)
{
    bool res;

    if (type == TYPE_USB_CONNECTION) res = SendDataUSB(trama,length);
    else if (type == TYPE_RS485_CONNECTION) res = SendDataRS485(trama, length, false);          // no wait for end of transmission!
    else if (type == TYPE_TCPIP_CONNECTION) res = SendDataTCPIP(trama, length);
    else if (type == TYPE_CLOUD_CONNECTION) res = SendDataCLOUD(trama, length);
    else res = false;

    return res;
}

bool CConnection::SendDataUSB(unsigned char *trama, int length)
{
    return SendDataRS485(trama,length,FALSE);


}

bool CConnection::SendDataRS485(unsigned char *trama, int length,bool wait)
{
    bool res = true;

    int r = serial.SendData( (const char *)trama, length ,wait);

    if (r != length) {
        msgError = Lang->GetAppMiss("MISS_ERROR_CON_LOCALSEND").c_str();
        res = false;
    }

    //Si s'utilitza la funcio SendBlock del serial llavors cal esperar
    int temps = ((1+bitsData+bitsStop)*(length+1)*1000)  / baud;

    if (wait)
        Sleep(temps);

    return res;
}


bool CConnection::SetSocketTCPIP(wxSocketBase *sock)
{
    if (sock)
        psocket=sock;
    return true;
}

bool CConnection::SendDataCLOUD(unsigned char *trama, int length)
{
    return SSLSend(trama,length);
}

bool CConnection::SendDataTCPIP(unsigned char *trama, int length)
{
    string buffer;
    string error;
    if (psocket){
        if (psocket->IsConnected()==false)
            return false;
        try {
            psocket->Write(trama,length);
        }
        catch (std::string error) {
            msgError = error;
            return false;
        }
    }
    return true;
}

bool CConnection::SendDataGPRS(unsigned char *trama, int length)
{
    return SendDataTCPIP(trama, length);
}

bool CConnection::SetBaud(int baud)
{
    if ( (baud == 1200) || (baud == 2400) ||
        (baud == 4800) || (baud == 9600) || (baud == 19200) ||
        (baud == 38400) || (baud == 57600) || (baud == 115200)
    ) {
        this->baud = baud;
        return true;
    }

    return false;
}


bool CConnection::SetBitsData(int bits)
{
    if ((bits>0) && (bits<=8)) {
        bitsData = bits;
        return true;
    }

    return false;
}

bool CConnection::SetBitsStop(int bits)
{

    if ((bits>=0) && (bits<=2)) {
        if (bits==1)
            bitsStop=0;
        else if (bits==2)
            bitsStop=2;
        else
            bitsStop = bits;
        return true;
    }

    return false;
}

//TODO: posar valors adequats per paritat
bool CConnection::SetParity(int par)
{
    paritat = par;
    return true;
}

bool CConnection::SetType(int tip)
{
    if ( (tip == TYPE_USB_CONNECTION)
    || (tip == TYPE_RS485_CONNECTION)
    || (tip == TYPE_TCPIP_CONNECTION)
    || (tip == TYPE_CLOUD_CONNECTION)
    ) {
        type = tip;
        return true;
    }
    else {
        type=-1;
        return false;
    }
}


bool CConnection::SetPortCom(int port)
{
    if (port>=0) {
        portCom = port;
        return true;
    }

    return false;
}


bool CConnection::SetDestIP(const string& ip)
{
    this->host = ip;
    return true;
}

bool CConnection::SetIPPort(int port)
{
    this->port = port;
    return true;
}

string& CConnection::GetIPDesti()
{
    return host;
}

int CConnection::GetIPPort()
{
    return port;
}

// --------------------------------- Cada type de conexio necessita tenir certes dades
bool CConnection::IsConfigured()
{
    bool res=false;

    char buf[128];
    string log;

    if (type == TYPE_RS485_CONNECTION) {
        if ((baud != -1) && (portCom != -1) && (bitsData != -1) && (bitsStop != -1) && (paritat != -1))
            res = true;
    }

    else if ((type == TYPE_TCPIP_CONNECTION) || (type == TYPE_CLOUD_CONNECTION)) {
        if ((host != "") && (port!=0))
            res = true;
    }
    else if (type==TYPE_USB_CONNECTION)
    {
        res = true;
    }
    else res = false;

    return res;
}

int CConnection::GetBaud()
{
    return baud;
}

int CConnection::GetBitsData()
{
    return bitsData;
}

int CConnection::GetBitsStop()
{
    return bitsStop;
}

int CConnection::GetParity()
{
    return paritat;
}

int CConnection::GetTypeCon()
{
    return type;
}

bool CConnection::SetReliefMode(bool truefalse)
{
    reliefmode=truefalse;
}

bool CConnection::IsReliefMode()
{
    return reliefmode;
}

int CConnection::GetPortCom()
{
    return portCom;
}

string& CConnection::GetLastError()
{
    return msgError;
}

int CConnection::CalcAvTimeOut(int time)
{

    avTimeout=(avTimeout+time)/2;
    return avTimeout;

}

int CConnection::GetAvTimeOut()
{

    return avTimeout;

}

int CConnection::GetAnswerTimeOut()
{
    int res;

    if (type == TYPE_USB_CONNECTION || type == TYPE_RS485_CONNECTION) res = TIMEOUT_RESPOSTA_LOCAL;
    else if (type == TYPE_TCPIP_CONNECTION) res = TIMEOUT_RESPOSTA_TCPIP;
    else if (type==TYPE_CLOUD_CONNECTION) res = TIMEOUT_RESPOSTA_TCPIP;
    else res = 0;

    if (!avTimeout){
        avTimeout=res;
        return res;
    }else{
        if ((avTimeout+res)>MAX_TIMEOUT_RESPOSTA)
            return MAX_TIMEOUT_RESPOSTA;
        return avTimeout+res;
    }

}

int CConnection::GetTimeoutAck()
{
    int res;

    if (type == TYPE_RS485_CONNECTION || type == TYPE_USB_CONNECTION) res = TIMEOUT_ACK_RS485;
    else if (type == TYPE_TCPIP_CONNECTION) res = TIMEOUT_ACK_TCPIP;
    else if (type==TYPE_CLOUD_CONNECTION) res = TIMEOUT_ACK_TCPIP;
    else res = 0;

    if (!avTimeout){
        avTimeout=res;
        return res;
    }else{
        if ((avTimeout+res)>MAX_TIMEOUT_RESPOSTA)
            return MAX_TIMEOUT_RESPOSTA;
        return avTimeout+res;
    }

}

int CConnection::GetResolucioProcessRx()
{
    if (type==TYPE_USB_CONNECTION || type==TYPE_RS485_CONNECTION){
        return RESOLUTION_PROCESRX_LOCAL;
    }else{
        return RESOLUTION_PROCESRX_DIGREMOTE;
    }
}
