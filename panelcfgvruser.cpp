#include "panelcfgvruser.h"
#include "mlink.h"

BEGIN_EVENT_TABLE(CPanelCfgVRUser,wxPanel)
	////Manual Code Start
	////Manual Code End
	EVT_BUTTON(ID_BOTOADD,CPanelCfgVRUser::BotoAddClick)
	EVT_BUTTON(ID_BOTODELETE,CPanelCfgVRUser::BotoDeleteClick)
	EVT_BUTTON(ID_BOTOREAD,CPanelCfgVRUser::BotoReadClick)

END_EVENT_TABLE()


CPanelCfgVRUser::CPanelCfgVRUser(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CUnLang *lang,Cmlink* eli,int fzone)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,150);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,150);
    }

    UnLang=lang;
    fixuser=fzone;
    //punters a 0
    estat=0;
    mlink=eli;
    WxPanel1=0;

    GeneraComponents();
}

void CPanelCfgVRUser::Clear()
{

}

CPanelCfgVRUser::~CPanelCfgVRUser()
{
    Clear();
    estat=0;
}

wxPoint& CPanelCfgVRUser::VwXSetwxPoint(long x,long y)
{
    m_tmppoint.x=x;
    m_tmppoint.y=y;
    return m_tmppoint;
}

wxSize& CPanelCfgVRUser::VwXSetwxSize(long w,long h){
    m_tmpsize.SetWidth(w);
    m_tmpsize.SetHeight(h);
    return m_tmpsize;
}

void CPanelCfgVRUser::GeneraComponents()
{

        Clear();

        WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(8, 8), wxSize(300, 125));

        ConfigVR = new wxStaticBox(WxPanel1, ID_CONFIGVR, UnLang->GetAppMiss("MSG_PANVR_TITLE"), wxPoint(4, 1), wxSize(300, 125));

        Zona = new wxStaticText(WxPanel1, ID_ZONAS, wxT("User"), wxPoint(16, 30), wxDefaultSize, 0, wxT("User"));

        ZoneEdit = new wxTextCtrl(WxPanel1, ID_ZONEEDIT, wxT("1"), wxPoint(52, 29), wxSize(51, 20), wxTE_CENTRE, wxDefaultValidator, wxT("ZoneEdit"));
        if ((fixuser<=MAX_PANELCFGVR_USER) && (fixuser>=MIN_PANELCFGVR_USER)){
            ZoneEdit->SetValue(_wx(iToS(fixuser)));
            ZoneEdit->SetEditable(false);
        }
        ZoneNum = new wxStaticText(WxPanel1, ID_ZONENUM, "Keyfob code", wxPoint(114, 30), wxDefaultSize, 0, wxT("UserNum"));

        ZoneCodeEdit = new wxTextCtrl(WxPanel1, ID_ZONECODEEDIT, wxT("000000"), wxPoint(198, 28), wxSize(94, 20), 0, wxDefaultValidator, wxT("ZoneCodeEdit"));;

        BotoAdd = new wxBitmapButton(WxPanel1, ID_BOTOADD, wxBitmap("icons/toolbar/edit_add.png", wxBITMAP_TYPE_PNG), wxPoint(26, 60), wxSize(45, 45), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoAdd"));

        BotoDelete = new wxBitmapButton(WxPanel1, ID_BOTODELETE, wxBitmap("icons/toolbar/edit_remove.png", wxBITMAP_TYPE_PNG), wxPoint(77, 60), wxSize(45, 45), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoDelete"));

        BotoRead = new wxBitmapButton(WxPanel1, ID_BOTOREAD, wxBitmap("icons/toolbar/download.png", wxBITMAP_TYPE_PNG), wxPoint(128, 60), wxSize(45, 45), wxBU_AUTODRAW, wxDefaultValidator, wxT("BotoRead"));


}

bool CPanelCfgVRUser::SetEstat(CEstat *estat)
{
    this->estat = estat;
    GeneraComponents();
    return true;
}

void CPanelCfgVRUser::UnSetEstat()
{
    estat=0;
    Clear();
}

CEstat *CPanelCfgVRUser::GetEstat()
{
    return estat;
}

//Aplica el valor del camp de text al CCodi corresponent
bool CPanelCfgVRUser::AplicaValors()
{
    if (!estat)
        return false;

    //No apliquem valors perque o son editables
    return true;
}

//Actualitza els valors del CEstat al camp de text
//(es el contrari que AplicaValors()
bool CPanelCfgVRUser::ActualitzaValors()
{
    if (!estat)
        return false;

    //La millor manera d'actualitzar els valors es fent un clear i tornant a generar-ho
    Clear();
    GeneraComponents();

    return true;
}

// -----------------------------------------------------------------------
// jvd code
#ifdef _ALIAS_ESTAT
bool CPanelCfgVRUser::SetGrups(CGrup *grupUAlias)
{

    for (int i=0;i<64;i++)
    {
        string temp=MSG_PAN_COL_ZONE;
        temp+=" ";
        temp+=iToS(i+1);
        CUalias[i]=temp;
    }
    if (grupUAlias!=0)
    {
        for (int i=0; i<grupUAlias->GetNumCodiFills(); i++) {
            this->codi = grupUAlias->GetCodiFill(i);
            CUalias[i]= codi->GetStringVal();
        }
    }


   return true;
}
#endif

/*
 * BotoAddClick
 */
void CPanelCfgVRUser::BotoAddClick(wxCommandEvent& event)
{

    string ze=ZoneEdit->GetValue().c_str();

    int u=sToI(ze);


/*    int ver=sToI(PanelVersion);

    if (ver>=520){*/
        int resultbox = wxMessageBox(UnLang->GetAppMiss("MSG_DLG_ASK_VR"), UnLang->GetAppMiss("MSG_DLG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
        if (resultbox == 2) {

            if ((u<=MAX_PANELCFGVR_USER) && (u>=MIN_PANELCFGVR_USER)){
                if (!mlink->EnviarConfigVRUser((u-1),0,ZoneCodeEdit->GetValue().c_str())){
                    string err = mlink->GetUltimError();
                    wxMessageBox(_wx(err), UnLang->GetAppMiss("MSG_DLG_TIT_ERROR"));
                }

            }else{
                wxMessageBox("User don't exist!");
            }
        }
/*    }else{
        wxMessageBox("This model don't supports remote programming");
    }*/
}

void CPanelCfgVRUser::BotoDeleteClick(wxCommandEvent& event)
{
    string ze=ZoneEdit->GetValue().c_str();

    int u=sToI(ze);
/*    unsigned int sens=WxComboBox1->GetSelection();
    unsigned char tsens=(unsigned char)sens;*/

/*    int ver=sToI(PanelVersion);

    if (ver>=520){*/
        int resultbox = wxMessageBox(UnLang->GetAppMiss("MSG_DLG_ASK_VR"), UnLang->GetAppMiss("MSG_DLG_TIT_CONFIRM"), wxYES_NO | wxICON_QUESTION);
        if (resultbox == 2) {
            if ((u<=MAX_PANELCFGVR_USER) && (u>=MIN_PANELCFGVR_USER)){
                if (!mlink->EnviarConfigVRUser((u-1),0xFF,ZoneCodeEdit->GetValue().c_str())){
                    string err = mlink->GetUltimError();
                    wxMessageBox(_wx(err), UnLang->GetAppMiss("MSG_DLG_TIT_ERROR"));
                }
            }else{
                wxMessageBox("user don't exist!");
            }
        }
/*    }else{
        wxMessageBox("This model don't supports remote programming");
    }*/
}


void CPanelCfgVRUser::Read()
{
    TConfigVRdata *frame;

    string ze=ZoneEdit->GetValue().c_str();

    int u=sToI(ze);
    unsigned char tsens;

/*    int ver=sToI(PanelVersion);

    if (ver>=520){*/

        if ((u<=MAX_PANELCFGVR_USER) && (u>=MIN_PANELCFGVR_USER)){
            unsigned char *p=mlink->DemanarConfigVRUser(u-1);
            if (p!=NULL){
                frame=(TConfigVRdata*)p;

                string cod="";
                cod+=nibble2ascii(frame->code[0]>>4);
                cod+=nibble2ascii(frame->code[0]);

                cod+=nibble2ascii(frame->code[1]>>4);
                cod+=nibble2ascii(frame->code[1]);

                cod+=nibble2ascii(frame->code[2]>>4);
                cod+=nibble2ascii(frame->code[2]);

/*                string cod;
                HexChainToAscii(&frame->code[0],&cod[0],6);*/

                ZoneCodeEdit->SetValue(_wx(cod));

            }else{
                string err = mlink->GetUltimError();
                wxMessageBox(_wx(err), UnLang->GetAppMiss("MSG_DLG_TIT_ERROR"));
            }
        }else{
            wxMessageBox("User don't exist!");
        }

}

void CPanelCfgVRUser::BotoReadClick(wxCommandEvent& event)
{
    this->Read();

}
