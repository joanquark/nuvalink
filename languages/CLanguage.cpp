#include "../msgs.h"

#define _wx(x) (wxString)((string)x).c_str()

CLanguage::CLanguage()
{
    deflang=wxT("es");
}

CLanguage::~CLanguage()
{

}

bool CLanguage::Load()
{
    ifstream fitxer;
    wxString buffer, fileName="lang.xml";
        //Analitzem el XML
        myXmlParser xml;
        //Li diem al objecte que interpreta el xml que informi al objecte instalacio
        xml.SetSubscriber(*this);
        bool docParsed = xml.parse( fileName);

        if (!docParsed) {
            msgError = this->GetAppMiss("MISS_ERROR_BAD_XML");
            return false;
        }

    return true;
}

void CLanguage::foundNode ( wxString & name, wxXmlNode* attributes )
{
    if (attributes->GetNodeContent()!="")
        return;

    if (name=="app") {
        tmpMap = &missapp;
    }
    else if (name=="model") {
        tmpMap = &missmodel;
    }else{
        return;
    }
    lastTag = name;
}

void CLanguage::foundElement ( wxString & name, wxString & value, wxXmlNode* attributes )
{
    if (name=="text")
        return;

    if (name=="MISS_DIALOG_CON_TCPCLIENT"){
        int kk=0;

    }


    if (lastTag == "app") {
        missapp[name] = value ;
    }
    else if (lastTag == "model") {
        missmodel[name] = value;
    }

}

string CLanguage::GetDefLang()
{
    string slang=deflang.c_str();
    return slang;
}

bool CLanguage::SetDefLang(wxString& lang)
{
    deflang=lang;
}

wxString& CLanguage::GetModelMiss(wxString& m){
    wxString tmp=missmodel[m];
    if (tmp.size())
        return missmodel[m];
    else
        return m;
}

wxString& CLanguage::GetModelMiss(const char* ch){
    string p=ch;
    wxString mp=_wx(p);
    return missmodel[mp];
}

wxString& CLanguage::GetAppMiss(wxString& m){
    if (missapp[m]!="")
        return missapp[m];
    else
        return m;
}

wxString& CLanguage::GetAppMiss(const char* ch){
    string p=ch;
    wxString mp=_wx(p);
    if (missapp[mp]!="")
        return missapp[mp];
    else
        return mp;
}

