#ifndef _CLanguage_H
#define _CLanguage_H

#include <iostream>
#include <fstream>
#include <map>
#include <string>

using namespace std;

#include "util.h"
#include "myXmlParser\myxmlparser.h"
#include "myXmlParser\XmlNotify.h"

class CLanguage;

#define UN_LANG_FILENAME    "Lang.xml"

class CLanguage: public XmlNotify
{
    map <wxString, wxString> missmodel;
    map <wxString, wxString> missapp;
    map <wxString, wxString> *tmpMap;

    wxString lastTag;
    wxString msgError;
    wxString deflang;

    public:

    CLanguage();
    ~CLanguage();
    bool Load();
    wxString& GetModelMiss(wxString& m);
    wxString& GetModelMiss(const char* ch);
    wxString& GetAppMiss(wxString& m);
    wxString& GetAppMiss(const char* ch);
//    wxString& GetLogMiss(wxString& m);
//    wxString& GetLogMiss(const char* ch);
    string    GetDefLang();
    bool      SetDefLang(wxString& lang);

    void foundNode		( wxString & name, wxXmlNode* attributes );
    void foundElement	( wxString & name, wxString & value,wxXmlNode* attributes );

};

#endif
