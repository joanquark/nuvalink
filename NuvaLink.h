#ifndef NUVALINK_H
#define NUVALINK_H

/* ****************************************************************************
 * Classe Cnuvalink
 *
 * Classe principal del programa. Aqui es troben les funcions i algorismes d'alt
 * nivell. Funcions com "Connect", "SendData", "SaveInstal", etc.
 * ************************************************************************** */

// basic file operations
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>

using namespace std;

#include "myXmlParser\myxmlparser.h"
#include "instalacio.h"
#include "Connection.h"
#include "paneledit.h"
#include "panelllegenda.h"
#include "TreeCtrl.h"
#include "protocolNIOT.h"
#include "logger.h"
#include "model.h"
#include "grup.h"
#include "codi.h"
#include "config.h"
#include "util.h"
#include "msgs.h"
#include "myapp.h"
#include "event.h"

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include <wx/textctrl.h>
#include <wx/treectrl.h>
#include <wx/string.h>
#include <wx/progdlg.h>
#include <wx/imaglist.h>
#include <wx/icon.h>
#include <wx/sound.h>

#define _wx(x) (wxString)((string)x).c_str()

#define ID_SECTION_GEN_STATUS           1
#define ID_SECTION_ZW_STATUS            2
#define ID_SECTION_UW_STATUS            3
#define ID_SECTION_OW_STATUS            4
#define ID_SECTION_BEACON_STATUS        5
#define ID_SECTION_MEM_PANEL            6
#define ID_SECTION_ANA_STATUS           7
#define ID_SECTION_GRAPH_STATUS         8
#define ID_SECTION_PIC_STATUS           9
#define ID_SECTION_VERIFY_STATUS        10
#define ID_SECTION_VMODEM_STATUS        11
#define ID_SECTION_AGRO_PANEL           12


#define MENU_FLOTANT_CAP        0
#define MENU_FLOTANT_GRUP       1
#define MENU_FLOTANT_INSTALACIO 2
#define MENU_FLOTANT_REBRE      3
#define MENU_FLOTANT_MODEL      4


#include "privlevel.h"

class Cnuvalink
{
    CTreeCtrl TreeCtr;
    CProtocolNIOT* protocolNIOT;
    bool online;
    bool rxcodi;
    bool conectant;
    bool OnTimerHello;
    bool tocaouts;          // rep sortides o estat general en refresc autom�tic.
    bool noMessages;
    string msgError;
    bool timeout;
    CModel *modelSelect;
    CGroup *grupSelect;
    CStatus *estattmp;

    uint16_t ArrayFwSignatures[256];                     // for a file of 128KByte / sectors of 4K
    uint16_t ArrayFwCheckSums[256];
    uint32_t fchecksum;
    uint16_t cursectorchecksum;
    uint32_t flongitud;
    string  faddress;       // file address
    string  LastFlashFile;
    int     FFTSentPtr;
    int     panelActiveInputs;
    unsigned char   RawMsk[65536];

    int indImgCarpeta;
    int indImgCarpetaOberta;
    int indImgInstalacio;
    int indImgCentral;
    int indImgDades;
    int indImgEstat;
    int indImgEvents;
    int indImgKeyPad;
    int indImgVerify;
    int indImgVmodem;
    int indImgVCOM;
    bool refrescAuto;

    public:
        wxWindow* parent;
        CLanguage* Lang;
        wxSound SoConnect;
        wxString Directory;
        //Components que han de ser publics pq a vegades hi accedeixo desde MyFrame
        CInstalation install;
        CConnection connecthandler;
        CLogger myLog;
        CConfig config;
        int privlevel;
        bool cloudmode;
        bool plugnplay;

        bool CheckPrivLevel(int upl);

        bool IsChangeProg;
        bool F_INSTAL_LEVEL2;
        wxString SN;            // Keeps serial number.
        wxString Lot;
        wxString voltage;
        wxString boardV;
        wxString user;
        //Components visuals wx, publics pq a vegades es controlen desde altres
        //components visuals (MyFrame, Panels, keypad, etc
        wxTextCtrl *control;
        wxTreeCtrl *Tree;
        wxTimer *timerHello;
        wxTimer *timerConexio;
        wxString NomInstal;
   //     wxTreeItemId TableId[400];
        CPanelEdit *panelEdit;
        wxProgressDialog *progressDlg;

        MyApp *myApp;
        int progress;
        int timerinactivity;

        int GetCobertura();
        bool TeCobertura();

        bool transmitting;
        //Funcions
        Cnuvalink(CLanguage* lang);
        ~Cnuvalink();

        void Init();

        bool SetDevSN(wxString serial);
        wxString GetDevSN();
        bool NewInstal(wxString model, wxString versio,CData* dt);
        bool OpenInstal(string& fileName);
        bool OpenInstalCloud(wxString json,wxString versio);
        bool SaveInstal();
        bool SaveInstal(string& fileName);
        bool Need2SaveInstal();
        bool AddComponent(string& model, string &versio);
        bool DeleteComponent(int index);
        void CloseInstal();
        void SelectGroup(wxTreeItemId &id);
        void SelectGroup();
        void SelectGroupNum(int tipus);                  // before connect.
        int FloatMnu(wxTreeItemId &id);
        CModel* GetModelSelec(wxTreeItemId &id);
        bool FastRxProg(wxTreeItemId &id,bool afterconnect);
        bool AskReceiveProg();
        bool Connect();
        bool ParseConnectAnswer();
        bool Need2ConfigConnection();
        bool AvailableComPort(int port);
        bool SendValues(wxTreeItemId &id,bool onlychanges);
        bool ReceiveValues(wxTreeItemId &id);
        bool VerifyValues(wxTreeItemId &id);
        bool SendValues(CGroup *grup,bool onlychanges);
        bool ReceiveValues(CGroup *grup);
        bool SendDateTime(unsigned char *datetime=0);
        bool SendReportTest();
        bool SendAreaArm(WORD areesMask, bool Connect=true);
        bool SendOutputAct(int sortida, bool activar=true);
        bool SendZBypass(unsigned char zone);
        bool RecordWave(uint16_t channel,uint16_t samples);
        int RequestBleDevSt(BYTE* buff);
        string RxBinFile(int filenum);
        bool RxStatus(int seccio);
        bool RxGsmRssi(void);
        bool RxOuputStatus();
        bool RxOutputs();
        bool RxEvents();
        bool Disconnect();
        bool ReceiveCodeValue(CCodi* codi, bool verificar=false);     // neet to call from CPanelBerify
        bool ApplyProg(wxTreeItemId &id);
        bool ConfigConnection();
        bool SetConnectionParams(int tipus, int portCom, string& numTelf, bool callback, bool especial, string& host, int port, string& serverhost, int serverport);
        bool RxMemBlock(int count, string add, unsigned char* valors,bool RamNotFlash);
        bool TxMemBlock(int count, string add, unsigned char* valors,bool isRam);
        bool TxFlashFile(int count, string add, unsigned char* valors);
        bool TxPacket(unsigned char tpkt,unsigned char* frame,int len);
        bool SendAsciiFrame(string frame,int len);
        bool SaveConfig();
        bool ResetGroup(wxTreeItemId &id);
        bool SetEepToFlash(void);
        bool SaveEepToFlash(void);
        bool SetFlashToEep(void);
        bool SetEepReset(void);
        bool SetBattmAhCntReset(void);
        bool SendOrderPacket(unsigned char order,unsigned char param1, unsigned char param2, unsigned char param3, unsigned char param4, unsigned char param5,int tries=3);
        bool TimerHello();
        string& GetLastError();
        void GetAbout(string *about);
        wxString Model;
        wxString Version;
        bool IsOnline();
        bool IsRxCodi();
        bool IsTxOn();
        CProtocolNIOT* GetProtocolNIOT();

        //niroblock code
        string StorageNameGroup;
        CGroup *StorageZAliasGroup,*StorageAAliasGroup,*StorageOAliasGroup,*StorageUAliasGroup,*StorageRAliasGroup;
        wxTreeItemId StorageID;
        string GetStrValID(wxTreeItemId idConvert);
        bool SendCodeValue(CCodi* codi,bool onlychanges);
        bool GenInstalJSON(wxString json);
    private:
        bool ReadInstalXML(string& fileName);
        bool ReadInstalJSON(wxString jsontxt,wxString ver);
        void FillTreeView();
        void GenModelBranch(wxTreeItemId& idTreeRoot, CModel *model);
        void GenGroupBranch(wxTreeItemId& idTreePare, CGroup *grup);
        void ClearTreeView();
        void GenDataBranch(wxTreeItemId& idTreeRoot, CData *dades);
        void GenStatusBranch(wxTreeItemId& idTreeModel, CModel *model);
        void GenEventsBranch(wxTreeItemId& idTreeModel, CModel *model);
        void GenVerifyBranch(wxTreeItemId& idTreeModel, CModel *model);
        void GenVModemBranch(wxTreeItemId& idTreeModel, CModel *model);
        void GenVCOMBranch(wxTreeItemId& idTreeModel, CModel *model);
        void GenGroupBranchFlash(wxTreeItemId& idTreePare, CGroupFlash *grupFlash);
        bool GenInstalXML(wxString *xml);
        bool GenComponentXML(CModel *model, wxString *xml);
        bool SendConsecutiveValues(CGroup* grup, bool onlych);
        bool ReceiveConsecutiveValues(CGroup *grup, bool verificar=false);
        bool SendGroupValues(CGroup* grup,bool onlychanges);
        bool ReveiveGroupValues(CGroup *grup, bool verificar=false);
        bool UpdateGrupFromMem(CGroup* grup, unsigned char *valors, int maxadd);
        bool UpdateStatusFromRawMem();
        bool IniRawMsk(CGroup* group,int maxadd);
        bool SendGroupValuesFlash(CGroupFlash* grupFlash);
        bool ReveiveGroupValuesFlash(CGroupFlash *grupFlash, bool verificar=false);
        bool SendFlashFile(CGroupFlash *grupFlash, CFitxerFlash *fitxer);
        bool CheckFlashFile(CGroupFlash *grupFlash, CFitxerFlash *fitxer,bool show,bool WORDCHECK);
        bool SendValuesModel(CModel *model);
        bool ReceiveValuesModel(CModel *model);
        int NumComPackets(wxTreeItemId &id, bool rebre, int sizef);
        int NumCodisInside(CGroup *grup, bool rebre);
        void CancelConnect();
        bool Progres (int inc=1, string msg="");
        bool Progres (int inc=1, wxString msg="");
        bool UserCancel();
        bool TooMuchError(int errors=3);

};

#endif
