#ifndef VCOMPANEL_H
#define VCOMPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(VCOMPanel)
	#include <wx/bmpbuttn.h>
	#include <wx/checkbox.h>
	#include <wx/choice.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	#include <wx/textctrl.h>
	#include <wx/timer.h>
	//*)
#endif
//(*Headers(VCOMPanel)
#include <wx/lcdwindow.h>
#include <wxSpeedButton.h>
//*)

#include "NIOT.h"
#include "protocolNIOT.h"
#include "config.h"
#include "serial/Serial.h"

class Cnuvalink;

class VCOMPanel: public wxPanel
{
	public:

		VCOMPanel(Cnuvalink *nuva,wxWindow* parent,wxWindowID id=wxID_ANY,const wxPoint& pos=wxDefaultPosition,const wxSize& size=wxDefaultSize);
		virtual ~VCOMPanel();

		void SetParams(CProtocolNIOT* protocolniot, CConfig* conf);
        void Start();
		//(*Declarations(VCOMPanel)
		wxBitmapButton* BtCh1;
		wxBitmapButton* BtCh2;
		wxCheckBox* CheckLog;
		wxCheckBox* CheckTxt;
		wxChoice* choiceCOM;
		wxLCDWindow* LcdTime;
		wxSpeedButton* BtSend;
		wxSpeedButton* StartButton;
		wxSpeedButton* StopButton;
		wxStaticText* CurSt;
		wxStaticText* StaticText1;
		wxStaticText* StaticText2;
		wxStaticText* StaticText3;
		wxStaticText* StaticText4;
		wxTextCtrl* TcpServerPort;
		wxTextCtrl* TxtHex;
		wxTextCtrl* TxtLogCOM;
		wxTimer TimerCtrlCOM;
		//*)

	protected:

		//(*Identifiers(VCOMPanel)
		static const long ID_STATICTEXT4;
		static const long ID_CHOICECOM;
		static const long ID_STATICTEXT3;
		static const long ID_TEXTCTRL2;
		static const long ID_CHECKBOX1;
		static const long ID_CHECKBOX2;
		static const long ID_STATICTEXT5;
		static const long ID_TEXTCTRL3;
		static const long ID_TEXTCTRL1;
		static const long ID_STATICTEXT1;
		static const long ID_LCDWINDOW;
		static const long ID_STATICTEXT2;
		static const long ID_SPEEDBUTTONCOM;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		static const long ID_BITMAPBUTTON1;
		static const long ID_BITMAPBUTTON2;
		static const long ID_TIMERCOM;
		//*)

	private:

		//(*Handlers(VCOMPanel)
		void OnTimerCtrlTrigger(wxTimerEvent& event);
		void OnStartBtLeftClick(wxCommandEvent& event);
		void OnStopBtLeftClick(wxCommandEvent& event);
		void OnChoiceCOMSelect(wxCommandEvent& event);
		void OnChoiceVCOMSelect(wxCommandEvent& event);
		void OnCheckLogClick(wxCommandEvent& event);
		void OnBtSendLeftClick(wxCommandEvent& even);
		void OnBtCh1Click(wxCommandEvent& event);
		void OnBtCh2Click(wxCommandEvent& event);
		//*)
        void SocketSModemEvent(wxSocketEvent &event);
        void ShowRawFrame(unsigned char* fr, int len,bool rx);
        void ProcessFrameFromPC(unsigned char* buff, int len);

        Cnuvalink*      nuvalink;
        wxSocketBase   *psocket;
        wxSocketServer *sockets;
        CSerial         serial;
        string          cmd;
        int             VCOMCOM;
        int             VCOMTcpPort;
        bool            enabled;
        bool            vconnected;
        bool            sconnected;
        bool            hconnected;
        char            VCOM;
		CProtocolNIOT*  protocolNIOT;
		CConfig*        config;
		int             eLapsed_t;
		int             cnt25ms;
	protected:

		void BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size);

		DECLARE_EVENT_TABLE()
};

#endif

