#include "main.h"
#include <wx/filename.h>
#include "LoginDlg.h"
#include <wx/stdpaths.h>
#include <wx/dir.h>
#include "CloudDialog.h"
#include "ConfigDialog.h"
#include "ModelDlg.h"
//#include "EPD/font24.c"
//#include "EPD/font20.c"
//#include "EPD/font16.c"
//#include "EPD/font12.c"

IMPLEMENT_APP(MyApp);

bool MyApp::OnInit()
{

    //A argv[0] hi ha el nom de l'executable en el seu directori
    string jo = argv[0];
    int l = jo.find_last_of("\\");
    if (l>0) {
        jo = jo.substr(0,l);
        ::wxSetWorkingDirectory(_wx(jo));
    }

    CLanguage* Lang = new CLanguage();
    Cnuvalink* nuvalink = new Cnuvalink(Lang);
	MyFrame* frame = new MyFrame(0L, _("NuvaLink"),Lang,nuvalink);
	frame->Show();
	frame->SetTitle("NuvaLink");

	nuvalink->parent = (wxWindow*)frame;
    nuvalink->control = frame->control;
    nuvalink->Tree = frame->Tree;
    nuvalink->panelEdit = frame->panelEdit;
    nuvalink->myLog.text = frame->control;
    nuvalink->timerHello = frame->timerHello;
    nuvalink->myApp = this;
    nuvalink->Init();
    nuvalink->myLog.MostraMsg(jo);

    frame->NeedLogin();

    if (argc>=2) {
        string nomFitxer = argv[1];
        nuvalink->myLog.MostraMsg(nomFitxer);
        if (frame->OpenInstal(nomFitxer)){
            if (argc>=3){
                string ip=argv[2];
                string port=argv[3];
                string buit="";
                nuvalink->SetConnectionParams(TYPE_TCPIP_CONNECTION,0,buit,false,false,ip,sToI(port),buit,0);
                nuvalink->Connect();
            }
        }
    }

	return true;
}

int idMenuLogin = wxNewId();
int idMenuQuit = wxNewId();
int idMenuAbout = wxNewId();
int idMenuNew = wxNewId();
int idMenuOpen = wxNewId();
int idMenuSave = wxNewId();
int idMenuSaveAs = wxNewId();
int idMenuClose = wxNewId();
int idMenuConConfig = wxNewId();
int idMenuConnect = wxNewId();
int idMenuRebre = wxNewId();
int idMenuEnviar = wxNewId();
int idMenuEnviarCh = wxNewId();
int idMenuRxFlash = wxNewId();
int idMenuTxFlash = wxNewId();
int idMenuMngPacket = wxNewId();
int idMenuLog = wxNewId();

int idMenuChangeModelName = wxNewId();
int idTree = wxNewId();
int idTimerHello = wxNewId();
int idToolRebre = wxNewId();
int idToolEnviar = wxNewId();
int idToolEnviarCh = wxNewId();
int idToolAplicar = wxNewId();
int idToolConnectar = wxNewId();
int idToolDisconnect = wxNewId();
int idToolNew = wxNewId();
int idToolOpen = wxNewId();
int idToolSave = wxNewId();
int idToolSaveAs = wxNewId();
int idToolConfig = wxNewId();
int idToolClose = wxNewId();
int idToolQuit = wxNewId();
int idToolVerificar = wxNewId();
//int idToolKeyPad = wxNewId();
int idToolLog = wxNewId();
int idToolReset = wxNewId();
int idToolEepReset = wxNewId();
int idToolFlash = wxNewId();
int idMenuUltims[4] = {wxNewId(), wxNewId(), wxNewId(), wxNewId()};

BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_CLOSE(MyFrame::OnXQuit)
    EVT_MENU(idMenuLogin, MyFrame::OnLogin)
	EVT_MENU(idMenuQuit, MyFrame::OnQuit)
	EVT_MENU(idToolQuit, MyFrame::OnQuit)
	EVT_MENU(idMenuAbout, MyFrame::OnAbout)
	EVT_MENU(idMenuNew, MyFrame::OnNew)
	EVT_MENU(idToolNew, MyFrame::OnNew)
	EVT_MENU(idMenuOpen, MyFrame::OnOpenMnu)
	EVT_MENU(idToolOpen, MyFrame::OnOpen)
	EVT_MENU(idMenuSave, MyFrame::OnSave)
	EVT_MENU(idToolSave, MyFrame::OnSave)
	EVT_MENU(idMenuSaveAs, MyFrame::OnSaveAs)
	EVT_MENU(idToolSaveAs, MyFrame::OnSaveAs)
	EVT_MENU(idMenuClose, MyFrame::OnClose)
	EVT_MENU(idToolClose, MyFrame::OnClose)
    EVT_MENU(idMenuChangeModelName, MyFrame::OnChangeModelName)
    EVT_MENU(idMenuUltims[0], MyFrame::OnOpenRecent0)
    EVT_MENU(idMenuUltims[1], MyFrame::OnOpenRecent1)
    EVT_MENU(idMenuUltims[2], MyFrame::OnOpenRecent2)
    EVT_MENU(idMenuUltims[3], MyFrame::OnOpenRecent3)

	EVT_MENU(idMenuConConfig, MyFrame::OnConConfig)
	EVT_MENU(idToolConfig, MyFrame::OnConConfig)
	EVT_MENU(idMenuConnect, MyFrame::OnConnect)

	EVT_TREE_SEL_CHANGED(idTree, MyFrame::OnTreeSelection)

	EVT_MENU(idToolConnectar, MyFrame::OnConnect)
	EVT_MENU(idToolDisconnect, MyFrame::OnDisconnect)

	EVT_MENU(idToolRebre, MyFrame::OnButtReceive)
    EVT_MENU(idMenuRebre, MyFrame::OnButtReceive)
	EVT_MENU(idToolEnviar, MyFrame::OnButtSend)
	EVT_MENU(idMenuEnviarCh, MyFrame::OnButtSendCh)
	EVT_MENU(idMenuEnviar, MyFrame::OnButtSend)
	EVT_MENU(idMenuRxFlash, MyFrame::OnMenuRxFlash)
	EVT_MENU(idMenuTxFlash, MyFrame::OnMenuTxFlash)
	EVT_MENU(idMenuMngPacket, MyFrame::OnMenuMngPacket)
	EVT_MENU(idMenuLog,MyFrame::OnMenuLog)
	EVT_MENU(idToolAplicar, MyFrame::OnAplicar)
	EVT_MENU(idToolReset, MyFrame::OnButtReset)
	EVT_MENU(idToolEepReset, MyFrame::OnButtEep/*OnButtEepReset*/)
	EVT_MENU(idToolFlash, MyFrame::OnButtFlash)
	EVT_MENU(idToolVerificar, MyFrame::OnButtVerify)
	EVT_MENU(idToolLog, MyFrame::OnButtLog)
	EVT_TIMER(idTimerHello, MyFrame::TimerHello)
	EVT_TREE_ITEM_MENU(idTree, MyFrame::OnFloatMnu)
END_EVENT_TABLE()


MyFrame::MyFrame(wxFrame *frame, const wxString& title,CLanguage* lang,Cnuvalink* nuva)
	: wxFrame(frame, -1, title)
{
    wxStandardPaths path;

    this->nuvalink=0;

    Lang=lang;
    Lang->Load();                  // carreguem missatges

    wxDateTime now;

    wxInitAllImageHandlers();

	wxMenuBar* mbar = new wxMenuBar();

	//Menu Instalacio
	wxMenu* fileMenu = new wxMenu("");

	MakeFileInstal(fileMenu);

	mbar->Append(fileMenu, Lang->GetAppMiss("MISS_MNU_INST"));

	wxMenu* conexioMenu = new wxMenu("");
	conexioMenu->Append(idMenuConConfig, Lang->GetAppMiss("MISS_MNU_CON_CONFIG"), Lang->GetAppMiss("MISS_MNU_CON_CONFIG_DESCR"));
	conexioMenu->Append(idMenuConnect, Lang->GetAppMiss("MISS_MNU_CON_CONNECT"), Lang->GetAppMiss("MISS_MNU_CON_CONNECT_DESCR"));
	conexioMenu->Append(idMenuRebre, Lang->GetAppMiss("MISS_MNU_CON_RECV"), Lang->GetAppMiss("MISS_MNU_CON_RECV_DESCR"));
	conexioMenu->Append(idMenuEnviar, Lang->GetAppMiss("MISS_MNU_CON_SEND"), Lang->GetAppMiss("MISS_MNU_CON_SEND_DESCR"));
	conexioMenu->Append(idMenuEnviarCh, Lang->GetAppMiss("MISS_MNU_CON_SENDCH"), Lang->GetAppMiss("MISS_MNU_CON_SENDCH_DESCR"));
	conexioMenu->Append(idMenuRxFlash, Lang->GetAppMiss("MISS_MNU_CON_RXFLASH"), Lang->GetAppMiss("MISS_MNU_CON_RXFLASH"));
	conexioMenu->Append(idMenuTxFlash, Lang->GetAppMiss("MISS_MNU_CON_TXFLASH"), Lang->GetAppMiss("MISS_MNU_CON_TXFLASH"));
	conexioMenu->Append(idMenuMngPacket, Lang->GetAppMiss("MISS_MNU_CON_MNGPACKET"), Lang->GetAppMiss("MISS_MNU_CON_MNGPACKET"));
	conexioMenu->Append(idMenuLog, "Logging", "Logging");
	mbar->Append(conexioMenu, Lang->GetAppMiss("MISS_MNU_CON"));

	wxMenu* helpMenu = new wxMenu("");
	helpMenu->Append(idMenuAbout, Lang->GetAppMiss("MISS_MNU_HELP_ABOUT"), Lang->GetAppMiss("MISS_MNU_HELP_ABOUT"));
	mbar->Append(helpMenu, Lang->GetAppMiss("MISS_MNU_HELP"));

	SetMenuBar(mbar);

    wxBoxSizer *topsizer = new wxBoxSizer( wxVERTICAL );
    wxBoxSizer *horsizer = new wxBoxSizer( wxHORIZONTAL );
    wxBoxSizer *sizerLogKP = new wxBoxSizer( wxVERTICAL );

    splitter1 = new wxSplitterWindow(this, -1, wxPoint(0, 0), wxSize(600, 400), wxSP_BORDER);
    splitter = new wxSplitterWindow(splitter1, -1, wxPoint(0, 0), wxSize(600, 400), wxSP_BORDER);

    Tree = new wxTreeCtrl(splitter, idTree, wxPoint(0,0), wxSize(300, 300));
    panel = new wxPanel(splitter, -1);

    wxBoxSizer *sizer01 = new wxBoxSizer( wxVERTICAL );
    panelEdit = new CPanelEdit(panel, nuva, -1, wxDefaultPosition, wxSize(400, 300), wxSUNKEN_BORDER|wxVSCROLL,wxT("scrolledWindow"),Lang);
    sizer01->Add(panelEdit, wxSizerFlags(1).Align(0).Expand());

    panel->SetSizer(sizer01);
    control = new wxTextCtrl(splitter1, -1, "", wxDefaultPosition , wxDefaultSize, wxTE_MULTILINE|wxTE_RICH2);
    control->Show(false);

    splitter->SplitVertically(Tree, panel, 150);
    splitter1->Initialize(splitter);
    topsizer->Add(splitter1, wxSizerFlags(1).Align(0).Expand().Border(wxALL, 1));
    //panelKeyPad = new CPanelKeyPad(this, -1, wxDefaultPosition, wxDefaultSize, wxNO_BORDER,"",Lang);

    timerHello = new wxTimer(this, idTimerHello);
    timerHello->Start(TIMER_HELLO_VALUE, false); //Cada 20 segons
    tb = CreateToolBar();

    tb->SetToolBitmapSize(wxSize(32,32));
    tb->AddTool(idToolNew, Lang->GetAppMiss("MISS_TOOLB_INST_NEW"), wxBitmap(path.GetDataDir()+"/icons/toolbar/filenew.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_INST_NEW"), Lang->GetAppMiss("MISS_MNU_INST_NEW_DESCR"));
    tb->AddTool(idToolOpen, Lang->GetAppMiss("MISS_TOOLB_INST_OPEN"), wxBitmap(path.GetDataDir()+"/icons/toolbar/fileopen.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_INST_OPEN"), Lang->GetAppMiss("MISS_MNU_INST_OPEN_DESCR"));
    tb->AddTool(idToolSave, Lang->GetAppMiss("MISS_TOOLB_INST_SAVE"), wxBitmap(path.GetDataDir()+"/icons/toolbar/filesave.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_INST_SAVE"), Lang->GetAppMiss("MISS_MNU_INST_SAVE_DESCR"));
    tb->AddTool(idToolSaveAs, Lang->GetAppMiss("MISS_TOOLB_INST_SAVEAS"), wxBitmap(path.GetDataDir()+"/icons/toolbar/filesaveas.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_INST_SAVEAS"), Lang->GetAppMiss("MISS_MNU_INST_SAVEAS_DESCR"));
    tb->AddTool(idToolClose, Lang->GetAppMiss("MISS_MNU_INST_CLOSE"), wxBitmap(path.GetDataDir()+"/icons/toolbar/fileclose.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_MNU_INST_CLOSE"), Lang->GetAppMiss("MISS_MNU_INST_CLOSE_DESCR"));
    tb->AddSeparator();
    tb->AddTool(idToolConfig, Lang->GetAppMiss("MISS_MNU_CON_CONFIG"), wxBitmap(path.GetDataDir()+"/icons/toolbar/configure.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_MNU_CON_CONFIG"), Lang->GetAppMiss("MISS_MNU_CON_CONFIG_DESCR"));
    tb->AddTool(idToolConnectar, Lang->GetAppMiss("MISS_MNU_CON_CONNECT"), wxBitmap(path.GetDataDir()+"/icons/toolbar/connect_creating.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_MNU_CON_CONNECT"), Lang->GetAppMiss("MISS_MNU_CON_CONNECT_DESCR"));

    tb->AddTool(idToolDisconnect, Lang->GetAppMiss("MISS_MNU_CON_DISCONNECT"), wxBitmap(path.GetDataDir()+"/icons/toolbar/connect_no.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_MNU_CON_DISCONNECT"), Lang->GetAppMiss("MISS_MNU_CON_DISCONNECT_DESCR"));
    tb->AddTool(idToolRebre, Lang->GetAppMiss("MISS_MNU_CON_RECV"), wxBitmap(path.GetDataDir()+"/icons/toolbar/download.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_MNU_CON_RECV"), Lang->GetAppMiss("MISS_MNU_CON_RECV_DESCR"));
    tb->AddTool(idToolEnviar, Lang->GetAppMiss("MISS_MNU_CON_SEND"), wxBitmap(path.GetDataDir()+"/icons/toolbar/upload.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL,Lang->GetAppMiss("MISS_MNU_CON_SEND"), Lang->GetAppMiss("MISS_MNU_CON_SEND_DESCR"));

    tb->AddTool(idToolAplicar, Lang->GetAppMiss("MISS_TOOLB_CON_APPLY"), wxBitmap(path.GetDataDir()+"/icons/toolbar/rebuild.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_CON_APPLY"), Lang->GetAppMiss("MISS_TOOLB_CON_APPLY_DESCR"));
    tb->AddTool(idToolVerificar, Lang->GetAppMiss("MISS_TOOLB_CON_VERIFY"), wxBitmap(path.GetDataDir()+"/icons/toolbar/ok.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_CON_VERIFY"), Lang->GetAppMiss("MISS_TOOLB_CON_VERIFY_DESCR"));
    tb->AddTool(idToolReset, Lang->GetAppMiss("MISS_TOOLB_CON_RESET"), wxBitmap(path.GetDataDir()+"/icons/toolbar/undo.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_CON_RESET"), Lang->GetAppMiss("MISS_TOOLB_CON_RESET_DESCR"));
    tb->AddTool(idToolFlash, Lang->GetAppMiss("MISS_TOOLB_CON_FLASH"), wxBitmap(path.GetDataDir()+"/icons/toolbar/flash.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_CON_FLASH"), Lang->GetAppMiss("MISS_TOOLB_CON_FLASH_DESCR"));
    tb->AddTool(idToolEepReset, Lang->GetAppMiss("MISS_TOOLB_CON_EEPRESET"), wxBitmap(path.GetDataDir()+"/icons/toolbar/EepReset.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_CON_EEPRESET"), Lang->GetAppMiss("MISS_TOOLB_CON_FLASH2EEP_DESCR"));

    tb->AddSeparator();
    tb->AddTool(idToolLog, Lang->GetAppMiss("MISS_TOOLB_LOG"), wxBitmap(path.GetDataDir()+"/icons/toolbar/kwrite.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_CHECK, Lang->GetAppMiss("MISS_TOOLB_LOG"), Lang->GetAppMiss("MISS_TOOLB_LOG_DESCR"));
    tb->AddTool(idToolQuit, Lang->GetAppMiss("MISS_TOOLB_QUIT"), wxBitmap(path.GetDataDir()+"/icons/toolbar/exit.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_QUIT"), Lang->GetAppMiss("MISS_MNU_INST_QUIT_DESCR"));

    SetInstOberta(false);
    SetOnline(false);

    tb->Realize();

    CreateStatusBar();

    SetSizer( topsizer );      // use the sizer for layout
    topsizer->SetSizeHints( this );   // set size hints to honour minimum size
    Maximize(true); //Maximitzem la finestra
    //Ara que ja sabem com som de grans, ens centrem en la pantalla
    CenterOnScreen();

    //Creem i carreguem llista d'imatges
    wxImageList *llistaImg = new wxImageList(32, 32);
    Tree->AssignImageList(llistaImg);

    Setnuvalink(nuva);
    this->nuvalink->Lang = Lang;

    // ------------------------------------------ compile of EPD fonts to binary file -------------------------------
/*    unsigned char *fontfile = new unsigned char [65535];
    int fsize=0;
    int i=0;
    for (fsize=0;fsize<sizeof(Font24_Table);fsize++){
        fontfile[fsize]=Font24_Table[fsize];
    }
    while (fsize<8192)
        fontfile[fsize++]=0xFF;

    for (i=0;i<sizeof(Font20_Table);i++){
        fontfile[fsize++]=Font20_Table[i];
    }
    while (i<4096){
        i++;
        fontfile[fsize++]=0xFF;
    }

    for (i=0;i<sizeof(Font16_Table);i++){
        fontfile[fsize++]=Font16_Table[i];
    }
    while (i<4096){
        fontfile[fsize++]=0xFF;
        i++;
    }

    for (i=0;i<sizeof(Font12_Table);i++){
        fontfile[fsize++]=Font12_Table[i];
    }
    while (i<4096){
        fontfile[fsize++]=0xFF;
        i++;
    }
        //Obre finestra de seleccionar fitxers
        wxFileDialog* fileDlg = new wxFileDialog(this,_wx("save bin"), "", "", "*.bin", wxSAVE);

        if (fileDlg->ShowModal() == wxID_OK) {
            //TODO: si el fitxer seleccionat ja existeix hauriem de dir-li
            //SetCursor(wxCURSOR_WAIT);
            wxString wxNomFitxer = fileDlg->GetFilename();
            wxString wxNomDirectori = fileDlg->GetDirectory();
            string nomFitxer = wxNomDirectori.c_str();
            nomFitxer += '\\';
            nomFitxer += wxNomFitxer.c_str();

            ofstream fitxer;

            fitxer.open( (char *) nomFitxer.c_str(), ios::binary );
            if (fitxer.good()) {
                //fitxer << fontfile;
                for (int i=0;i<fsize;i++){
                    fitxer.put(fontfile[i]);
                }
                fitxer.close();
            }
        }else{
            wxMessageBox("Bad File");
        }

        delete [] fontfile;
    */
    // ------------------------------------------ compile of EPD background image to binary file --------------------


}

MyFrame::~MyFrame()
{
    delete nuvalink;
}


void MyFrame::NeedLogin(void)
{
    wxStandardPaths path;
    if (nuvalink->config.login=="true"){

        nuvalink->config.privlevel=-1;        // will be logout if no valid code entered.

        LoginDlg* Login;
        Login=new LoginDlg(this,Lang);
        Login->SetConfig(&nuvalink->config);
        Login->ShowModal();

        if (nuvalink->config.privlevel<0){
            Close();
        }else{
            int privlevel=nuvalink->config.privlevel;
            nuvalink->privlevel=nuvalink->config.privlevel;
            nuvalink->cloudmode=nuvalink->config.cloudmode;
            if (nuvalink->cloudmode==true){
                tb->DeleteTool(idToolOpen);
                tb->DeleteTool(idToolSave);
                tb->InsertTool(1,idToolOpen, Lang->GetAppMiss("MISS_TOOLB_INST_OPEN"), wxBitmap(path.GetDataDir()+"/icons/toolbar/cloud-download.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_INST_OPEN"), Lang->GetAppMiss("MISS_MNU_INST_OPEN_DESCR"));
                tb->InsertTool(2,idToolSave, Lang->GetAppMiss("MISS_TOOLB_INST_SAVE"), wxBitmap(path.GetDataDir()+"/icons/toolbar/cloud-upload.png", wxBITMAP_TYPE_PNG), wxNullBitmap, wxITEM_NORMAL, Lang->GetAppMiss("MISS_TOOLB_INST_SAVE"), Lang->GetAppMiss("MISS_MNU_INST_SAVE_DESCR"));
                tb->Realize();
            }
        }
        delete Login;

    }
    nuvalink->myLog.refresh=true;
    splitter1->SplitHorizontally(splitter, control, -100);
    control->Show(true);
    tb->ToggleTool(idToolLog,true);
    Layout();
}

void MyFrame::OnXQuit(wxCloseEvent &event)
{

    if (nuvalink->config.privlevel<0){
        Destroy();
    }else{
        wxMessageDialog *msgDlg2 = new wxMessageDialog(this, Lang->GetAppMiss("MISS_DIALOG_ASK_CLOSE"),Lang->GetAppMiss("MISS_DIALOG_TIT_CLOSE"), wxYES_NO);

        int result2 = msgDlg2->ShowModal();

        if (result2 == wxID_YES) {
            if (online)
                nuvalink->Disconnect();
            //Si hi ha una instalacio oberta li preguntem si vol guardarla
            if (!AskSaveInstal())
                return;

            nuvalink->SaveConfig();
            Destroy();
        }else if (result2 == wxID_NO) {
            return;
        }
    }
}

void MyFrame::OnQuit(wxCommandEvent& event)
{
   /*
    if (online)
        nuvalink->Disconnect();

    //Si hi ha una instalacio oberta li preguntem si vol guardarla
   if (!AskSaveInstal())
    return;

   // wxMessageBox(_("Test"));
    nuvalink->SaveConfig();
*/
    Close();
}


void MyFrame::OnLogin(wxCommandEvent& event)
{
    LoginDlg* Login;
    Login=new LoginDlg(this,Lang);
    Login->SetConfig(&nuvalink->config);
    Login->ShowModal();

    nuvalink->privlevel=nuvalink->config.privlevel;
    delete Login;

}


void MyFrame::OnClose(wxCommandEvent& event)
{

    //wxMessageBox(_("test close"));
    //Si hi ha una instalacio oberta li preguntem si vol guardarla
    if (!AskSaveInstal())
        return;

    if (online) {
        nuvalink->Disconnect();
        SetOnline(false);
    }

    nuvalink->CloseInstal();
    SetInstOberta(false);
    SetTitle(_wx("nuvalink"));
}

void MyFrame::OnNew(wxCommandEvent& event)
{
    if (!AskSaveInstal())
        return;

    if (online) {
        nuvalink->Disconnect();
        SetOnline(false);
    }

    wxString model="";
    wxString versio="";
    CData* data=new CData();
    ModelDlg *dlg=new ModelDlg(this,wxID_ANY,wxDefaultPosition,wxDefaultSize,&model,&versio,Lang,data);
    int res=dlg->ShowModal();

    if (res==wxID_OK){
        nuvalink->SelectGroup();
        if (nuvalink->NewInstal(model, versio,data)) {
            wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_INST_CREATE"), Lang->GetAppMiss("MISS_DIALOG_TIT_INFO"));
            SetInstOberta(true);
            fileName = _wx(model);
            fileName+=" ";
            fileName+=_wx(versio);
            SetTitleBar();
        }
    }else{
        string msg = Lang->GetAppMiss("MISS_ERROR_INST_CREATE").c_str();
        msg += nuvalink->GetLastError();
        wxMessageBox((wxString)msg.c_str(), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        SetInstOberta(false);
    }

   // delete dlg;
}

bool MyFrame::AskSaveInstal()
{
    if (nuvalink->Need2SaveInstal()) {
        wxMessageDialog *msgDlg = new wxMessageDialog(this, Lang->GetAppMiss("MISS_DIALOG_ASK_SAVENOW"), Lang->GetAppMiss("MISS_DIALOG_TIT_SAVE"), wxYES_NO | wxCANCEL);

        int result = msgDlg->ShowModal();

        if (result == wxID_YES) {
            wxCommandEvent event;
            OnSave(event);
        }
        //No es el mateix el boto "NO" que el "CANCEL"
        //El "NO" diu que no vol guardar la instalacio anterior
        //El "CANCEL" cancela l'accio, es a dir, sortim de la funcio on estem
        else if (result == wxID_CANCEL) {
            return false;
        }
    }

    return true;
}

void MyFrame::Open(int recent)
{
    //Si hi ha una instalacio oberta li preguntem si vol guardarla
    if (!AskSaveInstal())
        return;

    if (online) {
        nuvalink->Disconnect();
        SetOnline(false);
    }

    string nomFitxer;

    if (recent <= -1) {
        //Obre finestra de seleccionar fitxers
        if (nuvalink->cloudmode==true && recent==-1){
            CloudDialog *dlg=new CloudDialog(this,wxID_ANY);
            string portcloud=nuvalink->config.GetParamTcpIp("ipcloudport").c_str();
            //string ipcloud = nuvalink->config.GetParamTcpIp("ipclouddest").c_str();
            string ipcloud = nuvalink->config.GetSessionServer().c_str();
            wxString versio="";
            dlg->SetParams(nuvalink->config.GetSysUser(),nuvalink->config.GetSysPass(),nuvalink->config.clouddevices,ipcloud,sToI(portcloud)-1,&versio);
            if (dlg->ShowModal() == wxID_OK){
                if (nuvalink->OpenInstalCloud(dlg->GetJSONDev(),versio)){
                    SetInstOberta(true);
                    wxMenuBar *mbar = GetMenuBar();
                    wxMenu *mInst = mbar->GetMenu(0);
                    MakeFileInstal(mInst);
                    int tipus;
                    wxTreeItemId id = Tree->GetRootItem();
                    id=Tree->GetNextVisible(id);
                    Tree->SelectItem(id);
                    nuvalink->SelectGroup(id);
                    Layout();
                    panel->Layout();
                    SetTitleBar();
                }
            }
            delete dlg;
            return;
        }else{
            wxFileDialog fileDlg(this, Lang->GetAppMiss("MISS_DIALOG_TIT_OPEN"), "", "", "*.mli", wxOPEN);
            if (fileDlg.ShowModal() == wxID_OK) {
                wxString wxNomFitxer = fileDlg.GetFilename();
                wxString wxNomDirectori = fileDlg.GetDirectory();
                nomFitxer = wxNomDirectori.c_str();
                nomFitxer += '\\';
                nomFitxer += wxNomFitxer.c_str();
            }
            else return;
        }
    }
    else {
        nomFitxer = nuvalink->config.GetLast(recent);
    }

    OpenInstal(nomFitxer);

}

bool MyFrame::OpenInstal(string& nomFitxer)
{

    SetCursor(wxCURSOR_WAIT);
    bool result = nuvalink->OpenInstal(nomFitxer);
    SetCursor(wxNullCursor);
    if (!result) {
        string msg = nuvalink->GetLastError();
        wxMessageBox((wxString)msg.c_str(), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
    SetInstOberta(result);

    wxString wxnomFitxer=_wx(nomFitxer);

    if (result) {
        nuvalink->config.AddLast(wxnomFitxer);
        wxMenuBar *mbar = GetMenuBar();
        wxMenu *mInst = mbar->GetMenu(0);
        MakeFileInstal(mInst);

        wxString path=wxnomFitxer;
        wxFileName *file=new wxFileName(path,wxPATH_DOS);
        fileName=file->GetName();
        nuvalink->Directory=file->GetPath(wxPATH_GET_VOLUME, wxPATH_NATIVE);       // guardem el directori de treball del fitxer.
        int tipus;
        wxTreeItemId id = Tree->GetRootItem();
        id=Tree->GetNextVisible(id);
        Tree->SelectItem(id);
        nuvalink->SelectGroup(id);
        Layout();
        panel->Layout();
        SetTitleBar();

    }
    return result;
}

void MyFrame::OnOpenRecent0(wxCommandEvent& event)
{
    Open(0);
}

void MyFrame::OnOpenRecent1(wxCommandEvent& event)
{
    Open(1);
}

void MyFrame::OnOpenRecent2(wxCommandEvent& event)
{
    Open(2);
}

void MyFrame::OnOpenRecent3(wxCommandEvent& event)
{
    Open(3);
}

void MyFrame::OnOpen(wxCommandEvent& event)
{
    Open(-1);
}

void MyFrame::OnOpenMnu(wxCommandEvent& event)      // just to differentiate from Download Cloud button.
{
    Open(-2);
}

void MyFrame::OnSaveAs(wxCommandEvent& event)
{

    //Obre finestra de seleccionar fitxers
    wxFileDialog* fileDlg = new wxFileDialog(this, Lang->GetAppMiss("MISS_DIALOG_TIT_SAVEAS"), "", "", "*.mli", wxSAVE);

    if (fileDlg->ShowModal() == wxID_OK) {
        // TODO: si el fitxer seleccionat ja existeix hauriem de dir-li
        SetCursor(wxCURSOR_WAIT);
        wxString wxNomFitxer = fileDlg->GetFilename();

        wxString wxNomDirectori = fileDlg->GetDirectory();
        string nomFitxer = wxNomDirectori.c_str();
        nomFitxer += '\\';
        nomFitxer += wxNomFitxer.c_str();
        bool result = nuvalink->SaveInstal(nomFitxer);

        wxString wxnomFitxer=_wx(nomFitxer);

        SetCursor(wxNullCursor);
        if (result) {
            wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_INST_SAVED"), Lang->GetAppMiss("MISS_DIALOG_TIT_INFO"));
            nuvalink->config.AddLast(wxnomFitxer);
            wxMenuBar *mbar = GetMenuBar();
            wxMenu *mInst = mbar->GetMenu(0);
            MakeFileInstal(mInst);

            wxString path=wxnomFitxer;
            wxFileName *file=new wxFileName(path,wxPATH_DOS);
            fileName=file->GetName();
            SetTitleBar();
            if (online)
                nuvalink->SendDateTime();       // this will save RamNv to flash
        }
        else {
            string error = nuvalink->GetLastError();
            wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        }
    }
}


void MyFrame::OnSave(wxCommandEvent& event)
{
    //Si la instalacio es nova i no te nom cal demanar-li
    if ((nuvalink->install.GetFileName() == "") && (nuvalink->cloudmode==false)) {
        OnSaveAs(event);
        return;
    }

    SetCursor(wxCURSOR_WAIT);
    bool result = nuvalink->SaveInstal();
    SetCursor(wxNullCursor);
    if (result) {
        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_INST_SAVED"), Lang->GetAppMiss("MISS_DIALOG_TIT_INFO"));
        wxString wxnomFitxer=_wx(nuvalink->install.GetFileName());
        nuvalink->config.AddLast(wxnomFitxer);
        wxMenuBar *mbar = GetMenuBar();
        wxMenu *mInst = mbar->GetMenu(0);
        MakeFileInstal(mInst);
        if (online)
            nuvalink->SendDateTime();       // this will save RamNv to flash
    }
    else {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
}

void MyFrame::OnChangeModelName(wxCommandEvent& event)
{

    wxTreeItemId id = Tree->GetSelection();
    CModel *model = nuvalink->GetModelSelec(id);
    if (model) {
        wxString nouNom = wxGetTextFromUser(Lang->GetAppMiss("MISS_DIALOG_ASK_MODELNAME"), Lang->GetAppMiss("MISS_DIALOG_TIT_MODELNAME"), _wx(model->GetDescr()));
        string nom = nouNom.c_str();
        if (nouNom != "") {
            model->SetDescr(nouNom);
            Tree->SetItemText(id, nouNom);
        }
    }
}

void MyFrame::Setnuvalink(Cnuvalink *nuvalink)
{
    this->nuvalink = nuvalink;
 //   panelKeyPad->nuvalink = nuvalink;

    //Carreguem l'icono de la config
    string iconPath = nuvalink->config.GetParamOem("icono").c_str();
    if (iconPath != "") {
        wxImage img = wxImage(_wx(iconPath), wxBITMAP_TYPE_GIF);
        img.SetMaskColour(0,0,0);
        wxBitmap bmp(img);
        wxIcon ico;
        ico.CopyFromBitmap(bmp);
        if (ico.Ok())
            SetIcon(ico);
    }
    else {
        wxIcon ico("icons/nuvathingslogo.ico", wxBITMAP_TYPE_ICO);
        if (ico.Ok())
            SetIcon(ico);
    }

    string nomPrograma = nuvalink->config.GetParamOem("nom-programa").c_str();
    if (nomPrograma != "")
        SetTitle(_wx(nomPrograma));

    wxMenuBar *mbar = GetMenuBar();
    wxMenu *mInst = mbar->GetMenu(0);
    MakeFileInstal(mInst);
}

void MyFrame::MakeFileInstal(wxMenu* fileMenu)
{
    if (fileMenu->GetMenuItemCount() == 0) {
        fileMenu->Append(idMenuLogin, "Login","Login");
        fileMenu->Append(idMenuNew, Lang->GetAppMiss("MISS_MNU_INST_NEW"), Lang->GetAppMiss("MISS_MNU_INST_NEW_DESCR"));
        fileMenu->Append(idMenuOpen, Lang->GetAppMiss("MISS_MNU_INST_OPEN"), Lang->GetAppMiss("MISS_MNU_INST_OPEN_DESCR"));
        fileMenu->Append(idMenuSave, Lang->GetAppMiss("MISS_MNU_INST_SAVE"), Lang->GetAppMiss("MISS_MNU_INST_SAVE_DESCR"));
        fileMenu->Append(idMenuSaveAs, Lang->GetAppMiss("MISS_MNU_INST_SAVEAS"), Lang->GetAppMiss("MISS_MNU_INST_SAVEAS_DESCR"));
        fileMenu->Append(idMenuClose, Lang->GetAppMiss("MISS_MNU_INST_CLOSE"), Lang->GetAppMiss("MISS_MNU_INST_CLOSE_DESCR"));
        fileMenu->Append(idMenuQuit, Lang->GetAppMiss("MISS_MNU_INST_QUIT"), Lang->GetAppMiss("MISS_MNU_INST_QUIT_DESCR"));
        fileMenu->AppendSeparator();
    }

    //Les ultimes instalacions obertes
	if (nuvalink) {
        for (int i=0; i<MAX_LAST_FILESOPEN; i++) {
            fileMenu->Destroy(idMenuUltims[i]);
        }

        for (int i=0; i<MAX_LAST_FILESOPEN; i++) {
            string nomFitxer = nuvalink->config.GetLast(i).c_str();
            if (nomFitxer=="") break;
            string menu = "&";
            menu += iToS(i+1) + ". ";
            menu += nomFitxer;
            fileMenu->Append(idMenuUltims[i], _wx(menu), _wx(menu));
        }

	}
}


//Es crida quan l'usuari selecciona un component del TreeView
void MyFrame::OnTreeSelection(wxTreeEvent& event)
{
    wxTreeItemId id = Tree->GetSelection();

    nuvalink->SelectGroup(id);

    Layout();
    panel->Layout();
    SetTitleBar();
}

//Es crida quan l'usuari presiona el boto dret en un component del TreeView
//o quan crida al menu amb algun altre sistema.
//Fara apareixer un men� flotant
void MyFrame::OnTreeMenu(wxTreeEvent& event)
{

    wxTreeItemId id = Tree->GetSelection();

}

void MyFrame::OnConnect(wxCommandEvent& event)
{
    if (online)
        return;

    if (nuvalink->Need2ConfigConnection())
        OnConConfig(event);


    SetCursor(wxCURSOR_WAIT);
    bool result = nuvalink->Connect();
    SetCursor(wxNullCursor);

    if (result) {
        nuvalink->SoConnect.Stop();
        nuvalink->SoConnect.Play( _wx("tada.wav") , wxSOUND_ASYNC );
        SetOnline(true);
        SetTitleBar();

        nuvalink->AskReceiveProg();

    } else {
        nuvalink->SoConnect.Stop();
        nuvalink->SoConnect.Play( _wx("chord.wav") , wxSOUND_ASYNC );

        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
}

void MyFrame::OnDisconnect(wxCommandEvent& event)
{

    if (nuvalink->Disconnect()) {
        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_CON_DISCONNECTED"), Lang->GetAppMiss("MISS_DIALOG_TIT_INFO"));
    }
    else {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }

    SetOnline(false);
    SetTitleBar();
}

void MyFrame::OnButtReceive(wxCommandEvent& event)
{
    wxTreeItemId id = Tree->GetSelection();
    if (!nuvalink->ReceiveValues(id)) {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
    SetTitleBar();
}


void MyFrame::OnButtVerify(wxCommandEvent& event)
{

    wxTreeItemId id = Tree->GetSelection();

    if (!nuvalink->VerifyValues(id)) {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
    else {
        wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_VERIFIED"), Lang->GetAppMiss("MISS_DIALOG_TIT_INFO"));
    }
}

void MyFrame::OnButtSend(wxCommandEvent& event)
{

    wxTreeItemId id = Tree->GetSelection();

    if (!nuvalink->SendValues(id,false)) {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
}


void MyFrame::OnButtSendCh(wxCommandEvent& event)
{

    wxTreeItemId id = Tree->GetSelection();

    if (!nuvalink->SendValues(id,true)) {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
}

void MyFrame::OnMenuRxFlash(wxCommandEvent& event)
{

    // First ask for address and count
    wxString add = wxGetTextFromUser("Flash HEXA address", "Flash HEXA address", "5000");
    string addr = (string)add.c_str();

    wxString cnt = wxGetTextFromUser("Bytes to Receive", "Bytes To Receive", "2048");
    if (cnt=="")   return;
    string cnt1 = (string)cnt.c_str();
    int count = sToI( cnt1 );


    unsigned char *valors = new unsigned char [count];

    if (!nuvalink->RxMemBlock(count,addr,valors,false)) {
        string error = Lang->GetAppMiss("MISS_ERROR_TIMEOUT").c_str();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }else{
        //Obre finestra de seleccionar fitxers
        wxFileDialog* fileDlg = new wxFileDialog(this, Lang->GetAppMiss("MISS_DIALOG_TIT_SAVEFLASH"), "", "", "*.flh", wxSAVE);

        if (fileDlg->ShowModal() == wxID_OK) {
            //TODO: si el fitxer seleccionat ja existeix hauriem de dir-li
            //SetCursor(wxCURSOR_WAIT);
            wxString wxNomFitxer = fileDlg->GetFilename();
            wxString wxNomDirectori = fileDlg->GetDirectory();
            string nomFitxer = wxNomDirectori.c_str();
            nomFitxer += '\\';
            nomFitxer += wxNomFitxer.c_str();

            ofstream fitxer;

            fitxer.open( (char *) nomFitxer.c_str(), ios::binary/*ifstream::out*/ );
            if (fitxer.good()) {
                //fitxer << valors;
                for (int i=0;i<count;i++){
                    fitxer.put(valors[i]);
                }
                fitxer.close();
            }
        }else{
            wxMessageBox("Bad File");
        }

    }
    delete [] valors;
}


void MyFrame::OnMenuTxFlash(wxCommandEvent& event)
{

    int count;

    wxFileDialog* fileDlg = new wxFileDialog(this, Lang->GetAppMiss("MISS_DIALOG_TIT_READFLASH"), "", "", "*.flh", wxOPEN);
    if (fileDlg->ShowModal() == wxID_OK) {
    }else{
        return;
    }

    wxString wxNomFitxer = fileDlg->GetFilename();
    wxString wxNomDirectori = fileDlg->GetDirectory();
    string nomFitxer = wxNomDirectori.c_str();
    nomFitxer += '\\';
    nomFitxer += wxNomFitxer.c_str();

    ifstream fitxer;

    fitxer.open( (char *) nomFitxer.c_str(),  ios::binary );
    // get length of file:
    fitxer.seekg (0, ios::end);
    count = fitxer.tellg();
    fitxer.seekg (0, ios::beg);

    // allocate memory:
    unsigned char *valors = new unsigned char [count+256];

    // read data as a block:
    fitxer.read ((char*)valors,count);
    fitxer.close();

    for (int i=count;i<(count+256);i++)         // forcing last F1 paquet to fill "non-file" bytes with FF
        valors[i]=0xFF;

    wxString nomfile=_wx(nomFitxer);
    if (nomfile.Contains("ADD") ){
        wxString addf=nomfile.AfterLast('D');
    }else if (nomfile.Contains("add")){
        wxString addf=nomfile.AfterLast('d');
    }

    // First ask for address and count
    wxString add = wxGetTextFromUser("Flash HEXA address", "Flash HEXA address", "5000");
    string addr = (string)add.c_str();

    if (!nuvalink->TxFlashFile(count,addr,valors)) {
        string error = Lang->GetAppMiss("MISS_ERROR_TIMEOUT").c_str();
        wxMessageBox(_wx("MISS_ERROR_TIMEOUT"), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
    delete [] valors;
}

void MyFrame::OnMenuMngPacket(wxCommandEvent& event)
{

    DManageFrame dialog(NULL,idDMngFrame,"Custom Packets",nuvalink);
    dialog.ShowModal();

}
void MyFrame::OnMenuLog(wxCommandEvent& event)
{
    if (nuvalink->myLog.refresh)
        nuvalink->myLog.refresh=false;
    else
        nuvalink->myLog.refresh=true;
}


void MyFrame::OnAplicar(wxCommandEvent& event)
{

    wxTreeItemId id = Tree->GetSelection();

    if (!nuvalink->ApplyProg(id)) {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }
    else{
        wxProgressDialog *pgdlg = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_APPLY"), Lang->GetAppMiss("MISS_DIALOG_TIT_APPLY"),
                    10, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_SMOOTH);
        for(int i=1;i<=12;i++){
            wxSleep(1);
            pgdlg->Update(i);
        }
        delete pgdlg;
    }
}

char credits[] = "Autor : Joan Vidal www.nuvathings.com";

void MyFrame::OnAbout(wxCommandEvent& event)
{

    string about;
    nuvalink->GetAbout(&about);

	wxMessageBox(_wx(about), Lang->GetAppMiss("MISS_DIALOG_TIT_ABOUT"));
}


void MyFrame::TimerHello(wxTimerEvent& event)
{
    if (!nuvalink->TimerHello()) {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        SetOnline(false);
    }
}

void MyFrame::SetOnline(bool online)
{

    wxMenuBar* mbar = GetMenuBar();

    this->online = online;
    if (online) {
        tb->EnableTool(idToolRebre, true);
        tb->EnableTool(idToolEnviar, true);
        tb->EnableTool(idToolAplicar, true);
        tb->EnableTool(idToolDisconnect, true);
        tb->EnableTool(idToolVerificar, true);
        if (nuvalink){
            tb->EnableTool(idToolFlash, true);
        }
        tb->EnableTool(idToolEepReset, true);
        tb->EnableTool(idToolConnectar, false);
        tb->EnableTool(idToolConfig, false);            // configurar connexio a false
        mbar->Enable(idMenuConnect, false);
        mbar->Enable(idMenuRebre, true);
        mbar->Enable(idMenuEnviar, true);
        mbar->Enable(idMenuEnviarCh, true);
        mbar->Enable(idMenuRxFlash, true);
        if ((nuvalink) && (nuvalink->privlevel>=2)){
            mbar->Enable(idMenuTxFlash, true);
            mbar->Enable(idMenuMngPacket,true);
        }
    } else {
        tb->EnableTool(idToolRebre, false);
        tb->EnableTool(idToolEnviar, false);
        tb->EnableTool(idToolAplicar, false);
        tb->EnableTool(idToolDisconnect, false);
        tb->EnableTool(idToolVerificar, false);
        tb->EnableTool(idToolFlash, false);
        tb->EnableTool(idToolEepReset, false);
        tb->EnableTool(idToolConfig, true);            // configurar connexio a true
        if (instOberta) {
            tb->EnableTool(idToolConnectar, true);
            mbar->Enable(idMenuConnect, true);
        }
        else {
            tb->EnableTool(idToolConnectar, false);
            mbar->Enable(idMenuConnect, false);
        }

        mbar->Enable(idMenuRebre, false);
        mbar->Enable(idMenuEnviar, false);
        mbar->Enable(idMenuEnviarCh, false);
        mbar->Enable(idMenuRxFlash, false);
        mbar->Enable(idMenuTxFlash, false);
        mbar->Enable(idMenuMngPacket,false);
    }
}

//Activa o desactiva els botons de l atool bar i els menus en funcio de si
//hi ha una instalacio oberta o no
void MyFrame::SetInstOberta(bool oberta)
{
    wxMenuBar* mbar = GetMenuBar();

    this->instOberta = oberta;
    if (oberta) {
        tb->EnableTool(idToolNew, false);
        tb->EnableTool(idToolOpen, false);
        tb->EnableTool(idToolSave, true);
        tb->EnableTool(idToolSaveAs, true);
        tb->EnableTool(idToolClose, true);
        tb->EnableTool(idToolConfig, true);
        tb->EnableTool(idToolVerificar, true);
        tb->EnableTool(idToolReset, true);
        mbar->Enable(idMenuSave, true);
        mbar->Enable(idMenuSaveAs, true);
        mbar->Enable(idMenuClose, true);
        mbar->Enable(idMenuConConfig, true);
    }
    else {
        tb->EnableTool(idToolNew, true);
        tb->EnableTool(idToolOpen, true);
        tb->EnableTool(idToolSave, false);
        tb->EnableTool(idToolSaveAs, false);
        tb->EnableTool(idToolClose, false);
        tb->EnableTool(idToolConfig, false);
        tb->EnableTool(idToolVerificar, false);
        tb->EnableTool(idToolReset, false);
        mbar->Enable(idMenuSave, false);
        mbar->Enable(idMenuSaveAs, false);
        mbar->Enable(idMenuClose, false);
        mbar->Enable(idMenuConConfig, false);
    }

    //Tambe actualitzem els botons de conexio ja que poden variar en funcio
    //de si hiha una instalacio oberta o no
    SetOnline(online);
}

void MyFrame::OnConConfig(wxCommandEvent& event)
{
    string numTelf="", portCom="", host="", port="";
    string serverhost="", serverport="";
    bool callback=false, especial=false;
    panelEdit->ApplyValues();

    ConfigDialog* dialog = new ConfigDialog(this);
    dialog->SetCfg(&nuvalink->config);
    int tipus=dialog->ShowModal();
    delete dialog;
    if (tipus>=0){
        nuvalink->config.SetConnectionType(tipus);
    }else{
        return;
    }

    if ((tipus == TYPE_TCPIP_CONNECTION) || (tipus==TYPE_CLOUD_CONNECTION)) {
        CData *dades = nuvalink->install.GetDataObj();
        string ipDefecte;
        string msgip=Lang->GetAppMiss("MISS_DIALOG_ASK_IP").c_str();

        if (tipus==TYPE_CLOUD_CONNECTION){
            ipDefecte= nuvalink->config.GetParamTcpIp("ipclouddest");
            serverhost=host=ipDefecte;
        }else{
            ipDefecte = dades->gethost();
            wxString ip = wxGetTextFromUser(_wx(msgip), Lang->GetAppMiss("MISS_DIALOG_TIT_IP"), _wx(ipDefecte));
            host = (string)ip.c_str();
            if (host == "")
                return;
        }

        string portDefecte;
        string msgport=Lang->GetAppMiss("MISS_DIALOG_ASK_IPPORT").c_str();

        if (tipus==TYPE_CLOUD_CONNECTION){
            portDefecte = nuvalink->config.GetParamTcpIp("ipcloudport");
        }else{
            wxString p = wxGetTextFromUser(_wx(msgport), Lang->GetAppMiss("MISS_DIALOG_TIT_IPPORT"), _wx(portDefecte));
            port = (string)p.c_str();
            if (port == "")
                return;
        }
    }

    if (!nuvalink->SetConnectionParams(tipus, sToI(portCom), numTelf, callback, especial, host, sToI(port),serverhost,sToI(serverport))) {
        string error = nuvalink->GetLastError();
        wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
    }

    panelEdit->UpdateValues();
    return;
}

void MyFrame::OnButtLog(wxCommandEvent& event)
{
    nuvalink->myLog.privlevel=nuvalink->privlevel;

    if (tb->GetToolState(idToolLog)) {
        nuvalink->myLog.refresh=true;
        splitter1->SplitHorizontally(splitter, control, -100);
        control->Show(true);
        Layout();
    }
    else {
        splitter1->Unsplit(control);
        control->Show(false);
        Layout();
    }
//    }
}

void MyFrame::OnButtReset(wxCommandEvent& event)
{

    wxMessageDialog *msgDlg = new wxMessageDialog(this, Lang->GetAppMiss("MISS_DIALOG_ASK_RESETVAL"),
    Lang->GetAppMiss("MISS_DIALOG_TIT_RESET"), wxYES_NO);

    int result = msgDlg->ShowModal();

    if (result == wxID_YES) {
        wxTreeItemId id = Tree->GetSelection();
        if (!nuvalink->ResetGroup(id)) {
            string error = nuvalink->GetLastError();
            wxMessageBox(_wx(error), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        }
    }
}


void MyFrame::OnButtFlash(wxCommandEvent& event)
{

    wxMessageDialog *msgDlg = new wxMessageDialog(this, Lang->GetAppMiss("MISS_DIALOG_ASK_COPYEEPFLASH"),
    Lang->GetAppMiss("MISS_DIALOG_TIT_RESET"), wxYES_NO);

    int result = msgDlg->ShowModal();

    if (result == wxID_YES) {
        nuvalink->SetEepToFlash();
    }
}

void MyFrame::OnButtEep(wxCommandEvent& event)
{
    wxMessageDialog *msgDlg = new wxMessageDialog(this, Lang->GetAppMiss("MISS_DIALOG_ASK_COPYFLASHEEP"),
    Lang->GetAppMiss("MISS_DIALOG_TIT_RESET"), wxYES_NO);

    int result = msgDlg->ShowModal();

    if (result == wxID_YES) {
        nuvalink->SetFlashToEep();
    }
}

void MyFrame::OnButtEepReset(wxCommandEvent& event){

    wxMessageDialog *msgDlg = new wxMessageDialog(this, Lang->GetAppMiss("MISS_DIALOG_ASK_EEPRESET"),
    Lang->GetAppMiss("MISS_DIALOG_TIT_RESET"), wxYES_NO);

    int result = msgDlg->ShowModal();

    if (result == wxID_YES) {
        nuvalink->SetEepReset();
    }
}


//Preguntem a nuvalink quin menu hem de mostrar en funcio del que estigui
//seleccionat a l'Tree
void MyFrame::OnFloatMnu(wxTreeEvent& event)
{
    wxTreeItemId id = Tree->GetSelection();
    int menu = nuvalink->FloatMnu(id);
    switch (menu) {
        case MENU_FLOTANT_GRUP:
            FloatMnuGrup();
            break;
        case MENU_FLOTANT_INSTALACIO:
            FloatMnuInstalacio();
            break;
        case MENU_FLOTANT_REBRE:
            FloatMnuRebre();
            break;
        case MENU_FLOTANT_MODEL:
            FloatMnuModel();
            break;
        default:
            break;
    }
}

void MyFrame::FloatMnuInstalacio()
{
    //Menu Instalacio
	wxMenu* fileMenu = new wxMenu("");
	fileMenu->Append(idMenuNew, Lang->GetAppMiss("MISS_MNU_INST_NEW"), Lang->GetAppMiss("MISS_MNU_INST_NEW_DESCR"));
	fileMenu->Append(idMenuOpen, Lang->GetAppMiss("MISS_MNU_INST_OPEN"), Lang->GetAppMiss("MISS_MNU_INST_OPEN_DESCR"));
	fileMenu->Append(idMenuSave, Lang->GetAppMiss("MISS_MNU_INST_SAVE"), Lang->GetAppMiss("MISS_MNU_INST_SAVE_DESCR"));
	fileMenu->Append(idMenuSaveAs, Lang->GetAppMiss("MISS_MNU_INST_SAVEAS"), Lang->GetAppMiss("MISS_MNU_INST_SAVEAS_DESCR"));
	fileMenu->AppendSeparator();

    PopupMenu(fileMenu);
    delete fileMenu;
}

void MyFrame::FloatMnuModel()
{
	wxMenu* fileMenu = new wxMenu("");
	fileMenu->Append(idMenuChangeModelName, Lang->GetAppMiss("MISS_MNU_INST_MODELNAME"), Lang->GetAppMiss("MISS_MNU_INST_MODELNAME_DESCR"));
	fileMenu->AppendSeparator();

    PopupMenu(fileMenu);
    delete fileMenu;
}

void MyFrame::FloatMnuGrup()
{
    //Menu Grup
	wxMenu* fileMenu = new wxMenu("");
	fileMenu->Append(idToolRebre, Lang->GetAppMiss("MISS_MNU_CON_RECV"), Lang->GetAppMiss("MISS_MNU_CON_RECV_DESCR"));
	fileMenu->Append(idToolEnviar, Lang->GetAppMiss("MISS_MNU_CON_SEND"), Lang->GetAppMiss("MISS_MNU_CON_SEND_DESCR"));
	fileMenu->Append(idToolEnviarCh, Lang->GetAppMiss("MISS_MNU_CON_SENDCH"), Lang->GetAppMiss("MISS_MNU_CON_SENDCH_DESCR"));
	fileMenu->Append(idToolVerificar, Lang->GetAppMiss("MISS_TOOLB_CON_VERIFY"), Lang->GetAppMiss("MISS_TOOLB_CON_VERIFY_DESCR"));
	fileMenu->Append(idToolReset, Lang->GetAppMiss("MISS_TOOLB_CON_RESET"), Lang->GetAppMiss("MISS_TOOLB_CON_RESET_DESCR"));

    if (!online) {
        fileMenu->Enable(idToolRebre, false);
        fileMenu->Enable(idToolEnviar, false);
        fileMenu->Enable(idToolEnviarCh, false);
        fileMenu->Enable(idToolVerificar, false);
    }

    PopupMenu(fileMenu);
    delete fileMenu;
}

void MyFrame::FloatMnuRebre()
{
    //Menu Rebre
	wxMenu* fileMenu = new wxMenu("");
	fileMenu->Append(idToolRebre, Lang->GetAppMiss("MISS_MNU_CON_RECV"), Lang->GetAppMiss("MISS_MNU_CON_RECV_DESCR"));

    if (!online) {
        fileMenu->Enable(idToolRebre, false);
    }


    PopupMenu(fileMenu);
    delete fileMenu;
}

void MyFrame::SetTitleBar()
{

    wxString titlebar=fileName + _wx(" - ");
    titlebar+=(nuvalink->NomInstal);
    wxString connectState;
    if(online)
    {
        switch(nuvalink->connecthandler.GetTypeCon())
        {
            case 1:connectState=" - USB : ";
                    break;
            case 2:connectState=" - RS485 : ";
                    break;
            case 4:connectState=" - TCP/IP : ";
                    break;
            case 5:connectState=" - CLOUD ";
                    break;
            default:
                    break;
        }

        connectState+=Lang->GetAppMiss("MISS_DIALOG_CON_CONNECTED");
        if(nuvalink->TeCobertura())
        {
            int cob=nuvalink->GetCobertura();
            if(cob>=0)
            {
                connectState+=" - ";
                connectState+=Lang->GetAppMiss("MISS_PANEL_COVER");
                connectState+=" : ";
                connectState+=_wx(GetStrVal(cob)) + "%";
            }
        }
    }
    else{
        connectState=" - ";
        connectState+=Lang->GetAppMiss("MISS_DIALOG_CON_DISCONNECTED");
    }

    titlebar+=connectState;
    titlebar+="NuvaLink";
    SetTitle(_(titlebar));
}

string MyFrame::GetStrVal(int intConvert) {
  string strReturn;

  std::stringstream out;
  out << intConvert;
  strReturn = out.str();

  return(strReturn);
}



// ################# Dialog Box Manage Frame ################
// # Manage saved frame                                     #
// #   Read content of tramas.sav file.                     #
// #             -1 ComboBox                                #
// #             -1 TextCtrl                                #
// #             -4 Buttons                                 #
// #                                                        #
// ##########################################################



// ---- DManageFrame Events Table ----
// Create an event when pushing Add Button
BEGIN_EVENT_TABLE(DManageFrame, wxDialog)
	EVT_BUTTON(idBotoAddFrame, DManageFrame::OnButtAddFrame)
	EVT_BUTTON(idBotoModifyFrame, DManageFrame::OnButtModifyFrame)
	EVT_BUTTON(idBotoDeleteFrame, DManageFrame::OnButtDeleteFrame)
	EVT_BUTTON(idBotoSendFrame, DManageFrame::OnButtSendFrame)
	EVT_TEXT(idMDesignator, DManageFrame::OnKeyComboboxChanged)
	EVT_TEXT(idMFrame, DManageFrame::OnKeyTextCtrlChanged)
END_EVENT_TABLE()

// ---- Manage Frame Dialog Box ----
DManageFrame::DManageFrame(wxWindow* parent, wxWindowID id, const wxString& title,Cnuvalink  *cnuvalink)
	: wxDialog(parent, id, title)
{

    // ----- reading trama.sav
    int SepIndex;


    this->nuvalink=cnuvalink;
    wxString PartFrame;
    wxString PartDesignator;
    FNoChangeLabel=1;
    wxFileInputStream input("tramas.sav"); //open the file
    if(input.Ok()){
        wxTextInputStream text(input);
        while(!input.Eof()){               // read line by line
            wxString line = text.ReadLine();
            if ((SepIndex=line.Find(" <=> "))!=-1){
                PartDesignator=PartFrame=line;
                PartDesignator.Remove(SepIndex);
                PartFrame.Remove(0,SepIndex+5);
                ListDesignators.Add(PartDesignator);
                ListFrames.Add(PartFrame);
            }
        }
    }
    // -END- reading trama.sav
    int width=250;
    int height=180;
    Mindex=0;
    this->SetSize(width, height);
    MDesignator = new wxComboBox(this, idMDesignator, ListDesignators.Item(Mindex), wxPoint(4,20), wxSize(220,20),ListDesignators);
    wxStaticText* MDesignatorTitle = new wxStaticText(this, -1, "Designator", wxPoint(4,4));
    MFrame = new wxTextCtrl(this, idMFrame, ListFrames.Item(Mindex), wxPoint(4,70), wxSize(220,20));
    wxStaticText* MFrameTitle = new wxStaticText(this, -1, "Frame (in hex. without CRC)", wxPoint(4,54));
    bSendFrame = new wxButton(this, idBotoSendFrame, "Send", wxPoint((width/2)-120,100), wxSize(50, 20));
    bAddFrame = new wxButton(this, idBotoAddFrame, "Add", wxPoint((width/2)-60,100), wxSize(50, 20));
    bModifyFrame = new wxButton(this, idBotoModifyFrame, "Modify", wxPoint((width/2),100), wxSize(50, 20));
    bDeleteFrame = new wxButton(this, idBotoDeleteFrame, "Delete", wxPoint((width/2)+60,100), wxSize(50, 20));
    FBoutonCancel = new wxButton(this,wxID_CANCEL,"Close", wxPoint((width/2)-60,130), wxSize(120, 20));
    FNoChangeLabel=0;
}

// ---- When changing Designator ComboBox content ----
// Change Close label button to "Cancel" if designator does not exist
// else update Frame TextCtrl
void DManageFrame::OnKeyComboboxChanged(wxCommandEvent& event)
{
    FNoChangeLabel=1;
    if((MDesignator->GetSelection())==wxNOT_FOUND){
        FBoutonCancel->SetLabel("Cancel");
    }else{
        MFrame->SetValue(ListFrames.Item(MDesignator->GetSelection()));
        Mindex=MDesignator->GetSelection();
        FBoutonCancel->SetLabel("Close");
    }
    FNoChangeLabel=0;
}

// ---- When pushing Delete Button ----
// Delete current Frame of tramas.sav file
void DManageFrame::OnButtDeleteFrame(wxCommandEvent& event){
    FNoChangeLabel=1;
    int i;
    int probe;
    ListFrames.RemoveAt(Mindex);
    ListDesignators.RemoveAt(Mindex);
    MDesignator->Delete(Mindex);
    if(!ListDesignators.Count()){
        MDesignator->SetValue("");
        MFrame->SetValue("");
    }else{
        if(ListDesignators.Count()<Mindex+1){
            Mindex--;
        }
        MDesignator->SetValue(ListDesignators.Item(Mindex));
        MFrame->SetValue(ListFrames.Item(Mindex));
    }
    // ----- Writing file -----
    wxFileOutputStream output("tramas.sav");
    if(output.Ok()){
        wxTextOutputStream lineout(output);
        for(i=0;i<ListDesignators.Count();i++){
            lineout << ListDesignators.Item(i);
            lineout << " <=> ";
            lineout << ListFrames.Item(i);
            lineout << "\n";
        }
        FBoutonCancel->SetLabel("Close");
    }else{
        wxMessageBox("Impossible to save frame\nCheck if tramas.sav file is present", "Error");
    }
    // -END- Writing file -----
    FNoChangeLabel=0;
}

// ---- When pushing Add Button ----
// Add Frame to tramas.sav file
void DManageFrame::OnButtAddFrame(wxCommandEvent& event){
    int i;
    FNoChangeLabel=1;
    for(i=0;i<ListDesignators.Count();i++){
            if(ListDesignators[i]==MDesignator->GetValue()){
                wxMessageBox("Sorry this Designator already exist!\nYou can not use twice the same name.", "Error");
                return;
            }
    }
    ListDesignators.Add(MDesignator->GetValue());
    ListFrames.Add(MFrame->GetValue());
    MDesignator->Append(MDesignator->GetValue());
    // ----- Writing file -----
    wxFileOutputStream output("tramas.sav");
    if(output.Ok()){
        wxTextOutputStream lineout(output);
        for(i=0;i<ListDesignators.Count();i++){
            lineout << ListDesignators.Item(i);
            lineout << " <=> ";
            lineout << ListFrames.Item(i);
            lineout << "\n";
        }
        FBoutonCancel->SetLabel("Close");
    }else{
        wxMessageBox("Impossible to save frame\nCheck if tramas.sav file is present", "Error");
    }
    // -END- Writing file -----
    FNoChangeLabel=0;
}

// ---- When pushing Modify Button ----
// Modify Frame in tramas.sav file
void DManageFrame::OnButtModifyFrame(wxCommandEvent& event){
    int i;
    FNoChangeLabel=1;
    for(i=0;i<ListDesignators.Count();i++){
            if((ListDesignators[i]==MDesignator->GetValue()) && (Mindex!=i)){
                wxMessageBox("Sorry this Designator already exist!\nYou can not use twice the same name.\nEdit the frame '"+ListDesignators[i]+"' that already exist.", "Error");
                return;
            }
    }
    ListFrames.RemoveAt(Mindex);
    ListFrames.Insert(MFrame->GetValue(),Mindex);
    ListDesignators.RemoveAt(Mindex);
    ListDesignators.Insert(MDesignator->GetValue(),Mindex);
    MDesignator->Insert(MDesignator->GetValue(),Mindex);
    MDesignator->Delete(Mindex+1);
    MDesignator->SetValue(MDesignator->GetValue());
    // ----- Writing file -----
    wxFileOutputStream output("tramas.sav");
    if(output.Ok()){
        wxTextOutputStream lineout(output);
        for(i=0;i<ListDesignators.Count();i++){
            lineout << ListDesignators.Item(i);
            lineout << " <=> ";
            lineout << ListFrames.Item(i);
            lineout << "\n";
        }
        FBoutonCancel->SetLabel("Close");
    }else{
        wxMessageBox("Impossible to save frame\nCheck if tramas.sav file is present", "Error");
    }
    // -END- Writing file -----
    FNoChangeLabel=0;
}

// ---- When pushing Modify Button ----
// Modify Frame in tramas.sav file
void DManageFrame::OnButtSendFrame(wxCommandEvent& event){

    int i; // used in "for" loops
    int ByteWritten,FrameSize;
    bool AutoFrame=0;
    unsigned char buffer[128];  // hex buffer to send
    wxString FrameToSend;       // Frame readed in ASCII, wxString
    const char* BuffToSend;     // Frame converted in ASCII, char*

    FrameToSend = MFrame->GetValue(); // read TextControl

    FrameSize=((FrameToSend.Length())/2);    // calculate size of readed frame
    BuffToSend = FrameToSend.ToAscii();      // convert to ASCII char buffer

    // ------- Verify and convert frame ------
    if (BuffToSend[0]>='A'){
        string fr=FrameToSend.c_str();
        nuvalink->SendAsciiFrame(fr,FrameToSend.Length());
    }else{

        for(i=0;i<FrameSize;i++){
            buffer[i]=(ascii2nibble(BuffToSend[i*2]) <<4) & 0xF0;
            buffer[i]|=ascii2nibble(BuffToSend[(i*2)+1]);
        }

        if (!nuvalink->TxPacket(buffer[0],&buffer[1],FrameSize-1)) {
            string error = nuvalink->GetLastError();
            wxMessageBox(_wx(error), "MISS_DIALOG_TIT_ERROR");
        }
    }

}


// ---- When changing text in Frame TextCtrl ----
// Change Close label button to "Cancel"
void DManageFrame::OnKeyTextCtrlChanged(wxCommandEvent& event)
{
    if(!FNoChangeLabel){
        FBoutonCancel->SetLabel("Cancel");
    }
}
