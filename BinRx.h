
#ifndef _BINRX_INCLUDED
#define _BINRX_INCLUDED

#define     LONG_BUFF_BIN                   256
#define     BINRX_TIMEOUT                   20      // 10 seconds for killing session
#define     LONG_DATA_BINRX(sizerx)         (sizerx-(sizeof(TBinFrStr)-LONG_BUFF_BIN))

typedef struct
{
    string code;
    int RxLong;
    int RxSize;
    int RxZone;
    int SeqNum;
    int TimeOut;
    int ns;
    wxString IP;
    int datapending;
    unsigned char bindata[1024000];         // up to 1M binary data!!!

    wxString FileName;
    int     BinCnt;
    unsigned char VersionH;
    unsigned char VersionL;
    unsigned char Ev;
    unsigned char Protocol;
    wxString Dir;
    BOOL    OnRSynch;
}TBinRx;


typedef struct{
    BYTE 	Header;
	BYTE	Protocol;
	BYTE    SeqNum[2];
	BYTE    Code[3];
	BYTE    Ctrl;
	BYTE    Buff[LONG_BUFF_BIN];
#ifdef _PIC_CRC
	BYTE 	Crc[2];
#endif
}TBinFrStr;


// ---------------------------------------------- BTP flags ---------------------
#define  M_BTP_CRC_ADD				0x80
#define  M_BTP_NEED_ACK				0x40
#define  M_BTP_REQUEST				0x20		// This a request and must be accepted from master even on disarmed.
#define  M_BTP_RES					0x10

	// for back compatibility
	#define 	M_BTP_TEST				M_BTP_REQUEST
	#define  M_BTP_TAMPER			0x10

#define  M_BTP_PROTOV				0x0F

// ------------------------------------------------- BTP protocols ------------------------------
#define BTP_JPEG							0
#define BTP_JPEG_QVGA_NOHEADER				1
#define BTP_JPEG_VGA_NOHEADER				2
#define BTP_JPEG_QCIF_NOHEADER				3
#define BTP_JPEG_QVGA_NOHEADERCX			4
#define BTP_JPEG_VGA_NOHEADERCX				5
#define BTP_MJPEG_QVGA_CX					6
#define BTP_MJPEG_VGA_CX					7


#define SIZE_JPEG_HEADER					624
#define SIZE_JPEG_HEADERCX					686
#define M_BTP_PROTOCOL						0x0F
#define CURRENT_BTP_PROTOCOL				BTP_JPEG_QVGA_NOHEADER

// -------------------------------------------------- BTP channels -------------------------------
#define KERPIC_CH_R0				0
#define KERPIC_CH_SOCKET		    1		// Sent to Socket TCP-UDP socket or CSD
#define KERPIC_CH_LIBCOMM		    2		// PIC when sending though RS485 or Wireless
#define KERPIC_CH_MODEM			    3		// PIC sending to remote modem!
#define KERPIC_CH_WS				4		// PIC Sending through Webserver
#define KERPIC_CH_SMTP			    5		// PIC sending to SMTP.
#define KERPIC_CH_R1				6
#define KERPIC_CH_R2				7

#define KERPIC_CH_SENT			0x08	// internal to the queue.

#ifdef _SENSOR
	#define LAST_BTP_STREAM_CHANNEL	KERPIC_CH_SMTP
#else
	#define LAST_BTP_STREAM_CHANNEL	KERPIC_CH_MODEM
#endif
// -------------------------------------------------- BTP VERSION -------------------------------
#define BTP_VERSION		0x0107
	#define BTP_VERSION_H	0x01
	#define BTP_VERSION_L	0x07
// --------------------------------------------------- BTP events ---------------------------------
#define BTP_EV_PREALARM		0
#define BTP_EV_ENTRYPATH	1
#define BTP_EV_ALARM		2
#define BTP_EV_TEST			3
#define BTP_EV_TAMPER		4
#define BTP_EV_ALARMCANCEL  5
#define BTP_EV_ANALOGALARM  6

#define NUM_BINRX_SESSIONS  5


#endif
