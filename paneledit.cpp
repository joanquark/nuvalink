#include "paneledit.h"

CPanelEdit::CPanelEdit(wxWindow* parent, Cnuvalink*  nuva, wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CLanguage* lang)
{

    nuvalink=nuva;
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,400,300);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(400,300);
    }

    this->Lang = lang;
    titol=0;
    panelEstat=0;
    //panelBitGrup=0;
    panelVR=0;
    panelCfgVR=0;
    panelVRUser=0;
    panelVROut=0;
    panelBeaconSt=0;
    panelMem=0;
    panelAgro=0;
    panelCfgVRUser=0;
    panelCfgVROut=0;
    panelEvents=0;
    panelVerify=0;
    panelVModem=0;
    panelVCOM=0;
    panelDades=0;
    panelFlashGrup=0;

    init();
    model="";
    versio="";
    privlevel=MASTER_PRIVLEVEL;
}

CPanelEdit::~CPanelEdit()
{
    Clear();
}

void CPanelEdit::Clear()
{
    for (int i=0; i<vPanelsCodi.size(); i++) {
        CPanelCodi *panelCodi = vPanelsCodi[i];
        delete panelCodi;
    }

    vPanelsCodi.clear();

    for (int i=0; i<vPanelsMesura.size(); i++) {
        CPanelMesura *panelMesura = vPanelsMesura[i];
        delete panelMesura;
    }

    vPanelsMesura.clear();

    if (panelEstat) {
        delete panelEstat;
        panelEstat=0;
    }

    if (panelVR) {
        delete panelVR;
        panelVR=0;
    }

    if (panelCfgVR) {
        delete panelCfgVR;
        panelCfgVR=0;
    }

    if (panelVRUser){
        delete panelVRUser;
        panelVRUser=0;
    }

    if (panelCfgVRUser){
        delete panelCfgVRUser;
        panelCfgVRUser=0;
    }

    if (panelVROut){
        delete panelVROut;
        panelVROut=0;
    }

    if (panelCfgVROut){
        delete panelCfgVROut;
        panelCfgVROut=0;
    }

    if (panelBeaconSt){
        delete panelBeaconSt;
        panelBeaconSt=0;
    }
    if (panelMem){
        delete panelMem;
        panelMem=0;
    }

    if (panelAgro){
        delete panelAgro;
        panelAgro=0;
    }

    if (panelEvents) {
        delete panelEvents;
        panelEvents=0;
    }
    if (panelVerify) {
        delete panelVerify;
        panelVerify=0;
    }
    if (panelVModem){
        delete panelVModem;
        panelVModem=0;
    }
    if (panelVCOM){
        delete panelVCOM;
        panelVCOM=0;
    }
    if (panelDades) {
        delete panelDades;
        panelDades=0;
    }

    if (panelFlashGrup) {
        delete panelFlashGrup;
        panelFlashGrup=0;
    }

    x_pos = 30;
    ClrTitle();

    int i=0;
    while (vSizer->Detach(i++));
    vSizer->Layout();
    Layout();
    FitInside();
}

wxPoint& CPanelEdit::VwXSetwxPoint(long x,long y)
{
    m_tmppoint.x=x;
    m_tmppoint.y=y;
    return m_tmppoint;
}

wxSize& CPanelEdit::VwXSetwxSize(long w,long h){
    m_tmpsize.SetWidth(w);
    m_tmpsize.SetHeight(h);
    return m_tmpsize;
}

void CPanelEdit::init()
{
    x_pos = 30;
    SetBackgroundColour(wxColour(0xf0,0xf0,0xf0));//wxColour(0xff,0xff,0xff));

    vSizer = new wxBoxSizer( wxVERTICAL );
    SetSizer( vSizer );
    SetScrollRate(20,20);
    Scroll(0,0);
    Show(true);
    Refresh();
}

int CPanelEdit::GenPanelCodi(CCodi *codi,Cnuvalink* nuvalink,unsigned int tzona,int priv)
{
    privlevel=priv;
    CPanelCodi *panelCodi = new CPanelCodi(this,-1,nuvalink,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang);
    panelCodi->SetCodi(codi,model,versio,privlevel, tzona);
    int height, width;
    panelCodi->GetSize(&width, &height);
    x_pos += height;
    vSizer->Add( panelCodi, wxSizerFlags(0).Expand().Border(wxLEFT|wxRIGHT|wxBOTTOM, 5));
    vSizer->Layout();
    FitInside();
    vPanelsCodi.push_back(panelCodi);
    return vPanelsCodi.size()-1;
}

bool CPanelEdit::GenPanelStatus(CStatus *estat, Cnuvalink *nuvalink,CGroup *grupZAlias,CGroup *grupAAlias,CGroup *grupOAlias,string ver, string id)
{
/*    panelEstat = new CPanelEstat(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang);
#ifdef _ALIAS_ESTAT
    panelEstat->SetGrups(grupZAlias,grupAAlias,grupOAlias);
#endif
    panelEstat->SetStatus(estat);
    panelEstat->nuvalink = nuvalink;
    PanelVersion=ver;
    PanelId=id;
    vSizer->Add( panelEstat, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();

    panelEstat->PanelVersion=ver;
    panelEstat->PanelId=id;*/
    panelEstat = new EstatPanel(this);
    PanelVersion=ver;
    PanelId=id;
    panelEstat->SetStatus(estat,nuvalink,Lang,id,ver);
    vSizer->Add( panelEstat, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
    return true;
}


bool CPanelEdit::CreaPanelFlashGrup(CGroupFlash *grupFlash)
{
    panelFlashGrup = new CPanelFlashGrup(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang);
    panelFlashGrup->SetFlashGrup(grupFlash);
    vSizer->Add( panelFlashGrup, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
    return true;
}


//bool CPanelEdit::GenPanelWireless(CStatus *estat)
bool CPanelEdit::GenPanelBeacons(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias)
{
    PanelVersion=ver;
    PanelId=id;

    panelBeaconSt = new BeaconPanel(this);
    panelBeaconSt->SetNuvaLink(nuvalink,Lang);
    vSizer->Add( panelBeaconSt, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
    return true;
}

//bool CPanelEdit::GenPanelWireless(CStatus *estat)
bool CPanelEdit::GenPanelMem(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias)
{
    PanelVersion=ver;
    PanelId=id;

    panelMem = new MemPanel(this);
    panelMem->SetNuvaLink(nuvalink,Lang);
    vSizer->Add( panelMem, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
    return true;
}

//bool CPanelEdit::GenPanelWireless(CStatus *estat)
bool CPanelEdit::GenPanelAgro(CData *data,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias)
{
    PanelVersion=ver;
    PanelId=id;

    panelAgro = new AgroPanel(this);

    panelAgro->SetNuvaLink(nuvalink,data);
    vSizer->Add( panelAgro, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
    return true;
}

//bool CPanelEdit::GenPanelWireless(CStatus *estat)
bool CPanelEdit::GenPanelWireless(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias)
{
/*    PanelVersion=ver;
    PanelId=id;

    panelVR = new CPanelVR(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang,nuvalink,estat,ver,id);

    if (sToI(PanelVersion)>=520)
        panelCfgVR = new CPanelCfgVR(this, -1, wxPoint(8, 8), wxSize(700, 140),-1,"pcfgvrusr",Lang,nuvalink,-1);


    vSizer->Add( panelVR, wxSizerFlags(1).Expand().Border(wxALL, 5));
    if (sToI(PanelVersion)>=520)
        vSizer->Add( panelCfgVR, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();*/
    return true;
}

bool CPanelEdit::GenPanelCfgWireless(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias,int fzone)
{
/*    PanelVersion=ver;
    PanelId=id;

    panelCfgVR = new CPanelCfgVR(this, -1, wxPoint(8, 8), wxSize(700, 140),-1,"pcfgvr",Lang,nuvalink,fzone);
//    new CPanelCfgVR(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang,nuvalink,estat,ver,id);

    vSizer->Add( panelCfgVR, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();*/
    return true;
}

void CPanelEdit::ReadPanelVR()
{
 /*   if (panelCfgVR)
        panelCfgVR->Read();*/
}


void CPanelEdit::ReadPanelUserVR()
{
/*    if (panelCfgVRUser)
        panelCfgVRUser->Read();*/
}

void CPanelEdit::ReadPanelOutVR()
{
 /*   if (panelCfgVROut)
        panelCfgVROut->Read();*/
}

//bool CPanelEdit::GenPanelWireless(CStatus *estat)
bool CPanelEdit::GenPanelWirelessUser(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupUAlias)
{
/*
    PanelVersion=ver;
    PanelId=id;

    panelVRUser = new CPanelVRUser(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang,nuvalink,estat,ver,id);
    panelVRUser->SetStatus(estat);

    if (sToI(PanelVersion)>=520)
        panelCfgVRUser = new CPanelCfgVRUser(this, -1, wxPoint(8, 8), wxSize(700, 140),-1,"pcfgvrusr",Lang,nuvalink,-1);


    vSizer->Add( panelVRUser, wxSizerFlags(1).Expand().Border(wxALL, 5));
    if (sToI(PanelVersion)>=520)
        vSizer->Add( panelCfgVRUser, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();

*/
    return true;
}


bool CPanelEdit::GenPanelCfgWirelessUser(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupZAlias,int user)
{
 /*   PanelVersion=ver;
    PanelId=id;

    panelCfgVRUser = new CPanelCfgVRUser(this, -1, wxPoint(8, 8), wxSize(700, 140),-1,"pcfgvrusr",Lang,nuvalink,user);
//    new CPanelCfgVR(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang,nuvalink,estat,ver,id);
    vSizer->Add( panelCfgVRUser, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();*/
    return true;
}

bool CPanelEdit::GenPanelWirelessOut(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupOAlias)
{
 /*   PanelVersion=ver;
    PanelId=id;

    panelVROut = new CPanelVROut(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang,nuvalink,estat,ver,id);
    panelVROut->SetStatus(estat);

    if (sToI(PanelVersion)>=520)
        panelCfgVROut = new CPanelCfgVROut(this, -1, wxPoint(8, 8), wxSize(700, 140),-1,"pcfgvrout",Lang,nuvalink,-1);

    vSizer->Add( panelVROut, wxSizerFlags(1).Expand().Border(wxALL, 5));
    if (sToI(PanelVersion)>=520)
        vSizer->Add( panelCfgVROut, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
*/
    return true;
}


bool CPanelEdit::GenPanelCfgWirelessOut(CStatus *estat,Cnuvalink *nuvalink,string ver,string id,CGroup *grupOAlias,int out)
{
 /*   PanelVersion=ver;
    PanelId=id;

    panelCfgVROut = new CPanelCfgVROut(this, -1, wxPoint(8, 8), wxSize(700, 140),-1,"pcfgvrusr",Lang,nuvalink,out);
//    new CPanelCfgVR(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang,nuvalink,estat,ver,id);
    vSizer->Add( panelCfgVROut, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();*/
    return true;
}


bool CPanelEdit::GenPanelVerify(Cnuvalink *nuvalink,CModel* sel,string ver,string id)
{
    PanelVersion=ver;
    PanelId=id;

    panelVerify = new CPanelVerify(nuvalink,sel,this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang/*,nuvalink,ver,id*/);

    vSizer->Add( panelVerify, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();

    return true;
}

bool CPanelEdit::GenPanelVModem(CProtocolNIOT* protocolniot, CConfig* conf,CModel* sel,string ver,string id)
{
    PanelVersion=ver;
    PanelId=id;

    panelVModem = new VModemPanel(this,wxID_ANY,wxDefaultPosition,wxDefaultSize);
    panelVModem->SetParams(protocolniot,conf);
    //panelVmodem->SetParams();
    vSizer->Add( panelVModem, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();

    return true;
}

bool CPanelEdit::GenPanelVCOM(Cnuvalink *nuva,CProtocolNIOT* protocolniot, CConfig* conf,CModel* sel,string ver,string id)
{
    PanelId=id;

    panelVCOM = new VCOMPanel(nuva,this,wxID_ANY,wxDefaultPosition,wxDefaultSize);
    panelVCOM->SetParams(protocolniot,conf);
    //panelVmodem->SetParams();
    vSizer->Add( panelVCOM, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();

    return true;
}

bool CPanelEdit::GenPanelEvents(Cnuvalink *nuvalink,CEventList *events,CGroup *grupZAlias,CGroup *grupAAlias, CGroup *grupUAlias, CGroup *grupOAlias,string ver, string id)
{
    PanelVersion=ver;
    PanelId=id;

    panelEvents = new EventsPanel(this,wxID_ANY,nuvalink,Lang,events,id,ver);

//    panelEvents = new CPanelEvents(nuvalink,this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER,"",Lang);
//    panelEvents->PanelVersion=ver;
//    panelEvents->PanelId=id;
#ifdef _ALIAS_ESTAT
        panelEvents->SetGrups(grupZAlias,grupAAlias,grupUAlias,grupOAlias);
#endif
//    panelEvents->SetLlista(events);


    vSizer->Add( panelEvents, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
    return true;
}

bool CPanelEdit::GenPanelDades(CData *dades,wxString *ver, wxString *id)
{
    panelDades = new DadesPanel(this,wxID_ANY,wxDefaultPosition,wxDefaultSize,id,ver,Lang,dades);
    vSizer->Add( panelDades, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Layout();
    FitInside();
    return true;
}

/*bool CPanelEdit::CreaPanelKeyPad()
{
    panelKeyPad = new CPanelKeyPad(this,-1,wxDefaultPosition,wxDefaultSize, wxNO_BORDER);
//    vSizer->Add( panelKeyPad, wxSizerFlags(1).Expand().Border(wxALL, 5));
    vSizer->Add( panelKeyPad, wxSizerFlags(0).Expand().Border(wxLEFT|wxRIGHT|wxBOTTOM, 5));
    vSizer->Layout();
    FitInside();
    return true;
}*/

CCodi *CPanelEdit::GetCodi(int index)
{
    if ((index>=vPanelsCodi.size()) || (index < 0))
        return 0;

    CPanelCodi *panelCodi = vPanelsCodi[index];
    return panelCodi->GetCodi();
}

/*CMesura *CPanelEdit::GetMesura(int index)
{
    if ((index>=vPanelsMesura.size()) || (index < 0))
        return 0;

    CPanelMesura *panelMesura = vPanelsMesura[index];
    return panelMesura->GetMesura();
}*/

CStatus *CPanelEdit::GetStatus()
{
    if (!panelEstat)
        return 0;

    return panelEstat->GetStatus();
}

/*CStatus *CPanelEdit::GetStatusViaRadios()
{
    if (!panelVR)
        return 0;

    return panelVR->GetStatus();
}*/

/*CGroup *CPanelEdit::GetBitGrup()
{
    if (!panelBitGrup)
        return 0;

    return panelBitGrup->GetBitGrup();
}*/

CEventList *CPanelEdit::GetEventList()
{
    if (!panelEvents)
        return 0;

    return panelEvents->GetLlista();
}

CData *CPanelEdit::GetData()
{
    if (!panelDades)
        return 0;

    return panelDades->GetData();
}

/*CGroup *CPanelEdit::GetGroupKeyPad()
{
    if (!panelKeyPad)
        return 0;

    return panelKeyPad->GetGroup();
}
*/

CGroupFlash *CPanelEdit::GetGroupFlash()
{
    if (!panelFlashGrup)
        return 0;

    return panelFlashGrup->GetFlashGrup();
}


void CPanelEdit::SetTitle(string &stitol)
{
    if (!titol) {
        titol = new wxStaticText(this,-1,wxT(""), wxDefaultPosition, wxDefaultSize, wxSIMPLE_BORDER|wxALIGN_CENTER);
        vSizer->Add( titol , wxSizerFlags(0).Expand().Border(wxALL, 5) );
    }
//    titol->SetTitle(wxT("Title"));
    titol->SetLabel(_wx(stitol));
    titol->SetFont(wxFont(14,74,90,90,0,wxT("Arial")));
    vSizer->Layout();
    //hSizer->Layout();
    Refresh();
    FitInside();
}

void CPanelEdit::ClrTitle()
{
    if (titol) delete titol;
    titol=0;
}

//Actualitza els camps de text amb els valors que tenen els CCodi
void CPanelEdit::UpdateValues()
{
    for (int i=0; i<vPanelsCodi.size(); i++) {
        CPanelCodi *panelCodi = vPanelsCodi[i];
        panelCodi->UpdateValues();
    }

/*    for (int i=0; i<vPanelsMesura.size(); i++) {
        CPanelMesura *panelMesura = vPanelsMesura[i];
        panelMesura->UpdateValues();
    }*/

    if (panelEstat) panelEstat->UpdateValues();
//    if (panelBitGrup) panelBitGrup->UpdateValues();
//    if (panelVR) panelVR->UpdateValues();
//    if (panelVRUser) panelVRUser->UpdateValues();
//    if (panelVROut) panelVROut->UpdateValues();
    if (panelEvents) panelEvents->UpdateValues();
    if (panelDades) panelDades->UpdateValues();
    //if (panelKeyPad) panelKeyPad->UpdateValues();
    if (panelFlashGrup) panelFlashGrup->UpdateValues();
#ifdef ESTAT_GRAFIC
    if (panelGrafic) panelGrafic->UpdateValues();
#endif
}

//Els valors que l'usuari ha modificat, els aplica als CCodi corresponents
void CPanelEdit::ApplyValues()
{
   // guardarem el codi si t� prou privilegi per guardarlo: done inside panelCodi->ApplyValues()
    for (int i=0; i<vPanelsCodi.size(); i++) {
        CPanelCodi *panelCodi = vPanelsCodi[i];
        panelCodi->ApplyValues();
    }

/*    for (int i=0; i<vPanelsMesura.size(); i++) {
        CPanelMesura *panelMesura = vPanelsMesura[i];
        panelMesura->ApplyValues();
    }*/

    if (panelEstat) panelEstat->ApplyValues();
//    if (panelBitGrup) panelBitGrup->ApplyValues();
//    if (panelVR) panelVR->ApplyValues();
//    if (panelVRUser) panelVRUser->ApplyValues();
//    if (panelVROut) panelVROut->ApplyValues();
    if (panelEvents) panelEvents->ApplyValues();
    if (panelDades) panelDades->ApplyValues();
    //if (panelKeyPad) panelKeyPad->ApplyValues();
    if (panelFlashGrup) panelFlashGrup->ApplyValues();

}


void CPanelEdit::ReadCodi(CCodi* codi)
{


}

void CPanelEdit::SendCodi(CCodi* codi)
{


}


BEGIN_EVENT_TABLE(CPanelEditEvt, wxEvtHandler)
//[evtEvt]add your code here

//[evtEvt]end your code
END_EVENT_TABLE()

