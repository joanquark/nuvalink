#include "wx_pch.h"
#include "DadesPanel.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(DadesPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(DadesPanel)
//*)

//(*IdInit(DadesPanel)
const long DadesPanel::ID_STATICTEXT1 = wxNewId();
const long DadesPanel::ID_CHOICE1 = wxNewId();
const long DadesPanel::ID_STATICTEXT13 = wxNewId();
const long DadesPanel::ID_CHOICE4 = wxNewId();
const long DadesPanel::ID_STATICTEXT14 = wxNewId();
const long DadesPanel::ID_CHOICE5 = wxNewId();
const long DadesPanel::ID_STATICTEXT3 = wxNewId();
const long DadesPanel::ID_CHOICE2 = wxNewId();
const long DadesPanel::ID_STATICTEXT15 = wxNewId();
const long DadesPanel::ID_TEXTCTRL10 = wxNewId();
const long DadesPanel::ID_STATICTEXT16 = wxNewId();
const long DadesPanel::ID_CHOICE6 = wxNewId();
const long DadesPanel::ID_STATICTEXT17 = wxNewId();
const long DadesPanel::ID_TEXTCTRL11 = wxNewId();
const long DadesPanel::ID_STATICTEXT2 = wxNewId();
const long DadesPanel::ID_TEXTCTRL1 = wxNewId();
const long DadesPanel::ID_STATICTEXT4 = wxNewId();
const long DadesPanel::ID_CHOICE3 = wxNewId();
const long DadesPanel::ID_STATICTEXT5 = wxNewId();
const long DadesPanel::ID_TEXTCTRL2 = wxNewId();
const long DadesPanel::ID_STATICTEXT6 = wxNewId();
const long DadesPanel::ID_TEXTCTRL3 = wxNewId();
const long DadesPanel::ID_STATICTEXT7 = wxNewId();
const long DadesPanel::ID_TEXTCTRL4 = wxNewId();
const long DadesPanel::ID_STATICTEXT8 = wxNewId();
const long DadesPanel::ID_TEXTCTRL5 = wxNewId();
const long DadesPanel::ID_STATICTEXT18 = wxNewId();
const long DadesPanel::ID_TEXTCTRL12 = wxNewId();
const long DadesPanel::ID_STATICTEXT9 = wxNewId();
const long DadesPanel::ID_TEXTCTRL6 = wxNewId();
const long DadesPanel::ID_STATICTEXT10 = wxNewId();
const long DadesPanel::ID_TEXTCTRL7 = wxNewId();
const long DadesPanel::ID_STATICTEXT11 = wxNewId();
const long DadesPanel::ID_TEXTCTRL8 = wxNewId();
const long DadesPanel::ID_STATICTEXT12 = wxNewId();
const long DadesPanel::ID_TEXTCTRL9 = wxNewId();
//*)

BEGIN_EVENT_TABLE(DadesPanel,wxPanel)
	//(*EventTable(DadesPanel)
	//*)
END_EVENT_TABLE()

DadesPanel::DadesPanel(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,wxString *model,wxString *versio,CLanguage *lang,CData *data)
{
    this->dades=data;
    this->Lang=lang;
    this->model=model;
    this->versio=versio;
	BuildContent(parent,id,pos,size);
	UpdateValues();
}

void DadesPanel::BuildContent(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size)
{
	//(*Initialize(DadesPanel)
	wxFlexGridSizer* FlexGridSizer1;
	wxFlexGridSizer* FlexGridSizer2;
	wxFlexGridSizer* FlexGridSizer3;
	wxFlexGridSizer* FlexGridSizer4;
	wxFlexGridSizer* FlexGridSizer5;
	wxStaticBoxSizer* StaticBoxSizer1;
	wxStaticBoxSizer* StaticBoxSizer2;
	wxStaticBoxSizer* StaticBoxSizer3;
	wxStaticBoxSizer* StaticBoxSizer4;

	Create(parent, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(0, 2, 0, 0);
	StaticBoxSizer1 = new wxStaticBoxSizer(wxVERTICAL, this, _("Device"));
	FlexGridSizer2 = new wxFlexGridSizer(0, 2, 0, 0);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("Family"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	FlexGridSizer2->Add(StaticText1, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceFamily = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxSize(149,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	ChoiceFamily->SetSelection( ChoiceFamily->Append(_("SPT")) );
	ChoiceFamily->Append(_("SEC"));
	ChoiceFamily->Append(_("LIFT"));
	ChoiceFamily->Append(_("TRACK"));
	FlexGridSizer2->Add(ChoiceFamily, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText13 = new wxStaticText(this, ID_STATICTEXT13, _("Model"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT13"));
	FlexGridSizer2->Add(StaticText13, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceModel = new wxChoice(this, ID_CHOICE4, wxDefaultPosition, wxSize(149,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE4"));
	FlexGridSizer2->Add(ChoiceModel, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText14 = new wxStaticText(this, ID_STATICTEXT14, _("Version"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT14"));
	FlexGridSizer2->Add(StaticText14, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	ChoiceVer = new wxChoice(this, ID_CHOICE5, wxDefaultPosition, wxSize(149,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE5"));
	FlexGridSizer2->Add(ChoiceVer, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("Connection"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	FlexGridSizer2->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	ConChoice = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxSize(149,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
	ConChoice->SetSelection( ConChoice->Append(_("USB")) );
	ConChoice->Append(_("RS485"));
	ConChoice->Append(_("CSD"));
	ConChoice->Append(_("TCP-IP"));
	ConChoice->Append(_("CLOUD"));
	FlexGridSizer2->Add(ConChoice, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText15 = new wxStaticText(this, ID_STATICTEXT15, _("Ext Power"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT15"));
	FlexGridSizer2->Add(StaticText15, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	ExtPTxt = new wxTextCtrl(this, ID_TEXTCTRL10, _("13.8"), wxDefaultPosition, wxSize(149,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL10"));
	FlexGridSizer2->Add(ExtPTxt, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText16 = new wxStaticText(this, ID_STATICTEXT16, _("Battery"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT16"));
	FlexGridSizer2->Add(StaticText16, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	BattChoice = new wxChoice(this, ID_CHOICE6, wxDefaultPosition, wxSize(149,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE6"));
	BattChoice->SetSelection( BattChoice->Append(_("-")) );
	BattChoice->Append(_("Lead 13.8V"));
	BattChoice->Append(_("LiPo 7.4V"));
	BattChoice->Append(_("LiFePO4 6.6V"));
	BattChoice->Append(_("LiSoCl2"));
	BattChoice->Append(_("Alkaline"));
	FlexGridSizer2->Add(BattChoice, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText17 = new wxStaticText(this, ID_STATICTEXT17, _("Rule"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT17"));
	FlexGridSizer2->Add(StaticText17, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	TextRule = new wxTextCtrl(this, ID_TEXTCTRL11, _("EN50136"), wxDefaultPosition, wxSize(148,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL11"));
	FlexGridSizer2->Add(TextRule, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer1->Add(FlexGridSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer1, 1, wxALL|wxALIGN_TOP|wxALIGN_CENTER_HORIZONTAL, 5);
	StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Customer"));
	FlexGridSizer3 = new wxFlexGridSizer(0, 2, 0, 0);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("Name"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	FlexGridSizer3->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtNom = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxSize(126,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	FlexGridSizer3->Add(TxtNom, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText4 = new wxStaticText(this, ID_STATICTEXT4, _("Type"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
	FlexGridSizer3->Add(StaticText4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TypeChoice = new wxChoice(this, ID_CHOICE3, wxDefaultPosition, wxSize(124,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE3"));
	TypeChoice->SetSelection( TypeChoice->Append(_("RES")) );
	TypeChoice->Append(_("COM"));
	TypeChoice->Append(_("IND"));
	TypeChoice->Append(_("AGR"));
	TypeChoice->Append(_("GPS"));
	TypeChoice->Append(_("BEC"));
	TypeChoice->Append(_("GPS"));
	FlexGridSizer3->Add(TypeChoice, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText5 = new wxStaticText(this, ID_STATICTEXT5, _("Customer Name"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT5"));
	FlexGridSizer3->Add(StaticText5, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtCusName = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxSize(125,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	FlexGridSizer3->Add(TxtCusName, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText6 = new wxStaticText(this, ID_STATICTEXT6, _("Customer Address"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT6"));
	FlexGridSizer3->Add(StaticText6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtCusAdd = new wxTextCtrl(this, ID_TEXTCTRL3, wxEmptyString, wxDefaultPosition, wxSize(125,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
	FlexGridSizer3->Add(TxtCusAdd, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText7 = new wxStaticText(this, ID_STATICTEXT7, _("Customer Phone"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT7"));
	FlexGridSizer3->Add(StaticText7, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtCusPh = new wxTextCtrl(this, ID_TEXTCTRL4, wxEmptyString, wxDefaultPosition, wxSize(126,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL4"));
	FlexGridSizer3->Add(TxtCusPh, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText8 = new wxStaticText(this, ID_STATICTEXT8, _("email"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT8"));
	FlexGridSizer3->Add(StaticText8, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Txtemail = new wxTextCtrl(this, ID_TEXTCTRL5, wxEmptyString, wxDefaultPosition, wxSize(126,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL5"));
	FlexGridSizer3->Add(Txtemail, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText18 = new wxStaticText(this, ID_STATICTEXT18, _("simCARD plan"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT18"));
	FlexGridSizer3->Add(StaticText18, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtSimSolution = new wxTextCtrl(this, ID_TEXTCTRL12, wxEmptyString, wxDefaultPosition, wxSize(128,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL12"));
	FlexGridSizer3->Add(TxtSimSolution, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer2->Add(FlexGridSizer3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer3 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Bidi Phones"));
	FlexGridSizer4 = new wxFlexGridSizer(0, 2, 0, 0);
	StaticText9 = new wxStaticText(this, ID_STATICTEXT9, _("Analog Bidi Phone"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT9"));
	FlexGridSizer4->Add(StaticText9, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	TxtPhModem = new wxTextCtrl(this, ID_TEXTCTRL6, wxEmptyString, wxDefaultPosition, wxSize(93,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL6"));
	FlexGridSizer4->Add(TxtPhModem, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText10 = new wxStaticText(this, ID_STATICTEXT10, _("Device GSM-CSD number"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT10"));
	FlexGridSizer4->Add(StaticText10, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtCSDPh = new wxTextCtrl(this, ID_TEXTCTRL7, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL7"));
	FlexGridSizer4->Add(TxtCSDPh, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer3->Add(FlexGridSizer4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer4 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("IP"));
	FlexGridSizer5 = new wxFlexGridSizer(0, 2, 0, 0);
	StaticText11 = new wxStaticText(this, ID_STATICTEXT11, _("Device IP               "), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT11"));
	FlexGridSizer5->Add(StaticText11, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtIP = new wxTextCtrl(this, ID_TEXTCTRL8, wxEmptyString, wxDefaultPosition, wxSize(125,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL8"));
	FlexGridSizer5->Add(TxtIP, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText12 = new wxStaticText(this, ID_STATICTEXT12, _("Device Port"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT12"));
	FlexGridSizer5->Add(StaticText12, 1, wxALL|wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL, 5);
	TxtPort = new wxTextCtrl(this, ID_TEXTCTRL9, wxEmptyString, wxDefaultPosition, wxSize(124,21), 0, wxDefaultValidator, _T("ID_TEXTCTRL9"));
	FlexGridSizer5->Add(TxtPort, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer4->Add(FlexGridSizer5, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(FlexGridSizer1);
	FlexGridSizer1->Fit(this);
	FlexGridSizer1->SetSizeHints(this);

	Connect(ID_CHOICE1,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&DadesPanel::OnChoiceFamilySelect);
	Connect(ID_CHOICE4,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&DadesPanel::OnChoiceModelSelect);
	Connect(ID_CHOICE5,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&DadesPanel::OnChoiceVerSelect);
	//*)


	if (*model==""){
        // creating new model
        UpdateModels(ChoiceFamily->GetString(ChoiceFamily->GetSelection()));
	}else{
        // editing an already installation of a model, so disable change the model.
	    ChoiceFamily->Disable();
	    ChoiceModel->Append(*model);
	    ChoiceVer->Append(*versio);
	    ChoiceModel->Disable();
        ChoiceVer->Disable();
	}
}

DadesPanel::~DadesPanel()
{
	//(*Destroy(DadesPanel)
	//*)
}

void DadesPanel::UnSetDades()
{
    dades=0;
}

CData *DadesPanel::GetData()
{
    return dades;
}


//Aplica el valor de DadesPanel al CData corresponent
bool DadesPanel::ApplyValues()
{
    if (!dades)
        return false;

    *model=ChoiceModel->GetString(ChoiceModel->GetSelection());
    *versio=ChoiceVer->GetString(ChoiceVer->GetSelection());

    string lab="";
    lab = (string)(TxtNom->GetLabel());
    if (lab=="")    lab="-";
    dades->setname(lab);

    dades->settype(TypeChoice->GetString(TypeChoice->GetSelection()).c_str());

    lab = (string)(TxtCusName->GetLabel());
    if (lab=="")    lab="-";
    dades->setcusname(lab);

    lab = (string)(TxtCusAdd->GetLabel());
    if (lab=="")    lab="-";
    dades->setaddress(lab);

    lab = (string)(TxtCusPh->GetLabel());
    if (lab=="")    lab="-";
    dades->setphone(lab);

    lab = (string)(Txtemail->GetLabel());
    if (lab=="")    lab="-";
    dades->setemail(lab);

    lab = (string)(TextRule->GetLabel());
    if (lab=="")    lab="-";
    dades->setrule(lab);

    lab = (string)(TxtPhModem->GetLabel());
    dades->setphonePstn(lab);

    lab = (string)(TxtCSDPh->GetLabel());
    dades->setphoneCsd(lab);

    int conex=ConChoice->GetSelection();
    dades->setconType(conex);

    lab = (string)(TxtIP->GetLabel());
    dades->sethost(lab);

    lab = (string)(TxtPort->GetLabel());
    dades->setport(sToI(lab));

    lab = (string)(TxtSimSolution->GetLabel());
    dades->setSimSolution(lab);

    *model=ChoiceModel->GetString(ChoiceModel->GetSelection());
    *versio=ChoiceVer->GetString(ChoiceVer->GetSelection());

    dades->settBatt(BattChoice->GetSelection());
    dades->setextPower(ExtPTxt->GetValue().c_str());
    return true;
}

bool DadesPanel::UpdateValues()
{
    if (!dades)
        return false;

    string lab;
    lab = dades->getname();
    TxtNom->SetLabel(_wx(lab));
    lab = dades->getcusname();
    TxtCusName->SetLabel(_wx(lab));
    lab=dades->gettype();
    int len=TypeChoice->GetCount();
    for (int i=0;i<len;i++){
        if (TypeChoice->GetString(i)==_wx(lab)){
            TypeChoice->SetSelection(i);
            break;
        }
    }

    lab = dades->getaddress();
    TxtCusAdd->SetLabel(_wx(lab));
    lab = dades->getphone();
    TxtCusPh->SetLabel(_wx(lab));
    lab = dades->getemail();
    Txtemail->SetLabel(_wx(lab));
    lab = dades->getrule();
    TextRule->SetLabel(_wx(lab));
    lab = dades->getphonePstn();
    TxtPhModem->SetLabel(_wx(lab));
    lab = dades->getphoneCsd();
    TxtCSDPh->SetLabel(_wx(lab));

    lab = dades->getSimSolution();
    TxtSimSolution->SetLabel(_wx(lab));

    lab = dades->gethost();
    TxtIP->SetLabel(_wx(lab));
    lab = iToS(dades->getport());
    TxtPort->SetLabel(_wx(lab));

    ConChoice->SetSelection(dades->getconType());

    BattChoice->SetSelection(dades->gettBatt());
    ExtPTxt->SetLabel(dades->getextPower());
    return true;
}


void DadesPanel::OnChoiceFamilySelect(wxCommandEvent& event)
{
    UpdateModels(ChoiceFamily->GetString(ChoiceFamily->GetSelection()));
}


bool DadesPanel::ModelExist(wxString name)
{
    for (int i=0; i<llistaModels.size(); i++) {
        if (llistaModels[i] == name)
            return true;
    }

    return false;

}

void DadesPanel::UpdateModels(wxString sDir)
{
    WIN32_FIND_DATA findData;
    sDir="\\models\\"+sDir;
    HANDLE hFind=FindFirstFile((wxGetCwd()+sDir+"\\"+PATRO_FITXERS_MODELS).c_str(), &findData);
    if (hFind == INVALID_HANDLE_VALUE) {
        return;
    }
    ChoiceModel->Clear();
    DirList.clear();
    // iterate over file names in directory
    do {
        string sFileName(findData.cFileName);
        DirList.Add(sFileName);
        //ChoiceModel->Append(sFileName);
    } while (FindNextFile(hFind, &findData));

    llistaModels.clear();
    ChoiceModel->Clear();

    for(int i = 0; i<DirList.size(); i++) {
        wxString nom = DirList[i];
        int posguio = nom.find('-');
        nom = nom.substr(0, posguio);

        if (ModelExist(nom)==false){
            llistaModels.Add(nom);
            ChoiceModel->Append(nom);
        }
    }
    ChoiceModel->Select(0);
    UpdateModelVer(ChoiceModel->GetString(ChoiceModel->GetSelection()));
}

void DadesPanel::OnChoiceModelSelect(wxCommandEvent& event)
{
    UpdateModelVer(ChoiceModel->GetString(ChoiceModel->GetSelection()));

    *model=ChoiceModel->GetString(ChoiceModel->GetSelection());
}

bool DadesPanel::UpdateModelVer(wxString mod)
{
    *model=ChoiceModel->GetString(ChoiceModel->GetSelection());
    int vers=0;
    ChoiceVer->Clear();
    for (int i = 0; i<DirList.size(); i++) {
        wxString nom = DirList[i];
        int posguio = nom.find('-');
        wxString nomnover = nom.substr(0, posguio);
        if (nomnover == mod) {
            wxString ver=nom.substr(posguio+1,3);
            ChoiceVer->Append(ver);
            ChoiceVer->Select(vers);
            vers++;
        }
    }
}

void DadesPanel::OnChoiceVerSelect(wxCommandEvent& event)
{

    *versio=ChoiceVer->GetString(ChoiceVer->GetSelection());
}
