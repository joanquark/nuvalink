#ifndef TreeCTR_H
#define TreeCTR_H

/*
 * Clase CTreeCtrl.
 * Serveix per saber en cada moment a quin objecte relaciona una ID de l'Tree.
 * Cal fer-ho aixi perque quan l'usuari selecciona un component de l'Tree
 * l'objecte Tree tan sols m'informa de la ID d'aquell component i jo he de
 * saber de que es tracta
*/

#include <wx/treectrl.h>

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#include "Data.h"
#include "grup.h"
#include "grupflash.h"
#include "model.h"
#include "Status.h"
#include "llistaevents.h"

#define TREE_TIPUS_BASE         0
#define TREE_TIPUS_CDATA        1
#define TREE_TIPUS_CGROUP       2
#define TREE_TIPUS_CMODEL       3
#define TREE_TIPUS_CSTATUS      4
#define TREE_TIPUS_STSECTION    5
#define TREE_TIPUS_EVENTS       6
#define TREE_TIPUS_VERIFY       7
#define TREE_TIPUS_VMODEM       8
#define TREE_TIPUS_VCOM         9
#define TREE_TIPUS_CFLASHGROUP   10


struct rel
{
    wxTreeItemId treeId;
    int tipus;
    int index;
};

class CTreeCtrl
{
    vector<rel *> table;
    vector<CData *> vdades;
    vector<CGroup *> vgrups;
    vector<CGroupFlash *> vflashgrups;
    vector<CModel *> vmodels;
    vector<CStatus *> vestats;
    vector<int> vseccions;
    vector<CEventList *> vevents;

    public:
        wxTreeCtrl *Tree;

        CTreeCtrl();
        void Clear();
        ~CTreeCtrl();

        bool SetRoot(wxTreeItemId &id);
        bool Add(wxTreeItemId &id, CData *dades);
        bool Add(wxTreeItemId &id, CGroup *grup);
        bool Add(wxTreeItemId &id, CGroupFlash *grupFlash);
        bool Add(wxTreeItemId &id, CModel *model);
        bool Add(wxTreeItemId &id, CStatus *estat);
        bool Add(wxTreeItemId &id, int idSeccio);
        bool Add(wxTreeItemId &id, CEventList *llista);
        bool AddVerify(wxTreeItemId &id);
        bool AddVModem(wxTreeItemId &id);
        bool AddVCOM(wxTreeItemId &id);
        bool AddKeyPad(wxTreeItemId &id);
        bool Del(wxTreeItemId &id);
        int GetTypeCon(wxTreeItemId &id);
        CData *GetData(wxTreeItemId &id);
        CGroup *GetGroup(wxTreeItemId &id);
        CGroupFlash *GetGroupFlash(wxTreeItemId &id);
        CModel *GetModel(wxTreeItemId &id);
        CStatus *GetStatus(wxTreeItemId &id);
        CEventList *GetEventList(wxTreeItemId &id);
        int GetSection(wxTreeItemId &id);
        wxTreeItemId GetRoot();

        //niroblock code
        CGroup *GetGroupByIndex(int aa);
        int SearchIndx(wxTreeItemId &id);


    private:

        int SearchParentModel(int indexFill);
        int BuscaEstatPare(int indexFill);
};

#endif
