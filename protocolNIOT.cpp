#include "protocolNIOT.h"
#include "logger.h"
#include "util.h"
#include "privlevel.h"
#include "cid.h"

// --------------------------------- Threaded version ----------------------------
void *CProtocolNIOT::Entry()
{
    int resol;
    int rx;
    while (1){
        resol=connecthandler->GetResolucioProcessRx();
        Sleep(resol);
        if ((online) && (connecthandler) && (connecthandler->GetTypeCon()<TYPE_TCPIP_CONNECTION) && (threaddisable==false)){
            // only for local USB/RS485 connectios to catch bus messages.
            if ((rx=ProcessRx(resol))>0){
                Sleep(resol + 2*rx);
            }
        }else{
            Sleep(5000);
        }
    }
}

bool CProtocolNIOT::DisThreadPoll(bool dis)
{
    threaddisable=dis;
    return dis;
}

void CProtocolNIOT::OnExit()
{
    int jj=0;
}

CProtocolNIOT::CProtocolNIOT()
{

    cntNoAct=0;
    lonTramaRx=0;
    lonTramaTx=0;
    lonTramaTmp=0;
    sequencia=false;
    dinsTrama=false;
    esPotReintentar=false;
    reliefmode=false;
//    timerconnecthandler=0;
    timerHello=0;
    progressDlg=0;
    progress=0;
    NumError=0;
    connecthandler=0;
    online=false;
    SN="";
    Hops=0;
}

CProtocolNIOT::~CProtocolNIOT()
{
    if (online) {
        connecthandler->Disconnect();
    }
    Stop();
    Sleep(1000);
}


bool CProtocolNIOT::GenSendCodiPacket( string &address, bool ramNotFlash, unsigned char *buffer, int lon, int offset)
{
    char caddress[4];

    while (address.length()<8) address = "0" + address;
    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }

    return GenSendCodiPacket( caddress, ramNotFlash, buffer, lon, offset);
}


//TramaEnviarCodi
//Encapsula totes les dades en una trama del protocol JR
//ram==true --> Tipus ram
//ram==false --> Tipus eeprom

bool CProtocolNIOT::GenSendTxtPacket(char *sms,int len)
{
    NIOT.FormatHeader(T_NIOT_PKT_TXT|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,0,T_NIOT_TXT_ADDDATA);

    NIOT.AddBlock(0xFF,sms,len);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}



bool CProtocolNIOT::GenSendCodiPacket(char *address, bool ramNotFlash, unsigned char *buffer, int lon, int offset)
{
    return GenSendMemPacket(address, ramNotFlash, buffer, lon, offset);
}



bool CProtocolNIOT::GenSendMemPacket(  string &address, bool ramNotFlash, unsigned char *buffer, int lon, int offset)
{

    char caddress[4];

    //Afegim zeros a l'esquerra per fer-lo de longitud 4
    while (address.length()<8) address = "0" + address;

    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }

    return GenSendMemPacket(caddress,ramNotFlash,buffer,lon,offset);
}

bool CProtocolNIOT::GenSendMemPacket( char *address, bool ramNotFlash, unsigned char *buffer, int lon, int offset)
{
    if (ramNotFlash)
        NIOT.FormatHeader(T_NIOT_PKT_RAM|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    else
        NIOT.FormatHeader(T_NIOT_PKT_FLASH|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);

    NIOT.AddBlock(0xFF,address,4);
    NIOT.AddBlock(0xFF,buffer,lon);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}


bool CProtocolNIOT::GenRxCodiPacket( string &address, bool ramNotFlash, int valLength, int offset)
{
    char caddress[4];

    //Afegim zeros a l'esquerra per fer-lo de longitud 4
    while (address.length()<8) address = "0" + address;

    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }

    return GenRxCodiPacket(caddress, ramNotFlash, valLength, offset);
}

bool CProtocolNIOT::GenRxCodiPacket(char *address, bool ramNotFlash, int valLength, int offset)
{
    return GenRxMemPacket(address, ramNotFlash, valLength, offset);
}

bool CProtocolNIOT::GenRxGroupPacket(string &address, bool ramNotFlash, int valLength, int off)
{
    char caddress[4];

    //Afegim zeros a l'esquerra per fer-lo de longitud 4
    while (address.length()<8) address = "0" + address;

    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }

    return GenRxGroupPacket(caddress, ramNotFlash, valLength, off);
}

bool CProtocolNIOT::GenRxGroupPacket(char *address, bool ramNotFlash, int valLength,  int offset)
{
    return GenRxMemPacket( address, ramNotFlash, valLength, offset);
}

bool CProtocolNIOT::GenRxMemPacket(string  &address, bool ramNotFlash, int valLength, int offset)
{
    char caddress[4];

    //Afegim zeros a l'esquerra per fer-lo de longitud 4
    while (address.length()<8) address = "0" + address;

    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }

    GenRxMemPacket(caddress, ramNotFlash, valLength, offset);
}

bool CProtocolNIOT::GenRxMemPacket(char *address, bool ramNotFlash, int valLength, int offset)
{
    unsigned char crc[2];

    if (connecthandler->GetTypeCon()==TYPE_RS485_CONNECTION || connecthandler->GetTypeCon()==TYPE_USB_CONNECTION){
        Sleep(20);
    }else{
        Sleep(100);         // digital connections , better avoid joining previous sent ACK with this petition.
    }

    if (ramNotFlash)
        NIOT.FormatHeader(T_NIOT_PKT_REQRAM|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    else
        NIOT.FormatHeader(T_NIOT_PKT_REQFLASH|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);

    NIOT.AddBlock(0xFF,address,4);
    WORD_VAL inflen;
    inflen.Val=(WORD)valLength;
    NIOT.AddBlock(0xFF,&inflen.v[0],2);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;


}


bool CProtocolNIOT::GenRxEventsPacket(string& address, bool eeprom, int punterIni, int bytesxevent, int numEvents)
{
    char caddress[4];
    int adr=hToI(address);
    //Li sumo a l'adre�a base l'offset del punter per trobar l'adre�a que m'interessa
    adr += punterIni*bytesxevent;
    address=iToH(adr);
    while (address.length()<8) address = "0" + address;

        // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }

    return GenRxMemPacket( caddress, false, bytesxevent*numEvents);
}


bool CProtocolNIOT::GenStablishPacket(int priv, string& codiInstalador, bool callback)
{

    reliefmode=false;
    char codiInstH = ascii2nibble(codiInstalador[0]);
    codiInstH <<= 4;
    codiInstH |= ascii2nibble(codiInstalador[1]);

    char codiInstM = ascii2nibble(codiInstalador[2]);
    codiInstM <<= 4;
    codiInstM |= ascii2nibble(codiInstalador[3]);

    char codiInstL;
    if (codiInstalador.length()>4){
        codiInstL = ascii2nibble(codiInstalador[4]);
        codiInstL <<= 4;
        codiInstL |= ascii2nibble(codiInstalador[5]);
    }else{
        codiInstL=0x00;
    }

    return GenStablishPacket(priv, codiInstH, codiInstM, codiInstL, callback);
}

//Genera la trama d'establiment de sessio. Indica si es sessio normal
//o si esperem un callback. Tambe indiquem el codi d'isntalador
//Trama 41
bool CProtocolNIOT::GenStablishPacket(int priv, char codiInstH, char codiInstM, char codiInstL, bool callback)
{
    unsigned char data;
    unsigned char rpc=T_NIOT_RPC_STABLISH;
    if (callback)
        data = 0x01;
    else
        data = 0x00;
    if (priv==PROD_PRIVLEVEL)
        data |= M_LEVEL4_ACCESS;
    data|=M_ACCEPT_EXT_ANSWER;

    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    NIOT.AddBlock(0xFF,&data,1);
    NIOT.AddBlock(0xFF,&codiInstH,1);
    NIOT.AddBlock(0xFF,&codiInstM,1);
    NIOT.AddBlock(0xFF,&codiInstL,1);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}


bool CProtocolNIOT::GenPacketGetBeaconSt()
{
    unsigned char rpc=T_NIOT_RPC_BEACON_STATUS;
    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;

}


//Genera la trama per registrar un
//o si esperem un callback. Tambe indiquem el codi d'isntalador
//Trama 41
bool CProtocolNIOT::GenWaveRecordPacket(uint16_t channel, uint16_t numsample)
{
    unsigned char data[4];
    unsigned char rpc=T_NIOT_RPC_RECORDANALOG;
    WORD_VAL ch;
    WORD_VAL sample;

    ch.Val=channel;
    sample.Val=numsample;

    data[0]=ch.v[0];
    data[1]=sample.v[0];
    data[2]=sample.v[1];

    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    NIOT.AddBlock(0xFF,&data[0],3);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}


// ------------------------------------------------------- GetFwSignatures ---------
bool CProtocolNIOT::GenPacketGetFlashSignatures(string address,int len){

    char caddress[4];
    while (address.length()<8) address = "0" + address;

    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }

    unsigned char rpc=T_NIOT_RPC_GETFLASHSIGNATURE;
    unsigned char numsec=len;
    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    NIOT.AddBlock(0xFF,&numsec,1);
    NIOT.AddBlock(0xFF,&caddress[0],4);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}



bool CProtocolNIOT::GenOrderPacket( char TOrder, unsigned char* params,int paramlen,bool noack=false){

    unsigned char data=TOrder;
    unsigned char rpc=T_NIOT_RPC_ORDER;
    unsigned char head=T_NIOT_PKT_RPC|M_NIOT_CRC_ADD;
    if (noack==false)
        head|=M_NIOT_NEED_ACK;

    NIOT.FormatHeader(head,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    NIOT.AddBlock(0xFF,&data,1);
    if (paramlen){
        NIOT.AddBlock(0xFF,params,paramlen);
    }
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;

}


bool CProtocolNIOT::GenEventPacket(WORD event,WORD num, unsigned char area)
{

    unsigned char rpc=T_NIOT_RPC_EVENT;
    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    WORD_VAL ev;
    ev.Val=event;
    NIOT.AddBlock(0xFF,&ev.v[0],2);
    WORD_VAL n;
    n.Val=num;
    NIOT.AddBlock(0xFF,&n.v[0],2);
    NIOT.AddBlock(0xFF,&area,1);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}

bool CProtocolNIOT::GenByePacket()
{
    GenOrderPacket(ORDER_FINISH_COMM);
}


bool CProtocolNIOT::GenTramaApplyProg()
{
    GenOrderPacket(ORDER_REFRESH_PROG);
}

bool CProtocolNIOT::GenRxFlashPacket(char *address, int len)
{
    return GenRxMemPacket(address,false,len,0);

}

bool CProtocolNIOT::GenRxFlashPacket(string& address, int len)
{
     char caddress[4];
    //Afegim zeros a l'esquerra per fer-lo de longitud 4
    while (address.length()<8) address = "0" + address;

    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }
    return GenRxMemPacket(caddress,false,len,0);
}


bool CProtocolNIOT::GenTxFlashPacket(char *address, unsigned char *buffer, int lon,bool ack,bool fw530)
{
    return GenSendMemPacket( address, false, buffer, lon, 0);

}

bool CProtocolNIOT::GenTxFlashPacket(string& address, unsigned char *buffer, int lon,bool ack,bool fw530)
{
    char caddress[4];
        //Afegim zeros a l'esquerra per fer-ho de longitud 8 ( 32 bits = 8 nibbles ! )
    while (address.length()<8) address = "0" + address;
    // address must be converted to Little endian form.
    for (int i=0; i<4; i++) {
        caddress[3-i] = ascii2nibble(address[i*2]);
        caddress[3-i] <<= 4;
        caddress[3-i] |= ascii2nibble(address[(i*2)+1]);
    }
    return GenTxFlashPacket( caddress, buffer, lon,ack,fw530);
}


bool CProtocolNIOT::GenACDCPacket(string IPADD,string PORT)
{

    unsigned char rpc=T_NIOT_RPC_ACDC;
    string res=IPADD+":"+PORT;

    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    NIOT.AddBlock(0xFF,(char*)&res[0],Strlen((const char*)&res[0]));
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}

// ---------------------------- stream packet sent on the go! -----------------
bool CProtocolNIOT::SendStreamPkt(unsigned char* data, int len)
{
    NIOT.FormatHeader(T_NIOT_PKT_STREAM|M_NIOT_CRC_ADD,this->Hops);        // NO NEED ACK!
    NIOT.AddBlock(0xFF,data,len);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    connecthandler->SendData(tramaTx, lonTramaTx);
    if (myLog) {
        myLog->MostraChar('>');
        myLog->MostraTrama(tramaTx, lonTramaTx);
    }
    return true;
}


bool CProtocolNIOT::SendAsciiFrame(string frame,int len)
{

    char* pc=&frame[0];
    *(pc+len)=0x0A;
    *(pc+len+1)=0x0D;
    connecthandler->SendData((unsigned char*)pc, len+2);
    if (myLog) myLog->MostraTrama((unsigned char*)pc,len);

}


// -------------------------------------------- Custom Packet ------------------------------------------
bool CProtocolNIOT::GenCustomPacket(unsigned char tpkt,unsigned char * frame,int len){

    NIOT.FormatHeader(tpkt,this->Hops);
    NIOT.AddBlock(0xFF,frame,len);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}

// -------------------------------------------- Vr Config frame ----------------------------------------
bool CProtocolNIOT::GenWConfigPacket(unsigned char z,unsigned char tsens, string& code){

    return true;
}

// -------------------------------------------- VrUser Config frame ----------------------------------------
bool CProtocolNIOT::GenWConfigPacketUser(unsigned char u,unsigned char tsens, string& code){

     return true;
}

// -------------------------------------------- VrUser Config frame ----------------------------------------
bool CProtocolNIOT::GenWConfigPacketOut(unsigned char out,unsigned char tsens, string& code){

    return true;
}


// -------------------------------------------- Firmware action frame------------------------------------
bool CProtocolNIOT::GenFwActionPacket( unsigned int checksum,unsigned char action){
    // SYN dest 0x31 0x6F action checkL checkH 0 0 0 CRC CRC


    unsigned char rpc[2];
    rpc[0]=T_NIOT_RPC_FIRMWARE;
    rpc[1]=action;

    checksum&=0x0000FFFF;       // 16 bit checksum
    string sfchecksum = iToH(checksum);
    char cfchecksum[2];
    //Afegim zeros a l'esquerra per fer-lo de longitud 4
    while (sfchecksum.length()<4) sfchecksum = "0" + sfchecksum;
    for (int i=0; i<2; i++) {
        cfchecksum[1-i] = ascii2nibble(sfchecksum[i*2]);
        cfchecksum[1-i] <<= 4;
        cfchecksum[1-i] |= ascii2nibble(sfchecksum[(i*2)+1]);
    }

    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc[0],2);
    NIOT.AddBlock(0xFF,cfchecksum,2);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}


bool CProtocolNIOT::GenRPCPacket( unsigned char funcio, unsigned char param1, unsigned char param2, unsigned char param3, unsigned char param4)
{

    unsigned char rpc=funcio;
    NIOT.FormatHeader(T_NIOT_PKT_RPC|M_NIOT_CRC_ADD|M_NIOT_NEED_ACK,this->Hops);
    NIOT.AddBlock(0xFF,&rpc,1);
    NIOT.AddBlock(0xFF,&param1,1);
    NIOT.AddBlock(0xFF,&param2,1);
    NIOT.AddBlock(0xFF,&param3,1);
    NIOT.AddBlock(0xFF,&param4,1);
    lonTramaTx=NIOT.CloseFr(tramaTx);
    return true;
}



bool CProtocolNIOT::GenArmAreaPacket( WORD areesMask)
{
    return GenRPCPacket( T_NIOT_RPC_ARM, BYTE(areesMask),BYTE(areesMask>>8),ON_USER_NUVALINK);
}


bool CProtocolNIOT::GenDisarmAreaPacket( WORD areesMask)
{
    return GenRPCPacket( T_NIOT_RPC_DISARM, BYTE(areesMask),BYTE(areesMask>>8), ON_USER_NUVALINK);
}

//bank = true, significa que la mascara es per les zones 1..8
//bank = false, mascara per les zones 9..16
bool CProtocolNIOT::GenBypassZPacket(uint16_t zone, uint16_t user)
{
    return GenRPCPacket( T_NIOT_RPC_BYPASS, (BYTE)user, (BYTE)zone);
}



bool CProtocolNIOT::GenActOutputPacket( unsigned char sortidesMask)
{
    return GenRPCPacket( T_NIOT_RPC_SETTRT, sortidesMask, 0x00);
}


bool CProtocolNIOT::GenDeactOutputPacket(unsigned char sortidesMask)
{
    return GenRPCPacket(T_NIOT_RPC_CLRTRT, sortidesMask, 0x00);
}



bool CProtocolNIOT::GenReportTestPacket()
{
    return GenEventPacket( EVENT_MANUAL_TEST | M_EVENT_NEW,0xFFFF, 0x00);
}

// ------------------------------------------- Enviar Trama revised ---------------
//S'ocupa d'enviar una trama.
//Envia la ultima trama creada amb alguna de les funcions de crear trama
//La funcio s'encarrega d'esperar l'ACK i de tenir en compte els timeouts i repeticions
bool CProtocolNIOT::SendPacket(int tries=3,bool longtimeout=false)
{
    unsigned char buffer[2048];
    if (connecthandler->GetTypeCon()>=TYPE_TCPIP_CONNECTION)
        while (connecthandler->ReceiveData(buffer,2048)!=0);

    // flush any pending data of socket , solving problem with Cloud connections.

    DiscardRx();                   // discard any previos frame.
    //Per defecte, els errors que torno son greus
    esPotReintentar=false;
    bool success=false;
    int resol=connecthandler->GetResolucioProcessRx();
    isBinRx=false;
    if (lonTramaTx==0) {
        msgError = MISS_ERROR_SEND_0;
        return success;
    }
    threaddisable=true;         // means polls in the loop and not in thread.

    if (!online) {
        msgError = MISS_ERROR_CON_NOT;
        return success;
    }

    int intents=0;

    do {
        bool txok;
        NIOT.UpdateTry(intents,lonTramaTx,tramaTx);
        txok=connecthandler->SendData(tramaTx, lonTramaTx);
        if (myLog) {
            myLog->MostraChar('>');
            myLog->MostraTrama(tramaTx, lonTramaTx);
        }
        if (txok) {
            waitAck=true;
            timeout = false;
            int xx=0;
            int temps = (int)(connecthandler->GetTimeoutAck() / resol);
            if (longtimeout==true)
                temps*=5;

           // if (this->reliefmode==true)
           //     temps+=EXTRA_TIMEOUT_RELIEFMODE/resol;
            while ((!SearchAnswer()) && (timeout==false)) {
                if (myApp)
                    myApp->Yield();     //deixem que el programa processi els missatges aixi no sembla penjat
                if (UserCancel()){
                    threaddisable=false;
                    return false;
                }
                Sleep(resol);
                ProcessRx(resol);
                xx++;
                if (xx>temps)
                    timeout=true;
            }
            if (timeout==false){
                avTimeOut=connecthandler->CalcAvTimeOut(xx*resol);
                if (privlevel>=ENG_PRIVLEVEL){
                    string ms="TAvAns:"+iToS(avTimeOut)+"\r";
                    if (myLog) myLog->MostraMsg(ms);
                }
                if (!IsNACK()) {
                    if (IsACK()){
                        success=true;
                        break;
                    }
                    if (IsDades()){
                        success=true;
                        break;
                    }
                    DiscardRx();
                    success=false;
                } else {
                    Sleep(400);             // Deixem respirar al receptor....
                    DiscardRx();
                    success=false;
                }
            }else{
                // TIMEOUT - No ha trobat resposta per� si no cal ACK sortim del bucle
                connecthandler->CalcAvTimeOut(xx*resol);   // si timeout incrementem-lo

            }
        }else{
            msgError = connecthandler->GetLastError();
            threaddisable=false;
            return success;
        }
    } while (++intents<tries);
    // la trama de grup complir� rigorosament els 4 intents.

    waitAck=false;
    threaddisable=false;
    if (IsTramaDeGrup()) {
        datatx+=(lonTramaTx-sizeof(TNIOTHeader));
        return true;
    }
    //Si hem rebut un ACK com a resposta ja ho descarto directament
    if (IsACK()){
        //DiscardRx(1);           // Ack gets self discarded
    }else{
        if (IsDades()) {
            // No la descarta, de manera que RxPacket ja la te en cua.
        }else{
            DiscardRx();
        }
    }
    if (success==false){
        msgError = MISS_ERROR_CON_RESP;
    }else{
        NumError=0;
    }
    datatx+=(lonTramaTx-sizeof(TNIOTHeader));
    return success;
}

// ------------------------------------------ enviar trama sense esperar ack ----------------
// ------------------------------------------- Enviar Trama revised -------------------------
//S'ocupa d'enviar una trama.
//Envia la ultima trama creada amb alguna de les funcions de crear trama
//La funcio s'encarrega d'esperar l'ACK i de tenir en compte els timeouts i repeticions

int CProtocolNIOT::SendPacketNoW(int add,int delay,int size)
{
    unsigned char buffer[128];
    //Per defecte, els errors que torno son greus
    int resol=connecthandler->GetResolucioProcessRx();
    isBinRx=false;
    if (lonTramaTx==0) {
        msgError = MISS_ERROR_SEND_0;
        return false;
    }
    if (!online) {
        msgError = MISS_ERROR_CON_NOT;
        return false;
    }

//    if (timerconnecthandler) timerconnecthandler->Stop();
//    if (timerconnecthandler) timerconnecthandler->Start();

    threaddisable=true;
    connecthandler->SendData(tramaTx, lonTramaTx);
    if (myLog){
        myLog->MostraChar('>');
        myLog->MostraTrama(tramaTx, lonTramaTx);
    }
    datatx+=(lonTramaTx-5);

    while (!SearchAnswer(true)){
        if (myApp)
            myApp->Yield();  //deixem que el programa processi els missatges aixi no sembla penjat

        if (UserCancel()){
            threaddisable=false;
            return false;
        }
        Sleep(resol);
        ProcessRx(resol);
        delay-=resol;

        if (delay<=0)
            break;
    }
    if (delay>0)
        Sleep(delay);       // acabem de dormir un temps

    threaddisable=false;
    return add;
}
// -------------------------------------------- Busca rebre packets stream ------------------
bool CProtocolNIOT::RxPacketStream(unsigned char* dades,uint16_t* len)
{
    threaddisable=true;
    WantedIden1=T_NIOT_PKT_STREAM;
    if (!connecthandler)
        return false;

    ProcessRx(50);
    CNIOT tr;
    if (SearchAnswer()){
        if (tr.GetTNIOTFr(tramaRx)==T_NIOT_PKT_STREAM){
            dades=GetData(len);
            return true;
        }
    }
    return false;

}

// ----------------------------------------- RxPacket ----------------------------------
bool CProtocolNIOT::RxPacket(unsigned char TNIOTPkt)
{
    threaddisable=true;
    isBinRx=false;
    WantedIden1=TNIOTPkt;
    esPotReintentar=false;
    if (!connecthandler)
        return false;

    int resol=connecthandler->GetResolucioProcessRx();
    if (!online)
        return false;

//    ProcessRx(resol);
    timeout=false;
    int xx=0;
    int temps = connecthandler->GetAnswerTimeOut() / resol;
    bool segueix=false;
    do {
        segueix=false;
        while ((!SearchAnswer()) && (!timeout)) {
            if (UserCancel()){
                threaddisable=false;
                return false;
            }
            Sleep(resol);
            ProcessRx(resol);
            xx++;
            if (xx>temps) timeout=true;
        }

        if ((!timeout) && (IsACK())) {
            // discard ack
            SearchAnswer( true);
            segueix=true;
        }
    } while (segueix);

    threaddisable=false;
    if (timeout) {
        msgError = MISS_ERROR_TIMEOUT;
        esPotReintentar=true;
        return false;
    }

    if (SearchAnswer( true)) {
        Sleep(10);
        return true;
    } else {
        return false;
    }
}


unsigned char* CProtocolNIOT::GetTramaTx(){
    return &tramaTx[0];
}

int CProtocolNIOT::GetLenTramaTx(){
    return lonTramaTx;
}

unsigned char* CProtocolNIOT::GetTramaRx(){
    return &tramaRx[0];
}

int CProtocolNIOT::GetLenTramaRx(){
    return lonTramaRx;
}


unsigned char *CProtocolNIOT::GetData(uint16_t *len)
{
    CNIOT m;
    unsigned char *res;
    if (IsDades()) {
        res = (tramaRx+sizeof(TNIOTHeader));
        *len = m.GetLenNIOTFr(tramaRx);
    }
    else res = 0;

    return res;
}

int CProtocolNIOT::GetDataAddress()
{
    int res;
    if (IsDades()) {
        DWORD_VAL dw;
        memcpy(&dw.v[0],(tramaRx+sizeof(TNIOTHeader)),4);                 // return data address.
        return dw.Val;
    }
    else res = -1;

    return res;
}



unsigned char CProtocolNIOT::GetIden()
{
    return tramaRx[3];

}

unsigned char CProtocolNIOT::ReceiveAllIdens()
{
    WantedIden1=0xFF;
    return 0xFF;
}

bool CProtocolNIOT::EnviarACK()
{

    int acklen=NIOT.NIOTAck(ACK);
    if (!connecthandler->SendData(NIOT.GetAckBuff(),acklen)) {
        msgError = MISS_ERROR_CON_SEND;
        return false;
    }
    else if (myLog) {
        myLog->MostraChar('>');
        myLog->MostraTrama(NIOT.GetAckBuff(), acklen);
    }

}

bool CProtocolNIOT::EnviarNACK(){

    int acklen=NIOT.NIOTAck(NAK);
    if (!connecthandler->SendData(NIOT.GetAckBuff(),acklen)) {
        msgError = MISS_ERROR_CON_SEND;
        return false;
    }
    else if (myLog) {
        myLog->MostraChar('>');
        myLog->MostraTrama(NIOT.GetAckBuff(), acklen);
    }
}

//SearchAnswer
//Busca a la cua de paquets rebuts si hi ha algun amb origen "desti" (resposta)
//En cas afirmatiu, el copia a tramaRx
bool CProtocolNIOT::SearchAnswer( bool discard=false)
{
    bool result=false;
    unsigned char *p;
    CNIOT m;

    if (RxLon==0)
        return false;
    //protomutex.Lock();

    //lonTramaRx=0;
 //   long int tamany=cuaRx.size();
 //   if (tamany>10 || tamany<0){
 //       cuaRx.clear();
 //       cuaRxLon.clear();
 //       protomutex.Unlock();
 //       return false;
 //   }

//    for (int i=0; i<cuaRx.size(); i++) {
        p = &tramaRx[0];
        // L'adr�a d'origen ha de ser la nostra "desti" per comprovar que ens contesta
        // el que li hem preguntat. Tambe entren si l'adre�a origen es la 00 pq a vegades
        // contesta la central en nom dels altres
        if (m.GetTNIOTFr(p)==T_NIOT_PKT_ACK){
            // ack frame
            result = true;
            lonTramaRx = RxLon;
            /*lonTramaRx = cuaRxLon[i];
            for (int c=0; c<lonTramaRx; c++)
                tramaRx[c] = p[c];
            //if (discard) {
                cuaRx.erase(cuaRx.begin()+i);
                cuaRxLon.erase(cuaRxLon.begin()+i);
                delete [] p;
            //}*/

        }else if (m.GetTNIOTFr(p)==T_NIOT_PKT_STREAM){
            result = true;
            lonTramaRx = RxLon;
            //lonTramaRx = cuaRxLon[i];
            /*for (int c=0; c<lonTramaRx; c++)
                tramaRx[c] = p[c];
            cuaRx.erase(cuaRx.begin()+i);
            cuaRxLon.erase(cuaRxLon.begin()+i);
            delete []p;*/
        }else{
            if ((WantedIden1==m.GetTNIOTFr(p)) || (waitAck==true) || (WantedIden1==0xFF)){
                result = true;
                lonTramaRx = RxLon;
                /*lonTramaRx = cuaRxLon[i];
                for (int c=0; c<lonTramaRx; c++)
                    tramaRx[c] = p[c];
                if (discard) {
                    cuaRx.erase(cuaRx.begin()+i);
                    cuaRxLon.erase(cuaRxLon.begin()+i);
                    delete [] p;
                }*/
            }
            /*if (discard==true){
                cuaRx.erase(cuaRx.begin()+i);
                cuaRxLon.erase(cuaRxLon.begin()+i);
                delete [] p;
            }*/
        }
//    }
//    protomutex.Unlock();

    if (discard)
        RxLon=0;
    return result;
}

bool CProtocolNIOT::SearchAnswer()
{
    return SearchAnswer(false);
}

bool CProtocolNIOT::IsReliefMode()
{
    return reliefmode;
}


// -----------------------------------------------------------------------------
// ---------------------------------------------------------- ProcessRx -------
//Funcio que es crida per Timer mentre hi ha connecthandler.
//Quan reb una trama
//amb l'adre�a correcte i el CRC correcte la guarda a tramaRx i
//activa els flags necessaris
//Les trames que no estan dirigides a cuaRxLonnuvalink, son ignorades
//Si rep una trama defectuosa (mal CRC) envia un NAK
int CProtocolNIOT::ProcessRx(int Timer)
{
    int resta=Timer>>1;
    unsigned char buffer[1024];
    bool afegircua=false;
    int i;
    if (!online) return;

    //Si no hi ha dades ens tornar� c=0
    int c = connecthandler->ReceiveData(buffer, 1024);
    if (c>0) {
        if (cntTInTrama>resta)
            cntTInTrama-=resta;
        for (i=0; i<c; i++) {
    #ifdef _COMM_DEBUG
            if (privlevel>=DEBUG_PRIVLEVEL && myLog){
                myLog->MostraChar('#');
                myLog->MostraChar((char)nibble2ascii((buffer[i]&0xF0)>>4));
                myLog->MostraChar((char)nibble2ascii((buffer[i]&0x0F)));
                if (i==(c-1)){
                    myLog->MostraChar(0x0D);
                }
            }
    #endif
            if (!dinsTrama) {
iniframerx:
                isascii=false;
                tramaTmp[0] = buffer[i];
                if (buffer[i]==SOH) {                   // NIOT protocol
                    dinsTrama=true;
                    cntTrama=1;
                    lonDades=-1;    //Indica que encara no se quants bytes de dades rebr�
                    lonBytes=256+12;   // to be determined.
                    cntTInTrama=0;
                }else if (((buffer[i]>=0x20) && (buffer[i]<0x81)) && (connecthandler->GetTypeCon()<=TYPE_RS485_CONNECTION)){
                    dinsTrama=true;
                    isascii=true;
                    cntTrama=1;
                    lonBytes=200;
                    // only in serial or USB modes.
                }
            } else {
                // El destinatari de la trama �s cre�ble.
                tramaTmp[cntTrama++] = buffer[i];
                if (!(cntTrama&0x0F))
                    cntTInTrama=0;          // cada 16 bytes resetejem timeout.

                if (isascii){
                    if (buffer[i]==0x0D){
                        dinsTrama=false;
                        if (myLog) myLog->MostraTrama(tramaTmp, cntTrama);
                    }
                }else if (tramaTmp[0]==SOH){
                    if (cntTrama == sizeof(TNIOTHeader)){
                        // Mirem que no es tracti del meu ACK en canal modem.
                        unsigned char pp=NIOT.GetTNIOTFr(tramaTmp);
                        if (pp==T_NIOT_PKT_ACK){
                            lonBytes=lonDades=NIOT.GetLenNIOTFr(tramaTmp)+1;     // ACK can also have data.
                            if (tramaTmp[1]&M_NIOT_CRC_ADD)
                                lonBytes+=2;        // as there is CRC added.
                            else if (NIOT.GetLenNIOTFr(tramaTmp)==0)
                                dinsTrama  = false;
                        }else{
                            lonBytes=lonDades=NIOT.GetLenNIOTFr(tramaTmp)+1;      // +1 because lonBytes got decremented afterwards
                            if (tramaTmp[1]&M_NIOT_CRC_ADD)
                                lonBytes+=2;        // as there is CRC added.
                        }
                        if ((tramaTmp[1]&M_NIOT_CIPHER)!=0){
                            // we must take in consideration the padding!
                            int j=(lonDades+SIZE_CIPHERED_NIOTHEADER+2)%16;	// +12 as we encrypt also part of the header except SOH,Flags and APPID + 2 for CRC part(duty)
                            if (j>0){							// Optimize size, useful in case of LRW.
                                int fill=16-j;
                                lonBytes+=(fill+1);             // + 1 because of next --longBytes
                            }
                        }
                    }
                }
                // -------------------------------------------------- end of frame rx --------------------------------
                if (!--lonBytes) {
EndRx:
                    //Si no era per nosaltres la descartem
                    if (tramaTmp[0]==SOH) {
                        int  crcgood;
                        NIOT.SetAesKey(this->aeskey);
                        crcgood=NIOT.ProcessNIOTRx(tramaTmp,(BYTE*)&NIOT_ALL_SUBSCRIBERS,cntTrama);
                        if (crcgood>=0) {
                            if (myLog){                     // #WARNING: writting a 266 byte char by char frame into log takes up to 150ms!!!
                                myLog->MostraChar('<');     // writting all frame in one time
                                myLog->MostraTrama(tramaTmp, cntTrama);
                            }
                            BYTE* pFr=tramaTmp;
                            if (NIOT.GetTNIOTFr(pFr)==T_NIOT_PKT_ENCAPSULATED){
                                this->reliefmode=true;
                                connecthandler->SetReliefMode(true);
                                if (NIOT.ProcessNIOTRx(tramaTmp+sizeof(TNIOTHeader),(BYTE*)&NIOT_ALL_SUBSCRIBERS,cntTrama)>=0){
                                    pFr=tramaTmp+sizeof(TNIOTHeader);
                                    cntTrama-=sizeof(TNIOTHeader);
                                }
                            }
                            this->Hops=NIOT.GetHops();        // annotate hops of the drame to apply!
                            AfegirTramaCua(pFr,cntTrama);
                            if (NIOT.GetTNIOTFr(pFr)>=T_NIOT_PKT_FLASH && NIOT.GetTNIOTFr(pFr)<=T_NIOT_PKT_DEVST){
                                // automatic serial number assign..
                                if (this->SN=="FFFFFFFF" && NIOT.GetSN()!="FFFFFFFF")
                                    this->SN=NIOT.GetSN();

                                if (NIOT.GetSN()==this->SN && NIOT.NeedAck())
                                    EnviarACK();

                                if (NIOT.GetTNIOTFr(pFr)==T_NIOT_PKT_RPC)
                                    int kk=0;
                            }
                            if (lonDades>0)
                                datarx+=lonDades;
                        }
                        else {
                            //CRC incorrecte
                            if (myLog) myLog->MostraChar('c');
                            if (myLog) myLog->MostraTrama(tramaTmp, cntTrama);
                            if (NIOT.GetTNIOTFr(tramaTmp)>=T_NIOT_PKT_FLASH && NIOT.GetTNIOTFr(tramaTmp)<=T_NIOT_PKT_DEVST){
                                if (NIOT.GetSN()==this->SN && NIOT.NeedAck())
                                    EnviarNACK();
                            }
                        }
                    } else {
                            //La trama no era per nosaltres
                        if (myLog) myLog->MostraChar('?');
                        if (myLog) myLog->MostraTrama(tramaTmp, cntTrama);
                        if (NIOT.GetTNIOTFr(tramaTmp)==T_NIOT_PKT_FLASH)
                            LongPBus=true;
                    }
                    dinsTrama=false;
                    cntTrama=0;
                    cntTInTrama=0;
                }
            }
        }
        cntNoAct=0;
    }
    else if (c == 0){
        //Si estem massa temps dins d'una trama, ho descartem
        if (dinsTrama) {
            cntTInTrama+=Timer;         // Ens passen quan temps fa que no es crida ProcessRx

            int timeouts=TIMEOUT_INTERCHAR_LOCAL;
            if ((connecthandler->GetTypeCon()!=TYPE_RS485_CONNECTION) && (connecthandler->GetTypeCon()!=TYPE_USB_CONNECTION))
                timeouts=TIMEOUT_INTERCHAR_REMOTE;

            if (cntTInTrama>timeouts){         // 20 ms en bucles d'esperar dades.
                dinsTrama=false;
                cntTInTrama=0;
                cntTrama=0;

                if (privlevel>=3){
                    string ms="Interchar Timeout";
                    if (myLog) myLog->MostraMsg(ms);
                    return (20);
                }
            }
        }
        //Si portem molt temps sense activitat, borrem el buffer
        else {
            cntNoAct+=Timer;
            if (cntNoAct>TIMEOUT_WATCHDOG_COMM) {     // 500 mssegons
                lonTramaRx=0;
                cntNoAct=0;
                ClearRxQueue();
            }
        }
    }
    else {
        //Hem perdut la connecthandler?!
        msgError = MISS_ERROR_CON_LOST;
        Stop();
        return 0;
    }
    return c;
}

bool CProtocolNIOT::crcOk(unsigned char *trama, int lon)
{
    unsigned char crc[2];

    CalcCRC(trama, lon-2, crc);

    if ((crc[0] == trama[lon-2]) && (crc[1] == trama[lon-1]))
        return true;
    else
        return false;
}

bool CProtocolNIOT::IsNACK()
{
    return IsNACK(tramaRx, lonTramaRx);
}

bool CProtocolNIOT::IsNACK(unsigned char* trama, int lon)
{
    if (lon==0)
        return false;

    if (NIOT.GetTNIOTFr(trama)==T_NIOT_PKT_ACK && (!(trama[1]&M_NIOT_TRUEACK)))
        return true;
    else
        return false;
}


bool CProtocolNIOT::IsACK()
{
    return IsACK(tramaRx, lonTramaRx);
}

bool CProtocolNIOT::IsACK(unsigned char* trama, int lon)
{
    if (lon==0)
        return false;

    if (NIOT.GetTNIOTFr(trama)==T_NIOT_PKT_ACK && trama[1]&M_NIOT_TRUEACK)
        return true;
    else
        return false;
}


bool CProtocolNIOT::IsACKW()
{
    return IsACKW(tramaRx, lonTramaRx);
}

bool CProtocolNIOT::IsACKW(unsigned char* trama, int lon)
{
    if (lon==0)
        return false;

    if (NIOT.GetTNIOTFr(trama)==T_NIOT_PKT_ACK && NIOT.GetTNIOTAck(trama)==0x15)
        return true;
    else
        return false;
}

//Retorna TRUE si es tracta d'una trama de dades (=No ACK, i NO NACK)
bool CProtocolNIOT::IsDades(unsigned char* trama, int lon)
{
    CNIOT m;
    if (m.GetTNIOTFr(trama)!=T_NIOT_PKT_ACK)
        return true;

    return false;
}

bool CProtocolNIOT::IsDades()
{
    return IsDades(tramaRx, lonTramaRx);
}

bool CProtocolNIOT::IsProcesBinRx()
{
    return isBinRx;
}

bool CProtocolNIOT::IsTramaDeGrup(unsigned char *trama, int lon)
{
    if (lon==0)
        return false;

    CNIOT m;
    if (m.IsBroadCast(trama))
        return true;
    else
        return false;
}

bool CProtocolNIOT::IsTramaDeGrup()
{
    return IsTramaDeGrup(tramaTx, lonTramaTx);
}

void CProtocolNIOT::DiscardRx(int num=1)
{
    for (int i=0;i<num;i++){
        if (SearchAnswer(true)==false)
            break;
    }

    lonTramaRx=0;
}

void CProtocolNIOT::ClearRxQueue()
{
/*    for (int i=0; i<cuaRx.size(); i++)
        delete [] cuaRx[i];

    cuaRx.clear();
    cuaRxLon.clear();*/
}

void CProtocolNIOT::Stop()
{
    online=false;
    ClearRxQueue();
    lonTramaRx=0;
    lonTramaTx=0;
    lonTramaTmp=0;
    dinsTrama=false;
}

void CProtocolNIOT::SetSN(string sn)
{
    this->SN=sn;
    NIOT.SetSN(sn);
}

string CProtocolNIOT::GetSN()
{
    return this->SN;
}

void CProtocolNIOT::Setconnecthandler(CConnection *connecthandler)
{
    this->connecthandler = connecthandler;
}

CConnection* CProtocolNIOT::Getconnecthandler()
{
    return connecthandler;
}


bool CProtocolNIOT::SocketLost()
{
    if (online && connecthandler)
        connecthandler->SocketLost();

    Stop();
}

string& CProtocolNIOT::GetLastError()
{
    return msgError;
}



void CProtocolNIOT::Start(int level,int tipus,wxSocketBase *sock)
{

    myLog=NULL;            // embedded nuvalink  will assign to a wxTextCtrl
    reliefmode=false;
    if (!connecthandler){
        connecthandler=new CConnection();
        connecthandler->SetType(tipus);
        connecthandler->SetSocketTCPIP(sock);
    }

    WantedIden1=0xFF;              // receives all info frames.
    Hops=0;
    privlevel=level;
//    if (timerconnecthandler) timerconnecthandler->Start(TIMER_connecthandler_VALUE, false);
//    if (timerHello) timerHello->Start(TIMER_HELLO_VALUE, false); //Cada 20 segons
    //cuaRx.clear();
//    tramaTx.clear();
//    tramaRx.clear();
    lonTramaRx=0;
    lonTramaTx=0;
    lonTramaTmp=0;
    threaddisable=true;         // to avoid thread entry too early
    online=true;
    sequencia=false;
    dinsTrama=false;

}

bool CProtocolNIOT::IsOnline()
{
    return online;
}

bool CProtocolNIOT::SetAesKey(string key)
{
    this->aeskey=key;
}

bool CProtocolNIOT::SetSubscriber(string code)
{
    this->cccc=code;
}


bool CProtocolNIOT::UseAes(bool useit)
{
    this->useaes=useit;
}

bool CProtocolNIOT::IsUseAes()
{
    return this->useaes;
}

void CProtocolNIOT::AfegirTramaCua(unsigned char *trama, int length)
{

    //unsigned char *buffTmp = new unsigned char [length];

    for (int a=0; a<length; a++)
        tramaRx[a] = trama[a];

    RxLon=length;

/*    protomutex.Lock();
    long int tamany=cuaRx.size();

    if (tamany>2 || tamany<0){
        protomutex.Unlock();
        return;
        //cuaRx.clear();
        //cuaRxLon.clear();
    }

    cuaRx.push_back(buffTmp);
    cuaRxLon.push_back(length);

    protomutex.Unlock();
    //if (IsTramaEstat(trama, length)*/
}

//A vegades les funcions de comunicacio retornen error pero pot ser un
//fallo poc greu com un timeout o masses NACKs seguits. En aquests casos
//es pot reintentar la operacio si la funcio que ens crida aixi ho decideix.
//
//Amb aquesta funcio consulta si l'ultim error era greu o si es pot reintentar
//la ultima operacio
bool CProtocolNIOT::EsPotReintentar()
{
    return esPotReintentar;
}

bool CProtocolNIOT::UserCancel()
{
    //myApp->Yield();
    if (progressDlg && progress) {
        if (!progressDlg->Update(*progress)) {
            msgError = MISS_ERROR_USER_ABORT;
            return true;
        }
    }
    else {
        //Cridem a l'aplicacio pq processi els missatges del windows
        if (myApp)
            myApp->Yield();
    }

    return false;
}

