#include "wx_pch.h"
#include "EstatPanel.h"
#include "NuvaLink.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(EstatPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(EstatPanel)
#include <wx/settings.h>
//*)

//(*IdInit(EstatPanel)
const long EstatPanel::ID_LISTCTRL1 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON1 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON2 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON3 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON4 = wxNewId();
const long EstatPanel::ID_LISTCTRL2 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON5 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON6 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON7 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON8 = wxNewId();
const long EstatPanel::ID_LISTCTRL3 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON9 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON10 = wxNewId();
const long EstatPanel::ID_LISTCTRL4 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON11 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON13 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON14 = wxNewId();
const long EstatPanel::ID_SPEEDBUTTON12 = wxNewId();
const long EstatPanel::ID_STATICTEXT1 = wxNewId();
const long EstatPanel::ID_STATICTEXT2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(EstatPanel,wxPanel)
	//(*EventTable(EstatPanel)
	//*)
END_EVENT_TABLE()

EstatPanel::EstatPanel(wxWindow* parent,wxWindowID id)
{
	BuildContent(parent,id);
}

void EstatPanel::BuildContent(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(EstatPanel)
	wxBoxSizer* BoxSizer1;
	wxBoxSizer* BoxSizer2;
	wxBoxSizer* BoxSizer3;
	wxBoxSizer* BoxSizer4;
	wxBoxSizer* BoxSizer5;
	wxBoxSizer* BoxSizer6;
	wxBoxSizer* BoxSizer7;
	wxBoxSizer* BoxSizer8;
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, wxID_ANY, wxDefaultPosition, wxSize(506,294), wxTAB_TRAVERSAL, _T("wxID_ANY"));
	SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_MENUBAR));
	FlexGridSizer1 = new wxFlexGridSizer(4, 2, 0, wxDLG_UNIT(this,wxSize(0,0)).GetWidth());
	FlexGridSizer1->AddGrowableCol(1);
	FlexGridSizer1->AddGrowableRow(1);
	StaticBoxSizer1 = new wxStaticBoxSizer(wxVERTICAL, this, _("Areas"));
	BoxSizer1 = new wxBoxSizer(wxVERTICAL);
	ListA = new wxListCtrl(this, ID_LISTCTRL1, wxDefaultPosition, wxSize(267,159), 0, wxDefaultValidator, _T("ID_LISTCTRL1"));
	BoxSizer1->Add(ListA, 1, wxALL|wxEXPAND, 5);
	BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
	wxBitmap BtArm_BMP(_("./icons/estat/armed.png"), wxBITMAP_TYPE_ANY);
	BtArm = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, BtArm_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	BtArm->SetUserData(0);
	BoxSizer2->Add(BtArm, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtDisarm_BMP(_("./icons/estat/disarmed.png"), wxBITMAP_TYPE_ANY);
	BtDisarm = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, BtDisarm_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	BtDisarm->SetUserData(0);
	BoxSizer2->Add(BtDisarm, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtNight_BMP(_("./icons/estat/moon.png"), wxBITMAP_TYPE_ANY);
	BtNight = new wxSpeedButton(this, ID_SPEEDBUTTON3, wxEmptyString, BtNight_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON3"));
	BtNight->SetUserData(0);
	BoxSizer2->Add(BtNight, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtHome_BMP(_("./icons/estat/home.png"), wxBITMAP_TYPE_ANY);
	BtHome = new wxSpeedButton(this, ID_SPEEDBUTTON4, wxEmptyString, BtHome_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON4"));
	BtHome->SetUserData(0);
	BoxSizer2->Add(BtHome, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	BoxSizer1->Add(BoxSizer2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
	StaticBoxSizer1->Add(BoxSizer1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer1, 1, wxALL|wxEXPAND, 5);
	StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Zones"));
	BoxSizer3 = new wxBoxSizer(wxVERTICAL);
	ListZ = new wxListCtrl(this, ID_LISTCTRL2, wxDefaultPosition, wxSize(296,161), 0, wxDefaultValidator, _T("ID_LISTCTRL2"));
	BoxSizer3->Add(ListZ, 1, wxALL|wxEXPAND, 5);
	BoxSizer4 = new wxBoxSizer(wxHORIZONTAL);
	wxBitmap BtOmit_BMP(_("./icons/estat/omit.png"), wxBITMAP_TYPE_ANY);
	BtOmit = new wxSpeedButton(this, ID_SPEEDBUTTON5, wxEmptyString, BtOmit_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON5"));
	BtOmit->SetUserData(0);
	BoxSizer4->Add(BtOmit, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtRestore_BMP(_("./icons/estat/on.png"), wxBITMAP_TYPE_ANY);
	BtRestore = new wxSpeedButton(this, ID_SPEEDBUTTON6, wxEmptyString, BtRestore_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON6"));
	BtRestore->SetUserData(0);
	BoxSizer4->Add(BtRestore, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtCam_BMP(_("./icons/estat/cam_32.png"), wxBITMAP_TYPE_ANY);
	BtCam = new wxSpeedButton(this, ID_SPEEDBUTTON7, wxEmptyString, BtCam_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON7"));
	BtCam->SetUserData(0);
	BoxSizer4->Add(BtCam, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtEye_BMP(_("./icons/estat/eye_icon.png"), wxBITMAP_TYPE_ANY);
	BtEye = new wxSpeedButton(this, ID_SPEEDBUTTON8, wxEmptyString, BtEye_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON8"));
	BtEye->SetUserData(0);
	BoxSizer4->Add(BtEye, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	BoxSizer3->Add(BoxSizer4, 0, wxALL|wxEXPAND, 2);
	StaticBoxSizer2->Add(BoxSizer3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 2);
	FlexGridSizer1->Add(StaticBoxSizer2, 1, wxALL|wxEXPAND, 5);
	StaticBoxSizer3 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Outputs"));
	BoxSizer5 = new wxBoxSizer(wxVERTICAL);
	ListO = new wxListCtrl(this, ID_LISTCTRL3, wxDefaultPosition, wxSize(238,80), 0, wxDefaultValidator, _T("ID_LISTCTRL3"));
	BoxSizer5->Add(ListO, 1, wxALL|wxEXPAND, 5);
	BoxSizer7 = new wxBoxSizer(wxHORIZONTAL);
	wxBitmap BtOn_BMP(_("./icons/estat/on.png"), wxBITMAP_TYPE_ANY);
	BtOn = new wxSpeedButton(this, ID_SPEEDBUTTON9, wxEmptyString, BtOn_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON9"));
	BtOn->SetUserData(0);
	BoxSizer7->Add(BtOn, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtOff_BMP(_("./icons/estat/omit.png"), wxBITMAP_TYPE_ANY);
	BtOff = new wxSpeedButton(this, ID_SPEEDBUTTON10, wxEmptyString, BtOff_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON10"));
	BtOff->SetUserData(0);
	BoxSizer7->Add(BtOff, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	BoxSizer5->Add(BoxSizer7, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer3->Add(BoxSizer5, 1, wxALL|wxEXPAND, 5);
	FlexGridSizer1->Add(StaticBoxSizer3, 1, wxALL|wxEXPAND, 5);
	StaticBoxSizer4 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("System"));
	BoxSizer6 = new wxBoxSizer(wxVERTICAL);
	ListSys = new wxListCtrl(this, ID_LISTCTRL4, wxDefaultPosition, wxSize(221,116), 0, wxDefaultValidator, _T("ID_LISTCTRL4"));
	BoxSizer6->Add(ListSys, 1, wxALL|wxEXPAND, 5);
	BoxSizer8 = new wxBoxSizer(wxHORIZONTAL);
	wxBitmap BtTest_BMP(_("./icons/estat/test.png"), wxBITMAP_TYPE_ANY);
	BtTest = new wxSpeedButton(this, ID_SPEEDBUTTON11, _("test"), BtTest_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(80,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON11"));
	BtTest->SetUserData(0);
	BoxSizer8->Add(BtTest, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtResetBatt_BMP(_("./icons/estat/nobattery.png"), wxBITMAP_TYPE_ANY);
	BtResetBatt = new wxSpeedButton(this, ID_SPEEDBUTTON13, _("resbatt"), BtResetBatt_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(80,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON13"));
	BtResetBatt->SetUserData(0);
	BoxSizer8->Add(BtResetBatt, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap sampleAn_BMP(_("./icons/estat/wave.png"), wxBITMAP_TYPE_ANY);
	sampleAn = new wxSpeedButton(this, ID_SPEEDBUTTON14, wxEmptyString, sampleAn_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(50,50), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON14"));
	sampleAn->SetUserData(0);
	BoxSizer8->Add(sampleAn, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap BtSync_BMP(_("./icons/estat/clocksyn.png"), wxBITMAP_TYPE_ANY);
	BtSync = new wxSpeedButton(this, ID_SPEEDBUTTON12, wxEmptyString, BtSync_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(48,48), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON12"));
	BtSync->SetUserData(0);
	BoxSizer8->Add(BtSync, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	PanelTxt = new wxStaticText(this, ID_STATICTEXT1, _("Panel"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	BoxSizer8->Add(PanelTxt, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	VerTxt = new wxStaticText(this, ID_STATICTEXT2, _("Version"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	BoxSizer8->Add(VerTxt, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	BoxSizer6->Add(BoxSizer8, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer4->Add(BoxSizer6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(StaticBoxSizer4, 1, wxALL|wxEXPAND, 5);
	SetSizer(FlexGridSizer1);
	SetSizer(FlexGridSizer1);
	Layout();

	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtArmLeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtDisarmLeftClick);
	Connect(ID_SPEEDBUTTON3,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtNightLeftClick);
	Connect(ID_SPEEDBUTTON4,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtHomeLeftClick);
	Connect(ID_SPEEDBUTTON5,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtOmitLeftClick);
	Connect(ID_SPEEDBUTTON6,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtRestoreLeftClick);
	Connect(ID_SPEEDBUTTON9,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtOnLeftClick);
	Connect(ID_SPEEDBUTTON10,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtOffLeftClick);
	Connect(ID_SPEEDBUTTON11,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtTestLeftClick);
	Connect(ID_SPEEDBUTTON13,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtResetBattLeftClick);
	Connect(ID_SPEEDBUTTON14,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnsampleAnLeftClick);
	Connect(ID_SPEEDBUTTON12,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EstatPanel::OnBtSyncLeftClick);
	//*)

}

EstatPanel::~EstatPanel()
{
	//(*Destroy(EstatPanel)
	//*)
}

void EstatPanel::SetStatus(CStatus* estat,Cnuvalink* nuvalink,CLanguage* lang,string id,string ver)
{
    this->estat = estat;
    this->nuvalink = nuvalink;
    this->Lang= lang;

    StaticBoxSizer1->GetStaticBox()->SetLabel(Lang->GetModelMiss("MISS_AREAS"));
    StaticBoxSizer2->GetStaticBox()->SetLabel(Lang->GetModelMiss("MISS_ZONES"));
    StaticBoxSizer3->GetStaticBox()->SetLabel(Lang->GetModelMiss("MISS_OUTPUTS"));
    StaticBoxSizer4->GetStaticBox()->SetLabel(Lang->GetModelMiss("MISS_SYSTEM"));
    VerTxt->SetLabel(ver);
    PanelTxt->SetLabel(id);
    UpdateValues();
    return true;
}

void EstatPanel::UnSetStatus()
{
    estat=0;

}

CStatus *EstatPanel::GetStatus()
{
    return estat;
}

#ifdef _NOTDEF
bool EstatPanel::SetGrups(CGroup *grupZAlias, CGroup *grupAAlias, CGroup *grupOAlias)
{

    for (int i=0;i<64;i++)
    {
        string temp=MISS_PANEL_COL_ZONE;
        temp+=" ";
        temp+=iToS(i+1);
        CZalias[i]=temp;
    }
    if(grupZAlias!=0)
    {
        for (int i=0; i<grupZAlias->GetNumChildCodi(); i++) {
            this->codi = grupZAlias->GetChildCodi(i);
            CZalias[i]= codi->GetStringVal();
        }
    }

    for (int i=0;i<64;i++){
        string temp =MISS_PANEL_COL_AREA;
        temp +=" ";
        temp +=iToS(i+1);
        CAalias[i]=temp;
    }

    if (grupAAlias!=0)
    {
        for (int i=0; i<grupAAlias->GetNumChildCodi(); i++) {
            this->codi = grupAAlias->GetChildCodi(i);
            CAalias[i]= codi->GetStringVal();
        }
    }

    for (int i=0;i<64;i++){
        string temp=MISS_PANEL_OUTPUT;
        temp+=" ";
        temp+=iToS(i+1);
        COalias[i]=temp;
    }

    if(grupOAlias!=0)
    {
        for (int i=0; i<grupOAlias->GetNumChildCodi(); i++) {
            this->codi = grupOAlias->GetChildCodi(i);
            COalias[i]= codi->GetStringVal();
        }
    }

   return true;
}
#endif

void EstatPanel::UpdateValues()
{
    string hora = estat->GetTime();
    string data = estat->GetDate();
    //DateTimeTxt->SetLabel(_wx(data+ " " + hora));

    ListA->ClearAll();
    wxImageList* listBmp	 = new wxImageList(32,32);
    ListA->SetImageList(listBmp,wxIMAGE_LIST_NORMAL);
    for (int i=0;i<estat->GetNumAreas();i++){
        WORD st=estat->GetAreaSt(i);
        if (st&M_AREA_ENABLED){
            if (st&M_AREA_ARM){
                if (st&M_AREA_IN){
                    listBmp->Add(wxBitmap("icons/estat/routein.png",wxBITMAP_TYPE_PNG));
                }else if (st&M_AREA_OUT){
                    listBmp->Add(wxBitmap("icons/estat/routeout.png",wxBITMAP_TYPE_PNG));
                }else if (st&M_AREA_NIGHT){
                    listBmp->Add(wxBitmap("icons/estat/moon.png",wxBITMAP_TYPE_PNG));
                }else if (st&M_AREA_PERIMETER){
                    listBmp->Add(wxBitmap("icons/estat/home.png",wxBITMAP_TYPE_PNG));
                }else{
                    listBmp->Add(wxBitmap("icons/estat/armed.png",wxBITMAP_TYPE_PNG));
                }
            }else{
                listBmp->Add(wxBitmap("icons/estat/disarmed.png",wxBITMAP_TYPE_PNG));
            }
            ListA->InsertItem(i,"Area: "+iToS(i+1),i);
        }
    }

    ListZ->ClearAll();

    wxImageList* listBmp1	 = new wxImageList(32,32);
    ListZ->SetImageList(listBmp1,wxIMAGE_LIST_NORMAL);
    wxString label;
    for (int i=0;i<estat->GetNumZones();i++){
        if (estat->IsOmiting(i)){
            label = Lang->GetAppMiss("MISS_PANEL_OMITED");
            listBmp1->Add(wxBitmap("icons/estat/omit.png",wxBITMAP_TYPE_PNG));
        }
        else if (estat->IsAlarm(i)){
            label = Lang->GetAppMiss("MISS_PANEL_ALARM");
            listBmp1->Add(wxBitmap("icons/estat/bell.png",wxBITMAP_TYPE_PNG));
        }
        else if (estat->IsDetecting(i) && estat->IsAvery(i)){
            label= "Tamper";
            listBmp1->Add(wxBitmap("icons/estat/bell.png", wxBITMAP_TYPE_PNG));
        }
        else if (estat->IsAvery(i)){
            label = Lang->GetAppMiss("MISS_PANEL_AVERY");
            listBmp1->Add(wxBitmap("icons/estat/warning.png",wxBITMAP_TYPE_PNG));
        }
        else if (estat->IsDetecting(i)){
            label = Lang->GetAppMiss("MISS_PANEL_DETECT");
            listBmp1->Add(wxBitmap("icons/estat/sensordetection.png",wxBITMAP_TYPE_PNG));

        }
        else{
            label = Lang->GetAppMiss("MISS_PANEL_OK");
            listBmp1->Add(wxBitmap("icons/estat/ok.png",wxBITMAP_TYPE_PNG));
        }
        ListZ->InsertItem(i,"Zone:"+iToS(i+1),i);
    }
    ListO->ClearAll();
    wxImageList* listBmp2	 = new wxImageList(32,32);
    ListO->SetImageList(listBmp2,wxIMAGE_LIST_NORMAL);
    for (int i=0;i<estat->GetNumSortides();i++)
    {
        if (estat->GetOutSt(i)){
            listBmp2->Add(wxBitmap("./icons/estat/on2.png",wxBITMAP_TYPE_PNG));
        }else{
            listBmp2->Add(wxBitmap("./icons/estat/omit.png",wxBITMAP_TYPE_PNG));
        }
        ListO->InsertItem(i,"OutPut:"+iToS(i+1),i);
    }

    ListSys->ClearAll();
    wxImageList* listBmp3	 = new wxImageList(32,32);
    ListSys->SetImageList(listBmp3,wxIMAGE_LIST_NORMAL);
    unsigned char stPanel = estat->GetStPanel();
    int i=0;
    if (stPanel & M_STPANEL_MEM_ALARM) {
        listBmp3->Add(wxBitmap("icons/estat/alarmmem.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"Alarm memory",i);
        i++;
    }
    if (stPanel & M_STPANEL_PREFIRE) {
        listBmp3->Add(wxBitmap("icons/estat/prefire.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"Prefire",i);
        i++;
    }
    if (stPanel & M_STPANEL_ERROR_TIME) {
        listBmp3->Add(wxBitmap("icons/estat/clockerror.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"clock fail",i);
        i++;
    }
    if (stPanel & M_STPANEL_ACFAIL/*estat->STSYS.SysSt.TZAC*/) {
        listBmp3->Add(wxBitmap("icons/estat/acfail.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"AC Fail",i);
        i++;
    }
    if (stPanel & M_STPANEL_LOWBATT/*estat->STSYS.SysSt.TZBatt*/) {
        listBmp3->Add(wxBitmap("icons/estat/nobattery.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"Batt Low",i);
        i++;
    }
    if (stPanel & M_STPANEL_FAIL_ALARMTX) {
        listBmp3->Add(wxBitmap("icons/estat/comfail.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"Comm Fail",i);
        i++;
    }
    int cobertura = estat->STSYS.GsmSt.Coverture;
    if (cobertura){
        listBmp3->Add(wxBitmap("icons/estat/cover.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,iToS(cobertura)+"%",i);
        i++;
    }
    if (estat->STSYS.IPSt.StIP & M_STIP_GPRSON){
        listBmp3->Add(wxBitmap("icons/estat/cell.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"Celular IP",i);
        i++;
    }
    if (estat->STSYS.IPSt.StIP & M_STIP_ISWIFI){
        listBmp3->Add(wxBitmap("icons/estat/wifiok.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"wifi"/*:-"+iToS(estat->STSYS.IPSt.Wifirssi*/,i);
        i++;
    }else if (estat->STSYS.IPSt.StIP & M_STIP_WIFIERROR){
        listBmp3->Add(wxBitmap("icons/estat/wifiok.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"wifi Fail",i);
        i++;
    }
    if (estat->STSYS.IPSt.EthSt==MAC_LINKED){
        listBmp3->Add(wxBitmap("icons/estat/ethok.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"ETH Linked",i);
        i++;
    }else if (estat->STSYS.IPSt.EthSt<MAC_LINKED){
        listBmp3->Add(wxBitmap("icons/estat/ethko.png",wxBITMAP_TYPE_PNG));
        ListSys->InsertItem(i,"ETH Fail",i);
        i++;
    }
}

void EstatPanel::OnBtArmLeftClick(wxCommandEvent& event)
{
    WORD areesMask=0x0000;
    long itemindex=-1;
    while ((itemindex=ListA->GetNextItem(itemindex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))>=0){
        if (itemindex<estat->GetNumAreas())
        areesMask|=((0x01)<<itemindex);
    }
    if (areesMask){
        if (!nuvalink->SendAreaArm(areesMask, true)) {
            string err = nuvalink->GetLastError();
            wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        }
        UpdateValues();
    }
}

void EstatPanel::OnBtDisarmLeftClick(wxCommandEvent& event)
{
    WORD areesMask=0x0000;
    long itemindex=-1;
    while ((itemindex=ListA->GetNextItem(itemindex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))>=0){
        if (itemindex<estat->GetNumAreas())
        areesMask|=((0x01)<<itemindex);
    }
    if (areesMask){
        if (!nuvalink->SendAreaArm(areesMask, false)) {
            string err = nuvalink->GetLastError();
            wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        }
        UpdateValues();
    }
}

void EstatPanel::OnBtNightLeftClick(wxCommandEvent& event)
{
    WORD areesMask=0x0000;
    long itemindex=-1;
    while ((itemindex=ListA->GetNextItem(itemindex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))>=0){
        if (itemindex<estat->GetNumAreas())
        areesMask|=((0x01)<<itemindex);
    }
    if (areesMask){
        if (!nuvalink->SendAreaArm(areesMask | M_USER_NIGHTARM, true)) {
            string err = nuvalink->GetLastError();
            wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        }
        UpdateValues();
    }

}

void EstatPanel::OnBtHomeLeftClick(wxCommandEvent& event)
{
    unsigned char areesMask=0x00;
    long itemindex=-1;
    while ((itemindex=ListA->GetNextItem(itemindex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))>=0){
        if (itemindex<estat->GetNumAreas())
        areesMask|=((0x01)<<itemindex);
    }
    if (areesMask){
        if (!nuvalink->SendAreaArm(areesMask | M_USER_NIGHTARM, true)) {
            string err = nuvalink->GetLastError();
            wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        }
        UpdateValues();
    }
}

void EstatPanel::OnBtSyncLeftClick(wxCommandEvent& event)
{
    nuvalink->SendDateTime(NULL);
}

void EstatPanel::OnBtTestLeftClick(wxCommandEvent& event)
{
    nuvalink->SendReportTest();
}

void EstatPanel::OnBtOnLeftClick(wxCommandEvent& event)
{
    ActivateOutputs(true);
}

void EstatPanel::OnBtOffLeftClick(wxCommandEvent& event)
{
    ActivateOutputs(false);
}

void EstatPanel::ActivateOutputs(bool activar)
{
    unsigned char sortidesMask=0x00;
    bool ctrlrelay=false;
    bool ctrlout=false;

    nuvalink->progress=0;
    wxProgressDialog *myPD = new wxProgressDialog(Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"), Lang->GetAppMiss("MISS_DIALOG_TIT_SEND"),
                13, NULL, wxPD_AUTO_HIDE | wxPD_APP_MODAL | wxPD_CAN_ABORT | wxPD_SMOOTH);
    nuvalink->progressDlg = myPD;


    long itemindex=-1;
    while ((itemindex=ListO->GetNextItem(itemindex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))>=0){
        if (itemindex<estat->GetNumSortides()){
            ctrlout=true;
            if (!nuvalink->SendOutputAct(itemindex, activar)) {
                string err = nuvalink->GetLastError();
                wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
                break;
            }
        }
    }

    if (ctrlout){
//        if (!nuvalink->RxStatus(true)) {
//            string err = nuvalink->GetLastError();
//            wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
//        }
    }

    delete myPD;
    nuvalink->progressDlg=0;
    nuvalink->progress=0;

    UpdateValues();

}

void EstatPanel::OnBtOmitLeftClick(wxCommandEvent& event)
{
    listzselect=false;

    long itemindex=-1;
    while ((itemindex=ListZ->GetNextItem(itemindex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))>=0){
        if (itemindex<estat->GetNumZones()){
            if (!nuvalink->SendZBypass(itemindex)) {
                string err = nuvalink->GetLastError();
                wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
                break;
            }
        }
    }
    //nuvalink->RxStatus(false);
    UpdateValues();

}

void EstatPanel::OnBtRestoreLeftClick(wxCommandEvent& event)
{
    listzselect=false;

    long itemindex=-1;
    while ((itemindex=ListZ->GetNextItem(itemindex,wxLIST_NEXT_ALL,wxLIST_STATE_SELECTED))>=0){
        if (itemindex<estat->GetNumZones()){
            if (!nuvalink->SendZBypass(itemindex)) {
                string err = nuvalink->GetLastError();
                wxMessageBox(_wx(err), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
                break;
            }
        }
    }
    //nuvalink->RxStatus(false);
    UpdateValues();
}



void EstatPanel::OnBtResetBattLeftClick(wxCommandEvent& event)
{
    nuvalink->SetBattmAhCntReset();
}

void EstatPanel::OnsampleAnLeftClick(wxCommandEvent& event)
{
    wxString Schannel=wxGetTextFromUser("AD Channel","ADChannel","2");
    string sch=Schannel.c_str();
    int channel=sToI(sch);
    wxString Ssamples=wxGetTextFromUser("Num samples","Num samples","4096");
    string ssam=Ssamples.c_str();
    int samples=sToI(ssam);

    nuvalink->RecordWave(channel,samples);

}
