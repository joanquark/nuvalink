#include "panelmesura.h"

CPanelMesura::CPanelMesura(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CUnLang* lang)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,25);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,25);
    }
    UnLang=lang;
    label=0;
    valor=0;
    mesura=0;
    hSizer = new wxBoxSizer( wxHORIZONTAL );
    SetSizer( hSizer );

}

void CPanelMesura::Clear()
{
    if (label) {
        delete label;
        label=0;
    }
    if (valor) {
        delete valor;
        valor=0;
    }

    int i=0;
    while (hSizer->Detach(i++));

}

CPanelMesura::~CPanelMesura()
{
    Clear();
    mesura=0;
}

wxPoint& CPanelMesura::VwXSetwxPoint(long x,long y)
{
    m_tmppoint.x=x;
    m_tmppoint.y=y;
    return m_tmppoint;
}

wxSize& CPanelMesura::VwXSetwxSize(long w,long h){
    m_tmpsize.SetWidth(w);
    m_tmpsize.SetHeight(h);
    return m_tmpsize;
}

void CPanelMesura::GeneraComponents()
{
    if (mesura) {
        Clear();

        //SetBackgroundColour(wxColour(0xff,0xff,0xff));
        SetBackgroundColour(wxColour(0xdd,0xdd,0xdd));

        label = new wxStaticText(this,-1,wxT(""),wxDefaultPosition,wxSize(1,-1),wxST_NO_AUTORESIZE|wxNO_BORDER);
        //label->SetBackgroundColour(wxColour(0x30,0x30,0x90));
        //label->SetForegroundColour(wxColour(0xff,0xff,0xff));
//        label->SetTitle(wxT("Description"));
        label->SetLabel(_wx(mesura->GetDescr()));
        //label->SetFont(wxFont(12,74,90,90,0,wxT("Times New Roman")));

        valor = new wxStaticText(this,-1,wxT(""),wxDefaultPosition,wxSize(1,-1),wxST_NO_AUTORESIZE|wxSIMPLE_BORDER);
        //valor->SetFont(wxFont(12,74,90,90,0,wxT("Times New Roman")));

        ActualitzaValors();

        hSizer->Add(label, wxSizerFlags(3).Center().Border(wxLEFT, 5));
        hSizer->Add(valor, wxSizerFlags(2).Right().Border(wxLEFT, 2));
        hSizer->Layout();
    }
}

bool CPanelMesura::SetMesura(CMesura *mesura)
{
    this->mesura = mesura;
    GeneraComponents();
    return true;
}

void CPanelMesura::UnSetMesura()
{
    mesura=0;
    Clear();
}

CMesura *CPanelMesura::GetMesura()
{
    return mesura;
}

//Aplica no te sentit en aquest panel perque es nomes de lectura
bool CPanelMesura::AplicaValors()
{
    return true;
}

//Actualitza els valors de la CMesura al camp de text
//(es el contrari que AplicaValors()
bool CPanelMesura::ActualitzaValors()
{
    if (!mesura)
        return false;

    string lab;

    if (!mesura->Calcula())
        lab = "-";
    else
        lab = mesura->GetStringVal();

    valor->SetLabel(_wx(lab));

    return true;
}
