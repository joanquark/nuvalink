[Setup]                           
AppName=NuvaLink
AppVerName=NuvaLink version 2.10
AppPublisher=Nuvathings
AppPublisherURL=http://www.nuvathings.com
AppSupportURL=http://www.nuvathings.com
AppUpdatesURL=http://www.nuvathings.com
DefaultDirName={pf}\NuvaLink
DefaultGroupName=NuvaLink
AllowNoIcons=yes
OutputDir=C:\LOCALPRJ\01.Cpp\NuvaLink\Release\output
OutputBaseFilename=NuvaLink-setup-v2.10
Compression=lzma
SolidCompression=yes
ChangesAssociations=yes

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: es; MessagesFile: "compiler:Languages\Spanish.isl"
Name: fr; MessagesFile: "compiler:Languages\French.isl"
Name: it; MessagesFile: "compiler:Languages\Italian.isl"
Name: pt; MessagesFile: "compiler:Languages\Portuguese.isl"
                                                            
[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLinkun.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\wolfSSL.dll"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\*.bin"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\config_ES.xml"; DestName: "config.xml"; DestDir: "{app}"; Languages: es;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\config_ES.xml"; DestName: "config.xml"; DestDir: "{app}"; Languages: pt;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\config_EN.xml"; DestName: "config.xml"; DestDir: "{app}"; Languages: en;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\config_EN.xml"; DestName: "config.xml"; DestDir: "{app}"; Languages: fr;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\config_EN.xml"; DestName: "config.xml"; DestDir: "{app}"; Languages: it;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\nvt.nvk"; DestDir: "{app}";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\chord.wav"; DestDir: "{app}";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\tramas.sav"; DestDir: "{app}";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\tada.wav"; DestDir: "{app}";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\models\SPT\*.xml"; DestDir: "{app}\models\SPT";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\models\LIFT\*.xml"; DestDir: "{app}\models\LIFT";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\models\TRACK\*.xml"; DestDir: "{app}\models\TRACK";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\icons\*"; DestDir: "{app}\icons";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\icons\estat\*"; DestDir: "{app}\icons\estat";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\icons\toolbar\*"; DestDir: "{app}\icons\toolbar";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\icons\grafic\*"; DestDir: "{app}\icons\grafic";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\drivers\*"; DestDir: "{app}\drivers";
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\Lang_ES.xml"; DestName: "lang.xml"; DestDir: "{app}"; Languages: es;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\Lang_EN.xml"; DestName: "lang.xml"; DestDir: "{app}"; Languages: en;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\Lang_FR.xml"; DestName: "lang.xml"; DestDir: "{app}"; Languages: fr;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\Lang_IT.xml"; DestName: "lang.xml"; DestDir: "{app}"; Languages: it;
Source: "C:\LOCALPRJ\01.Cpp\NuvaLink\Release\NuvaLink\Lang_PT.xml"; DestName: "lang.xml"; DestDir: "{app}"; Languages: pt;

; NOTE: Don't use "Flags: ignoreversion" on any shared system files


[Icons]
Name: "{group}\NuvaLink"; Filename: "{app}\NuvaLinkun.exe"; WorkingDir: "{app}";

Name: "{group}\{cm:UninstallProgram,NuvaLink}"; Filename: "{uninstallexe}"
Name: "{userdesktop}\NuvaLink"; Filename: "{app}\NuvaLinkun.exe"; Tasks: desktopicon; WorkingDir: "{app}"
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\NuvaLink"; Filename: "{app}\NuvaLinkun.exe"; Tasks: quicklaunchicon; WorkingDir: "{app}"

[Registry]
Root: HKCR; Subkey: ".nli"; ValueType: string; ValueName: ""; ValueData: "NuvaLink"; Flags: uninsdeletevalue
Root: HKCR; Subkey: "NuvaLink"; ValueType: string; ValueName: ""; ValueData: "panel IOT"; Flags: uninsdeletekey; Languages:es;
Root: HKCR; Subkey: "NuvaLink"; ValueType: string; ValueName: ""; ValueData: "IOT panel"; Flags: uninsdeletekey; Languages:en;
Root: HKCR; Subkey: "NuvaLink"; ValueType: string; ValueName: ""; ValueData: "IOT panel"; Flags: uninsdeletekey; Languages:fr;
Root: HKCR; Subkey: "NuvaLink"; ValueType: string; ValueName: ""; ValueData: "IOT panel"; Flags: uninsdeletekey; Languages:it;
Root: HKCR; Subkey: "NuvaLink\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\NuvaLinkun.exe,0"
Root: HKCR; Subkey: "NuvaLink\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\NuvaLinkun.exe"" ""%1"""

[Run]
Filename: "{app}\NuvaLinkun.exe"; Description: "{cm:LaunchProgram,NuvaLink}"; Flags: nowait postinstall skipifsilent

