//---------------------------------------------------------------------------
//
// Name:        eLigthFrm.cpp
// Author:      Emili
// Created:     12/1/2009 9:41:51
// Description: eLigthFrm class implementation
//
//---------------------------------------------------------------------------

#include "eLigthFrm.h"

//Do not add custom headers between
//Header Include Start and Header Include End
//wxDev-C++ designer will remove them
////Header Include Start
////Header Include End

//----------------------------------------------------------------------------
// eLigthFrm
//----------------------------------------------------------------------------
//Add Custom Events only in the appropriate block.
//Code added in other places will be removed by wxDev-C++
////Event Table Start
BEGIN_EVENT_TABLE(eLigthFrm,wxFrame)
	////Manual Code Start
	////Manual Code End
	
	EVT_CLOSE(eLigthFrm::OnClose)
END_EVENT_TABLE()
////Event Table End

eLigthFrm::eLigthFrm(wxWindow *parent, wxWindowID id, const wxString &title, const wxPoint &position, const wxSize& size, long style)
: wxFrame(parent, id, title, position, size, style)
{
	CreateGUIControls();
}

eLigthFrm::~eLigthFrm()
{
}

void eLigthFrm::CreateGUIControls()
{
	//Do not add custom code between
	//GUI Items Creation Start and GUI Items Creation End
	//wxDev-C++ designer will remove them.
	//Add the custom code before or after the blocks
	////GUI Items Creation Start

	WxPanel1 = new wxPanel(this, ID_WXPANEL1, wxPoint(0,0), wxSize(566,420));
	WxPanel1->SetFont(wxFont(8, wxSWISS, wxNORMAL,wxNORMAL, false, wxT("Tahoma")));

	WxButton1 = new wxButton(WxPanel1, ID_WXBUTTON1, wxT("WxButton1"), wxPoint(146,70), wxSize(191,139), 0, wxDefaultValidator, wxT("WxButton1"));
	WxButton1->SetFont(wxFont(8, wxSWISS, wxNORMAL,wxNORMAL, false, wxT("Tahoma")));

	SetTitle(wxT("eLigth"));
	SetIcon(wxNullIcon);
	SetSize(19,23,574,447);
	Center();
	
	////GUI Items Creation End
}

void eLigthFrm::OnClose(wxCloseEvent& event)
{
	Destroy();
}
