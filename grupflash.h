#ifndef GRUPFLASH_H
#define GRUPFLASH_H

#include <iostream>
#include <vector>
#include <string>
using namespace std;

#include "grup.h"
#include "codi.h"
#include "fitxerflash.h"

#define TIPUS_GRUPFLASH_COLUMNES    1
#define TIPUS_GRUPFLASH_FITXERS     2

class CGroupFlash
{
    wxString descr;
    string id;
    string address;
    CGroup *grupPare;
    int upl;
    string cat;
    //vector<string> colDescr;
    //vector<int> colTipus;
    vector<CCodi *> colCodi;
    vector<CFitxerFlash *> fitxers;
    int bytesElement, numElements, tipus;
    unsigned char *flash;

    public:
        CGroupFlash();
        CGroupFlash(CGroup *grupPare, wxString& descr);
        void Clear();
        ~CGroupFlash();

        bool SetDescr(wxString &descr);
        bool SetId(string &id);
        bool SetFather(CGroup *grupPare);
        CGroup* GetFather();
        bool SetAddress(string& address);
        bool SetUpl(int upl);
        int GetUpl(void);
        string& GetCat();
        bool SetCat(string& cat);
        wxString& GetDescr();
        wxString& GetColDescr(int col);
        string& GetId();
        string& GetAddress();
        bool Reset();

        //Dira si es tipus Columnes o Fitxers
        int GetTypeCon();

        //Columnes
        bool SetBytesElement(int bytes);
        bool SetNumElements(int n);
        int AddColumna();
        bool SetColumnaTipus(int col, int tipus);
        bool SetColumnaDescr(int col, wxString& descr);
        bool SetColumnaVal(int col, string& data);
        bool Create();

        bool SetElement(int numElement, unsigned char *data);
        bool VerificaElement(int numElement, unsigned char *data);
        bool SetElementCol(int numElement, int col, unsigned char *data);
        bool SetElementCol(int numElement, int col, string& data);
        int GetNumElements();
        int GetBytesElement();
        int GetNumCols();
        unsigned char* GetElement(int numElement);
        unsigned char* GetElementCol(int numElement, int col);
        string GetElementColString(int numElement, int col);
        CCodi *GetColumnaCodi(int col);

        //Fitxers
        int AddFitxer(CFitxerFlash *fitxer);
        int GetNumFiles();
        int GetBytesFitxers();
        CFitxerFlash* GetFile(int index);
        CFitxerFlash* GetFileById(wxString id);

    private:
        int lengthByTipus(int tipus);
};

#endif
