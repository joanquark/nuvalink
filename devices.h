
#ifndef _DEVICES_H
#define _DEVICES_H

#include <string>
using namespace std;

typedef unsigned char BYTE;

typedef struct{
    string id;
    string version;
    string verdate;
    string hard;
    string flashid;
    string bversion;        // boot version.
    string bverdate;        // boot date.
}TDevInf;

#define MANUFACTURER_MICRON				0x20
#define MANUFACTURER_MACRONIX			0xC2

#define SFLASH_8MBIT					0x14
#define SFLASH_16MBIT					0x15
#define SFLASH_32MBIT					0x16
#define SFLASH_64MBIT					0x17
#define SFLASH_128MBIT					0x18
#define SFLASH_256MBIT					0x19


	// Model specific addresses, depend on FlashID.
	#define ADD_FLASH_EVENTQUEUE			0x010000
	#define LONG_FLASH_EVENTQUEUE_256MB		651264		// 0x00010000 to 0x00A00000 = 651264*16
	#define LONG_FLASH_EVENTQUEUE_128MB		323584		// 0x00010000 to 0x00500000 = 323584*16
	#define LONG_FLASH_EVENTQUEUE_64MB		225280		// 0x00010000 to 0x00380000 = 225280*16
	#define LONG_FLASH_EVENTQUEUE_32MB		126976		// 0x00010000 to 0x00200000 = 126976*16
	#define LONG_FLASH_EVENTQUEUE_16MB		61440		// 0x00010000 to 0x00100000 = 61440*16
	#define LONG_FLASH_EVENTQUEUE_8MB		36864		// 0x00010000 to 0x000A0000 = 36864*16

	// Video and audio file system.
	#define  ADD_INI_FS_256MB				0x00A00000	// 0x00A00000 to 0x01F00000 = 24MByte for filesystem.
	#define  ADD_INI_FS_128MB				0x00500000	// 0x00500000 to 0x00F00000 = 11MByte for filesystem.
	#define  ADD_INI_FS_64MB				0x00380000	// 0x00380000 to 0x00700000 = 11MByte for filesystem.
	#define  ADD_INI_FS_32MB				0x00200000	// 0x00200000 to 0x00380000 = 11MByte for filesystem.
	#define  ADD_INI_FS_16MB				0x00100000	// 0x00100000 to 0x00180000 = 11MByte for filesystem.
		#define ADD_INI_BEACON_WHITELIST		0x00100000		//+B02+  0x00100000..0x00103FFF	 16KByte for beacon whiteList save
	#define  ADD_INI_FS_8MB					0x000A0000	// 0x000A0000 to 0x000C0000 = 11MByte for filesystem.

	// CORTEX and BLE Firmware addresses
	#define ADD_INI_FIRMWARE_256MB			0x01F00000		// it has 512K each firmware
		#define ADD_INI_FIRMWARE_BLE_256MB		0x01F80000
	#define ADD_INI_FIRMWARE_128MB			0x00F00000		// it has 512K each firmware
		#define ADD_INI_FIRMWARE_BLE_128MB		0x00F80000
	#define ADD_INI_FIRMWARE_64MB			0x00700000		// it has 512K each firmware
		#define ADD_INI_FIRMWARE_BLE_64MB		0x00780000
	#define ADD_INI_FIRMWARE_32MB			0x00380000		// it has 256K each firmware
		#define ADD_INI_FIRMWARE_BLE_32MB		0x003C0000
	#define ADD_INI_FIRMWARE_16MB			0x00180000		// it has 256K each firmware
		#define ADD_INI_FIRMWARE_BLE_16MB		0x001C0000
	#define ADD_INI_FIRMWARE_8MB			0x000C0000		// it has 128K each firmware
		#define ADD_INI_FIRMWARE_BLE_8MB		0x000E0000

	#define LEN_APP_FIRMWARE				0x0003F000		// boot[3F000..20000] does not updates itself!

#define isSecPanel(PanelId)  (PanelId=="Cirrus4T" || PanelId=="Cirrus4A6"|| PanelId=="Pileus4T")

typedef union _TNuvaHard
{
    BYTE v[13];
    struct
    {
        BYTE TPanel;
        BYTE VERH;
        BYTE VERL;
        BYTE DD;
        BYTE MM;
        BYTE YY;
        BYTE BOARDV;
        BYTE FLASHID;
        BYTE bVERH;      // from 01/05/20, boot const.
        BYTE bVERL;
        BYTE bDD;
        BYTE bMM;
        BYTE bYY;
    } Field;

}TNuvaHard;

class CDevice
{
    private:
        TNuvaHard device;
        TDevInf DevInf;
    public:
        bool    Construct(TNuvaHard hard,int len);
        bool    Construct(BYTE* fr,int len);
        string  GetDevRev();
        string  GetPanelDescr(BYTE TPanel);
        string  GetDevInfo();
        bool    CheckFirmwareFile(string filename,string &error);
        TDevInf GetDevInf();
        string  GetId();
        string  GetVersion();
        string  GetVersionFW();
        int     GetIntVersionFW();
        int     GetIntDateFW();

        string  GetBootVersion();
        string  GetBootVersionFW();
        int     GetIntBootVersionFW();
        int     GetIntBootDateFW();
        string  GetFwDate();

        int     GetFlashID();
        int     GetEventQueueSize();
        int     GetEventQueueAddress();
        int     GetUpdFwAddress();
};

    // ---------- Nuvathings products ---------------------
    #define TPANEL_STR4TA           0x30
    #define TPANEL_STR4TA4A6        0x31
    #define TPANEL_CIRRUS4T         0x40
    #define TPANEL_CIRRUS4A6        0x41
    #define TPANEL_PILEUS4T         0x48
    // ----------- Emotrack/Reistrack/Nuvatrack
    #define TPANEL_NUVATRACK        0x50
    #define TPANEL_NUVATRACK4A6     0x51

    // ------------ liftel products ---------------------
    #define TPANEL_LTIO             0x60
    #define TPANEL_LTM400           0x70

    // -------- battery types ---------------------------
    #define TBATT_NONE              0
    #define TBATT_LEAD_12V          1
    #define TBATT_LIPO_7V4          2
    #define TBATT_LIFEPO4_6V6       3
    #define TBATT_LISOCL2           4
    #define TBATT_ALKALINE          5

    // --------------- voltage conversions --------
	#define POWER_CONVERSION		5.54
	#define VCC						12
	#define VDD						3.3
	#define LSB_12B					(VDD/4096)
	#define VOLTAGEINPUT_CONV(x)	(WORD)((x/POWER_CONVERSION)/LSB_12B)
#endif
