#include "Data.h"
#include "devices.h"

CData::CData()
{
    Clear();
}

CData::~CData()
{
    Clear();
}

void CData::Clear()
{
    name=" ";
    type="RES";
    cusname=" ";
    address=" ";
    phone=" ";
    email=" ";
    phonePstn=" ";
    phoneCsd=" ";
    conType=0;
    host="";
    port=0;
    portCloud =0;
    hostCloud="";
    extPower="13.8";
    tBatt=0;
    simSolution="";

}

CData CData::operator= (CData param)
{
    name=param.getname();
    type=param.gettype();
    cusname=param.getcusname();
    address=param.getaddress();
    phone=param.getphone();
    email=param.getemail();
    phonePstn=param.getphonePstn();
    conType=param.getconType();
    phoneCsd=param.getphoneCsd();
    extPower=param.getextPower();
    tBatt=param.tBatt;
    host=param.host;
    port=param.port;
    simSolution=param.simSolution;


    return *this;
}

bool CData::setname(const string& instal)
{
    name = instal;
    return true;
}

bool CData::settype(const string& tipus)
{

    type = tipus;
    return true;
}

bool CData::setcusname(const string& client)
{
    cusname=client;
    return true;
}

bool CData::setaddress(const string& dir)
{
    address = dir;
    return true;
}

bool CData::setphone(const string& telf)
{
    phone = telf;
    return true;
}

bool CData::setemail(const string& email)
{
    this->email = email;
    return true;
}

bool CData::setrule(const string& rul)
{
    this->rule = rul;
    return true;
}

bool CData::setphonePstn(const string& num)
{
    phonePstn = num;
    return true;
}

bool CData::setphoneCsd(const string& num)
{
    phoneCsd = num;
    return true;
}

bool CData::setconType(int tipus)
{
    conType = tipus;
    return true;
}

bool CData::sethost(const string& ip)
{
    this->host = ip;
    return true;
}

bool CData::setport(int port)
{
    this->port = port;
    return true;
}

bool CData::setextPower(string ext)
{
    extPower=ext;
    return true;
}

bool CData::settBatt(int tb)
{
    tBatt=tb;
    return true;
}

bool CData::setSimSolution(string ext)
{
    simSolution=ext;
    return true;
}


string& CData::getextPower()
{
    return extPower;
}

int CData::gettBatt()
{
    return tBatt;
}

string CData::sgettBatt()
{
    string sbatt="Lead 13.8V";
    switch (tBatt){
        case TBATT_NONE:
            sbatt="-";
            break;
        case TBATT_LEAD_12V:
            break;
        case TBATT_LIFEPO4_6V6:
            sbatt="LiFePO4 6.6V";
            break;
        case TBATT_LIPO_7V4:
            sbatt="LiFePO4 7.4V";
            break;
        case TBATT_LISOCL2:
            sbatt="LiSOCl2";
            break;
        case TBATT_ALKALINE:
            sbatt="Alkaline";
            break;

    }
    return sbatt;
}

string& CData::getname()
{
    return name;
}

string& CData::gettype()
{
    return type;
}


string& CData::getcusname()
{
    return cusname;
}

string& CData::getaddress()
{
    return address;
}

string& CData::getphone()
{
    return phone;
}

string& CData::getemail()
{
    return email;
}

string& CData::getrule()
{
    return rule;
}

string& CData::getphonePstn()
{
    return phonePstn;
}

string& CData::getphoneCsd()
{
    return phoneCsd;
}


int CData::getconType()
{
    return conType;
}

int CData::getport()
{
    return port;
}

string& CData::gethost()
{
    return host;
}

int CData::getportCloud()
{
    return portCloud;
}

string& CData::gethostCloud()
{
    return hostCloud;
}

string& CData::getSimSolution()
{
    return simSolution;
}
