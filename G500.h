#ifndef G500_H
#define G500_H

#include <strings.h>

using namespace std;

#include <wx/socket.h>

#define _wx(x) (wxString)((string)x).c_str()

class G500
{
    public:
        G500();
        virtual ~G500();
        bool G500LabelPrint(int copies,wxString host,int port,wxString Model,wxString SN,wxString FW,wxString website,wxString logo,wxString power,wxString battery,wxString rules);
        //G500LabelPrint(int copies,wxString host,int port,wxString Model,wxString SN,wxString FW,wxString battery,wxString input,wxString rules);
    protected:

    private:
    wxSocketBase   *psocket;
    wxSocketClient *socketc;
    wxSocketServer *sockets;
    wxIPV4address IPADDR;
    bool IsSocketServer;

};

#endif // G500_H
