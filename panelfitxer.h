#ifndef PANELFITXER_H
#define PANELFITXER_H

#include <iostream>
#include <string>
using namespace std;

#include <wx/panel.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include <wx/bmpbuttn.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>

#include "fitxerflash.h"
#include "util.h"
#include "msgs.h"

#define _wx(x) (wxString)((string)x).c_str()

class CPanelFile: public wxPanel
{
    friend class CPanelEditEvt;
    protected:

        CFitxerFlash *fitxer;
        wxTextCtrl *text;
        wxStaticText *label;
        wxButton *botoExaminar;
        wxBoxSizer *hSizer, *vSizer;
        wxCheckBox *check;

    public:
        CPanelFile(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage* lang=0);
        void Clear();
        ~CPanelFile();
        CLanguage*    Lang;
        bool SetFitxer(CFitxerFlash *fitxer);
        CFitxerFlash *GetFile();
        void UnSetFitxer();
        bool ApplyValues();
        bool UpdateValues();

        void OnButtExaminar(wxCommandEvent& event);
        DECLARE_EVENT_TABLE();
    protected:
        void GenComponents();
};
// end CPanelFile

#endif
