#ifndef PANELVRUSER_H
#define PANELVRUSER_H

#include <iostream>
using namespace std;

class Cmlink;
#include "panelcfgvruser.h"
#include <wx/panel.h>
#include <wx/grid.h>
#include <wx/string.h>
#include <wx/stattext.h>
#include <wx/gauge.h>
#include <wx/textctrl.h>
#include <wx/bmpbuttn.h>
#include <wx/combobox.h>
#include <wx/statbox.h>
#include <wx/sizer.h>
#include <wx/checkbox.h>

#include "estat.h"
#include "msgs.h"
#include "grup.h"
#include "codi.h"




class CPanelVRUser: public wxPanel
{
    friend class CPanelEditEvt;
    protected:
        wxPoint m_tmppoint;
        wxSize  m_tmpsize;
        wxPoint& VwXSetwxPoint(long x,long y);
        wxSize& VwXSetwxSize(long w,long h);

        wxGrid *grid;
        wxBoxSizer *vSizer;
        CStatus *estat;
        CCodi *codi;

/*		wxStaticText *minuts;
		wxStaticText *Dbm;
		wxStaticText *RfLevel;
		wxStaticText *Test;
		wxTextCtrl *TestEdit;
		wxGauge *RfGauge;

		wxBitmapButton *BotoDelete;
		wxBitmapButton *BotoRead;
		wxBitmapButton *BotoAdd;
		wxStaticText *WxStaticText1;
		wxTextCtrl *ZoneCodeEdit;
		wxStaticText *ZoneNum;
		wxTextCtrl *ZoneEdit;
		wxStaticText *Zona;
		wxStaticBox *ConfigVR;*/
		CPanelCfgVRUser *WxPanel1;

		enum
		{
			////GUI Enum Control ID Start
/*			ID_BOTODELETE = 1018,
			ID_BOTOREAD = 1017,
			ID_BOTOADD = 1016,
			ID_WXSTATICTEXT1 = 1015,
			ID_ZONECODEEDIT = 1013,
			ID_ZONENUM = 1012,
			ID_ZONEEDIT = 1010,
			ID_ZONAS = 1009,
			ID_CONFIGVR = 1008,*/
			ID_WXPANEL1 = 1032,
			////GUI Enum Control ID End
			ID_DUMMY_VALUE_ //don't remove this value unless you have other enum values
		};

    public:

        Cmlink *mlink;
        string PanelVersion;
        string PanelId;
        CLanguage*    Language;
        bool SetGrups(CGroup *grupUAlias);
        string CUalias[24];

        CPanelVRUser(wxWindow* parent, wxWindowID id = -1, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxHSCROLL | wxVSCROLL, const wxString& name = wxT("scrolledWindow"),CLanguage *lang=0,Cmlink* el=0,CStatus *est=0,string pver="",string pid="");
        void Clear();
        ~CPanelVRUser();

        bool SetEstat(CStatus *estat);
        CStatus *GetEstat();
        void UnSetEstat();
        bool AplicaValors();
        bool ActualitzaValors();
/*        void BotoAddClick(wxCommandEvent& event);
        void BotoDeleteClick(wxCommandEvent& event);
        void BotoReadClick(wxCommandEvent& event);*/

    protected:
        void GeneraComponents();

//	private:
//		DECLARE_EVENT_TABLE();
};

#endif
