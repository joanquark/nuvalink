#include "Status.h"

#define _wx(x) (wxString)((string)x).c_str()


CStatus::CStatus(unsigned char TPanel)
{
    Clear();
    this->busadd=0x12;
    isPSTN=false;
    isETH=false;

    switch (TPanel){
        case TPANEL_CIRRUS4T:
        case TPANEL_STR4TA:
        case TPANEL_STR4TA4A6:
        case TPANEL_LTIO:
        case TPANEL_NUVATRACK:
        case TPANEL_NUVATRACK4A6:
            this->numAreas=8;
            this->numZones=64;
            this->numOuts=8;
            break;
        case TPANEL_CIRRUS4A6:
            this->numAreas=8;
            this->numZones=64;
            this->numOuts=8;
            break;
        case TPANEL_PILEUS4T:
            this->numAreas=32;
            this->numZones=256;
            this->numOuts=32;
            break;

        default:
            this->numAreas=1;
            this->numZones=2;
            this->numOuts=2;
    }
}

void CStatus::Clear()
{

    punterEvents=0;

    vdd=3.3;
    numZones=16;
    numAreas=4;

    punterEvents=0;
    dateTime.hours=0;
    dateTime.minuts=0;
    dateTime.seconds=0;
    dateTime.day=0;
    dateTime.month=0;
    dateTime.year=0;

    hasGsmRssi=false;
    hasGeneral=true;
    hasOuts=true;
    hasAnalog=true;
    hasBeacons=true;
    hasWireless=true;
    hasWirelessUser=true;
    hasWirelessOut=true;
    hasReportTest=true;

#ifdef _PICTURES
    hasPics=false;
#endif
}

CStatus::~CStatus()
{
    Clear();
}


bool CStatus::parseDevStPkt(unsigned char* data,int remaining)
{

    int cnt;
    while (remaining>0){
        unsigned char blockType = *data++;
        unsigned char blocklen  = *data++;

        remaining--;

        if (blocklen>1 && blocklen<=remaining && remaining>=1){
            remaining--;
            blocklen--;							// as long as blocklen counts len itself, and we parsed ID+LEN
        }else{
            return false;				// error, stop parsing.
        }

        blockType	 &= 0x00FF;

        if (blockType==(EVENT_SYS_STATUS0 & 0xFF)){
            /*  BYTE 	0xF0		// 0
                BYTE	0x0F		// 1 STATUS0 EVENT ID.
                BYTE	TDevice; 	// 2 TDevice!
                BYTE	FWVH;		// 3 Fw version high
                BYTE 	FWVL;		// 4 Fw Version low
                BYTE	BOARDV; 	// 5 Board ver
                BYTE	FlashID; 	// 6 Flash ID
                BYTE 	DevInf;		// 7 Gsm Dev info
                */
            STSYS.SysSt.TDevice=*data++;
            STSYS.SysSt.FwVerH=*data++;
            STSYS.SysSt.FwVerL=*data++;
            STSYS.SysSt.BoardVer=*data++;
            STSYS.SysSt.FlashID=*data++;
            STSYS.GsmSt.DevInf=*data++;
            //memcpy(&STSYS.GsmSt.CIMI.v[0],data,8);        // CIMI DELETED IN DEVST packet.
            //data+=8;
            remaining-=6;
        }else if (blockType==(EVENT_SYS_STATUS1 & 0x00FF)){
/* 	 BYTE 	0xF1			// 0
	 BYTE	0x0F			// 1 STATUS1 EVENT ID.
	 BYTE	TDevice; 		// 2 TDevice!
	 BYTE	LevelBatt;		// 3 batt level (8bit)
	 BYTE 	LevelExtP;		// 4 Ext power level (8bit)
	 BYTE 	PowerFail;		// 5 Indications
	 BYTE 	ComFail;		// 6 Indications
	 BYTE 	MiscInd;		// 7 Indications
	 BYTE 	StIP;			// 8 IP Status
	 BYTE	AnOutSt			// 9
	 BYTE 	AnOutPwr		// A
	 BYTE 	RFLRState;		// B Lora RF status
	 BYTE 	RFRegVersion;	// C Lora RF status
	 BYTE 	RFPktRssi;		// D Lora RF status
	 BYTE 	GsmSt;			// E Gsm Net State
	 BYTE 	gsmrssi;		// F Gsm rssi */
            STSYS.SysSt.TDevice=*data++;
            data+=2;
            STSYS.SysInd.PowerFail=*data++;
            STSYS.SysInd.ComFail=*data++;
            STSYS.SysInd.ExtraSign=*data++;
            STSYS.IPSt.StIP=*data++;
            data+=2;
            data+=3;
            STSYS.GsmSt.NetSt=*data++;
            STSYS.GsmSt.rssi=*data++;
            remaining-=14;
        }else if (blockType==(EVENT_AREA_STATUS & 0x00FF)){
            cnt=0;
            while (blocklen>0){
                ASt[cnt++].Status=*data++;
                blocklen--;
                remaining--;
            }
        }else if (blockType==(EVENT_IN_STATUSR & 0x00FF)){
            cnt=0;
            while (blocklen>0){
                InSt[cnt++].Status=(WORD)*data++;
                blocklen--;
                remaining--;
            }
        }else if (blockType==(EVENT_OUT_STATUSR & 0x00FF)){
            cnt=0;
            while (blocklen>0){
                OSt[cnt++].IOStatus=*data++;
                blocklen--;
                remaining--;
            }
        }
    }
    return true;
}

bool CStatus::UpdateFromRawMem(unsigned char* data,int id)
{
    string add=GetStEeprom(id);
    int addint=hToI(add);
    int len=GetStMemLen(id);
    switch(id){
        case ID_ESTAT_INPUT:
            memcpy(&InSt[0].Status,&data[addint],len);
            break;
        case ID_ESTAT_OUTPUT:
            memcpy(&OSt[0].IOStatus,&data[addint],len);
            break;
        case ID_ESTAT_AREAS:
            memcpy(&ASt[0].Status,&data[addint],len);
            break;
        case ID_ESTAT_SYS:
            memcpy(&STSYS.SysInd.PowerFail,&data[addint],len);
            break;

    }
}


WORD CStatus::GetAreaSt(int area)
{
    if (area<numAreas)
        return ASt[area].Status;
    else
        return 0;
}

WORD CStatus::GetOutSt(int out)
{
    if (out<numSortides)
        return (OSt[out].IOStatus&M_STOUT_ON);
    else
        return 0;
}

unsigned char CStatus::GetStPanel()
{
    return STSYS.SysSt.StPanel.Val;
}



bool CStatus::SetNumZ(int numZones)
{
    this->numZones = numZones;
    return true;
}

bool CStatus::SetNumUsers(int numUsers)
{
    this->numUsers = numUsers;
    return true;
}

int CStatus::GetNumZones()
{
    return numZones;
}

int CStatus::GetNumUsers()
{
    return numUsers;
}

bool CStatus::SetStatus()
{
    return true;
}

bool CStatus::SetStEeprom(int sec,string &val)
{
    this->StEeprom[sec]=val;
}

bool CStatus::SetStMemLen(int sec,int len)
{
    this->StMemLen[sec]=len;
}

string CStatus::GetStEeprom(int sec)
{
    return this->StEeprom[sec];
}

int CStatus::GetStMemLen(int sec)
{
    return this->StMemLen[sec];
}


bool CStatus::SetDev(string curmode,string curver,string curverdate,string curhard){
    this->curmodel=curmode;
    this->curver=curver;
    this->curverdate=curverdate;
    this->curhard=curhard;
}

bool CStatus::IsDetecting(int zona)
{
    if ((zona < 0) || (zona >= numZones))
        return false;

    if (InSt[zona].Status&M_IN_DETECT)
        return true;

    return false;

}

bool CStatus::IsAlarm(int zona)
{
    if ((zona < 0) || (zona >= numZones))
        return false;

    if (InSt[zona].Status&M_IN_ALARM)
        return true;

    return false;

}

bool CStatus::IsOmiting(int zona)
{
    if ((zona < 0) || (zona >= numZones)){
        return false;
    }

    if (InSt[zona].Status&M_IN_BYPASS)
        return true;
    return false;

}

bool CStatus::IsAvery(int zona)
{
    if ((zona < 0) || (zona >= numZones))
        return false;

    if (InSt[zona].Status&M_IN_FAILURE)
        return true;

    return false;

}

bool CStatus::SetDateTime(unsigned char *data)
{
    STSYS.SysSt.dt.hours = data[3];
    STSYS.SysSt.dt.minuts = data[4];
    STSYS.SysSt.dt.seconds = data[5];
    STSYS.SysSt.dt.day = data[6];
    STSYS.SysSt.dt.month = data[7];
    STSYS.SysSt.dt.year = data[8];
    return true;
}

unsigned char *CStatus::GetDateTime()
{
    return &dateTime.hours;
}

string &CStatus::GetTime()
{
    std::string *time = new std::string;
    int hores = (int)STSYS.SysSt.dt.hours;
    int minuts = (int)STSYS.SysSt.dt.minuts;
    int segons = (int)STSYS.SysSt.dt.seconds;
    char buffer[16];
    sprintf(buffer, "%02d:%02d:%02d", hores, minuts, segons);
    *time = buffer;
    return *time;
}

string &CStatus::GetDate()
{
    std::string *time = new std::string;
    int dia = (int)STSYS.SysSt.dt.day;
    int mes = (int)STSYS.SysSt.dt.month;
    int any = (int)STSYS.SysSt.dt.year;
    char buffer[16];
    sprintf(buffer, "%02d/%02d/%02d", dia, mes, any);
    *time = buffer;
    return *time;
}

bool CStatus::SetVDD(float vdd)
{
    if (vdd < 0.0) return false;

    this->vdd = vdd;
    return true;
}

int CStatus::GetNumSortides()
{
    return numSortides;
}

bool CStatus:: SetNumOuts(int num)
{
    numSortides=num;
}


bool CStatus::SetPunterEvents(int punter)
{
    punterEvents=punter;
    return true;
}

int CStatus::GetPunterEvents()
{
    return punterEvents;
}

bool CStatus::SetAlarmZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status|=M_IN_ALARM;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetDetectZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status|=M_IN_DETECT;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetAveriaZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status|=M_IN_FAILURE;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetOmitZ(int zona)
{
    if ((zona <0) || (zona>numZones))
        return false;

    InSt[zona].Status|=M_IN_BYPASS;

#ifdef ESTAT_GRAFIC
    ActualitzaCompGrafics();
#endif

    return true;
}

bool CStatus::SetAreaSt(WORD AreaState)
{
    STSYS.SysSt.StAreas.Val=AreaState;
    return true;
}

bool CStatus::SetStPanel(unsigned char StPanel)
{
    STSYS.SysSt.StPanel.Val=StPanel;
    return true;
}

bool CStatus::SetNumAreas(int arees)
{
    if (arees >= 0) {
        numAreas = arees;
        return true;
    }
    return false;
}

int CStatus::GetNumAreas()
{
    return numAreas;
}

bool CStatus::SetHasGeneral(bool general)
{
    hasGeneral = general;
    return true;
}

bool CStatus::HasGeneral()
{
    return hasGeneral;
}

bool CStatus::SetHasWireless(bool Radio)
{
    hasWireless = Radio;
    return true;
}

bool CStatus::HasBeacons()
{
    return hasBeacons;
}

bool CStatus::SetHasBeacons(bool bec)
{
    hasBeacons=bec;
    return true;
}

bool CStatus::HasWireless()
{
    return hasWireless;
}

bool CStatus::SetHasWirelessUser(bool Radio)
{
    hasWirelessUser = Radio;
    return true;
}

bool CStatus::HasWirelessUser()
{
    return hasWirelessUser;
}


bool CStatus::SetHasWirelessOut(bool Radio)
{
    hasWirelessOut = Radio;
    return true;
}

bool CStatus::HasWirelessOut()
{
    return hasWirelessOut;
}


bool CStatus::SetHasReportTest( bool test)
{
    hasReportTest = test;
    return true;
}

bool CStatus::HasReportTest()
{
    return hasReportTest;
}

#ifdef _PICTURES
bool CStatus::HasPictures(){
    return hasPics;
}

bool CStatus::SetHasPictures(bool pictures)
{
    hasPics=pictures;
    return true;
}
#endif

