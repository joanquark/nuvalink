#include "grup.h"
#define _wx(x) (wxString)((string)x).c_str()

CGroup::CGroup()
{
    descr="";
    grupPare=0;
    codis.clear();
    grups.clear();
    isBitGroup=false;
    isFollowCodis=false;
    dontRx=false;
    dontSend=false;
    distancia=0;
    for (int i=0; i<8; i++) {
        bitdescr.push_back(new string(""));
    }
    upl=0;
    cat="ON";
}

CGroup::CGroup(CGroup *grupPare, string& descr)
{
    upl=0;
    codis.clear();
    grups.clear();
    SetFather(grupPare);

    wxString wxdes=_wx(descr);

    SetDescr(wxdes);
    isBitGroup=false;
    for (int i=0; i<8; i++) {
        bitdescr.push_back(new string(""));
    }
    dontRx=false;
    dontSend=false;
    this->SetUpl(grupPare->GetUpl());
    this->SetCat(grupPare->GetCat());
}

void CGroup::Clear()
{
    for (int i=0; i<bitdescr.size(); i++) {
		*bitdescr[i]="";
        delete bitdescr[i];
    }
    bitdescr.clear();
    vector<string*> tmp;
    tmp.swap(bitdescr);

    grups.clear();
    vector<CGroup*> tmp1;
    tmp1.swap(grups);

    codis.clear();
    vector<CCodi*> tmp2;
    tmp2.swap(codis);

    descr="";
    grupPare=0;
    isBitGroup=false;
    isFollowCodis=false;
    dontRx=false;
}

CGroup::~CGroup()
{
    Clear();

}

bool CGroup::SetDescr(wxString &descr)
{
    this->descr = descr;
}

bool CGroup::SetFather(CGroup *grupPare)
{
    this->grupPare = grupPare;
}

bool CGroup::SetBitGroup()
{
    isBitGroup=true;
    return true;
}

bool CGroup::SetBitDescr(int bit, wxString& descr)
{
    if ((bit<0) || (bit>=8))
        return false;

    *bitdescr[bit] = descr;
    return true;
}

int CGroup::AddChild(CCodi *codiFill)
{
    codis.push_back(codiFill);
    return (codis.size()-1);
}

int CGroup::AddChild(CGroup *grupFill)
{
    //Ens assegurem que som el pare
    grupFill->SetFather(this);

    grups.push_back(grupFill);
    return (grups.size()-1);
}

int CGroup::GetChildNum()
{
    return (grups.size()+codis.size());
}

int CGroup::GetNumChildCodi()
{
    return codis.size();
}

int CGroup::GetNumChildGroup()
{
    return grups.size();
}

CGroup* CGroup::GetChildGroup(int index)
{
    return grups[index];
}

CCodi* CGroup::GetChildCodi(int index)
{
    return codis[index];
}

CGroup* CGroup::GetFather()
{
    return grupPare;
}

string& CGroup::GetDescr()
{
    return descr;
}

string& CGroup::GetBitDescr(int bit)
{
    if ((bit<0) || (bit>=8))
        return *bitdescr[0];

    return *bitdescr[bit];
}


bool CGroup::IsBitGroup()
{
    return isBitGroup;
}

bool CGroup::SetId(string& id)
{
    this->id = id;
    return true;
}

string& CGroup::GetId()
{
    return id;
}

bool CGroup::SetUpl(int nupl)
{
    this->upl = nupl;
}

int CGroup::GetUpl(void)
{
    return this->upl;
}

string& CGroup::GetCat()
{
    return this->cat;
}

bool CGroup::SetCat(string& catval)
{
    this->cat=catval;
    return true;
}

bool CGroup::SetFollowCodis()
{
    isFollowCodis = true;
    return true;
}

bool CGroup::FollowCodis()
{
    return isFollowCodis;
}

bool CGroup::SetDistance(int d)
{
    if (d>0)
        distancia = d;
    else
        distancia = 0;

    return true;
}

bool CGroup::SetNotRx()
{
    dontRx = true;
    return true;
}

bool CGroup::SetNotSend()
{
    dontSend = true;
    return true;
}

bool CGroup::DontRx()
{
    return dontRx;
}

bool CGroup::DontSend()
{
    return dontSend;
}

int CGroup::GetDistancia()
{
    return distancia;
}

bool CGroup::Reset()
{
    for (int i=0; i<codis.size(); i++) {
        CCodi *codi = codis[i];
        if (!codi->Reset())
            return false;
    }

    for (int i=0; i<grups.size(); i++) {
        CGroup *grup = grups[i];
        if (!grup->Reset())
            return false;
    }

    return true;
}

bool CGroup::GetChanges()
{
    for (int i=0; i<codis.size(); i++) {
        if (codis[i]->GetChanges())
            return true;
    }

    for(int i=0; i<grups.size(); i++) {
        if (grups[i]->GetChanges())
            return true;
    }

    return false;
}

void CGroup::ClrChanges()
{
    for (int i=0; i<codis.size(); i++) {
        codis[i]->ClrChanges();
    }

    for(int i=0; i<grups.size(); i++) {
        grups[i]->ClrChanges();
    }
}
