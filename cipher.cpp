// *************************** PGM EMILI i JOAN ********************************
// ***************** Ciphering library for IP Reporting and Wireless comm *****
// ****************************************************************************
/* ------- 10/05/06 - Created for PC, author Emili Sapena -------------------- */
/* ------- 15/05/06 - Optimized for microcontrollers, Joan Vidal ------------- */
#include "cipher.h"
#include "util.h"

   #define  LONG_BITS_KEY           16
   #define  MASK_ACCESS_KEYVECTOR   0x03
   #define  LONG_BYTES_KEY          (LONG_BITS_KEY/8)

   #define  INIT_VECTOR             0x34

// Transposition of bits.
    #define b0   0x01
    #define b1   0x02
    #define b2   0x04
    #define b3   0x08
    #define b4   0x10
    #define b5   0x20
    #define b6   0x40
    #define b7   0x80

// ----------------------------------------------- local prototypes --------------
unsigned char InitVect;
int cnt;
unsigned char res;
unsigned char key;

void     CipherChain( unsigned char num);
void     UnCipherChain( unsigned char num);

unsigned char S( unsigned char row, unsigned char col);


// ------------------------------------------------------ public prototypes ----
// S Box, used for the cipher block, accessed with nibbles.
const unsigned char S_Box[16][16] = {
    0x01, 0x00, 0x03, 0x02, 0x08, 0x05, 0x04, 0x0a, 0x07, 0x06, 0x09, 0x0c, 0x0b, 0x0f, 0x0e, 0x0d,
    0x07, 0x0d, 0x09, 0x0a, 0x0b, 0x0c, 0x08, 0x0e, 0x0f, 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06,
    0x08, 0x09, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x00, 0x01, 0x05, 0x02, 0x0a, 0x04, 0x03, 0x06, 0x07,
    0x04, 0x00, 0x01, 0x03, 0x05, 0x06, 0x07, 0x02, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0f, 0x0d, 0x0e,

    0x05, 0x0a, 0x09, 0x07, 0x03, 0x08, 0x0e, 0x02, 0x0c, 0x01, 0x0f, 0x06, 0x09, 0x0b, 0x04, 0x0d,
    0x0f, 0x07, 0x05, 0x02, 0x03, 0x04, 0x01, 0x06, 0x00, 0x08, 0x09, 0x0a, 0x0b, 0x0e, 0x0d, 0x0c,
    0x0e, 0x0f, 0x0d, 0x0c, 0x0a, 0x0b, 0x09, 0x08, 0x07, 0x06, 0x04, 0x05, 0x03, 0x02, 0x00, 0x01,
    0x02, 0x03, 0x04, 0x01, 0x07, 0x0f, 0x06, 0x0e, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x00, 0x05,

    0x09, 0x07, 0x04, 0x00, 0x01, 0x0d, 0x0e, 0x0f, 0x02, 0x03, 0x05, 0x06, 0x08, 0x0b, 0x0c, 0x0a,
    0x09, 0x08, 0x01, 0x04, 0x0d, 0x05, 0x0f, 0x07, 0x0b, 0x02, 0x0a, 0x03, 0x0c, 0x06, 0x0e, 0x00,
    0x03, 0x0b, 0x07, 0x09, 0x00, 0x0f, 0x02, 0x04, 0x0a, 0x0c, 0x06, 0x0e, 0x0d, 0x01, 0x08, 0x05,
    0x02, 0x03, 0x00, 0x01, 0x08, 0x07, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x04, 0x05, 0x06,

    0x06, 0x0e, 0x0f, 0x0d, 0x07, 0x04, 0x00, 0x01, 0x05, 0x03, 0x02, 0x08, 0x09, 0x0a, 0x0b, 0x0c,
    0x0f, 0x0a, 0x0c, 0x0b, 0x0d, 0x00, 0x01, 0x03, 0x02, 0x04, 0x0e, 0x05, 0x06, 0x07, 0x08, 0x09,
    0x00, 0x0b, 0x02, 0x01, 0x04, 0x03, 0x05, 0x06, 0x0e, 0x0f, 0x07, 0x08, 0x09, 0x0a, 0x0c, 0x0d,
    0x0a, 0x01, 0x0e, 0x0f, 0x09, 0x02, 0x0b, 0x0c, 0x03, 0x05, 0x08, 0x09, 0x07, 0x00, 0x06, 0x04,
};

// ------------------------------------------------ unsorting bits of the data ------
unsigned char T( unsigned char arg, unsigned char txt)
{
   unsigned char tr=0;

   if (arg){
      // T1 transformation
      // ------------------------- b0,b1,b2,b3,b4,b5,b6,b7
      //const unsigned char T1[8]={b1,b2,b3,b5,b6,b4,b7,b0};

      txt^=0x07;
      if (txt&b0){
         tr|=b1;
      }
      if (txt&b1){
         tr|=b2;
      }
      if (txt&b2){
         tr|=b3;
      }
      if (txt&b3){
         tr|=b5;
      }
      if (txt&b4){
         tr|=b6;
      }
      if (txt&b5){
         tr|=b4;
      }
      if (txt&b6){
         tr|=b7;
      }
      if (txt&b7){
         tr|=b0;
      }
   }else{
      // T2 transformation
      // ------------------------- b0,b1,b2,b3,b4,b5,b6,b7
      //const unsigned char T2[8]={b1,b0,b4,b5,b6,b7,b2,b3};
      txt^=0xc0;
      if (txt&b0){
         tr|=b1;
      }
      if (txt&b1){
         tr|=b0;
      }
      if (txt&b2){
         tr|=b4;
      }
      if (txt&b3){
         tr|=b5;
      }
      if (txt&b4){
         tr|=b6;
      }
      if (txt&b5){
         tr|=b7;
      }
      if (txt&b6){
         tr|=b2;
      }
      if (txt&b7){
         tr|=b3;
      }
   }
   return tr;
}



// ------------------------------------------ SBox ------------------------
// -------------- accesses in dibits to the S_box -------------------------
unsigned char S( unsigned char row, unsigned char col){

   return  S_Box[col&0x0F][row&0x0F];     // Col / row inverted, it is not a bug
}

// -------------------------------------------------------------  Codifying ----------
// PRe : current key charged in key variable




unsigned char C(unsigned char txt){

    /*unsigned char res=0x00, c;

    c = S( (txt&0xF0)>>4, (key&0xF0)>>4 );
    res |= (c&0x0F)<<4;

    c = S( (txt&0x0F), (key&0x0F) );
    res |= (c&0x0F);

    return res;*/

   unsigned char tres=0;

   tres |= S( txt,key );

   tres |= SwapA( S(SwapA(txt),SwapA(key)) );

   return tres;
}

// ------------------------------------------------------------------
// PRe : current key charged in key variable
unsigned char Cipherbyte(unsigned char text){

   res=T((key&0x01),text);

   if (!(key&0x02))  res ^= 0xFF;

   res=T((key&0x04),res);

   res = C(res);

//   res=T((key&0x08),res);

   //res=T((key&0x10),res);      // We don't use in order to put Asimetry

   res=T((key&0x20),res);

//   res = C(res);

//   res=T((key&0x40),res);

   res=T((key&0x80),res);

   return res;
}

// ------------------------------ Ciphering  a chain, and leaves codified data in the same position -------
// PRE : chain accessed with PChar;
void CipherChain(unsigned char* PChar, unsigned char* KeyVector,unsigned int num){

   unsigned char acc;

   InitVect=INIT_VECTOR;

   for (cnt=0;cnt<num;cnt++){
      key=KeyVector[cnt&MASK_ACCESS_KEYVECTOR];
      acc=Cipherbyte(InitVect);
      acc=(*PChar) ^ acc;
      *PChar++=acc;
      InitVect=acc;
   }
}

// ----------------------------------------------------- Unciphering procedures.
void UnCipherChain(unsigned char* PChar,unsigned char* KeyVector,unsigned int num){

   unsigned char acc;

   InitVect=INIT_VECTOR;

   for (cnt=0;cnt<num;cnt++){
      key=KeyVector[cnt&MASK_ACCESS_KEYVECTOR];
      acc=Cipherbyte(InitVect);
      InitVect=*PChar;
      acc=InitVect ^ acc;
      *PChar++=acc;
   }
}

