#ifndef PROTOCOLNIOT_H
#define PROTOCOLNIOT_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#include "util.h"
#include "Connection.h"
#include "logger.h"
//#include "eep.h"

#include "trames.h"
#include "msgs.h"
#include "myapp.h"
#include <iostream>
#include <fstream>
#include <wx/timer.h>
#include <wx/socket.h>
#include "crypto/AES_iv.h"
#include "NIOT.h"
#include "PNIOTNotify.h"
//#include "IID.h"
//#define _COMM_DEBUG

// --------------------------------------- Protocol timeout--
#define RESOLUTION_PROCESRX_MODEM             5      // necessita enxampar els bytes de sorolls.
#define RESOLUTION_PROCESRX_LOCAL             5      // Local
#define RESOLUTION_PROCESRX_DIGREMOTE         20     // CSD,IP

#define TIMER_HELLO_VALUE           20000
#define TIMER_CONEXIO_VALUE         100

#define TIMEOUT_INTERCHAR_LOCAL     70
#define TIMEOUT_INTERCHAR_REMOTE    250

#define TIMEOUT_WATCHDOG_COMM       2000

//timeouts en milisegons, to this value AvTimeOut is added
#define TIMEOUT_RESPOSTA_LOCAL      1500
#define TIMEOUT_RESPOSTA_MODEM      1500
#define TIMEOUT_RESPOSTA_CSD        2500
#define TIMEOUT_RESPOSTA_TCPIP      2500
#define MAX_TIMEOUT_RESPOSTA        4000
#define TIMEOUT_RESPOSTA_BINRX      8000

#define TIMEOUT_ACK_RS485           1500
#define TIMEOUT_ACK_MODEM           1500
#define TIMEOUT_ACK_CSD             2500
#define TIMEOUT_ACK_TCPIP           2500

#define EXTRA_TIMEOUT_RELIEFMODE    1000

// compte al final els temps en ms CTCPIP,GPRS acaben essen molt m�s grans dels estipulats
// ja que enviar o demanar dades del socket ocupa molt m�s temps

// ---------------------------------------- frame headers ---
#define MAX_BYTES_TRAMA         564
#define SYN                     0x16
#define STX                     0x02
#define ACK                     0x06
#define NAK                     0x15
#define DC1                     0x11
#define DC2                     0x12
#define DC3                     0x13
#define DC4                     0x14


#define NUM_FATAL_ERROR         9
#define _PIGGY_ACK
    // So the nuvalink accepts PIGGY_ACK libcomm working mode

#include "BinRx.h"


// ------------------------- class -----------------------------------------

class CProtocolNIOT : public wxThread
{
    public:
        virtual void *Entry();
        virtual void OnExit();
        CProtocolNIOT();
        ~CProtocolNIOT();

        int privlevel;
        int datatx;
        int datarx;
        bool waitAck;
        int cntack;
        wxString StatusBarBase;
        bool LongPBus;
        string portaEntrada;
        CLogger *myLog;
        wxTimer *timerConexio;
        wxTimer *timerHello;
        MyApp *myApp;
        wxProgressDialog *progressDlg;
        int *progress;
        int NumError;
        void SetSN(string SN);
        string GetSN();
        void Setconnecthandler(CConnection *conexio);
        CConnection *Getconnecthandler();
        bool SocketLost();
        bool GenSendCodiPacket( string &address, bool ramNoteeprom, unsigned char *buffer, int lon, int offset=0);
        bool GenSendCodiPacket( char *address, bool ramNoteeprom, unsigned char *buffer, int lon, int offset=0);
        bool GenSendTxtPacket(char *sms,int len);
        bool GenSendMemPacket(  string &address, bool ram, unsigned char *buffer, int lon, int offset=0);
        bool GenSendMemPacket(  char *address, bool ram, unsigned char *buffer, int lon, int offset=0);
        bool GenTramaDelayPolling(unsigned char time);
        void GenRepeatPacket();
        void GenSynPacket();
        bool GenRxCodiPacket(string &address, bool ramNoteeprom, int valLength, int offset=0);
        bool GenRxCodiPacket(char *address, bool ramNoteeprom, int valLength, int offset=0);
        bool GenRxGroupPacket(string &address, bool ramNoteeprom, int valLength, int offset=0);
        bool GenRxGroupPacket(char *address, bool ramNoteeprom, int valLength, int offset=0);
        bool GenRxMemPacket(string& address, bool ramNoteeprom, int valLength, int offset=0);
        bool GenRxMemPacket(char *address, bool ramNoteeprom, int valLength, int offset=0);
        bool GenRxPanelStatusPacket();
        bool GenRxDateTimePacket();
        bool GenRxGsmRssiPacket();
        int  GenNowDateTimePacket();
        bool GenRxEventsPacket(string& address, bool eeprom, int punterIni, int bytesxevent, int numEvents);
        bool GenRxEventsPacket(char* address, bool eeprom, int bytesxevent, int numEvents);
        bool GenStablishPacket(int priv, string& codiInstalador, bool callback=false);
        bool GenStablishPacket(int priv, char codiInstH, char codiInstM, char codiInstL, bool callback);
        bool GenHelloPacket();
        bool GenByePacket();
        bool GenOrderPacket( char TOrder, unsigned char* params=NULL,int paramlen=0,bool noack=false);
        bool GenPacketGetFlashSignatures(string address,int len);
        bool GenPacketGetBeaconSt();
        bool GenWaveRecordPacket(uint16_t channel, uint16_t numsample);
        bool GenEventPacket(WORD event,WORD num, unsigned char area);
        bool GenOrderPacketVR(char TOrder, unsigned char num);
        bool GenTramaApplyProg();
        bool GenTxFlashPacket(string& address, unsigned char *buffer, int lon,bool ack,bool fw530=false);
        bool GenTxFlashPacket(char *address, unsigned char *buffer, int lon,bool ack,bool fw530=false);
        bool GenFwActionPacket(unsigned int checksum,unsigned char action);
        bool GenACDCPacket(string IPADD,string PORT);
        bool GenCustomPacket(unsigned char tpkt,unsigned char * frame, int len);
        bool SendStreamPkt(unsigned char* data, int len);
        bool RxPacketStream(unsigned char* data,uint16_t* len);
        bool SendAsciiFrame(string frame,int len);
        bool GenWConfigPacket(unsigned char z,unsigned char tsens, string& code);
        bool GenWConfigPacketUser(unsigned char u,unsigned char tsens, string& code);
        bool GenWConfigPacketOut(unsigned char u,unsigned char tsens, string& code);
        bool GenRxFlashPacket(string& address, int numSectors);
        bool GenRxFlashPacket(char *address, int numSectors);
        bool GenArmAreaPacket(WORD areesMask);
        bool GenDisarmAreaPacket(WORD areesMask);
        bool GenBypassZPacket(uint16_t zone, uint16_t user);
        bool GenReportTestPacket();
        bool GenRPCPacket(unsigned char funcio, unsigned char param1=0, unsigned char param2=0, unsigned char param3=0, unsigned char param4=0);
        bool GenActOutputPacket(unsigned char sortidesMask);
        bool GenDeactOutputPacket(unsigned char sortidesMask);
        bool NoNeedAck();
        bool SendPacket(int tries=3,bool longtimeout=false);
		bool SendData(unsigned char *buff,int lon);
        int SendPacketNoW(int add,int delay,int size);
        bool RxPacket(unsigned char iden1);
        bool RxBTP();
        bool RxBinData(unsigned char* Frame,int sizerx);
        string GetBinRxFile(void);
        bool SetBinDirectory(wxString dir);
        void RxBinSendBackAck(unsigned char* buff,unsigned char ak);
        unsigned char *GetData(uint16_t *lon);
        int GetDataAddress();
        unsigned char *GetGrDades(uint16_t *lon);
        unsigned char GetIden(void);
        unsigned char ReceiveAllIdens(void);
        bool SendHello();
        int ProcessRx(int num);
        void DiscardRx(int tries=1);
        void ClearRxQueue();

        void Start(int level,int tipus,wxSocketBase *sock);
        void Stop();
        bool IsOnline();
        bool UserCancel();
        string &GetLastError();
        bool EsPotReintentar();
        bool SetSubscriber(string code);
        bool SetAesKey(string key);
        bool UseAes(bool useit);
        bool IsUseAes();
        bool IsProcesBinRx();
        bool IsReliefMode();
        bool DisThreadPoll(bool thread);

        unsigned char* GetTramaTx();
        int GetLenTramaTx();
        unsigned char* GetTramaRx();
        int GetLenTramaRx();
        bool SearchAnswer(bool discard);
        bool SearchAnswer();


    private:
        wxMutex protomutex;
        wxMutex threadmutex;

        unsigned char tramaTx[MAX_BYTES_TRAMA];
        unsigned char tramaRx[MAX_BYTES_TRAMA];
        unsigned char tramaTmp[MAX_BYTES_TRAMA];
        unsigned char BuffAck[32];
        int lonTramaTx;
        int lonTramaRx;
        int lonTramaTmp;
        int avTimeOut;
        //vector<unsigned char *> cuaRx;
        //vector<int> cuaRxLon;
        int RxLon;
        string msgError;
        CSerial *serial;
        wxSocketBase    *psocket;
        unsigned char WantedIden1;
        bool dinsTrama;
        int cntTrama;
        int lonDades;
        int lonBytes;
        int cntTInTrama;
        int cntNoAct;
        bool timeout;
        bool online;
        bool sequencia;
        bool esPotReintentar;
        bool mightPiggyAck;
        bool reliefmode;
        TBinRx BinRx;
        bool isBinRx;
        bool isJRProto;
        int CntBinRx;
        CConnection *connecthandler;
        bool isascii;
        bool threaddisable;
        string aeskey;
        string SN;
        unsigned char Hops;
        AES crypt;
        bool useaes;
        string cccc;
        unsigned int ns;
        CNIOT   NIOT;
    //    PNIOTNotify     *subscriber;
        bool crcOk(unsigned char *trama, int lon);
        bool IsNACK();
        bool IsNACK(unsigned char *trama, int lon);
        bool IsACK();
        bool IsACK(unsigned char *trama, int lon);
        bool IsBTP();
        bool IsBTP(unsigned char *trama, int lon);
        bool IsACKW();
        bool IsACKW(unsigned char *trama, int lon);
        bool IsDades();
        bool IsDades(unsigned char *trama, int lon);
        void AfegirTramaCua(unsigned char *trama, int length);
        bool EnviarACK();
        bool EnviarNACK();
        bool IsTramaDeGrup(unsigned char *trama, int lon);
        bool IsTramaDeGrup();
};

#endif



