#include "panelflashgrup.h"

CPanelFlashGrup::CPanelFlashGrup(wxWindow* parent,wxWindowID id,const wxPoint& pos,const wxSize& size,long style,const wxString& name,CLanguage * lang)
{
    Create(parent,id,pos,size,style,name);

    if((pos==wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(0,0,300,150);
    }

    if((pos!=wxDefaultPosition)&&(size==wxDefaultSize)) {
        SetSize(300,150);
    }
    Lang = lang;
    //punters a 0
    grupFlash=0;
    grid=0;
    vSizer = new wxBoxSizer( wxVERTICAL );
    SetSizer( vSizer );
}

void CPanelFlashGrup::Clear()
{
    if (grid) {
        delete grid;
        grid=0;
        vSizer->Detach(0);
    }

    panelFitxer.clear();
}

CPanelFlashGrup::~CPanelFlashGrup()
{
    Clear();
    grupFlash=0;
}

void CPanelFlashGrup::GenComponents()
{
    if (grupFlash) {
        Clear();

        if (grupFlash->GetTypeCon() == TIPUS_GRUPFLASH_COLUMNES) {
            grid = new wxGrid(this, -1);
            grid->CreateGrid(grupFlash->GetNumElements(), grupFlash->GetNumCols());

            //No posem nom a les files

            //Posem noms a les columnes
            int numCols=grupFlash->GetNumCols();
            for (int i=0; i<numCols; i++) {
                string descr;
                descr = grupFlash->GetColDescr(i);
                grid->SetColLabelValue( i, _wx(descr));
            }
            // titulem les columnes en integer i hexa
            for (int i=0; i<grupFlash->GetNumElements();i++){
                string v;
                v=iToS(i) + " : 0x" +iToH(i);
                grid->SetRowLabelValue(i,_wx(v));
            }

            UpdateValues();

            grid->SetRowLabelSize(100);
/*            wxFont *font=new wxFont();
            grid->SetDefaultCellFont(font);*/
            grid->SetDefaultCellAlignment(wxALIGN_RIGHT, wxALIGN_LEFT);
            grid->AutoSizeColumns();
            grid->AutoSizeRows();

            grid->DisableDragGridSize();
            //grid->EnableDragColSize();
            grid->DisableDragRowSize();
            grid->EnableEditing(true);

            grid->ForceRefresh();

            vSizer->Add( grid, wxSizerFlags(1).Align(0).Expand());
            vSizer->Layout();
        }
        else {
            for (int i=0; i<grupFlash->GetNumFiles(); i++) {
                CFitxerFlash *fitxer = grupFlash->GetFile(i);
                panelFitxer.push_back(new CPanelFile(this, -1, wxDefaultPosition, wxDefaultSize, wxNO_BORDER,"",Lang));
                int index = panelFitxer.size() -1;
                panelFitxer[index]->SetFitxer(fitxer);
                vSizer->Add( panelFitxer[index], wxSizerFlags(0).Align(0).Expand().Border(wxBOTTOM, 5));
            }
            vSizer->Layout();
        }
    }
}

bool CPanelFlashGrup::SetFlashGrup(CGroupFlash *grupFlash)
{
    this->grupFlash = grupFlash;
    GenComponents();
    return true;
}

void CPanelFlashGrup::UnSetFlashGrup()
{
    grupFlash=0;
    Clear();
}

CGroupFlash *CPanelFlashGrup::GetFlashGrup()
{
    return grupFlash;
}

//Aplica el valor del camp de text al CGroupFlash
bool CPanelFlashGrup::ApplyValues()
{
    if (!grupFlash)
        return false;

    if (grupFlash->GetTypeCon() == TIPUS_GRUPFLASH_COLUMNES) {
        if (!grid)
            return false;

        for (int i=0; i<grupFlash->GetNumElements(); i++) {
            for (int j=0; j<grupFlash->GetNumCols(); j++) {
                wxString val = grid->GetCellValue( i, j);
                string buf = val.c_str();
                grupFlash->SetElementCol(i, j, buf);
            }
        }
    }
    else {
        for (int i=0; i<panelFitxer.size(); i++) {
            panelFitxer[i]->ApplyValues();
        }

    }

    return true;
}

//Actualitza els valors dels codis al grid
//(es el contrari que ApplyValues()
bool CPanelFlashGrup::UpdateValues()
{
    if ((!grupFlash) || (!grid))
        return false;

    if (grupFlash->GetTypeCon() == TIPUS_GRUPFLASH_COLUMNES) {
        for (int i=0; i<grupFlash->GetNumElements(); i++) {
            for (int j=0; j<grupFlash->GetNumCols(); j++) {
                string val = grupFlash->GetElementColString(i, j);
                grid->SetCellValue( i, j, _wx(val));
            }
        }
    }
    else {
        for (int i=0; i<panelFitxer.size(); i++) {
            panelFitxer[i]->UpdateValues();
        }

    }

    return true;
}
