#include "wx_pch.h"
#include "nuvalink.h"
#include "EventsPanel.h"
#define _wx(x) (wxString)((string)x).c_str()


#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(EventsPanel)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(EventsPanel)
//*)

//(*IdInit(EventsPanel)
const long EventsPanel::ID_LISTCTRL1 = wxNewId();
const long EventsPanel::ID_CHECKLISTBOX1 = wxNewId();
const long EventsPanel::ID_SPEEDBUTTON1 = wxNewId();
const long EventsPanel::ID_SPEEDBUTTON2 = wxNewId();
//*)
const long EventsPanel::idBotoViewBin = wxNewId();

BEGIN_EVENT_TABLE(EventsPanel,wxPanel)
	//(*EventTable(EventsPanel)
	//*)
END_EVENT_TABLE()

EventsPanel::EventsPanel(wxWindow* parent,wxWindowID id,Cnuvalink *nuvalink,CLanguage* lang,CEventList *ev,string idp,string ver)
{
    this->PanelId=idp;
    this->PanelVersion=ver;
    this->events=ev;
    this->nuvalink=nuvalink;
    this->Lang=lang;
	BuildContent(parent,id);
	ListFill();
}

void EventsPanel::BuildContent(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(EventsPanel)
	wxFlexGridSizer* FlexGridSizer1;
	wxStaticBoxSizer* StaticBoxSizer1;
	wxStaticBoxSizer* StaticBoxSizer2;

	Create(parent, wxID_ANY, wxDefaultPosition, wxSize(1193,576), wxTAB_TRAVERSAL, _T("wxID_ANY"));
	FlexGridSizer1 = new wxFlexGridSizer(0, 1, 0, 0);
	StaticBoxSizer1 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("List"));
	llista = new wxListCtrl(this, ID_LISTCTRL1, wxDefaultPosition, wxSize(1200,550), wxLC_REPORT|wxVSCROLL|wxHSCROLL, wxDefaultValidator, _T("ID_LISTCTRL1"));
	StaticBoxSizer1->Add(llista, 1, wxALL|wxEXPAND, 5);
	FlexGridSizer1->Add(StaticBoxSizer1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer2 = new wxStaticBoxSizer(wxHORIZONTAL, this, _("Filter"));
	CheckListBox1 = new wxCheckListBox(this, ID_CHECKLISTBOX1, wxDefaultPosition, wxSize(180,90), 0, 0, 0, wxDefaultValidator, _T("ID_CHECKLISTBOX1"));
	CheckListBox1->Append(Lang->GetAppMiss("MISS_PANEL_CHK_ALARMS"));
	CheckListBox1->Append(Lang->GetAppMiss("MISS_PANEL_CHK_OMISIO"));
	CheckListBox1->Append(Lang->GetAppMiss("MISS_PANEL_CHK_AVERY"));
	CheckListBox1->Append(Lang->GetAppMiss("MISS_PANEL_CHK_ONOFF"));
	CheckListBox1->Append(Lang->GetAppMiss("MISS_PANEL_CHK_SUPERVISIO"));
	CheckListBox1->Append(Lang->GetAppMiss("MISS_PANEL_CHK_TEST"));
	StaticBoxSizer2->Add(CheckListBox1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticBoxSizer2->Add(-1,-1,2, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	wxBitmap ButExport_BMP(_("./icons/export.png"), wxBITMAP_TYPE_ANY);
	ButExport = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, ButExport_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(60,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	ButExport->SetMinSize(wxSize(-1,-1));
	ButExport->SetUserData(0);
	StaticBoxSizer2->Add(ButExport, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	wxBitmap ButClear_BMP(_("./icons/clear.png"), wxBITMAP_TYPE_ANY);
	ButClear = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, ButClear_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(60,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	ButClear->SetUserData(0);
	StaticBoxSizer2->Add(ButClear, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED, 5);
	FlexGridSizer1->Add(StaticBoxSizer2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(FlexGridSizer1);
	SetSizer(FlexGridSizer1);
	Layout();

	Connect(ID_CHECKLISTBOX1,wxEVT_COMMAND_CHECKLISTBOX_TOGGLED,(wxObjectEventFunction)&EventsPanel::OnCheckListBox1Toggled);
	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EventsPanel::OnButExportLeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&EventsPanel::OnButClearLeftClick);
	//*)
    thumbnail=NULL;
    if (isSecPanel(PanelId)){
        viewButton = new wxBitmapButton(this, idBotoViewBin, wxBitmap(wxImage(wxT("icons/estat/cam_32.png"))), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW, wxDefaultValidator);
        StaticBoxSizer2->Add( viewButton );
    }
    showAlarm=true;
    showONOFF=true;
    showSupervision=true;
    showAveria=true;
    showTest=true;
    showBypass=true;

}

void EventsPanel::Clear()
{
    llista->ClearAll();

    if (thumbnail){
        delete thumbnail;
        thumbnail=0;
    }
}

EventsPanel::~EventsPanel()
{
	//(*Destroy(EventsPanel)
	//*)

	Clear();
}


void EventsPanel::OnButExportLeftClick(wxCommandEvent& event)
{
    wxFileDialog* fileDlg = new wxFileDialog(this, Lang->GetAppMiss("MISS_DIALOG_TIT_EXPORTCSV"), "", "", "*.csv", wxSAVE);

    if (fileDlg->ShowModal() == wxID_OK) {
        //TODO: si el fitxer seleccionat ja existeix hauriem de dir-li
        wxString wxNomFitxer = fileDlg->GetFilename();
        wxString wxNomDirectori = fileDlg->GetDirectory();
        string nomFitxer = wxNomDirectori.c_str();
        nomFitxer += '\\';
        nomFitxer += wxNomFitxer.c_str();

        SetCursor(wxCURSOR_WAIT);
        bool result = ExportarEvents(nomFitxer);
        SetCursor(wxNullCursor);
        if (result) {
            wxMessageBox(Lang->GetAppMiss("MISS_DIALOG_EXP_EXPORTED"), Lang->GetAppMiss("MISS_DIALOG_TIT_INFO"));
        } else {
            wxMessageBox(_wx(msgError), Lang->GetAppMiss("MISS_DIALOG_TIT_ERROR"));
        }
    }
}

bool EventsPanel::ExportarEvents(string& fileName)
{
    ofstream fitxer;
    string buffer;

    fitxer.open( (char *) fileName.c_str(), ifstream::out );
    if (fitxer.good()) {
        GeneraCSVEvents(&buffer);
        fitxer << buffer;
        fitxer.close();
    }
    else {
        msgError = Lang->GetAppMiss("MISS_ERROR_WRITE_FILE") + fileName;
        return false;
    }

    return true;
}

void EventsPanel::OnButClearLeftClick(wxCommandEvent& event)
{
    wxMessageDialog *msgDlg = new wxMessageDialog(this, Lang->GetAppMiss("MISS_DIALOG_ASK_CLEAR"),
    Lang->GetAppMiss("MISS_DIALOG_TIT_CLEAR"), wxYES_NO);

    int result = msgDlg->ShowModal();

    if (result == wxID_YES) {
        events->Clear();
        UpdateValues();
    }
}

void EventsPanel::OnCheckListBox1Toggled(wxCommandEvent& event)
{
    showAlarm=false;
    if (CheckListBox1->IsChecked(0))
        showAlarm=true;

    showBypass=false;
    if (CheckListBox1->IsChecked(1))
        showBypass=true;

    showAveria=false;
    if (CheckListBox1->IsChecked(2))
        showAveria=true;

    showONOFF=false;
    if (CheckListBox1->IsChecked(3))
        showONOFF=true;

    showSupervision=false;
    if (CheckListBox1->IsChecked(4))
        showSupervision=true;

    showTest=false;
    if (CheckListBox1->IsChecked(5))
        showTest=true;

    ListFill();
}


void EventsPanel::ListFill()
{
    int col=0;

    if ((llista) && (events)) {
        llista->ClearAll();
        if (isSecPanel(PanelId) && thumbnail!=NULL){
            thumbnail->Clear();
        }

        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_DATE"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_TIME"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_CODE"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_TYPE"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_DESCR"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_ZONEUSER"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_AREA"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_CLIENT"));
        int colreported=8;
        if (isSecPanel(PanelId)){
            llista->InsertColumn(col++, "Bin Num");
            llista->InsertColumn(col++, "Bin File");
            colreported=10;
        }
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_REPORTED"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_GEO"));
        llista->InsertColumn(col++, Lang->GetAppMiss("MISS_PANEL_COL_ANALOG"));
        int fi = events->GetNumEvents(), i, j=0;
        for ( int rr = 0; rr < fi; rr++ ) {
            i = fi - 1 - rr;
            CEvent *event = events->GetEvent(i);
            if ( ( (event->IsTipusAlarm() && showAlarm)
                || (event->IsTipusONOFF() && showONOFF)
                || (event->IsTipusSupervisio() && showSupervision)
                || (event->IsTipusTest() && showTest)
                || (event->IsTipusAnalog() && showTest)
                || (event->IsTipusGeo() && showTest)
                || (event->IsTipusOmisio() && showBypass)
                || (event->IsTipusAveria() && showAveria)
                || (event->IsTipusOutput() && showSupervision)
                || (event->IsTipusBus() && showSupervision))
            ) {
                long tmp = llista->InsertItem(j, "-", 0);
                llista->SetItemData(tmp, j);
                if (event->IsTipusAlarm()) {
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(0xff,140,120));  // vermell
                    llista->SetItem( item );
                }else if (event->IsTipusBin()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(123,170,200));  // blau mar�
                    llista->SetItem( item );
                }else if (event->IsTipusONOFF()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(100,165,50));  // verd
                    llista->SetItem( item );
                }else if (event->IsTipusAveria()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(0xc0,0xc0,0xc0));  // gris
                    llista->SetItem( item );
                }else if ( event->IsTipusOmisio()){
                    wxListItem item;
                    item.SetId(tmp);
                    item.SetBackgroundColour(wxColor(160,120,50));  // marron
                    llista->SetItem( item );
                }

                string text = event->GetData();
                int CurYear=&text[6];
                // ------------------------------------------ reported -------------------------
                string reported="";
                if (event->IsCSReported())
                    reported=Lang->GetAppMiss("MISS_PANEL_CENTRAL_STATION");

                llista->SetItem(j, 0, _wx(text));
                text = event->GetHora();
                llista->SetItem(j, 1, _wx(text));
                text = event->GetContactID();
                llista->SetItem(j, 2, _wx(text));
                char buf[8];
                text = event->GetTypeCon();
                llista->SetItem(j, 3, _wx(text));
                text = event->GetDescr();
                llista->SetItem(j, 4, _wx(text));
                unsigned int zona = event->GetZona();
                sprintf(buf, "%d", zona);
                text = buf;
                llista->SetItem(j, 5, _wx(text));
                int area = event->GetArea();
                text = iToS(area);
                if (area)
                    area--;

                llista->SetItem(j, 6, _wx(text));

                //llista->SetColumnWidth(6,wxLIST_AUTOSIZE)
                text = event->GetAbonat();
                llista->SetItem(j, 7, _wx(text));

                llista->SetItem(j, colreported,_wx(reported));

                llista->SetItem(j, colreported+1,_wx(event->GetFix()));

                llista->SetItem(j, colreported+2,_wx(event->GetAnalog(0)+":"+event->GetAnalog(1)));

                j++;
            }
        }
        if (fi) {
            llista->SetColumnWidth( 0, wxLIST_AUTOSIZE );
            llista->SetColumnWidth( 1, wxLIST_AUTOSIZE );
            llista->SetColumnWidth( 4, wxLIST_AUTOSIZE );
            llista->SetColumnWidth( 5, wxLIST_AUTOSIZE_USEHEADER);
            llista->SetColumnWidth( 6, wxLIST_AUTOSIZE_USEHEADER);
            llista->SetColumnWidth( 7, wxLIST_AUTOSIZE_USEHEADER);
            llista->SetColumnWidth( 8, wxLIST_AUTOSIZE_USEHEADER);
            llista->SetColumnWidth( 9, wxLIST_AUTOSIZE_USEHEADER );
            llista->SetColumnWidth( 10, wxLIST_AUTOSIZE );
            //llista->Fit();
        }
    }
}


bool EventsPanel::SetGrups(CGroup *grupZAlias,CGroup *grupAAlias, CGroup *grupUAlias, CGroup *grupOAlias)
{

    for (int i=0;i<64;i++)
    {
        string temp=Lang->GetAppMiss("MISS_PANEL_COL_ZONE").c_str();
        temp+=" ";
        temp+=iToS(i+1);
        CZalias[i]=temp;
    }
    if(grupZAlias!=0)
    {
        for (int i=0; i<grupZAlias->GetNumChildCodi(); i++) {
            this->codi = grupZAlias->GetChildCodi(i);
            CZalias[i]= codi->GetStringVal();
        }
    }

    for (int i=0;i<64;i++){
        string temp =Lang->GetAppMiss("MISS_PANEL_COL_AREA").c_str();
        temp +=" ";
        temp +=iToS(i+1);
        CAalias[i]=temp;
    }
    if(grupAAlias!=0)
    {
        for (int i=0; i<grupAAlias->GetNumChildCodi(); i++) {
            this->codi = grupAAlias->GetChildCodi(i);
            CAalias[i]= codi->GetStringVal();
        }
    }
    for (int i=0;i<64;i++){
        string temp =Lang->GetAppMiss("MISS_PANEL_COL_USER").c_str();
        temp +=" ";
        temp +=iToS(i+1);
        CUalias[i]=temp;
    }
    if (grupUAlias!=0)
    {
        if ((grupUAlias->GetNumChildCodi()==9) || (grupUAlias->GetNumChildCodi()==17)){
            // hi ha codi de instalador.
            for (int i=0; i<grupUAlias->GetNumChildCodi(); i++) {
                this->codi = grupUAlias->GetChildCodi(i);
                CUalias[i]= codi->GetStringVal();
            }
        }else{
            CUalias[0]="System";
            // no hi ha codi de instalador.
            for (int i=1,j=0; i<grupUAlias->GetNumChildCodi(); i++,j++) {
                this->codi = grupUAlias->GetChildCodi(j);
                CUalias[i]= codi->GetStringVal();
            }
        }
    }
    CUalias[63]=CUalias[62]="nuvalink";

    for (int i=0;i<64;i++){
        string temp=Lang->GetAppMiss("MISS_PANEL_OUTPUT").c_str();
        temp+=" ";
        temp+=iToS(i+1);
        COalias[i]=temp;
    }
    if(grupOAlias!=0)
    {
        for (int i=0; i<grupOAlias->GetNumChildCodi(); i++) {
            this->codi = grupOAlias->GetChildCodi(i);
            COalias[i]= codi->GetStringVal();
        }
    }
    return true;
}

bool EventsPanel::ApplyValues()
{
    if (!events)
        return false;

    return true;
}

bool EventsPanel::UpdateValues()
{
    if (!events)
        return false;

    ListFill();

    return true;
}

CEventList *EventsPanel::GetLlista()
{
    return events;
}



bool EventsPanel::GeneraCSVEvents(string *csv)
{
    string separador = ";";

    *csv = Lang->GetAppMiss("MISS_PANEL_COL_DATE").c_str() + separador;
    *csv += Lang->GetAppMiss("MISS_PANEL_COL_TIME").c_str() + separador;
    *csv += Lang->GetAppMiss("MISS_PANEL_COL_CODE").c_str() + separador;
    *csv += Lang->GetAppMiss("MISS_PANEL_COL_TYPE").c_str() + separador;
    *csv += Lang->GetAppMiss("MISS_PANEL_COL_DESCR").c_str() + separador;
    *csv += Lang->GetAppMiss("MISS_PANEL_COL_ZONEUSER").c_str() + separador;
    *csv += Lang->GetAppMiss("MISS_PANEL_COL_AREA").c_str() + separador;
    *csv += Lang->GetAppMiss("MISS_PANEL_COL_CLIENT").c_str() + separador;

    *csv += "\n";

    int fi = events->GetNumEvents(), i;
    for ( int rr = 0; rr < fi; rr++ ) {
        /*if (invert==false)*/ i = fi - 1 - rr;
        //else i = rr;

        CEvent *event = events->GetEvent(i);

        if ( ((event->IsTipusAlarm() && showAlarm)
            || (event->IsTipusONOFF() && showONOFF)
            || (event->IsTipusSupervisio() && showSupervision)
            || (event->IsTipusTest() && showTest)
            || (event->IsTipusOmisio() && showBypass)
            || (event->IsTipusAveria() && showAveria)
            || (event->IsTipusBus() && showSupervision))
        ){
            *csv += event->GetData() + separador;
            *csv += event->GetHora() + separador;
            *csv += event->GetContactID() + separador;
            *csv += event->GetTypeCon() + separador;
            *csv += "\"" + event->GetDescr() + "\"" + separador;

            char buf[8];
            int zona = event->GetZona();
            sprintf(buf, "%d", zona);
            *csv += buf + separador;
            *csv += iToS(event->GetArea()) + separador;
            *csv += event->GetAbonat() + separador;

            *csv += "\n";
        }
    }
    return true;
}


