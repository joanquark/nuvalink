#include "wx_pch.h"
#include "ConfigDialog.h"

#ifndef WX_PRECOMP
	//(*InternalHeadersPCH(ConfigDialog)
	#include <wx/intl.h>
	#include <wx/string.h>
	//*)
#endif
//(*InternalHeaders(ConfigDialog)
//*)

//(*IdInit(ConfigDialog)
const long ConfigDialog::ID_STATICTEXT7 = wxNewId();
const long ConfigDialog::ID_CHOICE3 = wxNewId();
const long ConfigDialog::ID_STATICTEXT1 = wxNewId();
const long ConfigDialog::ID_CHOICE1 = wxNewId();
const long ConfigDialog::ID_STATICTEXT2 = wxNewId();
const long ConfigDialog::ID_CHOICE2 = wxNewId();
const long ConfigDialog::ID_STATICTEXT9 = wxNewId();
const long ConfigDialog::ID_TEXTCTRL5 = wxNewId();
const long ConfigDialog::ID_STATICTEXT3 = wxNewId();
const long ConfigDialog::ID_TEXTCTRL1 = wxNewId();
const long ConfigDialog::ID_STATICTEXT4 = wxNewId();
const long ConfigDialog::ID_TEXTCTRL2 = wxNewId();
const long ConfigDialog::ID_STATICTEXT5 = wxNewId();
const long ConfigDialog::ID_TEXTCTRL3 = wxNewId();
const long ConfigDialog::ID_STATICTEXT6 = wxNewId();
const long ConfigDialog::ID_TEXTCTRL4 = wxNewId();
const long ConfigDialog::ID_SPEEDBUTTON1 = wxNewId();
const long ConfigDialog::ID_SPEEDBUTTON2 = wxNewId();
//*)

BEGIN_EVENT_TABLE(ConfigDialog,wxDialog)
	//(*EventTable(ConfigDialog)
	//*)
END_EVENT_TABLE()

ConfigDialog::ConfigDialog(wxWindow* parent,wxWindowID id)
{
	BuildContent(parent,id);
}

void ConfigDialog::BuildContent(wxWindow* parent,wxWindowID id)
{
	//(*Initialize(ConfigDialog)
	wxBoxSizer* BoxSizer1;
	wxBoxSizer* BoxSizer2;
	wxBoxSizer* BoxSizer3;
	wxFlexGridSizer* FlexGridSizer1;

	Create(parent, wxID_ANY, _("Config"), wxDefaultPosition, wxDefaultSize, wxDEFAULT_DIALOG_STYLE, _T("wxID_ANY"));
	BoxSizer1 = new wxBoxSizer(wxVERTICAL);
	BoxSizer3 = new wxBoxSizer(wxHORIZONTAL);
	StaticText7 = new wxStaticText(this, ID_STATICTEXT7, _("Select Connection"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT7"));
	BoxSizer3->Add(StaticText7, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice1 = new wxChoice(this, ID_CHOICE3, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE3"));
	Choice1->Append(_("USB"));
	Choice1->Append(_("RS485"));
	Choice1->Append(_("TCP-IP"));
	Choice1->Append(_("Cloud"));
	BoxSizer3->Add(Choice1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	BoxSizer1->Add(BoxSizer3, 0, wxALL|wxEXPAND, 5);
	FlexGridSizer1 = new wxFlexGridSizer(3, 4, 5, 0);
	StaticText1 = new wxStaticText(this, ID_STATICTEXT1, _("RS485 ComPort"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT1"));
	FlexGridSizer1->Add(StaticText1, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	Choice2 = new wxChoice(this, ID_CHOICE1, wxDefaultPosition, wxDefaultSize, 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE1"));
	FlexGridSizer1->Add(Choice2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText2 = new wxStaticText(this, ID_STATICTEXT2, _("RS485 speed"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT2"));
	FlexGridSizer1->Add(StaticText2, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	choiceRS485Speed = new wxChoice(this, ID_CHOICE2, wxDefaultPosition, wxSize(90,21), 0, 0, 0, wxDefaultValidator, _T("ID_CHOICE2"));
	choiceRS485Speed->Append(_("4800"));
	choiceRS485Speed->Append(_("9600"));
	choiceRS485Speed->Append(_("19200"));
	choiceRS485Speed->Append(_("38400"));
	choiceRS485Speed->Append(_("57600"));
	choiceRS485Speed->Append(_("115200"));
	FlexGridSizer1->Add(choiceRS485Speed, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText9 = new wxStaticText(this, ID_STATICTEXT9, _("Virtual Modem Port"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT9"));
	FlexGridSizer1->Add(StaticText9, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtVCOM = new wxTextCtrl(this, ID_TEXTCTRL5, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL5"));
	FlexGridSizer1->Add(TxtVCOM, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	FlexGridSizer1->Add(-1,-1,1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText3 = new wxStaticText(this, ID_STATICTEXT3, _("IP Cloud"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT3"));
	FlexGridSizer1->Add(StaticText3, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtIPCloud = new wxTextCtrl(this, ID_TEXTCTRL1, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL1"));
	FlexGridSizer1->Add(TxtIPCloud, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText4 = new wxStaticText(this, ID_STATICTEXT4, _("Port Cloud"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT4"));
	FlexGridSizer1->Add(StaticText4, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtPortCloud = new wxTextCtrl(this, ID_TEXTCTRL2, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PASSWORD, wxDefaultValidator, _T("ID_TEXTCTRL2"));
	FlexGridSizer1->Add(TxtPortCloud, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText5 = new wxStaticText(this, ID_STATICTEXT5, _("IP PRN"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT5"));
	FlexGridSizer1->Add(StaticText5, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtIPPRN = new wxTextCtrl(this, ID_TEXTCTRL3, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL3"));
	FlexGridSizer1->Add(TxtIPPRN, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	StaticText6 = new wxStaticText(this, ID_STATICTEXT6, _("Port PRN"), wxDefaultPosition, wxDefaultSize, 0, _T("ID_STATICTEXT6"));
	FlexGridSizer1->Add(StaticText6, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	TxtPortPRN = new wxTextCtrl(this, ID_TEXTCTRL4, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, wxDefaultValidator, _T("ID_TEXTCTRL4"));
	FlexGridSizer1->Add(TxtPortPRN, 1, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	BoxSizer1->Add(FlexGridSizer1, 2, wxALL|wxEXPAND, 5);
	BoxSizer2 = new wxBoxSizer(wxHORIZONTAL);
	wxBitmap SpeedButton1_BMP(_("./icons/toolbar/ok.png"), wxBITMAP_TYPE_ANY);
	SpeedButton1 = new wxSpeedButton(this, ID_SPEEDBUTTON1, wxEmptyString, SpeedButton1_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(60,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON1"));
	SpeedButton1->SetUserData(0);
	BoxSizer2->Add(SpeedButton1, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	wxBitmap SpeedButton2_BMP(_("./icons/toolbar/exit.png"), wxBITMAP_TYPE_ANY);
	SpeedButton2 = new wxSpeedButton(this, ID_SPEEDBUTTON2, wxEmptyString, SpeedButton2_BMP, 0, 2, -1, true, wxDefaultPosition, wxSize(60,60), wxTAB_TRAVERSAL, wxDefaultValidator, _T("ID_SPEEDBUTTON2"));
	SpeedButton2->SetUserData(0);
	BoxSizer2->Add(SpeedButton2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL|wxSHAPED|wxFIXED_MINSIZE, 5);
	BoxSizer1->Add(BoxSizer2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL|wxALIGN_CENTER_VERTICAL, 5);
	SetSizer(BoxSizer1);
	BoxSizer1->Fit(this);
	BoxSizer1->SetSizeHints(this);

	Connect(ID_CHOICE1,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&ConfigDialog::OnChoice2Select);
	Connect(ID_CHOICE2,wxEVT_COMMAND_CHOICE_SELECTED,(wxObjectEventFunction)&ConfigDialog::OnChoice3Select);
	Connect(ID_SPEEDBUTTON1,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ConfigDialog::OnSpeedButton1LeftClick);
	Connect(ID_SPEEDBUTTON2,wxEVT_COMMAND_LEFT_CLICK,(wxObjectEventFunction)&ConfigDialog::OnSpeedButton2LeftClick);
	//*)
}

ConfigDialog::~ConfigDialog()
{
	//(*Destroy(ConfigDialog)
	//*)
}

void ConfigDialog::SetCfg(CConfig* con)
{
    this->config=con;
    Choice1->SetSelection(config->GetConnectionType());

    const HKEY HK=HKEY_LOCAL_MACHINE;
    const char * cle="HARDWARE\\DEVICEMAP\\SERIALCOMM";

    //Obrim el registre
    HKEY ret;
    RegOpenKeyEx(HK,cle,0,KEY_ALL_ACCESS,&ret);

    unsigned long lNom=128, lVal=128, ty=0;
    char nomVal[128];
    unsigned char valor[128];

    long res;
    int n=0;
    int comport=255;
    string temp;
    do {
        lNom=128;
        lVal=128;
        res = RegEnumValue( ret, n, nomVal, &lNom, 0, &ty, valor, &lVal);
        if (res == ERROR_NO_MORE_ITEMS)
            break;

        temp = nomVal;
        wxString np = (char *)valor;
        comport = ascii2nibble(valor[3]);
        //  List all ports 191217-
/*        if (temp.find("VCP",0) != string::npos){

        }else if (temp.find("USB",0) != string::npos){

        }*/
        Choice2->Append("COM"+iToS(comport));
        //Choice3->Append("COM"+iToS(comport));


        n++;
    } while (res != ERROR_NO_MORE_ITEMS && n<128);
    RegCloseKey(ret);
    //Choice2->SetSelection(0);
    //Choice3->SetSelection(0);
    TxtIPCloud->SetValue(config->GetParamTcpIp("ipclouddest"));
    TxtPortCloud->SetValue(config->GetParamTcpIp("ipcloudport"));
    TxtIPPRN->SetValue(config->GetParamTcpIp("ipprndest"));
    TxtPortPRN->SetValue(config->GetParamTcpIp("ipprnport"));
}

void ConfigDialog::OnSpeedButton1LeftClick(wxCommandEvent& event)
{

    config->SetParamTcpIp("ipclouddest",TxtIPCloud->GetValue());
    config->SetParamTcpIp("ipcloudport",TxtPortCloud->GetValue());
    config->SetParamTcpIp("ipprndest",TxtIPPRN->GetValue());
    config->SetParamTcpIp("ipprnport",TxtPortPRN->GetValue());
    config->SetConnectionType(Choice1->GetSelection());

    if (Choice1->GetSelection()==TYPE_RS485_CONNECTION){
        if (Choice2->GetSelection()!=wxNOT_FOUND){
            wxString comport=Choice2->GetStringSelection();
            long com;
            comport.ToLong(&com);
            config->SetParamConexio("portcom",(wxString)comport.substr(3,4));

            wxString speed=choiceRS485Speed->GetStringSelection();
            config->SetParamConexio("baud",speed);


        }
    }

    EndModal(Choice1->GetSelection());


}

void ConfigDialog::OnSpeedButton2LeftClick(wxCommandEvent& event)
{
    EndModal(-1);
}

void ConfigDialog::OnChoice2Select(wxCommandEvent& event)
{
}

void ConfigDialog::OnChoice3Select(wxCommandEvent& event)
{
}
