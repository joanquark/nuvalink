#ifndef ESTATPANEL_H
#define ESTATPANEL_H

#ifndef WX_PRECOMP
	//(*HeadersPCH(EstatPanel)
	#include <wx/listctrl.h>
	#include <wx/panel.h>
	#include <wx/sizer.h>
	#include <wx/stattext.h>
	//*)
#endif
//(*Headers(EstatPanel)
#include <wxSpeedButton.h>
//*)

#include "Status.h"
class Cnuvalink;
#include "languages/CLanguage.h"


class EstatPanel: public wxPanel
{
	public:

		EstatPanel(wxWindow* parent,wxWindowID id=wxID_ANY);
		virtual ~EstatPanel();
		void SetStatus(CStatus * st,Cnuvalink* nuvalink,CLanguage* lang,string id, string ver);
		void UnSetStatus();
		CStatus *GetStatus();
        void UpdateValues();
		void ActivateOutputs(bool activar);
		void ApplyValues(){
		};
#ifdef _NOTDEF
        bool SetGrups(CGroup *grupZAlias, CGroup *grupAAlias, CGroup *grupOAlias);
#endif

		//(*Declarations(EstatPanel)
		wxListCtrl* ListA;
		wxListCtrl* ListO;
		wxListCtrl* ListSys;
		wxListCtrl* ListZ;
		wxSpeedButton* BtArm;
		wxSpeedButton* BtCam;
		wxSpeedButton* BtDisarm;
		wxSpeedButton* BtEye;
		wxSpeedButton* BtHome;
		wxSpeedButton* BtNight;
		wxSpeedButton* BtOff;
		wxSpeedButton* BtOmit;
		wxSpeedButton* BtOn;
		wxSpeedButton* BtResetBatt;
		wxSpeedButton* BtRestore;
		wxSpeedButton* BtSync;
		wxSpeedButton* BtTest;
		wxSpeedButton* sampleAn;
		wxStaticBoxSizer* StaticBoxSizer1;
		wxStaticBoxSizer* StaticBoxSizer2;
		wxStaticBoxSizer* StaticBoxSizer3;
		wxStaticBoxSizer* StaticBoxSizer4;
		wxStaticText* PanelTxt;
		wxStaticText* VerTxt;
		//*)

	protected:

		//(*Identifiers(EstatPanel)
		static const long ID_LISTCTRL1;
		static const long ID_SPEEDBUTTON1;
		static const long ID_SPEEDBUTTON2;
		static const long ID_SPEEDBUTTON3;
		static const long ID_SPEEDBUTTON4;
		static const long ID_LISTCTRL2;
		static const long ID_SPEEDBUTTON5;
		static const long ID_SPEEDBUTTON6;
		static const long ID_SPEEDBUTTON7;
		static const long ID_SPEEDBUTTON8;
		static const long ID_LISTCTRL3;
		static const long ID_SPEEDBUTTON9;
		static const long ID_SPEEDBUTTON10;
		static const long ID_LISTCTRL4;
		static const long ID_SPEEDBUTTON11;
		static const long ID_SPEEDBUTTON13;
		static const long ID_SPEEDBUTTON14;
		static const long ID_SPEEDBUTTON12;
		static const long ID_STATICTEXT1;
		static const long ID_STATICTEXT2;
		//*)

	private:
        CStatus* estat;
        Cnuvalink* nuvalink;
        CLanguage* Lang;
        bool listzselect;

		//(*Handlers(EstatPanel)
		void OnBtArmLeftClick(wxCommandEvent& event);
		void OnBtDisarmLeftClick(wxCommandEvent& event);
		void OnSpeedButton1LeftClick(wxCommandEvent& event);
		void OnBtNightLeftClick(wxCommandEvent& event);
		void OnBtHomeLeftClick(wxCommandEvent& event);
		void OnBtSyncLeftClick(wxCommandEvent& event);
		void OnBtTestLeftClick(wxCommandEvent& event);
		void OnBtOnLeftClick(wxCommandEvent& event);
		void OnBtOffLeftClick(wxCommandEvent& event);
		void OnBtOmitLeftClick(wxCommandEvent& event);
		void OnBtRestoreLeftClick(wxCommandEvent& event);
		void OnBtCamLeftClick(wxCommandEvent& event);
		void OnBtEyeLeftClick(wxCommandEvent& event);
		void OnListOBeginDrag(wxListEvent& event);
		void OnBtResetBattLeftClick(wxCommandEvent& event);
		void OnsampleAnLeftClick(wxCommandEvent& event);
		//*)

	protected:

		void BuildContent(wxWindow* parent,wxWindowID id);
		DECLARE_EVENT_TABLE()
};

#endif
